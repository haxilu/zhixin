# 知心编译器-知语言

#### 介绍
知心编译器是完全支持中文（汉语或华语）的全中文编译器，可跨平台（windows和linux）。目标：为中文提供更好的中文编程开源编译器，编译出的源代码更优化。知心编译器、知音IDE和知意server共同构建完善的编程环境。开源编译器源码下载。特别声明：本编译器基于tcc优化汉化，旨在打造全中文编译器完全遵守开源协议。

#### 软件架构
语法制导一遍式快速编译。


#### 安装教程

1.  复制本程序目录下的“wei”文件夹路径，然后添加到系统环境变量“path”下
2.  “cmd”打开命令行窗口输入“zhi”命令即可获取“知心编译器”简明使用教程。
3.  目前可以编译“*.c”和“*.z”类型文件

#### 使用说明

1.  zhi hello.c 或zhi hello -o hello.exe 即可编译获取hello.exe可以执行文件
2.  zhi -run hello.c或zhi -run hello.z 无需编译即可直接运行*.c或*.z脚本源文件。
3.  zhi -v 显示知心编译器版本信息
4.  zhi -h 获取知心编译器简明使用教程
5.  zhi -about 获取知心编译器相关信息
6.  build-zhi.bat 在Windows平台使用此批处理文件，可以完成知心编译器自己编译自己（编译器自举）
7.  C语言的源码（C99标准）只需要修改“main(){}”为“主函数（）{}”即可完美运行编译。
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特别声明：本编译器基于tcc优化汉化。旨在打造全中文编译器，完全遵守开源协议。
#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
