/*
 * 核心库：hexinku.c生成hexinku.dll。此文件只在zhi.c第11行一个文件中引用，是知语言的基本库。
 */

#if !defined 是_源文件 || 是_源文件
#include "词法分析.c"
#include "语法分析.c"
#include "ELF文件处理.c"
#include "run开关.c"
#ifdef ZHI_TARGET_I386
#include "i386-gen.c"
#include "i386-link.c"
#include "i386-asm.c"
#elif defined(ZHI_TARGET_ARM)
#include "arm-gen.c"
#include "arm-link.c"
#include "arm-asm.c"
#elif defined(ZHI_TARGET_ARM64)
#include "arm64-gen.c"
#include "arm64-link.c"
#include "arm-asm.c"
#elif defined(ZHI_TARGET_C67)
#include "c67-gen.c"
#include "c67-link.c"
#include "COFF文件处理.c"
#elif defined(ZHI_TARGET_X86_64)
#include "x86_64-gen.c"
#include "x86_64-link.c"
#include "i386-asm.c"
#elif defined(ZHI_TARGET_RISCV64)
#include "riscv64-gen.c"
#include "riscv64-link.c"
#include "riscv64-asm.c"
#else
#error unknown target
#endif
#ifdef 配置_ZHI_汇编
#include "汇编处理.c"
#endif
#ifdef ZHI_TARGET_PE
#include "PE文件输出.c"
#endif
#ifdef ZHI_TARGET_MACHO
#include "MACH系统O文件处理.c"
#endif
#endif /* 是_源文件 */

#include "zhi.h"

/********************************************************/
/* 全局变量 */

#ifdef 内存_调试
static int 数量_states;
#endif

/*************************#ifdef _WIN32-开始*******************************/
#ifdef _WIN32
静态_函数 char *WIN32规范化斜杠(char *path)
{
    char *p;
    for (p = path; *p; ++p)
        if (*p == '\\')
            *p = '/';
    return path;
}

static HMODULE 应用程序载入的_模块;/*HMODULE：表示模块句柄。是代表应用程序载入的模块，win32系统下通常是被载入模块的线性地址。*/

/* 在Win32上，我们假设lib和include位于'zhi.exe'的位置 */
static void WIN32设置库路径(知心状态机 *s)
{
    char path[1024], *p;
/*获取当前运行程序的绝对路径。如果“应用程序载入的_模块”该参数为NULL，函数会获取当前进程的运行文件(.exe文件)的全路径。
 * path:一个指向接收存储模块的全路径的缓冲区的指针。
 * sizeof path:path缓冲区的长度。*/
    GetModuleFileNameA(应用程序载入的_模块, path, sizeof path);
    p = 取_文件基本名(WIN32规范化斜杠(strlwr(path)));/*strlwr(path)将字符串转换为小写*/
    if (p > path)
        --p;
    *p = 0;
    设置库路径(s, path);
}

#ifdef ZHI_TARGET_PE
static void WIN32添加系统目录(知心状态机 *s)
{
    char buf[1000];
/*获取系统目录GetSystemDirectoryA(lpBuffer,uSize);lpBuffer,缓冲区用于存放取得的系统目录，uSize缓冲区长度*/
    GetSystemDirectory(buf, sizeof buf);
    添加库路径(s, WIN32规范化斜杠(buf));
}
#endif

#ifdef HEXINKU_AS_DLL
/*DllMain (hDll, dwReason,lpReserved)
 * hDll参数：指向DLL本身的实例句柄；
 * dwReason参数：指明了DLL被调用的原因
 * (
 * 1.DLL_PROCESS_ATTACH:当DLL被进程 <<第一次>> 调用时，导致DllMain函数被调用.
 * 2.DLL_PROCESS_DETACH：当DLL被从进程的地址空间解除映射时，系统调用了它的DllMain.
 * 3.DLL_THREAD_ATTACH：当进程创建一线程时，系统查看当前映射到进程地址空间中的所有DLL文件映像.
 * 4.DLL_THREAD_DETACH：如果线程调用了ExitThread来结束线程（线程函数返回时，系统也会自动调用ExitThread）.
 * )*/
BOOL WINAPI DllMain (HINSTANCE hDll, DWORD dwReason, LPVOID lpReserved)
{
    if (DLL_PROCESS_ATTACH == dwReason)
        应用程序载入的_模块 = hDll;
    return TRUE;
}
#endif
#endif
/*************************#ifdef _WIN32-结束*******************************/
#ifndef 从线程_使用_核心库
#define WAIT_SEM()
#define POST_SEM()
#elif 已定义 _WIN32
static int 线程_临界区_初始化;
static CRITICAL_SECTION 线程_临界区;
/*如果某一时间点有线程在CriticalSection内的话,EnterCriticalSection()会让待进入CriticalSection区域内的其它线程处于等待状态.但是它会将待进入CriticalSection的线程切换到不占用CPU的状态,太棒了!
 *InitializeCriticalSection(&cs);//初始化临界区，cs就是“线程_临界区”也就是厕所。
 *EnterCriticalSection(&cs);//进入临界区
 *LeaveCriticalSection(&cs);//离开临界区
 *DeleteCriticalSection(&cs);//删除临界区
 *通俗解释就像上厕所：
 *门锁了，就等着，等到别人出来了，进去锁上，然后该干什么干什么，干完了，把门打开
 *门没锁，就进去，锁上，然后该干什么干什么，干完了，把门打开
*/
static void 待进入_信号量(void)
{
    if (!线程_临界区_初始化)
        InitializeCriticalSection(&线程_临界区), 线程_临界区_初始化 = 1;
    EnterCriticalSection(&线程_临界区);
}
#define WAIT_SEM() 待进入_信号量()
#define POST_SEM() LeaveCriticalSection(&线程_临界区);
#elif defined __APPLE__
/* 半兼容的MacOS没有非共享（本地处理）信号灯。 将调度框架用于轻型锁。  */
#include <dispatch/dispatch.h>
static int 线程_临界区_初始化;
static 调度_信号_t 临界区_信号量;
static void 待进入_信号量(void)
{
    if (!线程_临界区_初始化)
      临界区_信号量 = 调度_信号量_创建(1), 线程_临界区_初始化 = 1;
    调度_信号量_等待降低(临界区_信号量, DISPATCH_TIME_FOREVER);
}
#define WAIT_SEM() 待进入_信号量()
#define POST_SEM() 调度_信号量_提高(临界区_信号量)
#else
#include <semaphore.h>
static int 线程_临界区_初始化;
static sem_t 临界区_信号量;
static void 待进入_信号量(void)
{
    if (!线程_临界区_初始化)
        sem_init(&临界区_信号量, 0, 1), 线程_临界区_初始化 = 1;
    while (sem_wait (&临界区_信号量) < 0 && errno == EINTR);
}
#define WAIT_SEM() 待进入_信号量()
#define POST_SEM() sem_post(&临界区_信号量)
#endif

/********************************************************/
/* 复制一个字符串并截断它。 */
静态_函数 char *p字符串复制(char *buf, size_t buf_size, const char *s)
{
    char *q, *q_end;
    int c;

    if (buf_size > 0)
    {
        q = buf;
        q_end = buf + buf_size - 1;
        while (q < q_end)
        {
            c = *s++;
            if (c == '\0')
                break;
            *q++ = c;
        }
        *q = '\0';
    }
    return buf;
}

/* 字符串连接和截断。*/
静态_函数 char *连接_字符串(char *buf, size_t buf_size, const char *s)
{
    size_t len;
    len = strlen(buf);/*获取字符串的实际长度*/
    if (len < buf_size)
        p字符串复制(buf + len, buf_size - len, s);
    return buf;
}

静态_函数 char *复制_字符串(char *被覆盖输出, const char *被复制, size_t 字节数)
{
    memcpy(被覆盖输出, 被复制, 字节数);
    被覆盖输出[字节数] = '\0';
    return 被覆盖输出;
}
/*取_文件基本名包含扩展名*/
公共_函数 char *取_文件基本名(const char *name)
{
    char *p = strchr(name, 0);
    while (p > name && !IS_DIRSEP(p[-1]))
        --p;
    return p;
}

/* (如果没有扩展名，则返回指向字符串结尾的指针)*/
公共_函数 char *取_文件扩展名 (const char *name)
{
    char *b = 取_文件基本名(name);
    char *e = strrchr(b, '.');/*strrchr(b, c)查找字符c在字符串b中最后出现的位置，返回此位置之后的所有的字符串。*/
    return e ? e : strchr(b, 0);
}

/********************************************************/
/* 内存管理 */

#undef free
#undef malloc
#undef realloc

#ifndef 内存_调试

公共_函数 void 内存_释放(void *ptr)
{
    free(ptr);
}
/*基于malloc()函数优化。申请一块连续的指定大小的内存块区域以void*类型返回分配的内存区域地址*/
公共_函数 void *内存_申请(unsigned long 内存容量)
{
    void *ptr;
    ptr = malloc(内存容量);
    if (!ptr && 内存容量)
        错误("内存已满（重新分配）");
    return ptr;
}
/*初始化内存块：返回一块“size”（参数）大小的空内存区域*/
公共_函数 void *内存_初始化(unsigned long size)
{
    void *ptr;
    ptr = 内存_申请(size);
    /*memset(void *s, int ch, size_t n);作用是将某一块内存中的内容全部设置为指定的值，
        将s中当前位置后面的n个字节 用 ch 替换并返回 s */
    memset(ptr, 0, size);
    return ptr;
}
/*重新分配内存大小。ptr原内存大小，size为新内存大小（要返回的内存大小），*/
公共_函数 void *内存_重分配容量(void *ptr, unsigned long size)
{
    void *ptr1;
    ptr1 = realloc(ptr, size);
    if (!ptr1 && size)
        错误("内存已满（重新分配）");
    return ptr1;
}

公共_函数 char *字符串_宽度加1(const char *str)
{
    char *ptr;
    ptr = 内存_申请(strlen(str) + 1);/*strlen(str)求字符数组的长度*/
    strcpy(ptr, str);/*strcpy(ptr, str);把str复制给ptr*/
    return ptr;
}

#else

#define MEM_DEBUG_MAGIC1 0xFEEDDEB1
#define MEM_DEBUG_MAGIC2 0xFEEDDEB2
#define MEM_DEBUG_MAGIC3 0xFEEDDEB3
#define MEM_DEBUG_FILE_LEN 40
#define MEM_DEBUG_CHECK3(header) \
    ((内存_调试_头_t*)((char*)header + header->size))->magic3
#define MEM_USER_PTR(header) \
    ((char *)header + offsetof(内存_调试_头_t, magic3))
#define MEM_HEADER_PTR(ptr) \
    (内存_调试_头_t *)((char*)ptr - offsetof(内存_调试_头_t, magic3))

struct 内存_调试_header {
    unsigned magic1;
    unsigned size;
    struct 内存_调试_header *prev;
    struct 内存_调试_header *next;
    int line_num;
    char file_name[MEM_DEBUG_FILE_LEN + 1];
    unsigned magic2;
    ALIGNED(16) unsigned magic3;
};

typedef struct 内存_调试_header 内存_调试_头_t;

static 内存_调试_头_t *内存_调试_链;
static unsigned 内存_当前_大小;
static unsigned 内存_最大_大小;

static 内存_调试_头_t *malloc_check(void *ptr, const char *msg)
{
    内存_调试_头_t * header = MEM_HEADER_PTR(ptr);
    if (header->magic1 != MEM_DEBUG_MAGIC1 ||
        header->magic2 != MEM_DEBUG_MAGIC2 ||
        MEM_DEBUG_CHECK3(header) != MEM_DEBUG_MAGIC3 ||
        header->size == (unsigned)-1) {
        fprintf(stderr, "%s check failed\n", msg);
        if (header->magic1 == MEM_DEBUG_MAGIC1)
            fprintf(stderr, "%s:%u: 这里分配的块.\n",
                header->file_name, header->line_num);
        exit(1);
    }
    return header;
}

公共_函数 void *zhi_分配_调试(unsigned long size, const char *file, int line)
{
    int ofs;
    内存_调试_头_t *header;

    header = malloc(sizeof(内存_调试_头_t) + size);
    if (!header)
        错误("内存已满（重新分配）");

    header->magic1 = MEM_DEBUG_MAGIC1;
    header->magic2 = MEM_DEBUG_MAGIC2;
    header->size = size;
    MEM_DEBUG_CHECK3(header) = MEM_DEBUG_MAGIC3;
    header->line_num = line;
    ofs = strlen(file) - MEM_DEBUG_FILE_LEN;
    strncpy(header->file_name, file + (ofs > 0 ? ofs : 0), MEM_DEBUG_FILE_LEN);
    header->file_name[MEM_DEBUG_FILE_LEN] = 0;

    header->next = 内存_调试_链;
    header->prev = NULL;
    if (header->next)
        header->next->prev = header;
    内存_调试_链 = header;

    内存_当前_大小 += size;
    if (内存_当前_大小 > 内存_最大_大小)
        内存_最大_大小 = 内存_当前_大小;

    return MEM_USER_PTR(header);
}

公共_函数 void zhi_释放_调试(void *ptr)
{
    内存_调试_头_t *header;
    if (!ptr)
        return;
    header = malloc_check(ptr, "内存_释放");
    内存_当前_大小 -= header->size;
    header->size = (unsigned)-1;
    if (header->next)
        header->next->prev = header->prev;
    if (header->prev)
        header->prev->next = header->next;
    if (header == 内存_调试_链)
        内存_调试_链 = header->next;
    free(header);
}

公共_函数 void *zhi_分配z_调试(unsigned long size, const char *file, int line)
{
    void *ptr;
    ptr = zhi_分配_调试(size,file,line);
    memset(ptr, 0, size);
    return ptr;
}

公共_函数 void *zhi_重新分配_调试(void *ptr, unsigned long size, const char *file, int line)
{
    内存_调试_头_t *header;
    int mem_debug_chain_update = 0;
    if (!ptr)
        return zhi_分配_调试(size, file, line);
    header = malloc_check(ptr, "内存_重分配容量");
    内存_当前_大小 -= header->size;
    mem_debug_chain_update = (header == 内存_调试_链);
    header = realloc(header, sizeof(内存_调试_头_t) + size);
    if (!header)
        错误("内存已满（重新分配）");
    header->size = size;
    MEM_DEBUG_CHECK3(header) = MEM_DEBUG_MAGIC3;
    if (header->next)
        header->next->prev = header;
    if (header->prev)
        header->prev->next = header;
    if (mem_debug_chain_update)
        内存_调试_链 = header;
    内存_当前_大小 += size;
    if (内存_当前_大小 > 内存_最大_大小)
        内存_最大_大小 = 内存_当前_大小;
    return MEM_USER_PTR(header);
}

公共_函数 char *zhi_字符串dup_调试(const char *str, const char *file, int line)
{
    char *ptr;
    ptr = zhi_分配_调试(strlen(str) + 1, file, line);
    strcpy(ptr, str);
    return ptr;
}

公共_函数 void zhi_内存检查(void)
{
    if (内存_当前_大小) {
        内存_调试_头_t *header = 内存_调试_链;
        fprintf(stderr, "内存_调试: 内存_泄漏= %d 个字节, 内存_最大_大小= %d 个字节\n",内存_当前_大小, 内存_最大_大小);
        while (header) {fprintf(stderr, "%s:%u: 错误: %u 个字节泄漏\n",header->file_name, header->line_num, header->size);
            header = header->next;
        }
#if 内存_调试-0 == 2
        exit(2);
#endif
    }
}
#endif /* 内存_调试 */

#define free(p) 用_zhi_释放(p)
#define malloc(s) 用_zhi_内存分配(s)
#define realloc(p, s) 用_zhi_重新分配(p, s)

/********************************************************/
/* 动态字符串 */
静态_函数 void 动态数组_追加元素(void *ptab, int *数量_ptr, void *data)
{
    int 数组长度, 数量_alloc;
    void **数组;

    数组长度 = *数量_ptr;
    数组 = *(void ***)ptab;
    /* 每乘以2的幂就使数组大小加倍 */
    if ((数组长度 & (数组长度 - 1)) == 0)
    {
        if (!数组长度)
            数量_alloc = 1;
        else
            数量_alloc = 数组长度 * 2;
        数组 = 内存_重分配容量(数组, 数量_alloc * sizeof(void *));
        *(void***)ptab = 数组;
    }
    数组[数组长度++] = data;
    *数量_ptr = 数组长度;
}

静态_函数 void 动态数组_重分配容量(void *pp, int *n)
{
    void **p;
    for (p = *(void***)pp; *n; ++p, --*n)
        if (*p)
        	内存_释放(*p);
    内存_释放(*(void**)pp);
    *(void**)pp = NULL;
}
/*拆分路径*/
static void 拆分路径(知心状态机 *s, void *p_ary, int *p_nb_ary, const char *in)
{
    const char *p;
    do {
        int c;
        动态字符串 str;

        动态字符串_初始化(&str);
        for (p = in; c = *p, c != '\0' && c != PATHSEP[0]; ++p) {
            if (c == '{' && p[1] && p[2] == '}') {
                c = p[1], p += 2;
                if (c == 'B')
                    动态字符串_cat(&str, s->zhi_库_路径, -1);
                if (c == 'f' && file) {
                    /* 替换当前文件的目录 */
                    const char *f = file->true_filename;
                    const char *b = 取_文件基本名(f);
                    if (b > f)
                        动态字符串_cat(&str, f, b - f - 1);
                    else
                        动态字符串_cat(&str, ".", 1);
                }
            } else {
                动态字符串_追加单个字符(&str, c);
            }
        }
        if (str.字符串长度) {
            动态字符串_追加单个字符(&str, '\0');
            动态数组_追加元素(p_ary, p_nb_ary, 字符串_宽度加1(str.指向字符串的指针));
        }
        动态字符串_释放(&str);
        in = p+1;
    } while (*p);
}

/********************************************************/

static void 可变参数列表格式化后输出到字符数组(char *buf, int buf_size, const char *fmt, va_list ap)
{
    int len;
    len = strlen(buf);
    vsnprintf(buf + len, buf_size - len, fmt, ap);
}

static void strcat_printf(char *buf, int buf_size, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    可变参数列表格式化后输出到字符数组(buf, buf_size, fmt, ap);
    va_end(ap);
}

#define ERROR_WARN 0
#define ERROR_NOABORT 1
#define ERROR_ERROR 2

公共_函数 void 进入状态机(知心状态机 *状态机1)
{
    WAIT_SEM();
    zhi_状态 = 状态机1;
}

公共_函数 void 离开状态机(void)
{
    zhi_状态 = NULL;
    POST_SEM();
}

static void 错误1(int mode, const char *fmt, va_list ap)
{
    char buf[2048];
    缓冲文件 **pf, *f;
    知心状态机 *状态机1 = zhi_状态;

    buf[0] = '\0';
    if (状态机1 == NULL)
        /* 仅当从zhi_malloc（）调用时才可能发生：'内存不足' */
        goto no_file;

    if (状态机1 && !状态机1->错误_设置_jmp_启用)
        /* zhi_state刚刚由zhi_enter_state（）设置 */
        离开状态机();

    if (mode == ERROR_WARN) {
        if (状态机1->警告_none)
            return;
        if (状态机1->警告_错误)
            mode = ERROR_ERROR;
    }

    f = NULL;
    if (状态机1->错误_设置_jmp_启用) { /* we're called while parsing a file */
        /* use upper file if inline ":asm:" or 标识符 ":paste:" */
        for (f = file; f && f->文件名[0] == ':'; f = f->prev)
            ;
    }
    if (f) {
        for(pf = 状态机1->包含_堆; pf < 状态机1->包含_堆_ptr; pf++)
            strcat_printf(buf, sizeof(buf), "In file included from %s:%d:\n",
                (*pf)->文件名, (*pf)->line_num);
        strcat_printf(buf, sizeof(buf), "%s:%d: ",
            f->文件名, f->line_num - !!(标识符_标记 & 符_标记_行开始前));
    } else if (状态机1->当前_文件名) {
        strcat_printf(buf, sizeof(buf), "%s: ", 状态机1->当前_文件名);
    }

no_file:
    if (0 == buf[0])
        strcat_printf(buf, sizeof(buf), "zhi: ");
    if (mode == ERROR_WARN)
        strcat_printf(buf, sizeof(buf), "警告: ");
    else
        strcat_printf(buf, sizeof(buf), "错误: ");
    可变参数列表格式化后输出到字符数组(buf, sizeof(buf), fmt, ap);
    if (!状态机1 || !状态机1->错误_函数) {
        /* default case: stderr */
        if (状态机1 && 状态机1->输出_类型 == ZHI_输出_预处理 && 状态机1->预处理输出文件 == stdout)
            /* 在zhi -E期间打印换行符 */
            printf("\n"), fflush(stdout);
        fflush(stdout); /* flush -v output */
        fprintf(stderr, "%s\n", buf);
        fflush(stderr); /* print error/warning now (win32) */
    } else {
        状态机1->错误_函数(状态机1->错误_不透明, buf);
    }
    if (状态机1) {
        if (mode != ERROR_WARN)
            状态机1->数量_错误++;
        if (mode != ERROR_ERROR)
            return;
        if (状态机1->错误_设置_jmp_启用)
            longjmp(状态机1->错误_jmp_缓存, 1);
/*longjmp(envbuf,val);
longjmp函数中的参数envbuf是由setjmp函数所保存的堆栈环境，参数val设置setjmp函数的返回值。longjmp函数本身是没有返回值的，
它执行后跳转到保存envbuf参数的setjmp函数调用，并由setjmp函数调用返回，此时setjmp函数的返回值就是val。
setjmp和longjmp是C语言独有的，只有将它们结合起来使用，才能达到程序控制流有效转移的目的，按照程序员的预先设计的意图，
去实现对程序中可能出现的异常进行集中处理。
            */
    }
    exit(1);
}

HEXINKU接口 void 设置错误警告显示回调(知心状态机 *s, void *错误_不透明, ZHIErrorFunc 错误_函数)
{
    s->错误_不透明 = 错误_不透明;
    s->错误_函数 = 错误_函数;
}

HEXINKU接口 ZHIErrorFunc 返回错误警告回调(知心状态机 *s)
{
    return s->错误_函数;
}

HEXINKU接口 void *返回错误警告回调不透明指针(知心状态机 *s)
{
    return s->错误_不透明;
}

/* 错误而没有中止当前编译 */
公共_函数 void 错误提示(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    错误1(ERROR_NOABORT, fmt, ap);
    va_end(ap);
}

公共_函数 void 错误(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    for (;;) 错误1(ERROR_ERROR, fmt, ap);
}

公共_函数 void _zhi_警告(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    错误1(ERROR_WARN, fmt, ap);
    va_end(ap);
}

/********************************************************/
/* I/O层 */

静态_函数 void 打开缓存文件(知心状态机 *状态机1, const char *文件名, int initlen)
{
    缓冲文件 *bf;
    int buflen = initlen ? initlen : 输入输出_缓冲_大小;

    bf = 内存_初始化(sizeof(缓冲文件) + buflen);
    bf->buf_ptr = bf->buffer;
    bf->buf_end = bf->buffer + initlen;
    bf->buf_end[0] = CH_缓冲区结尾; /* put eob symbol */
    p字符串复制(bf->文件名, sizeof(bf->文件名), 文件名);
#ifdef _WIN32
    WIN32规范化斜杠(bf->文件名);
#endif
    bf->true_filename = bf->文件名;
    bf->line_num = 1;
    bf->如果已宏定义_堆_ptr = 状态机1->如果已宏定义_堆_ptr;
    bf->fd = -1;
    bf->prev = file;
    file = bf;
    标识符_标记 = 符_标记_行开始前 | 符_标记_文件开始前;
}

静态_函数 void 关闭文件(void)
{
    知心状态机 *状态机1 = zhi_状态;
    缓冲文件 *bf = file;
    if (bf->fd > 0) {
        close(bf->fd);
        总_行数 += bf->line_num;
    }
    if (bf->true_filename != bf->文件名)
        内存_释放(bf->true_filename);
    file = bf->prev;
    内存_释放(bf);
}

static int 打开文件(知心状态机 *状态机1, const char *文件名)
{
    int fd;
    if (strcmp(文件名, "-") == 0)
        fd = 0, 文件名 = "<stdin>";
    else
        fd = open(文件名, O_RDONLY | O_BINARY);
    if ((状态机1->显示信息 == 2 && fd >= 0) || 状态机1->显示信息 == 3)
        printf("%s %*s%s\n", fd < 0 ? "nf":"->",
               (int)(状态机1->包含_堆_ptr - 状态机1->包含_堆), "", 文件名);
    return fd;
}

静态_函数 int 打开一个新文件(知心状态机 *状态机1, const char *文件名)/*打开一个新文件()打开一个新文件并zhi_close()关闭它。*/
{
    int fd = 打开文件(状态机1, 文件名);
    if (fd < 0)
        return -1;
    打开缓存文件(状态机1, 文件名, 0);
    file->fd = fd;
    return 0;
}

/* 编译在“文件”中打开的文件。 错误返回非零. */
static int 编译_开始(知心状态机 *状态机1, int 文件类型, const char *文件名称, int 只读文件)
{
    /* 从这里进入代码部分，开始使用全局变量进行解析和代码生成（词法分析.c，语法分析.c，<target> -gen.c），其他线程需要等待我们完成。
       或者，我们可以对这些全局变量使用线程本地存储，这可能有优势也可能没有优势 */
    进入状态机(状态机1);
    if (setjmp(状态机1->错误_jmp_缓存) == 0)
    {
/*int setjmp(jmp_buf envbuf);
setjmp函数用缓冲区envbuf保存系统堆栈的内容，以便后续的longjmp函数使用。setjmp函数初次启用时返回0值。
    	 * */
        int 要_汇编;
        状态机1->错误_设置_jmp_启用 = 1;
        状态机1->数量_错误 = 0;

        if (只读文件 == -1)
        {
            int len = strlen(文件名称);
            打开缓存文件(状态机1, "<string>", len);
            memcpy(file->buffer, 文件名称, len);
        } else {
            打开缓存文件(状态机1, 文件名称, 0);
            file->fd = 只读文件;
        }
/*两个!是为了把非0值转换成1,而0值还是0。
        因为C语言中，所有非0值都表示真。
        所以!非0值 = 0，而!0 = 1。
        所以!!非0值 = 1，而!!0 = 0。
        例如：i=123     !i=0     !!i=1
        最后将123转换为1。
        就是其他数据类型向bool值的转换*/
        要_汇编 = !!(文件类型 & (文件格式_类型_汇编|文件格式_类型_ASMPP));
        保存_段_数据状态(状态机1);
        开始_预处理(状态机1, 要_汇编);
        zhi语法_初始化(状态机1);
        if (状态机1->输出_类型 == ZHI_输出_预处理)
        {
            预处理_源文件(状态机1);
        } else if (要_汇编)
        {
#ifdef 配置_ZHI_汇编
            zhi_汇编(状态机1, !!(文件类型 & 文件格式_类型_ASMPP));
#else
            zhi_错误_不中止("不支持汇编");
#endif
        } else {
            zhi语法_编译(状态机1);
        }
    }
    状态机1->错误_设置_jmp_启用 = 0;
    zhi语法_完成(状态机1);
    结束_预处理(状态机1);
    离开状态机();

    结束_段_数据状态(状态机1);
    return 状态机1->数量_错误 != 0 ? -1 : 0;
}

HEXINKU接口 int 编译包含ZHI源代码的字符串(知心状态机 *s, const char *str)
{
    return 编译_开始(s, s->文件类型, str, -1);
}

/*定义预处理器符号。 还可以通过'='运算符提供一个值 */
HEXINKU接口 void 定义预处理程序符号(知心状态机 *状态机1, const char *sym, const char *value)
{
    if (!value)
        value = "1";
    动态字符串_打印(&状态机1->命令行_定义, "#define %s %s\n", sym, value);
}

/* 取消定义预处理器符号 */
HEXINKU接口 void 未定义预处理符号(知心状态机 *状态机1, const char *sym)
{
    动态字符串_打印(&状态机1->命令行_定义, "#undef %s\n", sym);
}
HEXINKU接口 知心状态机 *初始化状态机(void)
{
    知心状态机 *s;

    s = 内存_初始化(sizeof(知心状态机));
    if (!s)
        return NULL;
#ifdef 内存_调试
    ++数量_states;/*自动变量：值不确定；静态变量：则为0；全局变量：则为0。*/
#endif

#undef gnu_扩展

    s->gnu_扩展 = 1;
    s->zhi_扩展 = 1;
    s->不使用通用符号 = 1;
    s->允许_在标识符中使用美元符号 = 1; /*默认打开，如在gcc / clang中*/
    s->ZHI版本 = 199901; /* 除非提供-std = c11，否则默认 */
    s->警告_隐式函数声明 = 1;
    s->允许_匿名联合和结构 = 1;

#ifdef 字符串_是_无符号
    s->字符串_无符号 = 1;
#endif
#ifdef ZHI_TARGET_I386
    s->段_大小 = 32;
#endif
    /* 如果要在Windows上使用带下划线的符号，请启用此功能： */
#if defined ZHI_TARGET_MACHO /* || 定义ZHI_TARGET_PE的下划线 */
    s->前导_下划线 = 1;
#endif
    s->预处理输出文件 = stdout;
    /* 可能在preprocess_start（）之前的error（）中使用 */
    s->包含_堆_ptr = s->包含_堆;

    zhi_elf_新建(s);

#ifdef _WIN32
    WIN32设置库路径(s);
#else
    设置库路径(s, 配置_ZHI目录);
#endif

    {
        char buffer[32]; int a,b,c;
        sscanf(ZHI_VERSION, "%d.%d.%d", &a, &b, &c);
        sprintf(buffer, "%d", a*10000 + b*100 + c);
        定义预处理程序符号(s, "__TINYC__", buffer);
    }

    /* 标准定义 */
    定义预处理程序符号(s, "__STDC__", NULL);
    定义预处理程序符号(s, "__STDC_VERSION__", "199901L");
    定义预处理程序符号(s, "__STDC_HOSTED__", NULL);

    /* 目标定义 */
#if defined(ZHI_TARGET_I386)
    定义预处理程序符号(s, "__i386__", NULL);
    定义预处理程序符号(s, "__i386", NULL);
    定义预处理程序符号(s, "i386", NULL);
#elif defined(ZHI_TARGET_X86_64)
    定义预处理程序符号(s, "__x86_64__", NULL);
#elif defined(ZHI_TARGET_ARM)
    定义预处理程序符号(s, "__ARM_ARCH_4__", NULL);
    定义预处理程序符号(s, "__arm_elf__", NULL);
    定义预处理程序符号(s, "__arm_elf", NULL);
    定义预处理程序符号(s, "arm_elf", NULL);
    定义预处理程序符号(s, "__arm__", NULL);
    定义预处理程序符号(s, "__arm", NULL);
    定义预处理程序符号(s, "arm", NULL);
    定义预处理程序符号(s, "__APCS_32__", NULL);
    定义预处理程序符号(s, "__ARMEL__", NULL);
#if defined(ZHI_ARM_EABI)
    定义预处理程序符号(s, "__ARM_EABI__", NULL);
#endif
#if defined(ZHI_ARM_HARDFLOAT)
    s->浮动_abi = ARM_HARD_FLOAT;
    定义预处理程序符号(s, "__ARM_PCS_VFP", NULL);
#else
    s->浮动_abi = ARM_SOFTFP_FLOAT;
#endif
#elif defined(ZHI_TARGET_ARM64)
    定义预处理程序符号(s, "__aarch64__", NULL);
#elif defined ZHI_TARGET_C67
    定义预处理程序符号(s, "__C67__", NULL);
#elif defined ZHI_TARGET_RISCV64
    定义预处理程序符号(s, "__riscv", NULL);
    定义预处理程序符号(s, "__riscv_xlen", "64");
    定义预处理程序符号(s, "__riscv_flen", "64");
    定义预处理程序符号(s, "__riscv_div", NULL);
    定义预处理程序符号(s, "__riscv_mul", NULL);
    定义预处理程序符号(s, "__riscv_fdiv", NULL);
    定义预处理程序符号(s, "__riscv_fsqrt", NULL);
    定义预处理程序符号(s, "__riscv_float_abi_double", NULL);
#endif

#ifdef ZHI_TARGET_PE
    定义预处理程序符号(s, "_WIN32", NULL);
    定义预处理程序符号(s, "__declspec(x)", "__attribute__((x))");
    定义预处理程序符号(s, "__cdecl", "");
# ifdef ZHI_TARGET_X86_64
    定义预处理程序符号(s, "_WIN64", NULL);
# endif
#else
    定义预处理程序符号(s, "__unix__", NULL);
    定义预处理程序符号(s, "__unix", NULL);
    定义预处理程序符号(s, "unix", NULL);
# if defined(__linux__)
    定义预处理程序符号(s, "__linux__", NULL);
    定义预处理程序符号(s, "__linux", NULL);
# endif
# if defined(__FreeBSD__)
    定义预处理程序符号(s, "__FreeBSD__", "__FreeBSD__");
    /* 带有zhi的FreeBSD上没有“本地线程存储”*/
    定义预处理程序符号(s, "__NO_TLS", NULL);
# endif
# if defined(__FreeBSD_kernel__)
    定义预处理程序符号(s, "__FreeBSD_kernel__", NULL);
# endif
# if defined(__NetBSD__)
    定义预处理程序符号(s, "__NetBSD__", "__NetBSD__");
# endif
# if defined(__OpenBSD__)
    定义预处理程序符号(s, "__OpenBSD__", "__OpenBSD__");
# endif
#endif

    /* zhi＆gcc定义 */
#if 指针_大小 == 4
    /* 32位系统. */
    定义预处理程序符号(s, "__SIZE_TYPE__", "unsigned int");
    定义预处理程序符号(s, "__PTRDIFF_TYPE__", "int");
    定义预处理程序符号(s, "__ILP32__", NULL);
#elif LONG_SIZE == 4
    /* 64位 Windows. */
    定义预处理程序符号(s, "__SIZE_TYPE__", "unsigned long long");
    定义预处理程序符号(s, "__PTRDIFF_TYPE__", "long long");
    定义预处理程序符号(s, "__LLP64__", NULL);
#else
    /* 其它64位系统. */
    定义预处理程序符号(s, "__SIZE_TYPE__", "unsigned long");
    定义预处理程序符号(s, "__PTRDIFF_TYPE__", "long");
    定义预处理程序符号(s, "__LP64__", NULL);
#endif
    定义预处理程序符号(s, "__SIZEOF_POINTER__", 指针_大小 == 4 ? "4" : "8");

#ifdef ZHI_TARGET_PE
    定义预处理程序符号(s, "__WCHAR_TYPE__", "unsigned short");
    定义预处理程序符号(s, "__WINT_TYPE__", "unsigned short");
#else
    定义预处理程序符号(s, "__WCHAR_TYPE__", "int");
    /* 默认情况下，wint_t是unsigned int，但在BSD上是（signed）int，在Windows上是unsigned short。 叹息，其他操作系统可能还有其他约定。  */
# if defined(__FreeBSD__) || defined (__FreeBSD_kernel__) \
  || defined(__NetBSD__) || defined(__OpenBSD__)
    定义预处理程序符号(s, "__WINT_TYPE__", "int");
#  ifdef __FreeBSD__
    /* 定义__GNUC__以从sys / cdefs.h中获取一些有用的东西，这些东西在FreeBSDs其他系统头文件中无条件地使用 :/ */
    定义预处理程序符号(s, "__GNUC__", "2");
    定义预处理程序符号(s, "__GNUC_MINOR__", "7");
    定义预处理程序符号(s, "__builtin_alloca", "alloca");
#  endif
# else
    定义预处理程序符号(s, "__WINT_TYPE__", "unsigned int");
    /* glibc定义 */
    定义预处理程序符号(s, "__REDIRECT(name, proto, alias)",
        "name proto __asm__ (#alias)");
    定义预处理程序符号(s, "__REDIRECT_NTH(name, proto, alias)",
        "name proto __asm__ (#alias) __THROW");
# endif
    /* 一些易于表达为宏的GCC内置函数.  */
    定义预处理程序符号(s, "__builtin_extract_return_addr(x)", "x");
#endif /* ndef ZHI_TARGET_PE */
#ifdef ZHI_TARGET_MACHO
    /* 模拟APPLE-GCC以编译libc头文件: */
    定义预处理程序符号(s, "__APPLE__", "1");
    定义预处理程序符号(s, "__GNUC__", "4");   /* darwin emits warning on GCC<4 */
    定义预处理程序符号(s, "__APPLE_CC__", "1"); /* for <TargetConditionals.h> */
    定义预处理程序符号(s, "__builtin_alloca", "alloca"); /* as we claim GNUC */

    /* 避免在libc-header文件中使用GCC / clang特定的内置函数: */
    定义预处理程序符号(s, "__FINITE_MATH_ONLY__", "1");
    定义预处理程序符号(s, "_FORTIFY_SOURCE", "0");
#endif /* ndef ZHI_TARGET_MACHO */
    return s;
}

HEXINKU接口 void 释放状态机(知心状态机 *状态机1)
{
    /* free 段数 */
    zhielf_删除(状态机1);

    /* free library paths */
    动态数组_重分配容量(&状态机1->库_路径, &状态机1->数量_库_路径);
    动态数组_重分配容量(&状态机1->crt_路径, &状态机1->数量_crt_路径);

    /* free include paths */
    动态数组_重分配容量(&状态机1->包含_路径, &状态机1->数量_包含_路径);
    动态数组_重分配容量(&状态机1->系统包含_路径, &状态机1->数量_系统包含_路径);

    内存_释放(状态机1->zhi_库_路径);
    内存_释放(状态机1->基本名称);
    内存_释放(状态机1->动态库路径);
    内存_释放(状态机1->加载_符号);
    内存_释放(状态机1->卸载_符号);
    内存_释放(状态机1->输出文件);
    内存_释放(状态机1->依赖_输出文件);
    动态数组_重分配容量(&状态机1->文件数, &状态机1->数量_文件数);
    动态数组_重分配容量(&状态机1->目标_依赖, &状态机1->数量_目标_依赖);
    动态数组_重分配容量(&状态机1->语法_库数, &状态机1->数量_语法_库数);
    动态数组_重分配容量(&状态机1->参数数组, &状态机1->参数数量);

    动态字符串_释放(&状态机1->命令行_定义);
    动态字符串_释放(&状态机1->命令行_包含);
#ifdef ZHI_是_本机
    /* free runtime memory */
    释放运行时内存(状态机1);
#endif

    内存_释放(状态机1);
#ifdef 内存_调试
    if (0 == --数量_states)
        zhi_内存检查();
#endif
}

HEXINKU接口 int 设置输出类型(知心状态机 *s, int 输出_类型)
{
    s->输出_类型 = 输出_类型;

    /* 总是对象的可执行文件 */
    if (输出_类型 == ZHI_输出_目标文件)
        s->输出_格式 = ZHI_输出_格式_ELF;

    if (s->字符串_无符号)
        定义预处理程序符号(s, "__CHAR_UNSIGNED__", NULL);

    if (s->ZHI版本 == 201112)
    {
        未定义预处理符号(s, "__STDC_VERSION__");
        定义预处理程序符号(s, "__STDC_VERSION__", "201112L");
        定义预处理程序符号(s, "__STDC_NO_ATOMICS__", NULL);
        定义预处理程序符号(s, "__STDC_NO_COMPLEX__", NULL);
        定义预处理程序符号(s, "__STDC_NO_THREADS__", NULL);
#ifndef ZHI_TARGET_PE
        /* 在Linux上，这与glibc libs包含的/usr/include/stdc-predef.h引入的定义冲突
        定义预处理程序符号(s, "__STDC_ISO_10646__", "201605L"); */
        定义预处理程序符号(s, "__STDC_UTF_16__", NULL);
        定义预处理程序符号(s, "__STDC_UTF_32__", NULL);
#endif
    }

    if (s->编译优化 > 0)
        定义预处理程序符号(s, "__OPTIMIZE__", NULL);

    if (s->选项_线程)
        定义预处理程序符号(s, "_REENTRANT", NULL);

    if (!s->不添加标准头)
    {
        /* 默认包含路径 */
        /* -isystem路径已被处理*/
        添加到系统包含路径(s, 配置_ZHI_系统包含路径);
    }

#ifdef 配置_ZHI_边界检查
    if (s->执行_边界_检查器)
    {
        /* 如果进行边界检查，则添加相应的部分 */
        zhielf_bounds_new(s);
        /* 定义符号 */
        定义预处理程序符号(s, "__BOUNDS_CHECKING_ON", NULL);
    }
#endif
    if (s->执行_调试)
    {
        /* 添加调试部分 */
        zhielf_stab_新建(s);
    }

    添加库路径(s, 配置_ZHI_库搜索路径);

#ifdef ZHI_TARGET_PE
# ifdef _WIN32
    if (!s->不添加标准库 && 输出_类型 != ZHI_输出_目标文件)
        WIN32添加系统目录(s);
# endif
#else
    /* crt对象的路径 */
    拆分路径(s, &s->crt_路径, &s->数量_crt_路径, 配置_ZHI_CRT前缀);
    /* 添加libc crt1 / crti对象 */
    if ((输出_类型 == ZHI_输出_EXE || 输出_类型 == ZHI_输出_DLL) &&
        !s->不添加标准库)
    {
#ifndef ZHI_TARGET_MACHO
        /* 具有LC_MAIN的Mach-O不需要任何crt启动代码.  */
        if (输出_类型 != ZHI_输出_DLL)
            zhi_添加_crt(s, "crt1.o");
        zhi_添加_crt(s, "crti.o");
#endif
    }
#endif
    return 0;
}

HEXINKU接口 int 添加包含路径(知心状态机 *s, const char *pathname)
{
    拆分路径(s, &s->包含_路径, &s->数量_包含_路径, pathname);
    return 0;
}

HEXINKU接口 int 添加到系统包含路径(知心状态机 *s, const char *pathname)
{
    拆分路径(s, &s->系统包含_路径, &s->数量_系统包含_路径, pathname);
    return 0;
}

静态_函数 int 添加内部文件(知心状态机 *状态机1, const char *文件名, int 文件类型位或错误打印)
{
    int 只读文件, 返回值;
    只读文件 = 打开文件(状态机1, 文件名);
    if (只读文件 < 0)
    {
        if (文件类型位或错误打印 & AFF_打印_错误)
            zhi_错误_不中止("没有找到文件 '%s' ", 文件名);
        return -1;
    }
    状态机1->当前_文件名 = 文件名;
    if (文件类型位或错误打印 & 文件格式_类型_BIN)
    {
        ElfW(ELF文件头) elf头文件;
        int 目标文件_类型;

        目标文件_类型 = zhi_目标文件_类型(只读文件, &elf头文件);
        lseek(只读文件, 0, SEEK_SET);

#ifdef ZHI_TARGET_MACHO
        if (0 == 目标文件_类型 && 0 == strcmp(取_文件扩展名(文件名), ".dylib"))
        	目标文件_类型 = AFF_二进制_DYN;
#endif

        switch (目标文件_类型)
        {
        case AFF_二进制_REL:
            返回值 = zhi_加载_对象_文件(状态机1, 只读文件, 0);
            break;
#ifndef ZHI_TARGET_PE
        case AFF_二进制_DYN:
            if (状态机1->输出_类型 == ZHI_输出_内存中运行)
            {
                返回值 = 0;
#ifdef ZHI_是_本机
                if (NULL == dl打开(文件名, RTLD_全局 | RTLD_依赖))
                    返回值 = -1;
#endif
            } else
            {
#ifndef ZHI_TARGET_MACHO
                返回值 = zhi_加载_dll(状态机1, fd, 文件名,(文件类型位或错误打印 & AFF_加载_引用的DLL) != 0);
#else
                返回值 = macho_加载_dll(状态机1, fd, 文件名,(文件类型位或错误打印 & AFF_加载_引用的DLL) != 0);
#endif
            }
            break;
#endif
        case AFF_二进制_AR:
            返回值 = zhi_加载_档案(状态机1, 只读文件, !(文件类型位或错误打印 & AFF_从存档加载_所有对象));
            break;
#ifdef ZHI_TARGET_COFF
        case AFF_二进制_C67:
            返回值 = zhi_加载_coff(状态机1, fd);
            break;
#endif
        default:
#ifdef ZHI_TARGET_PE
            返回值 = pe_加载_文件(状态机1, 文件名, 只读文件);
#elif defined(ZHI_TARGET_MACHO)
            返回值 = -1;
#else
            /* 作为GNU ld，如果无法识别，则认为它是ld脚本 */
            返回值 = zhi_加载_链接脚本(状态机1, fd);
#endif
            if (返回值 < 0)
                zhi_错误_不中止("%s: 无法识别的文件类型 %d", 文件名,目标文件_类型);
            break;
        }
        close(只读文件);
    } else {
        /* 更新目标部门 */
        动态数组_追加元素(&状态机1->目标_依赖, &状态机1->数量_目标_依赖, 字符串_宽度加1(文件名));
        返回值 = 编译_开始(状态机1, 文件类型位或错误打印, 文件名, 只读文件);//开始编译文件，编译文件入口
    }
    状态机1->当前_文件名 = NULL;
    return 返回值;
}

HEXINKU接口 int 添加文件(知心状态机 *s, const char *文件名)
{
    int 文件类型 = s->文件类型;
    if (0 == (文件类型 & 文件格式_类型_掩码))
    {
        const char *扩展名 = 取_文件扩展名(文件名);
        if (扩展名[0])
        {
        	扩展名++;
            if (!strcmp(扩展名, "S"))
                文件类型 = 文件格式_类型_ASMPP;
            else if (!strcmp(扩展名, "s"))
                文件类型 = 文件格式_类型_汇编;
            else if (!PATHCMP(扩展名, "z") || !PATHCMP(扩展名, "i")|| !PATHCMP(扩展名, "c"))/*设置源文件后缀名（扩展名）*/
                文件类型 = 文件格式_类型_Z;
            else
                文件类型 |= 文件格式_类型_BIN;
        } else
        {
            文件类型 = 文件格式_类型_Z;
        }
    }
    return 添加内部文件(s, 文件名, 文件类型 | AFF_打印_错误);
}

HEXINKU接口 int 添加库路径(知心状态机 *s, const char *pathname)
{
    拆分路径(s, &s->库_路径, &s->数量_库_路径, pathname);
    return 0;
}

static int 添加内部库(知心状态机 *s, const char *fmt,const char *文件名, int flags, char **paths, int 数量_paths)
{
    char buf[1024];
    int i;

    for(i = 0; i < 数量_paths; i++) {
        snprintf(buf, sizeof(buf), fmt, paths[i], 文件名);
        if (添加内部文件(s, buf, flags | 文件格式_类型_BIN) == 0)
            return 0;
    }
    return -1;
}

/* find and 加载 a dll. Return non zero if not found */
/* XXX: add '-动态库路径' option support ? */
静态_函数 int 添加dll文件(知心状态机 *s, const char *文件名, int flags)
{
    return 添加内部库(s, "%s/%s", 文件名, flags,
        s->库_路径, s->数量_库_路径);
}

#ifndef ZHI_TARGET_PE
静态_函数 int zhi_添加_crt(知心状态机 *状态机1, const char *文件名)
{
    if (-1 == 添加内部库(状态机1, "%s/%s",文件名, 0, 状态机1->crt_路径, 状态机1->数量_crt_路径))
        zhi_错误_不中止("没有找到文件 '%s' ", 文件名);
    return 0;
}
#endif

/* the library name is the same as the argument of the '-l' option */
HEXINKU接口 int 使用库名称添加库(知心状态机 *s, const char *libraryname)
{
#if defined ZHI_TARGET_PE
    const char *libs[] = { "%s/%s.def", "%s/lib%s.def", "%s/%s.dll", "%s/lib%s.dll", "%s/lib%s.a", NULL };
    const char **pp = s->执行_静态链接 ? libs + 4 : libs;
#elif defined ZHI_TARGET_MACHO
    const char *libs[] = { "%s/lib%s.dylib", "%s/lib%s.a", NULL };
    const char **pp = s->执行_静态链接 ? libs + 1 : libs;
#else
    const char *libs[] = { "%s/lib%s.so", "%s/lib%s.a", NULL };
    const char **pp = s->执行_静态链接 ? libs + 1 : libs;
#endif
    int flags = s->文件类型 & AFF_从存档加载_所有对象;
    while (*pp) {
        if (0 == 添加内部库(s, *pp,
            libraryname, flags, s->库_路径, s->数量_库_路径))
            return 0;
        ++pp;
    }
    return -1;
}

公共_函数 int 添加库错误(知心状态机 *状态机1, const char *libname)
{
    int 返回值 = 使用库名称添加库(状态机1, libname);
    if (返回值 < 0)
        zhi_错误_不中止("没有找到库 '%s' ", libname);
    return 返回值;
}

/* handle #pragma comment(lib,) */
静态_函数 void 处理实用注释库(知心状态机 *状态机1)
{
    int i;
    for (i = 0; i < 状态机1->数量_语法_库数; i++)
        添加库错误(状态机1, 状态机1->语法_库数[i]);
}

HEXINKU接口 int 在已编译的程序中添加符号(知心状态机 *状态机1, const char *name, const void *val)
{
#ifdef ZHI_TARGET_PE
    /* 在x86_64上，可能无法通过32位偏移量访问``val''，因此此处将其视为DLL中的内容进行处理。 */
    pe_导入(状态机1, 0, name, (uintptr_t)val);
#else
    设置_elf_符号(单词表_部分, (uintptr_t)val, 0,
        ELFW(ST_INFO)(STB_GLOBAL, STT_NOTYPE), 0,
        SHN_ABS, name);
#endif
    return 0;
}

HEXINKU接口 void 设置库路径(知心状态机 *s, const char *path)
{
    内存_释放(s->zhi_库_路径);
    s->zhi_库_路径 = 字符串_宽度加1(path);
}

#define WD_ALL    0x0001 /* warning is activated when using -Wall */
#define FD_INVERT 0x0002 /* invert value before storing */

typedef struct
{
    uint16_t offset;
    uint16_t flags;
    const char *name;
} 标记定义;

static int 没有标记(const char **pp)
{
    const char *p = *pp;
    if (*p != 'n' || *++p != 'o' || *++p != '-')
        return 0;
    *pp = p + 1;
    return 1;
}

静态_函数 int 设置标记(知心状态机 *s, const 标记定义 *flags, const char *name)
{
    int value, 返回值;
    const 标记定义 *p;
    const char *r;

    value = 1;
    r = name;
    if (没有标记(&r))
        value = 0;

    for (返回值 = -1, p = flags; p->name; ++p) {
        if (返回值) {
            if (strcmp(r, p->name))
                continue;
        } else {
            if (0 == (p->flags & WD_ALL))
                continue;
        }
        if (p->offset) {
            *((unsigned char *)s + p->offset) =
                p->flags & FD_INVERT ? !value : value;
            if (返回值)
                return 0;
        } else {
            返回值 = 0;
        }
    }
    return 返回值;
}

static int strstart(const char *val, const char **str)
{
    const char *str1, *val1;
    str1 = *str;
    val1 = val;
    while (*val1)
    {
        if (*str1 != *val1)
            return 0;
        str1++;
        val1++;
    }
    *str = str1;
    return 1;
}

/*与strstart类似，但会自动考虑ld选项可以
  *
  *-以双破折号或单破折号开头（例如'--基本名称'或'-基本名称'）
  *-参数可以单独或在'='之后给出（例如'-Wl，-基本名称，x.so'
  *或'-Wl，-基本名称 = x.so'）
  *
  *您总是以“ option [=]”形式提供“ val”（无前导-）
 */
static int 链接_选项(const char *str, const char *val, const char **ptr)
{
    const char *p, *q;
    int 返回值;

    /* 应该有1或2个破折号 */
    if (*str++ != '-')
        return 0;
    if (*str == '-')
        str++;

    /* then str & val should match (potentially up to '=') */
    p = str;
    q = val;

    返回值 = 1;
    if (q[0] == '?') {
        ++q;
        if (没有标记(&p))
            返回值 = -1;
    }

    while (*q != '\0' && *q != '=') {
        if (*p != *q)
            return 0;
        p++;
        q++;
    }

    /* '=' near eos means ',' or '=' is ok */
    if (*q == '=') {
        if (*p == 0)
            *ptr = p;
        if (*p != ',' && *p != '=')
            return 0;
        p++;
    } else if (*p) {
        return 0;
    }
    *ptr = p;
    return 返回值;
}

static const char *跳过_链接器_参数(const char **str)
{
    const char *状态机1 = *str;
    const char *s2 = strchr(状态机1, ',');
    *str = s2 ? s2++ : (s2 = 状态机1 + strlen(状态机1));
    return s2;
}

static void 复制_链接器_参数(char **pp, const char *s, int sep)
{
    const char *q = s;
    char *p = *pp;
    int l = 0;
    if (p && sep)
        p[l = strlen(p)] = sep, ++l;
    跳过_链接器_参数(&q);
    复制_字符串(l + (*pp = 内存_重分配容量(p, q - s + l + 1)), s, q - s);
}

/* set linker options */
static int 设置链接器(知心状态机 *s, const char *option)
{
    知心状态机 *状态机1 = s;
    while (*option) {

        const char *p = NULL;
        char *end = NULL;
        int ignoring = 0;
        int 返回值;

        if (链接_选项(option, "Bsymbolic", &p)) {
            s->先解析当前模块符号 = 1;
        } else if (链接_选项(option, "不添加标准库", &p)) {
            s->不添加标准库 = 1;
        } else if (链接_选项(option, "fini=", &p)) {
            复制_链接器_参数(&s->卸载_符号, p, 0);
            ignoring = 1;
        } else if (链接_选项(option, "image-base=", &p)
                || 链接_选项(option, "Ttext=", &p)) {
            s->代码段_地址 = strtoull(p, &end, 16);
            s->已有_代码段_地址 = 1;
        } else if (链接_选项(option, "init=", &p)) {
            复制_链接器_参数(&s->加载_符号, p, 0);
            ignoring = 1;
        } else if (链接_选项(option, "oformat=", &p)) {
#if defined(ZHI_TARGET_PE)
            if (strstart("pe-", &p)) {
#elif 指针_大小 == 8
            if (strstart("elf64-", &p)) {
#else
            if (strstart("elf32-", &p)) {
#endif
                s->输出_格式 = ZHI_输出_格式_ELF;
            } else if (!strcmp(p, "binary")) {
                s->输出_格式 = ZHI_输出_格式_二进制;
#ifdef ZHI_TARGET_COFF
            } else if (!strcmp(p, "coff")) {
                s->输出_格式 = ZHI_输出_格式_COFF;
#endif
            } else
                goto err;

        } else if (链接_选项(option, "as-needed", &p)) {
            ignoring = 1;
        } else if (链接_选项(option, "O", &p)) {
            ignoring = 1;
        } else if (链接_选项(option, "export-all-symbols", &p)) {
            s->导出所有符号 = 1;
        } else if (链接_选项(option, "export-dynamic", &p)) {
            s->导出所有符号 = 1;
        } else if (链接_选项(option, "动态库路径=", &p)) {
            复制_链接器_参数(&s->动态库路径, p, ':');
        } else if (链接_选项(option, "enable-new-dtags", &p)) {
            s->启用新的dtags = 1;
        } else if (链接_选项(option, "section-alignment=", &p)) {
            s->分段_对齐 = strtoul(p, &end, 16);
        } else if (链接_选项(option, "基本名称=", &p)) {
            复制_链接器_参数(&s->基本名称, p, 0);
#ifdef ZHI_TARGET_PE
        } else if (链接_选项(option, "large-address-aware", &p)) {
            s->pe_特征 |= 0x20;
        } else if (链接_选项(option, "file-alignment=", &p)) {
            s->pe_文件_对齐 = strtoul(p, &end, 16);
        } else if (链接_选项(option, "stack=", &p)) {
            s->pe_堆栈_大小 = strtoul(p, &end, 10);
        } else if (链接_选项(option, "subsystem=", &p)) {
#if defined(ZHI_TARGET_I386) || defined(ZHI_TARGET_X86_64)
            if (!strcmp(p, "native")) {
                s->pe_子系统 = 1;
            } else if (!strcmp(p, "console")) {
                s->pe_子系统 = 3;
            } else if (!strcmp(p, "gui") || !strcmp(p, "windows")) {
                s->pe_子系统 = 2;
            } else if (!strcmp(p, "posix")) {
                s->pe_子系统 = 7;
            } else if (!strcmp(p, "efiapp")) {
                s->pe_子系统 = 10;
            } else if (!strcmp(p, "efiboot")) {
                s->pe_子系统 = 11;
            } else if (!strcmp(p, "efiruntime")) {
                s->pe_子系统 = 12;
            } else if (!strcmp(p, "efirom")) {
                s->pe_子系统 = 13;
#elif defined(ZHI_TARGET_ARM)
            if (!strcmp(p, "wince")) {
                s->pe_子系统 = 9;
#endif
            } else
                goto err;
#endif
        } else if (返回值 = 链接_选项(option, "?whole-archive", &p), 返回值) {
            if (返回值 > 0)
                s->文件类型 |= AFF_从存档加载_所有对象;
            else
                s->文件类型 &= ~AFF_从存档加载_所有对象;
        } else if (p) {
            return 0;
        } else {
    err:
            错误_打印("不支持的链接器选项 '%s'", option);
        }

        if (ignoring && s->警告_不支持)
            zhi_警告("不支持的链接器选项 '%s'", option);

        option = 跳过_链接器_参数(&p);
    }
    return 1;
}

typedef struct
{
    const char *name;
    uint16_t index;
    uint16_t flags;
} 知心指令;

enum {
    知心编译_指令_帮助,
    知心编译_指令_关于,
    知心编译_指令_版本,
    知心编译_指令_添加头文件路径,
    知心编译_指令_用val定义sym,
    知心编译_指令_未定义的sym,
    知心编译_指令_P,
    知心编译_指令_添加库路径,
    知心编译_指令_设置zhi实用程序路径,
    知心编译_指令_l,
    知心编译_指令_显示编译统计,
    知心编译_指令_与backtrace链接,
    知心编译_指令_使用内置内存和边界检查器进行编译,
    知心编译_指令_ba,
    知心编译_指令_生成运行时调试信息,
    知心编译_指令_仅编译不链接,
    知心编译_指令_打印版本信息,
    知心编译_指令_使用E输出宏定义指令,
    知心编译_指令_链接到静态库,
    知心编译_指令_设置知心编译器标准,
    知心编译_指令_生成共享库,
    知心编译_指令_设置运行时使用的共享库名称,
    知心编译_指令_设置输出文件名,
    知心编译_指令_生成可重定位目标文件,
    知心编译_指令_s,
    知心编译_指令_traditional,
    知心编译_指令_设置链接器,
    知心编译_指令_Wp,
    知心编译_指令_设置和重置警告,
    知心编译_指令_仅用于定义OPTIMIZE,
    知心编译_指令_mfloat_abi,
    知心编译_指令_m,
    知心编译_指令_设置或重置flag,
    知心编译_指令_添加到系统包含路径,
    知心编译_指令_iwithprefix,
    知心编译_指令_在文件上方导入文件,
    知心编译_指令_不使用标准系统包含路径,
    知心编译_指令_不与标准crt和库链接,
    知心编译_指令_打印搜索路径,
    知心编译_指令_所有全局符号导出到动态链接器,
    知心编译_指令_param,
    知心编译_指令_pedantic,
    知心编译_指令_连接线程,
    知心编译_指令_编译源文件,
    知心编译_指令_禁止所有警告,
    知心编译_指令_pipe,
    知心编译_指令_生成预处理文件,
    知心编译_指令_生成make的依赖文件,
    知心编译_指令_指定依赖文件名,
    知心编译_指令_指定文件后缀类型,
    知心编译_指令_创建库,
    知心编译_指令_创建定义文件
};

#define 知心编译_指令_有_参数 0x0001
#define 知心编译_指令_NOSEP   0x0002 /* option和arg之前不能有空间 */

static const 知心指令 zhi_选项[] = {
    { "帮助", 知心编译_指令_帮助, 0 },
	{ "h", 知心编译_指令_帮助, 0 },
    { "help", 知心编译_指令_帮助, 0 },
    { "?", 知心编译_指令_帮助, 0 },
	{ "关于", 知心编译_指令_关于, 0 },
    { "about", 知心编译_指令_关于, 0 },
	{ "版本", 知心编译_指令_版本, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "v", 知心编译_指令_版本, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "version", 知心编译_指令_版本, 0 },
    { "I", 知心编译_指令_添加头文件路径, 知心编译_指令_有_参数 },
    { "D", 知心编译_指令_用val定义sym, 知心编译_指令_有_参数 },
    { "U", 知心编译_指令_未定义的sym, 知心编译_指令_有_参数 },
    { "P", 知心编译_指令_P, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "L", 知心编译_指令_添加库路径, 知心编译_指令_有_参数 },
    { "B", 知心编译_指令_设置zhi实用程序路径, 知心编译_指令_有_参数 },
    { "l", 知心编译_指令_l, 知心编译_指令_有_参数 },
    { "bench", 知心编译_指令_显示编译统计, 0 },
#ifdef ZHI_配置_记录回滚
    { "bt", 知心编译_指令_与backtrace链接, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
#endif
#ifdef 配置_ZHI_边界检查
    { "b", 知心编译_指令_使用内置内存和边界检查器进行编译, 0 },
#endif
    { "g", 知心编译_指令_生成运行时调试信息, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "c", 知心编译_指令_仅编译不链接, 0 },
    { "dumpversion", 知心编译_指令_打印版本信息, 0},
    { "d", 知心编译_指令_使用E输出宏定义指令, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "static", 知心编译_指令_链接到静态库, 0 },
    { "std", 知心编译_指令_设置知心编译器标准, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "shared", 知心编译_指令_生成共享库, 0 },
    { "基本名称", 知心编译_指令_设置运行时使用的共享库名称, 知心编译_指令_有_参数 },
    { "o", 知心编译_指令_设置输出文件名, 知心编译_指令_有_参数 },
    { "-param", 知心编译_指令_param, 知心编译_指令_有_参数 },
    { "pedantic", 知心编译_指令_pedantic, 0},
    { "pthread", 知心编译_指令_连接线程, 0},
	{ "运行", 知心编译_指令_编译源文件, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "run", 知心编译_指令_编译源文件, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "导出所有符号", 知心编译_指令_所有全局符号导出到动态链接器, 0 },
    { "r", 知心编译_指令_生成可重定位目标文件, 0 },
    { "s", 知心编译_指令_s, 0 },
    { "traditional", 知心编译_指令_traditional, 0 },
    { "Wl,", 知心编译_指令_设置链接器, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "Wp,", 知心编译_指令_Wp, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "W", 知心编译_指令_设置和重置警告, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "O", 知心编译_指令_仅用于定义OPTIMIZE, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
#ifdef ZHI_TARGET_ARM
    { "mfloat-abi", 知心编译_指令_mfloat_abi, 知心编译_指令_有_参数 },
#endif
    { "m", 知心编译_指令_m, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "f", 知心编译_指令_设置或重置flag, 知心编译_指令_有_参数 | 知心编译_指令_NOSEP },
    { "isystem", 知心编译_指令_添加到系统包含路径, 知心编译_指令_有_参数 },
    { "include", 知心编译_指令_在文件上方导入文件, 知心编译_指令_有_参数 },
    { "不添加标准头", 知心编译_指令_不使用标准系统包含路径, 0 },
    { "不添加标准库", 知心编译_指令_不与标准crt和库链接, 0 },
    { "print-search-dirs", 知心编译_指令_打印搜索路径, 0 },
    { "w", 知心编译_指令_禁止所有警告, 0 },
    { "pipe", 知心编译_指令_pipe, 0},
    { "E", 知心编译_指令_生成预处理文件, 0},
    { "MD", 知心编译_指令_生成make的依赖文件, 0},
    { "MF", 知心编译_指令_指定依赖文件名, 知心编译_指令_有_参数 },
    { "x", 知心编译_指令_指定文件后缀类型, 知心编译_指令_有_参数 },
    { "ar", 知心编译_指令_创建库, 0},
#ifdef ZHI_TARGET_PE
    { "impdef", 知心编译_指令_创建定义文件, 0},
#endif
    { NULL, 0, 0 },
};

static const 标记定义 选项_W[] = {
    { 0, 0, "all" },
    { offsetof(知心状态机, 警告_不支持), 0, "unsupported" },
    { offsetof(知心状态机, 警告_写字符串), 0, "write-strings" },
    { offsetof(知心状态机, 警告_错误), 0, "error" },
    { offsetof(知心状态机, 警告_gcc兼容), 0, "gcc-compat" },
    { offsetof(知心状态机, 警告_隐式函数声明), WD_ALL,
      "implicit-function-declaration" },
    { 0, 0, NULL }
};

static const 标记定义 选项_f[] = {
    { offsetof(知心状态机, 字符串_无符号), 0, "unsigned-char" },
    { offsetof(知心状态机, 字符串_无符号), FD_INVERT, "signed-char" },
    { offsetof(知心状态机, 不使用通用符号), FD_INVERT, "common" },
    { offsetof(知心状态机, 前导_下划线), 0, "leading-underscore" },
    { offsetof(知心状态机, 允许_匿名联合和结构), 0, "ms-extensions" },
    { offsetof(知心状态机, 允许_在标识符中使用美元符号), 0, "dollars-in-identifiers" },
    { 0, 0, NULL }
};

static const 标记定义 选项_m[] = {
    { offsetof(知心状态机, 模拟_对齐位域MS算法), 0, "ms-bitfields" },
#ifdef ZHI_TARGET_X86_64
    { offsetof(知心状态机, 支持mno和sse), FD_INVERT, "sse" },
#endif
    { 0, 0, NULL }
};

static void 解析_选项_D(知心状态机 *状态机1, const char *编译指令)
{
    char *sym = 字符串_宽度加1(编译指令);
    char *value = strchr(sym, '=');
    if (value)
        *value++ = '\0';
    定义预处理程序符号(状态机1, sym, value);
    内存_释放(sym);
}

static void 参数_解析_增加_文件(知心状态机 *s, const char* 文件名, int 文件类型)
{
    文件名称类型结构 *文件 = 内存_申请(sizeof *文件 + strlen(文件名));
    文件->类型 = 文件类型;
    strcpy(文件->名称, 文件名);
    动态数组_追加元素(&s->文件数, &s->数量_文件数, 文件);
}

static int 参数数量_解析_make_参数数组(const char *文件缓存区, int *命令行参数数量, char ***命令行参数数组)
{
    int 返回值 = 0, q, c;
    动态字符串 str;
    for(;;)/*永久循环*/
    {
        while (c = (unsigned char)*文件缓存区, c && c <= ' ')
          ++文件缓存区;
        if (c == 0)
            break;
        q = 0;
        动态字符串_初始化(&str);
        while (c = (unsigned char)*文件缓存区, c)
        {
            ++文件缓存区;
            if (c == '\\' && (*文件缓存区 == '"' || *文件缓存区 == '\\'))
            {
                c = *文件缓存区++;
            } else if (c == '"') {
                q = !q;
                continue;
            } else if (q == 0 && c <= ' ')
            {
                break;
            }
            动态字符串_追加单个字符(&str, c);
        }
        动态字符串_追加单个字符(&str, 0);
        动态数组_追加元素(命令行参数数组, 命令行参数数量, 字符串_宽度加1(str.指向字符串的指针));
        动态字符串_释放(&str);
        ++返回值;
    }
    return 返回值;
}

/* 读取文件列表 */
static void 从参数数组获取列表文件(知心状态机 *状态机,const char *文件名, int optind, int *pargc, char ***pargv)
{
    知心状态机 *状态机1 = 状态机;
    int 文件描述词, i;
    size_t 文件字节数;
    char *文件的缓存区;
    int 参数数量 = 0;
    char **命令行参数数组 = NULL;
/*open(文件名, O_RDONLY | O_BINARY)参数 文件名 指向欲打开的文件路径字符串. 下列是参数flags 所能使用的旗标:O_RDONLY 以只读方式打开文件.
 * 返回值：若所有欲核查的权限都通过了检查则返回0 值, 表示成功, 只要有一个权限被禁止则返回-1.*/
    文件描述词 = open(文件名, O_RDONLY | O_BINARY);
    if (文件描述词 < 0)
        错误_打印("找不到列表文件 '%s' ", 文件名);
    文件字节数 = lseek(文件描述词, 0, SEEK_END);/*移动文件的读写位置到文件尾位置*/
    文件的缓存区 = 内存_申请(文件字节数 + 1);
    文件的缓存区[文件字节数] = 0;
    lseek(文件描述词, 0, SEEK_SET);/*移动文件的读写位置到文件开始位置*/
    read(文件描述词, 文件的缓存区, 文件字节数);/*read(int fd,void *buf,int count);从文件说明符fd相关联的文件中读取count个字符，并把这些字符存储到buf所指的缓冲区中。*/
    close(文件描述词);

    for (i = 0; i < *pargc; ++i)
        if (i == optind)
            参数数量_解析_make_参数数组(文件的缓存区, &参数数量, &命令行参数数组);
        else
            动态数组_追加元素(&命令行参数数组, &参数数量, 字符串_宽度加1((*pargv)[i]));

    内存_释放(文件的缓存区);
    动态数组_重分配容量(&状态机->参数数组, &状态机->参数数量);
    *pargc = 状态机->参数数量 = 参数数量, *pargv = 状态机->参数数组 = 命令行参数数组;
}
/*开始解析main导入的参数文件*/
公共_函数 int 解析命令行参数(知心状态机 *状态机, int *pargc, char ***pargv, int optind)/*optind(选择)*/
{
    知心状态机 *状态机1 = 状态机;
    const 知心指令 *临时指令;
    const char *编译指令, *临时参数数组;
    const char *run = NULL;
    int x;
    动态字符串 连接器变量; /* 查找 -Wl 选项 */
    int tool = 0, arg_start = 0, noaction = optind;
    char **命令行参数数组 = *pargv;
    int 命令行参数数量 = *pargc;
    动态字符串_初始化(&连接器变量);

/***************************while()--开始***************************/
    while (optind < 命令行参数数量)
    {
    	临时参数数组 = 命令行参数数组[optind];
        if (临时参数数组[0] == '@' && 临时参数数组[1] != '\0')
        {
            从参数数组获取列表文件(状态机, 临时参数数组 + 1, optind, &命令行参数数量, &命令行参数数组);
            continue;
        }
        optind++;
        if (tool)
        {
            if (临时参数数组[0] == '-' && 临时参数数组[1] == 'v' && 临时参数数组[2] == 0)
                ++状态机->显示信息;
            continue;
        }
reparse:
        if (临时参数数组[0] != '-' || 临时参数数组[1] == '\0')
        {
            if (临时参数数组[0] != '@') /* 允许“ zhi file(s) -run @ args ...” */
                参数_解析_增加_文件(状态机, 临时参数数组, 状态机->文件类型);
            if (run)
            {
                设置编译选项(状态机, run);
                arg_start = optind - 1;
                break;
            }
            continue;
        }

        /* 在表中查找选项 */
        for(临时指令 = zhi_选项; ; ++临时指令)
        {
            const char *指令名称1 = 临时指令->name;
            const char *指令1 = 临时参数数组 + 1;
            if (指令名称1 == NULL)
                错误_打印("无效的指令 -- '%s'", 临时参数数组);
            if (!strstart(指令名称1, &指令1))
                continue;
            编译指令 = 指令1;
            if (临时指令->flags & 知心编译_指令_有_参数)
            {
                if (*指令1 == '\0' && !(临时指令->flags & 知心编译_指令_NOSEP))
                {
                    if (optind >= 命令行参数数量)
                    {参数_错误:
                        错误_打印(" '%s' 的参数丢失", 临时参数数组);
                    }
                    编译指令 = 命令行参数数组[optind++];
                }
            } else if (*指令1 != '\0')
                continue;
            break;
        }



        switch(临时指令->index)
        {
        case 知心编译_指令_帮助:
            x = 指令_HELP;
            goto extra_action;
        case 知心编译_指令_关于:
            x = 指令_ABOUT;
            goto extra_action;
        case 知心编译_指令_添加头文件路径:
            添加包含路径(状态机, 编译指令);
            break;
        case 知心编译_指令_用val定义sym:
            解析_选项_D(状态机, 编译指令);
            break;
        case 知心编译_指令_未定义的sym:
            未定义预处理符号(状态机, 编译指令);
            break;
        case 知心编译_指令_添加库路径:
            添加库路径(状态机, 编译指令);
            break;
        case 知心编译_指令_设置zhi实用程序路径:
            /* 设置zhi实用程序路径（主要用于zhi开发） */
            设置库路径(状态机, 编译指令);
            break;
        case 知心编译_指令_l:
            参数_解析_增加_文件(状态机, 编译指令, 文件格式_类型_库 | (状态机->文件类型 & ~文件格式_类型_掩码));
            状态机->数量_库数++;
            break;
        case 知心编译_指令_连接线程:
        	状态机->选项_线程 = 1;
            break;
        case 知心编译_指令_显示编译统计:
        	状态机->显示_编译统计 = 1;
            break;
#ifdef ZHI_配置_记录回滚
        case 知心编译_指令_与backtrace链接:
        	状态机->运行时_num_callers = atoi(编译指令);
        	状态机->执行_跟踪 = 1;
        	状态机->执行_调试 = 1;
            break;
#endif
#ifdef 配置_ZHI_边界检查
        case 知心编译_指令_使用内置内存和边界检查器进行编译:
        	状态机->执行_边界_检查器 = 1;
        	状态机->执行_跟踪 = 1;
        	状态机->执行_调试 = 1;
            break;
#endif
        case 知心编译_指令_生成运行时调试信息:
        	状态机->执行_调试 = 1;
            break;
        case 知心编译_指令_仅编译不链接:
            x = ZHI_输出_目标文件;
        set_output_type:
            if (状态机->输出_类型)
                zhi_警告("-%s: 覆盖已指定的编译器操作", 临时指令->name);
            状态机->输出_类型 = x;
            break;
        case 知心编译_指令_使用E输出宏定义指令:
            if (*编译指令 == 'D')
            	状态机->DX标号 = 3;
            else if (*编译指令 == 'M')
            	状态机->DX标号 = 7;
            else if (*编译指令 == 't')
            	状态机->DX标号 = 16;
            else if (是数字(*编译指令))
            	状态机->g_调试 |= atoi(编译指令);
            else
                goto 不支持的_选项;
            break;
        case 知心编译_指令_链接到静态库:
        	状态机->执行_静态链接 = 1;
            break;
        case 知心编译_指令_设置知心编译器标准:
            if (strcmp(编译指令, "=c11") == 0)
            	状态机->ZHI版本 = 201112;
            break;
        case 知心编译_指令_生成共享库:
            x = ZHI_输出_DLL;
            goto set_output_type;
        case 知心编译_指令_设置运行时使用的共享库名称:
        	状态机->基本名称 = 字符串_宽度加1(编译指令);
            break;
        case 知心编译_指令_设置输出文件名:
            if (状态机->输出文件)
            {
                zhi_警告("多个 -o 选项");
                内存_释放(状态机->输出文件);
            }
            状态机->输出文件 = 字符串_宽度加1(编译指令);
            break;
        case 知心编译_指令_生成可重定位目标文件:
            /* 生成.o 合并多个输出文件 */
        	状态机->选项_可重定位目标文件 = 1;
            x = ZHI_输出_目标文件;
            goto set_output_type;
        case 知心编译_指令_添加到系统包含路径:
            添加到系统包含路径(状态机, 编译指令);
            break;
        case 知心编译_指令_在文件上方导入文件:
            动态字符串_打印(&状态机->命令行_包含, "#include \"%s\"\n", 编译指令);
            break;
        case 知心编译_指令_不使用标准系统包含路径:
        	状态机->不添加标准头 = 1;
            break;
        case 知心编译_指令_不与标准crt和库链接:
        	状态机->不添加标准库 = 1;
            break;
        case 知心编译_指令_编译源文件:
#ifndef ZHI_是_本机
            错误_打印("-run 在交叉编译器中不可用");
#endif
            run = 编译指令;
            x = ZHI_输出_内存中运行;
            goto set_output_type;
        case 知心编译_指令_版本:
            do ++状态机->显示信息; while (*编译指令++ == 'v');
            ++noaction;
            break;
        case 知心编译_指令_设置或重置flag:
            if (设置标记(状态机, 选项_f, 编译指令) < 0)
                goto 不支持的_选项;
            break;
#ifdef ZHI_TARGET_ARM
        case 知心编译_指令_mfloat_abi:
            /* zhi还不支持软浮动 */
            if (!strcmp(编译指令, "softfp"))
            {
                s->浮动_abi = ARM_SOFTFP_FLOAT;
                未定义预处理符号(s, "__ARM_PCS_VFP");
            } else if (!strcmp(编译指令, "hard"))
                s->浮动_abi = ARM_HARD_FLOAT;
            else
                错误_打印("不支持的浮动abi '%s'", 编译指令);
            break;
#endif
        case 知心编译_指令_m:
            if (设置标记(状态机, 选项_m, 编译指令) < 0)
            {
                if (x = atoi(编译指令), x != 32 && x != 64)/*int atoi(const char *str) 把参数 str 所指向的字符串转换为一个整数（类型为 int 型）。*/
                    goto 不支持的_选项;
                if (指针_大小 != x/8)
                    return x;
                ++noaction;
            }
            break;
        case 知心编译_指令_设置和重置警告:
        	状态机->警告_none = 0;
            if (编译指令[0] && 设置标记(状态机, 选项_W, 编译指令) < 0)
                goto 不支持的_选项;
            break;
        case 知心编译_指令_禁止所有警告:
        	状态机->警告_none = 1;
            break;
        case 知心编译_指令_所有全局符号导出到动态链接器:
        	状态机->导出所有符号 = 1;
            break;
        case 知心编译_指令_设置链接器:
            if (连接器变量.字符串长度)
                --连接器变量.字符串长度, 动态字符串_追加单个字符(&连接器变量, ',');
            动态字符串_cat(&连接器变量, 编译指令, 0);
            if (设置链接器(状态机, 连接器变量.指向字符串的指针))
                动态字符串_释放(&连接器变量);
            break;
        case 知心编译_指令_Wp:
        	临时参数数组 = 编译指令;
            goto reparse;
        case 知心编译_指令_生成预处理文件:
            x = ZHI_输出_预处理;
            goto set_output_type;
        case 知心编译_指令_P:
        	状态机->P标号 = atoi(编译指令) + 1;
            break;
        case 知心编译_指令_生成make的依赖文件:
        	状态机->生成_依赖 = 1;
            break;
        case 知心编译_指令_指定依赖文件名:
        	状态机->依赖_输出文件 = 字符串_宽度加1(编译指令);
            break;
        case 知心编译_指令_打印版本信息:
            printf ("%s\n", ZHI_VERSION);
            exit(0);
            break;
        case 知心编译_指令_指定文件后缀类型:
            x = 0;
            if (*编译指令 == 'c')
                x = 文件格式_类型_Z;
            else if (*编译指令 == 'a')
                x = 文件格式_类型_ASMPP;
            else if (*编译指令 == 'b')
                x = 文件格式_类型_BIN;
            else if (*编译指令 == 'n')
                x = 文件格式_类型_NONE;
            else
                zhi_警告("不支持的语言 '%s'", 编译指令);
            状态机->文件类型 = x | (状态机->文件类型 & ~文件格式_类型_掩码);
            break;
        case 知心编译_指令_仅用于定义OPTIMIZE:
        	状态机->编译优化 = atoi(编译指令);
            break;
        case 知心编译_指令_打印搜索路径:
            x = 指令_打印_目录;
            goto extra_action;
        case 知心编译_指令_创建定义文件:
            x = 指令_IMPDEF;
            goto extra_action;
        case 知心编译_指令_创建库:
            x = 指令_AR;
        extra_action:
            arg_start = optind - 1;
            if (arg_start != noaction)
                错误_打印("无法在这里解析 %s ", 临时参数数组);
            tool = x;
            break;
        case 知心编译_指令_traditional:
        case 知心编译_指令_pedantic:
        case 知心编译_指令_pipe:
        case 知心编译_指令_s:
            /* 被忽略 */
            break;
        default:
   不支持的_选项:
            if (状态机->警告_不支持)
                zhi_警告("不支持的选项 '%s'", 临时参数数组);
            break;
        }
    }
/***************************while()--结束***************************/


    if (连接器变量.字符串长度)
    {
        临时参数数组 = 连接器变量.指向字符串的指针;
        goto 参数_错误;
    }
    *pargc = 命令行参数数量 - arg_start;
    *pargv = 命令行参数数组 + arg_start;
    if (tool)
        return tool;
    if (optind != noaction)
        return 0;
    if (状态机->显示信息 == 2)
        return 指令_打印_目录;
    if (状态机->显示信息)
        return 指令_V;
    return 指令_HELP;
}

HEXINKU接口 void 设置编译选项(知心状态机 *s, const char *r)
{
    char **命令行参数数组 = NULL;
    int 参数数量 = 0;
    参数数量_解析_make_参数数组(r, &参数数量, &命令行参数数组);
    解析命令行参数(s, &参数数量, &命令行参数数组, 0);
    动态数组_重分配容量(&命令行参数数组, &参数数量);
}

公共_函数 void 显示编译统计信息(知心状态机 *状态机1, unsigned total_time)
{
    if (total_time < 1)
        total_time = 1;
    if (总_字节数 < 1)
        总_字节数 = 1;
    fprintf(stderr, "* %d 个标识符, %d 行, %d 个字节\n"
                    "* %0.3f 秒, %u 行/秒, %0.1f 兆字节(MB)/秒\n",
           总_idents, 总_行数, 总_字节数,
           (double)total_time/1000,
           (unsigned)总_行数*1000/total_time,
           (double)总_字节数/1000/total_time);
#ifdef 内存_调试
    fprintf(stderr, "* 已使用 %d 个字节的内存\n", 内存_最大_大小);
#endif
}
