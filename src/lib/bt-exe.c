/* ------------------------------------------------------------- */
/* for linking 运行时_打印行 and the signal/exception handler
   from run开关.c into executables. */

#define 仅仅_配置_ZHI_回滚
#include "../run开关.c"

int (*__rt_错误)(void*, void*, const char *, va_list);

#ifndef _WIN32
# define __declspec(n)
#endif

__declspec(dllexport)
void __bt_init(运行时_上下文 *p, int num_callers, int mode)
{
    __attribute__((weak)) int main();
    __attribute__((weak)) void __bound_init(void*, int);
    struct 运行时_上下文 *rc = &g_rtctxt;
    //fprintf(stderr, "__bt_init %d %p %p %d\n", num_callers, p->stab_sym, p->bounds_start, mode), fflush(stderr);
    if (num_callers)
    {
        memcpy(rc, p, offsetof(运行时_上下文, next));
        rc->num_callers = num_callers - 1;
        rc->top_func = main;
        __rt_错误 = _rt_错误;
        设置_异常_处理程序();
    } else
    {
        p->next = rc->next, rc->next = p;
    }
    if (__bound_init && p->bounds_start)
        __bound_init(p->bounds_start, mode);
}

/* copy a string and truncate it. */
static char *p字符串复制(char *buf, size_t buf_size, const char *s)
{
    int l = strlen(s);
    if (l >= buf_size)
        l = buf_size - 1;
    memcpy(buf, s, l);
    buf[l] = 0;
    return buf;
}
