/*
 * ZHI的Mach-O文件处理
 */
#include "zhi.h"

/* 为了使我们的生活更轻松，我们正在生成Mach-O文件，
    不使用某些现代功能，但并不完全是经典功能
    它们确实使用了一些现代功能。 我们也只是
    生成64位Mach-O文件，并且仅生成本机字节序。

    特别是，我们生成的不使用的可执行文件
    DYLD_INFO用于动态链接信息，因为这需要我们构建一个
    导出名称的名称。 我们只是使用经典的单词表
    仍然受到现代行业的支持。

    但是我们确实使用LC_MAIN，这是一项“现代”功能，以免
    设置我们自己的crt代码。 我们没有使用惰性链接，所以即使功能
    呼叫在启动时解决。  */

#define DEBUG_MACHO 0
#define dprintf if (DEBUG_MACHO) printf

struct fat_header {
    uint32_t        magic;          /* FAT_MAGIC or FAT_MAGIC_64 */
    uint32_t        nfat_arch;      /* number of structs that follow */
};

struct fat_arch {
    int             cputype;        /* cpu specifier (int) */
    int             cpusubtype;     /* machine specifier (int) */
    uint32_t        offset;         /* file offset to this object file */
    uint32_t        size;           /* size of this object file */
    uint32_t        align;          /* alignment as a power of 2 */
};

#define FAT_MAGIC       0xcafebabe
#define FAT_CIGAM       0xbebafeca
#define FAT_MAGIC_64    0xcafebabf
#define FAT_CIGAM_64    0xbfbafeca

struct mach_header {
    uint32_t        magic;          /* mach magic number identifier */
    int             cputype;        /* cpu specifier */
    int             cpusubtype;     /* machine specifier */
    uint32_t        文件类型;       /* type of file */
    uint32_t        ncmds;          /* number of 加载 commands */
    uint32_t        sizeofcmds;     /* the size of all the 加载 commands */
    uint32_t        flags;          /* flags */
};

struct mach_header_64 {
    struct mach_header  mh;
    uint32_t            reserved;       /* reserved, pad to 64bit */
};

/* Constant for the magic field of the mach_header (32-bit architectures) */
#define MH_MAGIC        0xfeedface      /* the mach magic number */
#define MH_CIGAM        0xcefaedfe      /* NXSwapInt(MH_MAGIC) */
#define MH_MAGIC_64     0xfeedfacf      /* the 64-bit mach magic number */
#define MH_CIGAM_64     0xcffaedfe      /* NXSwapInt(MH_MAGIC_64) */

struct load_command {
    uint32_t        cmd;            /* type of 加载 command */
    uint32_t        cmdsize;        /* total size of command in bytes */
};

#define LC_REQ_DYLD 0x80000000
#define LC_SYMTAB        0x2
#define LC_DYSYMTAB      0xb
#define LC_LOAD_DYLIB    0xc
#define LC_ID_DYLIB      0xd
#define LC_LOAD_DYLINKER 0xe
#define LC_SEGMENT_64    0x19
#define LC_REEXPORT_DYLIB (0x1f | LC_REQ_DYLD)
#define LC_MAIN (0x28|LC_REQ_DYLD)

typedef int vm_prot_t;

struct segment_command_64 { /* for 64-bit architectures */
    uint32_t        cmd;            /* LC_SEGMENT_64 */
    uint32_t        cmdsize;        /* includes sizeof 段_64 structs */
    char            segname[16];    /* segment name */
    uint64_t        vmaddr;         /* memory address of this segment */
    uint64_t        vmsize;         /* memory size of this segment */
    uint64_t        fileoff;        /* file offset of this segment */
    uint64_t        filesize;       /* amount to map from the file */
    vm_prot_t       maxprot;        /* maximum VM protection */
    vm_prot_t       initprot;       /* initial VM protection */
    uint32_t        nsects;         /* number of 段数 in segment */
    uint32_t        flags;          /* flags */
};

struct 段_64 { /* for 64-bit architectures */
    char            sectname[16];   /* name of this section */
    char            segname[16];    /* segment this section goes in */
    uint64_t        addr;           /* memory address of this section */
    uint64_t        size;           /* size in bytes of this section */
    uint32_t        offset;         /* file offset of this section */
    uint32_t        align;          /* section alignment (power of 2) */
    uint32_t        reloff;         /* file offset of relocation entries */
    uint32_t        nreloc;         /* number of relocation entries */
    uint32_t        flags;          /* flags (section type and attributes)*/
    uint32_t        reserved1;      /* reserved (for offset or index) */
    uint32_t        reserved2;      /* reserved (for count or sizeof) */
    uint32_t        reserved3;      /* reserved */
};

#define S_REGULAR                       0x0
#define S_ZEROFILL                      0x1
#define S_NON_LAZY_SYMBOL_POINTERS      0x6
#define S_MOD_INIT_函数_POINTERS        0x9
#define S_MOD_TERM_函数_POINTERS        0xa

#define S_ATTR_PURE_INSTRUCTIONS        0x80000000
#define S_ATTR_SOME_INSTRUCTIONS        0x00000400

typedef uint32_t lc_str;

struct dylib_command {
    uint32_t cmd;                   /* LC_ID_DYLIB, LC_LOAD_{,WEAK_}DYLIB,
                                       LC_REEXPORT_DYLIB */
    uint32_t cmdsize;               /* includes pathname string */
    lc_str   name;                  /* library's path name */
    uint32_t timestamp;             /* library's build time stamp */
    uint32_t current_version;       /* library's current version number */
    uint32_t compatibility_version; /* library's compatibility vers number*/
};

struct dylinker_command {
    uint32_t        cmd;            /* LC_ID_DYLINKER, LC_LOAD_DYLINKER or
                                       LC_DYLD_ENVIRONMENT */
    uint32_t        cmdsize;        /* includes pathname string */
    lc_str          name;           /* dynamic linker's path name */
};

struct symtab_command {
    uint32_t        cmd;            /* LC_SYMTAB */
    uint32_t        cmdsize;        /* sizeof(struct symtab_command) */
    uint32_t        symoff;         /* symbol table offset */
    uint32_t        nsyms;          /* number of symbol table entries */
    uint32_t        stroff;         /* string table offset */
    uint32_t        strsize;        /* string table size in bytes */
};

struct dysymtab_command {
    uint32_t cmd;       /* LC_DYSYMTAB */
    uint32_t cmdsize;   /* sizeof(struct dysymtab_command) */

    uint32_t ilocalsym; /* index to local symbols */
    uint32_t nlocalsym; /* number of local symbols */

    uint32_t iextdefsym;/* index to externally defined symbols */
    uint32_t nextdefsym;/* number of externally defined symbols */

    uint32_t iundefsym; /* index to undefined symbols */
    uint32_t nundefsym; /* number of undefined symbols */

    uint32_t tocoff;    /* file offset to table of contents */
    uint32_t ntoc;      /* number of entries in table of contents */

    uint32_t modtaboff; /* file offset to module table */
    uint32_t nmodtab;   /* number of module table entries */

    uint32_t extrefsymoff;  /* offset to referenced symbol table */
    uint32_t nextrefsyms;   /* number of referenced symbol table entries */

    uint32_t 间接的ectsymoff;/* file offset to the 间接的ect symbol table */
    uint32_t n间接的ectsyms; /* number of 间接的ect symbol table entries */

    uint32_t extreloff; /* offset to external relocation entries */
    uint32_t nextrel;   /* number of external relocation entries */
    uint32_t locreloff; /* offset to local relocation entries */
    uint32_t nlocrel;   /* number of local relocation entries */
};

#define INDIRECT_SYMBOL_LOCAL   0x80000000

struct entry_point_command {
    uint32_t  cmd;      /* LC_MAIN only used in MH_EXECUTE filetypes */
    uint32_t  cmdsize;  /* 24 */
    uint64_t  entryoff; /* file (__TEXT) offset of main() */
    uint64_t  stacksize;/* if not zero, initial stack size */
};

enum skind {
    sk_unknown = 0,
    sk_discard,
    sk_text,
    sk_stubs,
    sk_ro_data,
    sk_uw_info,
    sk_nl_ptr,  // non-lazy pointers, aka GOT
    sk_la_ptr,  // lazy pointers
    sk_init,
    sk_fini,
    sk_rw_data,
    sk_bss,
    sk_linkedit,
    sk_last
};

struct nlist_64 {
    uint32_t  n_strx;      /* index into the string table */
    uint8_t n_type;        /* type flag, see below */
    uint8_t n_sect;        /* section number or NO_SECT */
    uint16_t n_desc;       /* see <mach-o/stab.h> */
    uint64_t n_value;      /* value of this symbol (or stab offset) */
};

#define N_UNDF  0x0
#define N_ABS   0x2
#define N_EXT   0x1
#define N_SECT  0xe

#define N_WEAK_REF      0x0040
#define N_WEAK_DEF      0x0080

struct macho {
    struct mach_header_64 mh;
    int seg2lc[4], nseg;
    struct load_command **lc;
    struct entry_point_command *ep;
    int nlc;
    struct {
        段 *s;
        int machosect;
    } sk_to_sect[sk_last];
    int *elfsectomacho;
    int *e2msym;
    段 *全局单词表副本, *字符表, *wdata, *间接的syms, *stubs;
    int stubsym;
    uint32_t ilocal, iextdef, iundef;
};

#define SHT_LINKEDIT (SHT_LOOS + 42)
#define SHN_FROMDLL  (SHN_LOOS + 2)  /* Symbol is undefined, comes from a DLL */

static void * add_lc(struct macho *mo, uint32_t cmd, uint32_t cmdsize)
{
    struct load_command *lc = 内存_初始化(cmdsize);
    lc->cmd = cmd;
    lc->cmdsize = cmdsize;
    mo->lc = 内存_重分配容量(mo->lc, sizeof(mo->lc[0]) * (mo->nlc + 1));
    mo->lc[mo->nlc++] = lc;
    return lc;
}

static struct segment_command_64 * add_segment(struct macho *mo, char *name)
{
    struct segment_command_64 *sc = add_lc(mo, LC_SEGMENT_64, sizeof(*sc));
    strncpy(sc->segname, name, 16);
    mo->seg2lc[mo->nseg++] = mo->nlc - 1;
    return sc;
}

static struct segment_command_64 * get_segment(struct macho *mo, int i)
{
    return (struct segment_command_64 *) (mo->lc[mo->seg2lc[i]]);
}

static int add_section(struct macho *mo, struct segment_command_64 **_seg, char *name)
{
    struct segment_command_64 *seg = *_seg;
    int ret = seg->nsects;
    struct 段_64 *sec;
    seg->nsects++;
    seg->cmdsize += sizeof(*sec);
    seg = 内存_重分配容量(seg, sizeof(*seg) + seg->nsects * sizeof(*sec));
    sec = (struct 段_64*)((char*)seg + sizeof(*seg)) + ret;
    memset(sec, 0, sizeof(*sec));
    strncpy(sec->sectname, name, 16);
    strncpy(sec->segname, seg->segname, 16);
    *_seg = seg;
    return ret;
}

static struct 段_64 *get_section(struct segment_command_64 *seg, int i)
{
    return (struct 段_64*)((char*)seg + sizeof(*seg)) + i;
}

static void * add_dylib(struct macho *mo, char *name)
{
    struct dylib_command *lc;
    int sz = (sizeof(*lc) + strlen(name) + 1 + 7) & -8;
    lc = add_lc(mo, LC_LOAD_DYLIB, sz);
    lc->name = sizeof(*lc);
    strcpy((char*)lc + lc->name, name);
    lc->timestamp = 2;
    lc->current_version = 1 << 16;
    lc->compatibility_version = 1 << 16;
    return lc;
}

static void check_relocs(知心状态机 *状态机1, struct macho *mo)
{
    段 *s;
    ElfW_Rel *rel;
    ElfW(符号) *sym;
    int i, type, 获取plt_入口, sym_index, for_code;
    struct 额外_符号_属性 *attr;

    状态机1->got = 创建_节(状态机1, ".got", SHT_PROGBITS, SHF_ALLOC | SHF_WRITE);
    mo->间接的syms = 创建_节(状态机1, "LEINDIR", SHT_LINKEDIT, SHF_ALLOC | SHF_WRITE);
    for (i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (s->sh_type != SHT_RELX)
            continue;
        for_each_elem(s, 0, rel, ElfW_Rel) {
            type = ELFW(R_TYPE)(rel->r_info);
            获取plt_入口 = 获取plt_入口_类型(type);
            for_code = 代码_重定位(type);
            /* We generate a non-lazy pointer for used undefined symbols
               and for defined symbols that must have a place for their
               address due to codegen (i.e. a 重定位 requiring a got slot).  */
            sym_index = ELFW(R_SYM)(rel->r_info);
            sym = &((ElfW(符号) *)单词表_部分->data)[sym_index];
            if (sym->st_shndx == SHN_UNDEF
                || 获取plt_入口 == ALWAYS_GOTPLT_ENTRY) {
                attr = get_额外_符号_属性(状态机1, sym_index, 1);
                if (!attr->dyn_index) {
                    uint32_t *pi = 段_ptr_添加(mo->间接的syms, sizeof(*pi));
                    attr->got_offset = 状态机1->got->数据_偏移;
                    attr->plt_offset = -1;
                    attr->dyn_index = 1; /* used as flag */
                    段_ptr_添加(状态机1->got, 指针_大小);
                    if (ELFW(ST_BIND)(sym->st_info) == STB_LOCAL) {
                        if (sym->st_shndx == SHN_UNDEF)
                          错误_打印("undefined local symbol???");
                        *pi = INDIRECT_SYMBOL_LOCAL;
                        /* The pointer slot we generated must point to the
                           symbol, whose address is only known after layout,
                           so register a simple relocation for that.  */
                        使_elf_重定位(状态机1->全局单词表副本, 状态机1->got, attr->got_offset,
                                      R_DATA_PTR, sym_index);
                    } else
                      *pi = mo->e2msym[sym_index];
                }
                if (for_code) {
                    if (attr->plt_offset == -1) {
                        uint8_t *jmp;
                        attr->plt_offset = mo->stubs->数据_偏移;
                        jmp = 段_ptr_添加(mo->stubs, 6);
                        jmp[0] = 0xff;  /* jmpq *ofs(%rip) */
                        jmp[1] = 0x25;
                        使_elf_重定位(状态机1->全局单词表副本, mo->stubs,
                                      attr->plt_offset + 2,
                                      R_X86_64_GOTPCREL, sym_index);
                    }
                    rel->r_info = ELFW(R_INFO)(mo->stubsym, type);
                    rel->r_addend += attr->plt_offset;
                }
            }
        }
    }
}

static int check_symbols(知心状态机 *状态机1, struct macho *mo)
{
    int sym_index, sym_end;
    int ret = 0;

    mo->ilocal = mo->iextdef = mo->iundef = -1;
    sym_end = 单词表_部分->数据_偏移 / sizeof(ElfW(符号));
    for (sym_index = 1; sym_index < sym_end; ++sym_index) {
        int elf_index = ((struct nlist_64 *)mo->全局单词表副本->data + sym_index - 1)->n_value;
        ElfW(符号) *sym = (ElfW(符号) *)单词表_部分->data + elf_index;
        const char *name = (char*)单词表_部分->link->data + sym->st_name;
        unsigned type = ELFW(ST_TYPE)(sym->st_info);
        unsigned bind = ELFW(ST_BIND)(sym->st_info);
        unsigned vis  = ELFW(ST_VISIBILITY)(sym->st_other);

        dprintf("%4d (%4d): %09llx %4d %4d %4d %3d %s\n",
                sym_index, elf_index, sym->st_value,
                type, bind, vis, sym->st_shndx, name);
        if (bind == STB_LOCAL) {
            if (mo->ilocal == -1)
              mo->ilocal = sym_index - 1;
            if (mo->iextdef != -1 || mo->iundef != -1)
              错误_打印("local syms after global ones");
        } else if (sym->st_shndx != SHN_UNDEF) {
            if (mo->iextdef == -1)
              mo->iextdef = sym_index - 1;
            if (mo->iundef != -1)
              错误_打印("external defined symbol after undefined");
        } else if (sym->st_shndx == SHN_UNDEF) {
            if (mo->iundef == -1)
              mo->iundef = sym_index - 1;
            if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK
                || 查找_elf_符号(状态机1->动态单词表_部分, name)) {
                /* Mark the symbol as coming from a dylib so that
                   重定位_符号 doesn't complain.  Normally bind_exe_dynsyms
                   would do this check, and place the symbol into 导出的动态单词表
                   which is checked by 重定位_符号.  But Mach-O doesn't use
                   bind_exe_dynsyms.  */
                sym->st_shndx = SHN_FROMDLL;
                continue;
            }
            zhi_错误_不中止("undefined symbol '%s'", name);
            ret = -1;
        }
    }
    return ret;
}

static void convert_symbol(知心状态机 *状态机1, struct macho *mo, struct nlist_64 *pn)
{
    struct nlist_64 n = *pn;
    ELF符号 *sym = (ElfW(符号) *)单词表_部分->data + pn->n_value;
    const char *name = (char*)单词表_部分->link->data + sym->st_name;
    switch(ELFW(ST_TYPE)(sym->st_info)) {
    case STT_NOTYPE:
    case STT_OBJECT:
    case STT_FUNC:
    case STT_SECTION:
        n.n_type = N_SECT;
        break;
    case STT_FILE:
        n.n_type = N_ABS;
        break;
    default:
        错误_打印("unhandled ELF symbol type %d %s",
                  ELFW(ST_TYPE)(sym->st_info), name);
    }
    if (sym->st_shndx == SHN_UNDEF)
      错误_打印("should have been rewritten to SHN_FROMDLL: %s", name);
    else if (sym->st_shndx == SHN_FROMDLL)
      n.n_type = N_UNDF, n.n_sect = 0;
    else if (sym->st_shndx == SHN_ABS)
      n.n_type = N_ABS, n.n_sect = 0;
    else if (sym->st_shndx >= SHN_LORESERVE)
      错误_打印("unhandled ELF symbol section %d %s", sym->st_shndx, name);
    else if (!mo->elfsectomacho[sym->st_shndx])
      错误_打印("ELF section %d not mapped into Mach-O for symbol %s",
                sym->st_shndx, name);
    else
      n.n_sect = mo->elfsectomacho[sym->st_shndx];
    if (ELFW(ST_BIND)(sym->st_info) == STB_GLOBAL)
      n.n_type |=  N_EXT;
    else if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK)
      n.n_desc |= N_WEAK_REF | (n.n_type != N_UNDF ? N_WEAK_DEF : 0);
    n.n_strx = pn->n_strx;
    n.n_value = sym->st_value;
    *pn = n;
}

static void convert_symbols(知心状态机 *状态机1, struct macho *mo)
{
    struct nlist_64 *pn;
    for_each_elem(mo->全局单词表副本, 0, pn, struct nlist_64)
        convert_symbol(状态机1, mo, pn);
}

static int machosymcmp(const void *_a, const void *_b)
{
    知心状态机 *状态机1 = zhi_状态;
    int ea = ((struct nlist_64 *)_a)->n_value;
    int eb = ((struct nlist_64 *)_b)->n_value;
    ELF符号 *sa = (ELF符号 *)单词表_部分->data + ea;
    ELF符号 *sb = (ELF符号 *)单词表_部分->data + eb;
    int r;
    /* locals, then defined externals, then undefined externals, the
       last two 段数 also by name, otherwise stable sort */
    r = (ELFW(ST_BIND)(sb->st_info) == STB_LOCAL)
        - (ELFW(ST_BIND)(sa->st_info) == STB_LOCAL);
    if (r)
      return r;
    r = (sa->st_shndx == SHN_UNDEF) - (sb->st_shndx == SHN_UNDEF);
    if (r)
      return r;
    if (ELFW(ST_BIND)(sa->st_info) != STB_LOCAL) {
        const char * na = (char*)单词表_部分->link->data + sa->st_name;
        const char * nb = (char*)单词表_部分->link->data + sb->st_name;
        r = strcmp(na, nb);
        if (r)
          return r;
    }
    return ea - eb;
}

static void create_symtab(知心状态机 *状态机1, struct macho *mo)
{
    int sym_index, sym_end;
    struct nlist_64 *pn;

    /* Stub creation belongs to check_relocs, but we need to create
       the symbol now, so its included in the sorting.  */
    mo->stubs = 创建_节(状态机1, "__stubs", SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR);
    mo->stubsym = 处理_elf_符号(状态机1->全局单词表副本, 0, 0,
                              ELFW(ST_INFO)(STB_LOCAL, STT_SECTION), 0,
                              mo->stubs->sh_num, ".__stubs");

    mo->全局单词表副本 = 创建_节(状态机1, "LESYMTAB", SHT_LINKEDIT, SHF_ALLOC | SHF_WRITE);
    mo->字符表 = 创建_节(状态机1, "LESTRTAB", SHT_LINKEDIT, SHF_ALLOC | SHF_WRITE);
    处理_elf_字符串(mo->字符表, " "); /* Mach-O starts 字符表 with a space */
    sym_end = 单词表_部分->数据_偏移 / sizeof(ElfW(符号));
    pn = 段_ptr_添加(mo->全局单词表副本, sizeof(*pn) * (sym_end - 1));
    for (sym_index = 1; sym_index < sym_end; ++sym_index) {
        ElfW(符号) *sym = (ElfW(符号) *)单词表_部分->data + sym_index;
        const char *name = (char*)单词表_部分->link->data + sym->st_name;
        pn[sym_index - 1].n_strx = 处理_elf_字符串(mo->字符表, name);
        pn[sym_index - 1].n_value = sym_index;
    }
    进入状态机(状态机1);  /* qsort needs global state */
    qsort(pn, sym_end - 1, sizeof(*pn), machosymcmp);
    离开状态机();
    mo->e2msym = 内存_申请(sym_end * sizeof(*mo->e2msym));
    mo->e2msym[0] = -1;
    for (sym_index = 1; sym_index < sym_end; ++sym_index) {
        mo->e2msym[pn[sym_index - 1].n_value] = sym_index - 1;
    }
}

struct {
    int seg;
    uint32_t flags;
    char *name;
} skinfo[sk_last] = {
    [sk_text] =         { 1, S_REGULAR | S_ATTR_PURE_INSTRUCTIONS
                             | S_ATTR_SOME_INSTRUCTIONS, "__text" },
    [sk_ro_data] =      { 1, S_REGULAR, "__rodata" },
    [sk_nl_ptr] =       { 2, S_NON_LAZY_SYMBOL_POINTERS, "__got" },
    [sk_init] =         { 2, S_MOD_INIT_函数_POINTERS, "__mod_init_func" },
    [sk_fini] =         { 2, S_MOD_TERM_函数_POINTERS, "__mod_term_func" },
    [sk_rw_data] =      { 2, S_REGULAR, "__data" },
    [sk_bss] =          { 2, S_ZEROFILL, "__bss" },
    [sk_linkedit] =     { 3, S_REGULAR, NULL },
};

static void collect_sections(知心状态机 *状态机1, struct macho *mo)
{
    int i, sk, numsec;
    uint64_t curaddr, fileofs;
    段 *s;
    struct segment_command_64 *seg = NULL;
    struct dylinker_command *dyldlc;
    struct symtab_command *symlc;
    struct dysymtab_command *dysymlc;
    char *str;

    seg = add_segment(mo, "__PAGEZERO");
    seg->vmsize = (uint64_t)1 << 32;

    seg = add_segment(mo, "__TEXT");
    seg->vmaddr = (uint64_t)1 << 32;
    seg->maxprot = 7;  // rwx
    seg->initprot = 5; // r-x

    seg = add_segment(mo, "__DATA");
    seg->vmaddr = -1;
    seg->maxprot = 7;  // rwx
    seg->initprot = 3; // rw-

    seg = add_segment(mo, "__LINKEDIT");
    seg->vmaddr = -1;
    seg->maxprot = 7;  // rwx
    seg->initprot = 1; // r--

    mo->ep = add_lc(mo, LC_MAIN, sizeof(*mo->ep));
    mo->ep->entryoff = 4096;

    i = (sizeof(*dyldlc) + strlen("/usr/lib/dyld") + 1 + 7) &-8;
    dyldlc = add_lc(mo, LC_LOAD_DYLINKER, i);
    dyldlc->name = sizeof(*dyldlc);
    str = (char*)dyldlc + dyldlc->name;
    strcpy(str, "/usr/lib/dyld");

    symlc = add_lc(mo, LC_SYMTAB, sizeof(*symlc));
    dysymlc = add_lc(mo, LC_DYSYMTAB, sizeof(*dysymlc));

    for(i = 0; i < 状态机1->数量_已加载的_dll数组; i++) {
        DLL参考 *dllref = 状态机1->已加载的_dll数组[i];
        if (dllref->level == 0)
          add_dylib(mo, dllref->name);
    }

    /* dyld requires a writable segment with classic Mach-O, but it ignores
       zero-sized segments for this, so force to have some data.  */
    段_ptr_添加(初始化数据_部分, 1);
    memset (mo->sk_to_sect, 0, sizeof(mo->sk_to_sect));
    for (i = 状态机1->数量_段数; i-- > 1;) {
        int type, flags;
        s = 状态机1->段数[i];
        type = s->sh_type;
        flags = s->sh_flags;
        sk = sk_unknown;
        if (flags & SHF_ALLOC) {
            switch (type) {
            default:           sk = sk_unknown; break;
            case SHT_INIT_ARRAY: sk = sk_init; break;
            case SHT_FINI_ARRAY: sk = sk_fini; break;
            case SHT_NOBITS:   sk = sk_bss; break;
            case SHT_SYMTAB:   sk = sk_discard; break;
            case SHT_STRTAB:   sk = s == 单词表字符串_段 ? sk_ro_data : sk_discard; break;
            case SHT_RELX:     sk = sk_discard; break;
            case SHT_LINKEDIT: sk = sk_linkedit; break;
            case SHT_PROGBITS:
                if (s == 状态机1->got)
                  sk = sk_nl_ptr;
                else if (flags & SHF_EXECINSTR)
                  sk = sk_text;
                else if (flags & SHF_WRITE)
                  sk = sk_rw_data;
                else
                  sk = sk_ro_data;
                break;
            }
        } else
          sk = sk_discard;
        s->prev = mo->sk_to_sect[sk].s;
        mo->sk_to_sect[sk].s = s;
    }
    fileofs = 4096;  /* leave space for mach-o headers */
    curaddr = get_segment(mo, 1)->vmaddr;
    curaddr += 4096;
    seg = NULL;
    numsec = 0;
    mo->elfsectomacho = 内存_初始化(sizeof(*mo->elfsectomacho) * 状态机1->数量_段数);
    for (sk = sk_unknown; sk < sk_last; sk++) {
        struct 段_64 *sec = NULL;
        if (seg) {
            seg->vmsize = curaddr - seg->vmaddr;
            seg->filesize = fileofs - seg->fileoff;
        }
        if (skinfo[sk].seg && mo->sk_to_sect[sk].s) {
            uint64_t al = 0;
            int si;
            seg = get_segment(mo, skinfo[sk].seg);
            if (skinfo[sk].name) {
                si = add_section(mo, &seg, skinfo[sk].name);
                numsec++;
                mo->lc[mo->seg2lc[skinfo[sk].seg]] = (struct load_command*)seg;
                mo->sk_to_sect[sk].machosect = si;
                sec = get_section(seg, si);
                sec->flags = skinfo[sk].flags;
            }
            if (seg->vmaddr == -1) {
                curaddr = (curaddr + 4095) & -4096;
                seg->vmaddr = curaddr;
                fileofs = (fileofs + 4095) & -4096;
                seg->fileoff = fileofs;
            }

            for (s = mo->sk_to_sect[sk].s; s; s = s->prev) {
                int a = 精确_对数2p1(s->sh_addralign);
                if (a && al < (a - 1))
                  al = a - 1;
                s->sh_size = s->数据_偏移;
            }
            if (sec)
              sec->align = al;
            al = 1U << al;
            if (al > 4096)
              zhi_警告("alignment > 4096"), sec->align = 12, al = 4096;
            curaddr = (curaddr + al - 1) & -al;
            fileofs = (fileofs + al - 1) & -al;
            if (sec) {
                sec->addr = curaddr;
                sec->offset = fileofs;
            }
            for (s = mo->sk_to_sect[sk].s; s; s = s->prev) {
                al = s->sh_addralign;
                curaddr = (curaddr + al - 1) & -al;
                dprintf("curaddr now 0x%llx\n", curaddr);
                s->sh_addr = curaddr;
                curaddr += s->sh_size;
                if (s->sh_type != SHT_NOBITS) {
                    fileofs = (fileofs + al - 1) & -al;
                    s->sh_偏移 = fileofs;
                    fileofs += s->sh_size;
                    dprintf("fileofs now %lld\n", fileofs);
                }
                if (sec)
                  mo->elfsectomacho[s->sh_num] = numsec;
            }
            if (sec)
              sec->size = curaddr - sec->addr;
        }
        if (DEBUG_MACHO)
          for (s = mo->sk_to_sect[sk].s; s; s = s->prev) {
              int type = s->sh_type;
              int flags = s->sh_flags;
              printf("%d section %-16s %-10s %09llx %04x %02d %s,%s,%s\n",
                     sk,
                     s->name,
                     type == SHT_PROGBITS ? "progbits" :
                     type == SHT_NOBITS ? "nobits" :
                     type == SHT_SYMTAB ? "全局单词表副本" :
                     type == SHT_STRTAB ? "字符表" :
                     type == SHT_INIT_ARRAY ? "init" :
                     type == SHT_FINI_ARRAY ? "fini" :
                     type == SHT_RELX ? "rel" : "???",
                     s->sh_addr,
                     (unsigned)s->数据_偏移,
                     s->sh_addralign,
                     flags & SHF_ALLOC ? "alloc" : "",
                     flags & SHF_WRITE ? "write" : "",
                     flags & SHF_EXECINSTR ? "exec" : ""
                    );
          }
    }
    if (seg) {
        seg->vmsize = curaddr - seg->vmaddr;
        seg->filesize = fileofs - seg->fileoff;
    }

    /* Fill 全局单词表副本 info */
    symlc->symoff = mo->全局单词表副本->sh_偏移;
    symlc->nsyms = mo->全局单词表副本->数据_偏移 / sizeof(struct nlist_64);
    symlc->stroff = mo->字符表->sh_偏移;
    symlc->strsize = mo->字符表->数据_偏移;

    dysymlc->iundefsym = mo->iundef == -1 ? symlc->nsyms : mo->iundef;
    dysymlc->iextdefsym = mo->iextdef == -1 ? dysymlc->iundefsym : mo->iextdef;
    dysymlc->ilocalsym = mo->ilocal == -1 ? dysymlc->iextdefsym : mo->ilocal;
    dysymlc->nlocalsym = dysymlc->iextdefsym - dysymlc->ilocalsym;
    dysymlc->nextdefsym = dysymlc->iundefsym - dysymlc->iextdefsym;
    dysymlc->nundefsym = symlc->nsyms - dysymlc->iundefsym;
    dysymlc->间接的ectsymoff = mo->间接的syms->sh_偏移;
    dysymlc->n间接的ectsyms = mo->间接的syms->数据_偏移 / sizeof(uint32_t);
}

static void macho_write(知心状态机 *状态机1, struct macho *mo, FILE *fp)
{
    int i, sk;
    uint64_t fileofs = 0;
    段 *s;
    mo->mh.mh.magic = MH_MAGIC_64;
    mo->mh.mh.cputype = 0x1000007;    // x86_64
    mo->mh.mh.cpusubtype = 0x80000003;// all | CPU_SUBTYPE_LIB64
    mo->mh.mh.文件类型 = 2;           // MH_EXECUTE
    mo->mh.mh.flags = 4;              // DYLDLINK
    mo->mh.mh.ncmds = mo->nlc;
    mo->mh.mh.sizeofcmds = 0;
    for (i = 0; i < mo->nlc; i++)
      mo->mh.mh.sizeofcmds += mo->lc[i]->cmdsize;

    fwrite(&mo->mh, 1, sizeof(mo->mh), fp);
    fileofs += sizeof(mo->mh);
    for (i = 0; i < mo->nlc; i++) {
        fwrite(mo->lc[i], 1, mo->lc[i]->cmdsize, fp);
        fileofs += mo->lc[i]->cmdsize;
    }

    for (sk = sk_unknown; sk < sk_last; sk++) {
        struct segment_command_64 *seg;
        if (!skinfo[sk].seg || !mo->sk_to_sect[sk].s)
          continue;
        seg = get_segment(mo, skinfo[sk].seg);
        for (s = mo->sk_to_sect[sk].s; s; s = s->prev) {
            if (s->sh_type != SHT_NOBITS) {
                while (fileofs < s->sh_偏移)
                  fputc(0, fp), fileofs++;
                if (s->sh_size) {
                    fwrite(s->data, 1, s->sh_size, fp);
                    fileofs += s->sh_size;
                }
            }
        }
    }
}

静态_函数 int macho_输出_文件(知心状态机 *状态机1, const char *文件名)
{
    int fd, mode, file_type;
    FILE *fp;
    int i, ret = -1;
    struct macho mo = {};

    file_type = 状态机1->输出_类型;
    if (file_type == ZHI_输出_目标文件)
        mode = 0666;
    else
        mode = 0777;
    unlink(文件名);
    fd = open(文件名, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, mode);
    if (fd < 0) {
        zhi_错误_不中止("could not write '%s: %s'", 文件名, strerror(errno));
        return -1;
    }
    fp = fdopen(fd, "wb");
    if (状态机1->显示信息)
        printf("<- %s\n", 文件名);

    zhi_添加_运行时(状态机1);
    解决_常见_符号(状态机1);
    create_symtab(状态机1, &mo);
    check_relocs(状态机1, &mo);
    ret = check_symbols(状态机1, &mo);
    if (!ret) {
        int i;
        段 *s;
        collect_sections(状态机1, &mo);
        重定位_符号(状态机1, 状态机1->全局单词表副本, 0);
        mo.ep->entryoff = 获取_符号_地址(状态机1, "_main", 1, 0)
                            - get_segment(&mo, 1)->vmaddr;
        if (状态机1->数量_错误)
          goto do_ret;

        for(i = 1; i < 状态机1->数量_段数; i++) {
            s = 状态机1->段数[i];
            if (s->重定位)
              重定位_段(状态机1, s);
        }
        convert_symbols(状态机1, &mo);

        macho_write(状态机1, &mo, fp);
    }
    ret = 0;

 do_ret:
    for (i = 0; i < mo.nlc; i++)
      内存_释放(mo.lc[i]);
    内存_释放(mo.lc);
    内存_释放(mo.elfsectomacho);
    内存_释放(mo.e2msym);

    fclose(fp);
    return ret;
}

static uint32_t swap32(uint32_t x)
{
  return (x >> 24) | (x << 24) | ((x >> 8) & 0xff00) | ((x & 0xff00) << 8);
}
#define SWAP(x) (swap ? swap32(x) : (x))

静态_函数 int macho_加载_dll(知心状态机 *状态机1, int fd, const char *文件名, int lev)
{
    unsigned char buf[sizeof(struct mach_header_64)];
    void *buf2;
    uint32_t machofs = 0;
    struct fat_header fh;
    struct mach_header mh;
    struct load_command *lc;
    int i, swap = 0;
    const char *基本名称 = 文件名;
    struct nlist_64 *全局单词表副本 = 0;
    uint32_t nsyms = 0;
    char *字符表 = 0;
    uint32_t strsize = 0;
    uint32_t iextdef = 0;
    uint32_t nextdef = 0;
    DLL参考 *dllref;

  again:
    if (全_读取(fd, buf, sizeof(buf)) != sizeof(buf))
      return -1;
    memcpy(&fh, buf, sizeof(fh));
    if (fh.magic == FAT_MAGIC || fh.magic == FAT_CIGAM) {
        struct fat_arch *fa = 加载_数据(fd, sizeof(fh),
                                        fh.nfat_arch * sizeof(*fa));
        swap = fh.magic == FAT_CIGAM;
        for (i = 0; i < SWAP(fh.nfat_arch); i++)
          if (SWAP(fa[i].cputype) == 0x01000007 /* CPU_TYPE_X86_64 */
              && SWAP(fa[i].cpusubtype) == 3)   /* CPU_SUBTYPE_X86_ALL */
            break;
        if (i == SWAP(fh.nfat_arch)) {
            内存_释放(fa);
            return -1;
        }
        machofs = SWAP(fa[i].offset);
        内存_释放(fa);
        lseek(fd, machofs, SEEK_SET);
        goto again;
    } else if (fh.magic == FAT_MAGIC_64 || fh.magic == FAT_CIGAM_64) {
        zhi_警告("%s: Mach-O fat 64bit 文件数 of type 0x%x not handled",
                    文件名, fh.magic);
        return -1;
    }

    memcpy(&mh, buf, sizeof(mh));
    if (mh.magic != MH_MAGIC_64)
      return -1;
    dprintf("found Mach-O at %d\n", machofs);
    buf2 = 加载_数据(fd, machofs + sizeof(struct mach_header_64), mh.sizeofcmds);
    for (i = 0, lc = buf2; i < mh.ncmds; i++) {
        dprintf("lc %2d: 0x%08x\n", i, lc->cmd);
        switch (lc->cmd) {
        case LC_SYMTAB:
        {
            struct symtab_command *sc = (struct symtab_command*)lc;
            nsyms = sc->nsyms;
            全局单词表副本 = 加载_数据(fd, machofs + sc->symoff, nsyms * sizeof(*全局单词表副本));
            strsize = sc->strsize;
            字符表 = 加载_数据(fd, machofs + sc->stroff, strsize);
            break;
        }
        case LC_ID_DYLIB:
        {
            struct dylib_command *dc = (struct dylib_command*)lc;
            基本名称 = (char*)lc + dc->name;
            dprintf(" ID_DYLIB %d 0x%x 0x%x %s\n",
                    dc->timestamp, dc->current_version,
                    dc->compatibility_version, 基本名称);
            break;
        }
        case LC_REEXPORT_DYLIB:
        {
            struct dylib_command *dc = (struct dylib_command*)lc;
            char *name = (char*)lc + dc->name;
            dprintf(" REEXPORT %s\n", name);
            int subfd = open(name, O_RDONLY | O_BINARY);
            if (subfd < 0)
              zhi_警告("can't open %s (reexported from %s)", name, 文件名);
            else {
                /* Hopefully the REEXPORTs never form a cycle, we don't check
                   for that!  */
                macho_加载_dll(状态机1, subfd, name, lev + 1);
                close(subfd);
            }
            break;
        }
        case LC_DYSYMTAB:
        {
            struct dysymtab_command *dc = (struct dysymtab_command*)lc;
            iextdef = dc->iextdefsym;
            nextdef = dc->nextdefsym;
            break;
        }
        }
        lc = (struct load_command*) ((char*)lc + lc->cmdsize);
    }

    /* if the dll is already loaded, do not 加载 it */
    for(i = 0; i < 状态机1->数量_已加载的_dll数组; i++) {
        dllref = 状态机1->已加载的_dll数组[i];
        if (!strcmp(基本名称, dllref->name)) {
            /* but update level if needed */
            if (lev < dllref->level)
                dllref->level = lev;
            goto the_end;
        }
    }
    dllref = 内存_初始化(sizeof(DLL参考) + strlen(基本名称));
    dllref->level = lev;
    strcpy(dllref->name, 基本名称);
    动态数组_追加元素(&状态机1->已加载的_dll数组, &状态机1->数量_已加载的_dll数组, dllref);

    if (!nsyms || !nextdef)
      zhi_警告("%s doesn't export any symbols?", 文件名);

    //dprintf("symbols (all):\n");
    dprintf("symbols (exported):\n");
    dprintf("    n: typ sec   desc              value name\n");
    //for (i = 0; i < nsyms; i++) {
    for (i = iextdef; i < iextdef + nextdef; i++) {
        struct nlist_64 *sym = 全局单词表副本 + i;
        dprintf("%5d: %3d %3d 0x%04x 0x%016llx %s\n",
                i, sym->n_type, sym->n_sect, sym->n_desc, sym->n_value,
                字符表 + sym->n_strx);
        设置_elf_符号(状态机1->动态单词表_部分, 0, 0,
                    ELFW(ST_INFO)(STB_GLOBAL, STT_NOTYPE),
                    0, SHN_UNDEF, 字符表 + sym->n_strx);
    }

  the_end:
    内存_释放(字符表);
    内存_释放(全局单词表副本);
    内存_释放(buf2);
    return 0;
}
