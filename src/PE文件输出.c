/*
 *  ZHIPE.C-ZHI编译器的PE文件输出。PE是windows下的可执行文件,elf是Linux下的.
 *PE文件的全称是Portable Executable，意为可移植的可执行的文件，常见的EXE、DLL、OCX、SYS、COM都是PE文件，
 *PE文件的全称是Portable PE文件是微软Windows操作系统上的程序文件（可能是间接被执行，如DLL）
 */

#include "zhi.h"

#define PE_合并_数据
/* #define PE_PRINT_SECTIONS */

#ifndef _WIN32
#define stricmp strcasecmp
#define strnicmp strncasecmp
#include <sys/stat.h> /* chmod() */
#endif

#ifdef ZHI_TARGET_X86_64
# define ADDR3264 ULONGLONG
# define PE_IMAGE_REL IMAGE_REL_BASED_DIR64
# define REL_类型_直接 R_X86_64_64
# define R_XXX_THUNKFIX R_X86_64_PC32
# define R_XXX_RELATIVE R_X86_64_RELATIVE
# define IMAGE_FILE_MACHINE 0x8664
# define RSRC_RELTYPE 3

#elif defined ZHI_TARGET_ARM
# define ADDR3264 DWORD
# define PE_IMAGE_REL IMAGE_REL_BASED_HIGHLOW
# define REL_类型_直接 R_ARM_ABS32
# define R_XXX_THUNKFIX R_ARM_ABS32
# define R_XXX_RELATIVE R_ARM_RELATIVE
# define IMAGE_FILE_MACHINE 0x01C0
# define RSRC_RELTYPE 7 /* ??? (not tested) */

#elif defined ZHI_TARGET_I386
# define ADDR3264 DWORD
# define PE_IMAGE_REL IMAGE_REL_BASED_HIGHLOW
# define REL_类型_直接 R_386_32
# define R_XXX_THUNKFIX R_386_32
# define R_XXX_RELATIVE R_386_RELATIVE
# define IMAGE_FILE_MACHINE 0x014C
# define RSRC_RELTYPE 7 /* DIR32NB */

#endif

#ifndef IMAGE_NT_SIGNATURE
/* ----------------------------------------------------------- */
/* 以下定义来自winnt.h */

typedef unsigned char BYTE;          //8位类型
typedef unsigned short WORD;         //16位类型
typedef unsigned int DWORD;          //32位类型
typedef unsigned long long ULONGLONG;//64位类型
#pragma pack(push, 1)

typedef struct _IMAGE_DOS_HEADER {  /* DOS .EXE header */
    WORD e_magic;         /* Magic number */
    WORD e_cblp;          /* Bytes on last page of file */
    WORD e_cp;            /* Pages in file */
    WORD e_crlc;          /* Relocations */
    WORD e_cparhdr;       /* Size of header in paragraphs */
    WORD e_minalloc;      /* Minimum extra paragraphs needed */
    WORD e_maxalloc;      /* Maximum extra paragraphs needed */
    WORD e_ss;            /* Initial (relative) SS value */
    WORD e_sp;            /* Initial SP value */
    WORD e_csum;          /* Checksum */
    WORD e_ip;            /* Initial IP value */
    WORD e_cs;            /* Initial (relative) CS value */
    WORD e_lfarlc;        /* File address of relocation table */
    WORD e_ovno;          /* Overlay number */
    WORD e_res[4];        /* Reserved words */
    WORD e_oemid;         /* OEM identifier (for e_oeminfo) */
    WORD e_oeminfo;       /* OEM information; e_oemid specific */
    WORD e_res2[10];      /* Reserved words */
    DWORD e_lfanew;        /* File address of new exe header */
} IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;

#define IMAGE_NT_SIGNATURE  0x00004550  /* PE00 */
#define SIZE_OF_NT_SIGNATURE 4

typedef struct _IMAGE_FILE_HEADER {
    WORD    Machine;                   /*标识目标机器类型的数字*/
    WORD    NumberOfSections;          /*节的树数目，windows加载器限制最大数目为96.*/
    DWORD   TimeDateStamp;             /*文件何时被创建*/
    DWORD   PointerToSymbolTable;      /*符号表的文件偏移*/
    DWORD   NumberOfSymbols;           /*符号表中的元素数目*/
    WORD    SizeOfOptionalHeader;      /*可选文件头的大小*/
    WORD    Characteristics;           /*指示文件属性的标志*/
} IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;


#define IMAGE_SIZEOF_FILE_HEADER 20

typedef struct _IMAGE_DATA_DIRECTORY {
    DWORD   VirtualAddress;
    DWORD   Size;
} IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;


typedef struct _IMAGE_OPTIONAL_HEADER {
    /* Standard fields. */
    WORD    Magic;
    BYTE    MajorLinkerVersion;
    BYTE    MinorLinkerVersion;
    DWORD   SizeOfCode;
    DWORD   SizeOfInitializedData;
    DWORD   SizeOfUninitializedData;
    DWORD   AddressOfEntryPoint;
    DWORD   BaseOfCode;
#ifndef ZHI_TARGET_X86_64
    DWORD   BaseOfData;
#endif
    /* NT additional fields. */
    ADDR3264 ImageBase;
    DWORD   SectionAlignment;
    DWORD   FileAlignment;
    WORD    MajorOperatingSystemVersion;
    WORD    MinorOperatingSystemVersion;
    WORD    MajorImageVersion;
    WORD    MinorImageVersion;
    WORD    MajorSubsystemVersion;
    WORD    MinorSubsystemVersion;
    DWORD   Win32VersionValue;
    DWORD   SizeOfImage;
    DWORD   SizeOfHeaders;
    DWORD   CheckSum;
    WORD    Subsystem;
    WORD    DllCharacteristics;
    ADDR3264 SizeOfStackReserve;
    ADDR3264 SizeOfStackCommit;
    ADDR3264 SizeOfHeapReserve;
    ADDR3264 SizeOfHeapCommit;
    DWORD   LoaderFlags;
    DWORD   NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[16];
} IMAGE_OPTIONAL_HEADER32, IMAGE_OPTIONAL_HEADER64, IMAGE_OPTIONAL_HEADER;

#define IMAGE_DIRECTORY_ENTRY_EXPORT          0   /* Export Directory */
#define IMAGE_DIRECTORY_ENTRY_IMPORT          1   /* Import Directory */
#define IMAGE_DIRECTORY_ENTRY_RESOURCE        2   /* Resource Directory */
#define IMAGE_DIRECTORY_ENTRY_EXCEPTION       3   /* Exception Directory */
#define IMAGE_DIRECTORY_ENTRY_SECURITY        4   /* Security Directory */
#define IMAGE_DIRECTORY_ENTRY_BASERELOC       5   /* Base Relocation Table */
#define IMAGE_DIRECTORY_ENTRY_DEBUG           6   /* Debug Directory */
/*      IMAGE_DIRECTORY_ENTRY_COPYRIGHT       7      (X86 usage) */
#define IMAGE_DIRECTORY_ENTRY_ARCHITECTURE    7   /* Architecture Specific Data */
#define IMAGE_DIRECTORY_ENTRY_GLOBALPTR       8   /* RVA(它是相对于文件加载起始地址的一个偏移值) of GP */
#define IMAGE_DIRECTORY_ENTRY_TLS             9   /* TLS Directory */
#define IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG    10   /* Load Configuration Directory */
#define IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT   11   /* Bound Import Directory in headers */
#define IMAGE_DIRECTORY_ENTRY_IAT            12   /* Import Address Table */
#define IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT   13   /* Delay Load Import Descriptors */
#define IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR 14   /* COM Runtime descriptor */

/* 段 header format. */
#define IMAGE_SIZEOF_SHORT_NAME         8

typedef struct _IMAGE_SECTION_HEADER {
    BYTE    Name[IMAGE_SIZEOF_SHORT_NAME];
    union {
            DWORD   PhysicalAddress;
            DWORD   VirtualSize;
    } Misc;
    DWORD   VirtualAddress;
    DWORD   SizeOfRawData;
    DWORD   PointerToRawData;
    DWORD   PointerToRelocations;
    DWORD   PointerToLinenumbers;
    WORD    NumberOfRelocations;
    WORD    NumberOfLinenumbers;
    DWORD   Characteristics;
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;

#define IMAGE_SIZEOF_SECTION_HEADER     40

typedef struct _IMAGE_EXPORT_DIRECTORY {
    DWORD Characteristics;
    DWORD TimeDateStamp;
    WORD MajorVersion;
    WORD MinorVersion;
    DWORD Name;
    DWORD Base;
    DWORD NumberOfFunctions;
    DWORD NumberOfNames;
    DWORD AddressOfFunctions;
    DWORD AddressOfNames;
    DWORD AddressOfNameOrdinals;
} IMAGE_EXPORT_DIRECTORY,*PIMAGE_EXPORT_DIRECTORY;

typedef struct _IMAGE_IMPORT_DESCRIPTOR {
    union {
        DWORD Characteristics;
        DWORD OriginalFirstThunk;
    };
    DWORD TimeDateStamp;
    DWORD ForwarderChain;
    DWORD Name;
    DWORD FirstThunk;
} IMAGE_IMPORT_DESCRIPTOR;

typedef struct _IMAGE_BASE_RELOCATION {
    DWORD   VirtualAddress;
    DWORD   SizeOfBlock;
//  WORD    TypeOffset[1];
} IMAGE_BASE_RELOCATION;

#define IMAGE_SIZEOF_BASE_RELOCATION     8

#define IMAGE_REL_BASED_ABSOLUTE         0
#define IMAGE_REL_BASED_HIGH             1
#define IMAGE_REL_BASED_LOW              2
#define IMAGE_REL_BASED_HIGHLOW          3
#define IMAGE_REL_BASED_HIGHADJ          4
#define IMAGE_REL_BASED_MIPS_JMPADDR     5
#define IMAGE_REL_BASED_SECTION          6
#define IMAGE_REL_BASED_REL32            7
#define IMAGE_REL_BASED_DIR64           10

#define IMAGE_SCN_CNT_CODE                  0x00000020
#define IMAGE_SCN_CNT_INITIALIZED_DATA      0x00000040
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA    0x00000080
#define IMAGE_SCN_MEM_DISCARDABLE           0x02000000
#define IMAGE_SCN_MEM_SHARED                0x10000000
#define IMAGE_SCN_MEM_EXECUTE               0x20000000
#define IMAGE_SCN_MEM_READ                  0x40000000
#define IMAGE_SCN_MEM_WRITE                 0x80000000

#pragma pack(pop)

/* ----------------------------------------------------------- */
#endif /* ndef IMAGE_NT_SIGNATURE */
/* ----------------------------------------------------------- */

#ifndef IMAGE_REL_BASED_DIR64
# define IMAGE_REL_BASED_DIR64 10
#endif

#pragma pack(push, 1)
struct pe_头
{
    IMAGE_DOS_HEADER doshdr;
    BYTE dosstub[0x40];
    DWORD nt_sig;
    IMAGE_FILE_HEADER 文件头;
#ifdef ZHI_TARGET_X86_64
    IMAGE_OPTIONAL_HEADER64 opthdr;
#else
#ifdef _WIN64
    IMAGE_OPTIONAL_HEADER32 opthdr;
#else
    IMAGE_OPTIONAL_HEADER opthdr;
#endif
#endif
};

struct pe_重定位_头 {
    DWORD offset;
    DWORD size;
};

struct pe_rsrc_header {
    struct _IMAGE_FILE_HEADER 文件头;
    struct _IMAGE_SECTION_HEADER sectionhdr;
};

struct pe_rsrc_reloc {
    DWORD offset;
    DWORD size;
    WORD type;
};
#pragma pack(pop)

/* ------------------------------------------------------------- */
/* 内部临时结构 */

enum {
    sec_text = 0,
    sec_data ,
    sec_bss ,
    sec_idata ,
    sec_pdata ,
    sec_other ,
    sec_rsrc ,
    sec_stab ,
    sec_stabstr ,
    sec_reloc ,
    sec_last
};

#if 0
static const DWORD pe_sec_flags[] = {
    0x60000020, /* ".text"     , */
    0xC0000040, /* ".data"     , */
    0xC0000080, /* ".bss"      , */
    0x40000040, /* ".idata"    , */
    0x40000040, /* ".pdata"    , */
    0xE0000060, /* < other >   , */
    0x40000040, /* ".rsrc"     , */
    0x42000802, /* ".stab"     , */
    0x42000040, /* ".重定位"    , */
};
#endif

struct 段_信息 {
    int cls;
    char name[32];
    DWORD sh_addr;
    DWORD sh_size;
    DWORD pe_flags;
    段 *sec;
    DWORD data_size;
    IMAGE_SECTION_HEADER ish;
};

struct 导入_信符号 {
    int sym_index;
    int iat_index;
    int thk_offset;
};

struct pe_导入_信息 {
    int dll_index;
    int sym_count;
    struct 导入_信符号 **symbols;
};

struct pe_信息 {
    知心状态机 *状态机1;
    段 *重定位;
    段 *thunk;
    const char *文件名;
    int type;
    DWORD sizeofheaders;
    ADDR3264 imagebase;
    const char *start_symbol;
    DWORD start_addr;
    DWORD imp_offs;
    DWORD imp_size;
    DWORD iat_offs;
    DWORD iat_size;
    DWORD exp_offs;
    DWORD exp_size;
    int subsystem;
    DWORD 分段_对齐;
    DWORD file_align;
    struct 段_信息 **sec_info;
    int sec_count;
    struct pe_导入_信息 **imp_info;
    int imp_count;
};

#define PE_NUL 0
#define PE_DLL 1
#define PE_GUI 2
#define PE_EXE 3
#define PE_RUN 4

/* --------------------------------------------*/

static const char *pe_导出_名称(知心状态机 *状态机1, ElfW(符号) *sym)
{
    const char *name = (char*)单词表_部分->link->data + sym->st_name;
    if (状态机1->前导_下划线 && name[0] == '_' && !(sym->st_other & ST_PE_标准调用))
        return name + 1;
    return name;
}

static int pe_查找_导入(知心状态机 * 状态机1, ElfW(符号) *sym)
{
    char buffer[200];
    const char *s, *p;
    int sym_index = 0, n = 0;
    int a, err = 0;

    do {
        s = pe_导出_名称(状态机1, sym);
        a = 0;
        if (n) {
            /* second try: */
	    if (sym->st_other & ST_PE_标准调用) {
                /* try w/0 stdcall deco (windows API convention) */
	        p = strrchr(s, '@');
	        if (!p || s[0] != '_')
	            break;
	        strcpy(buffer, s+1)[p-s-1] = 0;
	    } else if (s[0] != '_') { /* try non-ansi function */
	        buffer[0] = '_', strcpy(buffer + 1, s);
	    } else if (0 == memcmp(s, "__imp_", 6)) { /* mingw 2.0 */
	        strcpy(buffer, s + 6), a = 1;
	    } else if (0 == memcmp(s, "_imp__", 6)) { /* mingw 3.7 */
	        strcpy(buffer, s + 6), a = 1;
	    } else {
	        continue;
	    }
	    s = buffer;
        }
        sym_index = 查找_elf_符号(状态机1->动态单词表_部分, s);
        // printf("find (%d) %d %s\n", n, sym_index, s);
        if (sym_index
            && ELFW(ST_TYPE)(sym->st_info) == STT_OBJECT
            && 0 == (sym->st_other & ST_PE_IMPORT)
            && 0 == a
            ) err = -1, sym_index = 0;
    } while (0 == sym_index && ++n < 2);
    return n == 2 ? err : sym_index;
}

/*----------------------------------------------------------------------------*/

static int 动态数组_assoc(void **pp, int n, int key)
{
    int i;
    for (i = 0; i < n; ++i, ++pp)
    if (key == **(int **) pp)
        return i;
    return -1;
}

static DWORD 返回小值(DWORD a, DWORD b)
{
    return a < b ? a : b;
}

static DWORD 返回大值(DWORD a, DWORD b)
{
    return a < b ? b : a;
}

static DWORD pe_文件_对齐(struct pe_信息 *pe, DWORD n)
{
    return (n + (pe->file_align - 1)) & ~(pe->file_align - 1);
}

static DWORD pe_虚拟_对齐(struct pe_信息 *pe, DWORD n)
{
    return (n + (pe->分段_对齐 - 1)) & ~(pe->分段_对齐 - 1);
}

static void pe_对齐_段(段 *s, int a)
{
    int i = s->数据_偏移 & (a-1);
    if (i)
        段_ptr_添加(s, a - i);
}

static void pe_设置_数据目录(struct pe_头 *hdr, int dir, DWORD addr, DWORD size)
{
    hdr->opthdr.DataDirectory[dir].VirtualAddress = addr;
    hdr->opthdr.DataDirectory[dir].Size = size;
}

struct pe_文件 {
    FILE *op;
    DWORD sum;
    unsigned pos;
};

static int pe_f写(void *data, int len, struct pe_文件 *pf)
{
    WORD *p = data;
    DWORD sum;
    int ret, i;
    pf->pos += (ret = fwrite(data, 1, len, pf->op));
    sum = pf->sum;
    for (i = len; i > 0; i -= 2) {
        sum += (i >= 2) ? *p++ : *(BYTE*)p;
        sum = (sum + (sum >> 16)) & 0xFFFF;
    }
    pf->sum = sum;
    return len == ret ? 0 : -1;
}

static void pe_fpad(struct pe_文件 *pf, DWORD new_pos)
{
    char buf[256];
    int n, diff = new_pos - pf->pos;
    memset(buf, 0, sizeof buf);
    while (diff > 0) {
        diff -= n = 返回小值(diff, sizeof buf);
        fwrite(buf, n, 1, pf->op);
    }
    pf->pos = new_pos;
}

/*----------------------------------------------------------------------------*/
static int pe_写(struct pe_信息 *pe)
{
    static const struct pe_头 pe_template = {
    {
    /* IMAGE_DOS_HEADER doshdr */
    0x5A4D, /*WORD e_magic;         Magic number */
    0x0090, /*WORD e_cblp;          Bytes on last page of file */
    0x0003, /*WORD e_cp;            Pages in file */
    0x0000, /*WORD e_crlc;          Relocations */

    0x0004, /*WORD e_cparhdr;       Size of header in paragraphs */
    0x0000, /*WORD e_minalloc;      Minimum extra paragraphs needed */
    0xFFFF, /*WORD e_maxalloc;      Maximum extra paragraphs needed */
    0x0000, /*WORD e_ss;            Initial (relative) SS value */

    0x00B8, /*WORD e_sp;            Initial SP value */
    0x0000, /*WORD e_csum;          Checksum */
    0x0000, /*WORD e_ip;            Initial IP value */
    0x0000, /*WORD e_cs;            Initial (relative) CS value */
    0x0040, /*WORD e_lfarlc;        File address of relocation table */
    0x0000, /*WORD e_ovno;          Overlay number */
    {0,0,0,0}, /*WORD e_res[4];     Reserved words */
    0x0000, /*WORD e_oemid;         OEM identifier (for e_oeminfo) */
    0x0000, /*WORD e_oeminfo;       OEM information; e_oemid specific */
    {0,0,0,0,0,0,0,0,0,0}, /*WORD e_res2[10];      Reserved words */
    0x00000080  /*DWORD   e_lfanew;        File address of new exe header */
    },{
    /* BYTE dosstub[0x40] */
    /* 14 code bytes + "This program cannot be run in DOS mode.\r\r\n$" + 6 * 0x00 */
    0x0e,0x1f,0xba,0x0e,0x00,0xb4,0x09,0xcd,0x21,0xb8,0x01,0x4c,0xcd,0x21,0x54,0x68,
    0x69,0x73,0x20,0x70,0x72,0x6f,0x67,0x72,0x61,0x6d,0x20,0x63,0x61,0x6e,0x6e,0x6f,
    0x74,0x20,0x62,0x65,0x20,0x72,0x75,0x6e,0x20,0x69,0x6e,0x20,0x44,0x4f,0x53,0x20,
    0x6d,0x6f,0x64,0x65,0x2e,0x0d,0x0d,0x0a,0x24,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    },
    0x00004550, /* DWORD nt_sig = IMAGE_NT_SIGNATURE */
    {
    /* IMAGE_FILE_HEADER 文件头 */
    IMAGE_FILE_MACHINE, /*WORD    Machine; */
    0x0003, /*WORD    NumberOfSections; */
    0x00000000, /*DWORD   TimeDateStamp; */
    0x00000000, /*DWORD   PointerToSymbolTable; */
    0x00000000, /*DWORD   NumberOfSymbols; */
#if defined(ZHI_TARGET_X86_64)
    0x00F0, /*WORD    SizeOfOptionalHeader; */
    0x022F  /*WORD    Characteristics; */
#define CHARACTERISTICS_DLL 0x222E
#elif defined(ZHI_TARGET_I386)
    0x00E0, /*WORD    SizeOfOptionalHeader; */
    0x030F  /*WORD    Characteristics; */
#define CHARACTERISTICS_DLL 0x230E
#elif defined(ZHI_TARGET_ARM)
    0x00E0, /*WORD    SizeOfOptionalHeader; */
    0x010F, /*WORD    Characteristics; */
#define CHARACTERISTICS_DLL 0x230F
#endif
},{
    /* IMAGE_OPTIONAL_HEADER opthdr */
    /* Standard fields. */
#ifdef ZHI_TARGET_X86_64
    0x020B, /*WORD    Magic; */
#else
    0x010B, /*WORD    Magic; */
#endif
    0x06, /*BYTE    MajorLinkerVersion; */
    0x00, /*BYTE    MinorLinkerVersion; */
    0x00000000, /*DWORD   SizeOfCode; */
    0x00000000, /*DWORD   SizeOfInitializedData; */
    0x00000000, /*DWORD   SizeOfUninitializedData; */
    0x00000000, /*DWORD   AddressOfEntryPoint; */
    0x00000000, /*DWORD   BaseOfCode; */
#ifndef ZHI_TARGET_X86_64
    0x00000000, /*DWORD   BaseOfData; */
#endif
    /* NT additional fields. */
#if defined(ZHI_TARGET_ARM)
    0x00100000,	    /*DWORD   ImageBase; */
#else
    0x00400000,	    /*DWORD   ImageBase; */
#endif
    0x00001000, /*DWORD   SectionAlignment; */
    0x00000200, /*DWORD   FileAlignment; */
    0x0004, /*WORD    MajorOperatingSystemVersion; */
    0x0000, /*WORD    MinorOperatingSystemVersion; */
    0x0000, /*WORD    MajorImageVersion; */
    0x0000, /*WORD    MinorImageVersion; */
    0x0004, /*WORD    MajorSubsystemVersion; */
    0x0000, /*WORD    MinorSubsystemVersion; */
    0x00000000, /*DWORD   Win32VersionValue; */
    0x00000000, /*DWORD   SizeOfImage; */
    0x00000200, /*DWORD   SizeOfHeaders; */
    0x00000000, /*DWORD   CheckSum; */
    0x0002, /*WORD    Subsystem; */
    0x0000, /*WORD    DllCharacteristics; */
    0x00100000, /*DWORD   SizeOfStackReserve; */
    0x00001000, /*DWORD   SizeOfStackCommit; */
    0x00100000, /*DWORD   SizeOfHeapReserve; */
    0x00001000, /*DWORD   SizeOfHeapCommit; */
    0x00000000, /*DWORD   LoaderFlags; */
    0x00000010, /*DWORD   NumberOfRvaAndSizes; */

    /* IMAGE_DATA_DIRECTORY DataDirectory[16]; */
    {{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
     {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}}
    }};

    struct pe_头 pe_头 = pe_template;

    int i;
    struct pe_文件 pf = {0};
    DWORD file_offset;
    struct 段_信息 *si;
    IMAGE_SECTION_HEADER *psh;
    知心状态机 *状态机1 = pe->状态机1;

    pf.op = fopen(pe->文件名, "wb");
    if (NULL == pf.op) {
        zhi_错误_不中止("could not write '%s': %s", pe->文件名, strerror(errno));
        return -1;
    }

    pe->sizeofheaders = pe_文件_对齐(pe,
        sizeof (struct pe_头)
        + pe->sec_count * sizeof (IMAGE_SECTION_HEADER)
        );

    file_offset = pe->sizeofheaders;

    if (2 == pe->状态机1->显示信息)
        printf("-------------------------------"
               "\n  virt   file   size  section" "\n");
    for (i = 0; i < pe->sec_count; ++i) {
        DWORD addr, size;
        const char *sh_name;

        si = pe->sec_info[i];
        sh_name = si->name;
        addr = si->sh_addr - pe->imagebase;
        size = si->sh_size;
        psh = &si->ish;

        if (2 == pe->状态机1->显示信息)
            printf("%6x %6x %6x  %s\n",
                (unsigned)addr, (unsigned)file_offset, (unsigned)size, sh_name);

        switch (si->cls) {
            case sec_text:
                if (!pe_头.opthdr.BaseOfCode)
                    pe_头.opthdr.BaseOfCode = addr;
                break;

            case sec_data:
#ifndef ZHI_TARGET_X86_64
                if (!pe_头.opthdr.BaseOfData)
                    pe_头.opthdr.BaseOfData = addr;
#endif
                break;

            case sec_bss:
                break;

            case sec_reloc:
                pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_BASERELOC, addr, size);
                break;

            case sec_rsrc:
                pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_RESOURCE, addr, size);
                break;

            case sec_pdata:
                pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_EXCEPTION, addr, size);
                break;
        }

        if (pe->imp_size) {
            pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_IMPORT,
                pe->imp_offs, pe->imp_size);
            pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_IAT,
                pe->iat_offs, pe->iat_size);
        }
        if (pe->exp_size) {
            pe_设置_数据目录(&pe_头, IMAGE_DIRECTORY_ENTRY_EXPORT,
                pe->exp_offs, pe->exp_size);
        }

        memcpy(psh->Name, sh_name, 返回小值(strlen(sh_name), sizeof psh->Name));

        psh->Characteristics = si->pe_flags;
        psh->VirtualAddress = addr;
        psh->Misc.VirtualSize = size;
        pe_头.opthdr.SizeOfImage =
            返回大值(pe_虚拟_对齐(pe, size + addr), pe_头.opthdr.SizeOfImage);

        if (si->data_size) {
            psh->PointerToRawData = file_offset;
            file_offset = pe_文件_对齐(pe, file_offset + si->data_size);
            psh->SizeOfRawData = file_offset - psh->PointerToRawData;
            if (si->cls == sec_text)
                pe_头.opthdr.SizeOfCode += psh->SizeOfRawData;
            else
                pe_头.opthdr.SizeOfInitializedData += psh->SizeOfRawData;
        }
    }

    //pe_头.文件头.TimeDateStamp = time(NULL);
    pe_头.文件头.NumberOfSections = pe->sec_count;
    pe_头.opthdr.AddressOfEntryPoint = pe->start_addr;
    pe_头.opthdr.SizeOfHeaders = pe->sizeofheaders;
    pe_头.opthdr.ImageBase = pe->imagebase;
    pe_头.opthdr.Subsystem = pe->subsystem;
    if (pe->状态机1->pe_堆栈_大小)
        pe_头.opthdr.SizeOfStackReserve = pe->状态机1->pe_堆栈_大小;
    if (PE_DLL == pe->type)
        pe_头.文件头.Characteristics = CHARACTERISTICS_DLL;
    pe_头.文件头.Characteristics |= pe->状态机1->pe_特征;

    pe_f写(&pe_头, sizeof pe_头, &pf);
    for (i = 0; i < pe->sec_count; ++i)
        pe_f写(&pe->sec_info[i]->ish, sizeof(IMAGE_SECTION_HEADER), &pf);

    file_offset = pe->sizeofheaders;
    for (i = 0; i < pe->sec_count; ++i) {
        段 *s;
        si = pe->sec_info[i];
        for (s = si->sec; s; s = s->prev) {
            pe_fpad(&pf, file_offset);
            pe_f写(s->data, s->数据_偏移, &pf);
            if (s->prev)
                file_offset += s->prev->sh_addr - s->sh_addr;
        }
        file_offset = si->ish.PointerToRawData + si->ish.SizeOfRawData;
        pe_fpad(&pf, file_offset);
    }

    pf.sum += file_offset;
    fseek(pf.op, offsetof(struct pe_头, opthdr.CheckSum), SEEK_SET);
    pe_f写(&pf.sum, sizeof (DWORD), &pf);

    fclose (pf.op);
#ifndef _WIN32
    chmod(pe->文件名, 0777);
#endif

    if (2 == pe->状态机1->显示信息)
        printf("-------------------------------\n");
    if (pe->状态机1->显示信息)
        printf("<- %s (%u bytes)\n", pe->文件名, (unsigned)file_offset);

    return 0;
}

/*----------------------------------------------------------------------------*/

static struct 导入_信符号 *pe_添加_导入(struct pe_信息 *pe, int sym_index)
{
    int i;
    int dll_index;
    struct pe_导入_信息 *p;
    struct 导入_信符号 *s;
    ElfW(符号) *isym;

    isym = (ElfW(符号) *)pe->状态机1->动态单词表_部分->data + sym_index;
    dll_index = isym->st_size;

    i = 动态数组_assoc ((void**)pe->imp_info, pe->imp_count, dll_index);
    if (-1 != i) {
        p = pe->imp_info[i];
        goto found_dll;
    }
    p = 内存_初始化(sizeof *p);
    p->dll_index = dll_index;
    动态数组_追加元素(&pe->imp_info, &pe->imp_count, p);

found_dll:
    i = 动态数组_assoc ((void**)p->symbols, p->sym_count, sym_index);
    if (-1 != i)
        return p->symbols[i];

    s = 内存_初始化(sizeof *s);
    动态数组_追加元素(&p->symbols, &p->sym_count, s);
    s->sym_index = sym_index;
    return s;
}

void pe_释放_导入(struct pe_信息 *pe)
{
    int i;
    for (i = 0; i < pe->imp_count; ++i) {
        struct pe_导入_信息 *p = pe->imp_info[i];
        动态数组_重分配容量(&p->symbols, &p->sym_count);
    }
    动态数组_重分配容量(&pe->imp_info, &pe->imp_count);
}

/*----------------------------------------------------------------------------*/
static void pe_构建_导入(struct pe_信息 *pe)
{
    int thk_ptr, ent_ptr, dll_ptr, sym_cnt, i;
    DWORD rva_base = pe->thunk->sh_addr - pe->imagebase;
    int ndlls = pe->imp_count;
    知心状态机 *状态机1 = pe->状态机1;

    for (sym_cnt = i = 0; i < ndlls; ++i)
        sym_cnt += pe->imp_info[i]->sym_count;

    if (0 == sym_cnt)
        return;

    pe_对齐_段(pe->thunk, 16);
    pe->imp_size = (ndlls + 1) * sizeof(IMAGE_IMPORT_DESCRIPTOR);
    pe->iat_size = (sym_cnt + ndlls) * sizeof(ADDR3264);
    dll_ptr = pe->thunk->数据_偏移;
    thk_ptr = dll_ptr + pe->imp_size;
    ent_ptr = thk_ptr + pe->iat_size;
    pe->imp_offs = dll_ptr + rva_base;
    pe->iat_offs = thk_ptr + rva_base;
    段_ptr_添加(pe->thunk, pe->imp_size + 2*pe->iat_size);

    for (i = 0; i < pe->imp_count; ++i) {
        IMAGE_IMPORT_DESCRIPTOR *hdr;
        int k, n, dllindex;
        ADDR3264 v;
        struct pe_导入_信息 *p = pe->imp_info[i];
        const char *name;
        DLL参考 *dllref;

        dllindex = p->dll_index;
        if (dllindex)
            name = (dllref = pe->状态机1->已加载的_dll数组[dllindex-1])->name;
        else
            name = "", dllref = NULL;

        /* put the dll name into the import header */
        v = 处理_elf_字符串(pe->thunk, name);
        hdr = (IMAGE_IMPORT_DESCRIPTOR*)(pe->thunk->data + dll_ptr);
        hdr->FirstThunk = thk_ptr + rva_base;
        hdr->OriginalFirstThunk = ent_ptr + rva_base;
        hdr->Name = v + rva_base;

        for (k = 0, n = p->sym_count; k <= n; ++k) {
            if (k < n) {
                int iat_index = p->symbols[k]->iat_index;
                int sym_index = p->symbols[k]->sym_index;
                ElfW(符号) *imp_sym = (ElfW(符号) *)pe->状态机1->动态单词表_部分->data + sym_index;
                ElfW(符号) *org_sym = (ElfW(符号) *)单词表_部分->data + iat_index;
                const char *name = (char*)pe->状态机1->动态单词表_部分->link->data + imp_sym->st_name;
                int ordinal;

                org_sym->st_value = thk_ptr;
                org_sym->st_shndx = pe->thunk->sh_num;

                if (dllref)
                    v = 0, ordinal = imp_sym->st_value; /* ordinal from pe_加载_def */
                else
                    ordinal = 0, v = imp_sym->st_value; /* address from 在已编译的程序中添加符号() */

#ifdef ZHI_是_本机
                if (pe->type == PE_RUN) {
                    if (dllref) {
                        if ( !dllref->handle )
                            dllref->handle = LoadLibrary(dllref->name);
                        v = (ADDR3264)GetProcAddress(dllref->handle, ordinal?(char*)0+ordinal:name);
                    }
                    if (!v)
                        zhi_错误_不中止("could not resolve symbol '%s'", name);
                } else
#endif
                if (ordinal) {
                    v = ordinal | (ADDR3264)1 << (sizeof(ADDR3264)*8 - 1);
                } else {
                    v = pe->thunk->数据_偏移 + rva_base;
                    段_ptr_添加(pe->thunk, sizeof(WORD)); /* hint, not used */
                    处理_elf_字符串(pe->thunk, name);
                }

            } else {
                v = 0; /* last entry is zero */
            }

            *(ADDR3264*)(pe->thunk->data+thk_ptr) =
            *(ADDR3264*)(pe->thunk->data+ent_ptr) = v;
            thk_ptr += sizeof (ADDR3264);
            ent_ptr += sizeof (ADDR3264);
        }
        dll_ptr += sizeof(IMAGE_IMPORT_DESCRIPTOR);
    }
}

/* ------------------------------------------------------------- */

struct pe_排序_符号
{
    int index;
    const char *name;
};

static int 符号_比较(const void *va, const void *vb)
{
    const char *ca = (*(struct pe_排序_符号**)va)->name;
    const char *cb = (*(struct pe_排序_符号**)vb)->name;
    return strcmp(ca, cb);
}

static void pe_构建_导出(struct pe_信息 *pe)
{
    ElfW(符号) *sym;
    int sym_index, sym_end;
    DWORD rva_base, base_o, func_o, name_o, ord_o, str_o;
    IMAGE_EXPORT_DIRECTORY *hdr;
    int sym_count, ord;
    struct pe_排序_符号 **sorted, *p;
    知心状态机 *状态机1 = pe->状态机1;

    FILE *op;
    char buf[260];
    const char *dllname;
    const char *name;

    rva_base = pe->thunk->sh_addr - pe->imagebase;
    sym_count = 0, sorted = NULL, op = NULL;

    sym_end = 单词表_部分->数据_偏移 / sizeof(ElfW(符号));
    for (sym_index = 1; sym_index < sym_end; ++sym_index) {
        sym = (ElfW(符号)*)单词表_部分->data + sym_index;
        name = pe_导出_名称(pe->状态机1, sym);
        if ((sym->st_other & ST_PE_EXPORT)
            /* export only symbols from actually written 段数 */
            && pe->状态机1->段数[sym->st_shndx]->sh_addr) {
            p = 内存_申请(sizeof *p);
            p->index = sym_index;
            p->name = name;
            动态数组_追加元素(&sorted, &sym_count, p);
        }
#if 0
        if (sym->st_other & ST_PE_EXPORT)
            printf("export: %s\n", name);
        if (sym->st_other & ST_PE_标准调用)
            printf("stdcall: %s\n", name);
#endif
    }

    if (0 == sym_count)
        return;

    qsort (sorted, sym_count, sizeof *sorted, 符号_比较);

    pe_对齐_段(pe->thunk, 16);
    dllname = 取_文件基本名(pe->文件名);

    base_o = pe->thunk->数据_偏移;
    func_o = base_o + sizeof(IMAGE_EXPORT_DIRECTORY);
    name_o = func_o + sym_count * sizeof (DWORD);
    ord_o = name_o + sym_count * sizeof (DWORD);
    str_o = ord_o + sym_count * sizeof(WORD);

    hdr = 段_ptr_添加(pe->thunk, str_o - base_o);
    hdr->Characteristics        = 0;
    hdr->Base                   = 1;
    hdr->NumberOfFunctions      = sym_count;
    hdr->NumberOfNames          = sym_count;
    hdr->AddressOfFunctions     = func_o + rva_base;
    hdr->AddressOfNames         = name_o + rva_base;
    hdr->AddressOfNameOrdinals  = ord_o + rva_base;
    hdr->Name                   = str_o + rva_base;
    处理_elf_字符串(pe->thunk, dllname);

#if 1
    /* automatically write exports to <output-文件名>.def */
    p字符串复制(buf, sizeof buf, pe->文件名);
    strcpy(取_文件扩展名(buf), ".def");
    op = fopen(buf, "wb");
    if (NULL == op) {
        zhi_错误_不中止("could not create '%s': %s", buf, strerror(errno));
    } else {
        fprintf(op, "LIBRARY %s\n\nEXPORTS\n", dllname);
        if (pe->状态机1->显示信息)
            printf("<- %s (%d symbol%s)\n", buf, sym_count, &"s"[sym_count < 2]);
    }
#endif

    for (ord = 0; ord < sym_count; ++ord)
    {
        p = sorted[ord], sym_index = p->index, name = p->name;
        /* insert actual address later in 重定位_段() */
        使_elf_重定位(单词表_部分, pe->thunk,
            func_o, R_XXX_RELATIVE, sym_index);
        *(DWORD*)(pe->thunk->data + name_o)
            = pe->thunk->数据_偏移 + rva_base;
        *(WORD*)(pe->thunk->data + ord_o)
            = ord;
        处理_elf_字符串(pe->thunk, name);
        func_o += sizeof (DWORD);
        name_o += sizeof (DWORD);
        ord_o += sizeof (WORD);
        if (op)
            fprintf(op, "%s\n", name);
    }

    pe->exp_offs = base_o + rva_base;
    pe->exp_size = pe->thunk->数据_偏移 - base_o;
    动态数组_重分配容量(&sorted, &sym_count);
    if (op)
        fclose(op);
}

/* ------------------------------------------------------------- */
static void pe_建立_重定位 (struct pe_信息 *pe)
{
    DWORD offset, block_ptr, sh_addr, addr;
    int count, i;
    ElfW_Rel *rel, *rel_end;
    段 *s = NULL, *sr;
    struct pe_重定位_头 *hdr;

    sh_addr = offset = block_ptr = count = i = 0;
    rel = rel_end = NULL;

    for(;;) {
        if (rel < rel_end) {
            int type = ELFW(R_TYPE)(rel->r_info);
            addr = rel->r_offset + sh_addr;
            ++ rel;
            if (type != REL_类型_直接)
                continue;
            if (count == 0) { /* new 块 */
                block_ptr = pe->重定位->数据_偏移;
                段_ptr_添加(pe->重定位, sizeof(struct pe_重定位_头));
                offset = addr & 0xFFFFFFFF<<12;
            }
            if ((addr -= offset)  < (1<<12)) { /* one 块 spans 4k addresses */
                WORD *wp = 段_ptr_添加(pe->重定位, sizeof (WORD));
                *wp = addr | PE_IMAGE_REL<<12;
                ++count;
                continue;
            }
            -- rel;

        } else if (s) {
            sr = s->重定位;
            if (sr) {
                rel = (ElfW_Rel *)sr->data;
                rel_end = (ElfW_Rel *)(sr->data + sr->数据_偏移);
                sh_addr = s->sh_addr;
            }
            s = s->prev;
            continue;

        } else if (i < pe->sec_count) {
            s = pe->sec_info[i]->sec, ++i;
            continue;

        } else if (!count)
            break;

        /* fill the last 块 and ready for a new one */
        if (count & 1) /* align for DWORDS */
            段_ptr_添加(pe->重定位, sizeof(WORD)), ++count;
        hdr = (struct pe_重定位_头 *)(pe->重定位->data + block_ptr);
        hdr -> offset = offset - pe->imagebase;
        hdr -> size = count * sizeof(WORD) + sizeof(struct pe_重定位_头);
        count = 0;
    }
}

/* ------------------------------------------------------------- */
static int pe_段_类(段 *s)
{
    int type, flags;
    const char *name;

    type = s->sh_type;
    flags = s->sh_flags;
    name = s->name;
    if (flags & SHF_ALLOC) {
        if (type == SHT_PROGBITS) {
            if (flags & SHF_EXECINSTR)
                return sec_text;
            if (flags & SHF_WRITE)
                return sec_data;
            if (0 == strcmp(name, ".rsrc"))
                return sec_rsrc;
            if (0 == strcmp(name, ".iedat"))
                return sec_idata;
            if (0 == strcmp(name, ".pdata"))
                return sec_pdata;
        } else if (type == SHT_NOBITS) {
            if (flags & SHF_WRITE)
                return sec_bss;
        }
    } else {
        if (0 == strcmp(name, ".重定位"))
            return sec_reloc;
    }
    if (0 == memcmp(name, ".stab", 5))
        return name[5] ? sec_stabstr : sec_stab;
    if (flags & SHF_ALLOC)
        return sec_other;
    return -1;
}

static int pe_分配_地址 (struct pe_信息 *pe)
{
    int i, k, o, c;
    DWORD addr;
    int *段_order;
    struct 段_信息 *si;
    段 *s;

    if (PE_DLL == pe->type)
        pe->重定位 = 创建_节(pe->状态机1, ".重定位", SHT_PROGBITS, 0);
    // pe->thunk = 创建_节(pe->状态机1, ".iedat", SHT_PROGBITS, SHF_ALLOC);

    段_order = 内存_申请(pe->状态机1->数量_段数 * sizeof (int));
    for (o = k = 0 ; k < sec_last; ++k) {
        for (i = 1; i < pe->状态机1->数量_段数; ++i) {
            s = pe->状态机1->段数[i];
            if (k == pe_段_类(s))
                段_order[o++] = i;
        }
    }

    si = NULL;
    addr = pe->imagebase + 1;

    for (i = 0; i < o; ++i) {
        k = 段_order[i];
        s = pe->状态机1->段数[k];
        c = pe_段_类(s);

        if ((c == sec_stab || c == sec_stabstr) && 0 == pe->状态机1->执行_调试)
            continue;

#ifdef PE_合并_数据
        if (c == sec_bss)
            c = sec_data;
#endif
        if (si && c == si->cls) {
            /* merge with previous section */
            s->sh_addr = addr = ((addr - 1) | (16 - 1)) + 1;
        } else {
            si = NULL;
            s->sh_addr = addr = pe_虚拟_对齐(pe, addr);
        }

        if (c == sec_data && NULL == pe->thunk)
            pe->thunk = s;

        if (s == pe->thunk) {
            pe_构建_导入(pe);
            pe_构建_导出(pe);
        }
        if (s == pe->重定位)
            pe_建立_重定位 (pe);

        if (0 == s->数据_偏移)
            continue;

        if (si)
            goto add_section;

        si = 内存_初始化(sizeof *si);
        动态数组_追加元素(&pe->sec_info, &pe->sec_count, si);

        strcpy(si->name, s->name);
        si->cls = c;
        si->sh_addr = addr;

        si->pe_flags = IMAGE_SCN_MEM_READ;
        if (s->sh_flags & SHF_EXECINSTR)
            si->pe_flags |= IMAGE_SCN_MEM_EXECUTE | IMAGE_SCN_CNT_CODE;
        else if (s->sh_type == SHT_NOBITS)
            si->pe_flags |= IMAGE_SCN_CNT_UNINITIALIZED_DATA;
        else
            si->pe_flags |= IMAGE_SCN_CNT_INITIALIZED_DATA;
        if (s->sh_flags & SHF_WRITE)
            si->pe_flags |= IMAGE_SCN_MEM_WRITE;
        if (0 == (s->sh_flags & SHF_ALLOC))
            si->pe_flags |= IMAGE_SCN_MEM_DISCARDABLE;

add_section:
        addr += s->数据_偏移;
        si->sh_size = addr - si->sh_addr;
        if (s->sh_type != SHT_NOBITS) {
            段 **ps = &si->sec;
            while (*ps)
                ps = &(*ps)->prev;
            *ps = s, s->prev = NULL;
            si->data_size = si->sh_size;
        }
        //printf("%08x %05x %08x %s\n", si->sh_addr, si->sh_size, si->pe_flags, s->name);
    }
    内存_释放(段_order);
#if 0
    for (i = 1; i < pe->状态机1->数量_段数; ++i) {
        段 *s = pe->状态机1->段数[i];
        int type = s->sh_type;
        int flags = s->sh_flags;
        printf("section %-16s %-10s %08x %04x %s,%s,%s\n",
            s->name,
            type == SHT_PROGBITS ? "progbits" :
            type == SHT_NOBITS ? "nobits" :
            type == SHT_SYMTAB ? "全局单词表副本" :
            type == SHT_STRTAB ? "字符表" :
            type == SHT_RELX ? "rel" : "???",
            s->sh_addr,
            s->数据_偏移,
            flags & SHF_ALLOC ? "alloc" : "",
            flags & SHF_WRITE ? "write" : "",
            flags & SHF_EXECINSTR ? "exec" : ""
            );
    }
    pe->状态机1->显示信息 = 2;
#endif
    return 0;
}

/*----------------------------------------------------------------------------*/

static int pe_是一个函数(知心状态机 *状态机1, int sym_index)
{
    段 *sr = 生成代码_段->重定位;
    ElfW_Rel *rel, *rel_end;
    Elf32_无符32位数 info = ELF32_R_INFO(sym_index, R_386_PC32);
    if (!sr)
        return 0;
    rel_end = (ElfW_Rel *)(sr->data + sr->数据_偏移);
    for (rel = (ElfW_Rel *)sr->data; rel < rel_end; rel++)
        if (rel->r_info == info)
            return 1;
    return 0;
}

/*----------------------------------------------------------------------------*/
static int pe_检查_符号(struct pe_信息 *pe)
{
    ElfW(符号) *sym;
    int sym_index, sym_end;
    int ret = 0;
    知心状态机 *状态机1 = pe->状态机1;

    pe_对齐_段(生成代码_段, 8);

    sym_end = 单词表_部分->数据_偏移 / sizeof(ElfW(符号));
    for (sym_index = 1; sym_index < sym_end; ++sym_index) {

        sym = (ElfW(符号) *)单词表_部分->data + sym_index;
        if (sym->st_shndx == SHN_UNDEF) {

            const char *name = (char*)单词表_部分->link->data + sym->st_name;
            unsigned type = ELFW(ST_TYPE)(sym->st_info);
            int imp_sym = pe_查找_导入(pe->状态机1, sym);
            struct 导入_信符号 *is;

            if (imp_sym <= 0)
                goto not_found;

            if (type == STT_NOTYPE) {
                /* symbols from assembler have no type, find out which */
                if (pe_是一个函数(状态机1, sym_index))
                    type = STT_FUNC;
                else
                    type = STT_OBJECT;
            }

            is = pe_添加_导入(pe, imp_sym);

            if (type == STT_FUNC) {
                unsigned long offset = is->thk_offset;
                if (offset) {
                    /* got aliased symbol, like stricmp and _stricmp */

                } else {
                    char buffer[100];
                    WORD *p;

                    offset = 生成代码_段->数据_偏移;
                    /* add the 'jmp IAT[x]' instruction */
#ifdef ZHI_TARGET_ARM
                    p = 段_ptr_添加(生成代码_段, 8+4); // room for code and address
                    (*(DWORD*)(p)) = 0xE59FC000; // arm code ldr ip, [pc] ; PC+8+0 = 0001xxxx
                    (*(DWORD*)(p+2)) = 0xE59CF000; // arm code ldr pc, [ip]
#else
                    p = 段_ptr_添加(生成代码_段, 8);
                    *p = 0x25FF;
#ifdef ZHI_TARGET_X86_64
                    *(DWORD*)(p+1) = (DWORD)-4;
#endif
#endif
                    /* add a helper symbol, will be patched later in
                       pe_构建_导入 */
                    sprintf(buffer, "IAT.%s", name);
                    is->iat_index = 处理_elf_符号(
                        单词表_部分, 0, sizeof(DWORD),
                        ELFW(ST_INFO)(STB_GLOBAL, STT_OBJECT),
                        0, SHN_UNDEF, buffer);
#ifdef ZHI_TARGET_ARM
                    使_elf_重定位(单词表_部分, 生成代码_段,
                        offset + 8, R_XXX_THUNKFIX, is->iat_index); // offset to IAT position
#else
                    使_elf_重定位(单词表_部分, 生成代码_段, 
                        offset + 2, R_XXX_THUNKFIX, is->iat_index);
#endif
                    is->thk_offset = offset;
                }

                /* 内存_重分配容量 might have altered sym's address */
                sym = (ElfW(符号) *)单词表_部分->data + sym_index;

                /* patch the original symbol */
                sym->st_value = offset;
                sym->st_shndx = 生成代码_段->sh_num;
                sym->st_other &= ~ST_PE_EXPORT; /* do not export */
                continue;
            }

            if (type == STT_OBJECT) { /* data, ptr to that should be */
                if (0 == is->iat_index) {
                    /* original symbol will be patched later in pe_构建_导入 */
                    is->iat_index = sym_index;
                    continue;
                }
            }

        not_found:
            if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK)
                /* STB_WEAK undefined symbols are accepted */
                continue;
            zhi_错误_不中止("undefined symbol '%s'%s", name,
                imp_sym < 0 ? ", missing __declspec(dllimport)?":"");
            ret = -1;

        } else if (pe->状态机1->导出所有符号
                   && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            /* if -导出所有符号 option, then export all non local symbols */
            sym->st_other |= ST_PE_EXPORT;
        }
    }
    return ret;
}

/*----------------------------------------------------------------------------*/
#ifdef PE_PRINT_SECTIONS
static void pe_打印_段(FILE * f, 段 * s)
{
    /* just if you're curious */
    BYTE *p, *e, b;
    int i, n, l, m;
    p = s->data;
    e = s->data + s->数据_偏移;
    l = e - p;

    fprintf(f, "section  \"%s\"", s->name);
    if (s->link)
        fprintf(f, "\nlink     \"%s\"", s->link->name);
    if (s->重定位)
        fprintf(f, "\nreloc    \"%s\"", s->重定位->name);
    fprintf(f, "\nv_addr   %08X", (unsigned)s->sh_addr);
    fprintf(f, "\ncontents %08X", (unsigned)l);
    fprintf(f, "\n\n");

    if (s->sh_type == SHT_NOBITS)
        return;

    if (0 == l)
        return;

    if (s->sh_type == SHT_SYMTAB)
        m = sizeof(ElfW(符号));
    else if (s->sh_type == SHT_RELX)
        m = sizeof(ElfW_Rel);
    else
        m = 16;

    fprintf(f, "%-8s", "offset");
    for (i = 0; i < m; ++i)
        fprintf(f, " %02x", i);
    n = 56;

    if (s->sh_type == SHT_SYMTAB || s->sh_type == SHT_RELX) {
        const char *fields1[] = {
            "name",
            "value",
            "size",
            "bind",
            "type",
            "other",
            "shndx",
            NULL
        };

        const char *fields2[] = {
            "offs",
            "type",
            "symb",
            NULL
        };

        const char **p;

        if (s->sh_type == SHT_SYMTAB)
            p = fields1, n = 106;
        else
            p = fields2, n = 58;

        for (i = 0; p[i]; ++i)
            fprintf(f, "%6s", p[i]);
        fprintf(f, "  symbol");
    }

    fprintf(f, "\n");
    for (i = 0; i < n; ++i)
        fprintf(f, "-");
    fprintf(f, "\n");

    for (i = 0; i < l;)
    {
        fprintf(f, "%08X", i);
        for (n = 0; n < m; ++n) {
            if (n + i < l)
                fprintf(f, " %02X", p[i + n]);
            else
                fprintf(f, "   ");
        }

        if (s->sh_type == SHT_SYMTAB) {
            ElfW(符号) *sym = (ElfW(符号) *) (p + i);
            const char *name = s->link->data + sym->st_name;
            fprintf(f, "  %04X  %04X  %04X   %02X    %02X    %02X   %04X  \"%s\"",
                    (unsigned)sym->st_name,
                    (unsigned)sym->st_value,
                    (unsigned)sym->st_size,
                    (unsigned)ELFW(ST_BIND)(sym->st_info),
                    (unsigned)ELFW(ST_TYPE)(sym->st_info),
                    (unsigned)sym->st_other,
                    (unsigned)sym->st_shndx,
                    name);

        } else if (s->sh_type == SHT_RELX) {
            ElfW_Rel *rel = (ElfW_Rel *) (p + i);
            ElfW(符号) *sym =
                (ElfW(符号) *) s->link->data + ELFW(R_SYM)(rel->r_info);
            const char *name = s->link->link->data + sym->st_name;
            fprintf(f, "  %04X   %02X   %04X  \"%s\"",
                    (unsigned)rel->r_offset,
                    (unsigned)ELFW(R_TYPE)(rel->r_info),
                    (unsigned)ELFW(R_SYM)(rel->r_info),
                    name);
        } else {
            fprintf(f, "   ");
            for (n = 0; n < m; ++n) {
                if (n + i < l) {
                    b = p[i + n];
                    if (b < 32 || b >= 127)
                        b = '.';
                    fprintf(f, "%c", b);
                }
            }
        }
        i += m;
        fprintf(f, "\n");
    }
    fprintf(f, "\n\n");
}

static void pe_打印_段数(知心状态机 *状态机1, const char *fname)
{
    段 *s;
    FILE *f;
    int i;
    f = fopen(fname, "w");
    for (i = 1; i < 状态机1->数量_段数; ++i) {
        s = 状态机1->段数[i];
        pe_打印_段(f, s);
    }
    pe_打印_段(f, 状态机1->动态单词表_部分);
    fclose(f);
}
#endif

/* ------------------------------------------------------------- */
/* helper function for 加载/存储 to insert one more 间接的ection */

#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
静态_函数 堆栈值 *pe_获取导入(堆栈值 *sv, 堆栈值 *v2)
{
    int r2;
    if ((sv->r & (VT_值掩码|VT_符号)) != (VT_VC常量|VT_符号) || (sv->r2 != VT_VC常量))
        return sv;
    if (!sv->sym->a.dllimport)
        return sv;
    // printf("import %04x %04x %04x %s\n", sv->type.t, sv->sym->type.t, sv->r, 取_单词字符串(sv->sym->v, NULL));
    memset(v2, 0, sizeof *v2);
    v2->type.t = VT_指针;
    v2->r = VT_VC常量 | VT_符号 | VT_LVAL;
    v2->sym = sv->sym;

    r2 = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
    加载(r2, v2);
    v2->r = r2;
    if ((uint32_t)sv->c.i) {
        vpushv(v2);
        压入整数常量(sv->c.i);
        生成_整数二进制运算('+');
        *v2 = *栈顶值--;
    }
    v2->type.t = sv->type.t;
    v2->r |= sv->r & VT_LVAL;
    return v2;
}
#endif

静态_函数 int pe_导入(知心状态机 *状态机1, int dllindex, const char *name, 目标地址_类型 value)
{
    return 设置_elf_符号(
        状态机1->动态单词表_部分,
        value,
        dllindex, /* st_size */
        ELFW(ST_INFO)(STB_GLOBAL, STT_NOTYPE),
        0,
        value ? SHN_ABS : SHN_UNDEF,
        name
        );
}

static int 添加_dll引用(知心状态机 *状态机1, const char *dllname)
{
    DLL参考 *dllref;
    int i;
    for (i = 0; i < 状态机1->数量_已加载的_dll数组; ++i)
        if (0 == strcmp(状态机1->已加载的_dll数组[i]->name, dllname))
            return i + 1;
    dllref = 内存_初始化(sizeof(DLL参考) + strlen(dllname));
    strcpy(dllref->name, dllname);
    动态数组_追加元素(&状态机1->已加载的_dll数组, &状态机1->数量_已加载的_dll数组, dllref);
    return 状态机1->数量_已加载的_dll数组;
}

/* ------------------------------------------------------------- */

static int 读_内存(int fd, unsigned offset, void *buffer, unsigned len)
{
    lseek(fd, offset, SEEK_SET);
    return len == read(fd, buffer, len);
}

/* ------------------------------------------------------------- */

公共_函数 int zhi_获取_dll导出(const char *文件名, char **pp)
{
    int l, i, n, n0, ret;
    char *p;
    int fd;

    IMAGE_SECTION_HEADER ish;
    IMAGE_EXPORT_DIRECTORY ied;
    IMAGE_DOS_HEADER dh;
    IMAGE_FILE_HEADER ih;
    DWORD sig, ref, addr, ptr, namep;

    int pef_hdroffset, opt_hdroffset, sec_hdroffset;

    n = n0 = 0;
    p = NULL;
    ret = -1;

    fd = open(文件名, O_RDONLY | O_BINARY);
    if (fd < 0)
        goto the_end_1;
    ret = 1;
    if (!读_内存(fd, 0, &dh, sizeof dh))
        goto the_end;
    if (!读_内存(fd, dh.e_lfanew, &sig, sizeof sig))
        goto the_end;
    if (sig != 0x00004550)
        goto the_end;
    pef_hdroffset = dh.e_lfanew + sizeof sig;
    if (!读_内存(fd, pef_hdroffset, &ih, sizeof ih))
        goto the_end;
    opt_hdroffset = pef_hdroffset + sizeof ih;
    if (ih.Machine == 0x014C) {
        IMAGE_OPTIONAL_HEADER32 oh;
        sec_hdroffset = opt_hdroffset + sizeof oh;
        if (!读_内存(fd, opt_hdroffset, &oh, sizeof oh))
            goto the_end;
        if (IMAGE_DIRECTORY_ENTRY_EXPORT >= oh.NumberOfRvaAndSizes)
            goto the_end_0;
        addr = oh.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
    } else if (ih.Machine == 0x8664) {
        IMAGE_OPTIONAL_HEADER64 oh;
        sec_hdroffset = opt_hdroffset + sizeof oh;
        if (!读_内存(fd, opt_hdroffset, &oh, sizeof oh))
            goto the_end;
        if (IMAGE_DIRECTORY_ENTRY_EXPORT >= oh.NumberOfRvaAndSizes)
            goto the_end_0;
        addr = oh.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
    } else
        goto the_end;

    //printf("addr: %08x\n", addr);
    for (i = 0; i < ih.NumberOfSections; ++i) {
        if (!读_内存(fd, sec_hdroffset + i * sizeof ish, &ish, sizeof ish))
            goto the_end;
        //printf("vaddr: %08x\n", ish.VirtualAddress);
        if (addr >= ish.VirtualAddress && addr < ish.VirtualAddress + ish.SizeOfRawData)
            goto found;
    }
    goto the_end_0;

found:
    ref = ish.VirtualAddress - ish.PointerToRawData;
    if (!读_内存(fd, addr - ref, &ied, sizeof ied))
        goto the_end;

    namep = ied.AddressOfNames - ref;
    for (i = 0; i < ied.NumberOfNames; ++i) {
        if (!读_内存(fd, namep, &ptr, sizeof ptr))
            goto the_end;
        namep += sizeof ptr;
        for (l = 0;;) {
            if (n+1 >= n0)
                p = 内存_重分配容量(p, n0 = n0 ? n0 * 2 : 256);
            if (!读_内存(fd, ptr - ref + l++, p + n, 1)) {
                内存_释放(p), p = NULL;
                goto the_end;
            }
            if (p[n++] == 0)
                break;
        }
    }
    if (p)
        p[n] = 0;
the_end_0:
    ret = 0;
the_end:
    close(fd);
the_end_1:
    *pp = p;
    return ret;
}

/* -------------------------------------------------------------
 *  这适用于由“ windres.exe -O coff ...”生成的“ coff”格式的已编译Windows资源。
 */

static int pe_加载_资源(知心状态机 *状态机1, int fd)
{
    struct pe_rsrc_header hdr;
    段 *rsrc_section;
    int i, ret = -1, sym_index;
    BYTE *ptr;
    unsigned offs;

    if (!读_内存(fd, 0, &hdr, sizeof hdr))
        goto quit;

    if (hdr.文件头.Machine != IMAGE_FILE_MACHINE
        || hdr.文件头.NumberOfSections != 1
        || strcmp((char*)hdr.sectionhdr.Name, ".rsrc") != 0)
        goto quit;

    rsrc_section = 创建_节(状态机1, ".rsrc", SHT_PROGBITS, SHF_ALLOC);
    ptr = 段_ptr_添加(rsrc_section, hdr.sectionhdr.SizeOfRawData);
    offs = hdr.sectionhdr.PointerToRawData;
    if (!读_内存(fd, offs, ptr, hdr.sectionhdr.SizeOfRawData))
        goto quit;
    offs = hdr.sectionhdr.PointerToRelocations;
    sym_index = 处理_elf_符号(单词表_部分, 0, 0, 0, 0, rsrc_section->sh_num, ".rsrc");
    for (i = 0; i < hdr.sectionhdr.NumberOfRelocations; ++i) {
        struct pe_rsrc_reloc rel;
        if (!读_内存(fd, offs, &rel, sizeof rel))
            goto quit;
        // printf("rsrc_reloc: %x %x %x\n", rel.offset, rel.size, rel.type);
        if (rel.type != RSRC_RELTYPE)
            goto quit;
        使_elf_重定位(单词表_部分, rsrc_section,
            rel.offset, R_XXX_RELATIVE, sym_index);
        offs += sizeof rel;
    }
    ret = 0;
quit:
    return ret;
}

/* ------------------------------------------------------------- */

static char *修剪前(char *p)
{
    while (*p && (unsigned char)*p <= ' ')
	++p;
    return p;
}

static char *修剪后(char *a, char *e)
{
    while (e > a && (unsigned char)e[-1] <= ' ')
	--e;
    *e = 0;;
    return a;
}

/* ------------------------------------------------------------- */
static int pe_加载_def(知心状态机 *状态机1, int fd)
{
    int state = 0, ret = -1, dllindex = 0, ord;
    char line[400], dllname[80], *p, *x;
    FILE *fp;

    fp = fdopen(dup(fd), "rb");
    while (fgets(line, sizeof line, fp))
    {
        p = 修剪前(修剪后(line, strchr(line, 0)));
        if (0 == *p || ';' == *p)
            continue;

        switch (state) {
        case 0:
            if (0 != strnicmp(p, "LIBRARY", 7))
                goto quit;
            p字符串复制(dllname, sizeof dllname, 修剪前(p+7));
            ++state;
            continue;

        case 1:
            if (0 != stricmp(p, "EXPORTS"))
                goto quit;
            ++state;
            continue;

        case 2:
            dllindex = 添加_dll引用(状态机1, dllname);
            ++state;
            /* fall through */
        default:
            /* get ordinal and will 存储 in sym->st_value */
            ord = 0;
            x = strchr(p, ' ');
            if (x) {
                *x = 0, x = strrchr(x + 1, '@');
                if (x) {
                    char *d;
                    ord = (int)strtol(x + 1, &d, 10);
                    if (*d)
                        ord = 0;
                }
            }
            pe_导入(状态机1, dllindex, p, ord);
            continue;
        }
    }
    ret = 0;
quit:
    fclose(fp);
    return ret;
}

/* ------------------------------------------------------------- */
static int pe_加载_dll(知心状态机 *状态机1, const char *文件名)
{
    char *p, *q;
    int index, ret;

    ret = zhi_获取_dll导出(文件名, &p);
    if (ret) {
        return -1;
    } else if (p) {
        index = 添加_dll引用(状态机1, 取_文件基本名(文件名));
        for (q = p; *q; q += 1 + strlen(q))
            pe_导入(状态机1, index, q, 0);
        内存_释放(p);
    }
    return 0;
}

/* ------------------------------------------------------------- */
静态_函数 int pe_加载_文件(知心状态机 *状态机1, const char *文件名, int fd)
{
    int ret = -1;
    char buf[10];
    if (0 == strcmp(取_文件扩展名(文件名), ".def"))
        ret = pe_加载_def(状态机1, fd);
    else if (pe_加载_资源(状态机1, fd) == 0)
        ret = 0;
    else if (读_内存(fd, 0, buf, 4) && 0 == memcmp(buf, "MZ", 2))
        ret = pe_加载_dll(状态机1, 文件名);
    return ret;
}

/* ------------------------------------------------------------- */
#ifdef ZHI_TARGET_X86_64
static unsigned pe_添加_uwwind_信息(知心状态机 *状态机1)
{
    if (NULL == 状态机1->uw_pdata) {
        状态机1->uw_pdata = 查找_段(状态机1, ".pdata");
        状态机1->uw_pdata->sh_addralign = 4;
    }
    if (0 == 状态机1->uw_sym)
        状态机1->uw_sym = 处理_elf_符号(单词表_部分, 0, 0, 0, 0, 生成代码_段->sh_num, ".uw_base");
    if (0 == 状态机1->uw_offs) {
        /* As our functions all have the same stackframe, we use one entry for all */
        static const unsigned char uw_info[] = {
            0x01, // UBYTE: 3 Version , UBYTE: 5 Flags
            0x04, // UBYTE Size of prolog
            0x02, // UBYTE Count of unwind codes
            0x05, // UBYTE: 4 Frame Register (rbp), UBYTE: 4 Frame Register offset (scaled)
            // USHORT * n Unwind codes array
            // 0x0b, 0x01, 0xff, 0xff, // stack size
            0x04, 0x03, // set frame ptr (mov rsp -> rbp)
            0x01, 0x50  // push reg (rbp)
        };

        段 *s = 生成代码_段;
        unsigned char *p;

        段_ptr_添加(s, -s->数据_偏移 & 3); /* align */
        状态机1->uw_offs = s->数据_偏移;
        p = 段_ptr_添加(s, sizeof uw_info);
        memcpy(p, uw_info, sizeof uw_info);
    }

    return 状态机1->uw_offs;
}

静态_函数 void pe_添加_uwwind_数据(unsigned start, unsigned end, unsigned stack)
{
    知心状态机 *状态机1 = zhi_状态;
    段 *pd;
    unsigned o, n, d;
    struct /* _RUNTIME_FUNCTION */ {
      DWORD BeginAddress;
      DWORD EndAddress;
      DWORD UnwindData;
    } *p;

    d = pe_添加_uwwind_信息(状态机1);
    pd = 状态机1->uw_pdata;
    o = pd->数据_偏移;
    p = 段_ptr_添加(pd, sizeof *p);

    /* record this function */
    p->BeginAddress = start;
    p->EndAddress = end;
    p->UnwindData = d;

    /* put relocations on it */
    for (n = o + sizeof *p; o < n; o += sizeof p->BeginAddress)
        使_elf_重定位(单词表_部分, pd, o, R_XXX_RELATIVE, 状态机1->uw_sym);
}
#endif
/* ------------------------------------------------------------- */
#ifdef ZHI_TARGET_X86_64
#define PE_STDSYM(n,s) n
#else
#define PE_STDSYM(n,s) "_" n s
#endif

static void zhi_增加_支持(知心状态机 *状态机1, const char *文件名)
{
    if (添加dll文件(状态机1, 文件名, 0) < 0)
        zhi_错误_不中止("找不到 %s ", 文件名);
}

static void pe_增加_运行时(知心状态机 *状态机1, struct pe_信息 *pe)
{
    const char *start_symbol;
    int pe_type = 0;
    int unicode_entry = 0;

    if (查找_elf_符号(单词表_部分, PE_STDSYM("WinMain","@16")))
        pe_type = PE_GUI;
    else
    if (查找_elf_符号(单词表_部分, PE_STDSYM("wWinMain","@16"))) {
        pe_type = PE_GUI;
        unicode_entry = PE_GUI;
    }
    else
    if (ZHI_输出_DLL == 状态机1->输出_类型) {
        pe_type = PE_DLL;
    }
    else {
        pe_type = PE_EXE;
        if (查找_elf_符号(单词表_部分, "wmain"))
            unicode_entry = PE_EXE;
    }

    start_symbol =
        ZHI_输出_内存中运行 == 状态机1->输出_类型
        ? PE_GUI == pe_type ? (unicode_entry ? "__runwwinmain" : "__runwinmain")
            : (unicode_entry ? "__runwmain" : "__runmain")
        : PE_DLL == pe_type ? PE_STDSYM("__dllstart","@12")
            : PE_GUI == pe_type ? (unicode_entry ? "__wwinstart": "__winstart")
                : (unicode_entry ? "__wstart" : "__start")
        ;

    if (!状态机1->前导_下划线 || strchr(start_symbol, '@'))
        ++start_symbol;

#ifdef ZHI_配置_记录回滚
    if (状态机1->执行_跟踪) {
#ifdef 配置_ZHI_边界检查
        if (状态机1->执行_边界_检查器 && 状态机1->输出_类型 != ZHI_输出_DLL)
            zhi_增加_支持(状态机1, "bcheck.o");
#endif
        if (状态机1->输出_类型 == ZHI_输出_EXE)
            zhi_增加_支持(状态机1, "bt-exe.o");
        if (状态机1->输出_类型 == ZHI_输出_DLL)
            zhi_增加_支持(状态机1, "bt-dll.o");
        if (状态机1->输出_类型 != ZHI_输出_DLL)
            zhi_增加_支持(状态机1, "bt-log.o");
        if (状态机1->输出_类型 != ZHI_输出_内存中运行)
            zhi_add_btstub(状态机1);
    }
#endif

    /* 从hexinku1.a获取启动代码 */
#ifdef ZHI_是_本机
    if (ZHI_输出_内存中运行 != 状态机1->输出_类型 || 状态机1->运行时_入口)
#endif
    设置_全局_符号(状态机1, start_symbol, NULL, 0);

    if (0 == 状态机1->不添加标准库) {
        static const char *libs[] = {
            "msvcrt", "kernel32", "", "user32", "gdi32", NULL
        };
        const char **pp, *p;
        zhi_增加_支持(状态机1, ZHI_HEXINKU1);
        for (pp = libs; 0 != (p = *pp); ++pp) {
            if (*p)
                添加库错误(状态机1, p);
            else if (PE_DLL != pe_type && PE_GUI != pe_type)
                break;
        }
    }

    /* 需要这个用于'ELF文件处理.c：重定位_段（）' */
    if (ZHI_输出_DLL == 状态机1->输出_类型)
        状态机1->输出_类型 = ZHI_输出_EXE;
    if (ZHI_输出_内存中运行 == 状态机1->输出_类型)
        pe_type = PE_RUN;
    pe->type = pe_type;
    pe->start_symbol = start_symbol;
}

static void pe_设置_选项(知心状态机 * 状态机1, struct pe_信息 *pe)
{
    if (PE_DLL == pe->type) {
        /* XXX: 检查arm-pe 目标是否正确 */
        pe->imagebase = 0x10000000;
    } else {
#if defined(ZHI_TARGET_ARM)
        pe->imagebase = 0x00010000;
#else
        pe->imagebase = 0x00400000;
#endif
    }

#if defined(ZHI_TARGET_ARM)
    /* we use "console" subsystem by default */
    pe->subsystem = 9;
#else
    if (PE_DLL == pe->type || PE_GUI == pe->type)
        pe->subsystem = 2;
    else
        pe->subsystem = 3;
#endif
    /* Allow override via -Wl,-subsystem=... option */
    if (状态机1->pe_子系统 != 0)
        pe->subsystem = 状态机1->pe_子系统;

    /* set default file/section alignment */
    if (pe->subsystem == 1) {
        pe->分段_对齐 = 0x20;
        pe->file_align = 0x20;
    } else {
        pe->分段_对齐 = 0x1000;
        pe->file_align = 0x200;
    }

    if (状态机1->分段_对齐 != 0)
        pe->分段_对齐 = 状态机1->分段_对齐;
    if (状态机1->pe_文件_对齐 != 0)
        pe->file_align = 状态机1->pe_文件_对齐;

    if ((pe->subsystem >= 10) && (pe->subsystem <= 12))
        pe->imagebase = 0;

    if (状态机1->已有_代码段_地址)
        pe->imagebase = 状态机1->代码段_地址;
}

静态_函数 int pe_输出_文件(知心状态机 *状态机1, const char *文件名)
{
    int ret;
    struct pe_信息 pe;
    int i;

    memset(&pe, 0, sizeof pe);
    pe.文件名 = 文件名;
    pe.状态机1 = 状态机1;
    状态机1->文件类型 = 0;

#ifdef 配置_ZHI_边界检查
    zhi_新增_边界检查(状态机1);
#endif
    处理实用注释库(状态机1);
    pe_增加_运行时(状态机1, &pe);
    解决_常见_符号(状态机1);
    pe_设置_选项(状态机1, &pe);

    ret = pe_检查_符号(&pe);
    if (ret)
        ;
    else if (文件名) {
        pe_分配_地址(&pe);
        重定位_符号(状态机1, 状态机1->全局单词表副本, 0);
        状态机1->pe_图像库 = pe.imagebase;
        for (i = 1; i < 状态机1->数量_段数; ++i) {
            段 *s = 状态机1->段数[i];
            if (s->重定位) {
                重定位_段(状态机1, s);
            }
        }
        pe.start_addr = (DWORD)
            ((uintptr_t)zhi_获取_符号_错误(状态机1, pe.start_symbol)
                - pe.imagebase);
        if (状态机1->数量_错误)
            ret = -1;
        else
            ret = pe_写(&pe);
        动态数组_重分配容量(&pe.sec_info, &pe.sec_count);
    } else {
#ifdef ZHI_是_本机
        pe.thunk = 初始化数据_部分;
        pe_构建_导入(&pe);
        状态机1->运行时_入口 = pe.start_symbol;
#ifdef ZHI_TARGET_X86_64
        状态机1->uw_pdata = 查找_段(状态机1, ".pdata");
#endif
#endif
    }

    pe_释放_导入(&pe);

#ifdef PE_PRINT_SECTIONS
    pe_打印_段数(状态机1, "zhi.log");
#endif
    return ret;
}

/* ------------------------------------------------------------- */
