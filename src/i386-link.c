#ifdef TARGET_DEFS_ONLY

#define EM_ZHI_TARGET EM_386

/* relocation type for 32 bit data relocation */
#define R_DATA_32   R_386_32
#define R_DATA_PTR  R_386_32
#define R_JMP_SLOT  R_386_JMP_SLOT
#define R_GLOB_DAT  R_386_GLOB_DAT
#define R_COPY      R_386_COPY
#define R_RELATIVE  R_386_RELATIVE

#define R_NUM       R_386_NUM

#define ELF_START_ADDR 0x08048000
#define ELF_PAGE_SIZE  0x1000

#define PCRELATIVE_DLLPLT 0
#define RELOCATE_DLLPLT 0

#else /* !TARGET_DEFS_ONLY */

#include "zhi.h"

#ifndef ELF_OBJ_ONLY
/* Returns 1 for a code relocation, 0 for a data relocation. For unknown
   relocations, returns -1. */
int 代码_重定位 (int reloc_type)
{
    switch (reloc_type) {
	case R_386_RELATIVE:
	case R_386_16:
        case R_386_32:
	case R_386_GOTPC:
	case R_386_GOTOFF:
	case R_386_GOT32:
	case R_386_GOT32X:
	case R_386_GLOB_DAT:
	case R_386_COPY:
            return 0;

	case R_386_PC16:
	case R_386_PC32:
	case R_386_PLT32:
	case R_386_JMP_SLOT:
            return 1;
    }
    return -1;
}

/* Returns an enumerator to describe whether and when the relocation needs a
   GOT and/or PLT entry to be created. See zhi.h for a description of the
   different values. */
int 获取plt_入口_类型 (int reloc_type)
{
    switch (reloc_type) {
	case R_386_RELATIVE:
	case R_386_16:
	case R_386_GLOB_DAT:
	case R_386_JMP_SLOT:
	case R_386_COPY:
            return NO_GOTPLT_ENTRY;

        case R_386_32:
	    /* This relocations shouldn't normally need GOT or PLT
	       slots if it weren't for simplicity in the code generator.
	       See our caller for comments.  */
            return AUTO_GOTPLT_ENTRY;

	case R_386_PC16:
	case R_386_PC32:
            return AUTO_GOTPLT_ENTRY;

	case R_386_GOTPC:
	case R_386_GOTOFF:
            return BUILD_GOT_ONLY;

	case R_386_GOT32:
	case R_386_GOT32X:
	case R_386_PLT32:
            return ALWAYS_GOTPLT_ENTRY;
    }
    return -1;
}

静态_函数 unsigned 创建_plt_入口(知心状态机 *状态机1, unsigned got_offset, struct 额外_符号_属性 *attr)
{
    段 *plt = 状态机1->plt;
    uint8_t *p;
    int modrm;
    unsigned plt_offset, relofs;

    /* on i386 if we build a DLL, we add a %ebx offset */
    if (状态机1->输出_类型 == ZHI_输出_DLL)
        modrm = 0xa3;
    else
        modrm = 0x25;

    /* empty PLT: create PLT0 entry that pushes the library identifier
       (GOT + 指针_大小) and jumps to ld.so resolution routine
       (GOT + 2 * 指针_大小) */
    if (plt->数据_偏移 == 0) {
        p = 段_ptr_添加(plt, 16);
        p[0] = 0xff; /* pushl got + 指针_大小 */
        p[1] = modrm + 0x10;
        写32le(p + 2, 指针_大小);
        p[6] = 0xff; /* jmp *(got + 指针_大小 * 2) */
        p[7] = modrm;
        写32le(p + 8, 指针_大小 * 2);
    }
    plt_offset = plt->数据_偏移;

    /* The PLT slot refers to the relocation entry it needs via offset.
       The 重定位 entry is created below, so its offset is the current
       数据_偏移 */
    relofs = 状态机1->got->重定位 ? 状态机1->got->重定位->数据_偏移 : 0;

    /* Jump to GOT entry where ld.so initially put the address of ip + 4 */
    p = 段_ptr_添加(plt, 16);
    p[0] = 0xff; /* jmp *(got + x) */
    p[1] = modrm;
    写32le(p + 2, got_offset);
    p[6] = 0x68; /* push $xxx */
    写32le(p + 7, relofs);
    p[11] = 0xe9; /* jmp plt_start */
    写32le(p + 12, -(plt->数据_偏移));
    return plt_offset;
}

/* 重定位 the PLT: compute addresses and offsets in the PLT now that final
   address for PLT and GOT are known (see fill_program_header) */
静态_函数 void 重定位_plt(知心状态机 *状态机1)
{
    uint8_t *p, *p_end;

    if (!状态机1->plt)
      return;

    p = 状态机1->plt->data;
    p_end = p + 状态机1->plt->数据_偏移;

    if (p < p_end) {
        增加32le(p + 2, 状态机1->got->sh_addr);
        增加32le(p + 8, 状态机1->got->sh_addr);
        p += 16;
        while (p < p_end) {
            增加32le(p + 2, 状态机1->got->sh_addr);
            p += 16;
        }
    }
}
#endif

void 重定位(知心状态机 *状态机1, ElfW_Rel *rel, int type, unsigned char *ptr, 目标地址_类型 addr, 目标地址_类型 val)
{
    int sym_index, esym_index;

    sym_index = ELFW(R_SYM)(rel->r_info);

    switch (type) {
        case R_386_32:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                esym_index = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
                qrel->r_offset = rel->r_offset;
                if (esym_index) {
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_386_32);
                    qrel++;
                    return;
                } else {
                    qrel->r_info = ELFW(R_INFO)(0, R_386_RELATIVE);
                    qrel++;
                }
            }
            增加32le(ptr, val);
            return;
        case R_386_PC32:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                /* DLL relocation */
                esym_index = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_386_PC32);
                    qrel++;
                    return;
                }
            }
            增加32le(ptr, val - addr);
            return;
        case R_386_PLT32:
            增加32le(ptr, val - addr);
            return;
        case R_386_GLOB_DAT:
        case R_386_JMP_SLOT:
            写32le(ptr, val);
            return;
        case R_386_GOTPC:
            增加32le(ptr, 状态机1->got->sh_addr - addr);
            return;
        case R_386_GOTOFF:
            增加32le(ptr, val - 状态机1->got->sh_addr);
            return;
        case R_386_GOT32:
        case R_386_GOT32X:
            /* we 加载 the got offset */
            增加32le(ptr, get_额外_符号_属性(状态机1, sym_index, 0)->got_offset);
            return;
        case R_386_16:
            if (状态机1->输出_格式 != ZHI_输出_格式_二进制) {
            output_file:
                错误_打印("can only produce 16-bit binary 文件数");
            }
            写16le(ptr, 读16le(ptr) + val);
            return;
        case R_386_PC16:
            if (状态机1->输出_格式 != ZHI_输出_格式_二进制)
                goto output_file;
            写16le(ptr, 读16le(ptr) + val - addr);
            return;
        case R_386_RELATIVE:
#ifdef ZHI_TARGET_PE
            增加32le(ptr, val - 状态机1->pe_图像库);
#endif
            /* do nothing */
            return;
        case R_386_COPY:
            /* This relocation must copy initialized data from the library
            to the program .bss segment. Currently made like for ARM
            (to remove noise of default case). Is this true?
            */
            return;
        default:
            fprintf(stderr,"FIXME: handle 重定位 type %d at %x [%p] to %x\n",
                type, (unsigned)addr, ptr, (unsigned)val);
            return;
    }
}

#endif /* !TARGET_DEFS_ONLY */
