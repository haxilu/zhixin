/*
 *  GAS之类的ZHI汇编程序
 */

#define 全局_使用
#include "zhi.h"
#ifdef 配置_ZHI_汇编

static 段 *最后_代码_段; /* 处理.previous asm指令 */

静态_函数 int 汇编_获取_局部_标签_名称(知心状态机 *状态机1, unsigned int n)
{
    char buf[64];
    单词存储结构 *ts;

    snprintf(buf, sizeof(buf), "L..%u", n);
    ts = 单词表_查找(buf, strlen(buf));
    return ts->单词编码;
}

static int zhi_汇编_内部(知心状态机 *状态机1, int do_preprocess, int global);
static 符号* 汇编_新_标签(知心状态机 *状态机1, int label, int is_local);
static 符号* 汇编_新_标签1(知心状态机 *状态机1, int label, int is_local, int sh_num, int value);

/* 如果C名称以_开头，则通过删除第一个_来仅在C中表示以_开头的asm标签。 开头没有_的ASM名称与C名称不对应，但是我们也使用全局C单词表来跟踪ASM名称，因此我们需要将其转换为与C名称不冲突的名称，因此在前面添加 一个 '。' 为他们，但强制设置ELF汇编名称。  */
static int 汇编2c名称(int v, int *addeddot)
{
    const char *name;
    *addeddot = 0;
    if (!zhi_状态->前导_下划线)
      return v;
    name = 取_单词字符串(v, NULL);
    if (!name)
      return v;
    if (name[0] == '_') {
        v = 单词表_查找(name + 1, strlen(name) - 1)->单词编码;
    } else if (!strchr(name, '.')) {
        int n = strlen(name) + 2;
        char newname[n];
        snprintf(newname, n, ".%s", name);
        v = 单词表_查找(newname, n - 1)->单词编码;
        *addeddot = 1;
    }
    return v;
}

static 符号 *汇编_标签_查找(int v)
{
    符号 *sym;
    int addeddot;
    v = 汇编2c名称(v, &addeddot);
    sym = 符号_查询(v);
    while (sym && sym->符号_范围 && !(sym->type.t & VT_静态))
        sym = sym->prev_tok;
    return sym;
}

static 符号 *汇编_标签_推送(int v)
{
    int addeddot, v2 = 汇编2c名称(v, &addeddot);
    /* 我们总是为临时的符号定义（对于.set，对于实际的defs删除）添加VT_EXTERN，对于仅引用而言，它是正确的。  */
    符号 *sym = 推送_一个_全局标识符(v2, VT_汇编 | VT_外部 | VT_静态, 0);
    if (addeddot)
        sym->汇编_label = v;
    return sym;
}

/* 返回一个可以在汇编器内部使用的符号，名称为NAME。 来自asm和C源代码的符号共享一个名称空间。 如果我们生成一个asm符号，
 * 它也是一个（文件全局）C符号，但是它不能按名称访问（例如“ L.123”），或者其类型信息使得没有适当的C声明就无法使用。
 * 有时我们需要从asm可以通过名称访问的符号，这些符号在C中是匿名的，在这种情况下，可以使用CSYM将所有信息从该符号传输到
 * （可能是新创建的）asm符号。  */
静态_函数 符号* 获取_汇编_符号(int name, 符号 *csym)
{
    符号 *sym = 汇编_标签_查找(name);
    if (!sym) {
	sym = 汇编_标签_推送(name);
	if (csym)
	  sym->c = csym->c;
    }
    return sym;
}

static 符号* 汇编_段_符号(知心状态机 *状态机1, 段 *sec)
{
    char buf[100];
    int label = 单词表_查找(buf,
        snprintf(buf, sizeof buf, "L.%s", sec->name)
        )->单词编码;
    符号 *sym = 汇编_标签_查找(label);
    return sym ? sym : 汇编_新_标签1(状态机1, label, 1, sec->sh_num, 0);
}

/* 我们不使用C表达式解析器来处理符号。 也许可以对C表达式解析器进行调整。 */

static void 汇编_表达式_一元(知心状态机 *状态机1, 表达式值 *pe)
{
    符号 *sym;
    int op, label;
    uint64_t n;
    const char *p;

    switch(单词编码) {
    case 常量_预处理编号:
        p = 单词值.str.data;
        n = strtoull(p, (char **)&p, 0);
        if (*p == 'b' || *p == 'f') {
            /* 后退或前进标签 */
            label = 汇编_获取_局部_标签_名称(状态机1, n);
            sym = 汇编_标签_查找(label);
            if (*p == 'b') {
                /* backward : find the last corresponding defined label */
                if (sym && (!sym->c || elf符号(sym)->st_shndx == SHN_UNDEF))
                    sym = sym->prev_tok;
                if (!sym)
                    错误_打印("找不到本地标签 '%d' ", (int)n);
            } else {
                /* forward */
                if (!sym || (sym->c && elf符号(sym)->st_shndx != SHN_UNDEF)) {
                    /* if the last label is defined, then define a new one */
		    sym = 汇编_标签_推送(label);
                }
            }
	    pe->v = 0;
	    pe->sym = sym;
	    pe->pcrel = 0;
        } else if (*p == '\0') {
            pe->v = n;
            pe->sym = NULL;
	    pe->pcrel = 0;
        } else {
            错误_打印("无效的数字语法");
        }
        带有宏替换的下个标记();
        break;
    case '+':
        带有宏替换的下个标记();
        汇编_表达式_一元(状态机1, pe);
        break;
    case '-':
    case '~':
        op = 单词编码;
        带有宏替换的下个标记();
        汇编_表达式_一元(状态机1, pe);
        if (pe->sym)
            错误_打印("invalid operation with label");
        if (op == '-')
            pe->v = -pe->v;
        else
            pe->v = ~pe->v;
        break;
    case 常量_字符型:
    case 常量_长字符型:
	pe->v = 单词值.i;
	pe->sym = NULL;
	pe->pcrel = 0;
	带有宏替换的下个标记();
	break;
    case '(':
        带有宏替换的下个标记();
        汇编_表达式(状态机1, pe);
        跳过(')');
        break;
    case '.':
        pe->v = 输出代码索引;
        pe->sym = 汇编_段_符号(状态机1, 当前_生成代码_段);
        pe->pcrel = 0;
        带有宏替换的下个标记();
        break;
    default:
        if (单词编码 >= 符_识别) {
	    ELF符号 *esym;
            /* label case : if the label was not found, add one */
	    sym = 获取_汇编_符号(单词编码, NULL);
	    esym = elf符号(sym);
            if (esym && esym->st_shndx == SHN_ABS) {
                /* if absolute symbol, no need to put a symbol value */
                pe->v = esym->st_value;
                pe->sym = NULL;
		pe->pcrel = 0;
            } else {
                pe->v = 0;
                pe->sym = sym;
		pe->pcrel = 0;
            }
            带有宏替换的下个标记();
        } else {
            错误_打印("错误的表达语法 [%s]", 取_单词字符串(单词编码, &单词值));
        }
        break;
    }
}
    
static void 汇编_表达式_乘除余(知心状态机 *状态机1, 表达式值 *pe)
{
    int op;
    表达式值 e2;

    汇编_表达式_一元(状态机1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '*' && op != '/' && op != '%' && 
            op != 双符号_左位移 && op != 双符号_右位移)
            break;
        带有宏替换的下个标记();
        汇编_表达式_一元(状态机1, &e2);
        if (pe->sym || e2.sym)
            错误_打印("无效的操作标签");
        switch(op) {
        case '*':
            pe->v *= e2.v;
            break;
        case '/':  
            if (e2.v == 0) {
            div_error:
                错误_打印("被零除");
            }
            pe->v /= e2.v;
            break;
        case '%':  
            if (e2.v == 0)
                goto div_error;
            pe->v %= e2.v;
            break;
        case 双符号_左位移:
            pe->v <<= e2.v;
            break;
        default:
        case 双符号_右位移:
            pe->v >>= e2.v;
            break;
        }
    }
}

static void 汇编_表达式_逻辑(知心状态机 *状态机1, 表达式值 *pe)
{
    int op;
    表达式值 e2;

    汇编_表达式_乘除余(状态机1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '&' && op != '|' && op != '^')
            break;
        带有宏替换的下个标记();
        汇编_表达式_乘除余(状态机1, &e2);
        if (pe->sym || e2.sym)
            错误_打印("invalid operation with label");
        switch(op) {
        case '&':
            pe->v &= e2.v;
            break;
        case '|':  
            pe->v |= e2.v;
            break;
        default:
        case '^':
            pe->v ^= e2.v;
            break;
        }
    }
}

static inline void 汇编_表达式_和(知心状态机 *状态机1, 表达式值 *pe)
{
    int op;
    表达式值 e2;

    汇编_表达式_逻辑(状态机1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '+' && op != '-')
            break;
        带有宏替换的下个标记();
        汇编_表达式_逻辑(状态机1, &e2);
        if (op == '+') {
            if (pe->sym != NULL && e2.sym != NULL)
                goto cannot_relocate;
            pe->v += e2.v;
            if (pe->sym == NULL && e2.sym != NULL)
                pe->sym = e2.sym;
        } else {
            pe->v -= e2.v;
            /* 注意：在这种情况下，我们不如气体强大，因为我们在表达式中仅存储一个符号 */
	    if (!e2.sym) {
		/* OK */
	    } else if (pe->sym == e2.sym) { 
		/* OK */
		pe->sym = NULL; /* 相同的符号可以减为NULL */
	    } else {
		ELF符号 *esym1, *esym2;
		esym1 = elf符号(pe->sym);
		esym2 = elf符号(e2.sym);
		if (esym1 && esym1->st_shndx == esym2->st_shndx
		    && esym1->st_shndx != SHN_UNDEF) {
		    /* we also accept defined symbols in the same section */
		    pe->v += esym1->st_value - esym2->st_value;
		    pe->sym = NULL;
		} else if (esym2->st_shndx == 当前_生成代码_段->sh_num) {
		    /* When subtracting a defined symbol in current section
		       this actually makes the value PC-relative.  */
		    pe->v -= esym2->st_value - 输出代码索引 - 4;
		    pe->pcrel = 1;
		    e2.sym = NULL;
		} else {
cannot_relocate:
		    错误_打印("无效的操作标签");
		}
	    }
        }
    }
}

static inline void 汇编_表达式_比较(知心状态机 *状态机1, 表达式值 *pe)
{
    int op;
    表达式值 e2;

    汇编_表达式_和(状态机1, pe);
    for(;;) {
        op = 单词编码;
	if (op != 双符号_等于 && op != 双符号_不等于
	    && (op > 符_GT || op < 符号_ULE))
            break;
        带有宏替换的下个标记();
        汇编_表达式_和(状态机1, &e2);
        if (pe->sym || e2.sym)
            错误_打印("无效的操作标签");
        switch(op) {
	case 双符号_等于:
	    pe->v = pe->v == e2.v;
	    break;
	case 双符号_不等于:
	    pe->v = pe->v != e2.v;
	    break;
	case 符_LT:
	    pe->v = (int64_t)pe->v < (int64_t)e2.v;
	    break;
	case 双符号_大于等于:
	    pe->v = (int64_t)pe->v >= (int64_t)e2.v;
	    break;
	case 双符号_小于等于:
	    pe->v = (int64_t)pe->v <= (int64_t)e2.v;
	    break;
	case 符_GT:
	    pe->v = (int64_t)pe->v > (int64_t)e2.v;
	    break;
        default:
            break;
        }
	/* GAS compare results are -1/0 not 1/0.  */
	pe->v = -(int64_t)pe->v;
    }
}

静态_函数 void 汇编_表达式(知心状态机 *状态机1, 表达式值 *pe)
{
    汇编_表达式_比较(状态机1, pe);
}

静态_函数 int 汇编_整数_表达式(知心状态机 *状态机1)
{
    表达式值 e;
    汇编_表达式(状态机1, &e);
    if (e.sym)
        应为("constant");
    return e.v;
}

static 符号* 汇编_新_标签1(知心状态机 *状态机1, int label, int is_local,
                           int sh_num, int value)
{
    符号 *sym;
    ELF符号 *esym;

    sym = 汇编_标签_查找(label);
    if (sym) {
	esym = elf符号(sym);
	/* A VT_外部 symbol, even if it has a section is considered
	   overridable.  This is how we "define" .set targets.  Real
	   definitions won't have VT_外部 set.  */
        if (esym && esym->st_shndx != SHN_UNDEF) {
            /* the label is already defined */
            if (是_汇编_符号(sym)
                && (is_local == 1 || (sym->type.t & VT_外部)))
                goto new_label;
            if (!(sym->type.t & VT_外部))
                错误_打印("assembler label '%s' already defined",
                          取_单词字符串(label, NULL));
        }
    } else {
    new_label:
        sym = 汇编_标签_推送(label);
    }
    if (!sym->c)
      更新_外部_符号2(sym, SHN_UNDEF, 0, 0, 1);
    esym = elf符号(sym);
    esym->st_shndx = sh_num;
    esym->st_value = value;
    if (is_local != 2)
        sym->type.t &= ~VT_外部;
    return sym;
}

static 符号* 汇编_新_标签(知心状态机 *状态机1, int label, int is_local)
{
    return 汇编_新_标签1(状态机1, label, is_local, 当前_生成代码_段->sh_num, 输出代码索引);
}

/* 将LABEL的值设置为某些表达式的值（可能涉及其他符号）。 LABEL以后仍可以覆盖。  */
static 符号* 设置_符号(知心状态机 *状态机1, int label)
{
    long n;
    表达式值 e;
    符号 *sym;
    ELF符号 *esym;
    带有宏替换的下个标记();
    汇编_表达式(状态机1, &e);
    n = e.v;
    esym = elf符号(e.sym);
    if (esym)
	n += esym->st_value;
    sym = 汇编_新_标签1(状态机1, label, 2, esym ? esym->st_shndx : SHN_ABS, n);
    elf符号(sym)->st_other |= ST_ASM_SET;
    return sym;
}

static void 使用_段1(知心状态机 *状态机1, 段 *sec)
{
    当前_生成代码_段->数据_偏移 = 输出代码索引;
    当前_生成代码_段 = sec;
    输出代码索引 = 当前_生成代码_段->数据_偏移;
}

static void 使用_段(知心状态机 *状态机1, const char *name)
{
    段 *sec;
    sec = 查找_段(状态机1, name);
    使用_段1(状态机1, sec);
}

static void 推送_段(知心状态机 *状态机1, const char *name)
{
    段 *sec = 查找_段(状态机1, name);
    sec->prev = 当前_生成代码_段;
    使用_段1(状态机1, sec);
}

static void 弹出_段(知心状态机 *状态机1)
{
    段 *prev = 当前_生成代码_段->prev;
    if (!prev)
        错误_打印(".popsection without .pushsection");
    当前_生成代码_段->prev = NULL;
    使用_段1(状态机1, prev);
}

static void 汇编_解析_指令(知心状态机 *状态机1, int global)
{
    int n, offset, v, size, tok1;
    段 *sec;
    uint8_t *ptr;

    /* 汇编指令 */
    sec = 当前_生成代码_段;
    switch(单词编码) {
    case 符_汇编DIR_align:
    case 符_汇编DIR_balign:
    case 符_汇编DIR_p2align:
    case 符_汇编DIR_skip:
    case 符_汇编DIR_space:
        tok1 = 单词编码;
        带有宏替换的下个标记();
        n = 汇编_整数_表达式(状态机1);
        if (tok1 == 符_汇编DIR_p2align)
        {
            if (n < 0 || n > 30)
                错误_打印("invalid p2align, must be between 0 and 30");
            n = 1 << n;
            tok1 = 符_汇编DIR_align;
        }
        if (tok1 == 符_汇编DIR_align || tok1 == 符_汇编DIR_balign) {
            if (n < 0 || (n & (n-1)) != 0)
                错误_打印("alignment must be a positive power of two");
            offset = (输出代码索引 + n - 1) & -n;
            size = offset - 输出代码索引;
            /* 该部分必须具有兼容的对齐方式 */
            if (sec->sh_addralign < n)
                sec->sh_addralign = n;
        } else {
	    if (n < 0)
	        n = 0;
            size = n;
        }
        v = 0;
        if (单词编码 == ',') {
            带有宏替换的下个标记();
            v = 汇编_整数_表达式(状态机1);
        }
    zero_pad:
        if (sec->sh_type != SHT_NOBITS) {
            sec->数据_偏移 = 输出代码索引;
            ptr = 段_ptr_添加(sec, size);
            memset(ptr, v, size);
        }
        输出代码索引 += size;
        break;
    case 符_汇编DIR_quad:
#ifdef ZHI_TARGET_X86_64
	size = 8;
	goto 汇编_data;
#else
        带有宏替换的下个标记();
        for(;;) {
            uint64_t vl;
            const char *p;

            p = 单词值.str.data;
            if (单词编码 != 常量_预处理编号) {
            error_constant:
                错误_打印("64 bit constant");
            }
            vl = strtoll(p, (char **)&p, 0);
            if (*p != '\0')
                goto error_constant;
            带有宏替换的下个标记();
            if (sec->sh_type != SHT_NOBITS) {
                /* XXX: endianness */
                生成_le32(vl);
                生成_le32(vl >> 32);
            } else {
                输出代码索引 += 8;
            }
            if (单词编码 != ',')
                break;
            带有宏替换的下个标记();
        }
        break;
#endif
    case 符_汇编DIR_byte:
        size = 1;
        goto 汇编_data;
    case 符_汇编DIR_word:
    case 符_汇编DIR_short:
        size = 2;
        goto 汇编_data;
    case 符_汇编DIR_long:
    case 符_汇编DIR_int:
        size = 4;
    汇编_data:
        带有宏替换的下个标记();
        for(;;) {
            表达式值 e;
            汇编_表达式(状态机1, &e);
            if (sec->sh_type != SHT_NOBITS) {
                if (size == 4) {
                    生成_32位表达式(&e);
#ifdef ZHI_TARGET_X86_64
		} else if (size == 8) {
		    生成_64位表达式(&e);
#endif
                } else {
                    if (e.sym)
                        应为("constant");
                    if (size == 1)
                        生成(e.v);
                    else
                        生成_le16(e.v);
                }
            } else {
                输出代码索引 += size;
            }
            if (单词编码 != ',')
                break;
            带有宏替换的下个标记();
        }
        break;
    case 符_汇编DIR_fill:
        {
            int repeat, size, val, i, j;
            uint8_t repeat_buf[8];
            带有宏替换的下个标记();
            repeat = 汇编_整数_表达式(状态机1);
            if (repeat < 0) {
                错误_打印("repeat < 0; .fill ignored");
                break;
            }
            size = 1;
            val = 0;
            if (单词编码 == ',') {
                带有宏替换的下个标记();
                size = 汇编_整数_表达式(状态机1);
                if (size < 0) {
                    错误_打印("size < 0; .fill ignored");
                    break;
                }
                if (size > 8)
                    size = 8;
                if (单词编码 == ',') {
                    带有宏替换的下个标记();
                    val = 汇编_整数_表达式(状态机1);
                }
            }
            /* XXX: endianness */
            repeat_buf[0] = val;
            repeat_buf[1] = val >> 8;
            repeat_buf[2] = val >> 16;
            repeat_buf[3] = val >> 24;
            repeat_buf[4] = 0;
            repeat_buf[5] = 0;
            repeat_buf[6] = 0;
            repeat_buf[7] = 0;
            for(i = 0; i < repeat; i++) {
                for(j = 0; j < size; j++) {
                    生成(repeat_buf[j]);
                }
            }
        }
        break;
    case 符_汇编DIR_rept:
        {
            int repeat;
            单词字符串 *init_str;
            带有宏替换的下个标记();
            repeat = 汇编_整数_表达式(状态机1);
            init_str = 单词字符串_分配();
            while (带有宏替换的下个标记(), 单词编码 != 符_汇编DIR_endr) {
                if (单词编码 == CH_文件结尾)
                    错误_打印("we at end of file, .endr not found");
                单词字符串中添加当前解析的字符(init_str);
            }
            单词字符串_增加大小(init_str, -1);
            单词字符串_增加大小(init_str, 0);
            开始_宏(init_str, 1);
            while (repeat-- > 0) {
                zhi_汇编_内部(状态机1, (解析_标记 & 解析_标记_预处理),
				      global);
                宏_ptr = init_str->str;
            }
            结束_宏();
            带有宏替换的下个标记();
            break;
        }
    case 符_汇编DIR_org:
        {
            unsigned long n;
	    表达式值 e;
	    ELF符号 *esym;
            带有宏替换的下个标记();
	    汇编_表达式(状态机1, &e);
	    n = e.v;
	    esym = elf符号(e.sym);
	    if (esym) {
		if (esym->st_shndx != 当前_生成代码_段->sh_num)
		  应为("constant or same-section symbol");
		n += esym->st_value;
	    }
            if (n < 输出代码索引)
                错误_打印("attempt to .org backwards");
            v = 0;
            size = n - 输出代码索引;
            goto zero_pad;
        }
        break;
    case 符_汇编DIR_set:
	带有宏替换的下个标记();
	tok1 = 单词编码;
	带有宏替换的下个标记();
	/* 也接受“ .set东西”，但是对此不做任何事情。 它在GAS中用于设置各种功能，例如“ .set mips16”。 */
	if (单词编码 == ',')
	    设置_符号(状态机1, tok1);
	break;
    case 符_汇编DIR_globl:
    case 符_汇编DIR_global:
    case 符_汇编DIR_weak:
    case 符_汇编DIR_hidden:
	tok1 = 单词编码;
	do { 
            符号 *sym;
            带有宏替换的下个标记();
            sym = 获取_汇编_符号(单词编码, NULL);
	    if (tok1 != 符_汇编DIR_hidden)
                sym->type.t &= ~VT_静态;
            if (tok1 == 符_汇编DIR_weak)
                sym->a.weak = 1;
	    else if (tok1 == 符_汇编DIR_hidden)
	        sym->a.visibility = STV_HIDDEN;
            更新_存储(sym);
            带有宏替换的下个标记();
	} while (单词编码 == ',');
	break;
    case 符_汇编DIR_string:
    case 符_汇编DIR_ascii:
    case 符_汇编DIR_asciz:
        {
            const uint8_t *p;
            int i, size, t;

            t = 单词编码;
            带有宏替换的下个标记();
            for(;;) {
                if (单词编码 != 常量_字符串)
                    应为("string constant");
                p = 单词值.str.data;
                size = 单词值.str.size;
                if (t == 符_汇编DIR_ascii && size > 0)
                    size--;
                for(i = 0; i < size; i++)
                    生成(p[i]);
                带有宏替换的下个标记();
                if (单词编码 == ',') {
                    带有宏替换的下个标记();
                } else if (单词编码 != 常量_字符串) {
                    break;
                }
            }
	}
	break;
    case 符_汇编DIR_text:
    case 符_汇编DIR_data:
    case 符_汇编DIR_bss:
	{ 
            char sname[64];
            tok1 = 单词编码;
            n = 0;
            带有宏替换的下个标记();
            if (单词编码 != ';' && 单词编码 != 符_换行) {
		n = 汇编_整数_表达式(状态机1);
		带有宏替换的下个标记();
            }
            if (n)
                sprintf(sname, "%s%d", 取_单词字符串(tok1, NULL), n);
            else
                sprintf(sname, "%s", 取_单词字符串(tok1, NULL));
            使用_段(状态机1, sname);
	}
	break;
    case 符_汇编DIR_file:
        {
            char 文件名[512];

            文件名[0] = '\0';
            带有宏替换的下个标记();

            if (单词编码 == 常量_字符串)
                连接_字符串(文件名, sizeof(文件名), 单词值.str.data);
            else
                连接_字符串(文件名, sizeof(文件名), 取_单词字符串(单词编码, NULL));

            if (状态机1->警告_不支持)
                zhi_警告("ignoring .file %s", 文件名);

            带有宏替换的下个标记();
        }
        break;
    case 符_汇编DIR_ident:
        {
            char ident[256];

            ident[0] = '\0';
            带有宏替换的下个标记();

            if (单词编码 == 常量_字符串)
                连接_字符串(ident, sizeof(ident), 单词值.str.data);
            else
                连接_字符串(ident, sizeof(ident), 取_单词字符串(单词编码, NULL));

            if (状态机1->警告_不支持)
                zhi_警告("ignoring .ident %s", ident);

            带有宏替换的下个标记();
        }
        break;
    case 符_汇编DIR_size:
        { 
            符号 *sym;

            带有宏替换的下个标记();
            sym = 汇编_标签_查找(单词编码);
            if (!sym) {
                错误_打印("label not found: %s", 取_单词字符串(单词编码, NULL));
            }

            /* XXX .size name,label2-label1 */
            if (状态机1->警告_不支持)
                zhi_警告("ignoring .size %s,*", 取_单词字符串(单词编码, NULL));

            带有宏替换的下个标记();
            跳过(',');
            while (单词编码 != 符_换行 && 单词编码 != ';' && 单词编码 != CH_文件结尾) {
                带有宏替换的下个标记();
            }
        }
        break;
    case 符_汇编DIR_type:
        { 
            符号 *sym;
            const char *newtype;

            带有宏替换的下个标记();
            sym = 获取_汇编_符号(单词编码, NULL);
            带有宏替换的下个标记();
            跳过(',');
            if (单词编码 == 常量_字符串) {
                newtype = 单词值.str.data;
            } else {
                if (单词编码 == '@' || 单词编码 == '%')
                    带有宏替换的下个标记();
                newtype = 取_单词字符串(单词编码, NULL);
            }

            if (!strcmp(newtype, "function") || !strcmp(newtype, "STT_FUNC")) {
                sym->type.t = (sym->type.t & ~VT_基本类型) | VT_函数;
            }
            else if (状态机1->警告_不支持)
                zhi_警告("change type of '%s' from 0x%x to '%s' ignored", 
                    取_单词字符串(sym->v, NULL), sym->type.t, newtype);

            带有宏替换的下个标记();
        }
        break;
    case 符_汇编DIR_pushsection:
    case 符_汇编DIR_section:
        {
            char sname[256];
	    int old_nb_section = 状态机1->数量_段数;

	    tok1 = 单词编码;
            /* XXX: support more options */
            带有宏替换的下个标记();
            sname[0] = '\0';
            while (单词编码 != ';' && 单词编码 != 符_换行 && 单词编码 != ',') {
                if (单词编码 == 常量_字符串)
                    连接_字符串(sname, sizeof(sname), 单词值.str.data);
                else
                    连接_字符串(sname, sizeof(sname), 取_单词字符串(单词编码, NULL));
                带有宏替换的下个标记();
            }
            if (单词编码 == ',') {
                /* 跳过 section options */
                带有宏替换的下个标记();
                if (单词编码 != 常量_字符串)
                    应为("string constant");
                带有宏替换的下个标记();
                if (单词编码 == ',') {
                    带有宏替换的下个标记();
                    if (单词编码 == '@' || 单词编码 == '%')
                        带有宏替换的下个标记();
                    带有宏替换的下个标记();
                }
            }
            最后_代码_段 = 当前_生成代码_段;
	    if (tok1 == 符_汇编DIR_section)
	        使用_段(状态机1, sname);
	    else
	        推送_段(状态机1, sname);
	    /* If we just allocated a new section reset its alignment to
	       1.  创建_节 normally acts for GCC compatibility and
	       sets alignment to 指针_大小.  The assembler behaves different. */
	    if (old_nb_section != 状态机1->数量_段数)
	        当前_生成代码_段->sh_addralign = 1;
        }
        break;
    case 符_汇编DIR_previous:
        { 
            段 *sec;
            带有宏替换的下个标记();
            if (!最后_代码_段)
                错误_打印("no previous section referenced");
            sec = 当前_生成代码_段;
            使用_段1(状态机1, 最后_代码_段);
            最后_代码_段 = sec;
        }
        break;
    case 符_汇编DIR_popsection:
	带有宏替换的下个标记();
	弹出_段(状态机1);
	break;
#ifdef ZHI_TARGET_I386
    case 符_汇编DIR_code16:
        {
            带有宏替换的下个标记();
            状态机1->段_大小 = 16;
        }
        break;
    case 符_汇编DIR_code32:
        {
            带有宏替换的下个标记();
            状态机1->段_大小 = 32;
        }
        break;
#endif
#ifdef ZHI_TARGET_X86_64
    /* added for compatibility with GAS */
    case 符_汇编DIR_code64:
        带有宏替换的下个标记();
        break;
#endif
    default:
        错误_打印("未知的汇编指令 '.%s'", 取_单词字符串(单词编码, NULL));
        break;
    }
}


/* 解析一个文件 */
static int zhi_汇编_内部(知心状态机 *状态机1, int do_preprocess, int global)
{
    int opcode;
    int saved_解析_标记 = 解析_标记;

    解析_标记 = 解析_标记_汇编_文件 | 解析_标记_单词字符串;
    if (do_preprocess)
        解析_标记 |= 解析_标记_预处理;
    for(;;) {
        带有宏替换的下个标记();
        if (单词编码 == 符_文件结尾)
            break;
        解析_标记 |= 解析_标记_换行符; /* XXX: suppress that hack */
    redo:
        if (单词编码 == '#') {
            /* horrible gas comment */
            while (单词编码 != 符_换行)
                带有宏替换的下个标记();
        } else if (单词编码 >= 定义_汇编目录_第一个 && 单词编码 <= 定义_汇编目录_最后一个) {
            汇编_解析_指令(状态机1, global);
        } else if (单词编码 == 常量_预处理编号) {
            const char *p;
            int n;
            p = 单词值.str.data;
            n = strtoul(p, (char **)&p, 10);
            if (*p != '\0')
                应为("':'");
            /* new local label */
            汇编_新_标签(状态机1, 汇编_获取_局部_标签_名称(状态机1, n), 1);
            带有宏替换的下个标记();
            跳过(':');
            goto redo;
        } else if (单词编码 >= 符_识别) {
            /* instruction or label */
            opcode = 单词编码;
            带有宏替换的下个标记();
            if (单词编码 == ':') {
                /* new label */
                汇编_新_标签(状态机1, opcode, 0);
                带有宏替换的下个标记();
                goto redo;
            } else if (单词编码 == '=') {
		设置_符号(状态机1, opcode);
                goto redo;
            } else {
                汇编_指令代码(状态机1, opcode);
            }
        }
        /* end of line */
        if (单词编码 != ';' && 单词编码 != 符_换行)
            应为("end of line");
        解析_标记 &= ~解析_标记_换行符; /* XXX: suppress that hack */
    }

    解析_标记 = saved_解析_标记;
    return 0;
}

/* 汇编当前文件 */
静态_函数 int zhi_汇编(知心状态机 *状态机1, int do_preprocess)
{
    int ret;
    zhi_调试_开始(状态机1);
    /* default section is text */
    当前_生成代码_段 = 生成代码_段;
    输出代码索引 = 当前_生成代码_段->数据_偏移;
    不需要_代码生成 = 0;
    ret = zhi_汇编_内部(状态机1, do_preprocess, 1);
    当前_生成代码_段->数据_偏移 = 输出代码索引;
    zhi_结束_调试(状态机1);
    return ret;
}

/********************************************************************/
/* GCC内联asm支持 */

/* 在不进行C预处理的情况下，在当前C编译单元中组装字符串'str'。 注意：通过在末尾修改'\ 0'来修改str */
static void zhi_内联_汇编(知心状态机 *状态机1, char *str, int len, int global)
{
    const int *saved_宏_ptr = 宏_ptr;
    int dotid = 设置_等值数('.', IS_ID);
    int dolid = 设置_等值数('$', 0);

    打开缓存文件(状态机1, ":asm:", len);
    memcpy(file->buffer, str, len);
    宏_ptr = NULL;
    zhi_汇编_内部(状态机1, 0, global);
    关闭文件();

    设置_等值数('$', dolid);
    设置_等值数('.', dotid);
    宏_ptr = saved_宏_ptr;
}

/* 通过编号或ID（gcc 3扩展语法）查找约束。 如果找不到，则返回-1。 约束后以char返回* pp */
静态_函数 int 查找_约束(汇编操作数 *operands, int 数量_operands,
                           const char *name, const char **pp)
{
    int index;
    单词存储结构 *ts;
    const char *p;

    if (是数字(*name)) {
        index = 0;
        while (是数字(*name)) {
            index = (index * 10) + (*name) - '0';
            name++;
        }
        if ((unsigned)index >= 数量_operands)
            index = -1;
    } else if (*name == '[') {
        name++;
        p = strchr(name, ']');
        if (p) {
            ts = 单词表_查找(name, p - name);
            for(index = 0; index < 数量_operands; index++) {
                if (operands[index].id == ts->单词编码)
                    goto found;
            }
            index = -1;
        found:
            name = p + 1;
        } else {
            index = -1;
        }
    } else {
        index = -1;
    }
    if (pp)
        *pp = name;
    return index;
}

static void 替代_汇编_操作数(汇编操作数 *operands, int 数量_operands,动态字符串 *out_str, 动态字符串 *in_str)
{
    int c, index, modifier;
    const char *str;
    汇编操作数 *op;
    堆栈值 sv;

    动态字符串_初始化(out_str);
    str = in_str->指向字符串的指针;
    for(;;) {
        c = *str++;
        if (c == '%') {
            if (*str == '%') {
                str++;
                goto 添加_字符;
            }
            modifier = 0;
            if (*str == 'c' || *str == 'n' ||
                *str == 'b' || *str == 'w' || *str == 'h' || *str == 'k' ||
		*str == 'q' ||
		/*在PIC模式下，GCC中的P将在符号引用中添加“ @PLT”，并使文字操作数不使用“ $”修饰。 */
		*str == 'P')
                modifier = *str++;
            index = 查找_约束(operands, 数量_operands, str, &str);
            if (index < 0)
                错误_打印("invalid operand reference after %%");
            op = &operands[index];
            sv = *op->vt;
            if (op->reg >= 0) {
                sv.r = op->reg;
                if ((op->vt->r & VT_值掩码) == VT_LLOCAL && op->is_memory)
                    sv.r |= VT_LVAL;
            }
            替换_汇编_操作数(out_str, &sv, modifier);
        } else {
        添加_字符:
            动态字符串_追加单个字符(out_str, c);
            if (c == '\0')
                break;
        }
    }
}


static void 解析_汇编_操作数(汇编操作数 *operands, int *数量_operands_ptr,
                               int is_output)
{
    汇编操作数 *op;
    int 数量_operands;

    if (单词编码 != ':') {
        数量_operands = *数量_operands_ptr;
        for(;;) {
	    动态字符串 astr;
            if (数量_operands >= 最大_汇编_操作数)
                错误_打印("太多的汇编操作数");
            op = &operands[数量_operands++];
            op->id = 0;
            if (单词编码 == '[') {
                带有宏替换的下个标记();
                if (单词编码 < 符_识别)
                    应为("identifier");
                op->id = 单词编码;
                带有宏替换的下个标记();
                跳过(']');
            }
	    解析_多_字符串(&astr, "string constant");
            op->constraint = 内存_申请(astr.字符串长度);
            strcpy(op->constraint, astr.指向字符串的指针);
	    动态字符串_释放(&astr);
            跳过('(');
            通用表达式();
            if (is_output) {
                if (!(栈顶值->type.t & VT_数组))
                    测试_左值();
            } else {
                /* 我们希望避免使用LLOCAL的情况，除非使用'm'约束。 请注意，它可能来自寄存器存储，因此我们需要转换（reg）大小写 */
                if ((栈顶值->r & VT_LVAL) &&
                    ((栈顶值->r & VT_值掩码) == VT_LLOCAL ||
                     (栈顶值->r & VT_值掩码) < VT_VC常量) &&
                    !strchr(op->constraint, 'm')) {
                    将rc寄存器值存储在栈顶值中(寄存器类_整数);
                }
            }
            op->vt = 栈顶值;
            跳过(')');
            if (单词编码 == ',') {
                带有宏替换的下个标记();
            } else {
                break;
            }
        }
        *数量_operands_ptr = 数量_operands;
    }
}

/* 解析GCC asm（）指令 */
静态_函数 void 汇编_指令字符串(void)
{
    动态字符串 astr, astr1;
    汇编操作数 operands[最大_汇编_操作数];
    int 数量_outputs, 数量_operands, i, must_subst, out_reg;
    uint8_t clobber_regs[NB_ASM_REGS];
    段 *sec;

    /* 由于我们总是生成asm（）指令，因此可以忽略volatile */
    if (单词编码 == 关键词_VOLATILE1 || 单词编码 == 关键词_VOLATILE2 || 单词编码 == 关键词_VOLATILE3 || 单词编码 == 关键词_易变) {
        带有宏替换的下个标记();
    }
    解析_汇编_字符串(&astr);
    数量_operands = 0;
    数量_outputs = 0;
    must_subst = 0;
    memset(clobber_regs, 0, sizeof(clobber_regs));
    if (单词编码 == ':') {
        带有宏替换的下个标记();
        must_subst = 1;
        /* 输出参数 */
        解析_汇编_操作数(operands, &数量_operands, 1);
        数量_outputs = 数量_operands;
        if (单词编码 == ':') {
            带有宏替换的下个标记();
            if (单词编码 != ')') {
                /* 输入参数 */
                解析_汇编_操作数(operands, &数量_operands, 0);
                if (单词编码 == ':') {
                    /* 垃圾清单 */
                    /* XXX: 处理寄存器 */
                    带有宏替换的下个标记();
                    for(;;) {
                        if (单词编码 != 常量_字符串)
                            应为("string constant");
                        汇编_破坏者(clobber_regs, 单词值.str.data);/**/
                        带有宏替换的下个标记();
                        if (单词编码 == ',') {
                            带有宏替换的下个标记();
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }
    跳过(')');
    /* 注意：我们不吃';' 这样我们就可以在汇编程序解析后还原当前令牌 */
    if (单词编码 != ';')
        应为("';'");
    
    /* 将所有值保存在内存中 */
    保存_寄存器最多n个堆栈条目(0);

    /* 计算约束 */
    汇编_计算_约束(operands, 数量_operands, 数量_outputs, 
                            clobber_regs, &out_reg);

    /* 替换asm字符串中的操作数。 如果没有操作数（GCC行为），则不会进行替换*/
#ifdef ASM_DEBUG
    printf("asm: \"%s\"\n", (char *)astr.指向字符串的指针);
#endif
    if (must_subst) {
        替代_汇编_操作数(operands, 数量_operands, &astr1, &astr);
        动态字符串_释放(&astr);
    } else {
        astr1 = astr;
    }
#ifdef ASM_DEBUG
    printf("subst_asm: \"%s\"\n", (char *)astr1.指向字符串的指针);
#endif

    /* 产生负荷 */
    汇编_生成_代码(operands, 数量_operands, 数量_outputs, 0, 
                 clobber_regs, out_reg);    

    /* 用zhi内部汇编器组装轴向  */
    sec = 当前_生成代码_段;
    /* 用zhi内部汇编器组装字符串 */
    zhi_内联_汇编(zhi_状态, astr1.指向字符串的指针, astr1.字符串长度 - 1, 0);
    if (sec != 当前_生成代码_段) {
        zhi_警告("inline asm tries to change current section");
        使用_段1(zhi_状态, sec);
    }

    /* 恢复当前的C标识符 */
    带有宏替换的下个标记();

    /* 如果需要，存储输出值*/
    汇编_生成_代码(operands, 数量_operands, 数量_outputs, 1, 
                 clobber_regs, out_reg);
    
    /* 释放一切 */
    for(i=0;i<数量_operands;i++) {
        汇编操作数 *op;
        op = &operands[i];
        内存_释放(op->constraint);
        弹出堆栈值();
    }
    动态字符串_释放(&astr1);
}

静态_函数 void 汇编_全局_instr(void)
{
    动态字符串 astr;
    int saved_nocode_wanted = 不需要_代码生成;

    /* 始终会发出全局asm块。  */
    不需要_代码生成 = 0;
    带有宏替换的下个标记();
    解析_汇编_字符串(&astr);
    跳过(')');
    /* 注意：我们不吃';' 这样我们就可以在汇编程序解析后还原当前标识 */
    if (单词编码 != ';')
        应为("';'");
    
#ifdef ASM_DEBUG
    printf("汇编_global: \"%s\"\n", (char *)astr.指向字符串的指针);
#endif
    当前_生成代码_段 = 生成代码_段;
    输出代码索引 = 当前_生成代码_段->数据_偏移;

    /* 用zhi内部汇编器组装字符串 */
    zhi_内联_汇编(zhi_状态, astr.指向字符串的指针, astr.字符串长度 - 1, 1);
    
    当前_生成代码_段->数据_偏移 = 输出代码索引;

    /* 恢复当前的C标识符 */
    带有宏替换的下个标记();

    动态字符串_释放(&astr);
    不需要_代码生成 = saved_nocode_wanted;
}
#endif /* 配置_ZHI_汇编 */
