
/*本文件只在3个地方引用：Makefile文件第158行；zhi.h第1179行；词法分析.c第55行*/
/*************************面向对象-开始******************************/

	 定义(关键词_公共, "公共")
     //定义(关键词_public, "public")
	 定义(关键词_保护, "保护")
     //定义(关键词_protect, "protect")
	 定义(关键词_私有, "私有")
     //定义(关键词_private, "private")
	 定义(关键词_类, "类")
     //定义(关键词_class, "class")
	 定义(关键词_函数, "函数")
     //定义(关键词_function, "function")
	 定义(关键词_抽象, "抽象")
    // 定义(关键词_abstract, "abstract")
	 定义(关键词_继承, "继承")
     //定义(关键词_extends, "extends")
	 定义(关键词_对接, "对接")
    // 定义(关键词_implements, "implements")
	 定义(关键词_接口, "接口")
     //定义(关键词_interface, "interface")
	 定义(关键词_创建, "创建")
     //定义(关键词_new, "new")
	 定义(关键词_包, "包")
    // 定义(关键词_package, "package")
	 定义(关键词_当前对象, "当前对象")
    // 定义(关键词_this, "this")
	 定义(关键词_抛出, "抛出")
     //定义(关键词_throw, "throw")
/*************************面向对象-结束******************************/
/* keywords */
	 定义(关键词_无类型, "无类型")
     定义(关键词_VOID, "void")
     定义(关键词_整数型, "整数型")
     定义(关键词_INT, "int")
	 定义(关键词_长整数型, "长整数型")
     定义(关键词_LONG, "long")
	 定义(关键词_短整数型, "短整数型")
     定义(关键词_SHORT, "short")
	 定义(关键词_字符型, "字符型")
     定义(关键词_CHAR, "char")
	 定义(关键词_浮点型, "浮点型")
     定义(关键词_FLOAT, "float")
	 定义(关键词_双精度浮点型, "双精度浮点型")
     定义(关键词_DOUBLE, "double")
	 定义(关键词_逻辑型, "逻辑型")
     定义(关键词_BOOL, "_Bool")
	 定义(关键词_无符号, "无符号")
     定义(关键词_UNSIGNED, "unsigned")
	 定义(关键词_有符号, "有符号")
     定义(关键词_SIGNED1, "signed")
     定义(关键词_SIGNED2, "__signed") /* gcc keyword */
     定义(关键词_SIGNED3, "__signed__") /* gcc keyword */
	 定义(关键词_常量, "常量")
     定义(关键词_CONST1, "const")
     定义(关键词_CONST2, "__const") /* gcc keyword */
     定义(关键词_CONST3, "__const__") /* gcc keyword */
	 定义(关键词_寄存器, "寄存器")
     定义(关键词_REGISTER, "register")
	 定义(关键词_自动, "自动")
     定义(关键词_AUTO, "auto")
	 定义(关键词_外部, "外部")
     定义(关键词_EXTERN, "extern")
	 定义(关键词_静态, "静态")
     定义(关键词_STATIC, "static")

	 定义(关键词_如果, "如果")
     定义(关键词_IF, "if")
	 定义(关键词_否则, "否则")
     定义(关键词_ELSE, "else")
	 定义(关键词_执行, "执行")
     定义(关键词_DO, "do")
	 定义(关键词_判断, "判断")
     定义(关键词_WHILE, "while")
	 定义(关键词_循环, "循环")
     定义(关键词_FOR, "for")
	 定义(关键词_选择, "选择")
     定义(关键词_SWITCH, "switch")
	 定义(关键词_分支, "分支")
     定义(关键词_CASE, "case")
	 定义(关键词_跳出, "跳出")
     定义(关键词_BREAK, "break")
	 定义(关键词_返回, "返回")
     定义(关键词_RETURN, "return")
	 定义(关键词_去向, "去向")
     定义(关键词_GOTO, "goto")
	 定义(关键词_继续, "继续")
     定义(关键词_CONTINUE, "continue")


	 定义(关键词_易变, "易变")
     定义(关键词_VOLATILE1, "volatile")
     定义(关键词_VOLATILE2, "__volatile") /* gcc keyword */
     定义(关键词_VOLATILE3, "__volatile__") /* gcc keyword */
	 定义(关键词_内联, "内联")
     定义(关键词_INLINE1, "inline")
     定义(关键词_INLINE2, "__inline") /* gcc keyword */
     定义(关键词_INLINE3, "__inline__") /* gcc keyword */
	 定义(关键词_限定, "限定")
     定义(关键词_RESTRICT1, "restrict")
     定义(关键词_RESTRICT2, "__restrict")
     定义(关键词_RESTRICT3, "__restrict__")
     定义(关键词_EXTENSION, "__extension__") /* gcc keyword */

     定义(关键词_GENERIC, "_Generic")
	 定义(关键词_静态_声明, "_静态_声明")/*2020年6月21日为止添加的关键词*/
     定义(关键词_STATIC_ASSERT, "_Static_assert")/*2020年6月21日为止添加的关键词*/

	 定义(关键词_结构体, "结构体")
     定义(关键词_STRUCT, "struct")
	 定义(关键词_共用体, "共用体")
     定义(关键词_UNION, "union")
	 定义(关键词_别名, "类型定义")
     定义(关键词_TYPEDEF, "typedef")
	 定义(关键词_默认, "默认")
     定义(关键词_DEFAULT, "default")
	 定义(关键词_枚举, "枚举")
     定义(关键词_ENUM, "enum")
	 定义(关键词_获取大小, "取大小")
     定义(关键词_SIZEOF, "sizeof")
	 定义(关键词_属性, "__属性")
     定义(关键词_ATTRIBUTE1, "__attribute")
     定义(关键词_ATTRIBUTE2, "__attribute__")
	 定义(关键词_对齐, "__对齐")
     定义(关键词_ALIGNOF1, "__alignof")
     定义(关键词_ALIGNOF2, "__alignof__")
     定义(关键词_ALIGNOF3, "_Alignof")
     定义(关键词_ALIGNAS, "_Alignas")
	 定义(关键词_取类型, "取类型")
     定义(关键词_TYPEOF1, "typeof")
     定义(关键词_TYPEOF2, "__typeof")
     定义(关键词_TYPEOF3, "__typeof__")
	 定义(关键词_标签, "__标签__")
     定义(关键词_LABEL, "__label__")/*用于GCC块本地标签*/
	 定义(关键词_汇编, "汇编")
     定义(关键词_asm1, "asm")
     定义(关键词_asm2, "__asm")
     定义(关键词_asm3, "__asm__")
#ifdef ZHI_TARGET_ARM64
	 定义(关键词_无整128, "无整128")
     定义(关键词_UINT128, "__uint128_t")
#endif


/*********************************************************************/
/* the following are not keywords. They are included to ease parsing */
/* preprocessor only */
	 定义(关键词_定义, "定义")
     定义(关键词_DEFINE, "define")
	 定义(关键词_导入, "导入")
     定义(关键词_INCLUDE, "include")
	 定义(关键词_导入_下个, "导入下个")
     定义(关键词_INCLUDE_NEXT, "include_next")
	 定义(关键词_如果已定义, "如果已定义")
     定义(关键词_IFDEF, "ifdef")
	 定义(关键词_如果未定义, "如果未定义")
     定义(关键词_IFNDEF, "ifndef")
	 定义(关键词_否则如果, "否则如果")
     定义(关键词_ELIF, "elif")
	 定义(关键词_结束如果, "结束如果")
     定义(关键词_ENDIF, "endif")
	 定义(关键词_已定义, "已定义")
     定义(关键词_DEFINED, "defined")
	 定义(关键词_取消定义, "取消定义")
     定义(关键词_UNDEF, "undef")
	 定义(关键词_错误, "错误")
     定义(关键词_ERROR, "error")
	 定义(关键词_警告, "警告")
     定义(关键词_WARNING, "warning")
	 定义(管家词_行号, "行号")
     定义(关键词_LINE, "line")
	 定义(关键词_杂注, "杂注")
     定义(关键词_PRAGMA, "pragma")
	 定义(符___LINE___CN, "__行号__")
     定义(符___LINE__, "__LINE__")
	 定义(符___FILE___CN, "__文件__")
     定义(符___FILE__, "__FILE__")
	 定义(符___DATE___CN, "__日期__")
     定义(符___DATE__, "__DATE__")
	 定义(符___TIME___CN, "__时间__")
     定义(符___TIME__, "__TIME__")
	 定义(符___FUNCTION___CN, "__函数名__")
     定义(符___FUNCTION__, "__FUNCTION__")
	 定义(符___VA_ARGS___CN, "__变参__")
     定义(符___VA_ARGS__, "__VA_ARGS__")
	 定义(符___COUNTER___CN, "__计数__")
     定义(符___COUNTER__, "__COUNTER__")
	 定义(符___HAS_INCLUDE___CN, "__已经_包含")
     定义(符___HAS_INCLUDE, "__has_include")

/* special identifiers */
	 定义(符___函数___CN, "__函数__")
     定义(符___函数__, "__func__")

/* special floating point values */
     定义(符___NAN__, "__nan__")
     定义(符___SNAN__, "__snan__")
     定义(符___INF__, "__inf__")

/* attribute identifiers */
/* XXX: handle all 标识符s generically since speed is not critical */
     定义(符_SECTION1, "section")
     定义(符_SECTION2, "__段__")
     定义(符_ALIGNED1, "aligned")
     定义(符_ALIGNED2, "__aligned__")
     定义(符_PACKED1, "packed")
     定义(符_PACKED2, "__packed__")
     定义(符_WEAK1, "weak")
     定义(符_WEAK2, "__weak__")
     定义(符_ALIAS1, "alias")
     定义(符_ALIAS2, "__alias__")
     定义(符_UNUSED1, "unused")
     定义(符_UNUSED2, "__unused__")
     定义(符_CDECL1, "cdecl")
     定义(符_CDECL2, "__cdecl")
     定义(符_CDECL3, "__cdecl__")
     定义(符_标准调用1, "stdcall")
     定义(符_标准调用2, "__stdcall")
     定义(符_标准调用3, "__stdcall__")
     定义(符_快速调用1, "fastcall")
     定义(符_快速调用2, "__fastcall")
     定义(符_快速调用3, "__fastcall__")
     定义(符_REGPARM1, "regparm")
     定义(符_REGPARM2, "__regparm__")
     定义(符_CLEANUP1, "cleanup")
     定义(符_CLEANUP2, "__cleanup__")
     定义(符_CONSTRUCTOR1, "constructor")
     定义(符_CONSTRUCTOR2, "__constructor__")
     定义(符_DESTRUCTOR1, "destructor")
     定义(符_DESTRUCTOR2, "__destructor__")
     定义(符_ALWAYS_INLINE1, "always_inline")
     定义(符_ALWAYS_INLINE2, "__always_inline__")

     定义(符_MODE, "__mode__")
     定义(符_MODE_QI, "__QI__")
     定义(符_MODE_DI, "__DI__")
     定义(符_MODE_HI, "__HI__")
     定义(符_MODE_SI, "__SI__")
     定义(符_MODE_word, "__word__")

     定义(符_DLLEXPORT, "dllexport")
     定义(符_DLLIMPORT, "dllimport")
     定义(符_NODECORATE, "nodecorate")
     定义(符_NORETURN1, "noreturn")
     定义(符_NORETURN2, "__noreturn__")
     定义(符_NORETURN3, "_Noreturn")
     定义(符_VISIBILITY1, "visibility")
     定义(符_VISIBILITY2, "__visibility__")

     定义(符_builtin_types_compatible_p, "__builtin_types_compatible_p")
     定义(符_builtin_choose_expr, "__builtin_choose_expr")
     定义(符_builtin_constant_p, "__builtin_constant_p")
     定义(符_builtin_frame_address, "__builtin_frame_address")
     定义(符_builtin_return_address, "__builtin_return_address")
     定义(符_builtin_expect, "__builtin_expect")
     /*定义(符_builtin_va_list, "__builtin_va_list")*/
#if defined ZHI_TARGET_PE && defined ZHI_TARGET_X86_64
     定义(符_builtin_va_start, "__builtin_va_start")
#elif defined ZHI_TARGET_X86_64
     定义(符_builtin_va_arg_types, "__builtin_va_arg_types")
#elif defined ZHI_TARGET_ARM64
     定义(符_builtin_va_start, "__builtin_va_start")
     定义(符_builtin_va_arg, "__builtin_va_arg")
#elif defined ZHI_TARGET_RISCV64
     定义(符_builtin_va_start, "__builtin_va_start")
#endif

/* pragma */
     定义(符_pack, "pack")
#if !defined(ZHI_TARGET_I386) && !defined(ZHI_TARGET_X86_64)
     /* already defined for assembler */
     定义(符_汇编_push, "push")
     定义(符_汇编_pop, "pop")
#endif
     定义(符_comment, "comment")
     定义(符_lib, "lib")
     定义(符_push_macro, "push_macro")
     定义(符_pop_macro, "pop_macro")
     定义(符_once, "once")
     定义(符_option, "option")

/* builtin functions or variables */
#ifndef ZHI_ARM_EABI
     定义(符_memcpy, "memcpy")
     定义(符_memmove, "memmove")
     定义(符_memset, "memset")
     定义(符___divdi3, "__divdi3")
     定义(符___moddi3, "__moddi3")
     定义(符___udivdi3, "__udivdi3")
     定义(符___umoddi3, "__umoddi3")
     定义(符___ashrdi3, "__ashrdi3")
     定义(符___lshrdi3, "__lshrdi3")
     定义(符___ashldi3, "__ashldi3")
     定义(符___floatundisf, "__floatundisf")
     定义(符___floatundidf, "__floatundidf")
# ifndef ZHI_ARM_VFP
     定义(符___floatundixf, "__floatundixf")
     定义(符___fixunsxfdi, "__fixunsxfdi")
# endif
     定义(符___fixunssfdi, "__fixunssfdi")
     定义(符___fixunsdfdi, "__fixunsdfdi")
#endif

#if defined ZHI_TARGET_ARM
# ifdef ZHI_ARM_EABI
     定义(符_memcpy, "__aeabi_memcpy")
     定义(符_memmove, "__aeabi_memmove")
     定义(符_memmove4, "__aeabi_memmove4")
     定义(符_memmove8, "__aeabi_memmove8")
     定义(符_memset, "__aeabi_memset")
     定义(符___aeabi_ldivmod, "__aeabi_ldivmod")
     定义(符___aeabi_uldivmod, "__aeabi_uldivmod")
     定义(符___aeabi_idivmod, "__aeabi_idivmod")
     定义(符___aeabi_uidivmod, "__aeabi_uidivmod")
     定义(符___divsi3, "__aeabi_idiv")
     定义(符___udivsi3, "__aeabi_uidiv")
     定义(符___floatdisf, "__aeabi_l2f")
     定义(符___floatdidf, "__aeabi_l2d")
     定义(符___fixsfdi, "__aeabi_f2lz")
     定义(符___fixdfdi, "__aeabi_d2lz")
     定义(符___ashrdi3, "__aeabi_lasr")
     定义(符___lshrdi3, "__aeabi_llsr")
     定义(符___ashldi3, "__aeabi_llsl")
     定义(符___floatundisf, "__aeabi_ul2f")
     定义(符___floatundidf, "__aeabi_ul2d")
     定义(符___fixunssfdi, "__aeabi_f2ulz")
     定义(符___fixunsdfdi, "__aeabi_d2ulz")
# else
     定义(符___modsi3, "__modsi3")
     定义(符___umodsi3, "__umodsi3")
     定义(符___divsi3, "__divsi3")
     定义(符___udivsi3, "__udivsi3")
     定义(符___floatdisf, "__floatdisf")
     定义(符___floatdidf, "__floatdidf")
#  ifndef ZHI_ARM_VFP
     定义(符___floatdixf, "__floatdixf")
     定义(符___fixunssfsi, "__fixunssfsi")
     定义(符___fixunsdfsi, "__fixunsdfsi")
     定义(符___fixunsxfsi, "__fixunsxfsi")
     定义(符___fixxfdi, "__fixxfdi")
#  endif
     定义(符___fixsfdi, "__fixsfdi")
     定义(符___fixdfdi, "__fixdfdi")
# endif
#endif

#if defined ZHI_TARGET_C67
     定义(符__divi, "_divi")
     定义(符__divu, "_divu")
     定义(符__divf, "_divf")
     定义(符__divd, "_divd")
     定义(符__remi, "_remi")
     定义(符__remu, "_remu")
#endif

#if defined ZHI_TARGET_I386
     定义(符___fixsfdi, "__fixsfdi")
     定义(符___fixdfdi, "__fixdfdi")
     定义(符___fixxfdi, "__fixxfdi")
#endif

#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
     定义(符_alloca, "alloca")
#endif

#if defined ZHI_TARGET_PE
     定义(符___chkstk, "__chkstk")
#endif
#if defined ZHI_TARGET_ARM64 || defined ZHI_TARGET_RISCV64
     定义(符___arm64_clear_cache, "__arm64_clear_cache")
     定义(符___addtf3, "__addtf3")
     定义(符___subtf3, "__subtf3")
     定义(符___multf3, "__multf3")
     定义(符___divtf3, "__divtf3")
     定义(符___extendsftf2, "__extendsftf2")
     定义(符___extenddftf2, "__extenddftf2")
     定义(符___trunctfsf2, "__trunctfsf2")
     定义(符___trunctfdf2, "__trunctfdf2")
     定义(符___fixtfsi, "__fixtfsi")
     定义(符___fixtfdi, "__fixtfdi")
     定义(符___fixunstfsi, "__fixunstfsi")
     定义(符___fixunstfdi, "__fixunstfdi")
     定义(符___floatsitf, "__floatsitf")
     定义(符___floatditf, "__floatditf")
     定义(符___floatunsitf, "__floatunsitf")
     定义(符___floatunditf, "__floatunditf")
     定义(符___eqtf2, "__eqtf2")
     定义(符___netf2, "__netf2")
     定义(符___lttf2, "__lttf2")
     定义(符___letf2, "__letf2")
     定义(符___gttf2, "__gttf2")
     定义(符___getf2, "__getf2")
#endif

/* bound checking symbols */
#ifdef 配置_ZHI_边界检查
     定义(符___bound_ptr_add, "__bound_ptr_add")
     定义(符___bound_ptr_间接的1, "__bound_ptr_间接的1")
     定义(符___bound_ptr_间接的2, "__bound_ptr_间接的2")
     定义(符___bound_ptr_间接的4, "__bound_ptr_间接的4")
     定义(符___bound_ptr_间接的8, "__bound_ptr_间接的8")
     定义(符___bound_ptr_间接的12, "__bound_ptr_间接的12")
     定义(符___bound_ptr_间接的16, "__bound_ptr_间接的16")
     定义(符___bound_main_arg, "__bound_main_arg")
     定义(符___bound_local_new, "__bound_local_new")
     定义(符___bound_local_delete, "__bound_local_delete")
     定义(符___bound_setjmp, "__bound_setjmp")
     定义(符___bound_new_region, "__bound_new_region")
# ifdef ZHI_TARGET_PE
#  ifdef ZHI_TARGET_X86_64
     定义(符___bound_alloca_nr, "__bound_alloca_nr")
#  endif
     定义(符_malloc, "malloc")
     定义(符_free, "free")
     定义(符_realloc, "realloc")
     定义(符_memalign, "memalign")
     定义(符_calloc, "calloc")
# else
     定义(符_sigsetjmp, "sigsetjmp")
     定义(符___sigsetjmp, "__sigsetjmp")
     定义(符_siglongjmp, "siglongjmp")
# endif
     定义(符_mmap, "mmap")
     定义(符_munmap, "munmap")
     定义(符_memcmp, "memcmp")
     定义(符_strlen, "strlen")
     定义(符_strcpy, "strcpy")
     定义(符_strncpy, "strncpy")
     定义(符_strcmp, "strcmp")
     定义(符_strncmp, "strncmp")
     定义(符_strcat, "strcat")
     定义(符_strchr, "strchr")
     定义(符_strdup, "strdup")
     定义(符_setjmp, "setjmp")
     定义(符__setjmp, "_setjmp")
     定义(符_longjmp, "longjmp")
#endif

/* Tiny Assembler */
 定义_汇编目录(byte)              /* must be first directive */
 定义_汇编目录(word)
 定义_汇编目录(align)
 定义_汇编目录(balign)
 定义_汇编目录(p2align)
 定义_汇编目录(set)
 定义_汇编目录(skip)
 定义_汇编目录(space)
 定义_汇编目录(string)
 定义_汇编目录(asciz)
 定义_汇编目录(ascii)
 定义_汇编目录(file)
 定义_汇编目录(globl)
 定义_汇编目录(global)
 定义_汇编目录(weak)
 定义_汇编目录(hidden)
 定义_汇编目录(ident)
 定义_汇编目录(size)
 定义_汇编目录(type)
 定义_汇编目录(text)
 定义_汇编目录(data)
 定义_汇编目录(bss)
 定义_汇编目录(previous)
 定义_汇编目录(pushsection)
 定义_汇编目录(popsection)
 定义_汇编目录(fill)
 定义_汇编目录(rept)
 定义_汇编目录(endr)
 定义_汇编目录(org)
 定义_汇编目录(quad)
#if defined(ZHI_TARGET_I386)
 定义_汇编目录(code16)
 定义_汇编目录(code32)
#elif defined(ZHI_TARGET_X86_64)
 定义_汇编目录(code64)
#endif
 定义_汇编目录(short)
 定义_汇编目录(long)
 定义_汇编目录(int)
 定义_汇编目录(section)            /* must be last directive */

#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
#include "i386-tok.h"
#endif
