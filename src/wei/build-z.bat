chcp 936
@rem ------------------------------------------------------  //rem:为注释命令,回显；@rem：为注释命令,不回显
@rem batch file to build zhi using mingw, msvc or zhi itself 
@rem ------------------------------------------------------
@echo off                                                    //关闭回显功能
setlocal                                                     //临时环境变量
if (%1)==(-clean) goto :cleanup                              //%1：就是表示批处理的第一个参数，,使用shift命令可以对参数进行偏移，从而取到更多的参数。%1就是表示批处理的第一个参数，%~1表示删除参数外面的引号，在cmd中输bai入命令，test.bat "test"；%1表示的是“test“。%~1表示的是test，没有了双引号。
set CC=zhi
set /p VERSION= < ..\VERSION                                 //set /p 是不换行输出，echo是换行输出
set INST=
set BIN=
set DOC=no
set EXES_ONLY=no
goto :a0
:a2                                                          // :goto语句可以识别
shift                                                        //shift  自动将%1参数赋值给%0，%2参数赋值给%1，%3参数赋值给%2...,以此类推，像是偏移。
:a3
shift
:a0
if not (%1)==(-c) goto :a1
set CC=%~2                                                   //%~2,标识处理过的字符，如果不借助其他其他命令，批处理做多接收9个额外的参数。
if (%2)==(cl) set CC=@call :cl                               //@call :cl ,调用cl标号后面的内容，执行完之后返回到@call :cl，然后执行下一句；
goto :a2
:a1
if (%1)==(-t) set T=%2&& goto :a2
if (%1)==(-v) set VERSION=%~2&& goto :a2
if (%1)==(-i) set INST=%2&& goto :a2
if (%1)==(-b) set BIN=%2&& goto :a2
if (%1)==(-d) set DOC=yes&& goto :a3
if (%1)==(-x) set EXES_ONLY=yes&& goto :a3
if (%1)==() goto :p1
:usage
echo usage: build-zhi.bat [ options ... ]
echo options:
echo   -c prog              use prog (gcc/zhi/cl) to compile zhi
echo   -c "prog options"    use prog with options to compile zhi
echo   -t 32/64             force 32/64 bit default target
echo   -v "version"         set zhi version
echo   -i zhidir            install zhi into zhidir
echo   -b bindir           optionally install binaries into bindir elsewhere
echo   -d                   create zhi-doc.html too (needs makeinfo)
echo   -x                   just create the executables
echo   -clean               delete all previously produced files and directories
exit /B 1

@rem ------------------------------------------------------
@rem sub-routines

:cleanup
set LOG=echo
%LOG% removing files:
for %%f in (*zhi.exe hexinku.dll lib\*.a) do call :del_file %%f                 //for 变量1 in（元素挨个复制给前面的“变量1”）do 使用“变量1”
for %%f in (..\config.h ..\config.texi) do call :del_file %%f
for %%f in (include\*.h) do @if exist ..\%%f call :del_file %%f
for %%f in (include\zhilibc.h examples\hexinku_test.c) do call :del_file %%f
for %%f in (lib\*.o *.o *.obj *.def *.pdb *.lib *.exp *.ilk) do call :del_file %%f
%LOG% removing directories:
for %%f in (doc hexinku) do call :del_dir %%f
%LOG% done.
exit /B 0
:del_file
if exist %1 del %1 && %LOG%   %1
exit /B 0
:del_dir
if exist %1 rmdir /Q/S %1 && %LOG%   %1
exit /B 0

:cl
@echo off
set CMD=cl
:c0
set ARG=%1
set ARG=%ARG:.dll=.lib%
if (%1)==(-shared) set ARG=-LD
if (%1)==(-o) shift && set ARG=-Fe%2
set CMD=%CMD% %ARG%
shift
if not (%1)==() goto :c0
echo on
%CMD% -O1 -W2 -Zi -MT -GS- -nologo -link -opt:ref,icf
@exit /B %ERRORLEVEL%

@rem ------------------------------------------------------
@rem main program

:p1
if not %T%_==_ goto :p2
set T=32
if %PROCESSOR_ARCHITECTURE%_==AMD64_ set T=64
if %PROCESSOR_ARCHITEW6432%_==AMD64_ set T=64
:p2
if "%CC:~-3%"=="gcc" set CC=%CC% -Os -s -static
set D32=-DZHI_TARGET_PE -DZHI_TARGET_I386
set D64=-DZHI_TARGET_PE -DZHI_TARGET_X86_64
set P32=i386-win32
set P64=x86_64-win32
if %T%==64 goto :t64
set D=%D32%
set DX=%D64%
set PX=%P64%
goto :p3
:t64
set D=%D64%
set DX=%D32%
set PX=%P32%
goto :p3

:p3
@echo on

:config.h
echo>..\config.h #define ZHI_VERSION "%VERSION%"
echo>> ..\config.h #ifdef ZHI_TARGET_X86_64
echo>> ..\config.h #define ZHI_HEXINKU1 "hexinku1-64.a"
echo>> ..\config.h #else
echo>> ..\config.h #define ZHI_HEXINKU1 "hexinku1-32.a"
echo>> ..\config.h #endif

for %%f in (*zhi.exe *zhi.dll) do @del %%f

:compiler
%CC% -o hexinku.dll -shared ..\hexinku.c %D% -DHEXINKU_AS_DLL
@if errorlevel 1 goto :the_end
%CC% -o zhi.exe ..\zhi.c hexinku.dll %D% -DONE_SOURCE"=0"
%CC% -o %PX%-zhi.exe ..\zhi.c %DX%

@if (%EXES_ONLY%)==(yes) goto :files-done

if not exist hexinku mkdir hexinku                                      //mkdir创建文件夹及其子文件夹
if not exist doc mkdir doc
copy>nul ..\include\*.h include                                 //>nul:左侧的> 是重定向符，nul是空值。>nul把执行结果定向到空，。意思便是不返回任何结果，无论它是错是对。
copy>nul ..\zhilibc.h include
copy>nul ..\hexinku.h hexinku
copy>nul ..\tests\hexinku_test.c examples
copy>nul zhi-win32.txt doc

.\zhi -impdef hexinku.dll -o hexinku\hexinku.def           //核心库.c里的定义的创建定义文件
@if errorlevel 1 goto :the_end

:hexinku1.a
@set O1=hexinku1.o crt1.o crt1w.o wincrt1.o wincrt1w.o dllcrt1.o dllmain.o chkstk.o
.\zhi -m32 -c ../lib/hexinku1.c
.\zhi -m32 -c lib/crt1.c
.\zhi -m32 -c lib/crt1w.c
.\zhi -m32 -c lib/wincrt1.c
.\zhi -m32 -c lib/wincrt1w.c
.\zhi -m32 -c lib/dllcrt1.c
.\zhi -m32 -c lib/dllmain.c
.\zhi -m32 -c lib/chkstk.S
.\zhi -m32 -c ../lib/alloca86.S
.\zhi -m32 -c ../lib/alloca86-bt.S
.\zhi -m32 -ar lib/hexinku1-32.a %O1% alloca86.o alloca86-bt.o
@if errorlevel 1 goto :the_end
.\zhi -m64 -c ../lib/hexinku1.c
.\zhi -m64 -c lib/crt1.c
.\zhi -m64 -c lib/crt1w.c
.\zhi -m64 -c lib/wincrt1.c
.\zhi -m64 -c lib/wincrt1w.c
.\zhi -m64 -c lib/dllcrt1.c
.\zhi -m64 -c lib/dllmain.c
.\zhi -m64 -c lib/chkstk.S
.\zhi -m64 -c ../lib/alloca86_64.S
.\zhi -m64 -c ../lib/alloca86_64-bt.S
.\zhi -m64 -ar lib/hexinku1-64.a %O1% alloca86_64.o alloca86_64-bt.o
@if errorlevel 1 goto :the_end
.\zhi -m%T% -c ../lib/bcheck.c -o lib/bcheck.o -g
.\zhi -m%T% -c ../lib/bt-exe.c -o lib/bt-exe.o
.\zhi -m%T% -c ../lib/bt-log.c -o lib/bt-log.o
.\zhi -m%T% -c ../lib/bt-dll.c -o lib/bt-dll.o

:zhi-doc.html
@if not (%DOC%)==(yes) goto :doc-done
echo>..\config.texi @set VERSION %VERSION%
cmd /c makeinfo --html --no-split ../zhi-doc.texi -o doc/zhi-doc.html
:doc-done

:files-done
for %%f in (*.o *.def) do @del %%f

:copy-install
@if (%INST%)==() goto :the_end
if not exist %INST% mkdir %INST%
@if (%BIN%)==() set BIN=%INST%
if not exist %BIN% mkdir %BIN%
for %%f in (*zhi.exe *zhi.dll) do @copy>nul %%f %BIN%\%%f
@if not exist %INST%\lib mkdir %INST%\lib
for %%f in (lib\*.a lib\*.o lib\*.def) do @copy>nul %%f %INST%\%%f
for %%f in (include examples hexinku doc) do @xcopy>nul /s/i/q/y %%f %INST%\%%f

:the_end
exit /B %ERRORLEVEL%
