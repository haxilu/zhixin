#include <stdio.h>
#include <stdlib.h>	// atoi()

int fib(n)
{
	if (n <= 2)
		return 1;
	else
		return fib(n-1) + fib(n-2);
}

int main(int 参数数量, char **参数数组) 
{
	int n;
	if (参数数量 < 2) {
		printf("usage: fib n\n"
			   "Compute nth Fibonacci number\n");
		return 1;
	}
		
	n = atoi(参数数组[1]);
	printf("fib(%d) = %d\n", n, fib(n));
	return 0;
}
