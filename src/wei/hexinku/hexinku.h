/*该hexinku库使您可以将ZHI用作动态代码生成的后端。 hexinku.h概述API.
 * 只在zhi.h第297行引用
 * */
#ifndef HEXINKU_H
#define HEXINKU_H

#ifndef HEXINKU接口
# define HEXINKU接口
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct 知心状态机 知心状态机;


typedef void (*ZHIErrorFunc)(void *不透明, const char *msg);

/* 创建一个新的ZHI编译上下文 */
HEXINKU接口 知心状态机 *初始化状态机(void);

/* 释放ZHI编译上下文 */
HEXINKU接口 void 释放状态机(知心状态机 *s);

/* 在运行时设置CONFIG_ZHIDIR */
HEXINKU接口 void 设置库路径(知心状态机 *s, const char *path);

/* 设置错误/警告显示回调 */
HEXINKU接口 void 设置错误警告显示回调(知心状态机 *s, void *错误_不透明, ZHIErrorFunc 错误_函数);

/* 返回错误/警告回调 */
HEXINKU接口 ZHIErrorFunc 返回错误警告回调(知心状态机 *s);

/* 返回错误/警告回调不透明指针 */
HEXINKU接口 void *返回错误警告回调不透明指针(知心状态机 *s);

/* 从命令行设置选项（支持多个） */
HEXINKU接口 void 设置编译选项(知心状态机 *s, const char *str);

/*****************************/
/* 预处理器 */

/* 添加包含路径 */
HEXINKU接口 int 添加包含路径(知心状态机 *s, const char *pathname);

/* 添加系统包含路径 */
HEXINKU接口 int 添加到系统包含路径(知心状态机 *s, const char *pathname);

/* 定义预处理程序符号“ sym”。 可以放可选值 */
HEXINKU接口 void 定义预处理程序符号(知心状态机 *s, const char *sym, const char *value);

/* 未定义预处理符号“ sym” */
HEXINKU接口 void 未定义预处理符号(知心状态机 *s, const char *sym);

/*****************************/
/* 编译 */

/* 添加文件（C文件，dll，对象，库，ld脚本）。 如果错误，则返回-1。 */
HEXINKU接口 int 添加文件(知心状态机 *s, const char *文件名);

/* 编译包含C源代码的字符串。 如果错误，则返回-1。 */
HEXINKU接口 int 编译包含ZHI源代码的字符串(知心状态机 *s, const char *buf);

/*****************************/
/* 链接命令 */

/* 设置输出类型。 任何编译之前必须先调用 */
HEXINKU接口 int 设置输出类型(知心状态机 *s, int 输出_类型);
#define ZHI_输出_内存中运行   1 /* 输出将在内存中运行（默认） */
#define ZHI_输出_EXE      2 /* 可执行文件 */
#define ZHI_输出_DLL      3 /* 动态库 */
#define ZHI_输出_目标文件      4 /* 目标文件 */
#define ZHI_输出_预处理 5 /* 仅预处理（内部使用） */

HEXINKU接口 int 添加库路径(知心状态机 *s, const char *pathname);

/* 库名称与“ -l”选项的参数相同 */
HEXINKU接口 int 使用库名称添加库(知心状态机 *s, const char *libraryname);

/* 在已编译的程序中添加符号 */
HEXINKU接口 int 在已编译的程序中添加符号(知心状态机 *s, const char *name, const void *val);

/* 输出可执行文件，库文件或目标文件。 请勿在之前调用zhi_relocate（）。 */
HEXINKU接口 int 输出可执行文件或库文件或目标文件(知心状态机 *s, const char *文件名);

/* 链接并运行main（）函数并返回其值。 请勿在之前调用zhi_relocate（）。*/
HEXINKU接口 int ZHI_运行(知心状态机 *s, int 参数数量, char **参数数组);

/* 执行所有重定位（在使用zhi_get_symbol（）之前需要） */
HEXINKU接口 int 执行所有重定位(知心状态机 *状态机1, void *ptr);
/* 'ptr'的可能值：
    -ZHI_自动_重新排列：在内部分配和管理内存
    -NULL：返回以下步骤所需的内存大小
    -内存地址：将代码复制到调用方传递的内存中
    如果出错，则返回-1。 */
#define ZHI_自动_重新排列 (void*)1

/* 返回符号值；如果找不到，则返回NULL */
HEXINKU接口 void *获取elf符号值(知心状态机 *s, const char *name);

/* 返回符号值；如果找不到，则返回NULL */
HEXINKU接口 void 列出elf符号名称和值(知心状态机 *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val));

#ifdef __cplusplus
}
#endif

#endif
