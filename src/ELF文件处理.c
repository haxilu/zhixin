#include "zhi.h"

/* 定义它以在重定位处理期间获得一些调试输出  */
#undef DEBUG_RELOC

/********************************************************/
/* 全局变量 */

/* elf版本信息 */
struct 符号_版本 {
    char *lib;
    char *version;
    int out_index;
    int prev_same_lib;
};

#define 数量_符号_版本数     状态机1->数量_符号_版本数
#define 符号_版本数        状态机1->符号_版本数
#define 数量_符号_到_版本   状态机1->数量_符号_到_版本
#define 符号_到_版本      状态机1->符号_到_版本
#define dt_必须的数码       状态机1->dt_必须的数码
#define versym_section      状态机1->versym_section
#define verneed_section     状态机1->verneed_section

/* 特殊标志，指示该部分不应链接到其他部分 */
#define SHF_PRIVATE 0x80000000
/* 节 是动态单词表_节 */
#define SHF_DYNSYM 0x40000000

/* ------------------------------------------------------------------------- */

静态_函数 void zhi_elf_新建(知心状态机 *s)
{
    知心状态机 *状态机1 = s;
    /* 无节数是0 */
    动态数组_追加元素(&s->段数, &s->数量_段数, NULL);
    生成代码_段 = 创建_节(s, ".text", SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR);
    初始化数据_部分 = 创建_节(s, ".data", SHT_PROGBITS, SHF_ALLOC | SHF_WRITE);
    未初始化数据_部分 = 创建_节(s, ".bss", SHT_NOBITS, SHF_ALLOC | SHF_WRITE);
    通用_部分 = 创建_节(s, ".common", SHT_NOBITS, SHF_PRIVATE);
    通用_部分->sh_num = SHN_COMMON;

    /* 始终为链接阶段生成符号 */
    单词表_部分 = 新建_字符表(s, ".全局单词表副本", SHT_SYMTAB, 0,
                                ".字符表",
                                ".hashtab", SHF_PRIVATE);
    s->全局单词表副本 = 单词表_部分;

    /* 动态符号的专用单词表 */
    s->动态单词表_部分 = 新建_字符表(s, ".dynsymtab", SHT_SYMTAB, SHF_PRIVATE|SHF_DYNSYM,
                                      ".dynstrtab",
                                      ".dynhashtab", SHF_PRIVATE);
    get_额外_符号_属性(s, 0, 1);
}

#ifdef 配置_ZHI_边界检查
静态_函数 void zhielf_bounds_new(知心状态机 *s)
{
    知心状态机 *状态机1 = s;
    /* 创建边界部分 */
    全局边界_部分 = 创建_节(s, ".bounds",
                                 SHT_PROGBITS, SHF_ALLOC);
    本地边界_部分 = 创建_节(s, ".lbounds",
                                  SHT_PROGBITS, SHF_ALLOC);
}
#endif

静态_函数 void zhielf_stab_新建(知心状态机 *s)
{
    知心状态机 *状态机1 = s;
    int shf = 0;
#ifdef ZHI_配置_记录回滚
    /* 包括带有独立回溯支持的stab信息 */
    if (s->执行_跟踪 && s->输出_类型 != ZHI_输出_内存中运行)
        shf = SHF_ALLOC;
#endif
    符号调试_部分 = 创建_节(s, ".stab", SHT_PROGBITS, shf);
    符号调试_部分->sh_entsize = sizeof(单词表_符号);
    符号调试_部分->sh_addralign = sizeof ((单词表_符号*)0)->n_value;
    符号调试_部分->link = 创建_节(s, ".stabstr", SHT_STRTAB, shf);
    /* 首先输入 */
    处理_单词表字符串(s, "", 0, 0, 0, 0);
}

static void 释放_节(段 *s)
{
    内存_释放(s->data);
}

静态_函数 void zhielf_删除(知心状态机 *状态机1)
{
    int i;

#ifndef ELF_OBJ_ONLY
    /* free symbol versions */
    for (i = 0; i < 数量_符号_版本数; i++) {
        内存_释放(符号_版本数[i].version);
        内存_释放(符号_版本数[i].lib);
    }
    内存_释放(符号_版本数);
    内存_释放(符号_到_版本);
#endif

    /* free all 段数 */
    for(i = 1; i < 状态机1->数量_段数; i++)
        释放_节(状态机1->段数[i]);
    动态数组_重分配容量(&状态机1->段数, &状态机1->数量_段数);

    for(i = 0; i < 状态机1->数量_私有_节数; i++)
        释放_节(状态机1->私有_节数[i]);
    动态数组_重分配容量(&状态机1->私有_节数, &状态机1->数量_私有_节数);

    /* free any loaded DLLs */
#ifdef ZHI_是_本机
    for ( i = 0; i < 状态机1->数量_已加载的_dll数组; i++) {
        DLL参考 *ref = 状态机1->已加载的_dll数组[i];
        if ( ref->handle )
# ifdef _WIN32
            FreeLibrary((HMODULE)ref->handle);
# else
            dl关闭(ref->handle);
# endif
    }
#endif
    /* free loaded dlls array */
    动态数组_重分配容量(&状态机1->已加载的_dll数组, &状态机1->数量_已加载的_dll数组);
    内存_释放(状态机1->符号_额外属性);

    单词表_部分 = NULL; /* for run开关.c:运行时_打印行() */
}

/* 保存节数据状态 */
静态_函数 void 保存_段_数据状态(知心状态机 *状态机1)
{
    段 *段; int i;
    for (i = 1; i < 状态机1->数量_段数; i++)
    {
        段 = 状态机1->段数[i];
        段->sh_偏移 = 段->数据_偏移;
    }
    /* 在编译期间禁用符号哈希 */
    段 = 状态机1->全局单词表副本, 段->重定位 = 段->hash, 段->hash = NULL;
#if defined ZHI_TARGET_X86_64 && defined ZHI_TARGET_PE
    状态机1->uw_sym = 0;
#endif
}

/* 编译结束时，将所有UNDEF符号转换为全局符号，然后与以前存在的符号合并 */
静态_函数 void 结束_段_数据状态(知心状态机 *状态机1)
{
    段 *s = 状态机1->全局单词表副本;
    int first_sym, 数量_syms, *tr, i;

    first_sym = s->sh_偏移 / sizeof (ELF符号);
    数量_syms = s->数据_偏移 / sizeof (ELF符号) - first_sym;
    s->数据_偏移 = s->sh_偏移;
    s->link->数据_偏移 = s->link->sh_偏移;
    s->hash = s->重定位, s->重定位 = NULL;
    tr = 内存_初始化(数量_syms * sizeof *tr);

    for (i = 0; i < 数量_syms; ++i) {
        ELF符号 *sym = (ELF符号*)s->data + first_sym + i;
        if (sym->st_shndx == SHN_UNDEF
            && ELFW(ST_BIND)(sym->st_info) == STB_LOCAL)
            sym->st_info = ELFW(ST_INFO)(STB_GLOBAL, ELFW(ST_TYPE)(sym->st_info));
        tr[i] = 设置_elf_符号(s, sym->st_value, sym->st_size, sym->st_info,
            sym->st_other, sym->st_shndx, (char*)s->link->data + sym->st_name);
    }
    /* 现在更新重定位 */
    for (i = 1; i < 状态机1->数量_段数; i++) {
        段 *sr = 状态机1->段数[i];
        if (sr->sh_type == SHT_RELX && sr->link == s) {
            ElfW_Rel *rel = (ElfW_Rel*)(sr->data + sr->sh_偏移);
            ElfW_Rel *rel_end = (ElfW_Rel*)(sr->data + sr->数据_偏移);
            for (; rel < rel_end; ++rel) {
                int n = ELFW(R_SYM)(rel->r_info) - first_sym;
                if (n < 0) 错误_打印("内部：重定位中无效的符号索引");
                rel->r_info = ELFW(R_INFO)(tr[n], ELFW(R_TYPE)(rel->r_info));
            }
        }
    }
    内存_释放(tr);
}
/*创建_节()创建一个新部分。每个部分均假定使用ELF文件语义*/
静态_函数 段 *创建_节(知心状态机 *状态机1, const char *name, int sh_type, int sh_flags)
{
    段 *sec;

    sec = 内存_初始化(sizeof(段) + strlen(name));
    sec->状态机1 = 状态机1;
    strcpy(sec->name, name);
    sec->sh_type = sh_type;
    sec->sh_flags = sh_flags;
    switch(sh_type) {
    case SHT_GNU_versym:
        sec->sh_addralign = 2;
        break;
    case SHT_HASH:
    case SHT_REL:
    case SHT_RELA:
    case SHT_DYNSYM:
    case SHT_SYMTAB:
    case SHT_DYNAMIC:
    case SHT_GNU_verneed:
    case SHT_GNU_verdef:
        sec->sh_addralign = 指针_大小;
        break;
    case SHT_STRTAB:
        sec->sh_addralign = 1;
        break;
    default:
        sec->sh_addralign =  指针_大小; /* gcc/pcc default alignment */
        break;
    }

    if (sh_flags & SHF_PRIVATE) {
        动态数组_追加元素(&状态机1->私有_节数, &状态机1->数量_私有_节数, sec);
    } else {
        sec->sh_num = 状态机1->数量_段数;
        动态数组_追加元素(&状态机1->段数, &状态机1->数量_段数, sec);
    }

    return sec;
}

静态_函数 段 *新建_字符表(知心状态机 *状态机1,
                           const char *单词表_名称, int sh_type, int sh_flags,
                           const char *strtab_name,
                           const char *hash_name, int hash_sh_flags)
{
    段 *全局单词表副本, *字符表, *hash;
    int *ptr, 数量_buckets;

    全局单词表副本 = 创建_节(状态机1, 单词表_名称, sh_type, sh_flags);
    全局单词表副本->sh_entsize = sizeof(ElfW(符号));
    字符表 = 创建_节(状态机1, strtab_name, SHT_STRTAB, sh_flags);
    处理_elf_字符串(字符表, "");
    全局单词表副本->link = 字符表;
    处理_elf_符号(全局单词表副本, 0, 0, 0, 0, 0, NULL);

    数量_buckets = 1;

    hash = 创建_节(状态机1, hash_name, SHT_HASH, hash_sh_flags);
    hash->sh_entsize = sizeof(int);
    全局单词表副本->hash = hash;
    hash->link = 全局单词表副本;

    ptr = 段_ptr_添加(hash, (2 + 数量_buckets + 1) * sizeof(int));
    ptr[0] = 数量_buckets;
    ptr[1] = 1;
    memset(ptr + 2, 0, (数量_buckets + 1) * sizeof(int));
    return 全局单词表副本;
}

/* 重新分配节并将其内容设置为零 */
静态_函数 void 节_重新分配内存(段 *sec, unsigned long new_size)
{
    unsigned long size;
    unsigned char *data;

    size = sec->data_allocated;
    if (size == 0)
        size = 1;
    while (size < new_size)
        size = size * 2;
    data = 内存_重分配容量(sec->data, size);
    memset(data + sec->data_allocated, 0, size - sec->data_allocated);
    sec->data = data;
    sec->data_allocated = size;
}

/* 保留从当前偏移量开始的“ sec”节中每个“ align”至少对齐的“ size”字节，并返回对齐的偏移量 */
静态_函数 size_t 返回_节_对齐偏移量(段 *sec, 目标地址_类型 size, int align)
{
    size_t offset, offset1;

    offset = (sec->数据_偏移 + align - 1) & -align;
    offset1 = offset + size;
    if (sec->sh_type != SHT_NOBITS && offset1 > sec->data_allocated)
        节_重新分配内存(sec, offset1);
    sec->数据_偏移 = offset1;
    if (align > sec->sh_addralign)
        sec->sh_addralign = align;
    return offset;
}

/* 在sec-> data_offset中的“ sec”部分中至少保留“ size”个字节。 */
静态_函数 void *段_ptr_添加(段 *sec, 目标地址_类型 size)
{
    size_t offset = 返回_节_对齐偏移量(sec, size, 1);
    return sec->data + offset;
}

/* reserve at least 'size' bytes from section start */
静态_函数 void 段_保留(段 *sec, unsigned long size)
{
    if (size > sec->data_allocated)
        节_重新分配内存(sec, size);
    if (size > sec->数据_偏移)
        sec->数据_偏移 = size;
}

static 段 *find_段_create (知心状态机 *状态机1, const char *name, int create)
{
    段 *sec;
    int i;
    for(i = 1; i < 状态机1->数量_段数; i++) {
        sec = 状态机1->段数[i];
        if (!strcmp(name, sec->name))
            return sec;
    }
    /* 段数 are created as PROGBITS */
    return create ? 创建_节(状态机1, name, SHT_PROGBITS, SHF_ALLOC) : NULL;
}

/* return a reference to a section, and create it if it does not
   exists */
静态_函数 段 *查找_段(知心状态机 *状态机1, const char *name)
{
    return find_段_create (状态机1, name, 1);
}

/* ------------------------------------------------------------------------- */

静态_函数 int 处理_elf_字符串(段 *s, const char *sym)
{
    int offset, len;
    char *ptr;

    len = strlen(sym) + 1;
    offset = s->数据_偏移;
    ptr = 段_ptr_添加(s, len);
    memmove(ptr, sym, len);
    return offset;
}

/* elf symbol hashing function */
static unsigned long elf_hash(const unsigned char *name)
{
    unsigned long h = 0, g;

    while (*name) {
        h = (h << 4) + *name++;
        g = h & 0xf0000000;
        if (g)
            h ^= g >> 24;
        h &= ~g;
    }
    return h;
}

/* rebuild hash table of section s */
/* NOTE: we do factorize the hash table code to go faster */
static void rebuild_hash(段 *s, unsigned int 数量_buckets)
{
    ElfW(符号) *sym;
    int *ptr, *hash, 数量_syms, sym_index, h;
    unsigned char *字符表;

    字符表 = s->link->data;
    数量_syms = s->数据_偏移 / sizeof(ElfW(符号));

    if (!数量_buckets)
        数量_buckets = ((int*)s->hash->data)[0];

    s->hash->数据_偏移 = 0;
    ptr = 段_ptr_添加(s->hash, (2 + 数量_buckets + 数量_syms) * sizeof(int));
    ptr[0] = 数量_buckets;
    ptr[1] = 数量_syms;
    ptr += 2;
    hash = ptr;
    memset(hash, 0, (数量_buckets + 1) * sizeof(int));
    ptr += 数量_buckets + 1;

    sym = (ElfW(符号) *)s->data + 1;
    for(sym_index = 1; sym_index < 数量_syms; sym_index++) {
        if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            h = elf_hash(字符表 + sym->st_name) % 数量_buckets;
            *ptr = hash[h];
            hash[h] = sym_index;
        } else {
            *ptr = 0;
        }
        ptr++;
        sym++;
    }
}

/* return the symbol number */
静态_函数 int 处理_elf_符号(段 *s, 目标地址_类型 value, unsigned long size,
    int info, int other, int shndx, const char *name)
{
    int name_offset, sym_index;
    int nbuckets, h;
    ElfW(符号) *sym;
    段 *hs;

    sym = 段_ptr_添加(s, sizeof(ElfW(符号)));
    if (name && name[0])
        name_offset = 处理_elf_字符串(s->link, name);
    else
        name_offset = 0;
    /* XXX: endianness */
    sym->st_name = name_offset;
    sym->st_value = value;
    sym->st_size = size;
    sym->st_info = info;
    sym->st_other = other;
    sym->st_shndx = shndx;
    sym_index = sym - (ElfW(符号) *)s->data;
    hs = s->hash;
    if (hs) {
        int *ptr, *base;
        ptr = 段_ptr_添加(hs, sizeof(int));
        base = (int *)hs->data;
        /* only add global or weak symbols. */
        if (ELFW(ST_BIND)(info) != STB_LOCAL) {
            /* add another hashing entry */
            nbuckets = base[0];
            h = elf_hash((unsigned char *)s->link->data + name_offset) % nbuckets;
            *ptr = base[2 + h];
            base[2 + h] = sym_index;
            base[1]++;
            /* we resize the hash table */
            hs->数量_hashed_syms++;
            if (hs->数量_hashed_syms > 2 * nbuckets) {
                rebuild_hash(s, 2 * nbuckets);
            }
        } else {
            *ptr = 0;
            base[1]++;
        }
    }
    return sym_index;
}

静态_函数 int 查找_elf_符号(段 *s, const char *name)
{
    ElfW(符号) *sym;
    段 *hs;
    int nbuckets, sym_index, h;
    const char *name1;

    hs = s->hash;
    if (!hs)
        return 0;
    nbuckets = ((int *)hs->data)[0];
    h = elf_hash((unsigned char *) name) % nbuckets;
    sym_index = ((int *)hs->data)[2 + h];
    while (sym_index != 0) {
        sym = &((ElfW(符号) *)s->data)[sym_index];
        name1 = (char *) s->link->data + sym->st_name;
        if (!strcmp(name, name1))
            return sym_index;
        sym_index = ((int *)hs->data)[2 + nbuckets + sym_index];
    }
    return 0;
}

静态_函数 int 查找_常量_符号(知心状态机 *状态机1, const char *name)
{
    int ret;
    动态字符串 cstr;
    if (状态机1->前导_下划线) {
        动态字符串_初始化(&cstr);
        动态字符串_追加单个字符(&cstr, '_');
        动态字符串_cat(&cstr, name, 0);
        name = cstr.指向字符串的指针;
    }
    ret = 查找_elf_符号(状态机1->全局单词表副本, name);
    if (状态机1->前导_下划线)
      动态字符串_释放(&cstr);
    return ret;
}

/* return elf symbol value, signal error if 'err' is nonzero, decorate
   name if FORC */
静态_函数 目标地址_类型 获取_符号_地址(知心状态机 *状态机1, const char *name, int err, int forc)
{
    int sym_index;
    ElfW(符号) *sym;

    if (forc)
      sym_index = 查找_常量_符号(状态机1, name);
    else
      sym_index = 查找_elf_符号(状态机1->全局单词表副本, name);
    sym = &((ElfW(符号) *)状态机1->全局单词表副本->data)[sym_index];
    if (!sym_index || sym->st_shndx == SHN_UNDEF) {
        if (err)
            错误_打印("未定义 %s ", name);
        return 0;
    }
    return sym->st_value;
}

/* list elf symbol names and values */
静态_函数 void ELF_符号_列表(知心状态机 *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val))
{
    ElfW(符号) *sym;
    段 *全局单词表副本;
    int sym_index, end_sym;
    const char *name;
    unsigned char sym_vis, sym_bind;

    全局单词表副本 = s->全局单词表副本;
    end_sym = 全局单词表副本->数据_偏移 / sizeof (ELF符号);
    for (sym_index = 0; sym_index < end_sym; ++sym_index) {
        sym = &((ElfW(符号) *)全局单词表副本->data)[sym_index];
        if (sym->st_value) {
            name = (char *) 全局单词表副本->link->data + sym->st_name;
            sym_bind = ELFW(ST_BIND)(sym->st_info);
            sym_vis = ELFW(ST_VISIBILITY)(sym->st_other);
            if (sym_bind == STB_GLOBAL && sym_vis == STV_DEFAULT)
                symbol_cb(ctx, name, (void*)(uintptr_t)sym->st_value);
        }
    }
}

/* return elf symbol value */
HEXINKU接口 void *获取elf符号值(知心状态机 *s, const char *name)
{
    return (void*)(uintptr_t)获取_符号_地址(s, name, 0, 1);
}

/* list elf symbol names and values */
HEXINKU接口 void 列出elf符号名称和值(知心状态机 *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val))
{
    ELF_符号_列表(s, ctx, symbol_cb);
}

#if defined ZHI_是_本机 || defined ZHI_TARGET_PE
/* return elf symbol value or error */
静态_函数 void* zhi_获取_符号_错误(知心状态机 *s, const char *name)
{
    return (void*)(uintptr_t)获取_符号_地址(s, name, 1, 1);
}
#endif

#ifndef ELF_OBJ_ONLY
static void
version_add (知心状态机 *状态机1)
{
    int i;
    ElfW(符号) *sym;
    ElfW(Verneed) *vn = NULL;
    段 *全局单词表副本;
    int sym_index, end_sym, 数量_versions = 2, 数量_entries = 0;
    ElfW(Half) *versym;
    const char *name;

    if (0 == 数量_符号_版本数)
        return;
    versym_section = 创建_节(状态机1, ".gnu.version", SHT_GNU_versym, SHF_ALLOC);
    versym_section->sh_entsize = sizeof(ElfW(Half));
    versym_section->link = 状态机1->导出的动态单词表;

    /* add needed symbols */
    全局单词表副本 = 状态机1->导出的动态单词表;
    end_sym = 全局单词表副本->数据_偏移 / sizeof (ELF符号);
    versym = 段_ptr_添加(versym_section, end_sym * sizeof(ElfW(Half)));
    for (sym_index = 0; sym_index < end_sym; ++sym_index) {
        int dllindex, verndx;
        sym = &((ElfW(符号) *)全局单词表副本->data)[sym_index];
        name = (char *) 全局单词表副本->link->data + sym->st_name;
        dllindex = 查找_elf_符号(状态机1->动态单词表_部分, name);
        verndx = (dllindex && dllindex < 数量_符号_到_版本)
                 ? 符号_到_版本[dllindex] : -1;
        if (verndx >= 0) {
            if (!符号_版本数[verndx].out_index)
              符号_版本数[verndx].out_index = 数量_versions++;
            versym[sym_index] = 符号_版本数[verndx].out_index;
        } else
          versym[sym_index] = 0;
    }
    /* generate verneed section, but not when it will be empty.  Some
       dynamic linkers look at their contents even when DTVERNEEDNUM and
       section size is zero.  */
    if (数量_versions > 2) {
        verneed_section = 创建_节(状态机1, ".gnu.version_r",
                                      SHT_GNU_verneed, SHF_ALLOC);
        verneed_section->link = 状态机1->导出的动态单词表->link;
        for (i = 数量_符号_版本数; i-- > 0;) {
            struct 符号_版本 *sv = &符号_版本数[i];
            int n_same_libs = 0, prev;
            size_t vnofs;
            ElfW(Vernaux) *vna = 0;
            if (sv->out_index < 1)
              continue;
            vnofs = 返回_节_对齐偏移量(verneed_section, sizeof(*vn), 1);
            vn = (ElfW(Verneed)*)(verneed_section->data + vnofs);
            vn->vn_version = 1;
            vn->vn_file = 处理_elf_字符串(verneed_section->link, sv->lib);
            vn->vn_aux = sizeof (*vn);
            do {
                prev = sv->prev_same_lib;
                if (sv->out_index > 0) {
                    vna = 段_ptr_添加(verneed_section, sizeof(*vna));
                    vna->vna_hash = elf_hash ((const unsigned char *)sv->version);
                    vna->vna_flags = 0;
                    vna->vna_other = sv->out_index;
                    sv->out_index = -2;
                    vna->vna_name = 处理_elf_字符串(verneed_section->link, sv->version);
                    vna->vna_next = sizeof (*vna);
                    n_same_libs++;
                }
                if (prev >= 0)
                  sv = &符号_版本数[prev];
            } while(prev >= 0);
            vna->vna_next = 0;
            vn = (ElfW(Verneed)*)(verneed_section->data + vnofs);
            vn->vn_cnt = n_same_libs;
            vn->vn_next = sizeof(*vn) + n_same_libs * sizeof(*vna);
            数量_entries++;
        }
        if (vn)
          vn->vn_next = 0;
        verneed_section->sh_info = 数量_entries;
    }
    dt_必须的数码 = 数量_entries;
}
#endif

/* add an elf symbol : check if it is already defined and patch
   it. Return symbol index. NOTE that sh_num can be SHN_UNDEF. */
静态_函数 int 设置_elf_符号(段 *s, 目标地址_类型 value, unsigned long size,
                       int info, int other, int shndx, const char *name)
{
    知心状态机 *状态机1 = s->状态机1;
    ElfW(符号) *esym;
    int sym_bind, sym_index, sym_type, esym_bind;
    unsigned char sym_vis, esym_vis, new_vis;

    sym_bind = ELFW(ST_BIND)(info);
    sym_type = ELFW(ST_TYPE)(info);
    sym_vis = ELFW(ST_VISIBILITY)(other);

    if (sym_bind != STB_LOCAL) {
        /* we search global or weak symbols */
        sym_index = 查找_elf_符号(s, name);
        if (!sym_index)
            goto do_def;
        esym = &((ElfW(符号) *)s->data)[sym_index];
        if (esym->st_value == value && esym->st_size == size && esym->st_info == info
            && esym->st_other == other && esym->st_shndx == shndx)
            return sym_index;
        if (esym->st_shndx != SHN_UNDEF) {
            esym_bind = ELFW(ST_BIND)(esym->st_info);
            /* propagate the most constraining visibility */
            /* STV_DEFAULT(0)<STV_PROTECTED(3)<STV_HIDDEN(2)<STV_INTERNAL(1) */
            esym_vis = ELFW(ST_VISIBILITY)(esym->st_other);
            if (esym_vis == STV_DEFAULT) {
                new_vis = sym_vis;
            } else if (sym_vis == STV_DEFAULT) {
                new_vis = esym_vis;
            } else {
                new_vis = (esym_vis < sym_vis) ? esym_vis : sym_vis;
            }
            esym->st_other = (esym->st_other & ~ELFW(ST_VISIBILITY)(-1))
                             | new_vis;
            other = esym->st_other; /* in case we have to patch esym */
            if (shndx == SHN_UNDEF) {
                /* ignore adding of undefined symbol if the
                   corresponding symbol is already defined */
            } else if (sym_bind == STB_GLOBAL && esym_bind == STB_WEAK) {
                /* global overrides weak, so patch */
                goto do_patch;
            } else if (sym_bind == STB_WEAK && esym_bind == STB_GLOBAL) {
                /* weak is ignored if already global */
            } else if (sym_bind == STB_WEAK && esym_bind == STB_WEAK) {
                /* keep first-found weak definition, ignore subsequents */
            } else if (sym_vis == STV_HIDDEN || sym_vis == STV_INTERNAL) {
                /* ignore hidden symbols after */
            } else if ((esym->st_shndx == SHN_COMMON
                            || esym->st_shndx == 未初始化数据_部分->sh_num)
                        && (shndx < SHN_LORESERVE
                            && shndx != 未初始化数据_部分->sh_num)) {
                /* data symbol gets 优先权 over common/bss */
                goto do_patch;
            } else if (shndx == SHN_COMMON || shndx == 未初始化数据_部分->sh_num) {
                /* data symbol keeps 优先权 over common/bss */
            } else if (s->sh_flags & SHF_DYNSYM) {
                /* we accept that two DLL define the same symbol */
	    } else if (esym->st_other & ST_ASM_SET) {
		/* If the existing symbol came from an asm .set
		   we can override.  */
		goto do_patch;
            } else {
#if 0
                printf("new_bind=%x new_shndx=%x new_vis=%x old_bind=%x old_shndx=%x old_vis=%x\n",
                       sym_bind, shndx, new_vis, esym_bind, esym->st_shndx, esym_vis);
#endif
                zhi_错误_不中止("'%s' defined twice", name);
            }
        } else {
        do_patch:
            esym->st_info = ELFW(ST_INFO)(sym_bind, sym_type);
            esym->st_shndx = shndx;
            状态机1->新的_未定义的_符号 = 1;
            esym->st_value = value;
            esym->st_size = size;
            esym->st_other = other;
        }
    } else {
    do_def:
        sym_index = 处理_elf_符号(s, value, size,
                                ELFW(ST_INFO)(sym_bind, sym_type), other,
                                shndx, name);
    }
    return sym_index;
}

/* put relocation */
静态_函数 void 使_elf_重定位目标地址(段 *全局单词表副本, 段 *s, unsigned long offset,
                            int type, int symbol, 目标地址_类型 addend)
{
    知心状态机 *状态机1 = s->状态机1;
    char buf[256];
    段 *sr;
    ElfW_Rel *rel;

    sr = s->重定位;
    if (!sr) {
        /* if no relocation section, create it */
        snprintf(buf, sizeof(buf), REL_SECTION_FMT, s->name);
        /* if the 全局单词表副本 is allocated, then we consider the relocation
           are also */
        sr = 创建_节(s->状态机1, buf, SHT_RELX, 全局单词表副本->sh_flags);
        sr->sh_entsize = sizeof(ElfW_Rel);
        sr->link = 全局单词表副本;
        sr->sh_info = s->sh_num;
        s->重定位 = sr;
    }
    rel = 段_ptr_添加(sr, sizeof(ElfW_Rel));
    rel->r_offset = offset;
    rel->r_info = ELFW(R_INFO)(symbol, type);
#if SHT_RELX == SHT_RELA
    rel->r_addend = addend;
#endif
    if (SHT_RELX != SHT_RELA && addend)
        错误_打印("non-zero addend on REL architecture");
}

静态_函数 void 使_elf_重定位(段 *全局单词表副本, 段 *s, unsigned long offset,
                           int type, int symbol)
{
    使_elf_重定位目标地址(全局单词表副本, s, offset, type, symbol, 0);
}

/* Remove relocations for section S->重定位 starting at oldrelocoffset
   that are to the same place, retaining the last of them.  As side effect
   the relocations are sorted.  Possibly reduces the number of relocs.  */
静态_函数 void 压缩_多_重定位(段 *s, size_t oldrelocoffset)
{
    段 *sr = s->重定位;
    ElfW_Rel *r, *dest;
    ssize_t a;
    ElfW(地址) addr;

    if (oldrelocoffset + sizeof(*r) >= sr->数据_偏移)
      return;
    /* The relocs we're dealing with are the result of initializer parsing.
       So they will be mostly in order and there aren't many of them.
       Secondly we need a stable sort (which qsort isn't).  We use
       a simple insertion sort.  */
    for (a = oldrelocoffset + sizeof(*r); a < sr->数据_偏移; a += sizeof(*r)) {
	ssize_t i = a - sizeof(*r);
	addr = ((ElfW_Rel*)(sr->data + a))->r_offset;
	for (; i >= (ssize_t)oldrelocoffset &&
	       ((ElfW_Rel*)(sr->data + i))->r_offset > addr; i -= sizeof(*r)) {
	    ElfW_Rel tmp = *(ElfW_Rel*)(sr->data + a);
	    *(ElfW_Rel*)(sr->data + a) = *(ElfW_Rel*)(sr->data + i);
	    *(ElfW_Rel*)(sr->data + i) = tmp;
	}
    }

    r = (ElfW_Rel*)(sr->data + oldrelocoffset);
    dest = r;
    for (; r < (ElfW_Rel*)(sr->data + sr->数据_偏移); r++) {
	if (dest->r_offset != r->r_offset)
	  dest++;
	*dest = *r;
    }
    sr->数据_偏移 = (unsigned char*)dest - sr->data + sizeof(*r);
}

/* put stab debug information */

静态_函数 void 处理_单词表字符串(知心状态机 *状态机1, const char *str, int type, int other, int desc,
                      unsigned long value)
{
    单词表_符号 *sym;

    unsigned offset;
    if (type == N_SLINE
        && (offset = 符号调试_部分->数据_偏移)
        && (sym = (单词表_符号*)(符号调试_部分->data + offset) - 1)
        && sym->n_type == type
        && sym->n_value == value) {
        /* just update line_number in previous entry */
        sym->n_desc = desc;
        return;
    }

    sym = 段_ptr_添加(符号调试_部分, sizeof(单词表_符号));
    if (str) {
        sym->n_strx = 处理_elf_字符串(符号调试_部分->link, str);
    } else {
        sym->n_strx = 0;
    }
    sym->n_type = type;
    sym->n_other = other;
    sym->n_desc = desc;
    sym->n_value = value;
}

静态_函数 void 处理_单词表_r(知心状态机 *状态机1, const char *str, int type, int other, int desc,
                        unsigned long value, 段 *sec, int sym_index)
{
    使_elf_重定位(单词表_部分, 符号调试_部分,
                  符号调试_部分->数据_偏移 + 8,
                  sizeof ((单词表_符号*)0)->n_value == 指针_大小 ? R_DATA_PTR : R_DATA_32,
                  sym_index);
    处理_单词表字符串(状态机1, str, type, other, desc, value);
}

静态_函数 void 处理_单词表整数(知心状态机 *状态机1, int type, int other, int desc, int value)
{
    处理_单词表字符串(状态机1, NULL, type, other, desc, value);
}

静态_函数 struct 额外_符号_属性 *get_额外_符号_属性(知心状态机 *状态机1, int index, int alloc)
{
    int n;
    struct 额外_符号_属性 *tab;

    if (index >= 状态机1->数量_符号_额外属性) {
        if (!alloc)
            return 状态机1->符号_额外属性;
        /* 立即发现2的更大幂并重新分配数组 */
        n = 1;
        while (index >= n)
            n *= 2;
        tab = 内存_重分配容量(状态机1->符号_额外属性, n * sizeof(*状态机1->符号_额外属性));
        状态机1->符号_额外属性 = tab;
        memset(状态机1->符号_额外属性 + 状态机1->数量_符号_额外属性, 0,
               (n - 状态机1->数量_符号_额外属性) * sizeof(*状态机1->符号_额外属性));
        状态机1->数量_符号_额外属性 = n;
    }
    return &状态机1->符号_额外属性[index];
}

/* 在ELF文件单词表中，本地符号必须出现在全局符号和弱符号的下方。 由于ZHI在生成代码时无法对其进行排序，因此我们必须在之后进行。 还修改了所有重定位表，以考虑单词表排序 */
static void sort_syms(知心状态机 *状态机1, 段 *s)
{
    int *old_to_new_syms;
    ElfW(符号) *new_syms;
    int 数量_syms, i;
    ElfW(符号) *p, *q;
    ElfW_Rel *rel;
    段 *sr;
    int type, sym_index;

    数量_syms = s->数据_偏移 / sizeof(ElfW(符号));
    new_syms = 内存_申请(数量_syms * sizeof(ElfW(符号)));
    old_to_new_syms = 内存_申请(数量_syms * sizeof(int));

    /* first pass for local symbols */
    p = (ElfW(符号) *)s->data;
    q = new_syms;
    for(i = 0; i < 数量_syms; i++) {
        if (ELFW(ST_BIND)(p->st_info) == STB_LOCAL) {
            old_to_new_syms[i] = q - new_syms;
            *q++ = *p;
        }
        p++;
    }
    /* save the number of local symbols in section header */
    if( s->sh_size )    /* this 'if' makes IDA happy */
        s->sh_info = q - new_syms;

    /* then second pass for non local symbols */
    p = (ElfW(符号) *)s->data;
    for(i = 0; i < 数量_syms; i++) {
        if (ELFW(ST_BIND)(p->st_info) != STB_LOCAL) {
            old_to_new_syms[i] = q - new_syms;
            *q++ = *p;
        }
        p++;
    }

    /* we copy the new symbols to the old */
    memcpy(s->data, new_syms, 数量_syms * sizeof(ElfW(符号)));
    内存_释放(new_syms);

    /* now we modify all the relocations */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        sr = 状态机1->段数[i];
        if (sr->sh_type == SHT_RELX && sr->link == s) {
            for_each_elem(sr, 0, rel, ElfW_Rel) {
                sym_index = ELFW(R_SYM)(rel->r_info);
                type = ELFW(R_TYPE)(rel->r_info);
                sym_index = old_to_new_syms[sym_index];
                rel->r_info = ELFW(R_INFO)(sym_index, type);
            }
        }
    }

    内存_释放(old_to_new_syms);
}

/* 重定位 symbol table, resolve undefined symbols if do_resolve is
   true and output error if undefined symbol. */
静态_函数 void 重定位_符号(知心状态机 *状态机1, 段 *全局单词表副本, int do_resolve)
{
    ElfW(符号) *sym;
    int sym_bind, sh_num;
    const char *name;

    for_each_elem(全局单词表副本, 1, sym, ElfW(符号)) {
        sh_num = sym->st_shndx;
        if (sh_num == SHN_UNDEF) {
            name = (char *) 状态机1->全局单词表副本->link->data + sym->st_name;
            /* Use ld.so to resolve symbol for us (for zhi -run) */
            if (do_resolve) {
#if defined ZHI_是_本机 && !defined ZHI_TARGET_PE
#ifdef ZHI_TARGET_MACHO
                /* The symbols in the symtables have a prepended '_'
                   but dl符号() needs the undecorated name.  */
                void *addr = dl符号(RTLD_默认, name + 1);
#else
                void *addr = dl符号(RTLD_默认, name);
#endif
                if (addr) {
                    sym->st_value = (目标地址_类型) addr;
#ifdef DEBUG_RELOC
		    printf ("relocate_sym: %s -> 0x%lx\n", name, sym->st_value);
#endif
                    goto found;
                }
#endif
            /* if dynamic symbol exist, it will be used in 重定位_段 */
            } else if (状态机1->导出的动态单词表 && 查找_elf_符号(状态机1->导出的动态单词表, name))
                goto found;
            /* XXX: _fp_hw seems to be part of the ABI, so we ignore
               it */
            if (!strcmp(name, "_fp_hw"))
                goto found;
            /* only weak symbols are accepted to be undefined. Their
               value is zero */
            sym_bind = ELFW(ST_BIND)(sym->st_info);
            if (sym_bind == STB_WEAK)
                sym->st_value = 0;
            else
                zhi_错误_不中止("undefined symbol '%s'", name);
        } else if (sh_num < SHN_LORESERVE) {
            /* add section base */
            sym->st_value += 状态机1->段数[sym->st_shndx]->sh_addr;
        }
    found: ;
    }
}

/* 重定位 a given section (CPU dependent) by applying the relocations
   in the associated relocation section */
静态_函数 void 重定位_段(知心状态机 *状态机1, 段 *s)
{
    段 *sr = s->重定位;
    ElfW_Rel *rel;
    ElfW(符号) *sym;
    int type, sym_index;
    unsigned char *ptr;
    目标地址_类型 tgt, addr;

    qrel = (ElfW_Rel *)sr->data;

    for_each_elem(sr, 0, rel, ElfW_Rel) {
        ptr = s->data + rel->r_offset;
        sym_index = ELFW(R_SYM)(rel->r_info);
        sym = &((ElfW(符号) *)单词表_部分->data)[sym_index];
        type = ELFW(R_TYPE)(rel->r_info);
        tgt = sym->st_value;
#if SHT_RELX == SHT_RELA
        tgt += rel->r_addend;
#endif
        addr = s->sh_addr + rel->r_offset;
        重定位(状态机1, rel, type, ptr, addr, tgt);
    }
    /* if the relocation is allocated, we change its symbol table */
    if (sr->sh_flags & SHF_ALLOC) {
        sr->link = 状态机1->导出的动态单词表;
        if (状态机1->输出_类型 == ZHI_输出_DLL) {
            size_t r = (uint8_t*)qrel - sr->data;
            if (sizeof ((单词表_符号*)0)->n_value < 指针_大小
                && 0 == strcmp(s->name, ".stab"))
                r = 0; /* cannot apply 64bit relocation to 32bit value */
            sr->数据_偏移 = sr->sh_size = r;
        }
    }
}

#ifndef ELF_OBJ_ONLY
/* 重定位 relocation table in 'sr' */
static void relocate_rel(知心状态机 *状态机1, 段 *sr)
{
    段 *s;
    ElfW_Rel *rel;

    s = 状态机1->段数[sr->sh_info];
    for_each_elem(sr, 0, rel, ElfW_Rel)
        rel->r_offset += s->sh_addr;
}

/* count the number of dynamic relocations so that we can reserve
   their space */
static int prepare_dynamic_rel(知心状态机 *状态机1, 段 *sr)
{
    int count = 0;
#if defined(ZHI_TARGET_I386) || defined(ZHI_TARGET_X86_64)
    ElfW_Rel *rel;
    for_each_elem(sr, 0, rel, ElfW_Rel) {
        int sym_index = ELFW(R_SYM)(rel->r_info);
        int type = ELFW(R_TYPE)(rel->r_info);
        switch(type) {
#if defined(ZHI_TARGET_I386)
        case R_386_32:
            if (!get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index
                && ((ElfW(符号)*)单词表_部分->data + sym_index)->st_shndx == SHN_UNDEF) {
                /* don't fixup unresolved (weak) symbols */
                rel->r_info = ELFW(R_INFO)(sym_index, R_386_RELATIVE);
                break;
            }
#elif defined(ZHI_TARGET_X86_64)
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
#endif
            count++;
            break;
#if defined(ZHI_TARGET_I386)
        case R_386_PC32:
#elif defined(ZHI_TARGET_X86_64)
        case R_X86_64_PC32:
#endif
            if (get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index)
                count++;
            break;
        default:
            break;
        }
    }
    if (count) {
        /* allocate the section */
        sr->sh_flags |= SHF_ALLOC;
        sr->sh_size = count * sizeof(ElfW_Rel);
    }
#endif
    return count;
}
#endif

#if !defined(ELF_OBJ_ONLY) || defined(ZHI_TARGET_MACHO)
static void build_got(知心状态机 *状态机1)
{
    /* if no got, then create it */
    状态机1->got = 创建_节(状态机1, ".got", SHT_PROGBITS, SHF_ALLOC | SHF_WRITE);
    状态机1->got->sh_entsize = 4;
    设置_elf_符号(单词表_部分, 0, 4, ELFW(ST_INFO)(STB_GLOBAL, STT_OBJECT),
                0, 状态机1->got->sh_num, "_GLOBAL_OFFSET_TABLE_");
    /* keep space for _DYNAMIC pointer and two dummy got entries */
    段_ptr_添加(状态机1->got, 3 * 指针_大小);
}

/* Create a GOT and (for function call) a PLT entry corresponding to a symbol
   in 状态机1->全局单词表副本. When creating the dynamic symbol table entry for the GOT
   relocation, use 'size' and 'info' for the corresponding symbol metadata.
   Returns the offset of the GOT or (if any) PLT entry. */
static struct 额外_符号_属性 * put_got_entry(知心状态机 *状态机1, int dyn_reloc_type,
                                       int sym_index)
{
    int need_plt_entry;
    const char *name;
    ElfW(符号) *sym;
    struct 额外_符号_属性 *attr;
    unsigned got_offset;
    char plt_name[100];
    int len;

    need_plt_entry = (dyn_reloc_type == R_JMP_SLOT);
    attr = get_额外_符号_属性(状态机1, sym_index, 1);

    /* In case a function is both called and its address taken 2 GOT entries
       are created, one for taking the address (GOT) and the other for the PLT
       entry (PLTGOT).  */
    if (need_plt_entry ? attr->plt_offset : attr->got_offset)
        return attr;

    /* create the GOT entry */
    got_offset = 状态机1->got->数据_偏移;
    段_ptr_添加(状态机1->got, 指针_大小);

    /* Create the GOT relocation that will insert the address of the object or
       function of interest in the GOT entry. This is a static relocation for
       memory output (dl符号 will give us the address of symbols) and dynamic
       relocation otherwise (executable and DLLs). The relocation should be
       done lazily for GOT entry with *_JUMP_SLOT relocation type (the one
       associated to a PLT entry) but is currently done at 加载 time for an
       unknown reason. */

    sym = &((ElfW(符号) *) 单词表_部分->data)[sym_index];
    name = (char *) 单词表_部分->link->data + sym->st_name;

    if (状态机1->导出的动态单词表) {
	if (ELFW(ST_BIND)(sym->st_info) == STB_LOCAL) {
	    /* Hack alarm.  We don't want to emit dynamic symbols
	       and symbol based relocs for STB_LOCAL symbols, but rather
	       want to resolve them directly.  At this point the symbol
	       values aren't final yet, so we must defer this.  We will later
	       have to create a RELATIVE 重定位 anyway, so we misuse the
	       relocation slot to smuggle the symbol reference until
	       fill_local_got_entries.  Not that the sym_index is
	       relative to 单词表_部分, not 状态机1->导出的动态单词表!  Nevertheless
	       we use 状态机1->dyn_sym so that if this is the first call
	       that got->重定位 is correctly created.  Also note that
	       RELATIVE relocs are not normally created for the .got,
	       so the types serves as a marker for later (and is retained
	       also for the final output, which is okay because then the
	       got is just normal data).  */
	    使_elf_重定位(状态机1->导出的动态单词表, 状态机1->got, got_offset, R_RELATIVE,
			  sym_index);
	} else {
	    if (0 == attr->dyn_index)
                attr->dyn_index = 设置_elf_符号(状态机1->导出的动态单词表, sym->st_value,
                                              sym->st_size, sym->st_info, 0,
                                              sym->st_shndx, name);
	    使_elf_重定位(状态机1->导出的动态单词表, 状态机1->got, got_offset, dyn_reloc_type,
			  attr->dyn_index);
	}
    } else {
        使_elf_重定位(单词表_部分, 状态机1->got, got_offset, dyn_reloc_type,
                      sym_index);
    }

    if (need_plt_entry) {
        if (!状态机1->plt) {
    	    状态机1->plt = 创建_节(状态机1, ".plt", SHT_PROGBITS,
    			          SHF_ALLOC | SHF_EXECINSTR);
    	    状态机1->plt->sh_entsize = 4;
        }

        attr->plt_offset = 创建_plt_入口(状态机1, got_offset, attr);

        /* create a symbol 'sym@plt' for the PLT jump vector */
        len = strlen(name);
        if (len > sizeof plt_name - 5)
            len = sizeof plt_name - 5;
        memcpy(plt_name, name, len);
        strcpy(plt_name + len, "@plt");
        attr->plt_sym = 处理_elf_符号(状态机1->全局单词表副本, attr->plt_offset, sym->st_size,
            ELFW(ST_INFO)(STB_GLOBAL, STT_FUNC), 0, 状态机1->plt->sh_num, plt_name);

    } else {
        attr->got_offset = got_offset;
    }

    return attr;
}

/* build GOT and PLT entries */
静态_函数 void 创建_获取_入口(知心状态机 *状态机1)
{
    段 *s;
    ElfW_Rel *rel;
    ElfW(符号) *sym;
    int i, type, 获取plt_入口, reloc_type, sym_index;
    struct 额外_符号_属性 *attr;

    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (s->sh_type != SHT_RELX)
            continue;
        /* no need to handle got relocations */
        if (s->link != 单词表_部分)
            continue;
        for_each_elem(s, 0, rel, ElfW_Rel) {
            type = ELFW(R_TYPE)(rel->r_info);
            获取plt_入口 = 获取plt_入口_类型(type);
            if (获取plt_入口 == -1)
                错误_打印 ("Unknown relocation type for got: %d", type);
            sym_index = ELFW(R_SYM)(rel->r_info);
            sym = &((ElfW(符号) *)单词表_部分->data)[sym_index];

            if (获取plt_入口 == NO_GOTPLT_ENTRY) {
                continue;
            }

            /* Automatically create PLT/GOT [entry] if it is an undefined
	       reference (resolved at runtime), or the symbol is absolute,
	       probably created by 在已编译的程序中添加符号, and thus on 64-bit
	       targets might be too far from application code.  */
            if (获取plt_入口 == AUTO_GOTPLT_ENTRY) {
                if (sym->st_shndx == SHN_UNDEF) {
                    ElfW(符号) *esym;
		    int dynindex;
                    if (状态机1->输出_类型 == ZHI_输出_DLL && ! PCRELATIVE_DLLPLT)
                        continue;
		    /* Relocations for UNDEF symbols would normally need
		       to be transferred into the executable or shared object.
		       If that were done AUTO_GOTPLT_ENTRY wouldn't exist.
		       But ZHI doesn't do that (at least for exes), so we
		       need to resolve all such relocs locally.  And that
		       means PLT slots for functions in DLLs and COPY relocs for
		       data symbols.  COPY relocs were generated in
		       bind_exe_dynsyms (and the symbol adjusted to be defined),
		       and for functions we were generated a dynamic symbol
		       of function type.  */
		    if (状态机1->导出的动态单词表) {
			/* 导出的动态单词表 isn't set for -run :-/  */
			dynindex = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
			esym = (ElfW(符号) *)状态机1->导出的动态单词表->data + dynindex;
			if (dynindex
			    && (ELFW(ST_TYPE)(esym->st_info) == STT_FUNC
				|| (ELFW(ST_TYPE)(esym->st_info) == STT_NOTYPE
				    && ELFW(ST_TYPE)(sym->st_info) == STT_FUNC)))
			    goto jmp_slot;
		    }
                } else if (!(sym->st_shndx == SHN_ABS
#ifndef ZHI_TARGET_ARM
			&& 指针_大小 == 8
#endif
			))
                    continue;
            }

#ifdef ZHI_TARGET_X86_64
            if ((type == R_X86_64_PLT32 || type == R_X86_64_PC32) &&
		sym->st_shndx != SHN_UNDEF &&
                (ELFW(ST_VISIBILITY)(sym->st_other) != STV_DEFAULT ||
		 ELFW(ST_BIND)(sym->st_info) == STB_LOCAL ||
		 状态机1->输出_类型 == ZHI_输出_EXE)) {
                rel->r_info = ELFW(R_INFO)(sym_index, R_X86_64_PC32);
                continue;
            }
#endif
            reloc_type = 代码_重定位(type);
            if (reloc_type == -1)
                错误_打印 ("Unknown relocation type: %d", type);
            else if (reloc_type != 0) {
            jmp_slot:
                reloc_type = R_JMP_SLOT;
            } else
                reloc_type = R_GLOB_DAT;

            if (!状态机1->got)
                build_got(状态机1);

            if (获取plt_入口 == BUILD_GOT_ONLY)
                continue;

            attr = put_got_entry(状态机1, reloc_type, sym_index);

            if (reloc_type == R_JMP_SLOT)
                rel->r_info = ELFW(R_INFO)(attr->plt_sym, type);
        }
    }
}
#endif

静态_函数 int 设置_全局_符号(知心状态机 *状态机1, const char *name, 段 *sec, long offs)
{
    int shn = sec ? sec->sh_num : offs ? SHN_ABS : SHN_UNDEF;
    if (sec && offs == -1)
        offs = sec->数据_偏移;
    return 设置_elf_符号(单词表_部分, offs, 0,
        ELFW(ST_INFO)(name ? STB_GLOBAL : STB_LOCAL, STT_NOTYPE), 0, shn, name);
}

static void add_init_array_defines(知心状态机 *状态机1, const char *段_name)
{
    段 *s;
    long end_offset;
    char buf[1024];
    s = 查找_段(状态机1, 段_name);
    if (!s) {
        end_offset = 0;
        s = 初始化数据_部分;
    } else {
        end_offset = s->数据_偏移;
    }
    snprintf(buf, sizeof(buf), "__%s_start", 段_name + 1);
    设置_全局_符号(状态机1, buf, s, 0);
    snprintf(buf, sizeof(buf), "__%s_end", 段_name + 1);
    设置_全局_符号(状态机1, buf, s, end_offset);
}

#ifndef ZHI_TARGET_PE
static int zhi_增加_支持(知心状态机 *状态机1, const char *文件名)
{
    char buf[1024];
    snprintf(buf, sizeof(buf), "%s/%s", 状态机1->zhi_库_路径, 文件名);
    return 添加文件(状态机1, buf);
}
#endif

静态_函数 void 增加_数组 (知心状态机 *状态机1, const char *sec, int c)
{
    段 *s;
    s = 查找_段(状态机1, sec);
    s->sh_flags |= SHF_WRITE;
#ifndef ZHI_TARGET_PE
    s->sh_type = sec[1] == 'i' ? SHT_INIT_ARRAY : SHT_FINI_ARRAY;
#endif
    使_elf_重定位 (状态机1->全局单词表副本, s, s->数据_偏移, R_DATA_PTR, c);
    段_ptr_添加(s, 指针_大小);
}

#ifdef 配置_ZHI_边界检查
静态_函数 void zhi_新增_边界检查(知心状态机 *状态机1)
{
    if (0 == 状态机1->执行_边界_检查器)
        return;
    段_ptr_添加(全局边界_部分, sizeof(目标地址_类型));
}
#endif

#ifdef ZHI_配置_记录回滚
static void put_ptr(知心状态机 *状态机1, 段 *s, int offs)
{
    int c;
    c = 设置_全局_符号(状态机1, NULL, s, offs);
    s = 初始化数据_部分;
    使_elf_重定位 (状态机1->全局单词表副本, s, s->数据_偏移, R_DATA_PTR, c);
    段_ptr_添加(s, 指针_大小);
}

/* set symbol to STB_LOCAL and resolve. The point is to not export it as
   a dynamic symbol to allow so's to have one each with a different value. */
static void set_local_sym(知心状态机 *状态机1, const char *name, 段 *s, int offset)
{
    int c = 查找_elf_符号(状态机1->全局单词表副本, name);
    if (c) {
        ElfW(符号) *esym = (ElfW(符号)*)状态机1->全局单词表副本->data + c;
        esym->st_info = ELFW(ST_INFO)(STB_LOCAL, STT_NOTYPE);
        esym->st_value = offset;
        esym->st_shndx = s->sh_num;
    }
}

静态_函数 void zhi_add_btstub(知心状态机 *状态机1)
{
    段 *s;
    int n, o;
    动态字符串 cstr;

    s = 初始化数据_部分;
    o = s->数据_偏移;
    /* create (part of) a struct 运行时_上下文 (see run开关.c) */
    put_ptr(状态机1, 符号调试_部分, 0);
    put_ptr(状态机1, 符号调试_部分, -1);
    put_ptr(状态机1, 符号调试_部分->link, 0);
    段_ptr_添加(s, 3 * 指针_大小);
    /* prog_base */
#ifndef ZHI_TARGET_MACHO
    /* XXX this relocation is wrong, it uses sym-index 0 (local,undef) */
    使_elf_重定位(状态机1->全局单词表副本, s, s->数据_偏移, R_DATA_PTR, 0);
#endif
    段_ptr_添加(s, 指针_大小);
    n = 2 * 指针_大小;
#ifdef 配置_ZHI_边界检查
    if (状态机1->执行_边界_检查器) {
        put_ptr(状态机1, 全局边界_部分, 0);
        n -= 指针_大小;
    }
#endif
    段_ptr_添加(s, n);

    动态字符串_初始化(&cstr);
    动态字符串_打印(&cstr,
        " extern void __bt_init(),*__rt_info[],__bt_init_dll();"
        "__attribute__((constructor)) static void __bt_init_rt(){");
#ifdef ZHI_TARGET_PE
    if (状态机1->输出_类型 == ZHI_输出_DLL)
#ifdef 配置_ZHI_边界检查
        动态字符串_打印(&cstr, "__bt_init_dll(%d);", 状态机1->执行_边界_检查器);
#else
        动态字符串_打印(&cstr, "__bt_init_dll(0);");
#endif
#endif
    动态字符串_打印(&cstr, "__bt_init(__rt_info,%d, 0);}",
        状态机1->输出_类型 == ZHI_输出_DLL ? 0 : 状态机1->运行时_num_callers + 1);
    编译包含ZHI源代码的字符串(状态机1, cstr.指向字符串的指针);
    动态字符串_释放(&cstr);
    set_local_sym(状态机1, "___rt_info" + !状态机1->前导_下划线, s, o);
}
#endif

#ifndef ZHI_TARGET_PE
/* add zhi runtime libraries */
静态_函数 void zhi_添加_运行时(知心状态机 *状态机1)
{
    状态机1->文件类型 = 0;
#ifdef 配置_ZHI_边界检查
    zhi_新增_边界检查(状态机1);
#endif
    处理实用注释库(状态机1);
    /* add libc */
    if (!状态机1->不添加标准库) {
        if (状态机1->选项_线程)
            添加库错误(状态机1, "pthread");
        添加库错误(状态机1, "c");
#ifdef ZHI_LIBGCC
        if (!状态机1->执行_静态链接) {
            if (ZHI_LIBGCC[0] == '/')
                添加文件(状态机1, ZHI_LIBGCC);
            else
                添加dll文件(状态机1, ZHI_LIBGCC, 0);
        }
#endif
#ifdef 配置_ZHI_边界检查
        if (状态机1->执行_边界_检查器 && 状态机1->输出_类型 != ZHI_输出_DLL) {
            添加库错误(状态机1, "pthread");
            添加库错误(状态机1, "dl");
            zhi_增加_支持(状态机1, "bcheck.o");
        }
#endif
#ifdef ZHI_配置_记录回滚
        if (状态机1->执行_跟踪) {
            if (状态机1->输出_类型 == ZHI_输出_EXE)
                zhi_增加_支持(状态机1, "bt-exe.o");
            if (状态机1->输出_类型 != ZHI_输出_DLL)
                zhi_增加_支持(状态机1, "bt-log.o");
            if (状态机1->输出_类型 != ZHI_输出_内存中运行)
                zhi_add_btstub(状态机1);
        }
#endif
        zhi_增加_支持(状态机1, ZHI_HEXINKU1);
#ifndef ZHI_TARGET_MACHO
        /* add crt end if not memory output */
        if (状态机1->输出_类型 != ZHI_输出_内存中运行)
            zhi_添加_crt(状态机1, "crtn.o");
#endif
    }
}
#endif

/* add various standard linker symbols (must be done after the
   段数 are filled (for example after allocating common
   symbols)) */
static void zhi_add_linker_symbols(知心状态机 *状态机1)
{
    char buf[1024];
    int i;
    段 *s;

    设置_全局_符号(状态机1, "_etext", 生成代码_段, -1);
    设置_全局_符号(状态机1, "_edata", 初始化数据_部分, -1);
    设置_全局_符号(状态机1, "_end", 未初始化数据_部分, -1);
#ifdef ZHI_TARGET_RISCV64
    /* XXX should be .sdata+0x800, not .data+0x800 */
    设置_全局_符号(状态机1, "__global_pointer$", 初始化数据_部分, 0x800);
#endif
    /* horrible new standard ldscript defines */
    add_init_array_defines(状态机1, ".preinit_array");
    add_init_array_defines(状态机1, ".init_array");
    add_init_array_defines(状态机1, ".fini_array");
    /* add start and stop symbols for 段数 whose name can be
       expressed in C */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if ((s->sh_flags & SHF_ALLOC)
            && (s->sh_type == SHT_PROGBITS
                || s->sh_type == SHT_STRTAB)) {
            const char *p;
            /* check if section name can be expressed in C */
            p = s->name;
            for(;;) {
                int c = *p;
                if (!c)
                    break;
                if (!是id(c) && !是数字(c))
                    goto next_sec;
                p++;
            }
            snprintf(buf, sizeof(buf), "__start_%s", s->name);
            设置_全局_符号(状态机1, buf, s, 0);
            snprintf(buf, sizeof(buf), "__stop_%s", s->name);
            设置_全局_符号(状态机1, buf, s, -1);
        }
    next_sec: ;
    }
}

静态_函数 void 解决_常见_符号(知心状态机 *状态机1)
{
    ElfW(符号) *sym;

    /* Allocate common symbols in BSS.  */
    for_each_elem(单词表_部分, 1, sym, ElfW(符号)) {
        if (sym->st_shndx == SHN_COMMON) {
            /* symbol alignment is in st_value for SHN_COMMONs */
	    sym->st_value = 返回_节_对齐偏移量(未初始化数据_部分, sym->st_size,
					sym->st_value);
            sym->st_shndx = 未初始化数据_部分->sh_num;
        }
    }

    /* Now assign linker provided symbols their value.  */
    zhi_add_linker_symbols(状态机1);
}

static void zhi_output_binary(知心状态机 *状态机1, FILE *f,
                              const int *sec_order)
{
    段 *s;
    int i, offset, size;

    offset = 0;
    for(i=1;i<状态机1->数量_段数;i++) {
        s = 状态机1->段数[sec_order[i]];
        if (s->sh_type != SHT_NOBITS &&
            (s->sh_flags & SHF_ALLOC)) {
            while (offset < s->sh_偏移) {
                fputc(0, f);
                offset++;
            }
            size = s->sh_size;
            fwrite(s->data, 1, size, f);
            offset += size;
        }
    }
}

#ifndef ELF_OBJ_ONLY
静态_函数 void fill_got_entry(知心状态机 *状态机1, ElfW_Rel *rel)
{
    int sym_index = ELFW(R_SYM) (rel->r_info);
    ElfW(符号) *sym = &((ElfW(符号) *) 单词表_部分->data)[sym_index];
    struct 额外_符号_属性 *attr = get_额外_符号_属性(状态机1, sym_index, 0);
    unsigned offset = attr->got_offset;

    if (0 == offset)
        return;
    段_保留(状态机1->got, offset + 指针_大小);
#ifdef ZHI_TARGET_X86_64
    写64le(状态机1->got->data + offset, sym->st_value);
#else
    写32le(状态机1->got->data + offset, sym->st_value);
#endif
}

/* Perform relocation to GOT or PLT entries */
静态_函数 void fill_got(知心状态机 *状态机1)
{
    段 *s;
    ElfW_Rel *rel;
    int i;

    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (s->sh_type != SHT_RELX)
            continue;
        /* no need to handle got relocations */
        if (s->link != 单词表_部分)
            continue;
        for_each_elem(s, 0, rel, ElfW_Rel) {
            switch (ELFW(R_TYPE) (rel->r_info)) {
                case R_X86_64_GOT32:
                case R_X86_64_GOTPCREL:
		case R_X86_64_GOTPCRELX:
		case R_X86_64_REX_GOTPCRELX:
                case R_X86_64_PLT32:
                    fill_got_entry(状态机1, rel);
                    break;
            }
        }
    }
}

/* See put_got_entry for a description.  This is the second stage
   where GOT references to local defined symbols are rewritten.  */
static void fill_local_got_entries(知心状态机 *状态机1)
{
    ElfW_Rel *rel;
    if (!状态机1->got->重定位)
        return;
    for_each_elem(状态机1->got->重定位, 0, rel, ElfW_Rel) {
	if (ELFW(R_TYPE)(rel->r_info) == R_RELATIVE) {
	    int sym_index = ELFW(R_SYM) (rel->r_info);
	    ElfW(符号) *sym = &((ElfW(符号) *) 单词表_部分->data)[sym_index];
	    struct 额外_符号_属性 *attr = get_额外_符号_属性(状态机1, sym_index, 0);
	    unsigned offset = attr->got_offset;
	    if (offset != rel->r_offset - 状态机1->got->sh_addr)
	      zhi_错误_不中止("huh");
	    rel->r_info = ELFW(R_INFO)(0, R_RELATIVE);
#if SHT_RELX == SHT_RELA
	    rel->r_addend = sym->st_value;
#else
	    /* All our REL architectures also happen to be 32bit LE.  */
	    写32le(状态机1->got->data + offset, sym->st_value);
#endif
	}
    }
}

/* Bind symbols of executable: resolve undefined symbols from exported symbols
   in shared libraries and export non local defined symbols to shared libraries
   if -导出所有符号 switch was given on command line */
static void bind_exe_dynsyms(知心状态机 *状态机1)
{
    const char *name;
    int sym_index, index;
    ElfW(符号) *sym, *esym;
    int type;

    /* Resolve undefined symbols from dynamic symbols. When there is a match:
       - if STT_FUNC or STT_GNU_IFUNC symbol -> add it in PLT
       - if STT_OBJECT symbol -> add it in .bss section with suitable 重定位 */
    for_each_elem(单词表_部分, 1, sym, ElfW(符号)) {
        if (sym->st_shndx == SHN_UNDEF) {
            name = (char *) 单词表_部分->link->data + sym->st_name;
            sym_index = 查找_elf_符号(状态机1->动态单词表_部分, name);
            if (sym_index) {
                esym = &((ElfW(符号) *)状态机1->动态单词表_部分->data)[sym_index];
                type = ELFW(ST_TYPE)(esym->st_info);
                if ((type == STT_FUNC) || (type == STT_GNU_IFUNC)) {
                    /* Indirect functions shall have STT_FUNC type in executable
                     * 导出的动态单词表 section. Indeed, a dl符号 call following a lazy
                     * resolution would pick the symbol value from the
                     * executable 导出的动态单词表 entry which would contain the address
                     * of the function wanted by the caller of dl符号 instead of
                     * the address of the function that would return that
                     * address */
                    int dynindex
		      = 处理_elf_符号(状态机1->导出的动态单词表, 0, esym->st_size,
				    ELFW(ST_INFO)(STB_GLOBAL,STT_FUNC), 0, 0,
				    name);
		    int index = sym - (ElfW(符号) *) 单词表_部分->data;
		    get_额外_符号_属性(状态机1, index, 1)->dyn_index = dynindex;
                } else if (type == STT_OBJECT) {
                    unsigned long offset;
                    ElfW(符号) *导出的动态单词表;
                    offset = 未初始化数据_部分->数据_偏移;
                    /* XXX: which alignment ? */
                    offset = (offset + 16 - 1) & -16;
                    设置_elf_符号 (状态机1->全局单词表副本, offset, esym->st_size,
                                 esym->st_info, 0, 未初始化数据_部分->sh_num, name);
                    index = 处理_elf_符号(状态机1->导出的动态单词表, offset, esym->st_size,
                                        esym->st_info, 0, 未初始化数据_部分->sh_num,
                                        name);

                    /* Ensure R_COPY works for weak symbol aliases */
                    if (ELFW(ST_BIND)(esym->st_info) == STB_WEAK) {
                        for_each_elem(状态机1->动态单词表_部分, 1, 导出的动态单词表, ElfW(符号)) {
                            if ((导出的动态单词表->st_value == esym->st_value)
                                && (ELFW(ST_BIND)(导出的动态单词表->st_info) == STB_GLOBAL)) {
                                char *dynname = (char *) 状态机1->动态单词表_部分->link->data
                                                + 导出的动态单词表->st_name;
                                处理_elf_符号(状态机1->导出的动态单词表, offset, 导出的动态单词表->st_size,
                                            导出的动态单词表->st_info, 0,
                                            未初始化数据_部分->sh_num, dynname);
                                break;
                            }
                        }
                    }

                    使_elf_重定位(状态机1->导出的动态单词表, 未初始化数据_部分,
                                  offset, R_COPY, index);
                    offset += esym->st_size;
                    未初始化数据_部分->数据_偏移 = offset;
                }
            } else {
                /* STB_WEAK undefined symbols are accepted */
                /* XXX: _fp_hw seems to be part of the ABI, so we ignore it */
                if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK ||
                    !strcmp(name, "_fp_hw")) {
                } else {
                    zhi_错误_不中止("undefined symbol '%s'", name);
                }
            }
        } else if (状态机1->导出所有符号 && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            /* if -导出所有符号 option, then export all non local symbols */
            name = (char *) 单词表_部分->link->data + sym->st_name;
            设置_elf_符号(状态机1->导出的动态单词表, sym->st_value, sym->st_size, sym->st_info,
                        0, sym->st_shndx, name);
        }
    }
}

/* Bind symbols of libraries: export all non local symbols of executable that
   are referenced by shared libraries. The reason is that the dynamic loader
   search symbol first in executable and then in libraries. Therefore a
   reference to a symbol already defined by a library can still be resolved by
   a symbol in the executable. */
static void bind_libs_dynsyms(知心状态机 *状态机1)
{
    const char *name;
    int sym_index;
    ElfW(符号) *sym, *esym;

    for_each_elem(状态机1->动态单词表_部分, 1, esym, ElfW(符号)) {
        name = (char *) 状态机1->动态单词表_部分->link->data + esym->st_name;
        sym_index = 查找_elf_符号(单词表_部分, name);
        sym = &((ElfW(符号) *)单词表_部分->data)[sym_index];
        if (sym_index && sym->st_shndx != SHN_UNDEF
            && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            设置_elf_符号(状态机1->导出的动态单词表, sym->st_value, sym->st_size,
                sym->st_info, 0, sym->st_shndx, name);
        } else if (esym->st_shndx == SHN_UNDEF) {
            /* weak symbols can stay undefined */
            if (ELFW(ST_BIND)(esym->st_info) != STB_WEAK)
                zhi_警告("undefined dynamic symbol '%s'", name);
        }
    }
}

/* Export all non local symbols. This is used by shared libraries so that the
   non local symbols they define can resolve a reference in another shared
   library or in the executable. Correspondingly, it allows undefined local
   symbols to be resolved by other shared libraries or by the executable. */
static void export_global_syms(知心状态机 *状态机1)
{
    int dynindex, index;
    const char *name;
    ElfW(符号) *sym;

    for_each_elem(单词表_部分, 1, sym, ElfW(符号)) {
        if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
	    name = (char *) 单词表_部分->link->data + sym->st_name;
	    dynindex = 处理_elf_符号(状态机1->导出的动态单词表, sym->st_value, sym->st_size,
				   sym->st_info, 0, sym->st_shndx, name);
	    index = sym - (ElfW(符号) *) 单词表_部分->data;
            get_额外_符号_属性(状态机1, index, 1)->dyn_index = dynindex;
        }
    }
}
#endif

/* Allocate strings for section names and decide if an unallocated section
   should be output.
   NOTE: the strsec section comes last, so its size is also correct ! */
static int alloc_sec_names(知心状态机 *状态机1, int file_type, 段 *strsec)
{
    int i;
    段 *s;
    int textrel = 0;

    /* Allocate strings for section names */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        /* when generating a DLL, we include relocations but we may
           patch them */
#ifndef ELF_OBJ_ONLY
        if (file_type == ZHI_输出_DLL &&
            s->sh_type == SHT_RELX &&
            !(s->sh_flags & SHF_ALLOC) &&
            (状态机1->段数[s->sh_info]->sh_flags & SHF_ALLOC) &&
            prepare_dynamic_rel(状态机1, s)) {
            if (!(状态机1->段数[s->sh_info]->sh_flags & SHF_WRITE))
                textrel = 1;
        } else
#endif
        if ((状态机1->执行_调试 && s->sh_type != SHT_RELX) ||
            file_type == ZHI_输出_目标文件 ||
            (s->sh_flags & SHF_ALLOC) ||
	    i == (状态机1->数量_段数 - 1)) {
            /* we output all 段数 if debug or object file */
            s->sh_size = s->数据_偏移;
        }
	if (s->sh_size || (s->sh_flags & SHF_ALLOC))
            s->sh_name = 处理_elf_字符串(strsec, s->name);
    }
    strsec->sh_size = strsec->数据_偏移;
    return textrel;
}

/* Info to be copied in dynamic section */
struct dyn_inf {
    段 *dynamic;
    段 *dynstr;
    unsigned long 数据_偏移;
    目标地址_类型 rel_addr;
    目标地址_类型 rel_size;
#if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
    目标地址_类型 bss_addr;
    目标地址_类型 bss_size;
#endif
};

/* Assign 段数 to segments and decide how are 段数 laid out when loaded
   in memory. This function also fills corresponding program headers. */
static int layout_sections(知心状态机 *状态机1, ElfW(程序头表) *phdr, int phnum,
                           段 *interp, 段* strsec,
                           struct dyn_inf *dyninf, int *sec_order)
{
    int i, j, k, file_type, sh_order_index, file_offset;
    unsigned long s_align;
    long long tmp;
    目标地址_类型 addr;
    ElfW(程序头表) *ph;
    段 *s;

    file_type = 状态机1->输出_类型;
    sh_order_index = 1;
    file_offset = 0;
    if (状态机1->输出_格式 == ZHI_输出_格式_ELF)
        file_offset = sizeof(ElfW(ELF文件头)) + phnum * sizeof(ElfW(程序头表));
    s_align = ELF_PAGE_SIZE;
    if (状态机1->分段_对齐)
        s_align = 状态机1->分段_对齐;

    if (phnum > 0) {
        if (状态机1->已有_代码段_地址) {
            int a_offset, p_offset;
            addr = 状态机1->代码段_地址;
            /* we ensure that (addr % ELF_PAGE_SIZE) == file_offset %
               ELF_PAGE_SIZE */
            a_offset = (int) (addr & (s_align - 1));
            p_offset = file_offset & (s_align - 1);
            if (a_offset < p_offset)
                a_offset += s_align;
            file_offset += (a_offset - p_offset);
        } else {
            if (file_type == ZHI_输出_DLL)
                addr = 0;
            else
                addr = ELF_START_ADDR;
            /* compute address after headers */
            addr += (file_offset & (s_align - 1));
        }

        ph = &phdr[0];
        /* Leave one program headers for the program interpreter and one for
           the program header table itself if needed. These are done later as
           they require section layout to be done first. */
        if (interp)
            ph += 2;

        /* dynamic relocation table information, for .dynamic section */
        dyninf->rel_addr = dyninf->rel_size = 0;
#if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
        dyninf->bss_addr = dyninf->bss_size = 0;
#endif

        for(j = 0; j < 2; j++) {
            ph->p_type = PT_LOAD;
            if (j == 0)
                ph->p_flags = PF_R | PF_X;
            else
                ph->p_flags = PF_R | PF_W;
            ph->p_align = s_align;

            /* Decide the layout of 段数 loaded in memory. This must
               be done before program headers are filled since they contain
               info about the layout. We do the following ordering: interp,
               symbol tables, relocations, progbits, nobits */
            /* XXX: do faster and simpler sorting */
            for(k = 0; k < 5; k++) {
                for(i = 1; i < 状态机1->数量_段数; i++) {
                    s = 状态机1->段数[i];
                    /* compute if section should be included */
                    if (j == 0) {
                        if ((s->sh_flags & (SHF_ALLOC | SHF_WRITE)) !=
                            SHF_ALLOC)
                            continue;
                    } else {
                        if ((s->sh_flags & (SHF_ALLOC | SHF_WRITE)) !=
                            (SHF_ALLOC | SHF_WRITE))
                            continue;
                    }
                    if (s == interp) {
                        if (k != 0)
                            continue;
                    } else if ((s->sh_type == SHT_DYNSYM ||
                                s->sh_type == SHT_STRTAB ||
                                s->sh_type == SHT_HASH)
                               && !strstr(s->name, ".stab"))  {
                        if (k != 1)
                            continue;
                    } else if (s->sh_type == SHT_RELX) {
                        if (k != 2)
                            continue;
                    } else if (s->sh_type == SHT_NOBITS) {
                        if (k != 4)
                            continue;
                    } else {
                        if (k != 3)
                            continue;
                    }
                    sec_order[sh_order_index++] = i;

                    /* section matches: we align it and add its size */
                    tmp = addr;
                    addr = (addr + s->sh_addralign - 1) &
                        ~(s->sh_addralign - 1);
                    file_offset += (int) ( addr - tmp );
                    s->sh_偏移 = file_offset;
                    s->sh_addr = addr;

                    /* update program header infos */
                    if (ph->p_offset == 0) {
                        ph->p_offset = file_offset;
                        ph->p_vaddr = addr;
                        ph->p_paddr = ph->p_vaddr;
                    }
                    /* update dynamic relocation infos */
                    if (s->sh_type == SHT_RELX) {
#if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
                        if (!strcmp(strsec->data + s->sh_name, ".rel.got")) {
                            dyninf->rel_addr = addr;
                            dyninf->rel_size += s->sh_size; /* XXX only first rel. */
                        }
                        if (!strcmp(strsec->data + s->sh_name, ".rel.bss")) {
                            dyninf->bss_addr = addr;
                            dyninf->bss_size = s->sh_size; /* XXX only first rel. */
                        }
#else
                        if (dyninf->rel_size == 0)
                            dyninf->rel_addr = addr;
                        dyninf->rel_size += s->sh_size;
#endif
                    }
                    addr += s->sh_size;
                    if (s->sh_type != SHT_NOBITS)
                        file_offset += s->sh_size;
                }
            }
	    if (j == 0) {
		/* Make the first PT_LOAD segment include the program
		   headers itself (and the ELF header as well), it'll
		   come out with same memory use but will make various
		   tools like binutils strip work better.  */
		ph->p_offset &= ~(ph->p_align - 1);
		ph->p_vaddr &= ~(ph->p_align - 1);
		ph->p_paddr &= ~(ph->p_align - 1);
	    }
            ph->p_filesz = file_offset - ph->p_offset;
            ph->p_memsz = addr - ph->p_vaddr;
            ph++;
            if (j == 0) {
                if (状态机1->输出_格式 == ZHI_输出_格式_ELF) {
                    /* if in the middle of a page, we duplicate the page in
                       memory so that one copy is RX and the other is RW */
                    if ((addr & (s_align - 1)) != 0)
                        addr += s_align;
                } else {
                    addr = (addr + s_align - 1) & ~(s_align - 1);
                    file_offset = (file_offset + s_align - 1) & ~(s_align - 1);
                }
            }
        }
    }

    /* all other 段数 come after */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (phnum > 0 && (s->sh_flags & SHF_ALLOC))
            continue;
        sec_order[sh_order_index++] = i;

        file_offset = (file_offset + s->sh_addralign - 1) &
            ~(s->sh_addralign - 1);
        s->sh_偏移 = file_offset;
        if (s->sh_type != SHT_NOBITS)
            file_offset += s->sh_size;
    }

    return file_offset;
}

#ifndef ELF_OBJ_ONLY
/* put dynamic tag */
static void put_dt(段 *dynamic, int dt, 目标地址_类型 val)
{
    ElfW(Dyn) *dyn;
    dyn = 段_ptr_添加(dynamic, sizeof(ElfW(Dyn)));
    dyn->d_tag = dt;
    dyn->d_un.d_val = val;
}

static void fill_unloadable_phdr(ElfW(程序头表) *phdr, int phnum, 段 *interp,
                                 段 *dynamic)
{
    ElfW(程序头表) *ph;

    /* if interpreter, then add corresponding program header */
    if (interp) {
        ph = &phdr[0];

        ph->p_type = PT_PHDR;
        ph->p_offset = sizeof(ElfW(ELF文件头));
        ph->p_filesz = ph->p_memsz = phnum * sizeof(ElfW(程序头表));
        ph->p_vaddr = interp->sh_addr - ph->p_filesz;
        ph->p_paddr = ph->p_vaddr;
        ph->p_flags = PF_R | PF_X;
        ph->p_align = 4; /* interp->sh_addralign; */
        ph++;

        ph->p_type = PT_INTERP;
        ph->p_offset = interp->sh_偏移;
        ph->p_vaddr = interp->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = interp->sh_size;
        ph->p_memsz = interp->sh_size;
        ph->p_flags = PF_R;
        ph->p_align = interp->sh_addralign;
    }

    /* if dynamic section, then add corresponding program header */
    if (dynamic) {
        ph = &phdr[phnum - 1];

        ph->p_type = PT_DYNAMIC;
        ph->p_offset = dynamic->sh_偏移;
        ph->p_vaddr = dynamic->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = dynamic->sh_size;
        ph->p_memsz = dynamic->sh_size;
        ph->p_flags = PF_R | PF_W;
        ph->p_align = dynamic->sh_addralign;
    }
}

/* Fill the dynamic section with tags describing the address and size of
   段数 */
static void fill_dynamic(知心状态机 *状态机1, struct dyn_inf *dyninf)
{
    段 *dynamic = dyninf->dynamic;
    段 *s;

    /* put dynamic section entries */
    put_dt(dynamic, DT_HASH, 状态机1->导出的动态单词表->hash->sh_addr);
    put_dt(dynamic, DT_STRTAB, dyninf->dynstr->sh_addr);
    put_dt(dynamic, DT_SYMTAB, 状态机1->导出的动态单词表->sh_addr);
    put_dt(dynamic, DT_STRSZ, dyninf->dynstr->数据_偏移);
    put_dt(dynamic, DT_SYMENT, sizeof(ElfW(符号)));
#if 指针_大小 == 8
    put_dt(dynamic, DT_RELA, dyninf->rel_addr);
    put_dt(dynamic, DT_RELASZ, dyninf->rel_size);
    put_dt(dynamic, DT_RELAENT, sizeof(ElfW_Rel));
#else
#if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
    put_dt(dynamic, DT_PLTGOT, 状态机1->got->sh_addr);
    put_dt(dynamic, DT_PLTRELSZ, dyninf->rel_size);
    put_dt(dynamic, DT_JMPREL, dyninf->rel_addr);
    put_dt(dynamic, DT_PLTREL, DT_REL);
    put_dt(dynamic, DT_REL, dyninf->bss_addr);
    put_dt(dynamic, DT_RELSZ, dyninf->bss_size);
#else
    put_dt(dynamic, DT_REL, dyninf->rel_addr);
    put_dt(dynamic, DT_RELSZ, dyninf->rel_size);
    put_dt(dynamic, DT_RELENT, sizeof(ElfW_Rel));
#endif
#endif
    if (versym_section)
        put_dt(dynamic, DT_VERSYM, versym_section->sh_addr);
    if (verneed_section) {
        put_dt(dynamic, DT_VERNEED, verneed_section->sh_addr);
        put_dt(dynamic, DT_VERNEEDNUM, dt_必须的数码);
    }
    s = find_段_create (状态机1, ".preinit_array", 0);
    if (s && s->数据_偏移) {
        put_dt(dynamic, DT_PREINIT_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_PREINIT_ARRAYSZ, s->数据_偏移);
    }
    s = find_段_create (状态机1, ".init_array", 0);
    if (s && s->数据_偏移) {
        put_dt(dynamic, DT_INIT_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_INIT_ARRAYSZ, s->数据_偏移);
    }
    s = find_段_create (状态机1, ".fini_array", 0);
    if (s && s->数据_偏移) {
        put_dt(dynamic, DT_FINI_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_FINI_ARRAYSZ, s->数据_偏移);
    }
    s = find_段_create (状态机1, ".init", 0);
    if (s && s->数据_偏移) {
        put_dt(dynamic, DT_INIT, s->sh_addr);
    }
    s = find_段_create (状态机1, ".fini", 0);
    if (s && s->数据_偏移) {
        put_dt(dynamic, DT_FINI, s->sh_addr);
    }
    if (状态机1->执行_调试)
        put_dt(dynamic, DT_DEBUG, 0);
    put_dt(dynamic, DT_NULL, 0);
}

/* Relocate remaining 段数 and symbols (that is those not related to
   dynamic linking) */
static int final_sections_reloc(知心状态机 *状态机1)
{
    int i;
    段 *s;

    重定位_符号(状态机1, 状态机1->全局单词表副本, 0);

    if (状态机1->数量_错误 != 0)
        return -1;

    /* 重定位 段数 */
    /* XXX: ignore 段数 with allocated relocations ? */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (s->重定位 && (s != 状态机1->got || 状态机1->执行_静态链接))
            重定位_段(状态机1, s);
    }

    /* 重定位 relocation entries if the relocation tables are
       allocated in the executable */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if ((s->sh_flags & SHF_ALLOC) &&
            s->sh_type == SHT_RELX) {
            relocate_rel(状态机1, s);
        }
    }
    return 0;
}
#endif

/* Create an ELF file on disk.
   This function handle ELF specific layout requirements */
static void zhi_output_elf(知心状态机 *状态机1, FILE *f, int phnum, ElfW(程序头表) *phdr,
                           int file_offset, int *sec_order)
{
    int i, shnum, offset, size, file_type;
    段 *s;
    ElfW(ELF文件头) elf头;
    ElfW(节头表) 节头表, *节头;

    file_type = 状态机1->输出_类型;
    shnum = 状态机1->数量_段数;

    memset(&elf头, 0, sizeof(elf头));

    if (phnum > 0) {
        elf头.e_phentsize = sizeof(ElfW(程序头表));
        elf头.e_phnum = phnum;
        elf头.e_phoff = sizeof(ElfW(ELF文件头));
    }

    /* align to 4 */
    file_offset = (file_offset + 3) & -4;

    /* fill header */
    elf头.e_ident[0] = ELFMAG0;
    elf头.e_ident[1] = ELFMAG1;
    elf头.e_ident[2] = ELFMAG2;
    elf头.e_ident[3] = ELFMAG3;
    elf头.e_ident[4] = ELFCLASSW;
    elf头.e_ident[5] = ELF数据编码2的补码小尾数;
    elf头.e_ident[6] = EV_CURRENT;
#if !defined(ZHI_TARGET_PE) && (defined(__FreeBSD__) || defined(__FreeBSD_kernel__))
    /* FIXME: should set only for freebsd _target_, but we exclude only PE target */
    elf头.e_ident[EI_操作系统ABI识别] = EI_操作系统ABI识别_FREEBSD;
#endif
#ifdef ZHI_TARGET_ARM
#ifdef ZHI_ARM_EABI
    elf头.e_ident[EI_操作系统ABI识别] = 0;
    elf头.e_flags = EF_ARM_EABI_VER4;
    if (file_type == ZHI_输出_EXE || file_type == ZHI_输出_DLL)
        elf头.e_flags |= EF_ARM_HASENTRY;
    if (状态机1->浮动_abi == ARM_HARD_FLOAT)
        elf头.e_flags |= EF_ARM_VFP_FLOAT;
    else
        elf头.e_flags |= EF_ARM_SOFT_FLOAT;
#else
    elf头.e_ident[EI_操作系统ABI识别] = EI_操作系统ABI识别_ARM;
#endif
#elif defined ZHI_TARGET_RISCV64
    elf头.e_flags = EF_RISCV_FLOAT_ABI_DOUBLE;
#endif
    switch(file_type) {
    default:
    case ZHI_输出_EXE:
        elf头.e_type = ET_可执行文件;
        elf头.e_entry = 获取_符号_地址(状态机1, "_start", 1, 0);
        break;
    case ZHI_输出_DLL:
        elf头.e_type = ET_共享对象文件;
        elf头.e_entry = 生成代码_段->sh_addr; /* XXX: is it correct ? */
        break;
    case ZHI_输出_目标文件:
        elf头.e_type = ET_可重定位文件;
        break;
    }
    elf头.e_machine = EM_ZHI_TARGET;
    elf头.e_version = EV_CURRENT;
    elf头.e_shoff = file_offset;
    elf头.e_ehsize = sizeof(ElfW(ELF文件头));
    elf头.e_shentsize = sizeof(ElfW(节头表));
    elf头.e_shnum = shnum;
    elf头.e_shstrndx = shnum - 1;

    fwrite(&elf头, 1, sizeof(ElfW(ELF文件头)), f);
    fwrite(phdr, 1, phnum * sizeof(ElfW(程序头表)), f);
    offset = sizeof(ElfW(ELF文件头)) + phnum * sizeof(ElfW(程序头表));

    sort_syms(状态机1, 单词表_部分);
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[sec_order[i]];
        if (s->sh_type != SHT_NOBITS) {
            while (offset < s->sh_偏移) {
                fputc(0, f);
                offset++;
            }
            size = s->sh_size;
            if (size)
                fwrite(s->data, 1, size, f);
            offset += size;
        }
    }

    /* output section headers */
    while (offset < elf头.e_shoff) {
        fputc(0, f);
        offset++;
    }

    for(i = 0; i < 状态机1->数量_段数; i++) {
        节头 = &节头表;
        memset(节头, 0, sizeof(ElfW(节头表)));
        s = 状态机1->段数[i];
        if (s) {
            节头->sh_name = s->sh_name;
            节头->sh_type = s->sh_type;
            节头->sh_flags = s->sh_flags;
            节头->sh_entsize = s->sh_entsize;
            节头->sh_info = s->sh_info;
            if (s->link)
                节头->sh_link = s->link->sh_num;
            节头->sh_addralign = s->sh_addralign;
            节头->sh_addr = s->sh_addr;
            节头->sh_偏移 = s->sh_偏移;
            节头->sh_size = s->sh_size;
        }
        fwrite(节头, 1, sizeof(ElfW(节头表)), f);
    }
}

/* Write an elf, coff or "binary" file */
static int zhi_write_elf_file(知心状态机 *状态机1, const char *文件名, int phnum,
                              ElfW(程序头表) *phdr, int file_offset, int *sec_order)
{
    int fd, mode, file_type;
    FILE *f;

    file_type = 状态机1->输出_类型;
    if (file_type == ZHI_输出_目标文件)
        mode = 0666;
    else
        mode = 0777;
    unlink(文件名);
    fd = open(文件名, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, mode);
    if (fd < 0) {
        zhi_错误_不中止("could not write '%s'", 文件名);
        return -1;
    }
    f = fdopen(fd, "wb");
    if (状态机1->显示信息)
        printf("<- %s\n", 文件名);

#ifdef ZHI_TARGET_COFF
    if (状态机1->输出_格式 == ZHI_输出_格式_COFF)
        zhi_输出_coff(状态机1, f);
    else
#endif
    if (状态机1->输出_格式 == ZHI_输出_格式_ELF)
        zhi_output_elf(状态机1, f, phnum, phdr, file_offset, sec_order);
    else
        zhi_output_binary(状态机1, f, sec_order);
    fclose(f);

    return 0;
}

#ifndef ELF_OBJ_ONLY
/* Sort section headers by assigned sh_addr, remove 段数
   that we aren't going to output.  */
static void tidy_段_headers(知心状态机 *状态机1, int *sec_order)
{
    int i, nnew, l, *backmap;
    段 **snew, *s;
    ElfW(符号) *sym;

    snew = 内存_申请(状态机1->数量_段数 * sizeof(snew[0]));
    backmap = 内存_申请(状态机1->数量_段数 * sizeof(backmap[0]));
    for (i = 0, nnew = 0, l = 状态机1->数量_段数; i < 状态机1->数量_段数; i++) {
	s = 状态机1->段数[sec_order[i]];
	if (!i || s->sh_name) {
	    backmap[sec_order[i]] = nnew;
	    snew[nnew] = s;
	    ++nnew;
	} else {
	    backmap[sec_order[i]] = 0;
	    snew[--l] = s;
	}
    }
    for (i = 0; i < nnew; i++) {
	s = snew[i];
	if (s) {
	    s->sh_num = i;
            if (s->sh_type == SHT_RELX)
		s->sh_info = backmap[s->sh_info];
	}
    }

    for_each_elem(单词表_部分, 1, sym, ElfW(符号))
	if (sym->st_shndx != SHN_UNDEF && sym->st_shndx < SHN_LORESERVE)
	    sym->st_shndx = backmap[sym->st_shndx];
    if( !状态机1->执行_静态链接 ) {
        for_each_elem(状态机1->导出的动态单词表, 1, sym, ElfW(符号))
	    if (sym->st_shndx != SHN_UNDEF && sym->st_shndx < SHN_LORESERVE)
	        sym->st_shndx = backmap[sym->st_shndx];
    }
    for (i = 0; i < 状态机1->数量_段数; i++)
	sec_order[i] = i;
    内存_释放(状态机1->段数);
    状态机1->段数 = snew;
    状态机1->数量_段数 = nnew;
    内存_释放(backmap);
}
#endif

/* Output an elf, coff or binary file */
/* XXX: suppress unneeded 段数 */
static int elf_output_file(知心状态机 *状态机1, const char *文件名)
{
    int ret, phnum, shnum, file_type, file_offset, *sec_order;
    struct dyn_inf dyninf = {0};
    ElfW(程序头表) *phdr;
    段 *strsec, *interp, *dynamic, *dynstr;

    file_type = 状态机1->输出_类型;
    状态机1->数量_错误 = 0;
    ret = -1;
    phdr = NULL;
    sec_order = NULL;
    interp = dynamic = dynstr = NULL; /* avoid warning */

#ifndef ELF_OBJ_ONLY
    if (file_type != ZHI_输出_目标文件) {
        /* if linking, also link in runtime libraries (libc, libgcc, etc.) */
        zhi_添加_运行时(状态机1);
	解决_常见_符号(状态机1);

        if (!状态机1->执行_静态链接) {
            if (file_type == ZHI_输出_EXE) {
                char *ptr;
                /* allow override the dynamic loader */
                const char *elfint = getenv("LD_SO");
                if (elfint == NULL)
                    elfint = DEFAULT_ELFINTERP(状态机1);
                /* add interpreter section only if executable */
                interp = 创建_节(状态机1, ".interp", SHT_PROGBITS, SHF_ALLOC);
                interp->sh_addralign = 1;
                ptr = 段_ptr_添加(interp, 1 + strlen(elfint));
                strcpy(ptr, elfint);
            }

            /* add dynamic symbol table */
            状态机1->导出的动态单词表 = 新建_字符表(状态机1, ".导出的动态单词表", SHT_DYNSYM, SHF_ALLOC,
                                    ".dynstr",
                                    ".hash", SHF_ALLOC);
            dynstr = 状态机1->导出的动态单词表->link;
            /* add dynamic section */
            dynamic = 创建_节(状态机1, ".dynamic", SHT_DYNAMIC,
                                  SHF_ALLOC | SHF_WRITE);
            dynamic->link = dynstr;
            dynamic->sh_entsize = sizeof(ElfW(Dyn));

            build_got(状态机1);

            if (file_type == ZHI_输出_EXE) {
                bind_exe_dynsyms(状态机1);
                if (状态机1->数量_错误)
                    goto the_end;
                bind_libs_dynsyms(状态机1);
            } else {
                /* shared library case: simply export all global symbols */
                export_global_syms(状态机1);
            }
        }
        创建_获取_入口(状态机1);
	version_add (状态机1);
    }
#endif

    /* we add a section for symbols */
    strsec = 创建_节(状态机1, ".shstrtab", SHT_STRTAB, 0);
    处理_elf_字符串(strsec, "");

    /* Allocate strings for section names */
    ret = alloc_sec_names(状态机1, file_type, strsec);

#ifndef ELF_OBJ_ONLY
    if (dynamic) {
        int i;
        /* add a list of needed dlls */
        for(i = 0; i < 状态机1->数量_已加载的_dll数组; i++) {
            DLL参考 *dllref = 状态机1->已加载的_dll数组[i];
            if (dllref->level == 0)
                put_dt(dynamic, DT_NEEDED, 处理_elf_字符串(dynstr, dllref->name));
        }

        if (状态机1->动态库路径)
            put_dt(dynamic, 状态机1->启用新的dtags ? DT_RUNPATH : DT_RPATH,
                   处理_elf_字符串(dynstr, 状态机1->动态库路径));

        if (file_type == ZHI_输出_DLL) {
            if (状态机1->基本名称)
                put_dt(dynamic, DT_SONAME, 处理_elf_字符串(dynstr, 状态机1->基本名称));
            /* XXX: currently, since we do not handle PIC code, we
               must 重定位 the readonly segments */
            if (ret)
                put_dt(dynamic, DT_TEXTREL, 0);
        }

        if (状态机1->先解析当前模块符号)
            put_dt(dynamic, DT_SYMBOLIC, 0);

        dyninf.dynamic = dynamic;
        dyninf.dynstr = dynstr;
        /* remember offset and reserve space for 2nd call below */
        dyninf.数据_偏移 = dynamic->数据_偏移;
        fill_dynamic(状态机1, &dyninf);
        dynamic->sh_size = dynamic->数据_偏移;
        dynstr->sh_size = dynstr->数据_偏移;
    }
#endif

    /* compute number of program headers */
    if (file_type == ZHI_输出_目标文件)
        phnum = 0;
    else if (file_type == ZHI_输出_DLL)
        phnum = 3;
    else if (状态机1->执行_静态链接)
        phnum = 2;
    else
        phnum = 5;

    /* allocate program segment headers */
    phdr = 内存_初始化(phnum * sizeof(ElfW(程序头表)));

    /* compute number of 段数 */
    shnum = 状态机1->数量_段数;

    /* this array is used to reorder 段数 in the output file */
    sec_order = 内存_申请(sizeof(int) * shnum);
    sec_order[0] = 0;

    /* compute section to program header mapping */
    file_offset = layout_sections(状态机1, phdr, phnum, interp, strsec, &dyninf,
                                  sec_order);

#ifndef ELF_OBJ_ONLY
    /* Fill remaining program header and finalize relocation related to dynamic
       linking. */
    if (file_type != ZHI_输出_目标文件) {
        fill_unloadable_phdr(phdr, phnum, interp, dynamic);
        if (dynamic) {
            ElfW(符号) *sym;
            dynamic->数据_偏移 = dyninf.数据_偏移;
            fill_dynamic(状态机1, &dyninf);

            /* put in GOT the dynamic section address and 重定位 PLT */
            写32le(状态机1->got->data, dynamic->sh_addr);
            if (file_type == ZHI_输出_EXE
                || (RELOCATE_DLLPLT && file_type == ZHI_输出_DLL))
                重定位_plt(状态机1);

            /* 重定位 symbols in .导出的动态单词表 now that final addresses are known */
            for_each_elem(状态机1->导出的动态单词表, 1, sym, ElfW(符号)) {
                if (sym->st_shndx != SHN_UNDEF && sym->st_shndx < SHN_LORESERVE) {
                    /* do symbol relocation */
                    sym->st_value += 状态机1->段数[sym->st_shndx]->sh_addr;
                }
            }
        }

        /* if building executable or DLL, then 重定位 each section
           except the GOT which is already relocated */
        ret = final_sections_reloc(状态机1);
        if (ret)
            goto the_end;
	tidy_段_headers(状态机1, sec_order);

        /* Perform relocation to GOT or PLT entries */
        if (file_type == ZHI_输出_EXE && 状态机1->执行_静态链接)
            fill_got(状态机1);
        else if (状态机1->got)
            fill_local_got_entries(状态机1);
    }
#endif

    /* Create the ELF file with name '文件名' */
    ret = zhi_write_elf_file(状态机1, 文件名, phnum, phdr, file_offset, sec_order);
    状态机1->数量_段数 = shnum;
    goto the_end;
 the_end:
    内存_释放(sec_order);
    内存_释放(phdr);
    return ret;
}

HEXINKU接口 int 输出可执行文件或库文件或目标文件(知心状态机 *s, const char *文件名)
{
    int ret;
#ifdef ZHI_TARGET_PE
    if (s->输出_类型 != ZHI_输出_目标文件) {
        ret = pe_输出_文件(s, 文件名);
    } else
#elif ZHI_TARGET_MACHO
    if (s->输出_类型 != ZHI_输出_目标文件) {
        ret = macho_输出_文件(s, 文件名);
    } else
#endif
        ret = elf_output_file(s, 文件名);
    return ret;
}

静态_函数 ssize_t 全_读取(int fd, void *buf, size_t count) {
    char *cbuf = buf;
    size_t rnum = 0;
    while (1) {
        ssize_t num = read(fd, cbuf, count-rnum);
        if (num < 0) return num;
        if (num == 0) return rnum;
        rnum += num;
        cbuf += num;
    }
}

静态_函数 void *加载_数据(int fd, unsigned long file_offset, unsigned long size)
{
    void *data;

    data = 内存_申请(size);
    lseek(fd, file_offset, SEEK_SET);
    全_读取(fd, data, size);
    return data;
}

typedef struct 节合并信息 {
    段 *s;            /* corresponding existing section */
    unsigned long offset;  /* offset of the new section in the existing section */
    uint8_t 创建_节;       /* true if section 's' was added */
    uint8_t link_once;         /* true if link once section */
} 节合并信息;

静态_函数 int zhi_目标文件_类型(int fd, ElfW(ELF文件头) *h)
{
    int size = 全_读取(fd, h, sizeof *h);
    if (size == sizeof *h && 0 == memcmp(h, ELFMAG, 4)) /*把存储区 h 和存储区 ELFMAG 的前 4个字节进行比较。*/
    {
        if (h->e_type == ET_可重定位文件)
            return AFF_二进制_REL;
        if (h->e_type == ET_共享对象文件)
            return AFF_二进制_DYN;
    } else if (size >= 8) {
        if (0 == memcmp(h, ARMAG, 8))
            return AFF_二进制_AR;
#ifdef ZHI_TARGET_COFF
        if (((struct 文件头*)h)->f_magic == COFF_C67_MAGIC)
            return AFF_二进制_C67;
#endif
    }
    return 0;
}

/* 加载目标文件并将其与当前文件数合并 */
/* XXX: 正确处理单词表（调试）信息 */
静态_函数 int zhi_加载_对象_文件(知心状态机 *状态机1,
                                int fd, unsigned long file_offset)
{
    ElfW(ELF文件头) elf头;
    ElfW(节头表) *节头表, *节头;
    int size, i, j, offset, offseti, 数量_syms, sym_index, ret, 压缩;
    char *strsec, *字符表;
    int 节表_索引, 节表字符串_索引;
    int *old_to_new_syms;
    char *sh_name, *name;
    节合并信息 *节合并信息_表, *节合并;
    ElfW(符号) *sym, *全局单词表副本;
    ElfW_Rel *rel;
    段 *s;

    lseek(fd, file_offset, SEEK_SET);
    if (zhi_目标文件_类型(fd, &elf头) != AFF_二进制_REL)
        goto fail1;
    /* 测试特定于CPU的东西 */
    if (elf头.e_ident[5] != ELF数据编码2的补码小尾数 ||
        elf头.e_machine != EM_ZHI_TARGET) {
    fail1:
        zhi_错误_不中止("无效的目标文件");
        return -1;
    }
    /* read 段数 */
    节头表 = 加载_数据(fd, file_offset + elf头.e_shoff,sizeof(ElfW(节头表)) * elf头.e_shnum);
    节合并信息_表 = 内存_初始化(sizeof(节合并信息) * elf头.e_shnum);

    /* 加载 section names */
    节头 = &节头表[elf头.e_shstrndx];
    strsec = 加载_数据(fd, file_offset + 节头->sh_偏移, 节头->sh_size);

    /* 加载 全局单词表副本 and 字符表 */
    old_to_new_syms = NULL;
    全局单词表副本 = NULL;
    字符表 = NULL;
    数量_syms = 0;
    压缩 = 0;
    节表_索引 = 节表字符串_索引 = 0;

    for(i = 1; i < elf头.e_shnum; i++)
    {
        节头 = &节头表[i];
        if (节头->sh_type == SHT_SYMTAB)
        {
            if (全局单词表副本)
            {
                zhi_错误_不中止("对象只能包含一个 全局单词表副本");
            fail:
                ret = -1;
                goto the_end;
            }
            数量_syms = 节头->sh_size / sizeof(ElfW(符号));
            全局单词表副本 = 加载_数据(fd, file_offset + 节头->sh_偏移, 节头->sh_size);
            节合并信息_表[i].s = 单词表_部分;

            /* 现在加载字符表 */
            节头 = &节头表[节头->sh_link];
            字符表 = 加载_数据(fd, file_offset + 节头->sh_偏移, 节头->sh_size);
        }
	if (节头->sh_flags & SHF_COMPRESSED)
	    压缩 = 1;
    }
    /* 现在检查每个部分，并尝试将其内容与内存中的内容合并 */
    for(i = 1; i < elf头.e_shnum; i++) {
        /* 无需检查节名称字符表 */
        if (i == elf头.e_shstrndx)
            continue;
        节头 = &节头表[i];
	if (节头->sh_type == SHT_RELX)
	  节头 = &节头表[节头->sh_info];
        /* 忽略我们不处理的节数类型（再加上那些重定位） */
        if (节头->sh_type != SHT_PROGBITS &&
#ifdef ZHI_ARM_EABI
            节头->sh_type != SHT_ARM_EXIDX &&
#endif
            节头->sh_type != SHT_NOBITS &&
            节头->sh_type != SHT_PREINIT_ARRAY &&
            节头->sh_type != SHT_INIT_ARRAY &&
            节头->sh_type != SHT_FINI_ARRAY &&
            strcmp(strsec + 节头->sh_name, ".stabstr")
            )
            continue;
	if (压缩&& !strncmp(strsec + 节头->sh_name, ".debug_", sizeof(".debug_")-1))
	  continue;

	节头 = &节头表[i];
        sh_name = strsec + 节头->sh_name;
        if (节头->sh_addralign < 1)
            节头->sh_addralign = 1;
        /* 找到相应的部分（如果有） */
        for(j = 1; j < 状态机1->数量_段数;j++)
        {
            s = 状态机1->段数[j];
            if (!strcmp(s->name, sh_name))
            {
                if (!strncmp(sh_name, ".gnu.linkonce",sizeof(".gnu.linkonce") - 1))
                {
                    /* 如果“ linkonce”部分已经存在，我们将不再添加它。 这有点棘手，因为仍然可以在其中定义符号. */
                    节合并信息_表[i].link_once = 1;
                    goto next;
                }
                if (符号调试_部分) {
                    if (s == 符号调试_部分)
                        节表_索引 = i;
                    if (s == 符号调试_部分->link)
                        节表字符串_索引 = i;
                }
                goto found;
            }
        }
        /* 找不到：创建新节 */
        s = 创建_节(状态机1, sh_name, 节头->sh_type, 节头->sh_flags & ~SHF_GROUP);
        /* 从该部分获取尽可能多的信息。 sh_link和sh_info将在以后更新 */
        s->sh_addralign = 节头->sh_addralign;
        s->sh_entsize = 节头->sh_entsize;
        节合并信息_表[i].创建_节 = 1;
    found:
        if (节头->sh_type != s->sh_type) {
            zhi_错误_不中止("invalid section type");
            goto fail;
        }
        /* 对齐节的开始 */
        s->数据_偏移 += -s->数据_偏移 & (节头->sh_addralign - 1);
        if (节头->sh_addralign > s->sh_addralign)
            s->sh_addralign = 节头->sh_addralign;
        节合并信息_表[i].offset = s->数据_偏移;
        节合并信息_表[i].s = s;
        /* 级联 段数 */
        size = 节头->sh_size;
        if (节头->sh_type != SHT_NOBITS) {
            unsigned char *ptr;
            lseek(fd, file_offset + 节头->sh_偏移, SEEK_SET);
            ptr = 段_ptr_添加(s, size);
            全_读取(fd, ptr, size);
        } else {
            s->数据_偏移 += size;
        }
    next: ;
    }

    /* gr 重定位 stab strings */
    if (节表_索引 && 节表字符串_索引) {
        单词表_符号 *a, *b;
        unsigned o;
        s = 节合并信息_表[节表_索引].s;
        a = (单词表_符号 *)(s->data + 节合并信息_表[节表_索引].offset);
        b = (单词表_符号 *)(s->data + s->数据_偏移);
        o = 节合并信息_表[节表字符串_索引].offset;
        while (a < b) {
            if (a->n_strx)
                a->n_strx += o;
            a++;
        }
    }

    /* second short pass to update sh_link and sh_info fields of new
       段数 */
    for(i = 1; i < elf头.e_shnum; i++) {
        s = 节合并信息_表[i].s;
        if (!s || !节合并信息_表[i].创建_节)
            continue;
        节头 = &节头表[i];
        if (节头->sh_link > 0)
            s->link = 节合并信息_表[节头->sh_link].s;
        if (节头->sh_type == SHT_RELX) {
            s->sh_info = 节合并信息_表[节头->sh_info].s->sh_num;
            /* update backward link */
            状态机1->段数[s->sh_info]->重定位 = s;
        }
    }

    /* resolve symbols */
    old_to_new_syms = 内存_初始化(数量_syms * sizeof(int));

    sym = 全局单词表副本 + 1;
    for(i = 1; i < 数量_syms; i++, sym++) {
        if (sym->st_shndx != SHN_UNDEF &&
            sym->st_shndx < SHN_LORESERVE) {
            节合并 = &节合并信息_表[sym->st_shndx];
            if (节合并->link_once) {
                /* if a symbol is in a link once section, we use the
                   already defined symbol. It is very important to get
                   correct relocations */
                if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
                    name = 字符表 + sym->st_name;
                    sym_index = 查找_elf_符号(单词表_部分, name);
                    if (sym_index)
                        old_to_new_syms[i] = sym_index;
                }
                continue;
            }
            /* if no corresponding section added, no need to add symbol */
            if (!节合并->s)
                continue;
            /* convert section number */
            sym->st_shndx = 节合并->s->sh_num;
            /* offset value */
            sym->st_value += 节合并->offset;
        }
        /* add symbol */
        name = 字符表 + sym->st_name;
        sym_index = 设置_elf_符号(单词表_部分, sym->st_value, sym->st_size,
                                sym->st_info, sym->st_other,
                                sym->st_shndx, name);
        old_to_new_syms[i] = sym_index;
    }

    /* third pass to patch relocation entries */
    for(i = 1; i < elf头.e_shnum; i++) {
        s = 节合并信息_表[i].s;
        if (!s)
            continue;
        节头 = &节头表[i];
        offset = 节合并信息_表[i].offset;
        switch(s->sh_type) {
        case SHT_RELX:
            /* take relocation offset information */
            offseti = 节合并信息_表[节头->sh_info].offset;
            for_each_elem(s, (offset / sizeof(*rel)), rel, ElfW_Rel) {
                int type;
                unsigned sym_index;
                /* convert symbol index */
                type = ELFW(R_TYPE)(rel->r_info);
                sym_index = ELFW(R_SYM)(rel->r_info);
                /* NOTE: only one 全局单词表副本 assumed */
                if (sym_index >= 数量_syms)
                    goto invalid_reloc;
                sym_index = old_to_new_syms[sym_index];
                /* ignore link_once in rel section. */
                if (!sym_index && !节合并信息_表[节头->sh_info].link_once
#ifdef ZHI_TARGET_ARM
                    && type != R_ARM_V4BX
#elif defined ZHI_TARGET_RISCV64
                    && type != R_RISCV_ALIGN
                    && type != R_RISCV_RELAX
#endif
                   ) {
                invalid_reloc:
                    zhi_错误_不中止("Invalid relocation entry [%2d] '%s' @ %.8x",
                        i, strsec + 节头->sh_name, (int)rel->r_offset);
                    goto fail;
                }
                rel->r_info = ELFW(R_INFO)(sym_index, type);
                /* offset the relocation offset */
                rel->r_offset += offseti;
#ifdef ZHI_TARGET_ARM
                /* 从Thumb代码到PLT条目的跳转和分支需要特殊处理，因为PLT条目是ARM代码。
                 * 通过将这些指令转换为blx指令来处理引用PLT条目的无条件bl指令。
                 * 引用PLT条目的指令的其他情况需要在PLT条目之前添加Thumb桩，才能切换到ARM模式。
                 *  我们将符号属性的位plt_thumb_stub设置为指示这种情况。 */
                if (type == R_ARM_THM_JUMP24)
                    get_额外_符号_属性(状态机1, sym_index, 1)->plt_thumb_stub = 1;
#endif
            }
            break;
        default:
            break;
        }
    }

    ret = 0;
 the_end:
    内存_释放(全局单词表副本);
    内存_释放(字符表);
    内存_释放(old_to_new_syms);
    内存_释放(节合并信息_表);
    内存_释放(strsec);
    内存_释放(节头表);
    return ret;
}
/*ArchiveHeader:静态库标题（头）*/
typedef struct 静态库头 {
    char ar_name[16];           /* 目标文件名称 */
    char ar_date[12];           /* 原始目标文件的时间（始于Epoch时间点的秒数） */
    char ar_uid[6];             /* 目标文件ID */
    char ar_gid[6];             /* 组ID */
    char ar_mode[8];            /* 文件权限（如0644）   */
    char ar_size[10];           /* 文件的大小 */
    char ar_fmag[2];            /* 应该包含ARFMAG，ar_fmag中的值固定为“`\n”两个字符。 */
} 静态库头;

#define ARFMAG "`\n"

static unsigned long long get_be(const uint8_t *b, int n)
{
    unsigned long long ret = 0;
    while (n)
        ret = (ret << 8) | *b++, --n;
    return ret;
}

static int read_ar_header(int fd, int offset, 静态库头 *hdr)
{
    char *p, *e;
    int len;
    lseek(fd, offset, SEEK_SET);
    len = 全_读取(fd, hdr, sizeof(静态库头));
    if (len != sizeof(静态库头))
        return len ? -1 : 0;
    p = hdr->ar_name;
    for (e = p + sizeof hdr->ar_name; e > p && e[-1] == ' ';)
        --e;
    *e = '\0';
    hdr->ar_size[sizeof hdr->ar_size-1] = 0;
    return len;
}

/* 加载 only the objects which resolve undefined symbols */
static int zhi_load_alacarte(知心状态机 *状态机1, int fd, int size, int entrysize)
{
    int i, bound, nsyms, sym_index, len, ret = -1;
    unsigned long long off;
    uint8_t *data;
    const char *ar_names, *p;
    const uint8_t *ar_index;
    ElfW(符号) *sym;
    静态库头 hdr;

    data = 内存_申请(size);
    if (全_读取(fd, data, size) != size)
        goto the_end;
    nsyms = get_be(data, entrysize);
    ar_index = data + entrysize;
    ar_names = (char *) ar_index + nsyms * entrysize;

    do {
        bound = 0;
        for (p = ar_names, i = 0; i < nsyms; i++, p += strlen(p)+1) {
            段 *s = 单词表_部分;
            sym_index = 查找_elf_符号(s, p);
            if (!sym_index)
                continue;
            sym = &((ElfW(符号) *)s->data)[sym_index];
            if(sym->st_shndx != SHN_UNDEF)
                continue;
            off = get_be(ar_index + i * entrysize, entrysize);
            len = read_ar_header(fd, off, &hdr);
            if (len <= 0 || memcmp(hdr.ar_fmag, ARFMAG, 2)) {
                zhi_错误_不中止("invalid archive");
                goto the_end;
            }
            off += len;
            if (状态机1->显示信息 == 2)
                printf("   -> %s\n", hdr.ar_name);
            if (zhi_加载_对象_文件(状态机1, fd, off) < 0)
                goto the_end;
            ++bound;
        }
    } while(bound);
    ret = 0;
 the_end:
    内存_释放(data);
    return ret;
}

/* 加载 a '.a' file */
静态_函数 int zhi_加载_档案(知心状态机 *状态机1, int fd, int alacarte)
{
    静态库头 hdr;
    /* char magic[8]; */
    int size, len;
    unsigned long file_offset;
    ElfW(ELF文件头) elf头;

    /* 跳过 magic which was already checked */
    /* 全_读取(fd, magic, sizeof(magic)); */
    file_offset = sizeof ARMAG - 1;

    for(;;) {
        len = read_ar_header(fd, file_offset, &hdr);
        if (len == 0)
            return 0;
        if (len < 0) {
            zhi_错误_不中止("invalid archive");
            return -1;
        }
        file_offset += len;
        size = strtol(hdr.ar_size, NULL, 0);
        /* align to even */
        size = (size + 1) & ~1;
        if (alacarte) {
            /* coff单词表：我们处理 */
            if (!strcmp(hdr.ar_name, "/"))
                return zhi_load_alacarte(状态机1, fd, size, 4);
            if (!strcmp(hdr.ar_name, "/SYM64/"))
                return zhi_load_alacarte(状态机1, fd, size, 8);
        } else if (zhi_目标文件_类型(fd, &elf头) == AFF_二进制_REL) {
            if (状态机1->显示信息 == 2)
                printf("   -> %s\n", hdr.ar_name);
            if (zhi_加载_对象_文件(状态机1, fd, file_offset) < 0)
                return -1;
        }
        file_offset += size;
    }
}

#ifndef ELF_OBJ_ONLY
/* Set LV[I] to the global index of sym-version (LIB,VERSION).  Maybe resizes
   LV, maybe create a new entry for (LIB,VERSION).  */
static void set_ver_to_ver(知心状态机 *状态机1, int *n, int **lv, int i, char *lib, char *version)
{
    while (i >= *n) {
        *lv = 内存_重分配容量(*lv, (*n + 1) * sizeof(**lv));
        (*lv)[(*n)++] = -1;
    }
    if ((*lv)[i] == -1) {
        int v, prev_same_lib = -1;
        for (v = 0; v < 数量_符号_版本数; v++) {
            if (strcmp(符号_版本数[v].lib, lib))
              continue;
            prev_same_lib = v;
            if (!strcmp(符号_版本数[v].version, version))
              break;
        }
        if (v == 数量_符号_版本数) {
            符号_版本数 = 内存_重分配容量 (符号_版本数,
                                        (v + 1) * sizeof(*符号_版本数));
            符号_版本数[v].lib = 字符串_宽度加1(lib);
            符号_版本数[v].version = 字符串_宽度加1(version);
            符号_版本数[v].out_index = 0;
            符号_版本数[v].prev_same_lib = prev_same_lib;
            数量_符号_版本数++;
        }
        (*lv)[i] = v;
    }
}

/* Associates symbol SYM_INDEX (in dynsymtab) with sym-version index
   VERNDX.  */
static void
set_sym_version(知心状态机 *状态机1, int sym_index, int verndx)
{
    if (sym_index >= 数量_符号_到_版本) {
        int newelems = sym_index ? sym_index * 2 : 1;
        符号_到_版本 = 内存_重分配容量(符号_到_版本,
                                     newelems * sizeof(*符号_到_版本));
        memset(符号_到_版本 + 数量_符号_到_版本, -1,
               (newelems - 数量_符号_到_版本) * sizeof(*符号_到_版本));
        数量_符号_到_版本 = newelems;
    }
    if (符号_到_版本[sym_index] < 0)
      符号_到_版本[sym_index] = verndx;
}

struct versym_info {
    int 数量_versyms;
    ElfW(Verdef) *verdef;
    ElfW(Verneed) *verneed;
    ElfW(Half) *versym;
    int 数量_local_ver, *local_ver;
};


static void store_version(知心状态机 *状态机1, struct versym_info *v, char *dynstr)
{
    char *lib, *version;
    uint32_t next;
    int i;

#define	DEBUG_VERSION 0

    if (v->versym && v->verdef) {
      ElfW(Verdef) *vdef = v->verdef;
      lib = NULL;
      do {
        ElfW(Verdaux) *verdaux =
	  (ElfW(Verdaux) *) (((char *) vdef) + vdef->vd_aux);

#if DEBUG_VERSION
	printf ("verdef: version:%u flags:%u index:%u, hash:%u\n",
	        vdef->vd_version, vdef->vd_flags, vdef->vd_ndx,
		vdef->vd_hash);
#endif
	if (vdef->vd_cnt) {
          version = dynstr + verdaux->vda_name;

	  if (lib == NULL)
	    lib = version;
	  else
            set_ver_to_ver(状态机1, &v->数量_local_ver, &v->local_ver, vdef->vd_ndx,
                           lib, version);
#if DEBUG_VERSION
	  printf ("  verdaux(%u): %s\n", vdef->vd_ndx, version);
#endif
	}
        next = vdef->vd_next;
        vdef = (ElfW(Verdef) *) (((char *) vdef) + next);
      } while (next);
    }
    if (v->versym && v->verneed) {
      ElfW(Verneed) *vneed = v->verneed;
      do {
        ElfW(Vernaux) *vernaux =
	  (ElfW(Vernaux) *) (((char *) vneed) + vneed->vn_aux);

        lib = dynstr + vneed->vn_file;
#if DEBUG_VERSION
	printf ("verneed: %u %s\n", vneed->vn_version, lib);
#endif
	for (i = 0; i < vneed->vn_cnt; i++) {
	  if ((vernaux->vna_other & 0x8000) == 0) { /* hidden */
              version = dynstr + vernaux->vna_name;
              set_ver_to_ver(状态机1, &v->数量_local_ver, &v->local_ver, vernaux->vna_other,
                             lib, version);
#if DEBUG_VERSION
	    printf ("  vernaux(%u): %u %u %s\n",
		    vernaux->vna_other, vernaux->vna_hash,
		    vernaux->vna_flags, version);
#endif
	  }
	  vernaux = (ElfW(Vernaux) *) (((char *) vernaux) + vernaux->vna_next);
	}
        next = vneed->vn_next;
        vneed = (ElfW(Verneed) *) (((char *) vneed) + next);
      } while (next);
    }

#if DEBUG_VERSION
    for (i = 0; i < v->数量_local_ver; i++) {
      if (v->local_ver[i] > 0) {
        printf ("%d: lib: %s, version %s\n",
		i, 符号_版本数[v->local_ver[i]].lib,
                符号_版本数[v->local_ver[i]].version);
      }
    }
#endif
}

/* 加载一个DLL和所有引用的DLL。 'level = 0'表示该DLL被用户引用（因此应在生成的ELF文件中将其作为DT_NEEDED添加） */
静态_函数 int zhi_加载_dll(知心状态机 *状态机1, int fd, const char *文件名, int level)
{
    ElfW(ELF文件头) elf头;
    ElfW(节头表) *节头表, *节头, *sh1;
    int i, j, 数量_syms, 数量_dts, sym_bind, ret;
    ElfW(符号) *sym, *导出的动态单词表;
    ElfW(Dyn) *dt, *dynamic;

    char *dynstr;
    int sym_index;
    const char *name, *基本名称;
    DLL参考 *dllref;
    struct versym_info v;

    全_读取(fd, &elf头, sizeof(elf头));

    /* test CPU specific stuff */
    if (elf头.e_ident[5] != ELF数据编码2的补码小尾数 ||
        elf头.e_machine != EM_ZHI_TARGET) {
        zhi_错误_不中止("bad architecture");
        return -1;
    }

    /* read 段数 */
    节头表 = 加载_数据(fd, elf头.e_shoff, sizeof(ElfW(节头表)) * elf头.e_shnum);

    /* 加载 dynamic section and dynamic symbols */
    数量_syms = 0;
    数量_dts = 0;
    dynamic = NULL;
    导出的动态单词表 = NULL; /* avoid warning */
    dynstr = NULL; /* avoid warning */
    memset(&v, 0, sizeof v);

    for(i = 0, 节头 = 节头表; i < elf头.e_shnum; i++, 节头++) {
        switch(节头->sh_type) {
        case SHT_DYNAMIC:
            数量_dts = 节头->sh_size / sizeof(ElfW(Dyn));
            dynamic = 加载_数据(fd, 节头->sh_偏移, 节头->sh_size);
            break;
        case SHT_DYNSYM:
            数量_syms = 节头->sh_size / sizeof(ElfW(符号));
            导出的动态单词表 = 加载_数据(fd, 节头->sh_偏移, 节头->sh_size);
            sh1 = &节头表[节头->sh_link];
            dynstr = 加载_数据(fd, sh1->sh_偏移, sh1->sh_size);
            break;
        case SHT_GNU_verdef:
	    v.verdef = 加载_数据(fd, 节头->sh_偏移, 节头->sh_size);
	    break;
        case SHT_GNU_verneed:
	    v.verneed = 加载_数据(fd, 节头->sh_偏移, 节头->sh_size);
	    break;
        case SHT_GNU_versym:
            v.数量_versyms = 节头->sh_size / sizeof(ElfW(Half));
	    v.versym = 加载_数据(fd, 节头->sh_偏移, 节头->sh_size);
	    break;
        default:
            break;
        }
    }

    /* compute the real library name */
    基本名称 = 取_文件基本名(文件名);

    for(i = 0, dt = dynamic; i < 数量_dts; i++, dt++) {
        if (dt->d_tag == DT_SONAME) {
            基本名称 = dynstr + dt->d_un.d_val;
        }
    }

    /* if the dll is already loaded, do not 加载 it */
    for(i = 0; i < 状态机1->数量_已加载的_dll数组; i++) {
        dllref = 状态机1->已加载的_dll数组[i];
        if (!strcmp(基本名称, dllref->name)) {
            /* but update level if needed */
            if (level < dllref->level)
                dllref->level = level;
            ret = 0;
            goto the_end;
        }
    }

    if (v.数量_versyms != 数量_syms)
        内存_释放 (v.versym), v.versym = NULL;
    else
        store_version(状态机1, &v, dynstr);

    /* add the dll and its level */
    dllref = 内存_初始化(sizeof(DLL参考) + strlen(基本名称));
    dllref->level = level;
    strcpy(dllref->name, 基本名称);
    动态数组_追加元素(&状态机1->已加载的_dll数组, &状态机1->数量_已加载的_dll数组, dllref);

    /* add dynamic symbols in dynsym_section */
    for(i = 1, sym = 导出的动态单词表 + 1; i < 数量_syms; i++, sym++) {
        sym_bind = ELFW(ST_BIND)(sym->st_info);
        if (sym_bind == STB_LOCAL)
            continue;
        name = dynstr + sym->st_name;
        sym_index = 设置_elf_符号(状态机1->动态单词表_部分, sym->st_value, sym->st_size,
                                sym->st_info, sym->st_other, sym->st_shndx, name);
        if (v.versym) {
            ElfW(Half) vsym = v.versym[i];
            if ((vsym & 0x8000) == 0 && vsym > 0 && vsym < v.数量_local_ver)
                set_sym_version(状态机1, sym_index, v.local_ver[vsym]);
        }
    }

    /* 加载 all referenced DLLs */
    for(i = 0, dt = dynamic; i < 数量_dts; i++, dt++) {
        switch(dt->d_tag) {
        case DT_NEEDED:
            name = dynstr + dt->d_un.d_val;
            for(j = 0; j < 状态机1->数量_已加载的_dll数组; j++) {
                dllref = 状态机1->已加载的_dll数组[j];
                if (!strcmp(name, dllref->name))
                    goto already_loaded;
            }
            if (添加dll文件(状态机1, name, AFF_加载_引用的DLL) < 0) {
                zhi_错误_不中止("找不到引用的dll '%s' ", name);
                ret = -1;
                goto the_end;
            }
        already_loaded:
            break;
        }
    }
    ret = 0;
 the_end:
    内存_释放(dynstr);
    内存_释放(导出的动态单词表);
    内存_释放(dynamic);
    内存_释放(节头表);
    内存_释放(v.local_ver);
    内存_释放(v.verdef);
    内存_释放(v.verneed);
    内存_释放(v.versym);
    return ret;
}

#define LD_符_NAME 256
#define LD_符_文件结尾  (-1)

static int ld_inp(知心状态机 *状态机1)
{
    char b;
    if (状态机1->cc != -1) {
        int c = 状态机1->cc;
        状态机1->cc = -1;
        return c;
    }
    if (1 == read(状态机1->fd, &b, 1))
        return b;
    return CH_文件结尾;
}

/* return next ld script 标识符 */
static int 下一个_链接脚本_标识符(知心状态机 *状态机1, char *name, int name_size)
{
    int c, d, ch;
    char *q;

 redo:
    ch = ld_inp(状态机1);
    switch(ch) {
    case ' ':
    case '\t':
    case '\f':
    case '\v':
    case '\r':
    case '\n':
        goto redo;
    case '/':
        ch = ld_inp(状态机1);
        if (ch == '*') { /* comment */
            for (d = 0;; d = ch) {
                ch = ld_inp(状态机1);
                if (ch == CH_文件结尾 || (ch == '/' && d == '*'))
                    break;
            }
            goto redo;
        } else {
            q = name;
            *q++ = '/';
            goto parse_name;
        }
        break;
    case '\\':
    /* case 'a' ... 'z': */
    case 'a':
       case 'b':
       case 'c':
       case 'd':
       case 'e':
       case 'f':
       case 'g':
       case 'h':
       case 'i':
       case 'j':
       case 'k':
       case 'l':
       case 'm':
       case 'n':
       case 'o':
       case 'p':
       case 'q':
       case 'r':
       case 's':
       case 't':
       case 'u':
       case 'v':
       case 'w':
       case 'x':
       case 'y':
       case 'z':
    /* case 'A' ... 'z': */
    case 'A':
       case 'B':
       case 'C':
       case 'D':
       case 'E':
       case 'F':
       case 'G':
       case 'H':
       case 'I':
       case 'J':
       case 'K':
       case 'L':
       case 'M':
       case 'N':
       case 'O':
       case 'P':
       case 'Q':
       case 'R':
       case 'S':
       case 'T':
       case 'U':
       case 'V':
       case 'W':
       case 'X':
       case 'Y':
       case 'Z':
    case '_':
    case '.':
    case '$':
    case '~':
        q = name;
    parse_name:
        for(;;) {
            if (!((ch >= 'a' && ch <= 'z') ||
                  (ch >= 'A' && ch <= 'Z') ||
                  (ch >= '0' && ch <= '9') ||
                  strchr("/.-_+=$:\\,~", ch)))
                break;
            if ((q - name) < name_size - 1) {
                *q++ = ch;
            }
            ch = ld_inp(状态机1);
        }
        状态机1->cc = ch;
        *q = '\0';
        c = LD_符_NAME;
        break;
    case CH_文件结尾:
        c = LD_符_文件结尾;
        break;
    default:
        c = ch;
        break;
    }
    return c;
}

static int 链接_添加_文件(知心状态机 *状态机1, const char 文件名[])
{
    if (文件名[0] == '/') {
        if (配置_系统根目录[0] == '\0'
            && 添加内部文件(状态机1, 文件名, 文件格式_类型_BIN) == 0)
            return 0;
        文件名 = 取_文件基本名(文件名);
    }
    return 添加dll文件(状态机1, 文件名, 0);
}

static int 链接_添加_文件_列表(知心状态机 *状态机1, const char *cmd, int as_needed)
{
    char 文件名[1024], libname[1024];
    int t, group, nblibs = 0, ret = 0;
    char **libs = NULL;

    group = !strcmp(cmd, "GROUP");
    if (!as_needed)
        状态机1->新的_未定义的_符号 = 0;
    t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
    if (t != '(') {
        zhi_错误_不中止("( expected");
        ret = -1;
        goto lib_parse_error;
    }
    t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
    for(;;) {
        libname[0] = '\0';
        if (t == LD_符_文件结尾) {
            zhi_错误_不中止("unexpected end of file");
            ret = -1;
            goto lib_parse_error;
        } else if (t == ')') {
            break;
        } else if (t == '-') {
            t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
            if ((t != LD_符_NAME) || (文件名[0] != 'l')) {
                zhi_错误_不中止("library name expected");
                ret = -1;
                goto lib_parse_error;
            }
            p字符串复制(libname, sizeof libname, &文件名[1]);
            if (状态机1->执行_静态链接) {
                snprintf(文件名, sizeof 文件名, "lib%s.a", libname);
            } else {
                snprintf(文件名, sizeof 文件名, "lib%s.so", libname);
            }
        } else if (t != LD_符_NAME) {
            zhi_错误_不中止("文件名 expected");
            ret = -1;
            goto lib_parse_error;
        }
        if (!strcmp(文件名, "AS_NEEDED")) {
            ret = 链接_添加_文件_列表(状态机1, cmd, 1);
            if (ret)
                goto lib_parse_error;
        } else {
            /* TODO: Implement AS_NEEDED support. Ignore it for now */
            if (!as_needed) {
                ret = 链接_添加_文件(状态机1, 文件名);
                if (ret)
                    goto lib_parse_error;
                if (group) {
                    /* Add the 文件名 *and* the libname to avoid future conversions */
                    动态数组_追加元素(&libs, &nblibs, 字符串_宽度加1(文件名));
                    if (libname[0] != '\0')
                        动态数组_追加元素(&libs, &nblibs, 字符串_宽度加1(libname));
                }
            }
        }
        t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
        if (t == ',') {
            t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
        }
    }
    if (group && !as_needed) {
        while (状态机1->新的_未定义的_符号) {
            int i;
            状态机1->新的_未定义的_符号 = 0;
            for (i = 0; i < nblibs; i ++)
                链接_添加_文件(状态机1, libs[i]);
        }
    }
lib_parse_error:
    动态数组_重分配容量(&libs, &nblibs);
    return ret;
}

/* 解释GNU ldscript的子集以处理虚拟libc.so文件数 */
静态_函数 int zhi_加载_链接脚本(知心状态机 *状态机1, int fd)
{
    char cmd[64];
    char 文件名[1024];
    int t, ret;

    状态机1->fd = fd;
    状态机1->cc = -1;
    for(;;) {
        t = 下一个_链接脚本_标识符(状态机1, cmd, sizeof(cmd));
        if (t == LD_符_文件结尾)
            return 0;
        else if (t != LD_符_NAME)
            return -1;
        if (!strcmp(cmd, "INPUT") ||
            !strcmp(cmd, "GROUP")) {
            ret = 链接_添加_文件_列表(状态机1, cmd, 0);
            if (ret)
                return ret;
        } else if (!strcmp(cmd, "OUTPUT_FORMAT") ||
                   !strcmp(cmd, "TARGET")) {
            /* 忽略一些命令 */
            t = 下一个_链接脚本_标识符(状态机1, cmd, sizeof(cmd));
            if (t != '(') {
                zhi_错误_不中止("( 异常");
                return -1;
            }
            for(;;) {
                t = 下一个_链接脚本_标识符(状态机1, 文件名, sizeof(文件名));
                if (t == LD_符_文件结尾) {
                    zhi_错误_不中止("文件异常结束");
                    return -1;
                } else if (t == ')') {
                    break;
                }
            }
        } else {
            return -1;
        }
    }
    return 0;
}
#endif /* !ELF_OBJ_ONLY */
