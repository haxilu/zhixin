/*
 *  i386 specific functions for ZHI assembler
 */

#define 全局_使用
#include "zhi.h"

#define MAX_OPERANDS 3

#define 符_汇编_first 符_汇编_clc
#define 符_汇编_last 符_汇编_emms
#define 符_汇编_alllast 符_汇编_subps

#define OPC_B          0x01  /* only used with OPC_WL */
#define OPC_WL         0x02  /* accepts w, l or no suffix */
#define OPC_BWL        (OPC_B | OPC_WL) /* accepts b, w, l or no suffix */
#define OPC_REG        0x04 /* register is added to opcode */
#define OPC_MODRM      0x08 /* modrm encoding */

#define OPCT_MASK      0x70
#define OPC_FWAIT      0x10 /* add fwait opcode */
#define OPC_SHIFT      0x20 /* shift opcodes */
#define OPC_ARITH      0x30 /* arithmetic opcodes */
#define OPC_FARITH     0x40 /* FPU arithmetic opcodes */
#define OPC_TEST       0x50 /* test opcodes */
#define OPCT_IS(v,i) (((v) & OPCT_MASK) == (i))

#define OPC_0F        0x100 /* Is secondary map (0x0f prefix) */
#define OPC_48        0x200 /* Always has REX prefix */
#ifdef ZHI_TARGET_X86_64
# define OPC_WLQ     0x1000  /* accepts w, l, q or no suffix */
# define OPC_BWLQ    (OPC_B | OPC_WLQ) /* accepts b, w, l, q or no suffix */
# define OPC_WLX     OPC_WLQ
# define OPC_BWLX    OPC_BWLQ
#else
# define OPC_WLX     OPC_WL
# define OPC_BWLX    OPC_BWL
#endif

#define OPC_GROUP_SHIFT 13

/* in order to compress the operand type, we use specific operands and
   we or only with EA  */
enum {
    指令_REG8=0, /* warning: value is hardcoded from 符_汇编_xxx */
    指令_REG16,  /* warning: value is hardcoded from 符_汇编_xxx */
    指令_REG32,  /* warning: value is hardcoded from 符_汇编_xxx */
#ifdef ZHI_TARGET_X86_64
    指令_REG64,  /* warning: value is hardcoded from 符_汇编_xxx */
#endif
    指令_MMX,    /* warning: value is hardcoded from 符_汇编_xxx */
    指令_SSE,    /* warning: value is hardcoded from 符_汇编_xxx */
    指令_CR,     /* warning: value is hardcoded from 符_汇编_xxx */
    指令_TR,     /* warning: value is hardcoded from 符_汇编_xxx */
    指令_DB,     /* warning: value is hardcoded from 符_汇编_xxx */
    指令_SEG,
    指令_ST,
#ifdef ZHI_TARGET_X86_64
    指令_REG8_LOW, /* %spl,%bpl,%sil,%dil, encoded like ah,ch,dh,bh, but
		     with REX prefix, not used in insn templates */
#endif
    指令_IM8,
    指令_IM8S,
    指令_IM16,
    指令_IM32,
#ifdef ZHI_TARGET_X86_64
    指令_IM64,
#endif
    指令_EAX,    /* %al, %ax, %eax or %rax register */
    指令_ST0,    /* %st(0) register */
    指令_CL,     /* %cl register */
    指令_DX,     /* %dx register */
    指令_ADDR,   /* OP_EA with only offset */
    指令_INDIR,  /* *(expr) */
    /* composite types */
    指令_COMPOSITE_FIRST,
    指令_IM,     /* IM8 | IM16 | IM32 */
    指令_REG,    /* REG8 | REG16 | REG32 | REG64 */
    指令_REGW,   /* REG16 | REG32 | REG64 */
    指令_IMW,    /* IM16 | IM32 */
    指令_MMXSSE, /* MMX | SSE */
    指令_DISP,   /* Like 指令_ADDR, but emitted as displacement (for jumps) */
    指令_DISP8,  /* Like 指令_ADDR, but only 8bit (short jumps) */
    /* can be ored with any 指令_xxx */
    指令_EA = 0x80
};

#define OP_REG8   (1 << 指令_REG8)
#define OP_REG16  (1 << 指令_REG16)
#define OP_REG32  (1 << 指令_REG32)
#define OP_MMX    (1 << 指令_MMX)
#define OP_SSE    (1 << 指令_SSE)
#define OP_CR     (1 << 指令_CR)
#define OP_TR     (1 << 指令_TR)
#define OP_DB     (1 << 指令_DB)
#define OP_SEG    (1 << 指令_SEG)
#define OP_ST     (1 << 指令_ST)
#define OP_IM8    (1 << 指令_IM8)
#define OP_IM8S   (1 << 指令_IM8S)
#define OP_IM16   (1 << 指令_IM16)
#define OP_IM32   (1 << 指令_IM32)
#define OP_EAX    (1 << 指令_EAX)
#define OP_ST0    (1 << 指令_ST0)
#define OP_CL     (1 << 指令_CL)
#define OP_DX     (1 << 指令_DX)
#define OP_ADDR   (1 << 指令_ADDR)
#define OP_INDIR  (1 << 指令_INDIR)
#ifdef ZHI_TARGET_X86_64
# define OP_REG64 (1 << 指令_REG64)
# define OP_REG8_LOW (1 << 指令_REG8_LOW)
# define OP_IM64  (1 << 指令_IM64)
# define OP_EA32  (OP_EA << 1)
#else
# define OP_REG64 0
# define OP_REG8_LOW 0
# define OP_IM64  0
# define OP_EA32  0
#endif

#define OP_EA     0x40000000
#define OP_REG    (OP_REG8 | OP_REG16 | OP_REG32 | OP_REG64)

#ifdef ZHI_TARGET_X86_64
# define TREG_XAX   TREG_RAX
# define TREG_XCX   TREG_RCX
# define TREG_XDX   TREG_RDX
#else
# define TREG_XAX   TREG_EAX
# define TREG_XCX   TREG_ECX
# define TREG_XDX   TREG_EDX
#endif

typedef struct ASMInstr {
    uint16_t sym;
    uint16_t opcode;
    uint16_t instr_type;
    uint8_t 数量_ops;
    uint8_t op_type[MAX_OPERANDS]; /* see OP_xxx */
} ASMInstr;

typedef struct Operand {
    uint32_t type;
    int8_t  reg; /* register, -1 if none */
    int8_t  reg2; /* second register, -1 if none */
    uint8_t shift;
    表达式值 e;
} Operand;

static const uint8_t reg_to_size[9] = {
/*
    [OP_REG8] = 0,
    [OP_REG16] = 1,
    [OP_REG32] = 2,
#ifdef ZHI_TARGET_X86_64
    [OP_REG64] = 3,
#endif
*/
    0, 0, 1, 0, 2, 0, 0, 0, 3
};

#define NB_TEST_OPCODES 30

static const uint8_t test_bits[NB_TEST_OPCODES] = {
 0x00, /* o */
 0x01, /* no */
 0x02, /* b */
 0x02, /* c */
 0x02, /* nae */
 0x03, /* nb */
 0x03, /* nc */
 0x03, /* ae */
 0x04, /* e */
 0x04, /* z */
 0x05, /* ne */
 0x05, /* nz */
 0x06, /* be */
 0x06, /* na */
 0x07, /* nbe */
 0x07, /* a */
 0x08, /* s */
 0x09, /* ns */
 0x0a, /* p */
 0x0a, /* pe */
 0x0b, /* np */
 0x0b, /* po */
 0x0c, /* l */
 0x0c, /* nge */
 0x0d, /* nl */
 0x0d, /* ge */
 0x0e, /* le */
 0x0e, /* ng */
 0x0f, /* nle */
 0x0f, /* g */
};

static const uint8_t segment_prefixes[] = {
 0x26, /* es */
 0x2e, /* cs */
 0x36, /* ss */
 0x3e, /* ds */
 0x64, /* fs */
 0x65  /* gs */
};

static const ASMInstr 汇编_instrs[] = {
#define ALT(x) x
/* This removes a 0x0f in the second byte */
#define O(o) ((uint64_t) ((((o) & 0xff00) == 0x0f00) ? ((((o) >> 8) & ~0xff) | ((o) & 0xff)) : (o)))
/* This constructs instr_type from opcode, type and group.  */
#define T(o,i,g) ((i) | ((g) << OPC_GROUP_SHIFT) | ((((o) & 0xff00) == 0x0f00) ? OPC_0F : 0))
#define 定义_ASM_OP0(name, opcode)
#define 定义_ASM_OP0L(name, opcode, group, instr_type) { 符_汇编_ ## name, O(opcode), T(opcode, instr_type, group), 0, { 0 } },
#define 定义_ASM_OP1(name, opcode, group, instr_type, op0) { 符_汇编_ ## name, O(opcode), T(opcode, instr_type, group), 1, { op0 }},
#define 定义_ASM_OP2(name, opcode, group, instr_type, op0, op1) { 符_汇编_ ## name, O(opcode), T(opcode, instr_type, group), 2, { op0, op1 }},
#define 定义_ASM_OP3(name, opcode, group, instr_type, op0, op1, op2) { 符_汇编_ ## name, O(opcode), T(opcode, instr_type, group), 3, { op0, op1, op2 }},
#ifdef ZHI_TARGET_X86_64
# include "x86_64-asm.h"
#else
# include "i386-asm.h"
#endif
    /* last operation */
    { 0, },
};

static const uint16_t op0_codes[] = {
#define ALT(x)
#define 定义_ASM_OP0(x, opcode) opcode,
#define 定义_ASM_OP0L(name, opcode, group, instr_type)
#define 定义_ASM_OP1(name, opcode, group, instr_type, op0)
#define 定义_ASM_OP2(name, opcode, group, instr_type, op0, op1)
#define 定义_ASM_OP3(name, opcode, group, instr_type, op0, op1, op2)
#ifdef ZHI_TARGET_X86_64
# include "x86_64-asm.h"
#else
# include "i386-asm.h"
#endif
};

static inline int get_reg_shift(知心状态机 *状态机1)
{
    int shift, v;
    v = 汇编_整数_表达式(状态机1);
    switch(v) {
    case 1:
        shift = 0;
        break;
    case 2:
        shift = 1;
        break;
    case 4:
        shift = 2;
        break;
    case 8:
        shift = 3;
        break;
    default:
        应为("1, 2, 4 or 8 constant");
        shift = 0;
        break;
    }
    return shift;
}

#ifdef ZHI_TARGET_X86_64
static int 汇编_parse_numeric_reg(int t, unsigned int *type)
{
    int reg = -1;
    if (t >= 符_识别 && t < 单词_识别号) {
	const char *s = 单词表[t - 符_识别]->str;
	char c;
	*type = OP_REG64;
	if (*s == 'c') {
	    s++;
	    *type = OP_CR;
	}
	if (*s++ != 'r')
	  return -1;
	/* Don't allow leading '0'.  */
	if ((c = *s++) >= '1' && c <= '9')
	  reg = c - '0';
	else
	  return -1;
	if ((c = *s) >= '0' && c <= '5')
	  s++, reg = reg * 10 + c - '0';
	if (reg > 15)
	  return -1;
	if ((c = *s) == 0)
	  ;
	else if (*type != OP_REG64)
	  return -1;
	else if (c == 'b' && !s[1])
	  *type = OP_REG8;
	else if (c == 'w' && !s[1])
	  *type = OP_REG16;
	else if (c == 'd' && !s[1])
	  *type = OP_REG32;
	else
	  return -1;
    }
    return reg;
}
#endif

static int 汇编_parse_reg(unsigned int *type)
{
    int reg = 0;
    *type = 0;
    if (单词编码 != '%')
        goto error_32;
    带有宏替换的下个标记();
    if (单词编码 >= 符_汇编_eax && 单词编码 <= 符_汇编_edi) {
        reg = 单词编码 - 符_汇编_eax;
	*type = OP_REG32;
#ifdef ZHI_TARGET_X86_64
    } else if (单词编码 >= 符_汇编_rax && 单词编码 <= 符_汇编_rdi) {
        reg = 单词编码 - 符_汇编_rax;
	*type = OP_REG64;
    } else if (单词编码 == 符_汇编_rip) {
        reg = -2; /* Probably should use different escape code. */
	*type = OP_REG64;
    } else if ((reg = 汇编_parse_numeric_reg(单词编码, type)) >= 0
	       && (*type == OP_REG32 || *type == OP_REG64)) {
	;
#endif
    } else {
    error_32:
        应为("register");
    }
    带有宏替换的下个标记();
    return reg;
}

static void parse_operand(知心状态机 *状态机1, Operand *op)
{
    表达式值 e;
    int reg, 间接的;
    const char *p;

    间接的 = 0;
    if (单词编码 == '*') {
        带有宏替换的下个标记();
        间接的 = OP_INDIR;
    }

    if (单词编码 == '%') {
        带有宏替换的下个标记();
        if (单词编码 >= 符_汇编_al && 单词编码 <= 符_汇编_db7) {
            reg = 单词编码 - 符_汇编_al;
            op->type = 1 << (reg >> 3); /* WARNING: do not change constant order */
            op->reg = reg & 7;
            if ((op->type & OP_REG) && op->reg == TREG_XAX)
                op->type |= OP_EAX;
            else if (op->type == OP_REG8 && op->reg == TREG_XCX)
                op->type |= OP_CL;
            else if (op->type == OP_REG16 && op->reg == TREG_XDX)
                op->type |= OP_DX;
        } else if (单词编码 >= 符_汇编_dr0 && 单词编码 <= 符_汇编_dr7) {
            op->type = OP_DB;
            op->reg = 单词编码 - 符_汇编_dr0;
        } else if (单词编码 >= 符_汇编_es && 单词编码 <= 符_汇编_gs) {
            op->type = OP_SEG;
            op->reg = 单词编码 - 符_汇编_es;
        } else if (单词编码 == 符_汇编_st) {
            op->type = OP_ST;
            op->reg = 0;
            带有宏替换的下个标记();
            if (单词编码 == '(') {
                带有宏替换的下个标记();
                if (单词编码 != 常量_预处理编号)
                    goto reg_error;
                p = 单词值.str.data;
                reg = p[0] - '0';
                if ((unsigned)reg >= 8 || p[1] != '\0')
                    goto reg_error;
                op->reg = reg;
                带有宏替换的下个标记();
                跳过(')');
            }
            if (op->reg == 0)
                op->type |= OP_ST0;
            goto no_skip;
#ifdef ZHI_TARGET_X86_64
	} else if (单词编码 >= 符_汇编_spl && 单词编码 <= 符_汇编_dil) {
	    op->type = OP_REG8 | OP_REG8_LOW;
	    op->reg = 4 + 单词编码 - 符_汇编_spl;
        } else if ((op->reg = 汇编_parse_numeric_reg(单词编码, &op->type)) >= 0) {
	    ;
#endif
        } else {
        reg_error:
            错误_打印("unknown register %%%s", 取_单词字符串(单词编码, &单词值));
        }
        带有宏替换的下个标记();
    no_skip: ;
    } else if (单词编码 == '$') {
        /* constant value */
        带有宏替换的下个标记();
        汇编_表达式(状态机1, &e);
        op->type = OP_IM32;
        op->e = e;
        if (!op->e.sym) {
            if (op->e.v == (uint8_t)op->e.v)
                op->type |= OP_IM8;
            if (op->e.v == (int8_t)op->e.v)
                op->type |= OP_IM8S;
            if (op->e.v == (uint16_t)op->e.v)
                op->type |= OP_IM16;
#ifdef ZHI_TARGET_X86_64
            if (op->e.v != (int32_t)op->e.v && op->e.v != (uint32_t)op->e.v)
                op->type = OP_IM64;
#endif
        }
    } else {
        /* address(reg,reg2,shift) with all variants */
        op->type = OP_EA;
        op->reg = -1;
        op->reg2 = -1;
        op->shift = 0;
        if (单词编码 != '(') {
            汇编_表达式(状态机1, &e);
            op->e = e;
        } else {
            带有宏替换的下个标记();
            if (单词编码 == '%') {
                设为_指定标识符('(');
                op->e.v = 0;
                op->e.sym = NULL;
            } else {
                /* bracketed offset expression */
                汇编_表达式(状态机1, &e);
                if (单词编码 != ')')
                    应为(")");
                带有宏替换的下个标记();
                op->e.v = e.v;
                op->e.sym = e.sym;
            }
	    op->e.pcrel = 0;
        }
        if (单词编码 == '(') {
	    unsigned int type = 0;
            带有宏替换的下个标记();
            if (单词编码 != ',') {
                op->reg = 汇编_parse_reg(&type);
            }
            if (单词编码 == ',') {
                带有宏替换的下个标记();
                if (单词编码 != ',') {
                    op->reg2 = 汇编_parse_reg(&type);
                }
                if (单词编码 == ',') {
                    带有宏替换的下个标记();
                    op->shift = get_reg_shift(状态机1);
                }
            }
	    if (type & OP_REG32)
	        op->type |= OP_EA32;
            跳过(')');
        }
        if (op->reg == -1 && op->reg2 == -1)
            op->type |= OP_ADDR;
    }
    op->type |= 间接的;
}

/* XXX: unify with C code output ? */
静态_函数 void 生成_32位表达式(表达式值 *pe)
{
    if (pe->pcrel)
        /* If PC-relative, always set VT_符号, even without symbol,
	   so as to force a relocation to be emitted.  */
	生成_addrpc32(VT_符号, pe->sym, pe->v);
    else
	生成_addr32(pe->sym ? VT_符号 : 0, pe->sym, pe->v);
}

#ifdef ZHI_TARGET_X86_64
静态_函数 void 生成_64位表达式(表达式值 *pe)
{
    生成_地址64(pe->sym ? VT_符号 : 0, pe->sym, pe->v);
}
#endif

/* XXX: unify with C code output ? */
static void 生成_disp32(表达式值 *pe)
{
    符号 *sym = pe->sym;
    ELF符号 *esym = elf符号(sym);
    if (esym && esym->st_shndx == 当前_生成代码_段->sh_num) {
        /* same section: we can output an absolute value. Note
           that the ZHI compiler behaves differently here because
           it always outputs a relocation to ease (future) code
           elimination in the linker */
        生成_le32(pe->v + esym->st_value - 输出代码索引 - 4);
    } else {
        if (sym && sym->type.t == VT_无类型) {
            sym->type.t = VT_函数;
            sym->type.ref = NULL;
        }
        生成_addrpc32(VT_符号, sym, pe->v);
    }
}

/* generate the modrm operand */
static inline int 汇编_modrm(int reg, Operand *op)
{
    int mod, reg1, reg2, sib_reg1;

    if (op->type & (OP_REG | OP_MMX | OP_SSE)) {
        生成(0xc0 + (reg << 3) + op->reg);
    } else if (op->reg == -1 && op->reg2 == -1) {
        /* displacement only */
#ifdef ZHI_TARGET_X86_64
	生成(0x04 + (reg << 3));
	生成(0x25);
#else
	生成(0x05 + (reg << 3));
#endif
	生成_32位表达式(&op->e);
#ifdef ZHI_TARGET_X86_64
    } else if (op->reg == -2) {
        表达式值 *pe = &op->e;
        生成(0x05 + (reg << 3));
        生成_addrpc32(pe->sym ? VT_符号 : 0, pe->sym, pe->v);
        return 输出代码索引;
#endif
    } else {
        sib_reg1 = op->reg;
        /* fist compute displacement encoding */
        if (sib_reg1 == -1) {
            sib_reg1 = 5;
            mod = 0x00;
        } else if (op->e.v == 0 && !op->e.sym && op->reg != 5) {
            mod = 0x00;
        } else if (op->e.v == (int8_t)op->e.v && !op->e.sym) {
            mod = 0x40;
        } else {
            mod = 0x80;
        }
        /* compute if sib byte needed */
        reg1 = op->reg;
        if (op->reg2 != -1)
            reg1 = 4;
        生成(mod + (reg << 3) + reg1);
        if (reg1 == 4) {
            /* add sib byte */
            reg2 = op->reg2;
            if (reg2 == -1)
                reg2 = 4; /* indicate no index */
            生成((op->shift << 6) + (reg2 << 3) + sib_reg1);
        }
        /* add offset */
        if (mod == 0x40) {
            生成(op->e.v);
        } else if (mod == 0x80 || op->reg == -1) {
	    生成_32位表达式(&op->e);
        }
    }
    return 0;
}

#ifdef ZHI_TARGET_X86_64
#define REX_W 0x48
#define REX_R 0x44
#define REX_X 0x42
#define REX_B 0x41

static void 汇编_rex(int width64, Operand *ops, int 数量_ops, int *op_type,
		    int regi, int rmi)
{
  unsigned char rex = width64 ? 0x48 : 0;
  int saw_high_8bit = 0;
  int i;
  if (rmi == -1) {
      /* No mod/rm byte, but we might have a register op nevertheless
         (we will add it to the opcode later).  */
      for(i = 0; i < 数量_ops; i++) {
	  if (op_type[i] & (OP_REG | OP_ST)) {
	      if (ops[i].reg >= 8) {
		  rex |= REX_B;
		  ops[i].reg -= 8;
	      } else if (ops[i].type & OP_REG8_LOW)
		  rex |= 0x40;
	      else if (ops[i].type & OP_REG8 && ops[i].reg >= 4)
		  /* An 8 bit reg >= 4 without REG8 is ah/ch/dh/bh */
		  saw_high_8bit = ops[i].reg;
	      break;
	  }
      }
  } else {
      if (regi != -1) {
	  if (ops[regi].reg >= 8) {
	      rex |= REX_R;
	      ops[regi].reg -= 8;
	  } else if (ops[regi].type & OP_REG8_LOW)
	      rex |= 0x40;
	  else if (ops[regi].type & OP_REG8 && ops[regi].reg >= 4)
	      /* An 8 bit reg >= 4 without REG8 is ah/ch/dh/bh */
	      saw_high_8bit = ops[regi].reg;
      }
      if (ops[rmi].type & (OP_REG | OP_MMX | OP_SSE | OP_CR | OP_EA)) {
	  if (ops[rmi].reg >= 8) {
	      rex |= REX_B;
	      ops[rmi].reg -= 8;
	  } else if (ops[rmi].type & OP_REG8_LOW)
	      rex |= 0x40;
	  else if (ops[rmi].type & OP_REG8 && ops[rmi].reg >= 4)
	      /* An 8 bit reg >= 4 without REG8 is ah/ch/dh/bh */
	      saw_high_8bit = ops[rmi].reg;
      }
      if (ops[rmi].type & OP_EA && ops[rmi].reg2 >= 8) {
	  rex |= REX_X;
	  ops[rmi].reg2 -= 8;
      }
  }
  if (rex) {
      if (saw_high_8bit)
	  错误_打印("can't encode register %%%ch when REX prefix is required",
		    "acdb"[saw_high_8bit-4]);
      生成(rex);
  }
}
#endif

static void maybe_print_stats (void)
{
  static int already = 1;
  if (!already)
    /* print stats about opcodes */
    {
        const struct ASMInstr *pa;
        int freq[4];
        int op_vals[500];
        int 数量_op_vals, i, j;

	already = 1;
        数量_op_vals = 0;
        memset(freq, 0, sizeof(freq));
        for(pa = 汇编_instrs; pa->sym != 0; pa++) {
            freq[pa->数量_ops]++;
            //for(i=0;i<pa->数量_ops;i++) {
                for(j=0;j<数量_op_vals;j++) {
                    //if (pa->op_type[i] == op_vals[j])
                    if (pa->instr_type == op_vals[j])
                        goto found;
                }
                //op_vals[数量_op_vals++] = pa->op_type[i];
                op_vals[数量_op_vals++] = pa->instr_type;
            found: ;
            //}
        }
        for(i=0;i<数量_op_vals;i++) {
            int v = op_vals[i];
            //if ((v & (v - 1)) != 0)
                printf("%3d: %08x\n", i, v);
        }
        printf("size=%d nb=%d f0=%d f1=%d f2=%d f3=%d\n",
               (int)sizeof(汇编_instrs),
	       (int)sizeof(汇编_instrs) / (int)sizeof(ASMInstr),
               freq[0], freq[1], freq[2], freq[3]);
    }
}

静态_函数 void 汇编_指令代码(知心状态机 *状态机1, int opcode)
{
    const ASMInstr *pa;
    int i, modrm_index, modreg_index, reg, v, op1, seg_prefix, pc;
    int 数量_ops, s;
    Operand ops[MAX_OPERANDS], *pop;
    int op_type[3]; /* decoded op type */
    int alltypes;   /* OR of all operand types */
    int autosize;
    int p66;
#ifdef ZHI_TARGET_X86_64
    int rex64;
#endif

    maybe_print_stats();
    /* force synthetic ';' after prefix instruction, so we can handle */
    /* one-line things like "rep stosb" instead of only "rep\nstosb" */
    if (opcode >= 符_汇编_wait && opcode <= 符_汇编_repnz)
        设为_指定标识符(';');

    /* get operands */
    pop = ops;
    数量_ops = 0;
    seg_prefix = 0;
    alltypes = 0;
    for(;;) {
        if (单词编码 == ';' || 单词编码 == 符_换行)
            break;
        if (数量_ops >= MAX_OPERANDS) {
            错误_打印("incorrect number of operands");
        }
        parse_operand(状态机1, pop);
        if (单词编码 == ':') {
           if (pop->type != OP_SEG || seg_prefix)
               错误_打印("incorrect prefix");
           seg_prefix = segment_prefixes[pop->reg];
           带有宏替换的下个标记();
           parse_operand(状态机1, pop);
           if (!(pop->type & OP_EA)) {
               错误_打印("segment prefix must be followed by memory reference");
           }
        }
        pop++;
        数量_ops++;
        if (单词编码 != ',')
            break;
        带有宏替换的下个标记();
    }

    s = 0; /* avoid warning */

again:
    /* 编译优化 matching by using a lookup table (no hashing is needed
       !) */
    for(pa = 汇编_instrs; pa->sym != 0; pa++) {
	int it = pa->instr_type & OPCT_MASK;
        s = 0;
        if (it == OPC_FARITH) {
            v = opcode - pa->sym;
            if (!((unsigned)v < 8 * 6 && (v % 6) == 0))
                continue;
        } else if (it == OPC_ARITH) {
            if (!(opcode >= pa->sym && opcode < pa->sym + 8*NBWLX))
                continue;
            s = (opcode - pa->sym) % NBWLX;
	    if ((pa->instr_type & OPC_BWLX) == OPC_WLX)
	      {
		/* We need to reject the xxxb opcodes that we accepted above.
		   Note that pa->sym for WLX opcodes is the 'w' 标识符,
		   to get the 'b' 标识符 subtract one.  */
		if (((opcode - pa->sym + 1) % NBWLX) == 0)
		    continue;
	        s++;
	      }
        } else if (it == OPC_SHIFT) {
            if (!(opcode >= pa->sym && opcode < pa->sym + 7*NBWLX))
                continue;
            s = (opcode - pa->sym) % NBWLX;
        } else if (it == OPC_TEST) {
            if (!(opcode >= pa->sym && opcode < pa->sym + NB_TEST_OPCODES))
                continue;
	    /* cmovxx is a test opcode but accepts multiple sizes.
	       The suffixes aren't encoded in the table, instead we
	       simply force size autodetection always and deal with suffixed
	       variants below when we don't find e.g. "cmovzl".  */
	    if (pa->instr_type & OPC_WLX)
	        s = NBWLX - 1;
        } else if (pa->instr_type & OPC_B) {
#ifdef ZHI_TARGET_X86_64
	    /* Some instructions don't have the full size but only
	       bwl form.  insb e.g. */
	    if ((pa->instr_type & OPC_WLQ) != OPC_WLQ
		&& !(opcode >= pa->sym && opcode < pa->sym + NBWLX-1))
	        continue;
#endif
            if (!(opcode >= pa->sym && opcode < pa->sym + NBWLX))
                continue;
            s = opcode - pa->sym;
        } else if (pa->instr_type & OPC_WLX) {
            if (!(opcode >= pa->sym && opcode < pa->sym + NBWLX-1))
                continue;
            s = opcode - pa->sym + 1;
        } else {
            if (pa->sym != opcode)
                continue;
        }
        if (pa->数量_ops != 数量_ops)
            continue;
#ifdef ZHI_TARGET_X86_64
	/* Special case for moves.  Selecting the IM64->REG64 form
	   should only be done if we really have an >32bit imm64, and that
	   is hardcoded.  Ignore it here.  */
	if (pa->opcode == 0xb0 && ops[0].type != OP_IM64
	    && (ops[1].type & OP_REG) == OP_REG64
	    && !(pa->instr_type & OPC_0F))
	    continue;
#endif
        /* now decode and check each operand */
	alltypes = 0;
        for(i = 0; i < 数量_ops; i++) {
            int op1, op2;
            op1 = pa->op_type[i];
            op2 = op1 & 0x1f;
            switch(op2) {
            case 指令_IM:
                v = OP_IM8 | OP_IM16 | OP_IM32;
                break;
            case 指令_REG:
                v = OP_REG8 | OP_REG16 | OP_REG32 | OP_REG64;
                break;
            case 指令_REGW:
                v = OP_REG16 | OP_REG32 | OP_REG64;
                break;
            case 指令_IMW:
                v = OP_IM16 | OP_IM32;
                break;
	    case 指令_MMXSSE:
		v = OP_MMX | OP_SSE;
		break;
	    case 指令_DISP:
	    case 指令_DISP8:
		v = OP_ADDR;
		break;
            default:
                v = 1 << op2;
                break;
            }
            if (op1 & 指令_EA)
                v |= OP_EA;
	    op_type[i] = v;
            if ((ops[i].type & v) == 0)
                goto next;
	    alltypes |= ops[i].type;
        }
        /* all is matching ! */
        break;
    next: ;
    }
    if (pa->sym == 0) {
        if (opcode >= 符_汇编_first && opcode <= 符_汇编_last) {
            int b;
            b = op0_codes[opcode - 符_汇编_first];
            if (b & 0xff00) 
                生成(b >> 8);
            生成(b);
            return;
        } else if (opcode <= 符_汇编_alllast) {
            错误_打印("bad operand with opcode '%s'",
                  取_单词字符串(opcode, NULL));
        } else {
	    /* Special case for cmovcc, we accept size suffixes but ignore
	       them, but we don't want them to blow up our tables.  */
	    单词存储结构 *ts = 单词表[opcode - 符_识别];
	    if (ts->len >= 6
		&& strchr("wlq", ts->str[ts->len-1])
		&& !memcmp(ts->str, "cmov", 4)) {
		opcode = 单词表_查找(ts->str, ts->len-1)->单词编码;
		goto again;
	    }
            错误_打印("unknown opcode '%s'", ts->str);
        }
    }
    /* if the size is unknown, then evaluate it (OPC_B or OPC_WL case) */
    autosize = NBWLX-1;
#ifdef ZHI_TARGET_X86_64
    /* XXX the autosize should rather be zero, to not have to adjust this
       all the time.  */
    if ((pa->instr_type & OPC_BWLQ) == OPC_B)
        autosize = NBWLX-2;
#endif
    if (s == autosize) {
	/* Check for register operands providing hints about the size.
	   Start from the end, i.e. destination operands.  This matters
	   only for opcodes accepting different sized registers, lar and lsl
	   are such opcodes.  */
        for(i = 数量_ops - 1; s == autosize && i >= 0; i--) {
            if ((ops[i].type & OP_REG) && !(op_type[i] & (OP_CL | OP_DX)))
                s = reg_to_size[ops[i].type & OP_REG];
        }
        if (s == autosize) {
            if ((opcode == 符_汇编_push || opcode == 符_汇编_pop) &&
                (ops[0].type & (OP_SEG | OP_IM8S | OP_IM32)))
                s = 2;
	    else if ((opcode == 符_汇编_push || opcode == 符_汇编_pop) &&
		     (ops[0].type & OP_EA))
	        s = NBWLX - 2;
            else
                错误_打印("cannot infer opcode suffix");
        }
    }

#ifdef ZHI_TARGET_X86_64
    /* Generate addr32 prefix if needed */
    for(i = 0; i < 数量_ops; i++) {
        if (ops[i].type & OP_EA32) {
	    生成(0x67);
	    break;
        }
    }
#endif
    /* generate data16 prefix if needed */
    p66 = 0;
    if (s == 1)
        p66 = 1;
    else {
	/* accepting mmx+sse in all operands --> needs 0x66 to
	   switch to sse mode.  Accepting only sse in an operand --> is
	   already SSE insn and needs 0x66/f2/f3 handling.  */
        for (i = 0; i < 数量_ops; i++)
            if ((op_type[i] & (OP_MMX | OP_SSE)) == (OP_MMX | OP_SSE)
	        && ops[i].type & OP_SSE)
	        p66 = 1;
    }
    if (p66)
        生成(0x66);
#ifdef ZHI_TARGET_X86_64
    rex64 = 0;
    if (pa->instr_type & OPC_48)
        rex64 = 1;
    else if (s == 3 || (alltypes & OP_REG64)) {
        /* generate REX prefix */
	int default64 = 0;
	for(i = 0; i < 数量_ops; i++) {
	    if (op_type[i] == OP_REG64 && pa->opcode != 0xb8) {
		/* If only 64bit regs are accepted in one operand
		   this is a default64 instruction without need for
		   REX prefixes, except for movabs(0xb8).  */
		default64 = 1;
		break;
	    }
	}
	/* XXX find better encoding for the default64 instructions.  */
        if (((opcode != 符_汇编_push && opcode != 符_汇编_pop
	      && opcode != 符_汇编_pushw && opcode != 符_汇编_pushl
	      && opcode != 符_汇编_pushq && opcode != 符_汇编_popw
	      && opcode != 符_汇编_popl && opcode != 符_汇编_popq
	      && opcode != 符_汇编_call && opcode != 符_汇编_jmp))
	    && !default64)
            rex64 = 1;
    }
#endif

    /* now generates the operation */
    if (OPCT_IS(pa->instr_type, OPC_FWAIT))
        生成(0x9b);
    if (seg_prefix)
        生成(seg_prefix);

    v = pa->opcode;
    if (pa->instr_type & OPC_0F)
        v = ((v & ~0xff) << 8) | 0x0f00 | (v & 0xff);
    if ((v == 0x69 || v == 0x6b) && 数量_ops == 2) {
        /* kludge for imul $im, %reg */
        数量_ops = 3;
        ops[2] = ops[1];
        op_type[2] = op_type[1];
    } else if (v == 0xcd && ops[0].e.v == 3 && !ops[0].e.sym) {
        v--; /* int $3 case */
        数量_ops = 0;
    } else if ((v == 0x06 || v == 0x07)) {
        if (ops[0].reg >= 4) {
            /* push/pop %fs or %gs */
            v = 0x0fa0 + (v - 0x06) + ((ops[0].reg - 4) << 3);
        } else {
            v += ops[0].reg << 3;
        }
        数量_ops = 0;
    } else if (v <= 0x05) {
        /* arith case */
        v += ((opcode - 符_汇编_addb) / NBWLX) << 3;
    } else if ((pa->instr_type & (OPCT_MASK | OPC_MODRM)) == OPC_FARITH) {
        /* fpu arith case */
        v += ((opcode - pa->sym) / 6) << 3;
    }

    /* search which operand will be used for modrm */
    modrm_index = -1;
    modreg_index = -1;
    if (pa->instr_type & OPC_MODRM) {
	if (!数量_ops) {
	    /* A modrm opcode without operands is a special case (e.g. mfence).
	       It has a group and acts as if there's an register operand 0
	       (ax).  */
	    i = 0;
	    ops[i].type = OP_REG;
	    ops[i].reg = 0;
	    goto modrm_found;
	}
        /* first look for an ea operand */
        for(i = 0;i < 数量_ops; i++) {
            if (op_type[i] & OP_EA)
                goto modrm_found;
        }
        /* then if not found, a register or 间接的ection (shift instructions) */
        for(i = 0;i < 数量_ops; i++) {
            if (op_type[i] & (OP_REG | OP_MMX | OP_SSE | OP_INDIR))
                goto modrm_found;
        }
#ifdef ASM_DEBUG
        错误_打印("bad op table");
#endif
    modrm_found:
        modrm_index = i;
        /* if a register is used in another operand then it is
           used instead of group */
        for(i = 0;i < 数量_ops; i++) {
            int t = op_type[i];
            if (i != modrm_index &&
                (t & (OP_REG | OP_MMX | OP_SSE | OP_CR | OP_TR | OP_DB | OP_SEG))) {
                modreg_index = i;
                break;
            }
        }
    }
#ifdef ZHI_TARGET_X86_64
    汇编_rex (rex64, ops, 数量_ops, op_type, modreg_index, modrm_index);
#endif

    if (pa->instr_type & OPC_REG) {
        /* mov $im, %reg case */
        if (v == 0xb0 && s >= 1)
            v += 7;
        for(i = 0; i < 数量_ops; i++) {
            if (op_type[i] & (OP_REG | OP_ST)) {
                v += ops[i].reg;
                break;
            }
        }
    }
    if (pa->instr_type & OPC_B)
        v += s >= 1;
    if (数量_ops == 1 && pa->op_type[0] == 指令_DISP8) {
	ELF符号 *esym;
        int jmp_disp;

        /* see if we can really generate the jump with a byte offset */
	esym = elf符号(ops[0].e.sym);
        if (!esym || esym->st_shndx != 当前_生成代码_段->sh_num)
            goto no_short_jump;
        jmp_disp = ops[0].e.v + esym->st_value - 输出代码索引 - 2 - (v >= 0xff);
        if (jmp_disp == (int8_t)jmp_disp) {
            /* OK to generate jump */
	    ops[0].e.sym = 0;
            ops[0].e.v = jmp_disp;
	    op_type[0] = OP_IM8S;
        } else {
        no_short_jump:
	    /* long jump will be allowed. need to modify the
	       opcode slightly */
	    if (v == 0xeb) /* jmp */
	        v = 0xe9;
	    else if (v == 0x70) /* jcc */
	        v += 0x0f10;
	    else
	        错误_打印("invalid displacement");
        }
    }
    if (OPCT_IS(pa->instr_type, OPC_TEST))
        v += test_bits[opcode - pa->sym];
    op1 = v >> 16;
    if (op1)
        生成(op1);
    op1 = (v >> 8) & 0xff;
    if (op1)
        生成(op1);
    生成(v);

    if (OPCT_IS(pa->instr_type, OPC_SHIFT)) {
        reg = (opcode - pa->sym) / NBWLX;
        if (reg == 6)
            reg = 7;
    } else if (OPCT_IS(pa->instr_type, OPC_ARITH)) {
        reg = (opcode - pa->sym) / NBWLX;
    } else if (OPCT_IS(pa->instr_type, OPC_FARITH)) {
        reg = (opcode - pa->sym) / 6;
    } else {
        reg = (pa->instr_type >> OPC_GROUP_SHIFT) & 7;
    }

    pc = 0;
    if (pa->instr_type & OPC_MODRM) {
        /* if a register is used in another operand then it is
           used instead of group */
	if (modreg_index >= 0)
	    reg = ops[modreg_index].reg;
        pc = 汇编_modrm(reg, &ops[modrm_index]);
    }

    /* emit constants */
#ifndef ZHI_TARGET_X86_64
    if (!(pa->instr_type & OPC_0F)
	&& (pa->opcode == 0x9a || pa->opcode == 0xea)) {
        /* ljmp or lcall kludge */
	生成_32位表达式(&ops[1].e);
        if (ops[0].e.sym)
            错误_打印("cannot 重定位");
        生成_le16(ops[0].e.v);
        return;
    }
#endif
    for(i = 0;i < 数量_ops; i++) {
        v = op_type[i];
        if (v & (OP_IM8 | OP_IM16 | OP_IM32 | OP_IM64 | OP_IM8S | OP_ADDR)) {
            /* if multiple sizes are given it means we must look
               at the op size */
            if ((v | OP_IM8 | OP_IM64) == (OP_IM8 | OP_IM16 | OP_IM32 | OP_IM64)) {
                if (s == 0)
                    v = OP_IM8;
                else if (s == 1)
                    v = OP_IM16;
                else if (s == 2 || (v & OP_IM64) == 0)
                    v = OP_IM32;
                else
                    v = OP_IM64;
            }

            if ((v & (OP_IM8 | OP_IM8S | OP_IM16)) && ops[i].e.sym)
                错误_打印("cannot 重定位");

            if (v & (OP_IM8 | OP_IM8S)) {
                生成(ops[i].e.v);
            } else if (v & OP_IM16) {
                生成_le16(ops[i].e.v);
#ifdef ZHI_TARGET_X86_64
            } else if (v & OP_IM64) {
                生成_64位表达式(&ops[i].e);
#endif
	    } else if (pa->op_type[i] == 指令_DISP || pa->op_type[i] == 指令_DISP8) {
                生成_disp32(&ops[i].e);
            } else {
                生成_32位表达式(&ops[i].e);
            }
        }
    }

    /* after immediate operands, adjust pc-relative address */
    if (pc)
        增加32le(当前_生成代码_段->data + pc - 4, pc - 输出代码索引);
}

/* return the constraint priority (we allocate first the lowest
   numbered constraints) */
static inline int constraint_priority(const char *str)
{
    int priority, c, pr;

    /* we take the lowest priority */
    priority = 0;
    for(;;) {
        c = *str;
        if (c == '\0')
            break;
        str++;
        switch(c) {
        case 'A':
            pr = 0;
            break;
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'S':
        case 'D':
            pr = 1;
            break;
        case 'q':
            pr = 2;
            break;
        case 'r':
	case 'R':
	case 'p':
            pr = 3;
            break;
        case 'N':
        case 'M':
        case 'I':
	case 'e':
        case 'i':
        case 'm':
        case 'g':
            pr = 4;
            break;
        default:
            错误_打印("unknown constraint '%c'", c);
            pr = 0;
        }
        if (pr > priority)
            priority = pr;
    }
    return priority;
}

static const char *skip_constraint_modifiers(const char *p)
{
    while (*p == '=' || *p == '&' || *p == '+' || *p == '%')
        p++;
    return p;
}

/* If T (a 标识符) is of the form "%reg" returns the register
   number and type, otherwise return -1.  */
静态_函数 int 汇编_解析_注册变量 (int t)
{
    const char *s;
    Operand op;
    if (t < 符_识别)
        return -1;
    s = 单词表[t - 符_识别]->str;
    if (s[0] != '%')
        return -1;
    t = 单词表_查找(s+1, strlen(s)-1)->单词编码;
    设为_指定标识符(t);
    设为_指定标识符('%');
    parse_operand(zhi_状态, &op);
    /* Accept only integer regs for now.  */
    if (op.type & OP_REG)
        return op.reg;
    else
        return -1;
}

#define REG_OUT_MASK 0x01
#define REG_IN_MASK  0x02

#define is_reg_allocated(reg) (regs_allocated[reg] & reg_mask)

静态_函数 void 汇编_计算_约束(汇编操作数 *operands,
                                    int 数量_operands, int 数量_outputs,
                                    const uint8_t *clobber_regs,
                                    int *pout_reg)
{
    汇编操作数 *op;
    int sorted_op[最大_汇编_操作数];
    int i, j, k, p1, p2, tmp, reg, c, reg_mask;
    const char *str;
    uint8_t regs_allocated[NB_ASM_REGS];

    /* init fields */
    for(i=0;i<数量_operands;i++) {
        op = &operands[i];
        op->input_index = -1;
        op->ref_index = -1;
        op->reg = -1;
        op->is_memory = 0;
        op->is_rw = 0;
    }
    /* compute constraint priority and evaluate references to output
       constraints if input constraints */
    for(i=0;i<数量_operands;i++) {
        op = &operands[i];
        str = op->constraint;
        str = skip_constraint_modifiers(str);
        if (是数字(*str) || *str == '[') {
            /* this is a reference to another constraint */
            k = 查找_约束(operands, 数量_operands, str, NULL);
            if ((unsigned)k >= i || i < 数量_outputs)
                错误_打印("invalid reference in constraint %d ('%s')",
                      i, str);
            op->ref_index = k;
            if (operands[k].input_index >= 0)
                错误_打印("cannot reference twice the same operand");
            operands[k].input_index = i;
            op->priority = 5;
	} else if ((op->vt->r & VT_值掩码) == VT_LOCAL
		   && op->vt->sym
		   && (reg = op->vt->sym->r & VT_值掩码) < VT_VC常量) {
	    op->priority = 1;
	    op->reg = reg;
        } else {
            op->priority = constraint_priority(str);
        }
    }

    /* sort operands according to their priority */
    for(i=0;i<数量_operands;i++)
        sorted_op[i] = i;
    for(i=0;i<数量_operands - 1;i++) {
        for(j=i+1;j<数量_operands;j++) {
            p1 = operands[sorted_op[i]].priority;
            p2 = operands[sorted_op[j]].priority;
            if (p2 < p1) {
                tmp = sorted_op[i];
                sorted_op[i] = sorted_op[j];
                sorted_op[j] = tmp;
            }
        }
    }

    for(i = 0;i < NB_ASM_REGS; i++) {
        if (clobber_regs[i])
            regs_allocated[i] = REG_IN_MASK | REG_OUT_MASK;
        else
            regs_allocated[i] = 0;
    }
    /* esp cannot be used */
    regs_allocated[4] = REG_IN_MASK | REG_OUT_MASK;
    /* ebp cannot be used yet */
    regs_allocated[5] = REG_IN_MASK | REG_OUT_MASK;

    /* allocate registers and generate corresponding asm moves */
    for(i=0;i<数量_operands;i++) {
        j = sorted_op[i];
        op = &operands[j];
        str = op->constraint;
        /* no need to allocate references */
        if (op->ref_index >= 0)
            continue;
        /* select if register is used for output, input or both */
        if (op->input_index >= 0) {
            reg_mask = REG_IN_MASK | REG_OUT_MASK;
        } else if (j < 数量_outputs) {
            reg_mask = REG_OUT_MASK;
        } else {
            reg_mask = REG_IN_MASK;
        }
	if (op->reg >= 0) {
	    if (is_reg_allocated(op->reg))
	        错误_打印("asm regvar requests register that's taken already");
	    reg = op->reg;
	    goto reg_found;
	}
    try_next:
        c = *str++;
        switch(c) {
        case '=':
            goto try_next;
        case '+':
            op->is_rw = 1;
            /* FALL THRU */
        case '&':
            if (j >= 数量_outputs)
                错误_打印("'%c' modifier can only be applied to outputs", c);
            reg_mask = REG_IN_MASK | REG_OUT_MASK;
            goto try_next;
        case 'A':
            /* allocate both eax and edx */
            if (is_reg_allocated(TREG_XAX) ||
                is_reg_allocated(TREG_XDX))
                goto try_next;
            op->is_llong = 1;
            op->reg = TREG_XAX;
            regs_allocated[TREG_XAX] |= reg_mask;
            regs_allocated[TREG_XDX] |= reg_mask;
            break;
        case 'a':
            reg = TREG_XAX;
            goto alloc_reg;
        case 'b':
            reg = 3;
            goto alloc_reg;
        case 'c':
            reg = TREG_XCX;
            goto alloc_reg;
        case 'd':
            reg = TREG_XDX;
            goto alloc_reg;
        case 'S':
            reg = 6;
            goto alloc_reg;
        case 'D':
            reg = 7;
        alloc_reg:
            if (is_reg_allocated(reg))
                goto try_next;
            goto reg_found;
        case 'q':
            /* eax, ebx, ecx or edx */
            for(reg = 0; reg < 4; reg++) {
                if (!is_reg_allocated(reg))
                    goto reg_found;
            }
            goto try_next;
        case 'r':
	case 'R':
	case 'p': /* A general address, for x86(64) any register is acceptable*/
            /* any general register */
            for(reg = 0; reg < 8; reg++) {
                if (!is_reg_allocated(reg))
                    goto reg_found;
            }
            goto try_next;
        reg_found:
            /* now we can reload in the register */
            op->is_llong = 0;
            op->reg = reg;
            regs_allocated[reg] |= reg_mask;
            break;
	case 'e':
        case 'i':
            if (!((op->vt->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量))
                goto try_next;
            break;
        case 'I':
        case 'N':
        case 'M':
            if (!((op->vt->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量))
                goto try_next;
            break;
        case 'm':
        case 'g':
            /* nothing special to do because the operand is already in
               memory, except if the pointer itself is stored in a
               memory variable (VT_LLOCAL case) */
            /* XXX: fix constant case */
            /* if it is a reference to a memory zone, it must lie
               in a register, so we reserve the register in the
               input registers and a 加载 will be generated
               later */
            if (j < 数量_outputs || c == 'm') {
                if ((op->vt->r & VT_值掩码) == VT_LLOCAL) {
                    /* any general register */
                    for(reg = 0; reg < 8; reg++) {
                        if (!(regs_allocated[reg] & REG_IN_MASK))
                            goto reg_found1;
                    }
                    goto try_next;
                reg_found1:
                    /* now we can reload in the register */
                    regs_allocated[reg] |= REG_IN_MASK;
                    op->reg = reg;
                    op->is_memory = 1;
                }
            }
            break;
        default:
            错误_打印("asm constraint %d ('%s') could not be satisfied",
                  j, op->constraint);
            break;
        }
        /* if a reference is present for that operand, we assign it too */
        if (op->input_index >= 0) {
            operands[op->input_index].reg = op->reg;
            operands[op->input_index].is_llong = op->is_llong;
        }
    }

    /* compute out_reg. It is used to 存储 outputs registers to memory
       locations references by pointers (VT_LLOCAL case) */
    *pout_reg = -1;
    for(i=0;i<数量_operands;i++) {
        op = &operands[i];
        if (op->reg >= 0 &&
            (op->vt->r & VT_值掩码) == VT_LLOCAL  &&
            !op->is_memory) {
            for(reg = 0; reg < 8; reg++) {
                if (!(regs_allocated[reg] & REG_OUT_MASK))
                    goto reg_found2;
            }
            错误_打印("could not find free output register for reloading");
        reg_found2:
            *pout_reg = reg;
            break;
        }
    }

    /* print sorted constraints */
#ifdef ASM_DEBUG
    for(i=0;i<数量_operands;i++) {
        j = sorted_op[i];
        op = &operands[j];
        printf("%%%d [%s]: \"%s\" r=0x%04x reg=%d\n",
               j,
               op->id ? 取_单词字符串(op->id, NULL) : "",
               op->constraint,
               op->vt->r,
               op->reg);
    }
    if (*pout_reg >= 0)
        printf("out_reg=%d\n", *pout_reg);
#endif
}

静态_函数 void 替换_汇编_操作数(动态字符串 *add_str,堆栈值 *sv, int modifier)
{
    int r, reg, size, val;
    char buf[64];

    r = sv->r;
    if ((r & VT_值掩码) == VT_VC常量) {
        if (!(r & VT_LVAL) && modifier != 'c' && modifier != 'n' &&
	    modifier != 'P')
            动态字符串_追加单个字符(add_str, '$');
        if (r & VT_符号) {
	    const char *name = 取_单词字符串(sv->sym->v, NULL);
	    if (sv->sym->v >= 符号_第一个_匿名) {
		/* In case of anonymous symbols ("L.42", used
		   for static data labels) we can't find them
		   in the C symbol table when later looking up
		   this name.  So enter them now into the asm label
		   list when we still know the symbol.  */
		获取_汇编_符号(单词表_查找(name, strlen(name))->单词编码, sv->sym);
	    }
            if (zhi_状态->前导_下划线)
              动态字符串_追加单个字符(add_str, '_');
            动态字符串_cat(add_str, name, -1);
            if ((uint32_t)sv->c.i == 0)
                goto no_offset;
	    动态字符串_追加单个字符(add_str, '+');
        }
        val = sv->c.i;
        if (modifier == 'n')
            val = -val;
        snprintf(buf, sizeof(buf), "%d", (int)sv->c.i);
        动态字符串_cat(add_str, buf, -1);
    no_offset:;
#ifdef ZHI_TARGET_X86_64
        if (r & VT_LVAL)
            动态字符串_cat(add_str, "(%rip)", -1);
#endif
    } else if ((r & VT_值掩码) == VT_LOCAL) {
#ifdef ZHI_TARGET_X86_64
        snprintf(buf, sizeof(buf), "%d(%%rbp)", (int)sv->c.i);
#else
        snprintf(buf, sizeof(buf), "%d(%%ebp)", (int)sv->c.i);
#endif
        动态字符串_cat(add_str, buf, -1);
    } else if (r & VT_LVAL) {
        reg = r & VT_值掩码;
        if (reg >= VT_VC常量)
            错误_打印("internal compiler error");
        snprintf(buf, sizeof(buf), "(%%%s)",
#ifdef ZHI_TARGET_X86_64
                 取_单词字符串(符_汇编_rax + reg, NULL)
#else
                 取_单词字符串(符_汇编_eax + reg, NULL)
#endif
		 );
        动态字符串_cat(add_str, buf, -1);
    } else {
        /* register case */
        reg = r & VT_值掩码;
        if (reg >= VT_VC常量)
            错误_打印("internal compiler error");

        /* choose register operand size */
        if ((sv->type.t & VT_基本类型) == VT_字节 ||
	    (sv->type.t & VT_基本类型) == VT_逻辑)
            size = 1;
        else if ((sv->type.t & VT_基本类型) == VT_短整数)
            size = 2;
#ifdef ZHI_TARGET_X86_64
        else if ((sv->type.t & VT_基本类型) == VT_长长整数 ||
		 (sv->type.t & VT_基本类型) == VT_指针)
            size = 8;
#endif
        else
            size = 4;
        if (size == 1 && reg >= 4)
            size = 4;

        if (modifier == 'b') {
            if (reg >= 4)
                错误_打印("cannot use byte register");
            size = 1;
        } else if (modifier == 'h') {
            if (reg >= 4)
                错误_打印("cannot use byte register");
            size = -1;
        } else if (modifier == 'w') {
            size = 2;
        } else if (modifier == 'k') {
            size = 4;
#ifdef ZHI_TARGET_X86_64
        } else if (modifier == 'q') {
            size = 8;
#endif
        }

        switch(size) {
        case -1:
            reg = 符_汇编_ah + reg;
            break;
        case 1:
            reg = 符_汇编_al + reg;
            break;
        case 2:
            reg = 符_汇编_ax + reg;
            break;
        default:
            reg = 符_汇编_eax + reg;
            break;
#ifdef ZHI_TARGET_X86_64
        case 8:
            reg = 符_汇编_rax + reg;
            break;
#endif
        }
        snprintf(buf, sizeof(buf), "%%%s", 取_单词字符串(reg, NULL));
        动态字符串_cat(add_str, buf, -1);
    }
}

/* generate prolog and epilog code for asm statement */
静态_函数 void 汇编_生成_代码(汇编操作数 *operands, int 数量_operands,
                         int 数量_outputs, int is_output,
                         uint8_t *clobber_regs,
                         int out_reg)
{
    uint8_t regs_allocated[NB_ASM_REGS];
    汇编操作数 *op;
    int i, reg;

    /* Strictly speaking %Xbp and %Xsp should be included in the
       call-preserved registers, but currently it doesn't matter.  */
#ifdef ZHI_TARGET_X86_64
#ifdef ZHI_TARGET_PE
    static uint8_t reg_saved[] = { 3, 6, 7, 12, 13, 14, 15 };
#else
    static uint8_t reg_saved[] = { 3, 12, 13, 14, 15 };
#endif
#else
    static uint8_t reg_saved[] = { 3, 6, 7 };
#endif

    /* mark all used registers */
    memcpy(regs_allocated, clobber_regs, sizeof(regs_allocated));
    for(i = 0; i < 数量_operands;i++) {
        op = &operands[i];
        if (op->reg >= 0)
            regs_allocated[op->reg] = 1;
    }
    if (!is_output) {
        /* generate reg save code */
        for(i = 0; i < sizeof(reg_saved)/sizeof(reg_saved[0]); i++) {
            reg = reg_saved[i];
            if (regs_allocated[reg]) {
		if (reg >= 8)
		  生成(0x41), reg-=8;
                生成(0x50 + reg);
            }
        }

        /* generate 加载 code */
        for(i = 0; i < 数量_operands; i++) {
            op = &operands[i];
            if (op->reg >= 0) {
                if ((op->vt->r & VT_值掩码) == VT_LLOCAL &&
                    op->is_memory) {
                    /* memory reference case (for both input and
                       output cases) */
                    堆栈值 sv;
                    sv = *op->vt;
                    sv.r = (sv.r & ~VT_值掩码) | VT_LOCAL | VT_LVAL;
                    sv.type.t = VT_指针;
                    加载(op->reg, &sv);
                } else if (i >= 数量_outputs || op->is_rw) {
                    /* 加载 value in register */
                    加载(op->reg, op->vt);
                    if (op->is_llong) {
                        堆栈值 sv;
                        sv = *op->vt;
                        sv.c.i += 4;
                        加载(TREG_XDX, &sv);
                    }
                }
            }
        }
    } else {
        /* generate save code */
        for(i = 0 ; i < 数量_outputs; i++) {
            op = &operands[i];
            if (op->reg >= 0) {
                if ((op->vt->r & VT_值掩码) == VT_LLOCAL) {
                    if (!op->is_memory) {
                        堆栈值 sv;
                        sv = *op->vt;
                        sv.r = (sv.r & ~VT_值掩码) | VT_LOCAL;
			sv.type.t = VT_指针;
                        加载(out_reg, &sv);

			sv = *op->vt;
                        sv.r = (sv.r & ~VT_值掩码) | out_reg;
                        存储(op->reg, &sv);
                    }
                } else {
                    存储(op->reg, op->vt);
                    if (op->is_llong) {
                        堆栈值 sv;
                        sv = *op->vt;
                        sv.c.i += 4;
                        存储(TREG_XDX, &sv);
                    }
                }
            }
        }
        /* generate reg restore code */
        for(i = sizeof(reg_saved)/sizeof(reg_saved[0]) - 1; i >= 0; i--) {
            reg = reg_saved[i];
            if (regs_allocated[reg]) {
		if (reg >= 8)
		  生成(0x41), reg-=8;
                生成(0x58 + reg);
            }
        }
    }
}

静态_函数 void 汇编_破坏者(uint8_t *clobber_regs, const char *str)
{
    int reg;
    单词存储结构 *ts;
#ifdef ZHI_TARGET_X86_64
    unsigned int type;
#endif

    if (!strcmp(str, "memory") ||
        !strcmp(str, "cc") ||
	!strcmp(str, "flags"))
        return;
    ts = 单词表_查找(str, strlen(str));
    reg = ts->单词编码;
    if (reg >= 符_汇编_eax && reg <= 符_汇编_edi) {
        reg -= 符_汇编_eax;
    } else if (reg >= 符_汇编_ax && reg <= 符_汇编_di) {
        reg -= 符_汇编_ax;
#ifdef ZHI_TARGET_X86_64
    } else if (reg >= 符_汇编_rax && reg <= 符_汇编_rdi) {
        reg -= 符_汇编_rax;
    } else if ((reg = 汇编_parse_numeric_reg(reg, &type)) >= 0) {
	;
#endif
    } else {
        错误_打印("invalid clobber register '%s'", str);
    }
    clobber_regs[reg] = 1;
}
