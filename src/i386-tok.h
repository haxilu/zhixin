/* ------------------------------------------------------------------ */
/* 警告：标记的相对顺序很重要. */

/* register */
 定义_汇编(al)
 定义_汇编(cl)
 定义_汇编(dl)
 定义_汇编(bl)
 定义_汇编(ah)
 定义_汇编(ch)
 定义_汇编(dh)
 定义_汇编(bh)
 定义_汇编(ax)
 定义_汇编(cx)
 定义_汇编(dx)
 定义_汇编(bx)
 定义_汇编(sp)
 定义_汇编(bp)
 定义_汇编(si)
 定义_汇编(di)
 定义_汇编(eax)
 定义_汇编(ecx)
 定义_汇编(edx)
 定义_汇编(ebx)
 定义_汇编(esp)
 定义_汇编(ebp)
 定义_汇编(esi)
 定义_汇编(edi)
#ifdef ZHI_TARGET_X86_64
 定义_汇编(rax)
 定义_汇编(rcx)
 定义_汇编(rdx)
 定义_汇编(rbx)
 定义_汇编(rsp)
 定义_汇编(rbp)
 定义_汇编(rsi)
 定义_汇编(rdi)
#endif
 定义_汇编(mm0)
 定义_汇编(mm1)
 定义_汇编(mm2)
 定义_汇编(mm3)
 定义_汇编(mm4)
 定义_汇编(mm5)
 定义_汇编(mm6)
 定义_汇编(mm7)
 定义_汇编(xmm0)
 定义_汇编(xmm1)
 定义_汇编(xmm2)
 定义_汇编(xmm3)
 定义_汇编(xmm4)
 定义_汇编(xmm5)
 定义_汇编(xmm6)
 定义_汇编(xmm7)
 定义_汇编(cr0)
 定义_汇编(cr1)
 定义_汇编(cr2)
 定义_汇编(cr3)
 定义_汇编(cr4)
 定义_汇编(cr5)
 定义_汇编(cr6)
 定义_汇编(cr7)
 定义_汇编(tr0)
 定义_汇编(tr1)
 定义_汇编(tr2)
 定义_汇编(tr3)
 定义_汇编(tr4)
 定义_汇编(tr5)
 定义_汇编(tr6)
 定义_汇编(tr7)
 定义_汇编(db0)
 定义_汇编(db1)
 定义_汇编(db2)
 定义_汇编(db3)
 定义_汇编(db4)
 定义_汇编(db5)
 定义_汇编(db6)
 定义_汇编(db7)
 定义_汇编(dr0)
 定义_汇编(dr1)
 定义_汇编(dr2)
 定义_汇编(dr3)
 定义_汇编(dr4)
 定义_汇编(dr5)
 定义_汇编(dr6)
 定义_汇编(dr7)
 定义_汇编(es)
 定义_汇编(cs)
 定义_汇编(ss)
 定义_汇编(ds)
 定义_汇编(fs)
 定义_汇编(gs)
 定义_汇编(st)
 定义_汇编(rip)

#ifdef ZHI_TARGET_X86_64
 /* The four low parts of sp/bp/si/di that exist only on
    x86-64 (encoding aliased to ah,ch,dh,dh when not using REX). */
 定义_汇编(spl)
 定义_汇编(bpl)
 定义_汇编(sil)
 定义_汇编(dil)
#endif
 /* generic two operands */
 定义_BWLX(mov)

 定义_BWLX(add)
 定义_BWLX(or)
 定义_BWLX(adc)
 定义_BWLX(sbb)
 定义_BWLX(and)
 定义_BWLX(sub)
 定义_BWLX(xor)
 定义_BWLX(cmp)

 /* 一元 ops */
 定义_BWLX(inc)
 定义_BWLX(dec)
 定义_BWLX(not)
 定义_BWLX(neg)
 定义_BWLX(mul)
 定义_BWLX(imul)
 定义_BWLX(div)
 定义_BWLX(idiv)

 定义_BWLX(xchg)
 定义_BWLX(test)

 /* shifts */
 定义_BWLX(rol)
 定义_BWLX(ror)
 定义_BWLX(rcl)
 定义_BWLX(rcr)
 定义_BWLX(shl)
 定义_BWLX(shr)
 定义_BWLX(sar)

 定义_WLX(shld)
 定义_WLX(shrd)

 定义_汇编(pushw)
 定义_汇编(pushl)
#ifdef ZHI_TARGET_X86_64
 定义_汇编(pushq)
#endif
 定义_汇编(push)

 定义_汇编(popw)
 定义_汇编(popl)
#ifdef ZHI_TARGET_X86_64
 定义_汇编(popq)
#endif
 定义_汇编(pop)

 定义_BWL(in)
 定义_BWL(out)

 定义_WLX(movzb)
 定义_汇编(movzwl)
 定义_汇编(movsbw)
 定义_汇编(movsbl)
 定义_汇编(movswl)
#ifdef ZHI_TARGET_X86_64
 定义_汇编(movsbq)
 定义_汇编(movswq)
 定义_汇编(movzwq)
 定义_汇编(movslq)
#endif

 定义_WLX(lea)

 定义_汇编(les)
 定义_汇编(lds)
 定义_汇编(lss)
 定义_汇编(lfs)
 定义_汇编(lgs)

 定义_汇编(call)
 定义_汇编(jmp)
 定义_汇编(lcall)
 定义_汇编(ljmp)

 定义_ASMTEST(j,)

 定义_ASMTEST(set,)
 定义_ASMTEST(set,b)
 定义_ASMTEST(cmov,)

 定义_WLX(bsf)
 定义_WLX(bsr)
 定义_WLX(bt)
 定义_WLX(bts)
 定义_WLX(btr)
 定义_WLX(btc)

 定义_WLX(lar)
 定义_WLX(lsl)

 /* generic FP ops */
 定义_FP(add)
 定义_FP(mul)

 定义_汇编(fcom)
 定义_汇编(fcom_1) /* non existent op, just to have a regular table */
 定义_FP1(com)

 定义_FP(comp)
 定义_FP(sub)
 定义_FP(subr)
 定义_FP(div)
 定义_FP(divr)

 定义_BWLX(xadd)
 定义_BWLX(cmpxchg)

 /* string ops */
 定义_BWLX(cmps)
 定义_BWLX(scmp)
 定义_BWL(ins)
 定义_BWL(outs)
 定义_BWLX(lods)
 定义_BWLX(slod)
 定义_BWLX(movs)
 定义_BWLX(smov)
 定义_BWLX(scas)
 定义_BWLX(ssca)
 定义_BWLX(stos)
 定义_BWLX(ssto)

 /* generic asm ops */
#define ALT(x)
#define 定义_ASM_OP0(name, opcode) 定义_汇编(name)
#define 定义_ASM_OP0L(name, opcode, group, instr_type)
#define 定义_ASM_OP1(name, opcode, group, instr_type, op0)
#define 定义_ASM_OP2(name, opcode, group, instr_type, op0, op1)
#define 定义_ASM_OP3(name, opcode, group, instr_type, op0, op1, op2)
#ifdef ZHI_TARGET_X86_64
# include "x86_64-asm.h"
#else
# include "i386-asm.h"
#endif

#define ALT(x)
#define 定义_ASM_OP0(name, opcode)
#define 定义_ASM_OP0L(name, opcode, group, instr_type) 定义_汇编(name)
#define 定义_ASM_OP1(name, opcode, group, instr_type, op0) 定义_汇编(name)
#define 定义_ASM_OP2(name, opcode, group, instr_type, op0, op1) 定义_汇编(name)
#define 定义_ASM_OP3(name, opcode, group, instr_type, op0, op1, op2) 定义_汇编(name)
#ifdef ZHI_TARGET_X86_64
# include "x86_64-asm.h"
#else
# include "i386-asm.h"
#endif
