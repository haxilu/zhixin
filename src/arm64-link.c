#ifdef TARGET_DEFS_ONLY

#define EM_ZHI_TARGET EM_AARCH64

#define R_DATA_32  R_AARCH64_ABS32
#define R_DATA_PTR R_AARCH64_ABS64
#define R_JMP_SLOT R_AARCH64_JUMP_SLOT
#define R_GLOB_DAT R_AARCH64_GLOB_DAT
#define R_COPY     R_AARCH64_COPY
#define R_RELATIVE R_AARCH64_RELATIVE

#define R_NUM      R_AARCH64_NUM

#define ELF_START_ADDR 0x00400000
#define ELF_PAGE_SIZE 0x10000

#define PCRELATIVE_DLLPLT 1
#define RELOCATE_DLLPLT 1

#else /* !TARGET_DEFS_ONLY */

#include "zhi.h"

/* Returns 1 for a code relocation, 0 for a data relocation. For unknown
   relocations, returns -1. */
int 代码_重定位 (int reloc_type)
{
    switch (reloc_type) {
        case R_AARCH64_ABS32:
        case R_AARCH64_ABS64:
	case R_AARCH64_PREL32:
        case R_AARCH64_MOVW_UABS_G0_NC:
        case R_AARCH64_MOVW_UABS_G1_NC:
        case R_AARCH64_MOVW_UABS_G2_NC:
        case R_AARCH64_MOVW_UABS_G3:
        case R_AARCH64_ADR_PREL_PG_HI21:
        case R_AARCH64_ADD_ABS_LO12_NC:
        case R_AARCH64_ADR_GOT_PAGE:
        case R_AARCH64_LD64_GOT_LO12_NC:
        case R_AARCH64_LDST64_ABS_LO12_NC:
        case R_AARCH64_GLOB_DAT:
        case R_AARCH64_COPY:
            return 0;

        case R_AARCH64_JUMP26:
        case R_AARCH64_CALL26:
        case R_AARCH64_JUMP_SLOT:
            return 1;
    }
    return -1;
}

/* Returns an enumerator to describe whether and when the relocation needs a
   GOT and/or PLT entry to be created. See zhi.h for a description of the
   different values. */
int 获取plt_入口_类型 (int reloc_type)
{
    switch (reloc_type) {
	case R_AARCH64_PREL32:
        case R_AARCH64_MOVW_UABS_G0_NC:
        case R_AARCH64_MOVW_UABS_G1_NC:
        case R_AARCH64_MOVW_UABS_G2_NC:
        case R_AARCH64_MOVW_UABS_G3:
        case R_AARCH64_ADR_PREL_PG_HI21:
        case R_AARCH64_ADD_ABS_LO12_NC:
        case R_AARCH64_LDST64_ABS_LO12_NC:
        case R_AARCH64_GLOB_DAT:
        case R_AARCH64_JUMP_SLOT:
        case R_AARCH64_COPY:
            return NO_GOTPLT_ENTRY;

        case R_AARCH64_ABS32:
        case R_AARCH64_ABS64:
        case R_AARCH64_JUMP26:
        case R_AARCH64_CALL26:
            return AUTO_GOTPLT_ENTRY;

        case R_AARCH64_ADR_GOT_PAGE:
        case R_AARCH64_LD64_GOT_LO12_NC:
            return ALWAYS_GOTPLT_ENTRY;
    }
    return -1;
}

静态_函数 unsigned 创建_plt_入口(知心状态机 *状态机1, unsigned got_offset, struct 额外_符号_属性 *attr)
{
    段 *plt = 状态机1->plt;
    uint8_t *p;
    unsigned plt_offset;

    if (状态机1->输出_类型 == ZHI_输出_DLL)
        错误_打印("DLLs unimplemented!");

    if (plt->数据_偏移 == 0) {
        段_ptr_添加(plt, 32);
    }
    plt_offset = plt->数据_偏移;

    p = 段_ptr_添加(plt, 16);
    写32le(p, got_offset);
    写32le(p + 4, (uint64_t) got_offset >> 32);
    return plt_offset;
}

/* 重定位 the PLT: compute addresses and offsets in the PLT now that final
   address for PLT and GOT are known (see fill_program_header) */
静态_函数 void 重定位_plt(知心状态机 *状态机1)
{
    uint8_t *p, *p_end;

    if (!状态机1->plt)
      return;

    p = 状态机1->plt->data;
    p_end = p + 状态机1->plt->数据_偏移;

    if (p < p_end) {
        uint64_t plt = 状态机1->plt->sh_addr;
        uint64_t got = 状态机1->got->sh_addr;
        uint64_t off = (got >> 12) - (plt >> 12);
        if ((off + ((uint32_t)1 << 20)) >> 21)
            错误_打印("Failed relocating PLT (off=0x%lx, got=0x%lx, plt=0x%lx)", off, got, plt);
        写32le(p, 0xa9bf7bf0); // stp x16,x30,[sp,#-16]!
        写32le(p + 4, (0x90000010 | // adrp x16,...
			  (off & 0x1ffffc) << 3 | (off & 3) << 29));
        写32le(p + 8, (0xf9400211 | // ldr x17,[x16,#...]
			  (got & 0xff8) << 7));
        写32le(p + 12, (0x91000210 | // add x16,x16,#...
			   (got & 0xfff) << 10));
        写32le(p + 16, 0xd61f0220); // br x17
        写32le(p + 20, 0xd503201f); // nop
        写32le(p + 24, 0xd503201f); // nop
        写32le(p + 28, 0xd503201f); // nop
        p += 32;
        while (p < p_end) {
            uint64_t pc = plt + (p - 状态机1->plt->data);
            uint64_t addr = got + 读64le(p);
            uint64_t off = (addr >> 12) - (pc >> 12);
            if ((off + ((uint32_t)1 << 20)) >> 21)
                错误_打印("Failed relocating PLT (off=0x%lx, addr=0x%lx, pc=0x%lx)", off, addr, pc);
            写32le(p, (0x90000010 | // adrp x16,...
			  (off & 0x1ffffc) << 3 | (off & 3) << 29));
            写32le(p + 4, (0xf9400211 | // ldr x17,[x16,#...]
			      (addr & 0xff8) << 7));
            写32le(p + 8, (0x91000210 | // add x16,x16,#...
			      (addr & 0xfff) << 10));
            写32le(p + 12, 0xd61f0220); // br x17
            p += 16;
        }
    }
}

void 重定位(知心状态机 *状态机1, ElfW_Rel *rel, int type, unsigned char *ptr, 目标地址_类型 addr, 目标地址_类型 val)
{
    int sym_index = ELFW(R_SYM)(rel->r_info);
#ifdef DEBUG_RELOC
    ElfW(符号) *sym = &((ElfW(符号) *)单词表_部分->data)[sym_index];
#endif

    switch(type) {
        case R_AARCH64_ABS64:
            增加64le(ptr, val);
            return;
        case R_AARCH64_ABS32:
            增加32le(ptr, val);
            return;
	case R_AARCH64_PREL32:
	    写32le(ptr, val - addr);
	    return;
        case R_AARCH64_MOVW_UABS_G0_NC:
            写32le(ptr, ((读32le(ptr) & 0xffe0001f) |
                            (val & 0xffff) << 5));
            return;
        case R_AARCH64_MOVW_UABS_G1_NC:
            写32le(ptr, ((读32le(ptr) & 0xffe0001f) |
                            (val >> 16 & 0xffff) << 5));
            return;
        case R_AARCH64_MOVW_UABS_G2_NC:
            写32le(ptr, ((读32le(ptr) & 0xffe0001f) |
                            (val >> 32 & 0xffff) << 5));
            return;
        case R_AARCH64_MOVW_UABS_G3:
            写32le(ptr, ((读32le(ptr) & 0xffe0001f) |
                            (val >> 48 & 0xffff) << 5));
            return;
        case R_AARCH64_ADR_PREL_PG_HI21: {
            uint64_t off = (val >> 12) - (addr >> 12);
            if ((off + ((uint64_t)1 << 20)) >> 21)
                错误_打印("R_AARCH64_ADR_PREL_PG_HI21 relocation failed");
            写32le(ptr, ((读32le(ptr) & 0x9f00001f) |
                            (off & 0x1ffffc) << 3 | (off & 3) << 29));
            return;
        }
        case R_AARCH64_ADD_ABS_LO12_NC:
            写32le(ptr, ((读32le(ptr) & 0xffc003ff) |
                            (val & 0xfff) << 10));
            return;
        case R_AARCH64_LDST64_ABS_LO12_NC:
            写32le(ptr, ((读32le(ptr) & 0xffc003ff) |
                            (val & 0xff8) << 7));
            return;
        case R_AARCH64_JUMP26:
        case R_AARCH64_CALL26:
#ifdef DEBUG_RELOC
	    printf ("重定位 %d @ 0x%lx: val=0x%lx name=%s\n", type, addr, val,
		    (char *) 单词表_部分->link->data + sym->st_name);
#endif
            if (((val - addr) + ((uint64_t)1 << 27)) & ~(uint64_t)0xffffffc)
                错误_打印("R_AARCH64_(JUMP|CALL)26 relocation failed"
                          " (val=%lx, addr=%lx)", val, addr);
            写32le(ptr, (0x14000000 |
                            (uint32_t)(type == R_AARCH64_CALL26) << 31 |
                            ((val - addr) >> 2 & 0x3ffffff)));
            return;
        case R_AARCH64_ADR_GOT_PAGE: {
            uint64_t off =
                (((状态机1->got->sh_addr +
                   get_额外_符号_属性(状态机1, sym_index, 0)->got_offset) >> 12) - (addr >> 12));
            if ((off + ((uint64_t)1 << 20)) >> 21)
                错误_打印("R_AARCH64_ADR_GOT_PAGE relocation failed");
            写32le(ptr, ((读32le(ptr) & 0x9f00001f) |
                            (off & 0x1ffffc) << 3 | (off & 3) << 29));
            return;
        }
        case R_AARCH64_LD64_GOT_LO12_NC:
            写32le(ptr,
                      ((读32le(ptr) & 0xfff803ff) |
                       ((状态机1->got->sh_addr +
                         get_额外_符号_属性(状态机1, sym_index, 0)->got_offset) & 0xff8) << 7));
            return;
        case R_AARCH64_COPY:
            return;
        case R_AARCH64_GLOB_DAT:
        case R_AARCH64_JUMP_SLOT:
            /* They don't need addend */
#ifdef DEBUG_RELOC
	    printf ("重定位 %d @ 0x%lx: val=0x%lx name=%s\n", type, addr,
		    val - rel->r_addend,
		    (char *) 单词表_部分->link->data + sym->st_name);
#endif
            写64le(ptr, val - rel->r_addend);
            return;
        case R_AARCH64_RELATIVE:
#ifdef ZHI_TARGET_PE
            增加32le(ptr, val - 状态机1->pe_图像库);
#endif
            /* do nothing */
            return;
        default:
            fprintf(stderr, "FIXME: handle 重定位 type %x at %x [%p] to %x\n",
                    type, (unsigned)addr, ptr, (unsigned)val);
            return;
    }
}

#endif /* !TARGET_DEFS_ONLY */
