/*
 * 语法分析：递归下降式语法制导，单趟编译
 */
#define 全局_使用
#include "zhi.h"
/********************************************************/
/* 全局变量 */
静态_外部 int 返回符号, 匿名符号索引, 输出代码索引, 局部变量索引;
静态_外部 符号 *全局符号_堆栈;/*主要的符号堆栈：用于全局变量，函数和类型。*/
静态_外部 符号 *局部符号_堆栈;/*主要的符号堆栈：用于局部变量，函数和类型。*/
静态_外部 符号 *宏定义符号_堆栈;/*主要的符号堆栈：用于宏（#defines）。*/
静态_外部 符号 *全局符号_标签_堆栈;/*主要的符号堆栈：全局标签（用于goto）。*/
静态_外部 符号 *局部符号_标签_堆栈;

static 符号 *符号_释放_第一;
static void **符号_池;
static int 数量_符号_池;

static 符号 *清理_所有, *待定_去向;
static int 局部_范围;
static int in_大小;
static int in_通用;
static int 段_符号;

静态_外部 堆栈值 *栈顶值;
static 堆栈值 _堆栈值[1 + 堆栈值_大小];
#define 宏堆栈值 (_堆栈值 + 1)

静态_外部 int 需要_常量; /* 如果需要常量，则为true */
静态_外部 int 不需要_代码生成; /* 不需要代码生成 */
#define 未评估的子表达式 0xffff /* 未评估的子表达式 */
#define 不需要_静态数据输出 (不需要_代码生成 > 0) /* 也不需要静态数据输出 */
#define 仅_静态数据输出 (不需要_代码生成 & 0xC0000000) /* 仅静态数据输出 */

/* 自动代码抑制 ----> */
#define 代码_关() (不需要_代码生成 |= 0x20000000)
#define 代码_开() (不需要_代码生成 &= ~0x20000000)

/* 如果使用了'不需要_代码生成'标签，请清除它. */
静态_函数 void 生成符号(int t)
{
	if (t)
	{
		生成符号_地址(t, 输出代码索引);
	    代码_开();
	}
}
static int 返回代码索引(void)
{
	代码_开();
	return 输出代码索引;
}

/* 无条件跳转后设置“ 不需要_代码生成” */
static void g跳转_地址_acs(int t)
{
	生成跳转到_固定地址(t);
	代码_关();
}
static int g跳转_acs(int t)
{
	t = 生成跳转到标签(t);
	代码_关();
	return t;
}

/* 这些文件的末尾是#undef */
#define 生成跳转到_固定地址 g跳转_地址_acs
#define 生成跳转到标签 g跳转_acs
/* <---- */

静态_外部 int 全局_分配复合字符;  /* 如果必须在全局范围内分配复合文字，则为true（在初始化程序解析期间使用） */
静态_外部 C类型 当前函数_返回类型; /* 当前函数的返回类型（由返回指令使用） */
静态_外部 int 当前函数_可变参数; /* 如果当前函数是可变参数，则为true（由返回指令使用） */
静态_外部 int 函数_vc;
static int 最后_行_号, 新_文件, func_ind; /* 调试信息控制 */
静态_外部 const char *函数名称;
静态_外部 C类型 int_type, 函数_旧_类型, 字符_指针_类型;
static 动态字符串 初始字符串;

#if 指针_大小 == 4
#define VT_SIZE_T (VT_整数 | VT_无符号)
#define VT_指针DIFF_T VT_整数
#elif LONG_SIZE == 4
#define VT_SIZE_T (VT_长长整数 | VT_无符号)
#define VT_指针DIFF_T VT_长长整数
#else
#define VT_SIZE_T (VT_长整数 | VT_长长整数 | VT_无符号)
#define VT_指针DIFF_T (VT_长整数 | VT_长长整数)
#endif

静态_外部 struct 选择结构体 {
    struct 分支_t
	{
        int64_t v1, v2;
	    int sym;
    } **p;
    int n; /* 分支列表清单范围 */
    int 默认_符号; /* 默认符号 */
    int *bsym;
    struct 范围 *范围;
    struct 选择结构体 *prev;
    堆栈值 sv;
} *当前_选择; /* 当前选择 */

#define 局部变量_最多个数 8
/*当前函数堆栈中的临时局部变量列表. */
静态_外部 struct 局部_变量_范围
{
	int 位置; //堆栈上的偏移量。 值.
	short size;
	short align;
} 局部_变量_数组[局部变量_最多个数];
short 数量_局部_变量数_范围;
static struct 范围
{
    struct 范围 *prev;
    struct { int 局部变量索引, num; } vla;
    struct { 符号 *s; int n; } cl;
    int *bsym, *csym;
    符号 *lstk, *llstk;
} *当前_范围, *循环_范围, *根_范围;

/********************************************************/
/* 使调试支持 */

static const struct
{
  int type;
  const char *name;
} 默认_调试[] = {
    {   VT_整数, "int:t1=r1;-2147483648;2147483647;" },
    {   VT_字节, "char:t2=r2;0;127;" },
#if LONG_SIZE == 4
    {   VT_长整数 | VT_整数, "long int:t3=r3;-2147483648;2147483647;" },
#else
    {   VT_长长整数 | VT_长整数, "long int:t3=r3;-9223372036854775808;9223372036854775807;" },
#endif
    {   VT_整数 | VT_无符号, "unsigned int:t4=r4;0;037777777777;" },
#if LONG_SIZE == 4
    {   VT_长整数 | VT_整数 | VT_无符号, "long unsigned int:t5=r5;0;037777777777;" },
#else
    /* 使用八进制而不是-1，因此size_t有效（gcc中的-gstabs +） */
    {   VT_长长整数 | VT_长整数 | VT_无符号, "long unsigned int:t5=r5;0;01777777777777777777777;" },
#endif
    {   VT_128位整数, "__int128:t6=r6;0;-1;" },
    {   VT_128位整数 | VT_无符号, "__int128 unsigned:t7=r7;0;-1;" },
    {   VT_长长整数, "long long int:t8=r8;-9223372036854775808;9223372036854775807;" },
    {   VT_长长整数 | VT_无符号, "long long unsigned int:t9=r9;0;01777777777777777777777;" },
    {   VT_短整数, "short int:t10=r10;-32768;32767;" },
    {   VT_短整数 | VT_无符号, "short unsigned int:t11=r11;0;65535;" },
    {   VT_字节 | VT_显式符号, "signed char:t12=r12;-128;127;" },
    {   VT_字节 | VT_显式符号 | VT_无符号, "unsigned char:t13=r13;0;255;" },
    {   VT_浮点, "float:t14=r1;4;0;" },
    {   VT_双精度, "double:t15=r1;8;0;" },
    {   VT_长双精度, "long double:t16=r1;16;0;" },
    {   -1, "_Float32:t17=r1;4;0;" },
    {   -1, "_Float64:t18=r1;8;0;" },
    {   -1, "_Float128:t19=r1;16;0;" },
    {   -1, "_Float32x:t20=r1;8;0;" },
    {   -1, "_Float64x:t21=r1;16;0;" },
    {   -1, "_Decimal32:t22=r1;4;0;" },
    {   -1, "_Decimal64:t23=r1;8;0;" },
    {   -1, "_Decimal128:t24=r1;16;0;" },
    /* 如果默认字符是未签名的 */
    {   VT_字节 | VT_无符号, "unsigned char:t25=r25;0;255;" },
    {   VT_无类型, "void:t26=26" },
};

static int 调试_下个_类型;

static struct 调试_哈希
{
    int debug_type;
    符号 *type;
} *调试_哈希;

static int n_调试_哈希;

static struct 调试_信息
{
    int start;
    int end;
    int n_sym;
    struct debug_sym
    {
        int type;
        unsigned long value;
        char *str;
        段 *sec;
        int sym_index;
    } *sym;
    struct 调试_信息 *child, *next, *last, *parent;
} *调试_信息, *调试_信息_根;

/********************************************************/
#if 1
#define 优先级_解析器
static void 初始化_优先权(void);
#endif
/********************************************************/
#ifndef 配置_ZHI_汇编
静态_函数 void 汇编_指令字符串(void)
{
    错误_打印("不支持内联 asm（）");
}
静态_函数 void 汇编_全局_instr(void)
{
    错误_打印("不支持内联asm（）");
}
#endif

/* ------------------------------------------------------------------------- */
static void 通用_转换(C类型 *type);
static void 通用_转换_s(int t);
static inline C类型 *指定的_类型(C类型 *type);
static int 是_兼容_类型(C类型 *type1, C类型 *type2);
static int 解析_基本类型(C类型 *type, 属性定义 *ad);
static C类型 *类型_声明(C类型 *type, 属性定义 *ad, int *v, int td);
static void 解析_表达式_类型(C类型 *type);
static void 将值或表达式_直接存储在全局数据或本地数组中(C类型 *type, 段 *sec, unsigned long c);
static void 声明_初始化器(C类型 *type, 段 *sec, unsigned long c, int flags);
static void 块(int is_expr);
static void 声明_初始化_分配(C类型 *type, 属性定义 *ad, int r, int has_init, int v, int 范围);
static void 声明(int l);
static int 声明0(int l, int is_for_loop_init, 符号 *);
static void 等于_表达式(void);
static void vla_运行时_类型_大小(C类型 *type, int *a);
static int 是_兼容_不合格的_类型(C类型 *type1, C类型 *type2);
static inline int64_t 表达式_常量64(void);
static void 压入64位常量(int ty, unsigned long long v);
static void 压入指定类型常量(C类型 *type);
static int 生成值测试(int inv, int t);
static void 生成_内联函数(知心状态机 *s);
static void 释放_内联函数(知心状态机 *s);
static void 跳过_或_保存_块(单词字符串 **str);
static void 堆栈转为寄存器并复制到另一个寄存器(void);
static int 获取_临时_局部_变量(int size,int align);
static void 清除_临时_局部_变量_列表();
static void 投放_错误(C类型 *st, C类型 *dt);
静态_内联 int 是_浮点型(int t)
{
    int bt = t & VT_基本类型;
    return bt == VT_长双精度|| bt == VT_双精度|| bt == VT_浮点|| bt == VT_128位浮点;
}
static inline int 是_整数_型(int bt)
{
    return bt == VT_字节|| bt == VT_逻辑|| bt == VT_短整数|| bt == VT_整数|| bt == VT_长长整数;
}
static int 基本类型_大小(int bt)
{
    return bt == VT_字节 || bt == VT_逻辑 ? 1 :bt == VT_短整数 ? 2 :bt == VT_整数 ? 4 :bt == VT_长长整数 ? 8 :bt == VT_指针 ? 指针_大小 : 0;
}
/* 从类型返回函数返回寄存器 */
static int 返回_t的类型寄存器(int t)
{
    if (!是_浮点型(t))
        return 寄存器_返回16位整数寄存器;
#ifdef ZHI_TARGET_X86_64
    if ((t & VT_基本类型) == VT_长双精度)
        return TREG_ST0;
#elif defined ZHI_TARGET_RISCV64
    if ((t & VT_基本类型) == VT_长双精度)
        return 寄存器_返回16位整数寄存器;
#endif
    return 寄存器_返回浮点寄存器;
}

/* 返回第二个函数返回寄存器（如果有） */
static int 返回_t的类型寄存器2(int t)
{
    t &= VT_基本类型;
#if 指针_大小 == 4
    if (t == VT_长长整数)
        return 寄存器_返回32位整数寄存器;
#elif defined ZHI_TARGET_X86_64
    if (t == VT_128位整数)
        return 寄存器_返回32位整数寄存器;
    if (t == VT_128位浮点)
        return 寄存器_返回第二个浮点寄存器;
#elif defined ZHI_TARGET_RISCV64
    if (t == VT_长双精度)
        return 寄存器_返回32位整数寄存器;
#endif
    return VT_VC常量;
}

/* 对于两个单词的类型返回true */
#define 使用_两个单词的_类型(t) (返回_t的类型寄存器2(t) != VT_VC常量)

/* 将函数返回寄存器放入堆栈值 */
static void 将函数_返回寄存器_放入堆栈值(堆栈值 *sv, int t)
{
    sv->r = 返回_t的类型寄存器(t), sv->r2 = 返回_t的类型寄存器2(t);
}

/* 返回类型t的函数返回寄存器类 */
static int 返回类型t的函数_返回寄存器(int t)
{
    return 寄存器_类数[返回_t的类型寄存器(t)] & ~(寄存器类_浮点 | 寄存器类_整数);
}

/* 返回类型t的通用寄存器类 */
static int 返回类型t的_通用寄存器类(int t)
{
    if (!是_浮点型(t))
        return 寄存器类_整数;
#ifdef ZHI_TARGET_X86_64
    if ((t & VT_基本类型) == VT_长双精度)
        return 寄存器类_堆栈0;
    if ((t & VT_基本类型) == VT_128位浮点)
        return 寄存器类_返回浮点寄存器;
#elif defined ZHI_TARGET_RISCV64
    if ((t & VT_基本类型) == VT_长双精度)
        return 寄存器类_整数;
#endif
    return 寄存器类_浮点;
}

/* 返回对应于t和rc的第二个寄存器类 */
static int 返回类型t的2_通用寄存器类(int t, int rc)
{
    if (!使用_两个单词的_类型(t))
        return 0;
#ifdef RC_IRE2
    if (rc == 寄存器类_返回整数寄存器)
        return RC_IRE2;
#endif
#ifdef RC_FRE2
    if (rc == 寄存器类_返回浮点寄存器)
        return RC_FRE2;
#endif
    if (rc & 寄存器类_浮点)
        return 寄存器类_浮点;
    return 寄存器类_整数;
}

/* 我们使用自己的“有限”函数来避免非标准数学库的潜在问题 */
/* XXX: 依字节序排列 */
静态_函数 int ieee_有限的(double d)
{
    int p[4];
    memcpy(p, &d, sizeof(double));
    return ((unsigned)((p[1] | 0x800fffff) + 1)) >> 31;
}

/* 本机编译int long double */
#if (defined __i386__ || defined __x86_64__) \
    && (defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64)
# define ZHI_IS_NATIVE_387
#endif

静态_函数 void 测试_左值(void)
{
    if (!(栈顶值->r & VT_LVAL))
        应为("lvalue");
}

静态_函数 void 检查_堆栈值(void)
{
    if (栈顶值 != 宏堆栈值 - 1)
        错误_打印("内部编译器错误：堆栈值泄漏 (%d)",
                  (int)(栈顶值 - 宏堆栈值 + 1));
}

/* ------------------------------------------------------------------------- */
/* vstack调试辅助 */

#if 0
void 堆栈值调试辅助 (const char *lbl, int a, int b)
{
    int i;
    for (i = a; i < a + b; ++i) {
        堆栈值 *p = &栈顶值[-i];
        printf("%s 栈顶值[-%d] : type.t:%04x  r:%04x  r2:%04x  c.i:%d\n",lbl, i, p->type.t, p->r, p->r2, (int)p->c.i);
    }
}
#endif

/* ------------------------------------------------------------------------- */
/* 开始翻译单位信息 */
静态_函数 void zhi_调试_开始(知心状态机 *状态机1)
{
    if (状态机1->执行_调试)
    {
        int i;
        char buf[512];

        /* 文件信息：完整路径+文件名 */
        段_符号 = 处理_elf_符号(单词表_部分, 0, 0,ELFW(ST_INFO)(STB_LOCAL, STT_SECTION), 0,生成代码_段->sh_num, NULL);
        getcwd(buf, sizeof(buf));/*getcwd()会将当前工作目录的绝对路径复制到参数buffer所指的内存空间中*/
#ifdef _WIN32
        WIN32规范化斜杠(buf);
#endif
        连接_字符串(buf, sizeof(buf), "/");/**/
        处理_单词表_r(状态机1, buf, N_SO, 0, 0,生成代码_段->数据_偏移, 生成代码_段, 段_符号);
        处理_单词表_r(状态机1, file->prev->文件名, N_SO, 0, 0,生成代码_段->数据_偏移, 生成代码_段, 段_符号);
        for (i = 0; i < sizeof (默认_调试) / sizeof (默认_调试[0]); i++)
            处理_单词表字符串(状态机1, 默认_调试[i].name, N_LSYM, 0, 0, 0);
        新_文件 = 最后_行_号 = 0;
        func_ind = -1;
        调试_下个_类型 = sizeof(默认_调试) / sizeof(默认_调试[0]);
        调试_哈希 = NULL;
        n_调试_哈希 = 0;
        /* 我们目前正在“包含” <命令行> */
        zhi_调试_导入文件的开头(状态机1);
    }
    /* 必须放置类型为STT_FILE的elf符号，以便可以安全地使用STB_LOCAL符号 */
    处理_elf_符号(单词表_部分, 0, 0, ELFW(ST_INFO)(STB_LOCAL, STT_FILE), 0,SHN_ABS, file->文件名);
}
static void zhi_调试_stabs (知心状态机 *状态机1, const char *str, int type, unsigned long value,段 *sec, int sym_index)
{
    struct debug_sym *s;
    if (调试_信息)
    {
        调试_信息->sym =(struct debug_sym *)内存_重分配容量 (调试_信息->sym,sizeof(struct debug_sym) *(调试_信息->n_sym + 1));
        s = 调试_信息->sym + 调试_信息->n_sym++;
        s->type = type;
        s->value = value;
        s->str = 字符串_宽度加1(str);
        s->sec = sec;
        s->sym_index = sym_index;
    }
    else if (sec)
        处理_单词表_r (状态机1, str, type, 0, 0, value, sec, sym_index);
    else
        处理_单词表字符串 (状态机1, str, type, 0, 0, value);
}

static void zhi_调试_stabn(int type, int value)
{
    if (type == N_LBRAC)
    {
        struct 调试_信息 *info =(struct 调试_信息 *) 内存_初始化(sizeof (*info));
        info->start = value;
        info->parent = 调试_信息;
        if (调试_信息)
        {
            if (调试_信息->child)
            {
                if (调试_信息->child->last)
                    调试_信息->child->last->next = info;
                else
                    调试_信息->child->next = info;
                调试_信息->child->last = info;
            }
            else
                调试_信息->child = info;
        }
        else
            调试_信息_根 = info;
        调试_信息 = info;
    }
    else {
        调试_信息->end = value;
        调试_信息 = 调试_信息->parent;
    }
}
static void zhi_获取_调试_信息(知心状态机 *状态机1, 符号 *s, 动态字符串 *result)
{
    int type;
    int n = 0;
    int debug_type = -1;
    符号 *t = s;
    动态字符串 str;
    for (;;)
    {
        type = t->type.t & ~(VT_外部 | VT_静态 | VT_常量 | VT_易变);
        if ((type & VT_基本类型) != VT_字节)
            type &= ~VT_显式符号;
        if (type == VT_指针 || type == (VT_指针 | VT_数组))
            n++, t = t->type.ref;
        else
            break;
    }
    if ((type & VT_基本类型) == VT_结构体)
    {
        int i;
        t = t->type.ref;
        for (i = 0; i < n_调试_哈希; i++)
        {
            if (t == 调试_哈希[i].type)
            {
                debug_type = 调试_哈希[i].debug_type;
                break;
            }
        }
        if (debug_type == -1)
        {
            debug_type = ++调试_下个_类型;
            调试_哈希 = (struct 调试_哈希 *)内存_重分配容量 (调试_哈希,(n_调试_哈希 + 1) * sizeof(*调试_哈希));
            调试_哈希[n_调试_哈希].debug_type = debug_type;
            调试_哈希[n_调试_哈希++].type = t;
            动态字符串_初始化 (&str);
            动态字符串_打印 (&str, "%s:T%d=%c%d",(t->v & ~符号_结构体) >= 符号_第一个_匿名? "" : 取_单词字符串(t->v & ~符号_结构体, NULL),debug_type,是_共用体 (t->type.t) ? 'u' : 's',t->c);
            while (t->next)
            {
                int pos, size, align;
                t = t->next;
                动态字符串_打印 (&str, "%s:",(t->v & ~符号_字段) >= 符号_第一个_匿名? "" : 取_单词字符串(t->v & ~符号_字段, NULL));
                zhi_获取_调试_信息 (状态机1, t, &str);
                if (t->type.t & VT_位域)
                {
                    pos = t->c * 8 + BIT_POS(t->type.t);
                    size = BIT_大小(t->type.t);
                }
                else
                {
                    pos = t->c * 8;
                    size = 类型_大小(&t->type, &align) * 8;
                }
                动态字符串_打印 (&str, ",%d,%d;", pos, size);
            }
            动态字符串_打印 (&str, ";");
            zhi_调试_stabs(状态机1, str.指向字符串的指针, N_LSYM, 0, NULL, 0);
            动态字符串_释放 (&str);
        }
    }
    else if (是_枚举(type))
    {
        符号 *e = t = t->type.ref;
        debug_type = ++调试_下个_类型;
        动态字符串_初始化 (&str);
        动态字符串_打印 (&str, "%s:T%d=e",(t->v & ~符号_结构体) >= 符号_第一个_匿名? "" : 取_单词字符串(t->v & ~符号_结构体, NULL),debug_type);
        while (t->next)
        {
            t = t->next;
            动态字符串_打印 (&str, "%s:",(t->v & ~符号_字段) >= 符号_第一个_匿名? "" : 取_单词字符串(t->v & ~符号_字段, NULL));
            动态字符串_打印 (&str, e->type.t & VT_无符号 ? "%u," : "%d,",(int)t->enum_val);
        }
        动态字符串_打印 (&str, ";");
        zhi_调试_stabs(状态机1, str.指向字符串的指针, N_LSYM, 0, NULL, 0);
        动态字符串_释放 (&str);
    }
    else if ((type & VT_基本类型) != VT_函数) {
        type &= ~VT_结构体_掩码;
        for (debug_type = 1;debug_type <= sizeof(默认_调试) / sizeof(默认_调试[0]);debug_type++)
            if (默认_调试[debug_type - 1].type == type)
                break;
        if (debug_type > sizeof(默认_调试) / sizeof(默认_调试[0]))
            return;
    }
    if (n > 0)
        动态字符串_打印 (result, "%d=", ++调试_下个_类型);
    t = s;
    for (;;)
    {
        type = t->type.t & ~(VT_外部 | VT_静态 | VT_常量 | VT_易变);
        if ((type & VT_基本类型) != VT_字节)
            type &= ~VT_显式符号;
        if (type == VT_指针)
            动态字符串_打印 (result, "%d=*", ++调试_下个_类型);
        else if (type == (VT_指针 | VT_数组))
            动态字符串_打印 (result, "%d=ar1;0;%d;",++调试_下个_类型, t->type.ref->c - 1);
        else if (type == VT_函数)
        {
            动态字符串_打印 (result, "%d=f", ++调试_下个_类型);
            zhi_获取_调试_信息 (状态机1, t->type.ref, result);
            return;
        }
        else
            break;
        t = t->type.ref;
    }
    动态字符串_打印 (result, "%d", debug_type);
}
static void zhi_调试_完成 (知心状态机 *状态机1, struct 调试_信息 *cur)
{
    while (cur)
    {
        int i;
        struct 调试_信息 *next = cur->next;
        for (i = 0; i < cur->n_sym; i++) {
            struct debug_sym *s = &cur->sym[i];
            if (s->sec)
                处理_单词表_r(状态机1, s->str, s->type, 0, 0, s->value,s->sec, s->sym_index);
            else
                处理_单词表字符串(状态机1, s->str, s->type, 0, 0, s->value);
            内存_释放 (s->str);
        }
        内存_释放 (cur->sym);
        处理_单词表整数(状态机1, N_LBRAC, 0, 0, cur->start);
        zhi_调试_完成 (状态机1, cur->child);
        处理_单词表整数(状态机1, N_RBRAC, 0, 0, cur->end);
        内存_释放 (cur);
        cur = next;
    }
}
static void zhi_添加_调试_信息(知心状态机 *状态机1, int param, 符号 *s, 符号 *e)
{
    动态字符串 debug_str;
    动态字符串_初始化 (&debug_str);
    for (; s != e; s = s->prev)
    {
        if (!s->v || (s->r & VT_值掩码) != VT_LOCAL)
            continue;
        动态字符串_重置 (&debug_str);
        动态字符串_打印 (&debug_str, "%s:%s", 取_单词字符串(s->v, NULL), param ? "p" : "");
        zhi_获取_调试_信息(状态机1, s, &debug_str);
        zhi_调试_stabs(状态机1, debug_str.指向字符串的指针, param ? N_PSYM : N_LSYM, s->c, NULL, 0);
    }
    动态字符串_释放 (&debug_str);
}

static void zhi_调试_外部_符号(知心状态机 *状态机1, 符号 *sym, int sh_num, int sym_bind)
{
    段 *s = 状态机1->段数[sh_num];
    动态字符串 str;
    动态字符串_初始化 (&str);
    动态字符串_打印 (&str, "%s:%c",取_单词字符串(sym->v, NULL),sym_bind == STB_GLOBAL ? 'G' : 局部_范围 ? 'V' : 'S');
    zhi_获取_调试_信息(状态机1, sym, &str);
    if (sym_bind == STB_GLOBAL)
        zhi_调试_stabs(状态机1, str.指向字符串的指针, N_GSYM, 0, NULL, 0);
    else
        zhi_调试_stabs(状态机1, str.指向字符串的指针,(sym->type.t & VT_静态) && 初始化数据_部分 == s? N_STSYM : N_LCSYM, 0, s, sym->c);
    动态字符串_释放 (&str);
}
/* 放置翻译单位信息的末尾 */
静态_函数 void zhi_结束_调试(知心状态机 *状态机1)
{
    if (!状态机1->执行_调试)
        return;
    处理_单词表_r(状态机1, NULL, N_SO, 0, 0,生成代码_段->数据_偏移, 生成代码_段, 段_符号);
    内存_释放(调试_哈希);
}

static 缓冲文件* 用_新_文件(知心状态机 *状态机1)
{
    缓冲文件 *f = file;
    /* 如果从内联“：asm：”使用较高的文件 */
    if (f->文件名[0] == ':')
        f = f->prev;
    if (f && 新_文件)
    {
        处理_单词表_r(状态机1, f->文件名, N_SOL, 0, 0, 输出代码索引, 生成代码_段, 段_符号);
        新_文件 = 最后_行_号 = 0;
    }
    return f;
}
/* 生成行号信息 */
静态_函数 void zhi_调试_行(知心状态机 *状态机1)
{
    缓冲文件 *f;
    if (!状态机1->执行_调试|| 当前_生成代码_段 != 生成代码_段|| !(f = 用_新_文件(状态机1))|| 最后_行_号 == f->line_num)
        return;
    if (func_ind != -1)
    {
        处理_单词表整数(状态机1, N_SLINE, 0, f->line_num, 输出代码索引 - func_ind);
    } else
    {
        /* 来自zhi_assemble */
        处理_单词表_r(状态机1, NULL, N_SLINE, 0, f->line_num, 输出代码索引, 生成代码_段, 段_符号);
    }
    最后_行_号 = f->line_num;
}
/* 放置函数标识符 */
静态_函数 void zhi_调试_函数开始(知心状态机 *状态机1, 符号 *sym)
{
    动态字符串 debug_str;
    缓冲文件 *f;
    if (!状态机1->执行_调试)
        return;
    调试_信息_根 = NULL;
    调试_信息 = NULL;
    zhi_调试_stabn(N_LBRAC, 输出代码索引 - func_ind);
    if (!(f = 用_新_文件(状态机1)))
        return;
    动态字符串_初始化 (&debug_str);
    动态字符串_打印(&debug_str, "%s:%c", 函数名称, sym->type.t & VT_静态 ? 'f' : 'F');
    zhi_获取_调试_信息(状态机1, sym->type.ref, &debug_str);
    处理_单词表_r(状态机1, debug_str.指向字符串的指针, N_FUN, 0, f->line_num, 0, 当前_生成代码_段, sym->c);
    动态字符串_释放 (&debug_str);
    zhi_调试_行(状态机1);
}

/* 放置函数大小 */
静态_函数 void zhi_调试_函数结束(知心状态机 *状态机1, int size)
{
    if (!状态机1->执行_调试)
        return;
    zhi_调试_stabn(N_RBRAC, size);
    zhi_调试_完成 (状态机1, 调试_信息_根);
}
/* 放置备用文件名 */
静态_函数 void zhi_调试_备用文件(知心状态机 *状态机1, const char *文件名)
{
    if (0 == strcmp(file->文件名, 文件名))
        return;
    p字符串复制(file->文件名, sizeof(file->文件名), 文件名);
    新_文件 = 1;
}
/* #include的开头 */
静态_函数 void zhi_调试_导入文件的开头(知心状态机 *状态机1)
{
    if (!状态机1->执行_调试)
        return;
    处理_单词表字符串(状态机1, file->文件名, N_BINCL, 0, 0, 0);
    新_文件 = 1;
}
/* #include的结尾 */
静态_函数 void zhi_调试_导入文件的结尾(知心状态机 *状态机1)
{
    if (!状态机1->执行_调试)
        return;
    处理_单词表整数(状态机1, N_EINCL, 0, 0, 0);
    新_文件 = 1;
}
/* ------------------------------------------------------------------------- */
/* 初始化堆栈值和类型。 对于zhi -E也必须这样做 */
静态_函数 void zhi语法_初始化(知心状态机 *状态机1)
{
    栈顶值 = 宏堆栈值 - 1;
    memset(栈顶值, 0, sizeof *栈顶值);
    /* 定义一些常用的类型 */
    int_type.t = VT_整数;
    字符_指针_类型.t = VT_字节;
    修改类型_指针类型(&字符_指针_类型);
    函数_旧_类型.t = VT_函数;
    函数_旧_类型.ref = 符号_压入栈(符号_字段, &int_type, 0, 0);
    函数_旧_类型.ref->f.函数_调用 = 函数_CDECL;
    函数_旧_类型.ref->f.func_type = 函数_旧;
#ifdef 优先级_解析器
    初始化_优先权();
#endif
    动态字符串_初始化(&初始字符串);
}

静态_函数 int zhi语法_编译(知心状态机 *状态机1)
{
    当前_生成代码_段 = NULL;
    函数名称 = "";
    匿名符号索引 = 符号_第一个_匿名;
    段_符号 = 0;
    需要_常量 = 0;
    不需要_代码生成 = 0x80000000;
    局部_范围 = 0;

    zhi_调试_开始(状态机1);
#ifdef ZHI_TARGET_ARM
    arm_初始化(状态机1);
#endif
#ifdef 导入文件_调试
    printf("%s: **** 新建文件\n", file->文件名);
#endif
    解析_标记 = 解析_标记_预处理 | 解析_标记_标识符_数字 | 解析_标记_单词字符串;
    带有宏替换的下个标记();
    声明(VT_VC常量);
    生成_内联函数(状态机1);
    检查_堆栈值();
    /* 翻译单元信息结尾 */
    zhi_结束_调试(状态机1);
    return 0;
}
静态_函数 void zhi语法_完成(知心状态机 *状态机1)
{
    动态字符串_释放(&初始字符串);
    释放_内联函数(状态机1);
    符号_弹出(&全局符号_堆栈, NULL, 0);
    符号_弹出(&局部符号_堆栈, NULL, 0);
    /* 释放预处理器宏 */
    释放_宏定义堆栈(NULL);
    /* 释放sym_pools */
    动态数组_重分配容量(&符号_池, &数量_符号_池);
    符号_释放_第一 = NULL;
}
/* ------------------------------------------------------------------------- */
静态_函数 ELF符号 *elf符号(符号 *s)
{
  if (!s || !s->c)
    return NULL;
  return &((ELF符号 *)单词表_部分->data)[s->c];
}
/* 将存储属性应用于Elf符号 */
静态_函数 void 更新_存储(符号 *sym)
{
    ELF符号 *esym;
    int sym_bind, old_sym_bind;
    esym = elf符号(sym);
    if (!esym)
        return;
    if (sym->a.visibility)
        esym->st_other = (esym->st_other & ~ELFW(ST_VISIBILITY)(-1))| sym->a.visibility;
    if (sym->type.t & (VT_静态 | VT_内联))
        sym_bind = STB_LOCAL;
    else if (sym->a.weak)
        sym_bind = STB_WEAK;
    else
        sym_bind = STB_GLOBAL;
    old_sym_bind = ELFW(ST_BIND)(esym->st_info);
    if (sym_bind != old_sym_bind)
    {
        esym->st_info = ELFW(ST_INFO)(sym_bind, ELFW(ST_TYPE)(esym->st_info));
    }
#ifdef ZHI_TARGET_PE
    if (sym->a.dllimport)
        esym->st_other |= ST_PE_IMPORT;
    if (sym->a.dllexport)
        esym->st_other |= ST_PE_EXPORT;
#endif
#if 0
    printf("存储 %s: bind=%c vis=%d exp=%d imp=%d\n",取_单词字符串(sym->v, NULL),sym_bind == STB_WEAK ? 'w' : sym_bind == STB_LOCAL ? 'l' : 'g',sym->a.visibility,sym->a.dllexport,sym->a.dllimport);
#endif
}
/* ------------------------------------------------------------------------- */
/* 更新sym-> c，使其指向“ section”部分中的值为“ value”的外部符号 */

静态_函数 void 更新_外部_符号2(符号 *sym, int sh_num,目标地址_类型 value, unsigned long size,int can_add_underscore)
{
    int sym_type, sym_bind, info, other, t;
    ELF符号 *esym;
    const char *name;
    char buf1[256];
#ifdef 配置_ZHI_边界检查
    char buf[32];
#endif
    if (!sym->c)
    {
        name = 取_单词字符串(sym->v, NULL);
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器)
        {
            /* XXX: 避免这样做的静态？ */
            /* 如果激活了边界检查，我们将通过添加“ __bound”前缀来更改某些函数名称 */
#if defined(ZHI_TARGET_ARM) && defined(ZHI_ARM_EABI)
            if (strcmp (name, "memcpy") == 0 ||strcmp (name, "memmove") == 0 ||strcmp (name, "memset") == 0)
                goto add_bound;
#endif
            switch(sym->v)
            {
#ifdef ZHI_TARGET_PE
            /* XXX: 我们仅依赖于malloc钩子 */
            case 符_malloc:
            case 符_free:
            case 符_realloc:
            case 符_memalign:
            case 符_calloc:
#endif
            case 符_memcpy:
            case 符_memmove:
#if defined(ZHI_TARGET_ARM) && defined(ZHI_ARM_EABI)
            case 符_memmove4:
            case 符_memmove8:
#endif
            case 符_memset:
            case 符_memcmp:
            case 符_strlen:
            case 符_strcpy:
            case 符_strncpy:
            case 符_strcmp:
            case 符_strncmp:
            case 符_strcat:
            case 符_strchr:
            case 符_strdup:
#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
            case 符_alloca:
#endif
            case 符_mmap:
            case 符_munmap:
            case 符_longjmp:
#ifndef ZHI_TARGET_PE
            case 符_siglongjmp:
#endif
#if defined(ZHI_TARGET_ARM) && defined(ZHI_ARM_EABI)
add_bound:
#endif
                strcpy(buf, "__bound_");
                strcat(buf, name);
                name = buf;
                break;
            }
        }
#endif
        t = sym->type.t;
        if ((t & VT_基本类型) == VT_函数)
        {
            sym_type = STT_FUNC;
        } else if ((t & VT_基本类型) == VT_无类型)
        {
            sym_type = STT_NOTYPE;
        } else
        {
            sym_type = STT_OBJECT;
        }
        if (t & (VT_静态 | VT_内联))
            sym_bind = STB_LOCAL;
        else
            sym_bind = STB_GLOBAL;
        other = 0;
#ifdef ZHI_TARGET_PE
        if (sym_type == STT_FUNC && sym->type.ref)
        {
            符号 *ref = sym->type.ref;
            if (ref->a.nodecorate)
            {
                can_add_underscore = 0;
            }
            if (ref->f.函数_调用 == 函数_标准调用 && can_add_underscore)
            {
                sprintf(buf1, "_%s@%d", name, ref->f.func_args * 指针_大小);
                name = buf1;
                other |= ST_PE_标准调用;
                can_add_underscore = 0;
            }
        }
#endif
        if (zhi_状态->前导_下划线 && can_add_underscore)
        {
            buf1[0] = '_';
            p字符串复制(buf1 + 1, sizeof(buf1) - 1, name);
            name = buf1;
        }
        if (sym->汇编_label)
            name = 取_单词字符串(sym->汇编_label, NULL);
        info = ELFW(ST_INFO)(sym_bind, sym_type);
        sym->c = 处理_elf_符号(单词表_部分, value, size, info, other, sh_num, name);

        if (zhi_状态->执行_调试 && sym_type != STT_FUNC && sym->v < 符号_第一个_匿名)
            zhi_调试_外部_符号(zhi_状态, sym, sh_num, sym_bind);
    } else {
        esym = elf符号(sym);
        esym->st_value = value;
        esym->st_size = size;
        esym->st_shndx = sh_num;
    }
    更新_存储(sym);
}

静态_函数 void 更新_外部_符号(符号 *sym, 段 *section,目标地址_类型 value, unsigned long size)
{
    int sh_num = section ? section->sh_num : SHN_UNDEF;
    更新_外部_符号2(sym, sh_num, value, size, 1);
}
/* 在“ s”部分的符号“ sym”中添加新的重定位项*/
静态_函数 void 添加新的重定位项(段 *s, 符号 *sym, unsigned long offset, int type,目标地址_类型 addend)
{
    int c = 0;
    if (不需要_代码生成 && s == 当前_生成代码_段)
        return;
    if (sym)
    {
        if (0 == sym->c)
            更新_外部_符号(sym, NULL, 0, 0);
        c = sym->c;
    }
    /* 现在我们可以添加ELF重定位信息*/
    使_elf_重定位目标地址(单词表_部分, s, offset, type, c, addend);
}
#if 指针_大小 == 4
静态_函数 void 段部分符号重定位(段 *s, 符号 *sym, unsigned long offset, int type)
{
    添加新的重定位项(s, sym, offset, type, 0);
}
#endif
/* ------------------------------------------------------------------------- */
/* 符号分配器 */
static 符号 *__符号_分配器(void)
{
    符号 *sym_pool, *sym, *last_sym;
    int i;
    sym_pool = 内存_申请(符号_池_NB * sizeof(符号));
    动态数组_追加元素(&符号_池, &数量_符号_池, sym_pool);
    last_sym = 符号_释放_第一;
    sym = sym_pool;
    for(i = 0; i < 符号_池_NB; i++) {
        sym->next = last_sym;
        last_sym = sym;
        sym++;
    }
    符号_释放_第一 = last_sym;
    return last_sym;
}

static inline 符号 *符号_分配内存(void)
{
    符号 *sym;
#ifndef SYM_DEBUG
    sym = 符号_释放_第一;
    if (!sym)
        sym = __符号_分配器();
    符号_释放_第一 = sym->next;
    return sym;
#else
    sym = 内存_申请(sizeof(符号));
    return sym;
#endif
}

静态_内联 void 符号_释放(符号 *sym)
{
#ifndef SYM_DEBUG
    sym->next = 符号_释放_第一;
    符号_释放_第一 = sym;
#else
    内存_释放(sym);
#endif
}
/* 推送，不散列 */
静态_函数 符号 *符号_推送2(符号 **ps, int v, int t, int c)
{
    符号 *s;
    s = 符号_分配内存();
    memset(s, 0, sizeof *s);
    s->v = v;
    s->type.t = t;
    s->c = c;
    /* 加入堆栈 */
    s->prev = *ps;
    *ps = s;
    return s;
}

/* 找到一个符号并返回其关联的结构。 's'是符号堆栈的顶部*/
静态_函数 符号 *符号_查找2(符号 *s, int v)
{
    while (s)
    {
        if (s->v == v)
            return s;
        else if (s->v == -1)
            return NULL;
        s = s->prev;
    }
    return NULL;
}

/* 结构查询 */
静态_内联 符号 *结构体_查询(int v)
{
    v -= 符_识别;
    if ((unsigned)v >= (unsigned)(单词_识别号 - 符_识别))
        return NULL;
    return 单词表[v]->sym_struct;
}

/* 找到一个标识符 */
静态_内联 符号 *符号_查询(int v)
{
    v -= 符_识别;
    if ((unsigned)v >= (unsigned)(单词_识别号 - 符_识别))
        return NULL;
    return 单词表[v]->sym_identifier;
}

static int 符号_范围(符号 *s)
{
  if (是_枚举_变长 (s->type.t))
    return s->type.ref->符号_范围;
  else
    return s->符号_范围;
}
/* 将给定的符号压入符号堆栈.用于在本地符号堆栈中添加新符号。如果没有本地符号堆栈处于活动状态，则将其添加到全局符号堆栈中。 */
静态_函数 符号 *符号_压入栈(int v, C类型 *type, int r, int c)
{
    符号 *s, **ps;
    单词存储结构 *ts;
    if (局部符号_堆栈)
        ps = &局部符号_堆栈;
    else
        ps = &全局符号_堆栈;
    s = 符号_推送2(ps, v, type->t, c);
    s->type.ref = type->ref;
    s->r = r;
    /* 不要记录字段或匿名符号 */
    /* XXX: 简化 */
    if (!(v & 符号_字段) && (v & ~符号_结构体) < 符号_第一个_匿名)
    {
        /* 在标识符数组中记录符号 */
        ts = 单词表[(v & ~符号_结构体) - 符_识别];
        if (v & 符号_结构体)
            ps = &ts->sym_struct;
        else
            ps = &ts->sym_identifier;
        s->prev_tok = *ps;
        *ps = s;
        s->符号_范围 = 局部_范围;
        if (s->prev_tok && 符号_范围(s->prev_tok) == s->符号_范围)
            错误_打印("重新声明 '%s'",
                取_单词字符串(v & ~符号_结构体, NULL));
    }
    return s;
}

/* 推送一个全局标识符 */
静态_函数 符号 *推送_一个_全局标识符(int v, int t, int c)
{
    符号 *s, **ps;
    s = 符号_推送2(&全局符号_堆栈, v, t, c);
    s->r = VT_VC常量 | VT_符号;
    /* 不要记录匿名符号 */
    if (v < 符号_第一个_匿名)
    {
        ps = &单词表[v - 符_识别]->sym_identifier;
        /* 修改最上面的本地标识符，以使sym_identifier弹出时指向“ s”； 从内联asm调用时发生 */
        while (*ps != NULL && (*ps)->符号_范围)
            ps = &(*ps)->prev_tok;
        s->prev_tok = *ps;
        *ps = s;
    }
    return s;
}

/* 弹出符号，直到顶部达到“ b”。 如果KEEP不为零，则不要从列表中真正弹出它们，但要从令牌数组中删除它们。  */
静态_函数 void 符号_弹出(符号 **ptop, 符号 *b, int keep)
{
    符号 *s, *ss, **ps;
    单词存储结构 *ts;
    int v;
    s = *ptop;
    while(s != b) {
        ss = s->prev;
        v = s->v;
        /* 删除标识符数组中的符号 */
        /* XXX: 简化 */
        if (!(v & 符号_字段) && (v & ~符号_结构体) < 符号_第一个_匿名) {
            ts = 单词表[(v & ~符号_结构体) - 符_识别];
            if (v & 符号_结构体)
                ps = &ts->sym_struct;
            else
                ps = &ts->sym_identifier;
            *ps = s->prev_tok;
        }
	if (!keep)
	    符号_释放(s);
        s = ss;
    }
    if (!keep)
	*ptop = b;
}
/* ------------------------------------------------------------------------- */
static void vcheck_cmp(void)
{
    /*如果生成其他指令，则不能让cpu标志。 还要避免将VT_JMP放在堆栈顶部以外的任何位置，因为这会使代码生成器复杂化。 nocode_wanted时不要执行此操作。 vtop可能来自！nocode_wanted区域（请参见88_codeopt.c），并将其转换为寄存器而没有实际生成代码是错误的，因为它们的值可能仍用于实数。 我们在nocode_wanted下推送的所有值最终将再次弹出，因此当再次取消压缩代码时，VT_CMP / VT_JMP值将位于vtop中。 */

    if (栈顶值->r == VT_CMP && !不需要_代码生成)
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
}
/*vsetc()并vset()推动价值堆栈上的新的价值。如果先前的vtop存储在非常不安全的位置（例如，在CPU标志中），则会生成一些代码以将先前的vtop放入安全存储中。*/
static void vsetc(C类型 *type, int r, 恒定值 *vc)
{
    if (栈顶值 >= 宏堆栈值 + (堆栈值_大小 - 1))
        错误_打印("内存已满 (宏堆栈值)");
    vcheck_cmp();
    栈顶值++;
    栈顶值->type = *type;
    栈顶值->r = r;
    栈顶值->r2 = VT_VC常量;
    栈顶值->c = *vc;
    栈顶值->sym = NULL;
}
静态_函数 void vswap(void)
{
    堆栈值 tmp;
    vcheck_cmp();
    tmp = 栈顶值[0];
    栈顶值[0] = 栈顶值[-1];
    栈顶值[-1] = tmp;
}
/* 弹出堆栈值。在某些情况下，它还会生成清除代码（例如，如果在x86上使用堆叠的浮点寄存器）。 */
静态_函数 void 弹出堆栈值(void)
{
    int v;
    v = 栈顶值->r & VT_值掩码;
#if defined(ZHI_TARGET_I386) || defined(ZHI_TARGET_X86_64)
    /* 对于x86，我们需要弹出FP堆栈 */
    if (v == TREG_ST0)
    {
        o(0xd8dd); /* fstp %st(0) */
    } else
#endif
    if (v == VT_CMP)
    {
        /* 如果&&或||，则需要进行正确的跳转 未经测试 */
        生成符号(栈顶值->jtrue);
        生成符号(栈顶值->jfalse);
    }
    栈顶值--;
}
/* 推入类型“ type”的常量，该数组的值无用 */
static void 压入指定类型常量(C类型 *type)
{
    vset(type, VT_VC常量, 0);
}
/* 推送任意64位常量 */
static void 压入64位常量(int ty, unsigned long long v)
{
    恒定值 cval;
    C类型 ctype;
    ctype.t = ty;
    ctype.ref = NULL;
    cval.i = v;
    vsetc(&ctype, VT_VC常量, &cval);
}
/* 推整数常量 */
静态_函数 void 压入整数常量(int v)
{
    压入64位常量(VT_整数, v);
}
/* 推动指针大小的常量 */
static void 压入目标地址类型常量(目标地址_类型 v)
{
    压入64位常量(VT_SIZE_T, v);
}
/* 推动长久不变 */
static inline void 压入长长整数(long long v)
{
    压入64位常量(VT_长长整数, v);
}
静态_函数 void vset(C类型 *type, int r, int v)
{
    恒定值 cval;
    cval.i = v;
    vsetc(type, r, &cval);
}
static void vseti(int r, int v)
{
    C类型 type;
    type.t = VT_整数;
    type.ref = NULL;
    vset(&type, r, v);
}
静态_函数 void vpushv(堆栈值 *v)
{
    if (栈顶值 >= 宏堆栈值 + (堆栈值_大小 - 1))
        错误_打印("内存已满 (宏堆栈值)");
    栈顶值++;
    *栈顶值 = *v;
}
static void vdup(void)
{
    vpushv(栈顶值);
}
/* 将n个第一个堆栈元素旋转到底部I1 ... ...-> I2 ...在I1中[顶部右]*/
静态_函数 void vrotb(int n)
{
    int i;
    堆栈值 tmp;
    vcheck_cmp();
    tmp = 栈顶值[-n + 1];
    for(i=-n+1;i!=0;i++)
        栈顶值[i] = 栈顶值[i+1];
    栈顶值[0] = tmp;
}
/* 将条目e之前的n个元素旋转到顶部I1 ... In ...->在I1 ... I（n-1）... [顶部右]*/
静态_函数 void vrote(堆栈值 *e, int n)
{
    int i;
    堆栈值 tmp;
    vcheck_cmp();
    tmp = *e;
    for(i = 0;i < n - 1; i++)
        e[-i] = e[-i - 1];
    e[-n + 1] = tmp;
}
/* 将n个第一个堆栈元素旋转到顶部I1 ...在->在I1 ... I（n-1）中[顶部在右边]*/
静态_函数 void vrott(int n)
{
    vrote(栈顶值, n);
}
/* ------------------------------------------------------------------------- */
/* 栈顶值-> r = VT_CMP表示已通过比较或测试设置了CPU标志。 */

/* 从生成器调用以设置关系操作的结果  */
静态_函数 void vset_VT_CMP(int op)
{
    栈顶值->r = VT_CMP;
    栈顶值->cmp_op = op;
    栈顶值->jfalse = 0;
    栈顶值->jtrue = 0;
}
/* 在要求生成器将VT_CMP加载到寄存器之前被调用一次 */
static void vset_VT_JMP(void)
{
    int op = 栈顶值->cmp_op;
    if (栈顶值->jtrue || 栈顶值->jfalse)
    {
        /* 我们需要跳转到'mov $ 0，％R'或'mov $ 1，％R' */
        int inv = op & (op < 2); /* 小优化 */
        vseti(VT_JMP+inv, 生成值测试(inv, 0));
    } else {
        /* 否则将标志（rsp。0/1）转换为寄存器 */
        栈顶值->c.i = op;
        if (op < 2) /* 似乎没有发生 */
            栈顶值->r = VT_VC常量;
    }
}
/* 设置CPU标志，尚未跳转 */
static void gvtst_set(int inv, int t)
{
    int *p;

    if (栈顶值->r != VT_CMP)
    {
        压入整数常量(0);
        通用_操作(双符号_不等于);
        if (栈顶值->r != VT_CMP) /* 必须是VT_VC常量 */
            vset_VT_CMP(栈顶值->c.i != 0);
    }
    p = inv ? &栈顶值->jfalse : &栈顶值->jtrue;
    *p = g跳转_附件(*p, t);
}
/* 产生价值测试。生成任何值的测试（跳转，比较和整数） */
static int 生成值测试(int inv, int t)
{
    int op, x, u;
    gvtst_set(inv, t);
    t = 栈顶值->jtrue, u = 栈顶值->jfalse;
    if (inv)
        x = u, u = t, t = x;
    op = 栈顶值->cmp_op;
    /* 跳到想要的目标 */
    if (op > 1)
        t = g跳转_条件(op ^ inv, t);
    else if (op != inv)
        t = 生成跳转到标签(t);
    /* 解决互补跳到这里 */
    生成符号(u);
    栈顶值--;
    return t;
}
/* 生成零或非零测试 */
static void 生成_测试_零(int op)
{
    if (栈顶值->r == VT_CMP)
    {
        int j;
        if (op == 双符号_等于)
        {
            j = 栈顶值->jfalse;
            栈顶值->jfalse = 栈顶值->jtrue;
            栈顶值->jtrue = j;
            栈顶值->cmp_op ^= 1;
        }
    } else {
        压入整数常量(0);
        通用_操作(op);
    }
}
/* ------------------------------------------------------------------------- */
/* 推送TYPE的符号值 */
static inline void 压入符号值(C类型 *type, 符号 *sym)
{
    恒定值 cval;
    cval.i = 0;
    vsetc(type, VT_VC常量 | VT_符号, &cval);
    栈顶值->sym = sym;
}
/* 返回指向某节的静态符号 */
静态_函数 符号 *返回_指向节的_静态符号(C类型 *type, 段 *sec, unsigned long offset, unsigned long size)
{
    int v;
    符号 *sym;
    v = 匿名符号索引++;
    sym = 符号_压入栈(v, type, VT_VC常量 | VT_符号, 0);
    sym->type.t |= VT_静态;
    更新_外部_符号(sym, sec, offset, size);
    return sym;
}
/* 通过添加虚拟符号将引用推送到节偏移量 */
static void 将引用_推送到_节偏移量(C类型 *type, 段 *sec, unsigned long offset, unsigned long size)
{
    压入符号值(type, 返回_指向节的_静态符号(type, sec, offset, size));  
}
/* 定义对类型为“ u”的符号“ v”的新外部引用 */
静态_函数 符号 *外部_全局_符号(int v, C类型 *type)
{
    符号 *s;
    s = 符号_查询(v);
    if (!s)
    {
        /* 推动参考 */
        s = 推送_一个_全局标识符(v, type->t | VT_外部, 0);
        s->type.ref = type->ref;
    } else if (是_汇编_符号(s))
    {
        s->type.t = type->t | (s->type.t & VT_外部);
        s->type.ref = type->ref;
        更新_存储(s);
    }
    return s;
}
/* 合并符号属性。  */
static void 合并_符号属性(struct 符号属性 *sa, struct 符号属性 *sa1)
{
    if (sa1->aligned && !sa->aligned)
      sa->aligned = sa1->aligned;
    sa->packed |= sa1->packed;
    sa->weak |= sa1->weak;
    if (sa1->visibility != STV_DEFAULT)
    {
	int vis = sa->visibility;
	if (vis == STV_DEFAULT|| vis > sa1->visibility)
	  vis = sa1->visibility;
	sa->visibility = vis;
    }
    sa->dllexport |= sa1->dllexport;
    sa->nodecorate |= sa1->nodecorate;
    sa->dllimport |= sa1->dllimport;
}
/* 合并功能属性。  */
static void 合并_函数属性(struct 函数属性 *fa, struct 函数属性 *fa1)
{
    if (fa1->函数_调用 && !fa->函数_调用)
      fa->函数_调用 = fa1->函数_调用;
    if (fa1->func_type && !fa->func_type)
      fa->func_type = fa1->func_type;
    if (fa1->func_args && !fa->func_args)
      fa->func_args = fa1->func_args;
    if (fa1->func_noreturn)
      fa->func_noreturn = 1;
    if (fa1->func_ctor)
      fa->func_ctor = 1;
    if (fa1->func_dtor)
      fa->func_dtor = 1;
}
/* 合并属性。  */
static void 合并_属性(属性定义 *ad, 属性定义 *ad1)
{
    合并_符号属性(&ad->a, &ad1->a);
    合并_函数属性(&ad->f, &ad1->f);
    if (ad1->section)
      ad->section = ad1->section;
    if (ad1->alias_target)
      ad->alias_target = ad1->alias_target;
    if (ad1->汇编_label)
      ad->汇编_label = ad1->汇编_label;
    if (ad1->attr_mode)
      ad->attr_mode = ad1->attr_mode;
}
/* 合并一些类型属性。  */
static void 合并_其他类型属性(符号 *sym, C类型 *type)
{
    if (!(type->t & VT_外部) || 是_枚举_变长(sym->type.t))
    {
        if (!(sym->type.t & VT_外部))
            错误_打印("重新定义 '%s'", 取_单词字符串(sym->v, NULL));
        sym->type.t &= ~VT_外部;
    }
    if (是_汇编_符号(sym))
    {
        /* 如果两者都是静态的，则保持静态 */
        sym->type.t = type->t & (sym->type.t | ~VT_静态);
        sym->type.ref = type->ref;
    }
    if (!是_兼容_类型(&sym->type, type)) {
        错误_打印("重新定义 '%s'的类型不兼容",
                  取_单词字符串(sym->v, NULL));
    } else if ((sym->type.t & VT_基本类型) == VT_函数)
    {
        int static_proto = sym->type.t & VT_静态;
        /* 警告如果静态遵循非静态函数声明 */
        /* XXX 内联测试不应该在这里。 在我们再次实现gnu-inline模式之前，它会静默由我们的解决方法引起的有关mingw的警告。  */
        if ((type->t & VT_静态) && !static_proto && !((type->t | sym->type.t) & VT_内联))
            zhi_警告("静态存储被忽略以重新定义 '%s'",取_单词字符串(sym->v, NULL));
        /* 如果双方都同意或者一个人静态，则设置“ inline” */
        if ((type->t | sym->type.t) & VT_内联)
        {
            if (!((type->t ^ sym->type.t) & VT_内联)|| ((type->t | sym->type.t) & VT_静态))
                static_proto |= VT_内联;
        }
        if (0 == (type->t & VT_外部))
        {
            struct 函数属性 f = sym->type.ref->f;
            /* 放置完整类型，使用原型中的静态 */
            sym->type.t = (type->t & ~(VT_静态|VT_内联)) | static_proto;
            sym->type.ref = type->ref;
            合并_函数属性(&sym->type.ref->f, &f);
        } else
        {
            sym->type.t &= ~VT_内联 | static_proto;
        }

        if (sym->type.ref->f.func_type == 函数_旧 && type->ref->f.func_type != 函数_旧)
        {
            sym->type.ref = type->ref;
        }

    } else
    {
        if ((sym->type.t & VT_数组) && type->ref->c >= 0)
        {
            /* 设置数组大小（如果在外部声明中省略） */
            sym->type.ref->c = type->ref->c;
        }
        if ((type->t ^ sym->type.t) & VT_静态)
            zhi_警告("存储不匹配以重新定义 '%s'",取_单词字符串(sym->v, NULL));
    }
}
/* 合并一些存储属性.  */
static void 合并_其他存储属性(符号 *sym, 属性定义 *ad, C类型 *type)
{
    if (type)
        合并_其他类型属性(sym, type);
#ifdef ZHI_TARGET_PE
    if (sym->a.dllimport != ad->a.dllimport)
        错误_打印("不兼容的DLL链接，用于重新定义 '%s'",取_单词字符串(sym->v, NULL));
#endif
    合并_符号属性(&sym->a, &ad->a);
    if (ad->汇编_label)
        sym->汇编_label = ad->汇编_label;
    更新_存储(sym);
}
/* 将sym复制到其他堆栈 */
static 符号 *复制符号_到其他堆栈(符号 *s0, 符号 **ps)
{
    符号 *s;
    s = 符号_分配内存(), *s = *s0;
    s->prev = *ps, *ps = s;
    if (s->v < 符号_第一个_匿名)
    {
        ps = &单词表[s->v - 符_识别]->sym_identifier;
        s->prev_tok = *ps, *ps = s;
    }
    return s;
}
/* 将s-> type.ref复制到VT_FUNC和VT_指针的堆栈``ps'' */
static void 将ref_复制到_堆栈ps(符号 *s, 符号 **ps)
{
    int bt = s->type.t & VT_基本类型;
    if (bt == VT_函数 || bt == VT_指针)
    {
        符号 **sp = &s->type.ref;
        for (s = *sp, *sp = NULL; s; s = s->next)
        {
            符号 *s2 = 复制符号_到其他堆栈(s, ps);
            sp = &(*sp = s2)->next;
            将ref_复制到_堆栈ps(s2, ps);
        }
    }
}

/* 定义对符号“ v”的新外部引用*/
static 符号 *定义外部引用_符号(int v, C类型 *type, int r, 属性定义 *ad)
{
    符号 *s;
    /* 寻找全球象征 */
    s = 符号_查询(v);
    while (s && s->符号_范围)
        s = s->prev_tok;
    if (!s)
    {
        /* 推动参考 */
        s = 推送_一个_全局标识符(v, type->t, 0);
        s->r |= r;
        s->a = ad->a;
        s->汇编_label = ad->汇编_label;
        s->type.ref = type->ref;
        /* 复制类型到全局堆栈 */
        if (局部符号_堆栈)
            将ref_复制到_堆栈ps(s, &全局符号_堆栈);
    } else
    {
        合并_其他存储属性(s, ad, type);
    }
    /* 将变量推送到local_stack上（如果有） */
    if (局部符号_堆栈 && (s->type.t & VT_基本类型) != VT_函数)
        s = 复制符号_到其他堆栈(s, &局部符号_堆栈);
    return s;
}
/* 推送对全局符号v的引用 */
静态_函数 void 推送对_全局符号V_的引用(C类型 *type, int v)
{
    压入符号值(type, 外部_全局_符号(v, type));
}
/* 保存寄存器最多（栈顶值-n）个堆栈条目 */
静态_函数 void 保存_寄存器最多n个堆栈条目(int n)
{
    堆栈值 *p, *p1;
    for(p = 宏堆栈值, p1 = 栈顶值 - n; p <= p1; p++)
        将r保存到_内存堆栈(p->r);
}
/* 将r保存到内存堆栈，并将其标记为空闲 */
静态_函数 void 将r保存到_内存堆栈(int r)
{
    将r保存到_内存堆栈_最多n个条目(r, 0);
}
/* 将r保存到内存堆栈中，并将其标记为空闲（如果最多可以看到（栈顶值-n）堆栈条目） */
静态_函数 void 将r保存到_内存堆栈_最多n个条目(int r, int n)
{
    int l, size, align, bt;
    堆栈值 *p, *p1, sv;
    if ((r &= VT_值掩码) >= VT_VC常量)
        return;
    if (不需要_代码生成)
        return;
    l = 0;
    for(p = 宏堆栈值, p1 = 栈顶值 - n; p <= p1; p++)
    {
        if ((p->r & VT_值掩码) == r || p->r2 == r)
        {
            /* 如果尚未完成，必须将价值保存在堆栈中 */
            if (!l)
            {
                bt = p->type.t & VT_基本类型;
                if (bt == VT_无类型)
                    continue;
                if ((p->r & VT_LVAL) || bt == VT_函数)
                    bt = VT_指针;
                sv.type.t = bt;
                size = 类型_大小(&sv.type, &align);
                l = 获取_临时_局部_变量(size,align);
                sv.r = VT_LOCAL | VT_LVAL;
                sv.c.i = l;
                存储(p->r & VT_值掩码, &sv);
#if defined(ZHI_TARGET_I386) || defined(ZHI_TARGET_X86_64)
                /* x86特定：如果保存，则需要弹出fp寄存器ST0 */
                if (r == TREG_ST0)
                {
                    o(0xd8dd); /* fstp %st(0) */
                }
#endif
                /* special long long case */
                if (p->r2 < VT_VC常量 && 使用_两个单词的_类型(bt))
                {
                    sv.c.i += 指针_大小;
                    存储(p->r2, &sv);
                }
            }
            /* 将该堆栈条目标记为已保存在堆栈中 */
            if (p->r & VT_LVAL) {
                /* 还清除边界标记，因为函数的重定位地址存储在p-> c.i中 */
                p->r = (p->r & ~(VT_值掩码 | VT_有界的)) | VT_LLOCAL;
            } else
            {
                p->r = VT_LVAL | VT_LOCAL;
            }
            p->r2 = VT_VC常量;
            p->c.i = l;
        }
    }
}

#ifdef ZHI_TARGET_ARM
/* 在堆栈上找到最多包含一个引用的类“ rc2”的寄存器。* 如果不存在，请调用get_reg（rc） */
静态_函数 int 查找rc2寄存器_如果不存在就调用rc(int rc, int rc2)
{
    int r;
    堆栈值 *p;
    for(r=0;r<可用_寄存器数;r++)
    {
        if (寄存器_类数[r] & rc2)
        {
            int n;
            n=0;
            for(p = 宏堆栈值; p <= 栈顶值; p++)
            {
                if ((p->r & VT_值掩码) == r ||p->r2 == r)
                    n++;
            }
            if (n <= 1)
                return r;
        }
    }
    return 查找释放的rc寄存器_如果不存在就保存一个寄存器(rc);
}
#endif
/*查找免费的类“ rc”的寄存器。 如果不存在，请保存一个寄存器 */
静态_函数 int 查找释放的rc寄存器_如果不存在就保存一个寄存器(int rc)
{
    int r;
    堆栈值 *p;
    /* 寻找免费的注册 */
    for(r=0;r<可用_寄存器数;r++)
    {
        if (寄存器_类数[r] & rc)
        {
            if (不需要_代码生成)
                return r;
            for(p=宏堆栈值;p<=栈顶值;p++)
            {
                if ((p->r & VT_值掩码) == r ||p->r2 == r)
                    goto notfound;
            }
            return r;
        }
    notfound: ;
    }
    
    /* 没有剩余的寄存器：释放堆栈中的第一个寄存器（从底部开始非常重要，以确保我们不会溢出生成_opi（）中使用的寄存器） */
    for(p=宏堆栈值;p<=栈顶值;p++) {
        /* 查看第二个寄存器（如果很长） */
        r = p->r2;
        if (r < VT_VC常量 && (寄存器_类数[r] & rc))
            goto save_found;
        r = p->r & VT_值掩码;
        if (r < VT_VC常量 && (寄存器_类数[r] & rc))
        {
        save_found:
            将r保存到_内存堆栈(r);
            return r;
        }
    }
    /* 不应该来这里 */
    return -1;
}

/* 找到一个免费的临时局部变量（返回堆栈上的偏移量）以匹配大小并对齐。 如果不存在，请添加新的临时堆栈变量*/
static int 获取_临时_局部_变量(int size,int align)
{
	int i;
	struct 局部_变量_范围 *temp_var;
	int found_var;
	堆栈值 *p;
	int r;
	char free;
	char found;
	found=0;
	for(i=0;i<数量_局部_变量数_范围;i++)
	{
		temp_var=&局部_变量_数组[i];
		if(temp_var->size<size||align!=temp_var->align)
		{
			continue;
		}
		/*检查temp_var是否空闲*/
		free=1;
		for(p=宏堆栈值;p<=栈顶值;p++)
		{
			r=p->r&VT_值掩码;
			if(r==VT_LOCAL||r==VT_LLOCAL)
			{
				if(p->c.i==temp_var->位置)
				{
					free=0;
					break;
				}
			}
		}
		if(free)
		{
			found_var=temp_var->位置;
			found=1;
			break;
		}
	}
	if(!found)
	{
		局部变量索引 = (局部变量索引 - size) & -align;
		if(数量_局部_变量数_范围<局部变量_最多个数)
		{
			temp_var=&局部_变量_数组[i];
			temp_var->位置=局部变量索引;
			temp_var->size=size;
			temp_var->align=align;
			数量_局部_变量数_范围++;
		}
		found_var=局部变量索引;
	}
	return found_var;
}
static void 清除_临时_局部_变量_列表()
{
	数量_局部_变量数_范围=0;
}
/* 将寄存器“ s”（类型为“ t”）移至“ r”，并在需要时将r的先前值刷新到内存中 */
static void 移动_寄存器(int r, int s, int t)
{
    堆栈值 sv;
    if (r != s)
    {
        将r保存到_内存堆栈(r);
        sv.type.t = t;
        sv.type.ref = NULL;
        sv.r = s;
        sv.c.i = 0;
        加载(r, &sv);
    }
}
/* 获取vtop的地址（vtop必须为左值） */
静态_函数 void 获取栈顶值地址(void)
{
    栈顶值->r &= ~VT_LVAL;
    /* tricky: if saved lvalue, then we can go back to lvalue */
    if ((栈顶值->r & VT_值掩码) == VT_LLOCAL)
        栈顶值->r = (栈顶值->r & ~VT_值掩码) | VT_LOCAL | VT_LVAL;
}

#ifdef 配置_ZHI_边界检查
static void 生成左值边界代码(void)
{
    C类型 type1;

    栈顶值->r &= ~VT_强制边界检查;
    /* if lvalue, then use checking code before dereferencing */
    if (栈顶值->r & VT_LVAL) {
        /* if not VT_有界的 value, then make one */
        if (!(栈顶值->r & VT_有界的)) {
            /* must save type because we must set it to int to get pointer */
            type1 = 栈顶值->type;
            栈顶值->type.t = VT_指针;
            获取栈顶值地址();
            压入整数常量(0);
            生成_边界的_ptr_添加();
            栈顶值->r |= VT_LVAL;
            栈顶值->type = type1;
        }
        /* then check for dereferencing */
        生成_边界的_ptr_取消引用();
    }
}

/* we need to call __bound_ptr_add before we start to 加载 function
   args into registers */
静态_函数 void 生成左值边界代码_函数参数加载到寄存器(int 数量_args)
{
    int i;
    for (i = 1; i <= 数量_args; ++i)
        if (栈顶值[1 - i].r & VT_强制边界检查) {
            vrotb(i);
            生成左值边界代码();
            vrott(i);
        }
}

/* 将本地符号的范围从S添加到E（通过-> prev） */
static void 添加_局部_边界(符号 *s, 符号 *e)
{
    for (; s != e; s = s->prev) {
        if (!s->v || (s->r & VT_值掩码) != VT_LOCAL)
          continue;
        /* 添加数组/结构/联合，因为我们总是地址 */
        if ((s->type.t & VT_数组)
            || (s->type.t & VT_基本类型) == VT_结构体
            || s->a.addrtaken) {
            /* 添加局部边界信息 */
            int align, size = 类型_大小(&s->type, &align);
            目标地址_类型 *bounds_ptr = 段_ptr_添加(本地边界_部分,
                                                 2 * sizeof(目标地址_类型));
            bounds_ptr[0] = s->c;
            bounds_ptr[1] = size;
        }
    }
}
#endif

/* 围绕符号_拖动的包装，也可能会注册局部范围.  */
static void 弹出_局部_符号(符号 **ptop, 符号 *b, int keep, int ellipsis)
{
#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器 && !ellipsis && !keep)
        添加_局部_边界(*ptop, b);
#endif
    if (zhi_状态->执行_调试)
        zhi_添加_调试_信息 (zhi_状态, !局部_范围, *ptop, b);
    符号_弹出(ptop, b, keep);
}

static void incr_bf_adr(int o)
{
    栈顶值->type = 字符_指针_类型;
    获取栈顶值地址();
    压入目标地址类型常量(o);
    通用_操作('+');
    栈顶值->type.t = VT_字节 | VT_无符号;
    栈顶值->r |= VT_LVAL;
}

/* 压缩或未对齐位域的单字节加载模式 */
static void 单字节_加载_模式(C类型 *type, int bit_pos, int bit_size)
{
    int n, o, bits;
    将r保存到_内存堆栈_最多n个条目(栈顶值->r, 1);
    压入64位常量(type->t & VT_基本类型, 0); // B X
    bits = 0, o = bit_pos >> 3, bit_pos &= 7;
    do {
        vswap(); // X B
        incr_bf_adr(o);
        vdup(); // X B B
        n = 8 - bit_pos;
        if (n > bit_size)
            n = bit_size;
        if (bit_pos)
            压入整数常量(bit_pos), 通用_操作(符_SHR), bit_pos = 0; // X B Y
        if (n < 8)
            压入整数常量((1 << n) - 1), 通用_操作('&');
        通用_转换(type);
        if (bits)
            压入整数常量(bits), 通用_操作(双符号_左位移);
        vrotb(3); // B Y X
        通用_操作('|'); // B X
        bits += n, bit_size -= n, o = 1;
    } while (bit_size);
    vswap(), 弹出堆栈值();
    if (!(type->t & VT_无符号)) {
        n = ((type->t & VT_基本类型) == VT_长长整数 ? 64 : 32) - bits;
        压入整数常量(n), 通用_操作(双符号_左位移);
        压入整数常量(n), 通用_操作(双符号_右位移);
    }
}

/* 压缩或未对齐位域的单字节存储模式 */
static void 单字节_存储_模式(int bit_pos, int bit_size)
{
    int bits, n, o, m, c;

    c = (栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
    vswap(); // X B
    将r保存到_内存堆栈_最多n个条目(栈顶值->r, 1);
    bits = 0, o = bit_pos >> 3, bit_pos &= 7;
    do {
        incr_bf_adr(o); // X B
        vswap(); //B X
        c ? vdup() : 堆栈转为寄存器并复制到另一个寄存器(); // B V X
        vrott(3); // X B V
        if (bits)
            压入整数常量(bits), 通用_操作(符_SHR);
        if (bit_pos)
            压入整数常量(bit_pos), 通用_操作(双符号_左位移);
        n = 8 - bit_pos;
        if (n > bit_size)
            n = bit_size;
        if (n < 8) {
            m = ((1 << n) - 1) << bit_pos;
            压入整数常量(m), 通用_操作('&'); // X B V1
            vpushv(栈顶值-1); // X B V1 B
            压入整数常量(m & 0x80 ? ~m & 0x7f : ~m);
            通用_操作('&'); // X B V1 B1
            通用_操作('|'); // X B V2
        }
        vdup(), 栈顶值[-1] = 栈顶值[-2]; // X B B V2
        将栈顶值_存储在堆栈左值(), 弹出堆栈值(); // X B
        bits += n, bit_size -= n, bit_pos = 0, o = 1;
    } while (bit_size);
    弹出堆栈值(), 弹出堆栈值();
}

static int adjust_单字节(堆栈值 *sv, int bit_pos, int bit_size)
{
    int t;
    if (0 == sv->type.ref)
        return 0;
    t = sv->type.ref->auxtype;
    if (t != -1 && t != VT_结构体) {
        sv->type.t = (sv->type.t & ~VT_基本类型) | t;
        sv->r |= VT_LVAL;
    }
    return t;
}

/* 将属于类“ rc”的寄存器存储在vtop中。 左值被转换为值。 如果无法转换为寄存器值（例如结构），则无法使用。
 * 该gv(rc)函数生成代码以将vtop（堆栈的最高值）评估到寄存器中。rc选择应将值放置在哪个寄存器类中。
 * 将rc寄存器值存储在栈顶值中()是代码生成器最重要的功能。 */
静态_函数 int 将rc寄存器值存储在栈顶值中(int rc)
{
    int r, r2, r_ok, r2_ok, rc2, bt;
    int bit_pos, bit_size, size, align;

    /* NOTE: 查找释放的rc寄存器_如果不存在就保存一个寄存器 can modify 宏堆栈值[] */
    if (栈顶值->type.t & VT_位域) {
        C类型 type;

        bit_pos = BIT_POS(栈顶值->type.t);
        bit_size = BIT_大小(栈顶值->type.t);
        /* 删除位字段信息以避免循环 */
        栈顶值->type.t &= ~VT_结构体_掩码;

        type.ref = NULL;
        type.t = 栈顶值->type.t & VT_无符号;
        if ((栈顶值->type.t & VT_基本类型) == VT_逻辑)
            type.t |= VT_无符号;

        r = adjust_单字节(栈顶值, bit_pos, bit_size);

        if ((栈顶值->type.t & VT_基本类型) == VT_长长整数)
            type.t |= VT_长长整数;
        else
            type.t |= VT_整数;

        if (r == VT_结构体) {
            单字节_加载_模式(&type, bit_pos, bit_size);
        } else {
            int bits = (type.t & VT_基本类型) == VT_长长整数 ? 64 : 32;
            /* 转换为int以在以下操作中传播签名 */
            通用_转换(&type);
            /* 产生转变 */
            压入整数常量(bits - (bit_pos + bit_size));
            通用_操作(双符号_左位移);
            压入整数常量(bits - bit_size);
            /* 注意：如果未签名，则转换为SHR */
            通用_操作(双符号_右位移);
        }
        r = 将rc寄存器值存储在栈顶值中(rc);
    } else {
        if (是_浮点型(栈顶值->type.t) && 
            (栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
            unsigned long offset;
            /* CPU通常不能使用浮点常量，因此我们通常将它们存储在数据段中 */
            size = 类型_大小(&栈顶值->type, &align);
            if (不需要_静态数据输出)
                size = 0, align = 1;
            offset = 返回_节_对齐偏移量(初始化数据_部分, size, align);
            将引用_推送到_节偏移量(&栈顶值->type, 初始化数据_部分, offset, size);
	    vswap();
	    将值或表达式_直接存储在全局数据或本地数组中(&栈顶值->type, 初始化数据_部分, offset);
	    栈顶值->r |= VT_LVAL;
        }
#ifdef 配置_ZHI_边界检查
        if (栈顶值->r & VT_强制边界检查) 
            生成左值边界代码();
#endif

        bt = 栈顶值->type.t & VT_基本类型;

#ifdef ZHI_TARGET_RISCV64
        /* XXX 超级黑客 */
        if (bt == VT_长双精度 && rc == 寄存器类_浮点)
          rc = 寄存器类_整数;
#endif
        rc2 = 返回类型t的2_通用寄存器类(bt, rc);

        /* 在以下情况下需要重新加载：
            - 不变
            -左值（需要取消引用指针）
            -已经注册，但不在右侧 */
        r = 栈顶值->r & VT_值掩码;
        r_ok = !(栈顶值->r & VT_LVAL) && (r < VT_VC常量) && (寄存器_类数[r] & rc);
        r2_ok = !rc2 || ((栈顶值->r2 < VT_VC常量) && (寄存器_类数[栈顶值->r2] & rc2));

        if (!r_ok || !r2_ok) {
            if (!r_ok)
                r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(rc);
            if (rc2) {
                int load_type = (bt == VT_128位浮点) ? VT_双精度 : VT_指针DIFF_T;
                int original_type = 栈顶值->type.t;

                /* two register type 加载 :
                   expand to two words temporarily */
                if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
                    /* 加载 constant */
                    unsigned long long ll = 栈顶值->c.i;
                    栈顶值->c.i = ll; /* first word */
                    加载(r, 栈顶值);
                    栈顶值->r = r; /* save register value */
                    压入整数常量(ll >> 32); /* second word */
                } else if (栈顶值->r & VT_LVAL) {
                    /* We do not want to modifier the long long pointer here.
                       So we save any other instances down the stack */
                    将r保存到_内存堆栈_最多n个条目(栈顶值->r, 1);
                    /* 加载 from memory */
                    栈顶值->type.t = load_type;
                    加载(r, 栈顶值);
                    vdup();
                    栈顶值[-1].r = r; /* save register value */
                    /* increment pointer to get second word */
                    栈顶值->type.t = VT_指针DIFF_T;
                    获取栈顶值地址();
                    压入目标地址类型常量(指针_大小);
                    通用_操作('+');
                    栈顶值->r |= VT_LVAL;
                    栈顶值->type.t = load_type;
                } else {
                    /* move registers */
                    if (!r_ok)
                        加载(r, 栈顶值);
                    if (r2_ok && 栈顶值->r2 < VT_VC常量)
                        goto done;
                    vdup();
                    栈顶值[-1].r = r; /* save register value */
                    栈顶值->r = 栈顶值[-1].r2;
                }
                /* Allocate second register. Here we rely on the fact that
                   查找释放的rc寄存器_如果不存在就保存一个寄存器() tries first to free r2 of an 堆栈值. */
                r2 = 查找释放的rc寄存器_如果不存在就保存一个寄存器(rc2);
                加载(r2, 栈顶值);
                弹出堆栈值();
                /* write second register */
                栈顶值->r2 = r2;
            done:
                栈顶值->type.t = original_type;
            } else {
                if (栈顶值->r == VT_CMP)
                    vset_VT_JMP();
                /* one register type 加载 */
                加载(r, 栈顶值);
            }
        }
        栈顶值->r = r;
#ifdef ZHI_TARGET_C67
        /* uses register pairs for doubles */
        if (bt == VT_双精度)
            栈顶值->r2 = r+1;
#endif
    }
    return r;
}

/* 分别生成vtop [-1]和vtop [0]。 rc1和rc2类.对于前两名堆栈条目。 */
静态_函数 void 将rc寄存器值存储在栈顶值中2(int rc1, int rc2)
{
    /* generate more generic register first. But VT_JMP or VT_CMP
       values must be generated first in all cases to avoid possible
       reload errors */
    if (栈顶值->r != VT_CMP && rc1 <= rc2) {
        vswap();
        将rc寄存器值存储在栈顶值中(rc1);
        vswap();
        将rc寄存器值存储在栈顶值中(rc2);
        /* test if reload is needed for first register */
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
            vswap();
            将rc寄存器值存储在栈顶值中(rc1);
            vswap();
        }
    } else {
        将rc寄存器值存储在栈顶值中(rc2);
        vswap();
        将rc寄存器值存储在栈顶值中(rc1);
        vswap();
        /* test if reload is needed for first register */
        if ((栈顶值[0].r & VT_值掩码) >= VT_VC常量) {
            将rc寄存器值存储在栈顶值中(rc2);
        }
    }
}

#if 指针_大小 == 4
/* 以两个整数在堆栈上扩展64bit */
静态_函数 void 整数扩展(void)
{
    int u, v;
    u = 栈顶值->type.t & (VT_显式符号 | VT_无符号);
    v = 栈顶值->r & (VT_值掩码 | VT_LVAL);
    if (v == VT_VC常量) {
        vdup();
        栈顶值[0].c.i >>= 32;
    } else if (v == (VT_LVAL|VT_VC常量) || v == (VT_LVAL|VT_LOCAL)) {
        vdup();
        栈顶值[0].c.i += 4;
    } else {
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
        vdup();
        栈顶值[0].r = 栈顶值[-1].r2;
        栈顶值[0].r2 = 栈顶值[-1].r2 = VT_VC常量;
    }
    栈顶值[0].type.t = 栈顶值[-1].type.t = VT_整数 | u;
}
#endif

#if 指针_大小 == 4
/* build a long long from two ints */
static void 构建长长整数(int t)
{
    将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
    栈顶值[-1].r2 = 栈顶值[0].r;
    栈顶值[-1].type.t = t;
    弹出堆栈值();
}
#endif

/* 将堆栈条目转换为寄存器并将其值复制到另一个寄存器中 */
static void 堆栈转为寄存器并复制到另一个寄存器(void)
{
    int t, rc, r;

    t = 栈顶值->type.t;
#if 指针_大小 == 4
    if ((t & VT_基本类型) == VT_长长整数) {
        if (t & VT_位域) {
            将rc寄存器值存储在栈顶值中(寄存器类_整数);
            t = 栈顶值->type.t;
        }
        整数扩展();
        堆栈转为寄存器并复制到另一个寄存器();
        vswap();
        vrotb(3);
        堆栈转为寄存器并复制到另一个寄存器();
        vrotb(4);
        /* stack: H L L1 H1 */
        构建长长整数(t);
        vrotb(3);
        vrotb(3);
        vswap();
        构建长长整数(t);
        vswap();
        return;
    }
#endif
    /* duplicate value */
    rc = 返回类型t的_通用寄存器类(t);
    将rc寄存器值存储在栈顶值中(rc);
    r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(rc);
    vdup();
    加载(r, 栈顶值);
    栈顶值->r = r;
}

#if 指针_大小 == 4
/* 生成独立于CPU（无符号）的长时间操作 */
static void 生成_独立于CPU的长时间操作(int op)
{
    int t, a, b, op1, c, i;
    int func;
    unsigned short reg_iret = 寄存器_返回16位整数寄存器;
    unsigned short reg_lret = 寄存器_返回32位整数寄存器;
    堆栈值 tmp;

    switch(op) {
    case '/':
    case 符_指针除法:
        func = 符___divdi3;
        goto 生成_func;
    case 符_无符除法:
        func = 符___udivdi3;
        goto 生成_func;
    case '%':
        func = 符___moddi3;
        goto 生成_mod_func;
    case 符_无符取模运算:
        func = 符___umoddi3;
    生成_mod_func:
#ifdef ZHI_ARM_EABI
        reg_iret = TREG_R2;
        reg_lret = TREG_R3;
#endif
    生成_func:
        /* 调用泛型long long函数 */
        推送对_全局符号V_的引用(&函数_旧_类型, func);
        vrott(3);
        具体地址函数_调用(2);
        压入整数常量(0);
        栈顶值->r = reg_iret;
        栈顶值->r2 = reg_lret;
        break;
    case '^':
    case '&':
    case '|':
    case '*':
    case '+':
    case '-':
        //堆栈值调试辅助("生成_独立于CPU的长时间操作 A",0,2);
        t = 栈顶值->type.t;
        vswap();
        整数扩展();
        vrotb(3);
        整数扩展();
        /* stack: L1 H1 L2 H2 */
        tmp = 栈顶值[0];
        栈顶值[0] = 栈顶值[-3];
        栈顶值[-3] = tmp;
        tmp = 栈顶值[-2];
        栈顶值[-2] = 栈顶值[-3];
        栈顶值[-3] = tmp;
        vswap();
        /* stack: H1 H2 L1 L2 */
        //堆栈值调试辅助("生成_独立于CPU的长时间操作 B",0,4);
        if (op == '*') {
            vpushv(栈顶值 - 1);
            vpushv(栈顶值 - 1);
            通用_操作(符_无符32位乘法);
            整数扩展();
            /* stack: H1 H2 L1 L2 ML MH */
            for(i=0;i<4;i++)
                vrotb(6);
            /* stack: ML MH H1 H2 L1 L2 */
            tmp = 栈顶值[0];
            栈顶值[0] = 栈顶值[-2];
            栈顶值[-2] = tmp;
            /* stack: ML MH H1 L2 H2 L1 */
            通用_操作('*');
            vrotb(3);
            vrotb(3);
            通用_操作('*');
            /* stack: ML MH M1 M2 */
            通用_操作('+');
            通用_操作('+');
        } else if (op == '+' || op == '-') {
            /* XXX: add non carry method too (for MIPS or alpha) */
            if (op == '+')
                op1 = 符_加进位生成;
            else
                op1 = 符_减借位生成;
            通用_操作(op1);
            /* stack: H1 H2 (L1 op L2) */
            vrotb(3);
            vrotb(3);
            通用_操作(op1 + 1); /* 符_xxxC2 */
        } else {
            通用_操作(op);
            /* stack: H1 H2 (L1 op L2) */
            vrotb(3);
            vrotb(3);
            /* stack: (L1 op L2) H1 H2 */
            通用_操作(op);
            /* stack: (L1 op L2) (H1 op H2) */
        }
        /* stack: L H */
        构建长长整数(t);
        break;
    case 双符号_右位移:
    case 符_SHR:
    case 双符号_左位移:
        if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
            t = 栈顶值[-1].type.t;
            vswap();
            整数扩展();
            vrotb(3);
            /* stack: L H shift */
            c = (int)栈顶值->c.i;
            /* 常量：更简单 */
            /* 注意：所有评论均针对SHL。 其他情况通过交换单词来完成 */
            弹出堆栈值();
            if (op != 双符号_左位移)
                vswap();
            if (c >= 32) {
                /* stack: L H */
                弹出堆栈值();
                if (c > 32) {
                    压入整数常量(c - 32);
                    通用_操作(op);
                }
                if (op != 双符号_右位移) {
                    压入整数常量(0);
                } else {
                    堆栈转为寄存器并复制到另一个寄存器();
                    压入整数常量(31);
                    通用_操作(双符号_右位移);
                }
                vswap();
            } else {
                vswap();
                堆栈转为寄存器并复制到另一个寄存器();
                /* stack: H L L */
                压入整数常量(c);
                通用_操作(op);
                vswap();
                压入整数常量(32 - c);
                if (op == 双符号_左位移)
                    通用_操作(符_SHR);
                else
                    通用_操作(双符号_左位移);
                vrotb(3);
                /* stack: L L H */
                压入整数常量(c);
                if (op == 双符号_左位移)
                    通用_操作(双符号_左位移);
                else
                    通用_操作(符_SHR);
                通用_操作('|');
            }
            if (op != 双符号_左位移)
                vswap();
            构建长长整数(t);
        } else {
            /* XXX: 应该在x86上提供更快的后备功能？ */
            switch(op) {
            case 双符号_右位移:
                func = 符___ashrdi3;
                goto 生成_func;
            case 符_SHR:
                func = 符___lshrdi3;
                goto 生成_func;
            case 双符号_左位移:
                func = 符___ashldi3;
                goto 生成_func;
            }
        }
        break;
    default:
        /* 比较操作 */
        t = 栈顶值->type.t;
        vswap();
        整数扩展();
        vrotb(3);
        整数扩展();
        /* stack: L1 H1 L2 H2 */
        tmp = 栈顶值[-1];
        栈顶值[-1] = 栈顶值[-2];
        栈顶值[-2] = tmp;
        /* stack: L1 L2 H1 H2 */
        保存_寄存器最多n个堆栈条目(4);
        /* 对比高位 */
        op1 = op;
        /* 当值相等时，我们需要比较低位字。 由于跳跃是反向的，我们也将测试反向. */
        if (op1 == 符_LT)
            op1 = 双符号_小于等于;
        else if (op1 == 符_GT)
            op1 = 双符号_大于等于;
        else if (op1 == 符号_ULT)
            op1 = 符号_ULE;
        else if (op1 == 符_UGT)
            op1 = 符号_UGE;
        a = 0;
        b = 0;
        通用_操作(op1);
        if (op == 双符号_不等于) {
            b = 生成值测试(0, 0);
        } else {
            a = 生成值测试(1, 0);
            if (op != 双符号_等于) {
                /* 生成“不等于”测试 */
                压入整数常量(0);
                vset_VT_CMP(双符号_不等于);
                b = 生成值测试(0, 0);
            }
        }
        /* compare low. Always unsigned */
        op1 = op;
        if (op1 == 符_LT)
            op1 = 符号_ULT;
        else if (op1 == 双符号_小于等于)
            op1 = 符号_ULE;
        else if (op1 == 符_GT)
            op1 = 符_UGT;
        else if (op1 == 双符号_大于等于)
            op1 = 符号_UGE;
        通用_操作(op1);
#if 0//def ZHI_TARGET_I386
        if (op == 双符号_不等于) { 生成符号(b); break; }
        if (op == 双符号_等于) { 生成符号(a); break; }
#endif
        gvtst_set(1, a);
        gvtst_set(0, b);
        break;
    }
}
#endif

static uint64_t 生成_opic_sdiv(uint64_t a, uint64_t b)
{
    uint64_t x = (a >> 63 ? -a : a) / (b >> 63 ? -b : b);
    return (a ^ b) >> 63 ? -x : x;
}

static int 生成_opic_lt(uint64_t a, uint64_t b)
{
    return (a ^ (uint64_t)1 << 63) < (b ^ (uint64_t)1 << 63);
}

/* 处理整数常量优化和各种与机器无关的选项 */
static void 生成_整数常量优化和与机器无关的选项(int op)
{
    堆栈值 *v1 = 栈顶值 - 1;
    堆栈值 *v2 = 栈顶值;
    int t1 = v1->type.t & VT_基本类型;
    int t2 = v2->type.t & VT_基本类型;
    int c1 = (v1->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
    int c2 = (v2->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
    uint64_t l1 = c1 ? v1->c.i : 0;
    uint64_t l2 = c2 ? v2->c.i : 0;
    int shm = (t1 == VT_长长整数) ? 63 : 31;

    if (t1 != VT_长长整数 && (指针_大小 != 8 || t1 != VT_指针))
        l1 = ((uint32_t)l1 |
              (v1->type.t & VT_无符号 ? 0 : -(l1 & 0x80000000)));
    if (t2 != VT_长长整数 && (指针_大小 != 8 || t2 != VT_指针))
        l2 = ((uint32_t)l2 |
              (v2->type.t & VT_无符号 ? 0 : -(l2 & 0x80000000)));

    if (c1 && c2) {
        switch(op) {
        case '+': l1 += l2; break;
        case '-': l1 -= l2; break;
        case '&': l1 &= l2; break;
        case '^': l1 ^= l2; break;
        case '|': l1 |= l2; break;
        case '*': l1 *= l2; break;

        case 符_指针除法:
        case '/':
        case '%':
        case 符_无符除法:
        case 符_无符取模运算:
            /* if division by zero, generate explicit division */
            if (l2 == 0) {
                if (需要_常量 && !(不需要_代码生成 & 未评估的子表达式))
                    错误_打印("division by zero in constant");
                goto general_case;
            }
            switch(op) {
            default: l1 = 生成_opic_sdiv(l1, l2); break;
            case '%': l1 = l1 - l2 * 生成_opic_sdiv(l1, l2); break;
            case 符_无符除法: l1 = l1 / l2; break;
            case 符_无符取模运算: l1 = l1 % l2; break;
            }
            break;
        case 双符号_左位移: l1 <<= (l2 & shm); break;
        case 符_SHR: l1 >>= (l2 & shm); break;
        case 双符号_右位移:
            l1 = (l1 >> 63) ? ~(~l1 >> (l2 & shm)) : l1 >> (l2 & shm);
            break;
            /* tests */
        case 符号_ULT: l1 = l1 < l2; break;
        case 符号_UGE: l1 = l1 >= l2; break;
        case 双符号_等于: l1 = l1 == l2; break;
        case 双符号_不等于: l1 = l1 != l2; break;
        case 符号_ULE: l1 = l1 <= l2; break;
        case 符_UGT: l1 = l1 > l2; break;
        case 符_LT: l1 = 生成_opic_lt(l1, l2); break;
        case 双符号_大于等于: l1 = !生成_opic_lt(l1, l2); break;
        case 双符号_小于等于: l1 = !生成_opic_lt(l2, l1); break;
        case 符_GT: l1 = 生成_opic_lt(l2, l1); break;
            /* logical */
        case 双符号_逻辑与: l1 = l1 && l2; break;
        case 双符号_逻辑或: l1 = l1 || l2; break;
        default:
            goto general_case;
        }
	if (t1 != VT_长长整数 && (指针_大小 != 8 || t1 != VT_指针))
	    l1 = ((uint32_t)l1 |
		(v1->type.t & VT_无符号 ? 0 : -(l1 & 0x80000000)));
        v1->c.i = l1;
        栈顶值--;
    } else {
        /* if commutative ops, put c2 as constant */
        if (c1 && (op == '+' || op == '&' || op == '^' || 
                   op == '|' || op == '*' || op == 双符号_等于 || op == 双符号_不等于)) {
            vswap();
            c2 = c1; //c = c1, c1 = c2, c2 = c;
            l2 = l1; //l = l1, l1 = l2, l2 = l;
        }
        if (!需要_常量 &&
            c1 && ((l1 == 0 &&
                    (op == 双符号_左位移 || op == 符_SHR || op == 双符号_右位移)) ||
                   (l1 == -1 && op == 双符号_右位移))) {
            /* treat (0 << x), (0 >> x) and (-1 >> x) as constant */
            栈顶值--;
        } else if (!需要_常量 &&
                   c2 && ((l2 == 0 && (op == '&' || op == '*')) ||
                          (op == '|' &&
                            (l2 == -1 || (l2 == 0xFFFFFFFF && t2 != VT_长长整数))) ||
                          (l2 == 1 && (op == '%' || op == 符_无符取模运算)))) {
            /* treat (x & 0), (x * 0), (x | -1) and (x % 1) as constant */
            if (l2 == 1)
                栈顶值->c.i = 0;
            vswap();
            栈顶值--;
        } else if (c2 && (((op == '*' || op == '/' || op == 符_无符除法 ||
                          op == 符_指针除法) &&
                           l2 == 1) ||
                          ((op == '+' || op == '-' || op == '|' || op == '^' ||
                            op == 双符号_左位移 || op == 符_SHR || op == 双符号_右位移) &&
                           l2 == 0) ||
                          (op == '&' &&
                            (l2 == -1 || (l2 == 0xFFFFFFFF && t2 != VT_长长整数))))) {
            /* filter out NOP operations like x*1, x-0, x&-1... */
            栈顶值--;
        } else if (c2 && (op == '*' || op == 符_指针除法 || op == 符_无符除法)) {
            /* try to use shifts instead of muls or divs */
            if (l2 > 0 && (l2 & (l2 - 1)) == 0) {
                int n = -1;
                while (l2) {
                    l2 >>= 1;
                    n++;
                }
                栈顶值->c.i = n;
                if (op == '*')
                    op = 双符号_左位移;
                else if (op == 符_指针除法)
                    op = 双符号_右位移;
                else
                    op = 符_SHR;
            }
            goto general_case;
        } else if (c2 && (op == '+' || op == '-') &&
                   (((栈顶值[-1].r & (VT_值掩码 | VT_LVAL | VT_符号)) == (VT_VC常量 | VT_符号))
                    || (栈顶值[-1].r & (VT_值掩码 | VT_LVAL)) == VT_LOCAL)) {
            /* symbol + constant case */
            if (op == '-')
                l2 = -l2;
	    l2 += 栈顶值[-1].c.i;
	    /* The backends can't always deal with addends to symbols
	       larger than +-1<<31.  Don't construct such.  */
	    if ((int)l2 != l2)
	        goto general_case;
            栈顶值--;
            栈顶值->c.i = l2;
        } else {
        general_case:
                /* call low level op generator */
                if (t1 == VT_长长整数 || t2 == VT_长长整数 ||
                    (指针_大小 == 8 && (t1 == VT_指针 || t2 == VT_指针)))
                    生成_独立于CPU的长时间操作(op);
                else
                    生成_整数二进制运算(op);
        }
    }
}

/* 生成具有恒定传播的浮点运算 */
static void 生成_常量传播的浮点运算(int op)
{
    int c1, c2;
    堆栈值 *v1, *v2;
#if defined _MSC_VER && defined __x86_64__
    /* 避免对f1：-0.0，f2：0.0使用f1-= f2进行错误的优化 */
    volatile
#endif
    long double f1, f2;

    v1 = 栈顶值 - 1;
    v2 = 栈顶值;
    /* 目前，我们无法使用正向符号进行计算 */
    c1 = (v1->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
    c2 = (v2->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
    if (c1 && c2) {
        if (v1->type.t == VT_浮点) {
            f1 = v1->c.f;
            f2 = v2->c.f;
        } else if (v1->type.t == VT_双精度) {
            f1 = v1->c.d;
            f2 = v2->c.d;
        } else {
            f1 = v1->c.ld;
            f2 = v2->c.ld;
        }

        /* 注意：我们仅在有限数量（不是NaN或无穷大）（ANSI规范）的情况下进行常数传播*/
        if (!ieee_有限的(f1) || !ieee_有限的(f2))
            goto general_case;

        switch(op) {
        case '+': f1 += f2; break;
        case '-': f1 -= f2; break;
        case '*': f1 *= f2; break;
        case '/': 
            if (f2 == 0.0) {
		/* 如果不在初始化程序中，我们可能需要在运行时生成FP异常，否则我们想折叠。  */
                if (!需要_常量)
                    goto general_case;
            }
            f1 /= f2; 
            break;
            /* XXX: 还可以处理测试吗？ */
        default:
            goto general_case;
        }
        /* XXX: 溢出测试？ */
        if (v1->type.t == VT_浮点) {
            v1->c.f = f1;
        } else if (v1->type.t == VT_双精度) {
            v1->c.d = f1;
        } else {
            v1->c.ld = f1;
        }
        栈顶值--;
    } else {
    general_case:
        生成_浮点运算(op);
    }
}

/* 打印类型。 如果'varstr'不为NULL，则该变量也以以下类型打印 */
/* XXX: 联合体 */
/* XXX: 添加数组和函数指针 */
static void 字符串_的_类型(char *buf, int buf_size,C类型 *type, const char *varstr)
{
    int bt, v, t;
    符号 *s, *sa;
    char buf1[256];
    const char *tstr;

    t = type->t;
    bt = t & VT_基本类型;
    buf[0] = '\0';

    if (t & VT_外部)
        连接_字符串(buf, buf_size, "extern ");
    if (t & VT_静态)
        连接_字符串(buf, buf_size, "static ");
    if (t & VT_别名)
        连接_字符串(buf, buf_size, "typedef ");
    if (t & VT_内联)
        连接_字符串(buf, buf_size, "inline ");
    if (t & VT_易变)
        连接_字符串(buf, buf_size, "volatile ");
    if (t & VT_常量)
        连接_字符串(buf, buf_size, "const ");

    if (((t & VT_显式符号) && bt == VT_字节)
        || ((t & VT_无符号)
            && (bt == VT_短整数 || bt == VT_整数 || bt == VT_长长整数)
            && !是_枚举(t)
            ))
        连接_字符串(buf, buf_size, (t & VT_无符号) ? "unsigned " : "signed ");

    buf_size -= strlen(buf);
    buf += strlen(buf);

    switch(bt) {
    case VT_无类型:
        tstr = "void";
        goto add_tstr;
    case VT_逻辑:
        tstr = "_Bool";
        goto add_tstr;
    case VT_字节:
        tstr = "char";
        goto add_tstr;
    case VT_短整数:
        tstr = "short";
        goto add_tstr;
    case VT_整数:
        tstr = "int";
        goto maybe_long;
    case VT_长长整数:
        tstr = "long long";
    maybe_long:
        if (t & VT_长整数)
            tstr = "long";
        if (!是_枚举(t))
            goto add_tstr;
        tstr = "enum ";
        goto tstruct;
    case VT_浮点:
        tstr = "float";
        goto add_tstr;
    case VT_双精度:
        tstr = "double";
        if (!(t & VT_长整数))
            goto add_tstr;
    case VT_长双精度:
        tstr = "long double";
    add_tstr:
        连接_字符串(buf, buf_size, tstr);
        break;
    case VT_结构体:
        tstr = "struct ";
        if (是_共用体(t))
            tstr = "union ";
    tstruct:
        连接_字符串(buf, buf_size, tstr);
        v = type->ref->v & ~符号_结构体;
        if (v >= 符号_第一个_匿名)
            连接_字符串(buf, buf_size, "<anonymous>");
        else
            连接_字符串(buf, buf_size, 取_单词字符串(v, NULL));
        break;
    case VT_函数:
        s = type->ref;
        buf1[0]=0;
        if (varstr && '*' == *varstr) {
            连接_字符串(buf1, sizeof(buf1), "(");
            连接_字符串(buf1, sizeof(buf1), varstr);
            连接_字符串(buf1, sizeof(buf1), ")");
        }
        连接_字符串(buf1, buf_size, "(");
        sa = s->next;
        while (sa != NULL) {
            char buf2[256];
            字符串_的_类型(buf2, sizeof(buf2), &sa->type, NULL);
            连接_字符串(buf1, sizeof(buf1), buf2);
            sa = sa->next;
            if (sa)
                连接_字符串(buf1, sizeof(buf1), ", ");
        }
        if (s->f.func_type == 函数_省略)
            连接_字符串(buf1, sizeof(buf1), ", ...");
        连接_字符串(buf1, sizeof(buf1), ")");
        字符串_的_类型(buf, buf_size, &s->type, buf1);
        goto no_var;
    case VT_指针:
        s = type->ref;
        if (t & VT_数组) {
            if (varstr && '*' == *varstr)
                snprintf(buf1, sizeof(buf1), "(%s)[%d]", varstr, s->c);
            else
                snprintf(buf1, sizeof(buf1), "%s[%d]", varstr ? varstr : "", s->c);
            字符串_的_类型(buf, buf_size, &s->type, buf1);
            goto no_var;
        }
        p字符串复制(buf1, sizeof(buf1), "*");
        if (t & VT_常量)
            连接_字符串(buf1, buf_size, "const ");
        if (t & VT_易变)
            连接_字符串(buf1, buf_size, "volatile ");
        if (varstr)
            连接_字符串(buf1, sizeof(buf1), varstr);
        字符串_的_类型(buf, buf_size, &s->type, buf1);
        goto no_var;
    }
    if (varstr) {
        连接_字符串(buf, buf_size, " ");
        连接_字符串(buf, buf_size, varstr);
    }
 no_var: ;
}

static void 类型_不兼容_错误(C类型* st, C类型* dt, const char* fmt)
{
    char buf1[256], buf2[256];
    字符串_的_类型(buf1, sizeof(buf1), st, NULL);
    字符串_的_类型(buf2, sizeof(buf2), dt, NULL);
    错误_打印(fmt, buf1, buf2);
}

static void 类型_不兼容_警告(C类型* st, C类型* dt, const char* fmt)
{
    char buf1[256], buf2[256];
    字符串_的_类型(buf1, sizeof(buf1), st, NULL);
    字符串_的_类型(buf2, sizeof(buf2), dt, NULL);
    zhi_警告(fmt, buf1, buf2);
}

static int 指定类型的_大小(C类型 *type)
{
    int align;
    return 类型_大小(指定的_类型(type), &align);
}

static void vla_运行时_指向的_类型的大小(C类型 *type)
{
    int align;
    vla_运行时_类型_大小(指定的_类型(type), &align);
}

static inline int 是_空_指针(堆栈值 *p)
{
    if ((p->r & (VT_值掩码 | VT_LVAL | VT_符号)) != VT_VC常量)
        return 0;
    return ((p->type.t & VT_基本类型) == VT_整数 && (uint32_t)p->c.i == 0) ||
        ((p->type.t & VT_基本类型) == VT_长长整数 && p->c.i == 0) ||
        ((p->type.t & VT_基本类型) == VT_指针 &&
         (指针_大小 == 4 ? (uint32_t)p->c.i == 0 : p->c.i == 0) &&
         ((指定的_类型(&p->type)->t & VT_基本类型) == VT_无类型) &&
         0 == (指定的_类型(&p->type)->t & (VT_常量 | VT_易变))
         );
}

/* 比较函数类型。 旧功能与任何新功能匹配 */
static int 是_兼容的_函数(C类型 *type1, C类型 *type2)
{
    符号 *状态机1, *s2;

    状态机1 = type1->ref;
    s2 = type2->ref;
    if (状态机1->f.函数_调用 != s2->f.函数_调用)
        return 0;
    if (状态机1->f.func_type != s2->f.func_type
        && 状态机1->f.func_type != 函数_旧
        && s2->f.func_type != 函数_旧)
        return 0;
    /* 我们也应该检查函数的返回类型是否为函数_OLD，但这会导致内部使用的支持函数（例如符_memmove）出现问题 */
    if (状态机1->f.func_type == 函数_旧 && !状态机1->next)
        return 1;
    if (s2->f.func_type == 函数_旧 && !s2->next)
        return 1;
    for (;;) {
        if (!是_兼容_不合格的_类型(&状态机1->type, &s2->type))
            return 0;
        状态机1 = 状态机1->next;
        s2 = s2->next;
        if (!状态机1)
            return !s2;
        if (!s2)
            return 0;
    }
}

/* 如果type1和type2相同，则返回true。 如果unqualified为true，则忽略类型上的限定符。
 */
static int 比较_2个类型(C类型 *type1, C类型 *type2, int unqualified)
{
    int bt1, t1, t2;

    t1 = type1->t & VT_类型;
    t2 = type2->t & VT_类型;
    if (unqualified) {
        /* 比较前去除预选赛 */
        t1 &= ~(VT_常量 | VT_易变);
        t2 &= ~(VT_常量 | VT_易变);
    }

    /* 默认Vs显式签名仅对char很重要 */
    if ((t1 & VT_基本类型) != VT_字节) {
        t1 &= ~VT_显式符号;
        t2 &= ~VT_显式符号;
    }
    /* XXX: 位域 ? */
    if (t1 != t2)
        return 0;

    if ((t1 & VT_数组)
        && !(type1->ref->c < 0
          || type2->ref->c < 0
          || type1->ref->c == type2->ref->c))
            return 0;

    /* 测试更复杂的案例 */
    bt1 = t1 & VT_基本类型;
    if (bt1 == VT_指针) {
        type1 = 指定的_类型(type1);
        type2 = 指定的_类型(type2);
        return 是_兼容_类型(type1, type2);
    } else if (bt1 == VT_结构体) {
        return (type1->ref == type2->ref);
    } else if (bt1 == VT_函数) {
        return 是_兼容的_函数(type1, type2);
    } else if (是_枚举(type1->t) && 是_枚举(type2->t)) {
        /* 如果两个都是枚举，则它们必须相同；如果只有一个，则t1和t2必须相等，这已在上面进行了检查。  */
        return type1->ref == type2->ref;
    } else {
        return 1;
    }
}

/*检查OP1和OP2是否可以与操作OP“组合”，如果非null（指针正负除外），则组合类型存储在DEST中。 */
static int 结合_类型(C类型 *dest, 堆栈值 *op1, 堆栈值 *op2, int op)
{
    C类型 *type1 = &op1->type, *type2 = &op2->type, type;
    int t1 = type1->t, t2 = type2->t, bt1 = t1 & VT_基本类型, bt2 = t2 & VT_基本类型;
    int ret = 1;

    type.t = VT_无类型;
    type.ref = NULL;

    if (bt1 == VT_无类型 || bt2 == VT_无类型) {
        ret = op == '?' ? 1 : 0;
        /* 注意：作为扩展，我们仅接受一侧的空隙 */
        type.t = VT_无类型;
    } else if (bt1 == VT_指针 || bt2 == VT_指针) {
        if (op == '+') ; /* 处理来电者 */
        /* http://port70.net/~nsz/c/c99/n1256.html#6.5.15p6 */
        /* 如果一个为空的ptr常量，则结果类型为另一个。  */
        else if (是_空_指针 (op2)) type = *type1;
        else if (是_空_指针 (op1)) type = *type2;
        else if (bt1 != bt2) {
            /* 接受带有警告的指针和整数之间的比较或cond-expr */
            if ((op == '?' || 符_ISCOND(op))
                && (是_整数_型(bt1) || 是_整数_型(bt2)))
              zhi_警告(" %s 指针/整数不匹配", op == '?' ? "条件表达式" : "比较");
            else if (op != '-' || !是_整数_型(bt2))
              ret = 0;
            type = *(bt1 == VT_指针 ? type1 : type2);
        } else {
            C类型 *pt1 = 指定的_类型(type1);
            C类型 *pt2 = 指定的_类型(type2);
            int pbt1 = pt1->t & VT_基本类型;
            int pbt2 = pt2->t & VT_基本类型;
            int newquals, copied = 0;
            if (pbt1 != VT_无类型 && pbt2 != VT_无类型
                && !比较_2个类型(pt1, pt2, 1/*不合格*/)) {
                if (op != '?' && !符_ISCOND(op))
                  ret = 0;
                else
                  类型_不兼容_警告(type1, type2, op == '?' ? "条件表达式 ('%s' and '%s') 中的指针类型不匹配." : "比较中的指针类型不匹配('%s' and '%s')");
            }
            if (op == '?') {
                /* 首选指向void的指针，否则指向类型的指针减去限定符应兼容 */
                type = *((pbt1 == VT_无类型) ? type1 : type2);
                /* 结合资格 */
                newquals = ((pt1->t | pt2->t) & (VT_常量 | VT_易变));
                if ((~指定的_类型(&type)->t & (VT_常量 | VT_易变))
                    & newquals)
                  {
                    /* 复制指针目标符号 */
                    type.ref = 符号_压入栈(符号_字段, &type.ref->type,
                                        0, type.ref->c);
                    copied = 1;
                    指定的_类型(&type)->t |= newquals;
                  }
                /* 如果可能，指向不完整数组的指针将转换为指向已完成数组的指针*/
                if (pt1->t & VT_数组
                    && pt2->t & VT_数组
                    && 指定的_类型(&type)->ref->c < 0
                    && (pt1->ref->c > 0 || pt2->ref->c > 0))
                  {
                    if (!copied)
                      type.ref = 符号_压入栈(符号_字段, &type.ref->type,
                                          0, type.ref->c);
                    指定的_类型(&type)->ref =
                        符号_压入栈(符号_字段, &指定的_类型(&type)->ref->type,
                                 0, 指定的_类型(&type)->ref->c);
                    指定的_类型(&type)->ref->c =
                        0 < pt1->ref->c ? pt1->ref->c : pt2->ref->c;
                  }
            }
        }
        if (符_ISCOND(op))
          type.t = VT_SIZE_T;
    } else if (bt1 == VT_结构体 || bt2 == VT_结构体) {
        if (op != '?' || !比较_2个类型(type1, type2, 1))
          ret = 0;
        type = *type1;
    } else if (是_浮点型(bt1) || 是_浮点型(bt2)) {
        if (bt1 == VT_长双精度 || bt2 == VT_长双精度) {
            type.t = VT_长双精度;
        } else if (bt1 == VT_双精度 || bt2 == VT_双精度) {
            type.t = VT_双精度;
        } else {
            type.t = VT_浮点;
        }
    } else if (bt1 == VT_长长整数 || bt2 == VT_长长整数) {
        /* 投给最大的行动 */
        type.t = VT_长长整数 | VT_长整数;
        if (bt1 == VT_长长整数)
          type.t &= t1;
        if (bt2 == VT_长长整数)
          type.t &= t2;
        /* 如果长时间不适合，请转换为unsigned */
        if ((t1 & (VT_基本类型 | VT_无符号 | VT_位域)) == (VT_长长整数 | VT_无符号) ||
            (t2 & (VT_基本类型 | VT_无符号 | VT_位域)) == (VT_长长整数 | VT_无符号))
          type.t |= VT_无符号;
    } else {
        /* 整数运算 */
        type.t = VT_整数 | (VT_长整数 & (t1 | t2));
        /* 如果它不适合整数，则转换为无符号 */
        if ((t1 & (VT_基本类型 | VT_无符号 | VT_位域)) == (VT_整数 | VT_无符号) ||
            (t2 & (VT_基本类型 | VT_无符号 | VT_位域)) == (VT_整数 | VT_无符号))
          type.t |= VT_无符号;
    }
    if (dest)
      *dest = type;
    return ret;
}

/* 通用生成_op：处理类型问题 */
静态_函数 void 通用_操作(int op)
{
    int u, t1, t2, bt1, bt2, t;
    C类型 type1, combtype;

redo:
    t1 = 栈顶值[-1].type.t;
    t2 = 栈顶值[0].type.t;
    bt1 = t1 & VT_基本类型;
    bt2 = t2 & VT_基本类型;
        
    if (bt1 == VT_函数 || bt2 == VT_函数) {
	if (bt2 == VT_函数) {
	    修改类型_指针类型(&栈顶值->type);
	    获取栈顶值地址();
	}
	if (bt1 == VT_函数) {
	    vswap();
	    修改类型_指针类型(&栈顶值->type);
	    获取栈顶值地址();
	    vswap();
	}
	goto redo;
    } else if (!结合_类型(&combtype, 栈顶值 - 1, 栈顶值, op)) {
        zhi_错误_不中止("无效的二进制操作数类型");
        弹出堆栈值();
    } else if (bt1 == VT_指针 || bt2 == VT_指针) {
        /* 至少一个操作数是一个指针 */
        /*关系操作：必须同时是两个指针 */
        if (符_ISCOND(op))
            goto std_op;
        /* 如果两个指针都必须是'-'op */
        if (bt1 == VT_指针 && bt2 == VT_指针) {
            if (op != '-')
                错误_打印("不能在这里使用指针");
            if (栈顶值[-1].type.t & VT_变长数组) {
                vla_运行时_指向的_类型的大小(&栈顶值[-1].type);
            } else {
                压入整数常量(指定类型的_大小(&栈顶值[-1].type));
            }
            vrott(3);
            生成_整数常量优化和与机器无关的选项(op);
            栈顶值->type.t = VT_指针DIFF_T;
            vswap();
            通用_操作(符_指针除法);
        } else {
            /* 正好一个指针：必须为'+'或'-'。 */
            if (op != '-' && op != '+')
                错误_打印("cannot use pointers here");
            /* 将指针作为第一个操作数 */
            if (bt2 == VT_指针) {
                vswap();
                t = t1, t1 = t2, t2 = t;
            }
#if 指针_大小 == 4
            if ((栈顶值[0].type.t & VT_基本类型) == VT_长长整数)
                /* XXX: 在这里截断，因为生成_独立于CPU的长期操作无法处理ptr + long long */
                通用_转换_s(VT_整数);
#endif
            type1 = 栈顶值[-1].type;
            if (栈顶值[-1].type.t & VT_变长数组)
                vla_运行时_指向的_类型的大小(&栈顶值[-1].type);
            else {
                u = 指定类型的_大小(&栈顶值[-1].type);
                if (u < 0)
                    错误_打印("未知的数组元素大小");
#if 指针_大小 == 8
                压入长长整数(u);
#else
                /* XXX: 转换为int吗？ （长大案）*/
                压入整数常量(u);
#endif
            }
            通用_操作('*');
#ifdef 配置_ZHI_边界检查
            if (zhi_状态->执行_边界_检查器 && !需要_常量) {
                /* 如果是有界指针，我们会生成一个特殊的代码来测试界 */
                if (op == '-') {
                    压入整数常量(0);
                    vswap();
                    通用_操作('-');
                }
                生成_边界的_ptr_添加();
            } else
#endif
            {
                生成_整数常量优化和与机器无关的选项(op);
            }
            type1.t &= ~VT_数组;
            /* 如果 生成_整数常量优化和与机器无关的选项（）交换操作数，则再次输入 */
            栈顶值->type = type1;
        }
    } else {
        /* 浮点数只能用于一些操作 */
        if (是_浮点型(combtype.t)
            && op != '+' && op != '-' && op != '*' && op != '/'
            && !符_ISCOND(op))
            错误_打印("无效的二进制操作数");
        else if (op == 符_SHR || op == 双符号_右位移 || op == 双符号_左位移) {
            t = bt1 == VT_长长整数 ? VT_长长整数 : VT_整数;
            if ((t1 & (VT_基本类型 | VT_无符号 | VT_位域)) == (t | VT_无符号))
              t |= VT_无符号;
            t |= (VT_长整数 & t1);
            combtype.t = t;
        }
    std_op:
        t = t2 = combtype.t;
        /* XXX: 当前，一些未签名的操作是显式的，因此我们在此处对其进行修改 */
        if (t & VT_无符号) {
            if (op == 双符号_右位移)
                op = 符_SHR;
            else if (op == '/')
                op = 符_无符除法;
            else if (op == '%')
                op = 符_无符取模运算;
            else if (op == 符_LT)
                op = 符号_ULT;
            else if (op == 符_GT)
                op = 符_UGT;
            else if (op == 双符号_小于等于)
                op = 符号_ULE;
            else if (op == 双符号_大于等于)
                op = 符号_UGE;
        }
        vswap();
        通用_转换_s(t);
        vswap();
        /* 移位和long long的特殊情况：我们将移位保留为整数 */
        if (op == 符_SHR || op == 双符号_右位移 || op == 双符号_左位移)
            t2 = VT_整数;
        通用_转换_s(t2);
        if (是_浮点型(t))
            生成_常量传播的浮点运算(op);
        else
            生成_整数常量优化和与机器无关的选项(op);
        if (符_ISCOND(op)) {
            /* 关系运算：结果为整数 */
            栈顶值->type.t = VT_整数;
        } else {
            栈顶值->type.t = t;
        }
    }
    // 确保我们已转换为右值:
    if (栈顶值->r & VT_LVAL)
        将rc寄存器值存储在栈顶值中(是_浮点型(栈顶值->type.t & VT_基本类型) ? 寄存器类_浮点 : 寄存器类_整数);
}

#if defined ZHI_TARGET_ARM64 || defined ZHI_TARGET_RISCV64 || defined ZHI_TARGET_ARM
#define 生成_cvt_itof1 生成_整数转换为浮点
#else
/* 泛型为无符号 long long case */
static void 生成_cvt_itof1(int t)
{
    if ((栈顶值->type.t & (VT_基本类型 | VT_无符号)) == 
        (VT_长长整数 | VT_无符号)) {

        if (t == VT_浮点)
            推送对_全局符号V_的引用(&函数_旧_类型, 符___floatundisf);
#if 长双精度_大小 != 8
        else if (t == VT_长双精度)
            推送对_全局符号V_的引用(&函数_旧_类型, 符___floatundixf);
#endif
        else
            推送对_全局符号V_的引用(&函数_旧_类型, 符___floatundidf);
        vrott(2);
        具体地址函数_调用(1);
        压入整数常量(0);
        将函数_返回寄存器_放入堆栈值(栈顶值, t);
    } else {
        生成_整数转换为浮点(t);
    }
}
#endif

#if defined ZHI_TARGET_ARM64 || defined ZHI_TARGET_RISCV64
#define 生成_cvt_ftoi1 生成_浮点转换为整数
#else
/* generic ftoi for unsigned long long case */
static void 生成_cvt_ftoi1(int t)
{
    int st;
    if (t == (VT_长长整数 | VT_无符号)) {
        /* 没有本地处理 */
        st = 栈顶值->type.t & VT_基本类型;
        if (st == VT_浮点)
            推送对_全局符号V_的引用(&函数_旧_类型, 符___fixunssfdi);
#if 长双精度_大小 != 8
        else if (st == VT_长双精度)
            推送对_全局符号V_的引用(&函数_旧_类型, 符___fixunsxfdi);
#endif
        else
            推送对_全局符号V_的引用(&函数_旧_类型, 符___fixunsdfdi);
        vrott(2);
        具体地址函数_调用(1);
        压入整数常量(0);
        将函数_返回寄存器_放入堆栈值(栈顶值, t);
    } else {
        生成_浮点转换为整数(t);
    }
}
#endif

/* char / short的特殊延迟演员表 */
static void 强制_字符型短整型_转换(void)
{
    int sbt = BFGET(栈顶值->r, VT_强制转换) == 2 ? VT_长长整数 : VT_整数;
    int dbt = 栈顶值->type.t;
    栈顶值->r &= ~VT_强制转换;
    栈顶值->type.t = sbt;
    通用_转换_s(dbt == VT_逻辑 ? VT_字节|VT_无符号 : dbt);
    栈顶值->type.t = dbt;
}

static void 通用_转换_s(int t)
{
    C类型 type;
    type.t = t;
    type.ref = NULL;
    通用_转换(&type);
}

/* 将“栈顶值”转换为“类型”。 禁止转换到位域。 */
static void 通用_转换(C类型 *type)
{
    int sbt, dbt, sf, df, c;
    int dbt_bt, sbt_bt, ds, ss, bits, trunc;

    /* special delayed cast for char/short */
    if (栈顶值->r & VT_强制转换)
        强制_字符型短整型_转换();

    /* bitfields first get cast to ints */
    if (栈顶值->type.t & VT_位域)
        将rc寄存器值存储在栈顶值中(寄存器类_整数);

    dbt = type->t & (VT_基本类型 | VT_无符号);
    sbt = 栈顶值->type.t & (VT_基本类型 | VT_无符号);
    if (sbt == VT_函数)
        sbt = VT_指针;

again:
    if (sbt != dbt) {
        sf = 是_浮点型(sbt);
        df = 是_浮点型(dbt);
        dbt_bt = dbt & VT_基本类型;
        sbt_bt = sbt & VT_基本类型;

        c = (栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
#if !defined ZHI_是_本机 && !defined ZHI_IS_NATIVE_387
        c &= (dbt != VT_长双精度) | !!不需要_代码生成;
#endif
        if (c) {
            /* constant case: we can do it now */
            /* XXX: in ISOC, cannot do it if error in convert */
            if (sbt == VT_浮点)
                栈顶值->c.ld = 栈顶值->c.f;
            else if (sbt == VT_双精度)
                栈顶值->c.ld = 栈顶值->c.d;

            if (df) {
                if (sbt_bt == VT_长长整数) {
                    if ((sbt & VT_无符号) || !(栈顶值->c.i >> 63))
                        栈顶值->c.ld = 栈顶值->c.i;
                    else
                        栈顶值->c.ld = -(long double)-栈顶值->c.i;
                } else if(!sf) {
                    if ((sbt & VT_无符号) || !(栈顶值->c.i >> 31))
                        栈顶值->c.ld = (uint32_t)栈顶值->c.i;
                    else
                        栈顶值->c.ld = -(long double)-(uint32_t)栈顶值->c.i;
                }

                if (dbt == VT_浮点)
                    栈顶值->c.f = (float)栈顶值->c.ld;
                else if (dbt == VT_双精度)
                    栈顶值->c.d = (double)栈顶值->c.ld;
            } else if (sf && dbt == VT_逻辑) {
                栈顶值->c.i = (栈顶值->c.ld != 0);
            } else {
                if(sf)
                    栈顶值->c.i = 栈顶值->c.ld;
                else if (sbt_bt == VT_长长整数 || (指针_大小 == 8 && sbt == VT_指针))
                    ;
                else if (sbt & VT_无符号)
                    栈顶值->c.i = (uint32_t)栈顶值->c.i;
                else
                    栈顶值->c.i = ((uint32_t)栈顶值->c.i | -(栈顶值->c.i & 0x80000000));

                if (dbt_bt == VT_长长整数 || (指针_大小 == 8 && dbt == VT_指针))
                    ;
                else if (dbt == VT_逻辑)
                    栈顶值->c.i = (栈顶值->c.i != 0);
                else {
                    uint32_t m = dbt_bt == VT_字节 ? 0xff :
                                 dbt_bt == VT_短整数 ? 0xffff :
                                  0xffffffff;
                    栈顶值->c.i &= m;
                    if (!(dbt & VT_无符号))
                        栈顶值->c.i |= -(栈顶值->c.i & ((m >> 1) + 1));
                }
            }
            goto done;

        } else if (dbt == VT_逻辑
            && (栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号))
                == (VT_VC常量 | VT_符号)) {
            /* addresses are considered non-zero (see zhitest.c:sinit23) */
            栈顶值->r = VT_VC常量;
            栈顶值->c.i = 1;
            goto done;
        }

        /* cannot generate code for global or static initializers */
        if (仅_静态数据输出)
            goto done;

        /* non constant case: generate code */
        if (dbt == VT_逻辑) {
            生成_测试_零(双符号_不等于);
            goto done;
        }

        if (sf || df) {
            if (sf && df) {
                /* convert from fp to fp */
                生成_浮点转换为另一种浮点(dbt);
            } else if (df) {
                /* convert int to fp */
                生成_cvt_itof1(dbt);
            } else {
                /* convert fp to int */
                sbt = dbt;
                if (dbt_bt != VT_长长整数 && dbt_bt != VT_整数)
                    sbt = VT_整数;
                生成_cvt_ftoi1(sbt);
                goto again; /* may need char/short cast */
            }
            goto done;
        }

        ds = 基本类型_大小(dbt_bt);
        ss = 基本类型_大小(sbt_bt);
        if (ds == 0 || ss == 0) {
            if (dbt_bt == VT_无类型)
                goto done;
            投放_错误(&栈顶值->type, type);
        }
        if (是_枚举(type->t) && type->ref->c < 0)
            错误_打印("cast to incomplete type");

        /* same size and no sign conversion needed */
        if (ds == ss && ds >= 4)
            goto done;
        if (dbt_bt == VT_指针 || sbt_bt == VT_指针) {
            zhi_警告("cast between pointer and integer of different size");
            if (sbt_bt == VT_指针) {
                /* put integer type to allow logical operations below */
                栈顶值->type.t = (指针_大小 == 8 ? VT_长长整数 : VT_整数);
            }
        }

        /* processor allows { int a = 0, b = *(char*)&a; }
           That means that if we cast to less width, we can just
           change the type and read it still later. */
        #define ALLOW_SUBTYPE_ACCESS 1

        if (ALLOW_SUBTYPE_ACCESS && (栈顶值->r & VT_LVAL)) {
            /* value still in memory */
            if (ds <= ss)
                goto done;
            /* ss <= 4 here */
            if (ds <= 4) {
                将rc寄存器值存储在栈顶值中(寄存器类_整数);
                goto done; /* no 64bit envolved */
            }
        }
        将rc寄存器值存储在栈顶值中(寄存器类_整数);

        trunc = 0;
#if 指针_大小 == 4
        if (ds == 8) {
            /* generate high word */
            if (sbt & VT_无符号) {
                压入整数常量(0);
                将rc寄存器值存储在栈顶值中(寄存器类_整数);
            } else {
                堆栈转为寄存器并复制到另一个寄存器();
                压入整数常量(31);
                通用_操作(双符号_右位移);
            }
            构建长长整数(dbt);
        } else if (ss == 8) {
            /* from long long: just take low order word */
            整数扩展();
            弹出堆栈值();
        }
        ss = 4;

#elif 指针_大小 == 8
        if (ds == 8) {
            /* need to convert from 32bit to 64bit */
            if (sbt & VT_无符号) {
#if defined(ZHI_TARGET_RISCV64)
                /* RISC-V keeps 32bit vals in registers sign-extended.
                   So here we need a zero-extension.  */
                trunc = 32;
#else
                goto done;
#endif
            } else {
                生成_cvt_sxtw();
                goto done;
            }
            ss = ds, ds = 4, dbt = sbt;
        } else if (ss == 8) {
            /* XXX some architectures (e.g. risc-v) would like it
               better for this merely being a 32-to-64 sign or zero-
               extension.  */
            trunc = 32; /* zero upper 32 bits */
        } else {
            ss = 4;
        }
#endif

        if (ds >= ss)
            goto done;
#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64 || defined ZHI_TARGET_ARM64
        if (ss == 4) {
            生成_cvt_csti(dbt);
            goto done;
        }
#endif
        bits = (ss - ds) * 8;
        /* for unsigned, 通用_操作 will convert SAR to SHR */
        栈顶值->type.t = (ss == 8 ? VT_长长整数 : VT_整数) | (dbt & VT_无符号);
        压入整数常量(bits);
        通用_操作(双符号_左位移);
        压入整数常量(bits - trunc);
        通用_操作(双符号_右位移);
        压入整数常量(trunc);
        通用_操作(符_SHR);
    }
done:
    栈顶值->type = *type;
    栈顶值->type.t &= ~ ( VT_常量 | VT_易变 | VT_数组);
}

/* 返回类型大小，编译时已知。 将对齐方式设置为“ a” */
静态_函数 int 类型_大小(C类型 *type, int *a)
{
    符号 *s;
    int bt;

    bt = type->t & VT_基本类型;
    if (bt == VT_结构体) {
        /* 结构体/联合体 */
        s = type->ref;
        *a = s->r;
        return s->c;
    } else if (bt == VT_指针) {
        if (type->t & VT_数组) {
            int ts;

            s = type->ref;
            ts = 类型_大小(&s->type, a);

            if (ts < 0 && s->c < 0)
                ts = -ts;

            return ts * s->c;
        } else {
            *a = 指针_大小;
            return 指针_大小;
        }
    } else if (是_枚举(type->t) && type->ref->c < 0) {
        return -1; /* incomplete enum */
    } else if (bt == VT_长双精度) {
        *a = 长双精度_对齐;
        return 长双精度_大小;
    } else if (bt == VT_双精度 || bt == VT_长长整数) {
#ifdef ZHI_TARGET_I386
#ifdef ZHI_TARGET_PE
        *a = 8;
#else
        *a = 4;
#endif
#elif defined(ZHI_TARGET_ARM)
#ifdef ZHI_ARM_EABI
        *a = 8; 
#else
        *a = 4;
#endif
#else
        *a = 8;
#endif
        return 8;
    } else if (bt == VT_整数 || bt == VT_浮点) {
        *a = 4;
        return 4;
    } else if (bt == VT_短整数) {
        *a = 2;
        return 2;
    } else if (bt == VT_128位整数 || bt == VT_128位浮点) {
        *a = 8;
        return 16;
    } else {
        /* char, void, function, _Bool */
        *a = 1;
        return 1;
    }
}

/* 在运行时在值堆栈顶部已知的推送类型大小。 将对齐方式设置为“ a” */
静态_函数 void vla_运行时_类型_大小(C类型 *type, int *a)
{
    if (type->t & VT_变长数组) {
        类型_大小(&type->ref->type, a);
        vset(&int_type, VT_LOCAL|VT_LVAL, type->ref->c);
    } else {
        压入整数常量(类型_大小(type, a));
    }
}

/* 返回t的指针类型 */
static inline C类型 *指定的_类型(C类型 *type)
{
    return &type->ref->type;
}

/* 修改类型，使其成为类型的指针。 */
静态_函数 void 修改类型_指针类型(C类型 *type)
{
    符号 *s;
    s = 符号_压入栈(符号_字段, type, 0, -1);
    type->t = VT_指针 | (type->t & VT_存储);
    type->ref = s;
}

/* 如果type1和type2完全相同（包括限定符），则返回true.
*/
static int 是_兼容_类型(C类型 *type1, C类型 *type2)
{
    return 比较_2个类型(type1,type2,0);
}

/* 如果type1和type2相同，则返回true（忽略限定符）.
*/
static int 是_兼容_不合格的_类型(C类型 *type1, C类型 *type2)
{
    return 比较_2个类型(type1,type2,1);
}

static void 投放_错误(C类型 *st, C类型 *dt)
{
    类型_不兼容_错误(st, dt, "无法将 '%s' 转换为 '%s'");
}

/* 验证类型兼容性以将栈顶值存储为“ dt”类型 */
static void 验证_指定_转换(C类型 *dt)
{
    C类型 *st, *type1, *type2;
    int dbt, sbt, qualwarn, lvl;

    st = &栈顶值->type; /* source type */
    dbt = dt->t & VT_基本类型;
    sbt = st->t & VT_基本类型;
    if (dt->t & VT_常量)
        zhi_警告("只读位置的指定");
    switch(dbt) {
    case VT_无类型:
        if (sbt != dbt)
            错误_打印("赋予空表达");
        break;
    case VT_指针:
        /* special cases for pointers */
        /* '0' can also be a pointer */
        if (是_空_指针(栈顶值))
            break;
        /* accept implicit pointer to integer cast with warning */
        if (是_整数_型(sbt)) {
            zhi_警告("赋值使指针从整数开始而无需强制转换");
            break;
        }
        type1 = 指定的_类型(dt);
        if (sbt == VT_指针)
            type2 = 指定的_类型(st);
        else if (sbt == VT_函数)
            type2 = st; /* a function is implicitly a function pointer */
        else
            goto error;
        if (是_兼容_类型(type1, type2))
            break;
        for (qualwarn = lvl = 0;; ++lvl) {
            if (((type2->t & VT_常量) && !(type1->t & VT_常量)) ||
                ((type2->t & VT_易变) && !(type1->t & VT_易变)))
                qualwarn = 1;
            dbt = type1->t & (VT_基本类型|VT_长整数);
            sbt = type2->t & (VT_基本类型|VT_长整数);
            if (dbt != VT_指针 || sbt != VT_指针)
                break;
            type1 = 指定的_类型(type1);
            type2 = 指定的_类型(type2);
        }
        if (!是_兼容_不合格的_类型(type1, type2)) {
            if ((dbt == VT_无类型 || sbt == VT_无类型) && lvl == 0) {
                /* void * can match anything */
            } else if (dbt == sbt
                && 是_整数_型(sbt & VT_基本类型)
                && 是_枚举(type1->t) + 是_枚举(type2->t)
                    + !!((type1->t ^ type2->t) & VT_无符号) < 2) {
		/* 像GCC一样，默认情况下也不会仅针对指针目标签名的更改发出警告。 但是，请警告不同的基本类型，尤其是对于未签名的枚举和已签名的int目标。  */
            } else {
                zhi_警告("从不兼容的指针类型指定");
                break;
            }
        }
        if (qualwarn)
            zhi_警告("指定从指针目标类型中丢弃限定词");
        break;
    case VT_字节:
    case VT_短整数:
    case VT_整数:
    case VT_长长整数:
        if (sbt == VT_指针 || sbt == VT_函数) {
            zhi_警告("赋值从指针进行整数转换而无需强制转换");
        } else if (sbt == VT_结构体) {
            goto case_VT_STRUCT;
        }
        /* XXX: more tests */
        break;
    case VT_结构体:
    case_VT_STRUCT:
        if (!是_兼容_不合格的_类型(dt, st)) {
    error:
            投放_错误(st, dt);
        }
        break;
    }
}

static void 通用_指定类型_转换(C类型 *dt)
{
    验证_指定_转换(dt);
    通用_转换(dt);
}

/* 将栈顶值存储在堆栈中的左值中 */
静态_函数 void 将栈顶值_存储在堆栈左值(void)
{
    int sbt, dbt, ft, r, size, align, bit_size, bit_pos, delayed_cast;

    ft = 栈顶值[-1].type.t;
    sbt = 栈顶值->type.t & VT_基本类型;
    dbt = ft & VT_基本类型;

    验证_指定_转换(&栈顶值[-1].type);

    if (sbt == VT_结构体) {
        /* if structure, only generate pointer */
        /* structure assignment : generate memcpy */
        /* XXX: 编译优化 if small size */
            size = 类型_大小(&栈顶值->type, &align);

            /* destination */
            vswap();
#ifdef 配置_ZHI_边界检查
            if (栈顶值->r & VT_强制边界检查)
                生成左值边界代码(); /* check would be wrong after 获取栈顶值地址() */
#endif
            栈顶值->type.t = VT_指针;
            获取栈顶值地址();

            /* address of memcpy() */
#ifdef ZHI_ARM_EABI
            if(!(align & 7))
                推送对_全局符号V_的引用(&函数_旧_类型, 符_memmove8);
            else if(!(align & 3))
                推送对_全局符号V_的引用(&函数_旧_类型, 符_memmove4);
            else
#endif
            /* Use memmove, rather than memcpy, as dest and src may be same: */
            推送对_全局符号V_的引用(&函数_旧_类型, 符_memmove);

            vswap();
            /* source */
            vpushv(栈顶值 - 2);
#ifdef 配置_ZHI_边界检查
            if (栈顶值->r & VT_强制边界检查)
                生成左值边界代码();
#endif
            栈顶值->type.t = VT_指针;
            获取栈顶值地址();
            /* type size */
            压入整数常量(size);
            具体地址函数_调用(3);
        /* leave source on stack */

    } else if (ft & VT_位域) {
        /* bitfield 存储 handling */

        /* save lvalue as expression result (example: s.b = s.a = n;) */
        vdup(), 栈顶值[-1] = 栈顶值[-2];

        bit_pos = BIT_POS(ft);
        bit_size = BIT_大小(ft);
        /* remove bit field info to avoid loops */
        栈顶值[-1].type.t = ft & ~VT_结构体_掩码;

        if (dbt == VT_逻辑) {
            通用_转换(&栈顶值[-1].type);
            栈顶值[-1].type.t = (栈顶值[-1].type.t & ~VT_基本类型) | (VT_字节 | VT_无符号);
        }
        r = adjust_单字节(栈顶值 - 1, bit_pos, bit_size);
        if (dbt != VT_逻辑) {
            通用_转换(&栈顶值[-1].type);
            dbt = 栈顶值[-1].type.t & VT_基本类型;
        }
        if (r == VT_结构体) {
            单字节_存储_模式(bit_pos, bit_size);
        } else {
            unsigned long long mask = (1ULL << bit_size) - 1;
            if (dbt != VT_逻辑) {
                /* mask source */
                if (dbt == VT_长长整数)
                    压入长长整数(mask);
                else
                    压入整数常量((unsigned)mask);
                通用_操作('&');
            }
            /* shift source */
            压入整数常量(bit_pos);
            通用_操作(双符号_左位移);
            vswap();
            /* duplicate destination */
            vdup();
            vrott(3);
            /* 加载 destination, mask and or with source */
            if (dbt == VT_长长整数)
                压入长长整数(~(mask << bit_pos));
            else
                压入整数常量(~((unsigned)mask << bit_pos));
            通用_操作('&');
            通用_操作('|');
            /* 存储 result */
            将栈顶值_存储在堆栈左值();
            /* ... and discard */
            弹出堆栈值();
        }
    } else if (dbt == VT_无类型) {
        --栈顶值;
    } else {
            /* 编译优化 char/short casts */
            delayed_cast = 0;
            if ((dbt == VT_字节 || dbt == VT_短整数)
                && 是_整数_型(sbt)
                ) {
                if ((栈顶值->r & VT_强制转换)
                    && 基本类型_大小(dbt) > 基本类型_大小(sbt)
                    )
                    强制_字符型短整型_转换();
                delayed_cast = 1;
            } else {
                通用_转换(&栈顶值[-1].type);
            }

#ifdef 配置_ZHI_边界检查
            /* bound check case */
            if (栈顶值[-1].r & VT_强制边界检查) {
                vswap();
                生成左值边界代码();
                vswap();
            }
#endif
            将rc寄存器值存储在栈顶值中(返回类型t的_通用寄存器类(dbt)); /* generate value */

            if (delayed_cast) {
                栈顶值->r |= BFVAL(VT_强制转换, (sbt == VT_长长整数) + 1);
                //zhi_警告("deley cast %x -> %x", sbt, dbt);
                栈顶值->type.t = ft & VT_类型;
            }

            /* if lvalue was saved on stack, must read it */
            if ((栈顶值[-1].r & VT_值掩码) == VT_LLOCAL) {
                堆栈值 sv;
                r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
                sv.type.t = VT_指针DIFF_T;
                sv.r = VT_LOCAL | VT_LVAL;
                sv.c.i = 栈顶值[-1].c.i;
                加载(r, &sv);
                栈顶值[-1].r = r | VT_LVAL;
            }

            r = 栈顶值->r & VT_值掩码;
            /* two word case handling :
               存储 second register at word + 4 (or +8 for x86-64)  */
            if (使用_两个单词的_类型(dbt)) {
                int load_type = (dbt == VT_128位浮点) ? VT_双精度 : VT_指针DIFF_T;
                栈顶值[-1].type.t = load_type;
                存储(r, 栈顶值 - 1);
                vswap();
                /* convert to int to increment easily */
                栈顶值->type.t = VT_指针DIFF_T;
                获取栈顶值地址();
                压入目标地址类型常量(指针_大小);
                通用_操作('+');
                栈顶值->r |= VT_LVAL;
                vswap();
                栈顶值[-1].type.t = load_type;
                /* XXX: it works because r2 is spilled last ! */
                存储(栈顶值->r2, 栈顶值 - 1);
            } else {
                /* single word */
                存储(r, 栈顶值 - 1);
            }
        vswap();
        栈顶值--; /* NOT 弹出堆栈值() because on x86 it would flush the fp stack */
    }
}

/* post定义POST / PRE添加。 c是令牌++或- */
静态_函数 void inc(int post, int c)
{
    测试_左值();
    vdup(); /* 保存左值 */
    if (post) {
        堆栈转为寄存器并复制到另一个寄存器(); /* 重复值 */
        vrotb(3);
        vrotb(3);
    }
    /* 添加常量 */
    压入整数常量(c - 符_MID); 
    通用_操作('+');
    将栈顶值_存储在堆栈左值(); /* 存储值 */
    if (post)
        弹出堆栈值(); /* 如果发布操作，返回保存的值 */
}

静态_函数 void 解析_多_字符串 (动态字符串 *astr, const char *msg)
{
    /* read the string */
    if (单词编码 != 常量_字符串)
        应为(msg);
    动态字符串_初始化(astr);
    while (单词编码 == 常量_字符串) {
        /* XXX: add \0 handling too ? */
        动态字符串_cat(astr, 单词值.str.data, -1);
        带有宏替换的下个标记();
    }
    动态字符串_追加单个字符(astr, '\0');
}

/* 如果I> = 1且为2的幂，则返回log2（i）+1。 如果我为0，则返回0。  */
静态_函数 int 精确_对数2p1(int i)
{
  int ret;
  if (!i)
    return 0;
  for (ret = 1; i >= 1 << 8; ret += 8)
    i >>= 8;
  if (i >= 1 << 4)
    ret += 4, i >>= 4;
  if (i >= 1 << 2)
    ret += 2, i >>= 2;
  if (i >= 1 << 1)
    ret++;
  return ret;
}

/* Parse __attribute__((...)) GNUC extension. */
static void 解析_属性(属性定义 *ad)
{
    int t, n;
    动态字符串 astr;
    
redo:
    if (单词编码 != 关键词_ATTRIBUTE1 && 单词编码 != 关键词_ATTRIBUTE2 && 单词编码 != 关键词_属性)
        return;
    带有宏替换的下个标记();
    跳过('(');
    跳过('(');
    while (单词编码 != ')') {
        if (单词编码 < 符_识别)
            应为("attribute name");
        t = 单词编码;
        带有宏替换的下个标记();
        switch(t) {
	case 符_CLEANUP1:
	case 符_CLEANUP2:
	{
	    符号 *s;

	    跳过('(');
	    s = 符号_查询(单词编码);
	    if (!s) {
	      zhi_警告("函数 '%s'是隐式声明",
			  取_单词字符串(单词编码, &单词值));
	      s = 外部_全局_符号(单词编码, &函数_旧_类型);
            } else if ((s->type.t & VT_基本类型) != VT_函数)
                错误_打印("'%s' 未声明为函数", 取_单词字符串(单词编码, &单词值));
	    ad->cleanup_func = s;
	    带有宏替换的下个标记();
            跳过(')');
	    break;
	}
        case 符_CONSTRUCTOR1:
        case 符_CONSTRUCTOR2:
            ad->f.func_ctor = 1;
            break;
        case 符_DESTRUCTOR1:
        case 符_DESTRUCTOR2:
            ad->f.func_dtor = 1;
            break;
        case 符_ALWAYS_INLINE1:
        case 符_ALWAYS_INLINE2:
            ad->f.func_alwinl = 1;
            break;
        case 符_SECTION1:
        case 符_SECTION2:
            跳过('(');
	    解析_多_字符串(&astr, "section name");
            ad->section = 查找_段(zhi_状态, (char *)astr.指向字符串的指针);
            跳过(')');
	    动态字符串_释放(&astr);
            break;
        case 符_ALIAS1:
        case 符_ALIAS2:
            跳过('(');
	    解析_多_字符串(&astr, "alias(\"target\")");
            ad->alias_target = /* save string as 标识符, for later */
              单词表_查找((char*)astr.指向字符串的指针, astr.字符串长度-1)->单词编码;
            跳过(')');
	    动态字符串_释放(&astr);
            break;
	case 符_VISIBILITY1:
	case 符_VISIBILITY2:
            跳过('(');
	    解析_多_字符串(&astr,
			   "visibility(\"default|hidden|internal|protected\")");
	    if (!strcmp (astr.指向字符串的指针, "default"))
	        ad->a.visibility = STV_DEFAULT;
	    else if (!strcmp (astr.指向字符串的指针, "hidden"))
	        ad->a.visibility = STV_HIDDEN;
	    else if (!strcmp (astr.指向字符串的指针, "internal"))
	        ad->a.visibility = STV_INTERNAL;
	    else if (!strcmp (astr.指向字符串的指针, "protected"))
	        ad->a.visibility = STV_PROTECTED;
	    else
                应为("visibility(\"default|hidden|internal|protected\")");
            跳过(')');
	    动态字符串_释放(&astr);
            break;
        case 符_ALIGNED1:
        case 符_ALIGNED2:
            if (单词编码 == '(') {
                带有宏替换的下个标记();
                n = 表达式_常量();
                if (n <= 0 || (n & (n - 1)) != 0) 
                    错误_打印("alignment must be a positive power of two");
                跳过(')');
            } else {
                n = MAX_ALIGN;
            }
            ad->a.aligned = 精确_对数2p1(n);
	    if (n != 1 << (ad->a.aligned - 1))
	      错误_打印("alignment of %d is larger than implemented", n);
            break;
        case 符_PACKED1:
        case 符_PACKED2:
            ad->a.packed = 1;
            break;
        case 符_WEAK1:
        case 符_WEAK2:
            ad->a.weak = 1;
            break;
        case 符_UNUSED1:
        case 符_UNUSED2:
            /* currently, no need to handle it because zhi does not
               track unused objects */
            break;
        case 符_NORETURN1:
        case 符_NORETURN2:
            ad->f.func_noreturn = 1;
            break;
        case 符_CDECL1:
        case 符_CDECL2:
        case 符_CDECL3:
            ad->f.函数_调用 = 函数_CDECL;
            break;
        case 符_标准调用1:
        case 符_标准调用2:
        case 符_标准调用3:
            ad->f.函数_调用 = 函数_标准调用;
            break;
#ifdef ZHI_TARGET_I386
        case 符_REGPARM1:
        case 符_REGPARM2:
            跳过('(');
            n = 表达式_常量();
            if (n > 3) 
                n = 3;
            else if (n < 0)
                n = 0;
            if (n > 0)
                ad->f.函数_调用 = 函数_快速调用1 + n - 1;
            跳过(')');
            break;
        case 符_快速调用1:
        case 符_快速调用2:
        case 符_快速调用3:
            ad->f.函数_调用 = 函数_快速调用W;
            break;            
#endif
        case 符_MODE:
            跳过('(');
            switch(单词编码) {
                case 符_MODE_DI:
                    ad->attr_mode = VT_长长整数 + 1;
                    break;
                case 符_MODE_QI:
                    ad->attr_mode = VT_字节 + 1;
                    break;
                case 符_MODE_HI:
                    ad->attr_mode = VT_短整数 + 1;
                    break;
                case 符_MODE_SI:
                case 符_MODE_word:
                    ad->attr_mode = VT_整数 + 1;
                    break;
                default:
                    zhi_警告("__mode__(%s) not supported\n", 取_单词字符串(单词编码, NULL));
                    break;
            }
            带有宏替换的下个标记();
            跳过(')');
            break;
        case 符_DLLEXPORT:
            ad->a.dllexport = 1;
            break;
        case 符_NODECORATE:
            ad->a.nodecorate = 1;
            break;
        case 符_DLLIMPORT:
            ad->a.dllimport = 1;
            break;
        default:
            if (zhi_状态->警告_不支持)
                zhi_警告("'%s' attribute ignored", 取_单词字符串(t, NULL));
            /* 跳过 parameters */
            if (单词编码 == '(') {
                int parenthesis = 0;
                do {
                    if (单词编码 == '(') 
                        parenthesis++;
                    else if (单词编码 == ')') 
                        parenthesis--;
                    带有宏替换的下个标记();
                } while (parenthesis && 单词编码 != -1);
            }
            break;
        }
        if (单词编码 != ',')
            break;
        带有宏替换的下个标记();
    }
    跳过(')');
    跳过(')');
    goto redo;
}

static 符号 * 查找_域 (C类型 *type, int v, int *cumofs)
{
    符号 *s = type->ref;
    v |= 符号_字段;
    while ((s = s->next) != NULL) {
	if ((s->v & 符号_字段) &&
	    (s->type.t & VT_基本类型) == VT_结构体 &&
	    (s->v & ~符号_字段) >= 符号_第一个_匿名) {
	    符号 *ret = 查找_域 (&s->type, v, cumofs);
	    if (ret) {
                *cumofs += s->c;
	        return ret;
            }
	}
	if (s->v == v)
	  break;
    }
    return s;
}

static void 结构体_布局(C类型 *type, 属性定义 *ad)
{
    int size, align, maxalign, offset, c, bit_pos, bit_size;
    int packed, a, bt, prevbt, prev_bit_size;
    int pcc = !zhi_状态->模拟_对齐位域MS算法;
    int pragma_pack = *zhi_状态->包_堆_ptr;
    符号 *f;

    maxalign = 1;
    offset = 0;
    c = 0;
    bit_pos = 0;
    prevbt = VT_结构体; /* make it never match */
    prev_bit_size = 0;

//#define BF_DEBUG

    for (f = type->ref->next; f; f = f->next) {
        if (f->type.t & VT_位域)
            bit_size = BIT_大小(f->type.t);
        else
            bit_size = -1;
        size = 类型_大小(&f->type, &align);
        a = f->a.aligned ? 1 << (f->a.aligned - 1) : 0;
        packed = 0;

        if (pcc && bit_size == 0) {
            /* in pcc mode, packing does not affect zero-width bitfields */

        } else {
            /* in pcc mode, attribute packed overrides if set. */
            if (pcc && (f->a.packed || ad->a.packed))
                align = packed = 1;

            /* pragma pack overrides align if lesser and packs bitfields always */
            if (pragma_pack) {
                packed = 1;
                if (pragma_pack < align)
                    align = pragma_pack;
                /* in pcc mode pragma pack also overrides individual align */
                if (pcc && pragma_pack < a)
                    a = 0;
            }
        }
        /* some individual align was specified */
        if (a)
            align = a;

        if (type->ref->type.t == VT_共用体) {
	    if (pcc && bit_size >= 0)
	        size = (bit_size + 7) >> 3;
	    offset = 0;
	    if (size > c)
	        c = size;

	} else if (bit_size < 0) {
            if (pcc)
                c += (bit_pos + 7) >> 3;
	    c = (c + align - 1) & -align;
	    offset = c;
	    if (size > 0)
	        c += size;
	    bit_pos = 0;
	    prevbt = VT_结构体;
	    prev_bit_size = 0;

	} else {
	    /* A bit-field.  Layout is more complicated.  There are two
	       options: PCC (GCC) compatible and MS compatible */
            if (pcc) {
		/* In PCC layout a bit-field is placed adjacent to the
                   preceding bit-fields, except if:
                   - it has zero-width
                   - an individual alignment was given
                   - it would overflow its base type container and
                     there is no packing */
                if (bit_size == 0) {
            new_field:
		    c = (c + ((bit_pos + 7) >> 3) + align - 1) & -align;
		    bit_pos = 0;
                } else if (f->a.aligned) {
                    goto new_field;
                } else if (!packed) {
                    int a8 = align * 8;
	            int ofs = ((c * 8 + bit_pos) % a8 + bit_size + a8 - 1) / a8;
                    if (ofs > size / align)
                        goto new_field;
                }

                /* in pcc mode, long long bitfields have type int if they fit */
                if (size == 8 && bit_size <= 32)
                    f->type.t = (f->type.t & ~VT_基本类型) | VT_整数, size = 4;

                while (bit_pos >= align * 8)
                    c += align, bit_pos -= align * 8;
                offset = c;

		/* In PCC layout named bit-fields influence the alignment
		   of the containing struct using the base types alignment,
		   except for packed fields (which here have correct align).  */
		if (f->v & 符号_第一个_匿名
                    // && bit_size // ??? gcc on ARM/rpi does that
                    )
		    align = 1;

	    } else {
		bt = f->type.t & VT_基本类型;
		if ((bit_pos + bit_size > size * 8)
                    || (bit_size > 0) == (bt != prevbt)
                    ) {
		    c = (c + align - 1) & -align;
		    offset = c;
		    bit_pos = 0;
		    /* In MS bitfield mode a bit-field run always uses
		       at least as many bits as the underlying type.
		       To start a new run it's also required that this
		       or the last bit-field had non-zero width.  */
		    if (bit_size || prev_bit_size)
		        c += size;
		}
		/* In MS layout the records alignment is normally
		   influenced by the field, except for a zero-width
		   field at the start of a run (but by further zero-width
		   fields it is again).  */
		if (bit_size == 0 && prevbt != bt)
		    align = 1;
		prevbt = bt;
                prev_bit_size = bit_size;
	    }

	    f->type.t = (f->type.t & ~(0x3f << VT_结构体_转换))
		        | (bit_pos << VT_结构体_转换);
	    bit_pos += bit_size;
	}
	if (align > maxalign)
	    maxalign = align;

#ifdef BF_DEBUG
	printf("set field %s offset %-2d size %-2d align %-2d",
	       取_单词字符串(f->v & ~符号_字段, NULL), offset, size, align);
	if (f->type.t & VT_位域) {
	    printf(" pos %-2d bits %-2d",
                    BIT_POS(f->type.t),
                    BIT_大小(f->type.t)
                    );
	}
	printf("\n");
#endif

        f->c = offset;
	f->r = 0;
    }

    if (pcc)
        c += (bit_pos + 7) >> 3;

    /* 存储 size and alignment */
    a = bt = ad->a.aligned ? 1 << (ad->a.aligned - 1) : 1;
    if (a < maxalign)
        a = maxalign;
    type->ref->r = a;
    if (pragma_pack && pragma_pack < maxalign && 0 == pcc) {
        /* can happen if individual align for some member was given.  In
           this case MSVC ignores maxalign when aligning the size */
        a = pragma_pack;
        if (a < bt)
            a = bt;
    }
    c = (c + a - 1) & -a;
    type->ref->c = c;

#ifdef BF_DEBUG
    printf("struct size %-2d align %-2d\n\n", c, a), fflush(stdout);
#endif

    /* check whether we can access bitfields by their type */
    for (f = type->ref->next; f; f = f->next) {
        int s, px, cx, c0;
        C类型 t;

        if (0 == (f->type.t & VT_位域))
            continue;
        f->type.ref = f;
        f->auxtype = -1;
        bit_size = BIT_大小(f->type.t);
        if (bit_size == 0)
            continue;
        bit_pos = BIT_POS(f->type.t);
        size = 类型_大小(&f->type, &align);
        if (bit_pos + bit_size <= size * 8 && f->c + size <= c)
            continue;

        /* try to access the field using a different type */
        c0 = -1, s = align = 1;
        t.t = VT_字节;
        for (;;) {
            px = f->c * 8 + bit_pos;
            cx = (px >> 3) & -align;
            px = px - (cx << 3);
            if (c0 == cx)
                break;
            s = (px + bit_size + 7) >> 3;
            if (s > 4) {
                t.t = VT_长长整数;
            } else if (s > 2) {
                t.t = VT_整数;
            } else if (s > 1) {
                t.t = VT_短整数;
            } else {
                t.t = VT_字节;
            }
            s = 类型_大小(&t, &align);
            c0 = cx;
        }

        if (px + bit_size <= s * 8 && cx + s <= c) {
            /* update offset and bit position */
            f->c = cx;
            bit_pos = px;
	    f->type.t = (f->type.t & ~(0x3f << VT_结构体_转换))
		        | (bit_pos << VT_结构体_转换);
            if (s != size)
                f->auxtype = t.t;
#ifdef BF_DEBUG
            printf("FIX field %s offset %-2d size %-2d align %-2d "
                "pos %-2d bits %-2d\n",
                取_单词字符串(f->v & ~符号_字段, NULL),
                cx, s, align, px, bit_size);
#endif
        } else {
            /* fall back to 加载/存储 single-byte wise */
            f->auxtype = VT_结构体;
#ifdef BF_DEBUG
            printf("FIX field %s : 加载 byte-wise\n",
                 取_单词字符串(f->v & ~符号_字段, NULL));
#endif
        }
    }
}

/* enum/struct/union 声明. u is VT_枚举/VT_结构体/VT_共用体 */
static void 结构_声明(C类型 *type, int u)
{
    int v, c, size, align, flexible;
    int bit_size, bsize, bt;
    符号 *s, *ss, **ps;
    属性定义 ad, ad1;
    C类型 type1, btype;

    memset(&ad, 0, sizeof ad);
    带有宏替换的下个标记();
    解析_属性(&ad);
    if (单词编码 != '{') {
        v = 单词编码;
        带有宏替换的下个标记();
        /* struct already defined ? return it */
        if (v < 符_识别)
            应为("struct/union/enum 名称");
        s = 结构体_查询(v);
        if (s && (s->符号_范围 == 局部_范围 || 单词编码 != '{')) {
            if (u == s->type.t)
                goto do_decl;
            if (u == VT_枚举 && 是_枚举(s->type.t))
                goto do_decl;
            错误_打印("'%s'重新定义", 取_单词字符串(v, NULL));
        }
    } else {
        v = 匿名符号索引++;
    }
    /* Record the original enum/struct/union 标识符.  */
    type1.t = u == VT_枚举 ? u | VT_整数 | VT_无符号 : u;
    type1.ref = NULL;
    /* we put an undefined size for struct/union */
    s = 符号_压入栈(v | 符号_结构体, &type1, 0, -1);
    s->r = 0; /* default alignment is zero as gcc */
do_decl:
    type->t = s->type.t;
    type->ref = s;

    if (单词编码 == '{') {
        带有宏替换的下个标记();
        if (s->c != -1)
            错误_打印("struct/union/enum already defined");
        s->c = -2;
        /* cannot be empty */
        /* non empty enums are not allowed */
        ps = &s->next;
        if (u == VT_枚举) {
            long long ll = 0, pl = 0, nl = 0;
	    C类型 t;
            t.ref = s;
            /* enum symbols have static storage */
            t.t = VT_整数|VT_静态|VT_枚举_变长;
            for(;;) {
                v = 单词编码;
                if (v < 符_没识别)
                    应为("identifier");
                ss = 符号_查询(v);
                if (ss && !局部符号_堆栈)
                    错误_打印("redefinition of enumerator '%s'",
                              取_单词字符串(v, NULL));
                带有宏替换的下个标记();
                if (单词编码 == '=') {
                    带有宏替换的下个标记();
		    ll = 表达式_常量64();
                }
                ss = 符号_压入栈(v, &t, VT_VC常量, 0);
                ss->enum_val = ll;
                *ps = ss, ps = &ss->next;
                if (ll < nl)
                    nl = ll;
                if (ll > pl)
                    pl = ll;
                if (单词编码 != ',')
                    break;
                带有宏替换的下个标记();
                ll++;
                /* NOTE: we accept a trailing comma */
                if (单词编码 == '}')
                    break;
            }
            跳过('}');
            /* set integral type of the enum */
            t.t = VT_整数;
            if (nl >= 0) {
                if (pl != (unsigned)pl)
                    t.t = (LONG_SIZE==8 ? VT_长长整数|VT_长整数 : VT_长长整数);
                t.t |= VT_无符号;
            } else if (pl != (int)pl || nl != (int)nl)
                t.t = (LONG_SIZE==8 ? VT_长长整数|VT_长整数 : VT_长长整数);
            s->type.t = type->t = t.t | VT_枚举;
            s->c = 0;
            /* set type for enum members */
            for (ss = s->next; ss; ss = ss->next) {
                ll = ss->enum_val;
                if (ll == (int)ll) /* default is int if it fits */
                    continue;
                if (t.t & VT_无符号) {
                    ss->type.t |= VT_无符号;
                    if (ll == (unsigned)ll)
                        continue;
                }
                ss->type.t = (ss->type.t & ~VT_基本类型)
                    | (LONG_SIZE==8 ? VT_长长整数|VT_长整数 : VT_长长整数);
            }
        } else {
            c = 0;
            flexible = 0;
            while (单词编码 != '}') {
                if (!解析_基本类型(&btype, &ad1)) {
		    跳过(';');
		    continue;
		}
                while (1) {
		    if (flexible)
		        错误_打印("flexible array member '%s' not at the end of struct",
                              取_单词字符串(v, NULL));
                    bit_size = -1;
                    v = 0;
                    type1 = btype;
                    if (单词编码 != ':') {
			if (单词编码 != ';')
                            类型_声明(&type1, &ad1, &v, 类型_直接);
                        if (v == 0) {
                    	    if ((type1.t & VT_基本类型) != VT_结构体)
                        	应为("identifier");
                    	    else {
				int v = btype.ref->v;
				if (!(v & 符号_字段) && (v & ~符号_结构体) < 符号_第一个_匿名) {
				    if (zhi_状态->允许_匿名联合和结构 == 0)
                        		应为("identifier");
				}
                    	    }
                        }
                        if (类型_大小(&type1, &align) < 0) {
			    if ((u == VT_结构体) && (type1.t & VT_数组) && c)
			        flexible = 1;
			    else
			        错误_打印("field '%s' has incomplete type",
                                      取_单词字符串(v, NULL));
                        }
                        if ((type1.t & VT_基本类型) == VT_函数 ||
			    (type1.t & VT_基本类型) == VT_无类型 ||
                            (type1.t & VT_存储))
                            错误_打印("invalid type for '%s'", 
                                  取_单词字符串(v, NULL));
                    }
                    if (单词编码 == ':') {
                        带有宏替换的下个标记();
                        bit_size = 表达式_常量();
                        /* XXX: handle v = 0 case for messages */
                        if (bit_size < 0)
                            错误_打印("negative width in bit-field '%s'", 
                                  取_单词字符串(v, NULL));
                        if (v && bit_size == 0)
                            错误_打印("zero width for bit-field '%s'", 
                                  取_单词字符串(v, NULL));
			解析_属性(&ad1);
                    }
                    size = 类型_大小(&type1, &align);
                    if (bit_size >= 0) {
                        bt = type1.t & VT_基本类型;
                        if (bt != VT_整数 && 
                            bt != VT_字节 && 
                            bt != VT_短整数 &&
                            bt != VT_逻辑 &&
                            bt != VT_长长整数)
                            错误_打印("bitfields must have scalar type");
                        bsize = size * 8;
                        if (bit_size > bsize) {
                            错误_打印("width of '%s' exceeds its type",
                                  取_单词字符串(v, NULL));
                        } else if (bit_size == bsize
                                    && !ad.a.packed && !ad1.a.packed) {
                            /* no need for bit fields */
                            ;
                        } else if (bit_size == 64) {
                            错误_打印("field width 64 not implemented");
                        } else {
                            type1.t = (type1.t & ~VT_结构体_掩码)
                                | VT_位域
                                | (bit_size << (VT_结构体_转换 + 6));
                        }
                    }
                    if (v != 0 || (type1.t & VT_基本类型) == VT_结构体) {
                        /* Remember we've seen a real field to check
			   for placement of flexible array member. */
			c = 1;
                    }
		    /* If member is a struct or bit-field, enforce
		       placing into the struct (as anonymous).  */
                    if (v == 0 &&
			((type1.t & VT_基本类型) == VT_结构体 ||
			 bit_size >= 0)) {
		        v = 匿名符号索引++;
		    }
                    if (v) {
                        ss = 符号_压入栈(v | 符号_字段, &type1, 0, 0);
                        ss->a = ad1.a;
                        *ps = ss;
                        ps = &ss->next;
                    }
                    if (单词编码 == ';' || 单词编码 == 符_文件结尾)
                        break;
                    跳过(',');
                }
                跳过(';');
            }
            跳过('}');
	    解析_属性(&ad);
            if (ad.cleanup_func) {
                zhi_警告("attribute '__cleanup__' ignored on type");
            }
            结构体_布局(type, &ad);
        }
    }
}

static void 符号_的_属性(属性定义 *ad, 符号 *s)
{
    合并_符号属性(&ad->a, &s->a);
    合并_函数属性(&ad->f, &s->f);
}

/*将类型限定符添加到类型。 如果类型是数组，则将限定符添加到元素类型，并进行复制，因为它可能是typedef。 */
static void 解析_基本类型_合规(C类型 *type, int qualifiers)
{
    while (type->t & VT_数组) {
        type->ref = 符号_压入栈(符号_字段, &type->ref->type, 0, type->ref->c);
        type = &type->ref->type;
    }
    type->t |= qualifiers;
}

/* 如果没有类型声明，则返回0。 否则，返回基本类型并跳过它。
 */
static int 解析_基本类型(C类型 *type, 属性定义 *ad)
{
    int t, u, bt, st, type_found, typespec_found, g, n;
    符号 *s;
    C类型 type1;

    memset(ad, 0, sizeof(属性定义));
    type_found = 0;
    typespec_found = 0;
    t = VT_整数;
    bt = st = -1;
    type->ref = NULL;

    while(1) {
        switch(单词编码) {
        case 关键词_EXTENSION:
            /* currently, we really ignore extension */
            带有宏替换的下个标记();
            continue;

            /* basic types */
        case 关键词_CHAR:
        case 关键词_字符型:
            u = VT_字节;
        basic_type:
            带有宏替换的下个标记();
        basic_type1:
            if (u == VT_短整数 || u == VT_长整数) {
                if (st != -1 || (bt != -1 && bt != VT_整数))
                    tmbt: 错误_打印("too many basic types");
                st = u;
            } else {
                if (bt != -1 || (st != -1 && u != VT_整数))
                    goto tmbt;
                bt = u;
            }
            if (u != VT_整数)
                t = (t & ~(VT_基本类型|VT_长整数)) | u;
            typespec_found = 1;
            break;
        case 关键词_VOID:
        case 关键词_无类型:
            u = VT_无类型;
            goto basic_type;
        case 关键词_SHORT:
        case 关键词_短整数型:
            u = VT_短整数;
            goto basic_type;
        case 关键词_INT:
        case 关键词_整数型:
            u = VT_整数;
            goto basic_type;
        case 关键词_ALIGNAS:
            { int n;
              属性定义 ad1;
              带有宏替换的下个标记();
              跳过('(');
              memset(&ad1, 0, sizeof(属性定义));
              if (解析_基本类型(&type1, &ad1)) {
                  类型_声明(&type1, &ad1, &n, 类型_抽象);
                  if (ad1.a.aligned)
                    n = 1 << (ad1.a.aligned - 1);
                  else
                    类型_大小(&type1, &n);
              } else {
                  n = 表达式_常量();
                  if (n <= 0 || (n & (n - 1)) != 0)
                    错误_打印("对齐方式必须是两个的正幂");
              }
              跳过(')');
              ad->a.aligned = 精确_对数2p1(n);
            }
            continue;
        case 关键词_LONG:
        case 关键词_长整数型:
            if ((t & VT_基本类型) == VT_双精度) {
                t = (t & ~(VT_基本类型|VT_长整数)) | VT_长双精度;
            } else if ((t & (VT_基本类型|VT_长整数)) == VT_长整数) {
                t = (t & ~(VT_基本类型|VT_长整数)) | VT_长长整数;
            } else {
                u = VT_长整数;
                goto basic_type;
            }
            带有宏替换的下个标记();
            break;
#ifdef ZHI_TARGET_ARM64
        case 关键词_UINT128:
        case 关键词_无整128:
            /* GCC的__uint128_t出现在某些Linux标头文件数中。 使它成为long double的代名词，以获取正确的大小和对齐方式。 */
            u = VT_长双精度;
            goto basic_type;
#endif
        case 关键词_BOOL:
        case 关键词_逻辑型:
            u = VT_逻辑;
            goto basic_type;
        case 关键词_FLOAT:
        case 关键词_浮点型:
            u = VT_浮点;
            goto basic_type;
        case 关键词_DOUBLE:
        case 关键词_双精度浮点型:
            if ((t & (VT_基本类型|VT_长整数)) == VT_长整数) {
                t = (t & ~(VT_基本类型|VT_长整数)) | VT_长双精度;
            } else {
                u = VT_双精度;
                goto basic_type;
            }
            带有宏替换的下个标记();
            break;
        case 关键词_ENUM:
        case 关键词_枚举:
            结构_声明(&type1, VT_枚举);
        basic_type2:
            u = type1.t;
            type->ref = type1.ref;
            goto basic_type1;
        case 关键词_STRUCT:
        case 关键词_结构体:
            结构_声明(&type1, VT_结构体);
            goto basic_type2;
        case 关键词_UNION:
        case 关键词_共用体:
            结构_声明(&type1, VT_共用体);
            goto basic_type2;

            /* type modifiers */
        case 关键词_CONST1:
        case 关键词_CONST2:
        case 关键词_CONST3:
        case 关键词_常量:
            type->t = t;
            解析_基本类型_合规(type, VT_常量);
            t = type->t;
            带有宏替换的下个标记();
            break;
        case 关键词_VOLATILE1:
        case 关键词_VOLATILE2:
        case 关键词_VOLATILE3:
        case 关键词_易变:
            type->t = t;
            解析_基本类型_合规(type, VT_易变);
            t = type->t;
            带有宏替换的下个标记();
            break;
        case 关键词_SIGNED1:
        case 关键词_SIGNED2:
        case 关键词_SIGNED3:
        case 关键词_有符号:
            if ((t & (VT_显式符号|VT_无符号)) == (VT_显式符号|VT_无符号))
                错误_打印("有符号和无符号修饰符");
            t |= VT_显式符号;
            带有宏替换的下个标记();
            typespec_found = 1;
            break;
        case 关键词_REGISTER:
        case 关键词_寄存器:
        case 关键词_AUTO:
        case 关键词_自动:
        case 关键词_RESTRICT1:
        case 关键词_RESTRICT2:
        case 关键词_RESTRICT3:
        case 关键词_限定:
            带有宏替换的下个标记();
            break;
        case 关键词_UNSIGNED:
        case 关键词_无符号:
            if ((t & (VT_显式符号|VT_无符号)) == VT_显式符号)
                错误_打印("有符号和无符号修饰符");
            t |= VT_显式符号 | VT_无符号;
            带有宏替换的下个标记();
            typespec_found = 1;
            break;

            /* 存储 */
        case 关键词_EXTERN:
        case 关键词_外部:
            g = VT_外部;
            goto storage;
        case 关键词_STATIC:
        case 关键词_静态:
            g = VT_静态;
            goto storage;
        case 关键词_TYPEDEF:
        case 关键词_别名:
            g = VT_别名;
            goto storage;
       storage:
            if (t & (VT_外部|VT_静态|VT_别名) & ~g)
                错误_打印("多个存储类别");
            t |= g;
            带有宏替换的下个标记();
            break;
        case 关键词_INLINE1:
        case 关键词_INLINE2:
        case 关键词_INLINE3:
        case 关键词_内联:
            t |= VT_内联;
            带有宏替换的下个标记();
            break;
        case 符_NORETURN3:
            带有宏替换的下个标记();
            ad->f.func_noreturn = 1;
            break;
            /* GNUC属性 */
        case 关键词_ATTRIBUTE1:
        case 关键词_ATTRIBUTE2:
        case 关键词_属性:
            解析_属性(ad);
            if (ad->attr_mode) {
                u = ad->attr_mode -1;
                t = (t & ~(VT_基本类型|VT_长整数)) | u;
            }
            continue;
            /* GNUC类型 */
        case 关键词_TYPEOF1:
        case 关键词_TYPEOF2:
        case 关键词_TYPEOF3:
        case 关键词_取类型:
            带有宏替换的下个标记();
            解析_表达式_类型(&type1);
            /* 删除除typedef外的所有存储修饰符 */
            type1.t &= ~(VT_存储&~VT_别名);
	    if (type1.ref)
                符号_的_属性(ad, type1.ref);
            goto basic_type2;
        default:
            if (typespec_found)
                goto the_end;
            s = 符号_查询(单词编码);
            if (!s || !(s->type.t & VT_别名))
                goto the_end;

            n = 单词编码, 带有宏替换的下个标记();
            if (单词编码 == ':' && !in_通用) {
                /* 忽略是否是标签 */
                设为_指定标识符(n);
                goto the_end;
            }

            t &= ~(VT_基本类型|VT_长整数);
            u = t & ~(VT_常量 | VT_易变), t ^= u;
            type->t = (s->type.t & ~VT_别名) | u;
            type->ref = s->type.ref;
            if (t)
                解析_基本类型_合规(type, t);
            t = type->t;
            /* 从typedef获取属性 */
            符号_的_属性(ad, s);
            typespec_found = 1;
            st = bt = -2;
            break;
        }
        type_found = 1;
    }
the_end:
    if (zhi_状态->字符串_无符号) {
        if ((t & (VT_显式符号|VT_基本类型)) == VT_字节)
            t |= VT_无符号;
    }
    /* VT_LONG仅用作VT_INT / VT_LLONG的修饰符 */
    bt = t & (VT_基本类型|VT_长整数);
    if (bt == VT_长整数)
        t |= LONG_SIZE == 8 ? VT_长长整数 : VT_整数;
#ifdef ZHI_TARGET_PE
    if (bt == VT_长双精度)
        t = (t & ~(VT_基本类型|VT_长整数)) | (VT_双精度|VT_长整数);
#endif
    type->t = t;
    return type_found;
}

/* 转换函数参数类型（数组到指针，函数到函数指针） */
static inline void 转换_函数参数_类型(C类型 *pt)
{
    /* 删除const和volatile限定词（XXX：const可用于指示const函数参数 */
    pt->t &= ~(VT_常量 | VT_易变);
    /* 数组必须根据ANSI C转换为指针*/
    pt->t &= ~VT_数组;
    if ((pt->t & VT_基本类型) == VT_函数) {
        修改类型_指针类型(pt);
    }
}

静态_函数 void 解析_汇编_字符串(动态字符串 *astr)
{
    跳过('(');
    解析_多_字符串(astr, "字符串常量");
}

/* 解析一个汇编标签并返回标识符 */
static int 在字符串_查找_汇编标签并解析(void)
{
    int v;
    动态字符串 astr;

    带有宏替换的下个标记();
    解析_汇编_字符串(&astr);
    跳过(')');
#ifdef ASM_DEBUG
    printf("汇编_alias: \"%s\"\n", (char *)astr.指向字符串的指针);
#endif
    v = 单词表_查找(astr.指向字符串的指针, astr.字符串长度 - 1)->单词编码;
    动态字符串_释放(&astr);
    return v;
}

static int post_函数类型(C类型 *type, 属性定义 *ad, int storage, int td)
{
    int n, l, t1, arg_size, align, unused_align;
    符号 **plast, *s, *first;
    属性定义 ad1;
    C类型 pt;

    if (单词编码 == '(') {
        /* 函数类型或递归声明符（如果是，则返回） */
        带有宏替换的下个标记();
	if (td && !(td & 类型_抽象))
	  return 0;
	if (单词编码 == ')')
	  l = 0;
	else if (解析_基本类型(&pt, &ad1))
	  l = 函数_新;
	else if (td) {
	    合并_属性 (ad, &ad1);
	    return 0;
	} else
	  l = 函数_旧;
        first = NULL;
        plast = &first;
        arg_size = 0;
        if (l) {
            for(;;) {
                /* 读取参数名称并计算偏移量 */
                if (l != 函数_旧) {
                    if ((pt.t & VT_基本类型) == VT_无类型 && 单词编码 == ')')
                        break;
                    类型_声明(&pt, &ad1, &n, 类型_直接 | 类型_抽象);
                    if ((pt.t & VT_基本类型) == VT_无类型)
                        错误_打印("参数声明为void");
                } else {
                    n = 单词编码;
                    if (n < 符_没识别)
                        应为("identifier");
                    pt.t = VT_无类型; /* 无效的类型 */
                    pt.ref = NULL;
                    带有宏替换的下个标记();
                }
                转换_函数参数_类型(&pt);
                arg_size += (类型_大小(&pt, &align) + 指针_大小 - 1) / 指针_大小;
                s = 符号_压入栈(n | 符号_字段, &pt, 0, 0);
                *plast = s;
                plast = &s->next;
                if (单词编码 == ')')
                    break;
                跳过(',');
                if (l == 函数_新 && 单词编码 == 符_三个圆点) {
                    l = 函数_省略;
                    带有宏替换的下个标记();
                    break;
                }
		if (l == 函数_新 && !解析_基本类型(&pt, &ad1))
		    错误_打印("无效的类型");
            }
        } else
            /* 如果没有参数，则使用旧型原型 */
            l = 函数_旧;
        跳过(')');
        /* NOTE: const is ignored in returned type as it has a special
           meaning in gcc / C++ */
        type->t &= ~VT_常量; 
        /* some ancient pre-K&R C allows a function to return an array
           and the array brackets to be put after the arguments, such 
           that "int c()[]" means something like "int[] c()" */
        if (单词编码 == '[') {
            带有宏替换的下个标记();
            跳过(']'); /* only handle simple "[]" */
            修改类型_指针类型(type);
        }
        /* we push a anonymous symbol which will contain the function prototype */
        ad->f.func_args = arg_size;
        ad->f.func_type = l;
        s = 符号_压入栈(符号_字段, type, 0, 0);
        s->a = ad->a;
        s->f = ad->f;
        s->next = first;
        type->t = VT_函数;
        type->ref = s;
    } else if (单词编码 == '[') {
	int saved_nocode_wanted = 不需要_代码生成;
        /* array definition */
        带有宏替换的下个标记();
	while (1) {
	    /* XXX The optional type-quals and static should only be accepted
	       in parameter decls.  The '*' as well, and then even only
	       in prototypes (not function defs).  */
	    switch (单词编码) {
	    case 关键词_RESTRICT1: case 关键词_RESTRICT2: case 关键词_RESTRICT3: case 关键词_限定:
	    case 关键词_CONST1:
	    case 关键词_常量:
	    case 关键词_VOLATILE1:
	    case 关键词_易变:
	    case 关键词_STATIC:
	    case '*':
		带有宏替换的下个标记();
		continue;
	    default:
		break;
	    }
	    break;
	}
        n = -1;
        t1 = 0;
        if (单词编码 != ']') {
            if (!局部符号_堆栈 || (storage & VT_静态))
                压入整数常量(表达式_常量());
            else {
		/* VLAs (which can only happen with 局部符号_堆栈 && !VT_静态)
		   length must always be evaluated, even under 不需要_代码生成,
		   so that its size slot is initialized (e.g. under sizeof
		   or typeof).  */
		不需要_代码生成 = 0;
		通用表达式();
	    }
            if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
                n = 栈顶值->c.i;
                if (n < 0)
                    错误_打印("invalid array size");
            } else {
                if (!是_整数_型(栈顶值->type.t & VT_基本类型))
                    错误_打印("size of variable length array should be an integer");
                n = 0;
                t1 = VT_变长数组;
            }
        }
        跳过(']');
        /* parse next post type */
        post_函数类型(type, ad, storage, 0);

        if ((type->t & VT_基本类型) == VT_函数)
            错误_打印("declaration of an array of functions");
        if ((type->t & VT_基本类型) == VT_无类型
            || 类型_大小(type, &unused_align) < 0)
            错误_打印("declaration of an array of incomplete type elements");

        t1 |= type->t & VT_变长数组;

        if (t1 & VT_变长数组) {
            if (n < 0)
              错误_打印("need explicit inner array size in VLAs");
            局部变量索引 -= 类型_大小(&int_type, &align);
            局部变量索引 &= -align;
            n = 局部变量索引;

            vla_运行时_类型_大小(type, &align);
            通用_操作('*');
            vset(&int_type, VT_LOCAL|VT_LVAL, n);
            vswap();
            将栈顶值_存储在堆栈左值();
        }
        if (n != -1)
            弹出堆栈值();
	不需要_代码生成 = saved_nocode_wanted;
                
        /* we push an anonymous symbol which will contain the array
           element type */
        s = 符号_压入栈(符号_字段, type, 0, n);
        type->t = (t1 ? VT_变长数组 : VT_数组) | VT_指针;
        type->ref = s;
    }
    return 1;
}

/* 解析类型声明符（基本类型除外），并以'type'返回类型。 'td'是一个位掩码，指示需要哪种类型的声明。
 *  “类型”应包含基本类型。 “ ad”是基本类型的属性定义。 可以通过类型_声明（）进行修改。
 *  如果此（可能是抽象的）声明符是一个指针链，则它返回指向类型的最里面的指针（等于* type，但是是一个不同的指针），
 *  否则返回类型本身，该类型用于递归调用。  */
static C类型 *类型_声明(C类型 *type, 属性定义 *ad, int *v, int td)
{
    C类型 *post, *ret;
    int qualifiers, storage;

    /* 递归类型，先删除存储位，然后再应用*/
    storage = type->t & VT_存储;
    type->t &= ~VT_存储;
    post = ret = type;

    while (单词编码 == '*') {
        qualifiers = 0;
    redo:
        带有宏替换的下个标记();
        switch(单词编码) {
        case 关键词_CONST1:
        case 关键词_CONST2:
        case 关键词_CONST3:
        case 关键词_常量:
            qualifiers |= VT_常量;
            goto redo;
        case 关键词_VOLATILE1:
        case 关键词_VOLATILE2:
        case 关键词_VOLATILE3:
        case 关键词_易变:
            qualifiers |= VT_易变;
            goto redo;
        case 关键词_RESTRICT1:
        case 关键词_RESTRICT2:
        case 关键词_RESTRICT3:
        case 关键词_限定:
            goto redo;
	/* XXX: clarify attribute handling */
	case 关键词_ATTRIBUTE1:
	case 关键词_ATTRIBUTE2:
	case 关键词_属性:
	    解析_属性(ad);
	    break;
        }
        修改类型_指针类型(type);
        type->t |= qualifiers;
	if (ret == type)
	    /* innermost pointed to type is the one for the first derivation */
	    ret = 指定的_类型(type);
    }

    if (单词编码 == '(') {
	/* This is possibly a parameter type list for abstract declarators
	   ('int ()'), use post_函数类型 for testing this.  */
	if (!post_函数类型(type, ad, 0, td)) {
	    /* It's not, so it's a nested declarator, and the post operations
	       apply to the innermost pointed to type (if any).  */
	    /* XXX: this is not correct to modify 'ad' at this point, but
	       the syntax is not clear */
	    解析_属性(ad);
	    post = 类型_声明(type, ad, v, td);
	    跳过(')');
	} else
	  goto abstract;
    } else if (单词编码 >= 符_识别 && (td & 类型_直接)) {
	/* type identifier */
	*v = 单词编码;
	带有宏替换的下个标记();
    } else {
  abstract:
	if (!(td & 类型_抽象))
	  应为("identifier");
	*v = 0;
    }
    post_函数类型(post, ad, storage, 0);
    解析_属性(ad);
    type->t |= storage;
    return ret;
}

/* 完全错误检查和绑定检查的间接访问 */
静态_函数 void 间接的(void)
{
    if ((栈顶值->type.t & VT_基本类型) != VT_指针) {
        if ((栈顶值->type.t & VT_基本类型) == VT_函数)
            return;
        应为("pointer");
    }
    if (栈顶值->r & VT_LVAL)
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
    栈顶值->type = *指定的_类型(&栈顶值->type);
    /* Arrays and functions are never lvalues */
    if (!(栈顶值->type.t & (VT_数组| VT_变长数组))
        && (栈顶值->type.t & VT_基本类型) != VT_函数) {
        栈顶值->r |= VT_LVAL;
        /* if bound checking, the referenced pointer must be checked */
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器)
            栈顶值->r |= VT_强制边界检查;
#endif
    }
}

/* 将参数传递给函数并进行类型检查和转换 */
static void 参数传给函数_并进行类型检查和转换(符号 *func, 符号 *arg)
{
    int func_type;
    C类型 type;

    func_type = func->f.func_type;
    if (func_type == 函数_旧 ||
        (func_type == 函数_省略 && arg == NULL)) {
        /* default casting : only need to convert float to double */
        if ((栈顶值->type.t & VT_基本类型) == VT_浮点) {
            通用_转换_s(VT_双精度);
        } else if (栈顶值->type.t & VT_位域) {
            type.t = 栈顶值->type.t & (VT_基本类型 | VT_无符号);
	    type.ref = 栈顶值->type.ref;
            通用_转换(&type);
        } else if (栈顶值->r & VT_强制转换) {
            强制_字符型短整型_转换();
        }
    } else if (arg == NULL) {
        错误_打印("too many arguments to function");
    } else {
        type = arg->type;
        type.t &= ~VT_常量; /* need to do that to avoid false warning */
        通用_指定类型_转换(&type);
    }
}

/*解析表达式并返回其类型，而没有任何副作用。 */
static void 解析表达式_返回类型(C类型 *type, void (*expr_fn)(void))
{
    不需要_代码生成++;
    expr_fn();
    *type = 栈顶值->type;
    弹出堆栈值();
    不需要_代码生成--;
}

/* 解析类型为'（type）'或'（expr）'的表达式并返回其类型 */
static void 解析_表达式_类型(C类型 *type)
{
    int n;
    属性定义 ad;

    跳过('(');
    if (解析_基本类型(type, &ad)) {
        类型_声明(type, &ad, &n, 类型_抽象);
    } else {
        解析表达式_返回类型(type, 通用表达式);
    }
    跳过(')');
}

static void 解析_指定类型(C类型 *type)
{
    属性定义 ad;
    int n;

    if (!解析_基本类型(type, &ad)) {
        应为("type");
    }
    类型_声明(type, &ad, &n, 类型_抽象);
}

static void 解析_内置_参数(int nc, const char *args)
{
    char c, sep = '(';
    C类型 t;
    if (nc)
        不需要_代码生成++;
    带有宏替换的下个标记();
    while ((c = *args++)) {
	跳过(sep);
	sep = ',';
	switch (c) {
	    case 'e': 等于_表达式(); continue;
	    case 't': 解析_指定类型(&t); 压入指定类型常量(&t); continue;
	    default: 错误_打印("internal error"); break;
	}
    }
    跳过(')');
    if (nc)
        不需要_代码生成--;
}

静态_函数 void 一元(void)
{
    int n, t, align, size, r, sizeof_caller;
    C类型 type;
    符号 *s;
    属性定义 ad;

    /* 生成行号信息 */
    if (zhi_状态->执行_调试)
        zhi_调试_行(zhi_状态);

    sizeof_caller = in_大小;
    in_大小 = 0;
    type.ref = NULL;
    /* XXX: GCC 2.95.3不会生成表，尽管这里应该更好 */
 tok_next:
    switch(单词编码) {
    case 关键词_EXTENSION:
        带有宏替换的下个标记();
        goto tok_next;
    case 常量_长字符型:
#ifdef ZHI_TARGET_PE
        t = VT_短整数|VT_无符号;
        goto push_tokc;
#endif
    case 常量_整数:
    case 常量_字符型: 
	t = VT_整数;
 push_tokc:
	type.t = t;
	vsetc(&type, VT_VC常量, &单词值);
        带有宏替换的下个标记();
        break;
    case 常量_无符整数:
        t = VT_整数 | VT_无符号;
        goto push_tokc;
    case 常量_长长整数:
        t = VT_长长整数;
	goto push_tokc;
    case 常量_无符长长整数:
        t = VT_长长整数 | VT_无符号;
	goto push_tokc;
    case 常量_浮点型:
        t = VT_浮点;
	goto push_tokc;
    case 常量_双精度:
        t = VT_双精度;
	goto push_tokc;
    case 常量_长双精度:
        t = VT_长双精度;
	goto push_tokc;
    case 常量_长整数:
        t = (LONG_SIZE == 8 ? VT_长长整数 : VT_整数) | VT_长整数;
	goto push_tokc;
    case 常量_无符长整数:
        t = (LONG_SIZE == 8 ? VT_长长整数 : VT_整数) | VT_长整数 | VT_无符号;
	goto push_tokc;
    case 符___FUNCTION__:
    case 符___FUNCTION___CN:
        if (!gnu_扩展)
            goto 标识符_识别ifier;
        /* fall thru */
    case 符___函数__:
    case 符___函数___CN:
        {
            void *ptr;
            int len;
            /* special function name identifier */
            len = strlen(函数名称) + 1;
            /* generate char[len] type */
            type.t = VT_字节;
            修改类型_指针类型(&type);
            type.t |= VT_数组;
            type.ref->c = len;
            将引用_推送到_节偏移量(&type, 初始化数据_部分, 初始化数据_部分->数据_偏移, len);
            if (!不需要_静态数据输出) {
                ptr = 段_ptr_添加(初始化数据_部分, len);
                memcpy(ptr, 函数名称, len);
            }
            带有宏替换的下个标记();
        }
        break;
    case 常量_长字符串:
#ifdef ZHI_TARGET_PE
        t = VT_短整数 | VT_无符号;
#else
        t = VT_整数;
#endif
        goto str_init;
    case 常量_字符串:
        /* string parsing */
        t = VT_字节;
        if (zhi_状态->字符串_无符号)
            t = VT_字节 | VT_无符号;
    str_init:
        if (zhi_状态->警告_写字符串)
            t |= VT_常量;
        type.t = t;
        修改类型_指针类型(&type);
        type.t |= VT_数组;
        memset(&ad, 0, sizeof(属性定义));
        声明_初始化_分配(&type, &ad, VT_VC常量, 2, 0, 0);
        break;
    case '(':
        带有宏替换的下个标记();
        /* cast ? */
        if (解析_基本类型(&type, &ad)) {
            类型_声明(&type, &ad, &n, 类型_抽象);
            跳过(')');
            /* check ISOC99 compound literal */
            if (单词编码 == '{') {
                    /* data is allocated locally by default */
                if (全局_分配复合字符)
                    r = VT_VC常量;
                else
                    r = VT_LOCAL;
                /* all except arrays are lvalues */
                if (!(type.t & VT_数组))
                    r |= VT_LVAL;
                memset(&ad, 0, sizeof(属性定义));
                声明_初始化_分配(&type, &ad, r, 1, 0, 0);
            } else {
                if (sizeof_caller) {
                    压入指定类型常量(&type);
                    return;
                }
                一元();
                通用_转换(&type);
            }
        } else if (单词编码 == '{') {
	    int saved_nocode_wanted = 不需要_代码生成;
            if (需要_常量 && !(不需要_代码生成 & 未评估的子表达式))
                错误_打印("expected constant");
            /* save all registers */
            保存_寄存器最多n个堆栈条目(0);
            /* statement expression : we do not accept break/continue
               inside as GCC does.  We do retain the 不需要_代码生成 state,
	       as statement expressions can't ever be entered from the
	       outside, so any reactivation of code emission (from labels
	       or loop heads) can be disabled again after the end of it. */
            块(1);
	    不需要_代码生成 = saved_nocode_wanted;
            跳过(')');
        } else {
            通用表达式();
            跳过(')');
        }
        break;
    case '*':
        带有宏替换的下个标记();
        一元();
        间接的();
        break;
    case '&':
        带有宏替换的下个标记();
        一元();
        /* functions names must be treated as function pointers,
           except for 一元 '&' and sizeof. Since we consider that
           functions are not lvalues, we only have to handle it
           there and in function calls. */
        /* arrays can also be used although they are not lvalues */
        if ((栈顶值->type.t & VT_基本类型) != VT_函数 &&
            !(栈顶值->type.t & VT_数组))
            测试_左值();
        if (栈顶值->sym)
          栈顶值->sym->a.addrtaken = 1;
        修改类型_指针类型(&栈顶值->type);
        获取栈顶值地址();
        break;
    case '!':
        带有宏替换的下个标记();
        一元();
        生成_测试_零(双符号_等于);
        break;
    case '~':
        带有宏替换的下个标记();
        一元();
        压入整数常量(-1);
        通用_操作('^');
        break;
    case '+':
        带有宏替换的下个标记();
        一元();
        if ((栈顶值->type.t & VT_基本类型) == VT_指针)
            错误_打印("pointer not accepted for 一元 plus");
        /* In order to force cast, we add zero, except for floating point
	   where we really need an noop (otherwise -0.0 will be transformed
	   into +0.0).  */
	if (!是_浮点型(栈顶值->type.t)) {
	    压入整数常量(0);
	    通用_操作('+');
	}
        break;
    case 关键词_SIZEOF:
    case 关键词_获取大小:
    case 关键词_ALIGNOF1:
    case 关键词_ALIGNOF2:
    case 关键词_ALIGNOF3:
	case 关键词_对齐:
        t = 单词编码;
        带有宏替换的下个标记();
        in_大小++;
        解析表达式_返回类型(&type, 一元); /* Perform a in_大小 = 0; */
        s = NULL;
        if (栈顶值[1].r & VT_符号)
            s = 栈顶值[1].sym; /* hack: accessing previous 栈顶值 */
        size = 类型_大小(&type, &align);
        if (s && s->a.aligned)
            align = 1 << (s->a.aligned - 1);
        if (t == 关键词_SIZEOF || t == 关键词_获取大小) {
            if (!(type.t & VT_变长数组)) {
                if (size < 0)
                    错误_打印("sizeof applied to an incomplete type");
                压入目标地址类型常量(size);
            } else {
                vla_运行时_类型_大小(&type, &align);
            }
        } else {
            压入目标地址类型常量(align);
        }
        栈顶值->type.t |= VT_无符号;
        break;

    case 符_builtin_expect:
	/* __builtin_expect is a no-op for now */
	解析_内置_参数(0, "ee");
	弹出堆栈值();
        break;
    case 符_builtin_types_compatible_p:
	解析_内置_参数(0, "tt");
	栈顶值[-1].type.t &= ~(VT_常量 | VT_易变);
	栈顶值[0].type.t &= ~(VT_常量 | VT_易变);
	n = 是_兼容_类型(&栈顶值[-1].type, &栈顶值[0].type);
	栈顶值 -= 2;
	压入整数常量(n);
        break;
    case 符_builtin_choose_expr:
	{
	    int64_t c;
	    带有宏替换的下个标记();
	    跳过('(');
	    c = 表达式_常量64();
	    跳过(',');
	    if (!c) {
		不需要_代码生成++;
	    }
	    等于_表达式();
	    if (!c) {
		弹出堆栈值();
		不需要_代码生成--;
	    }
	    跳过(',');
	    if (c) {
		不需要_代码生成++;
	    }
	    等于_表达式();
	    if (c) {
		弹出堆栈值();
		不需要_代码生成--;
	    }
	    跳过(')');
	}
        break;
    case 符_builtin_constant_p:
	解析_内置_参数(1, "e");
	n = (栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;
	栈顶值--;
	压入整数常量(n);
        break;
    case 符_builtin_frame_address:
    case 符_builtin_return_address:
        {
            int tok1 = 单词编码;
            int level;
            带有宏替换的下个标记();
            跳过('(');
            if (单词编码 != 常量_整数) {
                错误_打印("%s 只接受正整数",
                          tok1 == 符_builtin_return_address ?
                          "__builtin_return_address" :
                          "__builtin_frame_address");
            }
            level = (uint32_t)单词值.i;
            带有宏替换的下个标记();
            跳过(')');
            type.t = VT_无类型;
            修改类型_指针类型(&type);
            vset(&type, VT_LOCAL, 0);       /* 局部框架 */
            while (level--) {
#ifdef ZHI_TARGET_RISCV64
                压入整数常量(2*指针_大小);
                通用_操作('-');
#endif
                修改类型_指针类型(&栈顶值->type);
                间接的();                    /* -> 父框架 */
            }
            if (tok1 == 符_builtin_return_address) {
                // 假定返回地址刚好在堆栈上的帧指针上方
#ifdef ZHI_TARGET_ARM
                压入整数常量(2*指针_大小);
                通用_操作('+');
#elif defined ZHI_TARGET_RISCV64
                压入整数常量(指针_大小);
                通用_操作('-');
#else
                压入整数常量(指针_大小);
                通用_操作('+');
#endif
                修改类型_指针类型(&栈顶值->type);
                间接的();
            }
        }
        break;
#ifdef ZHI_TARGET_RISCV64
    case 符_builtin_va_start:
        解析_内置_参数(0, "ee");
        r = 栈顶值->r & VT_值掩码;
        if (r == VT_LLOCAL)
            r = VT_LOCAL;
        if (r != VT_LOCAL)
            错误_打印("__builtin_va_start expects a local variable");
        生成_va_开始();
	将栈顶值_存储在堆栈左值();
        break;
#endif
#ifdef ZHI_TARGET_X86_64
#ifdef ZHI_TARGET_PE
    case 符_builtin_va_start:
	解析_内置_参数(0, "ee");
        r = 栈顶值->r & VT_值掩码;
        if (r == VT_LLOCAL)
            r = VT_LOCAL;
        if (r != VT_LOCAL)
            错误_打印("__builtin_va_start expects a local variable");
        栈顶值->r = r;
	栈顶值->type = 字符_指针_类型;
	栈顶值->c.i += 8;
	将栈顶值_存储在堆栈左值();
        break;
#else
    case 符_builtin_va_arg_types:
	解析_内置_参数(0, "t");
	压入整数常量(分类_x86_64_va_arg(&栈顶值->type));
	vswap();
	弹出堆栈值();
	break;
#endif
#endif

#ifdef ZHI_TARGET_ARM64
    case 符_builtin_va_start: {
	解析_内置_参数(0, "ee");
        //xx check types
        生成_va_开始();
        压入整数常量(0);
        栈顶值->type.t = VT_无类型;
        break;
    }
    case 符_builtin_va_arg: {
	解析_内置_参数(0, "et");
	type = 栈顶值->type;
	弹出堆栈值();
        //xx check types
        生成_va_参数(&type);
        栈顶值->type = type;
        break;
    }
    case 符___arm64_clear_cache: {
	解析_内置_参数(0, "ee");
        生成_清理_缓存();
        压入整数常量(0);
        栈顶值->type.t = VT_无类型;
        break;
    }
#endif
    /* pre operations */
    case 双符号_自加1:
    case 双符号_自减1:
        t = 单词编码;
        带有宏替换的下个标记();
        一元();
        inc(0, t);
        break;
    case '-':
        带有宏替换的下个标记();
        一元();
        t = 栈顶值->type.t & VT_基本类型;
	if (是_浮点型(t)) {
            /* In IEEE negate(x) isn't subtract(0,x), but rather
	       subtract(-0, x).  */
	    压入指定类型常量(&栈顶值->type);
	    if (t == VT_浮点)
	        栈顶值->c.f = -1.0 * 0.0;
	    else if (t == VT_双精度)
	        栈顶值->c.d = -1.0 * 0.0;
            else
	        栈顶值->c.ld = -1.0 * 0.0;
	} else
	    压入整数常量(0);
	vswap();
	通用_操作('-');
        break;
    case 双符号_逻辑与:
        if (!gnu_扩展)
            goto 标识符_识别ifier;
        带有宏替换的下个标记();
        /* allow to take the address of a label */
        if (单词编码 < 符_没识别)
            应为("label identifier");
        s = 标签_查找(单词编码);
        if (!s) {
            s = 标签_推送(&全局符号_标签_堆栈, 单词编码, 标签_正向定义);
        } else {
            if (s->r == 标签_被声明)
                s->r = 标签_正向定义;
        }
        if (!s->type.t) {
            s->type.t = VT_无类型;
            修改类型_指针类型(&s->type);
            s->type.t |= VT_静态;
        }
        压入符号值(&s->type, s);
        带有宏替换的下个标记();
        break;

    case 关键词_GENERIC:
    {
	C类型 controlling_type;
	int has_default = 0;
	int has_match = 0;
	int learn = 0;
	单词字符串 *str = NULL;
	int saved_const_wanted = 需要_常量;

	带有宏替换的下个标记();
	跳过('(');
	需要_常量 = 0;
	解析表达式_返回类型(&controlling_type, 等于_表达式);
	controlling_type.t &= ~(VT_常量 | VT_易变 | VT_数组);
	if ((controlling_type.t & VT_基本类型) == VT_函数)
	  修改类型_指针类型(&controlling_type);
	需要_常量 = saved_const_wanted;
	for (;;) {
	    learn = 0;
	    跳过(',');
	    if (单词编码 == 关键词_DEFAULT || 单词编码 == 关键词_默认) {
		if (has_default)
		    错误_打印("太多的‘默认’");
		has_default = 1;
		if (!has_match)
		    learn = 1;
		带有宏替换的下个标记();
	    } else {
	        属性定义 ad_tmp;
		int itmp;
	        C类型 cur_type;

                in_通用++;
		解析_基本类型(&cur_type, &ad_tmp);
                in_通用--;

		类型_声明(&cur_type, &ad_tmp, &itmp, 类型_抽象);
		if (比较_2个类型(&controlling_type, &cur_type, 0)) {
		    if (has_match) {
		      错误_打印("类型匹配两次");
		    }
		    has_match = 1;
		    learn = 1;
		}
	    }
	    跳过(':');
	    if (learn) {
		if (str)
		    单词字符串_释放(str);
		跳过_或_保存_块(&str);
	    } else {
		跳过_或_保存_块(NULL);
	    }
	    if (单词编码 == ')')
		break;
	}
	if (!str) {
	    char buf[60];
	    字符串_的_类型(buf, sizeof buf, &controlling_type, NULL);
	    错误_打印("类型 '%s' 与任何关联都不匹配", buf);
	}
	开始_宏(str, 1);
	带有宏替换的下个标记();
	等于_表达式();
	if (单词编码 != 符_文件结尾)
	    应为(",");
	结束_宏();
        带有宏替换的下个标记();
	break;
    }
    // special qnan , snan and infinity values
    case 符___NAN__:
        n = 0x7fc00000;
special_math_val:
	压入整数常量(n);
	栈顶值->type.t = VT_浮点;
        带有宏替换的下个标记();
        break;
    case 符___SNAN__:
	n = 0x7f800001;
	goto special_math_val;
    case 符___INF__:
	n = 0x7f800000;
	goto special_math_val;

    default:
    标识符_识别ifier:
        t = 单词编码;
        带有宏替换的下个标记();
        if (t < 符_没识别)
            应为("identifier");
        s = 符号_查询(t);
        if (!s || 是_汇编_符号(s)) {
            const char *name = 取_单词字符串(t, NULL);
            if (单词编码 != '(')
                错误_打印("'%s' 未声明", name);
            /* for simple function calls, we tolerate undeclared
               external reference to int() function */
            if (zhi_状态->警告_隐式函数声明
#ifdef ZHI_TARGET_PE
                /* people must be warned about using undeclared WINAPI functions
                   (which usually start with uppercase letter) */
                || (name[0] >= 'A' && name[0] <= 'Z')
#endif
            )
                zhi_警告("函数 '%s'是隐式声明", name);
            s = 外部_全局_符号(t, &函数_旧_类型);
        }

        r = s->r;
        /* A symbol that has a register is a local register variable,
           which starts out as VT_LOCAL value.  */
        if ((r & VT_值掩码) < VT_VC常量)
            r = (r & ~VT_值掩码) | VT_LOCAL;

        vset(&s->type, r, s->c);
        /* Point to s as backpointer (even without r&VT_符号).
	   Will be used by at least the x86 inline asm parser for
	   regvars.  */
	栈顶值->sym = s;

        if (r & VT_符号) {
            栈顶值->c.i = 0;
        } else if (r == VT_VC常量 && 是_枚举_变长(s->type.t)) {
            栈顶值->c.i = s->enum_val;
        }
        break;
    }
    
    /* post operations */
    while (1) {
        if (单词编码 == 双符号_自加1 || 单词编码 == 双符号_自减1) {
            inc(1, 单词编码);
            带有宏替换的下个标记();
        } else if (单词编码 == '.' || 单词编码 == 双符号_结构体指针运算符 || 单词编码 == 常量_双精度) {
            int qualifiers, cumofs = 0;
            /* field */ 
            if (单词编码 == 双符号_结构体指针运算符) 
                间接的();
            qualifiers = 栈顶值->type.t & (VT_常量 | VT_易变);
            测试_左值();
            获取栈顶值地址();
            /* 应为 pointer on structure */
            if ((栈顶值->type.t & VT_基本类型) != VT_结构体)
                应为("struct or union");
            if (单词编码 == 常量_双精度)
                应为("field name");
            带有宏替换的下个标记();
            if (单词编码 == 常量_整数 || 单词编码 == 常量_无符整数)
                应为("field name");
	    s = 查找_域(&栈顶值->type, 单词编码, &cumofs);
            if (!s)
                错误_打印("找不到字段: %s",  取_单词字符串(单词编码 & ~符号_字段, &单词值));
            /* add field offset to pointer */
            栈顶值->type = 字符_指针_类型; /* change type to 'char *' */
            压入整数常量(cumofs + s->c);
            通用_操作('+');
            /* change type to field type, and set to lvalue */
            栈顶值->type = s->type;
            栈顶值->type.t |= qualifiers;
            /* an array is never an lvalue */
            if (!(栈顶值->type.t & VT_数组)) {
                栈顶值->r |= VT_LVAL;
#ifdef 配置_ZHI_边界检查
                /* if bound checking, the referenced pointer must be checked */
                if (zhi_状态->执行_边界_检查器)
                    栈顶值->r |= VT_强制边界检查;
#endif
            }
            带有宏替换的下个标记();
        } else if (单词编码 == '[') {
            带有宏替换的下个标记();
            通用表达式();
            通用_操作('+');
            间接的();
            跳过(']');
        } else if (单词编码 == '(') {
            堆栈值 ret;
            符号 *sa;
            int 数量_args, ret_nregs, ret_align, regsize, variadic;

            /* function call  */
            if ((栈顶值->type.t & VT_基本类型) != VT_函数) {
                /* pointer test (no array accepted) */
                if ((栈顶值->type.t & (VT_基本类型 | VT_数组)) == VT_指针) {
                    栈顶值->type = *指定的_类型(&栈顶值->type);
                    if ((栈顶值->type.t & VT_基本类型) != VT_函数)
                        goto 错误_函数;
                } else {
                错误_函数:
                    应为("函数 指针");
                }
            } else {
                栈顶值->r &= ~VT_LVAL; /* no lvalue */
            }
            /* get return type */
            s = 栈顶值->type.ref;
            带有宏替换的下个标记();
            sa = s->next; /* first parameter */
            数量_args = regsize = 0;
            ret.r2 = VT_VC常量;
            /* compute first implicit argument if a structure is returned */
            if ((s->type.t & VT_基本类型) == VT_结构体) {
                variadic = (s->f.func_type == 函数_省略);
                ret_nregs = gfunc_sret(&s->type, variadic, &ret.type,
                                       &ret_align, &regsize);
                if (ret_nregs <= 0) {
                    /* get some space for the returned structure */
                    size = 类型_大小(&s->type, &align);
#ifdef ZHI_TARGET_ARM64
                /* On arm64, a small struct is return in registers.
                   It is much easier to write it to memory if we know
                   that we are allowed to write some extra bytes, so
                   round the allocated space up to a power of 2: */
                if (size < 16)
                    while (size & (size - 1))
                        size = (size | (size - 1)) + 1;
#endif
                    局部变量索引 = (局部变量索引 - size) & -align;
                    ret.type = s->type;
                    ret.r = VT_LOCAL | VT_LVAL;
                    /* pass it as 'int' to avoid structure arg passing
                       problems */
                    vseti(VT_LOCAL, 局部变量索引);
                    ret.c = 栈顶值->c;
                    if (ret_nregs < 0)
                      栈顶值--;
                    else
                      数量_args++;
                }
            } else {
                ret_nregs = 1;
                ret.type = s->type;
            }

            if (ret_nregs > 0) {
                /* return in register */
                ret.c.i = 0;
                将函数_返回寄存器_放入堆栈值(&ret, ret.type.t);
            }
            if (单词编码 != ')') {
                for(;;) {
                    等于_表达式();
                    参数传给函数_并进行类型检查和转换(s, sa);
                    数量_args++;
                    if (sa)
                        sa = sa->next;
                    if (单词编码 == ')')
                        break;
                    跳过(',');
                }
            }
            if (sa)
                错误_打印("函数的参数太少");
            跳过(')');
#ifdef 配置_ZHI_边界检查
            if (zhi_状态->执行_边界_检查器 &&
                (数量_args == 1 || 数量_args == 2) &&
                (栈顶值[-数量_args].r & VT_符号) &&
                (栈顶值[-数量_args].sym->v == 符_setjmp ||
                 栈顶值[-数量_args].sym->v == 符__setjmp
#ifndef ZHI_TARGET_PE
                 || 栈顶值[-数量_args].sym->v == 符_sigsetjmp
                 || 栈顶值[-数量_args].sym->v == 符___sigsetjmp
#endif
                )) {
                推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_setjmp);
                vpushv(栈顶值 - 数量_args);
                if (数量_args == 2)
                    vpushv(栈顶值 - 数量_args);
                具体地址函数_调用(数量_args);
            }
#endif
            具体地址函数_调用(数量_args);

            if (ret_nregs < 0) {
                vsetc(&ret.type, ret.r, &ret.c);
#ifdef ZHI_TARGET_RISCV64
                arch_transfer_ret_regs(1);
#endif
            } else {
                /* return value */
                for (r = ret.r + ret_nregs + !ret_nregs; r-- > ret.r;) {
                    vsetc(&ret.type, r, &ret.c);
                    栈顶值->r2 = ret.r2; /* Loop only happens when r2 is VT_VC常量 */
                }

                /* handle packed struct return */
                if (((s->type.t & VT_基本类型) == VT_结构体) && ret_nregs) {
                    int addr, offset;

                    size = 类型_大小(&s->type, &align);
                    /* We're writing whole regs often, make sure there's enough
                       space.  Assume register size is power of 2.  */
                    if (regsize > align)
                      align = regsize;
                    局部变量索引 = (局部变量索引 - size) & -align;
                    addr = 局部变量索引;
                    offset = 0;
                    for (;;) {
                        vset(&ret.type, VT_LOCAL | VT_LVAL, addr + offset);
                        vswap();
                        将栈顶值_存储在堆栈左值();
                        栈顶值--;
                        if (--ret_nregs == 0)
                          break;
                        offset += regsize;
                    }
                    vset(&s->type, VT_LOCAL | VT_LVAL, addr);
                }

                /* Promote char/short return values. This is matters only
                   for calling function that were not compiled by ZHI and
                   only on some architectures.  For those where it doesn't
                   matter we 应为 things to be already promoted to int,
                   but not larger.  */
                t = s->type.t & VT_基本类型;
                if (t == VT_字节 || t == VT_短整数 || t == VT_逻辑) {
#ifdef PROMOTE_RET
                    栈顶值->r |= BFVAL(VT_强制转换, 1);
#else
                    栈顶值->type.t = VT_整数;
#endif
                }
            }
            if (s->f.func_noreturn)
                代码_关();
        } else {
            break;
        }
    }
}

#ifndef 优先级_解析器 /* 原始的自上而下的解析器 */

static void 表达式_prod(void)
{
    int t;

    一元();
    while ((t = 单词编码) == '*' || t == '/' || t == '%') {
        带有宏替换的下个标记();
        一元();
        通用_操作(t);
    }
}

static void 表达式_sum(void)
{
    int t;

    表达式_prod();
    while ((t = 单词编码) == '+' || t == '-') {
        带有宏替换的下个标记();
        表达式_prod();
        通用_操作(t);
    }
}

static void 表达式_转换(void)
{
    int t;

    表达式_sum();
    while ((t = 单词编码) == 双符号_左位移 || t == 双符号_右位移) {
        带有宏替换的下个标记();
        表达式_sum();
        通用_操作(t);
    }
}

static void 表达式_cmp(void)
{
    int t;

    表达式_转换();
    while (((t = 单词编码) >= 符号_ULE && t <= 符_GT) ||t == 符号_ULT || t == 符号_UGE)
    {
        带有宏替换的下个标记();
        表达式_转换();
        通用_操作(t);
    }
}

static void 表达式_cmpeq(void)
{
    int t;

    表达式_cmp();
    while ((t = 单词编码) == 双符号_等于 || t == 双符号_不等于) {
        带有宏替换的下个标记();
        表达式_cmp();
        通用_操作(t);
    }
}

static void 表达式_位与(void)
{
    表达式_cmpeq();
    while (单词编码 == '&') {
        带有宏替换的下个标记();
        表达式_cmpeq();
        通用_操作('&');
    }
}

static void 表达式_异或(void)
{
    表达式_位与();
    while (单词编码 == '^') {
        带有宏替换的下个标记();
        表达式_位与();
        通用_操作('^');
    }
}

static void 表达式_位或(void)
{
    表达式_异或();
    while (单词编码 == '|') {
        带有宏替换的下个标记();
        表达式_异或();
        通用_操作('|');
    }
}

static void 表达式_逻辑与或(int op);

static void 表达式_逻辑与(void)
{
    表达式_位或();
    if (单词编码 == 双符号_逻辑与)
        表达式_逻辑与或(单词编码);
}

static void 表达式_逻辑或(void)
{
    表达式_逻辑与();
    if (单词编码 == 双符号_逻辑或)
        表达式_逻辑与或(单词编码);
}

# define 表达式_逻辑与或_下一个(op) op == 双符号_逻辑与 ? 表达式_位或() : 表达式_逻辑与()
#else /* defined 优先级_解析器 */
# define 表达式_逻辑与或_下一个(op) 一元(), 表达式_中缀(优先权(op) + 1)
# define 表达式_逻辑或() 一元(), 表达式_中缀(1)

static int 优先权(int 单词编码)
{
    switch (单词编码) {
        case 双符号_逻辑或: return 1;
        case 双符号_逻辑与: return 2;
	case '|': return 3;
	case '^': return 4;
	case '&': return 5;
	case 双符号_等于: case 双符号_不等于: return 6;
 relat: case 符号_ULT: case 符号_UGE: return 7;
	case 双符号_左位移: case 双符号_右位移: return 8;
	case '+': case '-': return 9;
	case '*': case '/': case '%': return 10;
	default:
	    if (单词编码 >= 符号_ULE && 单词编码 <= 符_GT)
	        goto relat;
	    return 0;
    }
}
static unsigned char 优先权数组[256];
static void 初始化_优先权(void)
{
    int i;
    for (i = 0; i < 256; i++)
	优先权数组[i] = 优先权(i);
}
#define 优先权(i) ((unsigned)i < 256 ? 优先权数组[i] : 0)

static void 表达式_逻辑与或(int op);

static void 表达式_中缀(int p)
{
    int t = 单词编码, p2;
    while ((p2 = 优先权(t)) >= p) {
        if (t == 双符号_逻辑或 || t == 双符号_逻辑与) {
            表达式_逻辑与或(t);
        } else {
            带有宏替换的下个标记();
            一元();
            if (优先权(单词编码) > p2)
              表达式_中缀(p2 + 1);
            通用_操作(t);
        }
        t = 单词编码;
    }
}
#endif

/* 假设栈顶值是在条件上下文中使用的值（即与零比较），如果为假，则返回0;如果为true，则返回1，如果不能静态确定，
 * 则返回-1。  */
static int 条件_3种返回值(void)
{
    int c = -1;
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量 &&
	(!(栈顶值->r & VT_符号) || !栈顶值->sym->a.weak)) {
	vdup();
        通用_转换_s(VT_逻辑);
	c = 栈顶值->c.i;
	弹出堆栈值();
    }
    return c;
}

static void 表达式_逻辑与或(int op)
{
    int t = 0, cc = 1, f = 0, i = op == 双符号_逻辑与, c;
    for(;;) {
        c = f ? i : 条件_3种返回值();
        if (c < 0)
            保存_寄存器最多n个堆栈条目(1), cc = 0;
        else if (c != i)
            不需要_代码生成++, f = 1;
        if (单词编码 != op)
            break;
        if (c < 0)
            t = 生成值测试(i, t);
        else
            弹出堆栈值();
        带有宏替换的下个标记();
        表达式_逻辑与或_下一个(op);
    }
    if (cc || f) {
        弹出堆栈值();
        压入整数常量(i ^ f);
        生成符号(t);
        不需要_代码生成 -= f;
    } else {
        gvtst_set(i, t);
    }
}

static int 是_条件_布尔(堆栈值 *sv)
{
    if ((sv->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量
        && (sv->type.t & VT_基本类型) == VT_整数)
        return (unsigned)sv->c.i < 2;
    if (sv->r == VT_CMP)
        return 1;
    return 0;
}

static void 条件_表达式(void)
{
    int tt, u, r1, r2, rc, t1, t2, islv, c, g;
    堆栈值 sv;
    C类型 type;
    int ncw_prev;

    表达式_逻辑或();
    if (单词编码 == '?') {
        带有宏替换的下个标记();
	c = 条件_3种返回值();
        g = (单词编码 == ':' && gnu_扩展);
        tt = 0;
        if (!g) {
            if (c < 0) {
                保存_寄存器最多n个堆栈条目(1);
                tt = 生成值测试(1, 0);
            } else {
                弹出堆栈值();
            }
        } else if (c < 0) {
            /* needed to avoid having different registers saved in
               each branch */
            保存_寄存器最多n个堆栈条目(1);
            堆栈转为寄存器并复制到另一个寄存器();
            tt = 生成值测试(0, 0);
        }

        ncw_prev = 不需要_代码生成;
        if (c == 0)
          不需要_代码生成++;
        if (!g)
          通用表达式();

        if (c < 0 && 栈顶值->r == VT_CMP) {
            t1 = 生成值测试(0, 0);
            压入整数常量(0);
            gvtst_set(0, t1);
            将rc寄存器值存储在栈顶值中(寄存器类_整数);
        }

        if ((栈顶值->type.t & VT_基本类型) == VT_函数)
          修改类型_指针类型(&栈顶值->type);
        sv = *栈顶值; /* save value to handle it later */
        栈顶值--; /* no 弹出堆栈值 so that FP stack is not flushed */

        if (g) {
            u = tt;
        } else if (c < 0) {
            u = 生成跳转到标签(0);
            生成符号(tt);
        } else
          u = 0;

        不需要_代码生成 = ncw_prev;
        if (c == 1)
          不需要_代码生成++;
        跳过(':');
        条件_表达式();

        if (c < 0 && 是_条件_布尔(栈顶值) && 是_条件_布尔(&sv)) {
            if (sv.r == VT_CMP) {
                t1 = sv.jtrue;
                t2 = u;
            } else {
                t1 = 生成值测试(0, 0);
                t2 = 生成跳转到标签(0);
                生成符号(u);
                vpushv(&sv);
            }
            gvtst_set(0, t1);
            gvtst_set(1, t2);
            不需要_代码生成 = ncw_prev;
            //  zhi_警告("two conditions 条件_表达式");
            return;
        }

        if ((栈顶值->type.t & VT_基本类型) == VT_函数)
          修改类型_指针类型(&栈顶值->type);

        /* cast operands to correct type according to ISOC rules */
        if (!结合_类型(&type, &sv, 栈顶值, '?'))
          类型_不兼容_错误(&sv.type, &栈顶值->type,
            "type mismatch in conditional expression (have '%s' and '%s')");
        /* keep structs lvalue by transforming `(expr ? a : b)` to `*(expr ? &a : &b)` so
           that `(expr ? a : b).mem` does not error  with "lvalue expected" */
        islv = (栈顶值->r & VT_LVAL) && (sv.r & VT_LVAL) && VT_结构体 == (type.t & VT_基本类型);

        /* now we convert second operand */
        if (c != 1) {
            通用_转换(&type);
            if (islv) {
                修改类型_指针类型(&栈顶值->type);
                获取栈顶值地址();
            } else if (VT_结构体 == (栈顶值->type.t & VT_基本类型))
              获取栈顶值地址();
        }

        rc = 返回类型t的_通用寄存器类(type.t);
        /* for long longs, we use fixed registers to avoid having
           to handle a complicated move */
        if (使用_两个单词的_类型(type.t))
          rc = 返回类型t的函数_返回寄存器(type.t);

        tt = r2 = 0;
        if (c < 0) {
            r2 = 将rc寄存器值存储在栈顶值中(rc);
            tt = 生成跳转到标签(0);
        }
        生成符号(u);
        不需要_代码生成 = ncw_prev;

        /* this is horrible, but we must also convert first
           operand */
        if (c != 0) {
            *栈顶值 = sv;
            通用_转换(&type);
            if (islv) {
                修改类型_指针类型(&栈顶值->type);
                获取栈顶值地址();
            } else if (VT_结构体 == (栈顶值->type.t & VT_基本类型))
              获取栈顶值地址();
        }

        if (c < 0) {
            r1 = 将rc寄存器值存储在栈顶值中(rc);
            移动_寄存器(r2, r1, islv ? VT_指针 : type.t);
            栈顶值->r = r2;
            生成符号(tt);
        }

        if (islv)
          间接的();
    }
}

static void 等于_表达式(void)
{
    int t;
    
    条件_表达式();
    if ((t = 单词编码) == '=' || 符_ASSIGN(t)) {
        测试_左值();
        带有宏替换的下个标记();
        if (t == '=') {
            等于_表达式();
        } else {
            vdup();
            等于_表达式();
            通用_操作(符_ASSIGN_OP(t));
        }
        将栈顶值_存储在堆栈左值();
    }
}

静态_函数 void 通用表达式(void)
{
    while (1) {
        等于_表达式();
        if (单词编码 != ',')
            break;
        弹出堆栈值();
        带有宏替换的下个标记();
    }
}

/* 解析一个常量表达式并在栈顶值中返回值。  */
static void 表达式_常量1(void)
{
    需要_常量++;
    不需要_代码生成 += 未评估的子表达式 + 1;
    条件_表达式();
    不需要_代码生成 -= 未评估的子表达式 + 1;
    需要_常量--;
}

/* parse an integer constant and return its value. */
static inline int64_t 表达式_常量64(void)
{
    int64_t c;
    表达式_常量1();
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) != VT_VC常量)
        应为("constant expression");
    c = 栈顶值->c.i;
    弹出堆栈值();
    return c;
}

/* parse an integer constant and return its value.
   Complain if it doesn't fit 32bit (signed or unsigned).  */
静态_函数 int 表达式_常量(void)
{
    int c;
    int64_t wc = 表达式_常量64();
    c = wc;
    if (c != wc && (unsigned)c != wc)
        错误_打印("constant exceeds 32 bit");
    return c;
}

/* ------------------------------------------------------------------------- */
/* return from function */

#ifndef ZHI_TARGET_ARM64
static void 生成函数_返回值(C类型 *func_type)
{
    if ((func_type->t & VT_基本类型) == VT_结构体) {
        C类型 type, ret_type;
        int ret_align, ret_nregs, regsize;
        ret_nregs = gfunc_sret(func_type, 当前函数_可变参数, &ret_type,
                               &ret_align, &regsize);
        if (ret_nregs < 0) {
#ifdef ZHI_TARGET_RISCV64
            arch_transfer_ret_regs(0);
#endif
        } else if (0 == ret_nregs) {
            /* if returning structure, must copy it to implicit
               first pointer arg 位置 */
            type = *func_type;
            修改类型_指针类型(&type);
            vset(&type, VT_LOCAL | VT_LVAL, 函数_vc);
            间接的();
            vswap();
            /* copy structure value to pointer */
            将栈顶值_存储在堆栈左值();
        } else {
            /* returning structure packed into registers */
            int size, addr, align, rc;
            size = 类型_大小(func_type,&align);
            if ((栈顶值->r != (VT_LOCAL | VT_LVAL) ||
                 (栈顶值->c.i & (ret_align-1)))
                && (align & (ret_align-1))) {
                局部变量索引 = (局部变量索引 - size) & -ret_align;
                addr = 局部变量索引;
                type = *func_type;
                vset(&type, VT_LOCAL | VT_LVAL, addr);
                vswap();
                将栈顶值_存储在堆栈左值();
                弹出堆栈值();
                vset(&ret_type, VT_LOCAL | VT_LVAL, addr);
            }
            栈顶值->type = ret_type;
            rc = 返回类型t的函数_返回寄存器(ret_type.t);
            if (ret_nregs == 1)
                将rc寄存器值存储在栈顶值中(rc);
            else {
                for (;;) {
                    vdup();
                    将rc寄存器值存储在栈顶值中(rc);
                    弹出堆栈值();
                    if (--ret_nregs == 0)
                      break;
                    /* We assume that when a structure is returned in multiple
                       registers, their classes are consecutive values of the
                       suite s(n) = 2^n */
                    rc <<= 1;
                    栈顶值->c.i += regsize;
                }
            }
        }
    } else {
        将rc寄存器值存储在栈顶值中(返回类型t的函数_返回寄存器(func_type->t));
    }
    栈顶值--; /* NOT 弹出堆栈值() because on x86 it would flush the fp stack */
}
#endif

static void 检查_函数_返回值(void)
{
    if ((当前函数_返回类型.t & VT_基本类型) == VT_无类型)
        return;
    if ((!strcmp (函数名称, "main")||!strcmp (函数名称, "主函数"))&& (当前函数_返回类型.t & VT_基本类型) == VT_整数)
    {
        /* main默认返回0 */
        压入整数常量(0);
        通用_指定类型_转换(&当前函数_返回类型);
        生成函数_返回值(&当前函数_返回类型);
    } else
    {
        zhi_警告("函数可能不返回任何值: '%s'", 函数名称);
    }
}

/* ------------------------------------------------------------------------- */
/* switch/case */

static int 分支_对比(const void *pa, const void *pb)
{
    int64_t a = (*(struct 分支_t**) pa)->v1;
    int64_t b = (*(struct 分支_t**) pb)->v1;
    return a < b ? -1 : a > b;
}

static void gtst_addr(int t, int a)
{
    生成符号_地址(生成值测试(0, t), a);
}

static void 生成分支(struct 分支_t **base, int len, int *bsym)
{
    struct 分支_t *p;
    int e;
    int ll = (栈顶值->type.t & VT_基本类型) == VT_长长整数;
    while (len > 8) {
        /* binary search */
        p = base[len/2];
        vdup();
	if (ll)
	    压入长长整数(p->v2);
	else
	    压入整数常量(p->v2);
        通用_操作(双符号_小于等于);
        e = 生成值测试(1, 0);
        vdup();
	if (ll)
	    压入长长整数(p->v1);
	else
	    压入整数常量(p->v1);
        通用_操作(双符号_大于等于);
        gtst_addr(0, p->sym); /* v1 <= x <= v2 */
        /* x < v1 */
        生成分支(base, len/2, bsym);
        /* x > v2 */
        生成符号(e);
        e = len/2 + 1;
        base += e; len -= e;
    }
    /* linear scan */
    while (len--) {
        p = *base++;
        vdup();
	if (ll)
	    压入长长整数(p->v2);
	else
	    压入整数常量(p->v2);
        if (p->v1 == p->v2) {
            通用_操作(双符号_等于);
            gtst_addr(0, p->sym);
        } else {
            通用_操作(双符号_小于等于);
            e = 生成值测试(1, 0);
            vdup();
	    if (ll)
	        压入长长整数(p->v1);
	    else
	        压入整数常量(p->v1);
            通用_操作(双符号_大于等于);
            gtst_addr(0, p->sym);
            生成符号(e);
        }
    }
    *bsym = 生成跳转到标签(*bsym);
}

/* ------------------------------------------------------------------------- */
/* __attribute__((cleanup(fn))) */

static void 尝试_调用_范围_清理(符号 *stop)
{
    符号 *cls = 当前_范围->cl.s;

    for (; cls != stop; cls = cls->ncl) {
	符号 *fs = cls->next;
	符号 *vs = cls->prev_tok;

	压入符号值(&fs->type, fs);
	vset(&vs->type, vs->r, vs->c);
	栈顶值->sym = vs;
        修改类型_指针类型(&栈顶值->type);
	获取栈顶值地址();
	具体地址函数_调用(1);
    }
}

static void 尝试_调用_去向_清理(符号 *cleanupstate)
{
    符号 *oc, *cc;
    int ocd, ccd;

    if (!当前_范围->cl.s)
	return;

    /* search NCA of both cleanup chains given parents and initial depth */
    ocd = cleanupstate ? cleanupstate->v & ~符号_字段 : 0;
    for (ccd = 当前_范围->cl.n, oc = cleanupstate; ocd > ccd; --ocd, oc = oc->ncl)
      ;
    for (cc = 当前_范围->cl.s; ccd > ocd; --ccd, cc = cc->ncl)
      ;
    for (; cc != oc; cc = cc->ncl, oc = oc->ncl, --ccd)
      ;

    尝试_调用_范围_清理(cc);
}

/* call 'func' for each __attribute__((cleanup(func))) */
static void 块_清理(struct 范围 *o)
{
    int jmp = 0;
    符号 *g, **pg;
    for (pg = &待定_去向; (g = *pg) && g->c > o->cl.n;) {
        if (g->prev_tok->r & 标签_正向定义) {
            符号 *pcl = g->next;
            if (!jmp)
                jmp = 生成跳转到标签(0);
            生成符号(pcl->jnext);
            尝试_调用_范围_清理(o->cl.s);
            pcl->jnext = 生成跳转到标签(0);
            if (!o->cl.n)
                goto remove_pending;
            g->c = o->cl.n;
            pg = &g->prev;
        } else {
    remove_pending:
            *pg = g->prev;
            符号_释放(g);
        }
    }
    生成符号(jmp);
    尝试_调用_范围_清理(o->cl.s);
}

/* ------------------------------------------------------------------------- */
/* VLA */

static void vla_还原(int 局部变量索引)
{
    if (局部变量索引)
        生成_vla_sp_恢复(局部变量索引);
}

static void vla_离开(struct 范围 *o)
{
    if (o->vla.num < 当前_范围->vla.num)
        vla_还原(o->vla.局部变量索引);
}

/* ------------------------------------------------------------------------- */
/* local scopes */

void 新_范围(struct 范围 *o)
{
    /* copy and link previous 范围 */
    *o = *当前_范围;
    o->prev = 当前_范围;
    当前_范围 = o;

    /* record local declaration stack position */
    o->lstk = 局部符号_堆栈;
    o->llstk = 局部符号_标签_堆栈;

    ++局部_范围;

    if (zhi_状态->执行_调试)
        zhi_调试_stabn(N_LBRAC, 输出代码索引 - func_ind);
}

void 上一个_范围(struct 范围 *o, int is_expr)
{
    vla_离开(o->prev);

    if (o->cl.s != o->prev->cl.s)
        块_清理(o->prev);

    /* pop locally defined labels */
    标签_弹出(&局部符号_标签_堆栈, o->llstk, is_expr);

    /* In the is_expr case (a statement expression is finished here),
       栈顶值 might refer to symbols on the 局部符号_堆栈.  Either via the
       type or via 栈顶值->sym.  We can't pop those nor any that in turn
       might be referred to.  To make it easier we don't roll back
       any symbols in that case; some upper level call to 块() will
       do that.  We do have to remove such symbols from the lookup
       tables, though.  符号_弹出 will do that.  */

    /* pop locally defined symbols */
    弹出_局部_符号(&局部符号_堆栈, o->lstk, is_expr, 0);
    当前_范围 = o->prev;
    --局部_范围;

    if (zhi_状态->执行_调试)
        zhi_调试_stabn(N_RBRAC, 输出代码索引 - func_ind);
}

/* leave a 范围 via break/continue(/goto) */
void 离开_范围(struct 范围 *o)
{
    if (!o)
        return;
    尝试_调用_范围_清理(o->cl.s);
    vla_离开(o);
}

/* ------------------------------------------------------------------------- */
/* call 块 from 'for do while' loops */

static void 循环块(int *bsym, int *csym)
{
    struct 范围 *lo = 循环_范围, *co = 当前_范围;
    int *b = co->bsym, *c = co->csym;
    if (csym) {
        co->csym = csym;
        循环_范围 = co;
    }
    co->bsym = bsym;
    块(0);
    co->bsym = b;
    if (csym) {
        co->csym = c;
        循环_范围 = lo;
    }
}

static void 块(int is_expr)
{
    int a, b, c, d, e, t;
    struct 范围 o;
    符号 *s;

    if (is_expr) {
        /* 默认返回值为（void） */
        压入整数常量(0);
        栈顶值->type.t = VT_无类型;
    }

again:
    t = 单词编码, 带有宏替换的下个标记();

    if (t == 关键词_IF || t == 关键词_如果) {
        跳过('(');
        通用表达式();
        跳过(')');
        a = 生成值测试(1, 0);
        块(0);
        if (单词编码 == 关键词_ELSE || 单词编码 == 关键词_否则) {
            d = 生成跳转到标签(0);
            生成符号(a);
            带有宏替换的下个标记();
            块(0);
            生成符号(d); /* patch else jmp */
        } else {
            生成符号(a);
        }

    } else if (t == 关键词_WHILE || t == 关键词_判断) {
        d = 返回代码索引();
        跳过('(');
        通用表达式();
        跳过(')');
        a = 生成值测试(1, 0);
        b = 0;
        循环块(&a, &b);
        生成跳转到_固定地址(d);
        生成符号_地址(b, d);
        生成符号(a);

    } else if (t == '{') {
        新_范围(&o);

        /* handle local labels declarations */
        while (单词编码 == 关键词_LABEL || 单词编码 == 关键词_标签) {
            do {
                带有宏替换的下个标记();
                if (单词编码 < 符_没识别)
                    应为("label identifier");
                标签_推送(&局部符号_标签_堆栈, 单词编码, 标签_被声明);
                带有宏替换的下个标记();
            } while (单词编码 == ',');
            跳过(';');
        }

        while (单词编码 != '}') {
	    声明(VT_LOCAL);
            if (单词编码 != '}') {
                if (is_expr)
                    弹出堆栈值();
                块(is_expr);
            }
        }

        上一个_范围(&o, is_expr);
        if (局部_范围)
            带有宏替换的下个标记();
        else if (!不需要_代码生成)
            检查_函数_返回值();

    } else if (t == 关键词_RETURN || t == 关键词_返回) {
        b = (当前函数_返回类型.t & VT_基本类型) != VT_无类型;
        if (单词编码 != ';') {
            通用表达式();
            if (b) {
                通用_指定类型_转换(&当前函数_返回类型);
            } else {
                if (栈顶值->type.t != VT_无类型)
                    zhi_警告("void function returns a value");
                栈顶值--;
            }
        } else if (b) {
            zhi_警告("'return' with no value");
            b = 0;
        }
        离开_范围(根_范围);
        if (b)
            生成函数_返回值(&当前函数_返回类型);
        跳过(';');
        /* jump unless last stmt in top-level 块 */
        if (单词编码 != '}' || 局部_范围 != 1)
            返回符号 = 生成跳转到标签(返回符号);
        代码_关();

    } else if (t == 关键词_BREAK || t == 关键词_跳出) {
        /* compute jump */
        if (!当前_范围->bsym)
            错误_打印("cannot break");
        if (当前_选择 && 当前_范围->bsym == 当前_选择->bsym)
            离开_范围(当前_选择->范围);
        else
            离开_范围(循环_范围);
        *当前_范围->bsym = 生成跳转到标签(*当前_范围->bsym);
        跳过(';');

    } else if (t == 关键词_CONTINUE || t == 关键词_继续) {
        /* compute jump */
        if (!当前_范围->csym)
            错误_打印("cannot continue");
        离开_范围(循环_范围);
        *当前_范围->csym = 生成跳转到标签(*当前_范围->csym);
        跳过(';');

    } else if (t == 关键词_FOR || t == 关键词_循环) {
        新_范围(&o);

        跳过('(');
        if (单词编码 != ';') {
            /* c99 for-loop init 声明? */
            if (!声明0(VT_LOCAL, 1, NULL)) {
                /* no, regular for-loop init expr */
                通用表达式();
                弹出堆栈值();
            }
        }
        跳过(';');
        a = b = 0;
        c = d = 返回代码索引();
        if (单词编码 != ';') {
            通用表达式();
            a = 生成值测试(1, 0);
        }
        跳过(';');
        if (单词编码 != ')') {
            e = 生成跳转到标签(0);
            d = 返回代码索引();
            通用表达式();
            弹出堆栈值();
            生成跳转到_固定地址(c);
            生成符号(e);
        }
        跳过(')');
        循环块(&a, &b);
        生成跳转到_固定地址(d);
        生成符号_地址(b, d);
        生成符号(a);
        上一个_范围(&o, 0);

    } else if (t == 关键词_DO || t ==关键词_执行) {
        a = b = 0;
        d = 返回代码索引();
        循环块(&a, &b);
        生成符号(b);
        if(单词编码 == 关键词_WHILE)/*此处只能用tok而不能用t*/
        {
        	跳过(关键词_WHILE);
        }else if(单词编码 == 关键词_判断)
        {
        	跳过(关键词_判断);
        }
        跳过('(');
	通用表达式();
        跳过(')');
        跳过(';');
	c = 生成值测试(0, 0);
	生成符号_地址(c, d);
        生成符号(a);

    } else if (t == 关键词_SWITCH || t == 关键词_选择) {
        struct 选择结构体 *sw;

        sw = 内存_初始化(sizeof *sw);
        sw->bsym = &a;
        sw->范围 = 当前_范围;
        sw->prev = 当前_选择;
        当前_选择 = sw;

        跳过('(');
        通用表达式();
        跳过(')');
        sw->sv = *栈顶值--; /* save switch value */

        a = 0;
        b = 生成跳转到标签(0); /* jump to first case */
        循环块(&a, NULL);
        a = 生成跳转到标签(a); /* add implicit break */
        /* case lookup */
        生成符号(b);

        qsort(sw->p, sw->n, sizeof(void*), 分支_对比);
        for (b = 1; b < sw->n; b++)
            if (sw->p[b - 1]->v2 >= sw->p[b]->v1)
                错误_打印("duplicate case value");

        /* Our switch table sorting is signed, so the compared
           value needs to be as well when it's 64bit.  */
        vpushv(&sw->sv);
        if ((栈顶值->type.t & VT_基本类型) == VT_长长整数)
            栈顶值->type.t &= ~VT_无符号;
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
        d = 0, 生成分支(sw->p, sw->n, &d);
        弹出堆栈值();
        if (sw->默认_符号)
            生成符号_地址(d, sw->默认_符号);
        else
            生成符号(d);
        /* break label */
        生成符号(a);

        动态数组_重分配容量(&sw->p, &sw->n);
        当前_选择 = sw->prev;
        内存_释放(sw);

    } else if (t == 关键词_CASE || t == 关键词_分支) {
        struct 分支_t *cr = 内存_申请(sizeof(struct 分支_t));
        if (!当前_选择)
            应为("switch");
        cr->v1 = cr->v2 = 表达式_常量64();
        if (gnu_扩展 && 单词编码 == 符_三个圆点) {
            带有宏替换的下个标记();
            cr->v2 = 表达式_常量64();
            if (cr->v2 < cr->v1)
                zhi_警告("empty case range");
        }
        cr->sym = 返回代码索引();
        动态数组_追加元素(&当前_选择->p, &当前_选择->n, cr);
        跳过(':');
        is_expr = 0;
        goto block_after_label;

    } else if (t == 关键词_DEFAULT || t == 关键词_默认) {
        if (!当前_选择)
            应为("switch");
        if (当前_选择->默认_符号)
            错误_打印("too many 'default'");
        当前_选择->默认_符号 = 返回代码索引();
        跳过(':');
        is_expr = 0;
        goto block_after_label;

    } else if (t == 关键词_GOTO || t == 关键词_去向) {
        vla_还原(根_范围->vla.局部变量索引);
        if (单词编码 == '*' && gnu_扩展) {
            /* computed goto */
            带有宏替换的下个标记();
            通用表达式();
            if ((栈顶值->type.t & VT_基本类型) != VT_指针)
                应为("pointer");
            g去向();

        } else if (单词编码 >= 符_没识别) {
	    s = 标签_查找(单词编码);
	    /* put forward definition if needed */
            if (!s)
              s = 标签_推送(&全局符号_标签_堆栈, 单词编码, 标签_正向定义);
            else if (s->r == 标签_被声明)
              s->r = 标签_正向定义;

	    if (s->r & 标签_正向定义) {
		/* start new goto chain for cleanups, linked via label->next */
		if (当前_范围->cl.s && !不需要_代码生成) {
                    符号_推送2(&待定_去向, 符号_字段, 0, 当前_范围->cl.n);
                    待定_去向->prev_tok = s;
                    s = 符号_推送2(&s->next, 符号_字段, 0, 0);
                    待定_去向->next = s;
                }
		s->jnext = 生成跳转到标签(s->jnext);
	    } else {
		尝试_调用_去向_清理(s->cleanupstate);
		生成跳转到_固定地址(s->jnext);
	    }
	    带有宏替换的下个标记();

        } else {
            应为("label identifier");
        }
        跳过(';');

    } else if (t == 关键词_asm1 || t == 关键词_asm2 || t == 关键词_asm3 || t == 关键词_汇编) {
        汇编_指令字符串();

    } else {
        if (单词编码 == ':' && t >= 符_没识别) {
            /* label case */
	    带有宏替换的下个标记();
            s = 标签_查找(t);
            if (s) {
                if (s->r == 标签_定义)
                    错误_打印("duplicate label '%s'", 取_单词字符串(s->v, NULL));
                s->r = 标签_定义;
		if (s->next) {
		    符号 *pcl; /* pending cleanup goto */
		    for (pcl = s->next; pcl; pcl = pcl->prev)
		      生成符号(pcl->jnext);
		    符号_弹出(&s->next, NULL, 0);
		} else
		  生成符号(s->jnext);
            } else {
                s = 标签_推送(&全局符号_标签_堆栈, t, 标签_定义);
            }
            s->jnext = 返回代码索引();
            s->cleanupstate = 当前_范围->cl.s;

    block_after_label:
            vla_还原(当前_范围->vla.局部变量索引);
            /* we accept this, but it is a mistake */
            if (单词编码 == '}') {
                zhi_警告("deprecated use of label at end of compound statement");
            } else {
                goto again;
            }

        } else {
            /* expression case */
            if (t != ';') {
                设为_指定标识符(t);
                if (is_expr) {
                    弹出堆栈值();
                    通用表达式();
                } else {
                    通用表达式();
                    弹出堆栈值();
                }
                跳过(';');
            }
        }
    }
}

/* This skips over a stream of 标识符s containing balanced {} and ()
   pairs, stopping at outer ',' ';' and '}' (or matching '}' if we started
   with a '{').  If STR then allocates and stores the skipped 标识符s
   in *STR.  This doesn't check if () and {} are nested correctly,
   i.e. "({)}" is accepted.  */
static void 跳过_或_保存_块(单词字符串 **str)
{
    int braces = 单词编码 == '{';
    int level = 0;
    if (str)
      *str = 单词字符串_分配();

    while ((level > 0 || (单词编码 != '}' && 单词编码 != ',' && 单词编码 != ';' && 单词编码 != ')'))) {
	int t;
	if (单词编码 == 符_文件结尾) {
	     if (str || level > 0)
	       错误_打印("unexpected end of file");
	     else
	       break;
	}
	if (str)
	  单词字符串中添加当前解析的字符(*str);
	t = 单词编码;
	带有宏替换的下个标记();
	if (t == '{' || t == '(') {
	    level++;
	} else if (t == '}' || t == ')') {
	    level--;
	    if (level == 0 && braces && t == '}')
	      break;
	}
    }
    if (str) {
	单词字符串_增加大小(*str, -1);
	单词字符串_增加大小(*str, 0);
    }
}

#define EXPR_CONST 1
#define EXPR_ANY   2

static void 解析_初始化_元素(int 解析表达式_返回类型)
{
    int saved_global_expr;
    switch(解析表达式_返回类型) {
    case EXPR_CONST:
        /* compound literals must be allocated globally in this case */
        saved_global_expr = 全局_分配复合字符;
        全局_分配复合字符 = 1;
        表达式_常量1();
        全局_分配复合字符 = saved_global_expr;
        /* NOTE: symbols are accepted, as well as lvalue for anon symbols
	   (compound literals).  */
        if (((栈顶值->r & (VT_值掩码 | VT_LVAL)) != VT_VC常量
             && ((栈顶值->r & (VT_符号|VT_LVAL)) != (VT_符号|VT_LVAL)
                 || 栈顶值->sym->v < 符号_第一个_匿名))
#ifdef ZHI_TARGET_PE
                 || ((栈顶值->r & VT_符号) && 栈顶值->sym->a.dllimport)
#endif
           )
            错误_打印("initializer element is not constant");
        break;
    case EXPR_ANY:
        等于_表达式();
        break;
    }
}

/* 将零用于基于变量的init */
static void 将0_用于变量的初始化(段 *sec, unsigned long c, int size)
{
    if (sec) {
        /* 无事可做，因为全局变量已经设置为零 */
    } else {
        推送对_全局符号V_的引用(&函数_旧_类型, 符_memset);
        vseti(VT_LOCAL, c);
#ifdef ZHI_TARGET_ARM
        压入目标地址类型常量(size);
        压入整数常量(0);
#else
        压入整数常量(0);
        压入目标地址类型常量(size);
#endif
        具体地址函数_调用(3);
    }
}

#define DIF_FIRST     1
#define DIF_SIZE_ONLY 2
#define DIF_HAVE_ELEM 4

/* t是数组或结构类型。 c是数组或结构地址。 cur_field是指向当前字段的指针，对于数组，“ c”成员包含当前起始索引。
 *  “标志”与声明_初始化器中的一样。 “ al”包含当前容器的已初始化长度（从c开始）。 这将返回它的新长度。  */
static int 声明_指示符(C类型 *type, 段 *sec, unsigned long c,符号 **cur_field, int flags, int al)
{
    符号 *s, *f;
    int index, index_last, align, l, 数量_elems, elem_size;
    unsigned long corig = c;

    elem_size = 0;
    数量_elems = 1;

    if (flags & DIF_HAVE_ELEM)
        goto no_designator;

    if (gnu_扩展 && 单词编码 >= 符_没识别) {
        l = 单词编码, 带有宏替换的下个标记();
        if (单词编码 == ':')
            goto struct_field;
        设为_指定标识符(l);
    }

    /* NOTE: we only support ranges for last designator */
    while (数量_elems == 1 && (单词编码 == '[' || 单词编码 == '.')) {
        if (单词编码 == '[') {
            if (!(type->t & VT_数组))
                应为("array type");
            带有宏替换的下个标记();
            index = index_last = 表达式_常量();
            if (单词编码 == 符_三个圆点 && gnu_扩展) {
                带有宏替换的下个标记();
                index_last = 表达式_常量();
            }
            跳过(']');
            s = type->ref;
	    if (index < 0 || (s->c >= 0 && index_last >= s->c) ||
		index_last < index)
	        错误_打印("invalid index");
            if (cur_field)
		(*cur_field)->c = index_last;
            type = 指定的_类型(type);
            elem_size = 类型_大小(type, &align);
            c += index * elem_size;
            数量_elems = index_last - index + 1;
        } else {
            int cumofs;
            带有宏替换的下个标记();
            l = 单词编码;
        struct_field:
            带有宏替换的下个标记();
            if ((type->t & VT_基本类型) != VT_结构体)
                应为("struct/union type");
            cumofs = 0;
	    f = 查找_域(type, l, &cumofs);
            if (!f)
                应为("field");
            if (cur_field)
                *cur_field = f;
	    type = &f->type;
            c += cumofs + f->c;
        }
        cur_field = NULL;
    }
    if (!cur_field) {
        if (单词编码 == '=') {
            带有宏替换的下个标记();
        } else if (!gnu_扩展) {
	    应为("=");
        }
    } else {
    no_designator:
        if (type->t & VT_数组) {
	    index = (*cur_field)->c;
	    if (type->ref->c >= 0 && index >= type->ref->c)
	        错误_打印("index too large");
            type = 指定的_类型(type);
            c += index * 类型_大小(type, &align);
        } else {
            f = *cur_field;
	    while (f && (f->v & 符号_第一个_匿名) && (f->type.t & VT_位域))
	        *cur_field = f = f->next;
            if (!f)
                错误_打印("too many field init");
	    type = &f->type;
            c += f->c;
        }
    }
    /* must put zero in holes (note that doing it that way
       ensures that it even works with designators) */
    if (!(flags & DIF_SIZE_ONLY) && c - corig > al)
	将0_用于变量的初始化(sec, corig + al, c - corig - al);
    声明_初始化器(type, sec, c, flags & ~DIF_FIRST);

    /* XXX: make it more general */
    if (!(flags & DIF_SIZE_ONLY) && 数量_elems > 1) {
        unsigned long c_end;
        uint8_t *src, *dst;
        int i;

        if (!sec) {
	    vset(type, VT_LOCAL|VT_LVAL, c);
	    for (i = 1; i < 数量_elems; i++) {
		vset(type, VT_LOCAL|VT_LVAL, c + elem_size * i);
		vswap();
		将栈顶值_存储在堆栈左值();
	    }
	    弹出堆栈值();
        } else if (!不需要_静态数据输出) {
	    c_end = c + 数量_elems * elem_size;
	    if (c_end > sec->data_allocated)
	        节_重新分配内存(sec, c_end);
	    src = sec->data + c;
	    dst = src;
	    for(i = 1; i < 数量_elems; i++) {
		dst += elem_size;
		memcpy(dst, src, elem_size);
	    }
	}
    }
    c += 数量_elems * 类型_大小(type, &align);
    if (c - corig > al)
      al = c - corig;
    return al;
}

/* 将值或表达式直接存储在全局数据或本地数组中 */
static void 将值或表达式_直接存储在全局数据或本地数组中(C类型 *type, 段 *sec, unsigned long c)
{
    int bt;
    void *ptr;
    C类型 dtype;

    dtype = *type;
    dtype.t &= ~VT_常量; /* need to do that to avoid false warning */

    if (sec) {
	int size, align;
        /* XXX: not portable */
        /* XXX: generate error if incorrect relocation */
        通用_指定类型_转换(&dtype);
        bt = type->t & VT_基本类型;

        if ((栈顶值->r & VT_符号)
            && bt != VT_指针
            && bt != VT_函数
            && (bt != (指针_大小 == 8 ? VT_长长整数 : VT_整数)
                || (type->t & VT_位域))
            && !((栈顶值->r & VT_VC常量) && 栈顶值->sym->v >= 符号_第一个_匿名)
            )
            错误_打印("initializer element is not computable at 加载 time");

        if (不需要_静态数据输出) {
            栈顶值--;
            return;
        }

	size = 类型_大小(type, &align);
	段_保留(sec, c + size);
        ptr = sec->data + c;

        /* XXX: make code faster ? */
	if ((栈顶值->r & (VT_符号|VT_VC常量)) == (VT_符号|VT_VC常量) &&
	    栈顶值->sym->v >= 符号_第一个_匿名 &&
	    /* XXX This rejects compound literals like
	       '(void *){ptr}'.  The problem is that '&sym' is
	       represented the same way, which would be ruled out
	       by the 符号_第一个_匿名 check above, but also '"string"'
	       in 'char *p = "string"' is represented the same
	       with the type being VT_指针 and the symbol being an
	       anonymous one.  That is, there's no difference in 栈顶值
	       between '(void *){x}' and '&(void *){x}'.  Ignore
	       pointer typed entities here.  Hopefully no real code
	       will ever use compound literals with scalar type.  */
	    (栈顶值->type.t & VT_基本类型) != VT_指针) {
	    /* These come from compound literals, memcpy stuff over.  */
	    段 *ssec;
	    ELF符号 *esym;
	    ElfW_Rel *rel;
	    esym = elf符号(栈顶值->sym);
	    ssec = zhi_状态->段数[esym->st_shndx];
	    memmove (ptr, ssec->data + esym->st_value + (int)栈顶值->c.i, size);
	    if (ssec->重定位) {
		/* We need to copy over all memory contents, and that
		   includes relocations.  Use the fact that relocs are
		   created it order, so look from the end of relocs
		   until we hit one before the copied region.  */
		int num_relocs = ssec->重定位->数据_偏移 / sizeof(*rel);
		rel = (ElfW_Rel*)(ssec->重定位->data + ssec->重定位->数据_偏移);
		while (num_relocs--) {
		    rel--;
		    if (rel->r_offset >= esym->st_value + size)
		      continue;
		    if (rel->r_offset < esym->st_value)
		      break;
		    /* Note: if the same fields are initialized multiple
		       times (possible with designators) then we possibly
		       add multiple relocations for the same offset here.
		       That would lead to wrong code, the last 重定位 needs
		       to win.  We clean this up later after the whole
		       initializer is parsed.  */
		    使_elf_重定位目标地址(单词表_部分, sec,
				   c + rel->r_offset - esym->st_value,
				   ELFW(R_TYPE)(rel->r_info),
				   ELFW(R_SYM)(rel->r_info),
#if 指针_大小 == 8
				   rel->r_addend
#else
				   0
#endif
				  );
		}
	    }
	} else {
            if (type->t & VT_位域) {
                int bit_pos, bit_size, bits, n;
                unsigned char *p, v, m;
                bit_pos = BIT_POS(栈顶值->type.t);
                bit_size = BIT_大小(栈顶值->type.t);
                p = (unsigned char*)ptr + (bit_pos >> 3);
                bit_pos &= 7, bits = 0;
                while (bit_size) {
                    n = 8 - bit_pos;
                    if (n > bit_size)
                        n = bit_size;
                    v = 栈顶值->c.i >> bits << bit_pos;
                    m = ((1 << n) - 1) << bit_pos;
                    *p = (*p & ~m) | (v & m);
                    bits += n, bit_size -= n, bit_pos = 0, ++p;
                }
            } else
            switch(bt) {
		/* XXX: 在交叉编译时，我们假设每种类型在主机和目标上的表示都相同，这对于长double而言可能是错误的 */
	    case VT_逻辑:
		栈顶值->c.i = 栈顶值->c.i != 0;
	    case VT_字节:
		*(char *)ptr |= 栈顶值->c.i;
		break;
	    case VT_短整数:
		*(short *)ptr |= 栈顶值->c.i;
		break;
	    case VT_浮点:
		*(float*)ptr = 栈顶值->c.f;
		break;
	    case VT_双精度:
		*(double *)ptr = 栈顶值->c.d;
		break;
	    case VT_长双精度:
#if defined ZHI_IS_NATIVE_387
                if (sizeof (long double) >= 10) /* zero pad ten-byte LD */
                    memcpy(ptr, &栈顶值->c.ld, 10);
#ifdef __TINYC__
                else if (sizeof (long double) == sizeof (double))
                    __asm__("fldl %1\nfstpt %0\n" : "=m" (*ptr) : "m" (栈顶值->c.ld));
#endif
                else if (栈顶值->c.ld == 0.0)
                    ;
                else
#endif
                if (sizeof(long double) == 长双精度_大小)
		    *(long double*)ptr = 栈顶值->c.ld;
                else if (sizeof(double) == 长双精度_大小)
		    *(double *)ptr = (double)栈顶值->c.ld;
                else
                    错误_打印("can't cross compile long double constants");
		break;
#if 指针_大小 != 8
	    case VT_长长整数:
		*(long long *)ptr |= 栈顶值->c.i;
		break;
#else
	    case VT_长长整数:
#endif
	    case VT_指针:
		{
		    目标地址_类型 val = 栈顶值->c.i;
#if 指针_大小 == 8
		    if (栈顶值->r & VT_符号)
		      添加新的重定位项(sec, 栈顶值->sym, c, R_DATA_PTR, val);
		    else
		      *(目标地址_类型 *)ptr |= val;
#else
		    if (栈顶值->r & VT_符号)
		      段部分符号重定位(sec, 栈顶值->sym, c, R_DATA_PTR);
		    *(目标地址_类型 *)ptr |= val;
#endif
		    break;
		}
	    default:
		{
		    int val = 栈顶值->c.i;
#if 指针_大小 == 8
		    if (栈顶值->r & VT_符号)
		      添加新的重定位项(sec, 栈顶值->sym, c, R_DATA_PTR, val);
		    else
		      *(int *)ptr |= val;
#else
		    if (栈顶值->r & VT_符号)
		      段部分符号重定位(sec, 栈顶值->sym, c, R_DATA_PTR);
		    *(int *)ptr |= val;
#endif
		    break;
		}
	    }
	}
        栈顶值--;
    } else {
        vset(&dtype, VT_LOCAL|VT_LVAL, c);
        vswap();
        将栈顶值_存储在堆栈左值();
        弹出堆栈值();
    }
}

/* 't' contains the type and storage info. 'c' is the offset of the
   object in section 'sec'. If 'sec' is NULL, it means stack based
   allocation. 'flags & DIF_FIRST' is true if array '{' must be read (multi
   dimension implicit array init handling). 'flags & DIF_SIZE_ONLY' is true if
   size only evaluation is wanted (only for arrays). */
static void 声明_初始化器(C类型 *type, 段 *sec, unsigned long c, 
                             int flags)
{
    int len, n, no_oblock, i;
    int size1, align1;
    符号 *s, *f;
    符号 indexsym;
    C类型 *t1;

    if (!(flags & DIF_HAVE_ELEM) && 单词编码 != '{' &&
	/* In case of strings we have special handling for arrays, so
	   don't consume them as initializer value (which would commit them
	   to some anonymous symbol).  */
	单词编码 != 常量_长字符串 && 单词编码 != 常量_字符串 &&
	!(flags & DIF_SIZE_ONLY)) {
	解析_初始化_元素(!sec ? EXPR_ANY : EXPR_CONST);
        flags |= DIF_HAVE_ELEM;
    }

    if ((flags & DIF_HAVE_ELEM) &&
	!(type->t & VT_数组) &&
	/* Use i_c_parameter_t, to strip toplevel qualifiers.
	   The source type might have VT_常量 set, which is
	   of course assignable to non-const elements.  */
	是_兼容_不合格的_类型(type, &栈顶值->type)) {
        将值或表达式_直接存储在全局数据或本地数组中(type, sec, c);
    } else if (type->t & VT_数组) {
        s = type->ref;
        n = s->c;
        t1 = 指定的_类型(type);
        size1 = 类型_大小(t1, &align1);

        no_oblock = 1;
        if (((flags & DIF_FIRST) && 单词编码 != 常量_长字符串 && 单词编码 != 常量_字符串) ||
            单词编码 == '{') {
            if (单词编码 != '{')
                错误_打印("character array initializer must be a literal,"
                    " optionally enclosed in braces");
            跳过('{');
            no_oblock = 0;
        }

        /* only parse strings here if correct type (otherwise: handle
           them as ((w)char *) expressions */
        if ((单词编码 == 常量_长字符串 && 
#ifdef ZHI_TARGET_PE
             (t1->t & VT_基本类型) == VT_短整数 && (t1->t & VT_无符号)
#else
             (t1->t & VT_基本类型) == VT_整数
#endif
            ) || (单词编码 == 常量_字符串 && (t1->t & VT_基本类型) == VT_字节)) {
            int nb;
	    len = 0;
            动态字符串_重置(&初始字符串);
            if (size1 != (单词编码 == 常量_字符串 ? 1 : sizeof(nwchar_t)))
              错误_打印("unhandled string literal merging");
            while (单词编码 == 常量_字符串 || 单词编码 == 常量_长字符串) {
                if (初始字符串.字符串长度)
                  初始字符串.字符串长度 -= size1;
                if (单词编码 == 常量_字符串)
                  len += 单词值.str.size;
                else
                  len += 单词值.str.size / sizeof(nwchar_t);
                len--;
                动态字符串_cat(&初始字符串, 单词值.str.data, 单词值.str.size);
                带有宏替换的下个标记();
            }
            if (单词编码 != ')' && 单词编码 != '}' && 单词编码 != ',' && 单词编码 != ';'
                && 单词编码 != 符_文件结尾) {
                /* Not a lone literal but part of a bigger expression.  */
                设为_指定标识符(size1 == 1 ? 常量_字符串 : 常量_长字符串);
                单词值.str.size = 初始字符串.字符串长度;
                单词值.str.data = 初始字符串.指向字符串的指针;
                indexsym.c = 0;
                f = &indexsym;
                goto do_init_list;
            }
            nb = len;
            if (n >= 0 && len > n)
              nb = n;
            if (!(flags & DIF_SIZE_ONLY)) {
                if (len > nb)
                  zhi_警告("initializer-string for array is too long");
                /* in order to go faster for common case (char
                   string in global variable, we handle it
                   specifically */
                if (sec && size1 == 1) {
                    if (!不需要_静态数据输出)
                      memcpy(sec->data + c, 初始字符串.指向字符串的指针, nb);
                } else {
                    for(i=0;i<nb;i++) {
                        if (size1 == 1)
                          当前取到的源码字符 = ((unsigned char *)初始字符串.指向字符串的指针)[i];
                        else
                          当前取到的源码字符 = ((nwchar_t *)初始字符串.指向字符串的指针)[i];
                        压入整数常量(当前取到的源码字符);
                        将值或表达式_直接存储在全局数据或本地数组中(t1, sec, c + i * size1);
                    }
                }
            }
            /* only add trailing zero if enough storage (no
               warning in this case since it is standard) */
            if (n < 0 || len < n) {
                if (!(flags & DIF_SIZE_ONLY)) {
		    压入整数常量(0);
                    将值或表达式_直接存储在全局数据或本地数组中(t1, sec, c + (len * size1));
                }
                len++;
            }
	    len *= size1;
        } else {
	    indexsym.c = 0;
	    f = &indexsym;

          do_init_list:
	    len = 0;
	    while (单词编码 != '}' || (flags & DIF_HAVE_ELEM)) {
		len = 声明_指示符(type, sec, c, &f, flags, len);
		flags &= ~DIF_HAVE_ELEM;
		if (type->t & VT_数组) {
		    ++indexsym.c;
		    /* special test for multi dimensional arrays (may not
		       be strictly correct if designators are used at the
		       same time) */
		    if (no_oblock && len >= n*size1)
		        break;
		} else {
		    if (s->type.t == VT_共用体)
		        f = NULL;
		    else
		        f = f->next;
		    if (no_oblock && f == NULL)
		        break;
		}

		if (单词编码 == '}')
		    break;
		跳过(',');
	    }
        }
        /* put zeros at the end */
	if (!(flags & DIF_SIZE_ONLY) && len < n*size1)
	    将0_用于变量的初始化(sec, c + len, n*size1 - len);
        if (!no_oblock)
            跳过('}');
        /* patch type size if needed, which happens only for array types */
        if (n < 0)
            s->c = size1 == 1 ? len : ((len + size1 - 1)/size1);
    } else if ((type->t & VT_基本类型) == VT_结构体) {
	size1 = 1;
        no_oblock = 1;
        if ((flags & DIF_FIRST) || 单词编码 == '{') {
            跳过('{');
            no_oblock = 0;
        }
        s = type->ref;
        f = s->next;
        n = s->c;
	goto do_init_list;
    } else if (单词编码 == '{') {
        if (flags & DIF_HAVE_ELEM)
          跳过(';');
        带有宏替换的下个标记();
        声明_初始化器(type, sec, c, flags & ~DIF_HAVE_ELEM);
        跳过('}');
    } else if ((flags & DIF_SIZE_ONLY)) {
	/* If we supported only ISO C we wouldn't have to accept calling
	   this on anything than an array if DIF_SIZE_ONLY (and even then
	   only on the outermost level, so no recursion would be needed),
	   because initializing a flex array member isn't supported.
	   But GNU C supports it, so we need to recurse even into
	   subfields of structs and arrays when DIF_SIZE_ONLY is set.  */
        /* just 跳过 expression */
        跳过_或_保存_块(NULL);
    } else {
	if (!(flags & DIF_HAVE_ELEM)) {
	    /* This should happen only when we haven't parsed
	       the init element above for fear of committing a
	       string constant to memory too early.  */
	    if (单词编码 != 常量_字符串 && 单词编码 != 常量_长字符串)
	      应为("string constant");
	    解析_初始化_元素(!sec ? EXPR_ANY : EXPR_CONST);
	}
        将值或表达式_直接存储在全局数据或本地数组中(type, sec, c);
    }
}

/* parse an initializer for type 't' if 'has_init' is non zero, and
   allocate space in local or global data space ('r' is either
   VT_LOCAL or VT_VC常量). If 'v' is non zero, then an associated
   variable 'v' of 范围 '范围' is declared before initializers
   are parsed. If 'v' is zero, then a reference to the new object
   is put in the value stack. If 'has_init' is 2, a special parsing
   is done to handle string constants. */
static void 声明_初始化_分配(C类型 *type, 属性定义 *ad, int r,
                                   int has_init, int v, int 范围)
{
    int size, align, addr;
    单词字符串 *init_str = NULL;

    段 *sec;
    符号 *flexible_array;
    符号 *sym = NULL;
    int saved_nocode_wanted = 不需要_代码生成;
#ifdef 配置_ZHI_边界检查
    int bcheck = zhi_状态->执行_边界_检查器 && !不需要_静态数据输出;
#endif

    /* Always allocate static or global variables */
    if (v && (r & VT_值掩码) == VT_VC常量)
        不需要_代码生成 |= 0x80000000;

    flexible_array = NULL;
    if ((type->t & VT_基本类型) == VT_结构体) {
        符号 *field = type->ref->next;
        if (field) {
            while (field->next)
                field = field->next;
            if (field->type.t & VT_数组&& field->type.ref->c < 0)
                flexible_array = field;
        }
    }

    size = 类型_大小(type, &align);
    /* If unknown size, we must evaluate it before
       evaluating initializers because
       initializers can generate global data too
       (e.g. string pointers or ISOC99 compound
       literals). It also simplifies local
       initializers handling */
    if (size < 0 || (flexible_array && has_init)) {
        if (!has_init) 
            错误_打印("unknown type size");
        /* get all init string */
        if (has_init == 2) {
	    init_str = 单词字符串_分配();
            /* only get strings */
            while (单词编码 == 常量_字符串 || 单词编码 == 常量_长字符串) {
                单词字符串中添加当前解析的字符(init_str);
                带有宏替换的下个标记();
            }
	    单词字符串_增加大小(init_str, -1);
	    单词字符串_增加大小(init_str, 0);
        } else {
	    跳过_或_保存_块(&init_str);
        }
        设为_指定标识符(0);

        /* compute size */
        开始_宏(init_str, 1);
        带有宏替换的下个标记();
        声明_初始化器(type, NULL, 0, DIF_FIRST | DIF_SIZE_ONLY);
        /* prepare second initializer parsing */
        宏_ptr = init_str->str;
        带有宏替换的下个标记();
        
        /* if still unknown size, error */
        size = 类型_大小(type, &align);
        if (size < 0) 
            错误_打印("unknown type size");
    }
    /* If there's a flex member and it was used in the initializer
       adjust size.  */
    if (flexible_array &&
	flexible_array->type.ref->c > 0)
        size += flexible_array->type.ref->c
	        * 指定类型的_大小(&flexible_array->type);
    /* take into account specified alignment if bigger */
    if (ad->a.aligned) {
	int speca = 1 << (ad->a.aligned - 1);
        if (speca > align)
            align = speca;
    } else if (ad->a.packed) {
        align = 1;
    }

    if (!v && 不需要_静态数据输出)
        size = 0, align = 1;

    if ((r & VT_值掩码) == VT_LOCAL) {
        sec = NULL;
#ifdef 配置_ZHI_边界检查
        if (bcheck && v) {
            /* add padding between stack variables for bound checking */
            局部变量索引--;
        }
#endif
        局部变量索引 = (局部变量索引 - size) & -align;
        addr = 局部变量索引;
#ifdef 配置_ZHI_边界检查
        if (bcheck && v) {
            /* add padding between stack variables for bound checking */
            局部变量索引--;
        }
#endif
        if (v) {
            /* local variable */
#ifdef 配置_ZHI_汇编
	    if (ad->汇编_label) {
		int reg = 汇编_解析_注册变量(ad->汇编_label);
		if (reg >= 0)
		    r = (r & ~VT_值掩码) | reg;
	    }
#endif
            sym = 符号_压入栈(v, type, r, addr);
	    if (ad->cleanup_func) {
		符号 *cls = 符号_推送2(&清理_所有,
                    符号_字段 | ++当前_范围->cl.n, 0, 0);
		cls->prev_tok = sym;
		cls->next = ad->cleanup_func;
		cls->ncl = 当前_范围->cl.s;
		当前_范围->cl.s = cls;
	    }

            sym->a = ad->a;
        } else {
            /* push local reference */
            vset(type, r, addr);
        }
    } else {
        if (v && 范围 == VT_VC常量) {
            /* see if the symbol was already defined */
            sym = 符号_查询(v);
            if (sym) {
                合并_其他存储属性(sym, ad, type);
                /* we accept several definitions of the same global variable. */
                if (!has_init && sym->c && elf符号(sym)->st_shndx != SHN_UNDEF)
                    goto no_alloc;
            }
        }

        /* allocate symbol in corresponding section */
        sec = ad->section;
        if (!sec) {
            if (has_init)
                sec = 初始化数据_部分;
            else if (zhi_状态->不使用通用符号)
                sec = 未初始化数据_部分;
        }

        if (sec) {
	    addr = 返回_节_对齐偏移量(sec, size, align);
#ifdef 配置_ZHI_边界检查
            /* add padding if bound check */
            if (bcheck)
                返回_节_对齐偏移量(sec, 1, 1);
#endif
        } else {
            addr = align; /* SHN_COMMON is special, symbol value is align */
	    sec = 通用_部分;
        }

        if (v) {
            if (!sym) {
                sym = 符号_压入栈(v, type, r | VT_符号, 0);
                合并_其他存储属性(sym, ad, NULL);
            }
            /* update symbol definition */
	    更新_外部_符号(sym, sec, addr, size);
        } else {
            /* push global reference */
            将引用_推送到_节偏移量(type, sec, addr, size);
            sym = 栈顶值->sym;
	    栈顶值->r |= r;
        }

#ifdef 配置_ZHI_边界检查
        /* handles bounds now because the symbol must be defined
           before for the relocation */
        if (bcheck) {
            目标地址_类型 *bounds_ptr;

            添加新的重定位项(全局边界_部分, sym, 全局边界_部分->数据_偏移, R_DATA_PTR, 0);
            /* then add global bound info */
            bounds_ptr = 段_ptr_添加(全局边界_部分, 2 * sizeof(目标地址_类型));
            bounds_ptr[0] = 0; /* relocated */
            bounds_ptr[1] = size;
        }
#endif
    }

    if (type->t & VT_变长数组) {
        int a;

        if (不需要_静态数据输出)
            goto no_alloc;

        /* save current stack pointer */
        if (根_范围->vla.局部变量索引 == 0) {
            struct 范围 *v = 当前_范围;
            生成_vla_sp_保存(局部变量索引 -= 指针_大小);
            do v->vla.局部变量索引 = 局部变量索引; while ((v = v->prev));
        }

        vla_运行时_类型_大小(type, &a);
        生成_vla_分配(type, a);
#if defined ZHI_TARGET_PE && defined ZHI_TARGET_X86_64
        /* on _WIN64, because of the function args scratch area, the
           result of alloca differs from RSP and is returned in RAX.  */
        生成_vla_结果(addr), addr = (局部变量索引 -= 指针_大小);
#endif
        生成_vla_sp_保存(addr);
        当前_范围->vla.局部变量索引 = addr;
        当前_范围->vla.num++;
    } else if (has_init) {
	size_t oldreloc_offset = 0;
	if (sec && sec->重定位)
	  oldreloc_offset = sec->重定位->数据_偏移;
        声明_初始化器(type, sec, addr, DIF_FIRST);
	if (sec && sec->重定位)
	  压缩_多_重定位(sec, oldreloc_offset);
        /* patch flexible array member size back to -1, */
        /* for possible subsequent similar declarations */
        if (flexible_array)
            flexible_array->type.ref->c = -1;
    }

 no_alloc:
    /* restore parse state if needed */
    if (init_str) {
        结束_宏();
        带有宏替换的下个标记();
    }

    不需要_代码生成 = saved_nocode_wanted;
}

/* 解析由符号“ sym”定义的函数，并在“当前_生成代码_段”中生成其代码 */
static void 生成_函数(符号 *sym)
{
    /* Initialize VLA state */
    struct 范围 f = { 0 };
    当前_范围 = 根_范围 = &f;

    不需要_代码生成 = 0;
    输出代码索引 = 当前_生成代码_段->数据_偏移;
    if (sym->a.aligned) {
	size_t newoff = 返回_节_对齐偏移量(当前_生成代码_段, 0,
				    1 << (sym->a.aligned - 1));
	生成_fill_nops(newoff - 输出代码索引);
    }
    /* NOTE: we patch the symbol size later */
    更新_外部_符号(sym, 当前_生成代码_段, 输出代码索引, 0);
    if (sym->type.ref->f.func_ctor)
        增加_数组 (zhi_状态, ".init_array", sym->c);
    if (sym->type.ref->f.func_dtor)
        增加_数组 (zhi_状态, ".fini_array", sym->c);

    函数名称 = 取_单词字符串(sym->v, NULL);
    func_ind = 输出代码索引;
    当前函数_返回类型 = sym->type.ref->type;
    当前函数_可变参数 = sym->type.ref->f.func_type == 函数_省略;

    /* put debug symbol */
    zhi_调试_函数开始(zhi_状态, sym);
    /* push a dummy symbol to enable local sym storage */
    符号_推送2(&局部符号_堆栈, 符号_字段, 0, 0);
    局部_范围 = 1; /* for function parameters */
    生成函数_序言(sym);
    局部_范围 = 0;
    返回符号 = 0;
    清除_临时_局部_变量_列表();
    块(0);
    生成符号(返回符号);
    不需要_代码生成 = 0;
    /* reset local stack */
    弹出_局部_符号(&局部符号_堆栈, NULL, 0, 当前函数_可变参数);
    生成函数_结尾();
    当前_生成代码_段->数据_偏移 = 输出代码索引;
    局部_范围 = 0;
    标签_弹出(&全局符号_标签_堆栈, NULL, 0);
    符号_弹出(&清理_所有, NULL, 0);
    /* patch symbol size */
    elf符号(sym)->st_size = 输出代码索引 - func_ind;
    /* end of function */
    zhi_调试_函数结束(zhi_状态, 输出代码索引 - func_ind);
    /* It's better to crash than to generate wrong code */
    当前_生成代码_段 = NULL;
    函数名称 = ""; /* for safety */
    当前函数_返回类型.t = VT_无类型; /* for safety */
    当前函数_可变参数 = 0; /* for safety */
    输出代码索引 = 0; /* for safety */
    不需要_代码生成 = 0x80000000;
    检查_堆栈值();
    /* do this after funcend debug info */
    带有宏替换的下个标记();
}

static void 生成_内联函数(知心状态机 *s)
{
    符号 *sym;
    int inline_generated, i;
    struct 内联函数 *fn;

    打开缓存文件(s, ":inline:", 0);
    /* iterate while inline function are referenced */
    do {
        inline_generated = 0;
        for (i = 0; i < s->数量_内联_函数数; ++i) {
            fn = s->内联_函数数[i];
            sym = fn->sym;
            if (sym && (sym->c || !(sym->type.t & VT_内联))) {
                /* the function was used or forced (and then not internal):
                   generate its code and convert it to a normal function */
                fn->sym = NULL;
                zhi_调试_备用文件(s, fn->文件名);
                开始_宏(fn->函数_字符串, 1);
                带有宏替换的下个标记();
                当前_生成代码_段 = 生成代码_段;
                生成_函数(sym);
                结束_宏();

                inline_generated = 1;
            }
        }
    } while (inline_generated);
    关闭文件();
}

static void 释放_内联函数(知心状态机 *s)
{
    int i;
    for (i = 0; i < s->数量_内联_函数数; ++i)
    {
        struct 内联函数 *fn = s->内联_函数数[i];
        if (fn->sym)
            单词字符串_释放(fn->函数_字符串);
    }
    动态数组_重分配容量(&s->内联_函数数, &s->数量_内联_函数数);
}

static int 声明0(int l, int is_for_loop_init, 符号 *func_sym)
{
    int v, has_init, r;
    C类型 type, btype;
    符号 *sym;
    属性定义 ad, adbase;

    while (1) {
	if (单词编码 == 关键词_STATIC_ASSERT || 单词编码 == 关键词_静态_声明) {
	    动态字符串 error_str;
	    int c;

	    带有宏替换的下个标记();
	    跳过('(');
	    c = 表达式_常量();

	    if (单词编码 == ')') {
		if (!c)
		    错误_打印("_Static_assert fail");
		带有宏替换的下个标记();
		goto static_assert_out;
	    }

	    跳过(',');
	    解析_多_字符串(&error_str, "string constant");
	    if (c == 0)
		错误_打印("%s", (char *)error_str.指向字符串的指针);
	    动态字符串_释放(&error_str);
	    跳过(')');
	  static_assert_out:
            跳过(';');
	    continue;
	}
        if (!解析_基本类型(&btype, &adbase)) {
            if (is_for_loop_init)
                return 0;
            /* 跳过 redundant ';' if not in old parameter 声明 范围 */
            if (单词编码 == ';' && l != VT_CMP) {
                带有宏替换的下个标记();
                continue;
            }
            if (l != VT_VC常量)
                break;
            if (单词编码 == 关键词_asm1 || 单词编码 == 关键词_asm2 || 单词编码 == 关键词_asm3 || 单词编码 == 关键词_汇编) {
                /* global asm 块 */
                汇编_全局_instr();
                continue;
            }
            if (单词编码 >= 符_没识别) {
               /* special test for old K&R protos without explicit int
                  type. Only accepted when defining global data */
                btype.t = VT_整数;
            } else {
                if (单词编码 != 符_文件结尾)
                    应为("declaration");
                break;
            }
        }
        if (单词编码 == ';') {
	    if ((btype.t & VT_基本类型) == VT_结构体) {
		int v = btype.ref->v;
		if (!(v & 符号_字段) && (v & ~符号_结构体) >= 符号_第一个_匿名)
        	    zhi_警告("unnamed struct/union that defines no instances");
                带有宏替换的下个标记();
                continue;
	    }
            if (是_枚举(btype.t)) {
                带有宏替换的下个标记();
                continue;
            }
        }
        while (1) { /* iterate thru each declaration */
            type = btype;
	    /* If the base type itself was an array type of unspecified
	       size (like in 'typedef int arr[]; arr x = {1};') then
	       we will overwrite the unknown size by the real one for
	       this 声明.  We need to unshare the ref symbol holding
	       that size.  */
	    if ((type.t & VT_数组) && type.ref->c < 0) {
		type.ref = 符号_压入栈(符号_字段, &type.ref->type, 0, type.ref->c);
	    }
	    ad = adbase;
            类型_声明(&type, &ad, &v, 类型_直接);
#if 0
            {
                char buf[500];
                字符串_的_类型(buf, sizeof(buf), &type, 取_单词字符串(v, NULL));
                printf("type = '%s'\n", buf);
            }
#endif
            if ((type.t & VT_基本类型) == VT_函数) {
                if ((type.t & VT_静态) && (l == VT_LOCAL))
                    错误_打印("function without file 范围 cannot be static");
                /* if old style function prototype, we accept a
                   declaration list */
                sym = type.ref;
                if (sym->f.func_type == 函数_旧 && l == VT_VC常量)
                    声明0(VT_CMP, 0, sym);
#ifdef ZHI_TARGET_MACHO
                if (sym->f.func_alwinl
                    && ((type.t & (VT_外部 | VT_内联))
                        == (VT_外部 | VT_内联))) {
                    /* always_inline functions must be handled as if they
                       don't generate multiple global defs, even if extern
                       inline, i.e. GNU inline semantics for those.  Rewrite
                       them into static inline.  */
                    type.t &= ~VT_外部;
                    type.t |= VT_静态;
                }
#endif
                /* always compile 'extern inline' */
                if (type.t & VT_外部)
                    type.t &= ~VT_内联;
            }

            if (gnu_扩展 && (单词编码 == 关键词_asm1 || 单词编码 == 关键词_asm2 || 单词编码 == 关键词_asm3 || 单词编码 == 关键词_汇编)) {
                ad.汇编_label = 在字符串_查找_汇编标签并解析();
                /* parse one last attribute list, after asm label */
                解析_属性(&ad);
            #if 0
                /* gcc不允许在函数定义中使用__asm __（“ label”），但是为什么不这样做呢？ */
                if (单词编码 == '{')
                    应为(";");
            #endif
            }

#ifdef ZHI_TARGET_PE
            if (ad.a.dllimport || ad.a.dllexport) {
                if (type.t & VT_静态)
                    错误_打印("不能与静态链接");
                if (type.t & VT_别名) {
                    zhi_警告("使用别名（类型定义）忽略了 '%s' 属性。",
                        ad.a.dllimport ? (ad.a.dllimport = 0, "dllimport") :
                        (ad.a.dllexport = 0, "dllexport"));
                } else if (ad.a.dllimport) {
                    if ((type.t & VT_基本类型) == VT_函数)
                        ad.a.dllimport = 0;
                    else
                        type.t |= VT_外部;
                }
            }
#endif
            if (单词编码 == '{') {
                if (l != VT_VC常量)
                    错误_打印("无法使用局部函数");
                if ((type.t & VT_基本类型) != VT_函数)
                    应为("函数定义");

                /* 拒绝函数定义中的抽象声明符使不带decl的旧样式参数具有int类型 */
                sym = type.ref;
                while ((sym = sym->next) != NULL) {
                    if (!(sym->v & ~符号_字段))
                        应为("identifier");
                    if (sym->type.t == VT_无类型)
                        sym->type = int_type;
                }

                /* apply post-declaraton attributes */
                合并_函数属性(&type.ref->f, &ad.f);

                /* put function symbol */
                type.t &= ~VT_外部;
                sym = 定义外部引用_符号(v, &type, 0, &ad);

                /* static inline functions are just recorded as a kind
                   of macro. Their code will be emitted at the end of
                   the compilation unit only if they are used */
                if (sym->type.t & VT_内联) {
                    struct 内联函数 *fn;
                    fn = 内存_申请(sizeof *fn + strlen(file->文件名));
                    strcpy(fn->文件名, file->文件名);
                    fn->sym = sym;
		    跳过_或_保存_块(&fn->函数_字符串);
                    动态数组_追加元素(&zhi_状态->内联_函数数,
				 &zhi_状态->数量_内联_函数数, fn);
                } else {
                    /* compute text section */
                    当前_生成代码_段 = ad.section;
                    if (!当前_生成代码_段)
                        当前_生成代码_段 = 生成代码_段;
                    生成_函数(sym);
                }
                break;
            } else {
		if (l == VT_CMP) {
		    /* find parameter in function parameter list */
		    for (sym = func_sym->next; sym; sym = sym->next)
			if ((sym->v & ~符号_字段) == v)
			    goto found;
		    错误_打印("declaration for parameter '%s' but no such parameter",
			      取_单词字符串(v, NULL));
found:
		    if (type.t & VT_存储) /* 'register' is okay */
		        错误_打印("storage class specified for '%s'",
				  取_单词字符串(v, NULL));
		    if (sym->type.t != VT_无类型)
		        错误_打印("redefinition of parameter '%s'",
				  取_单词字符串(v, NULL));
		    转换_函数参数_类型(&type);
		    sym->type = type;
		} else if (type.t & VT_别名) {
                    /* save typedefed type  */
                    /* XXX: test storage specifiers ? */
                    sym = 符号_查询(v);
                    if (sym && sym->符号_范围 == 局部_范围) {
                        if (!是_兼容_类型(&sym->type, &type)
                            || !(sym->type.t & VT_别名))
                            错误_打印("incompatible redefinition of '%s'",
                                取_单词字符串(v, NULL));
                        sym->type = type;
                    } else {
                        sym = 符号_压入栈(v, &type, 0, 0);
                    }
                    sym->a = ad.a;
                    sym->f = ad.f;
		} else if ((type.t & VT_基本类型) == VT_无类型
			   && !(type.t & VT_外部)) {
		    错误_打印("declaration of void object");
                } else {
                    r = 0;
                    if ((type.t & VT_基本类型) == VT_函数) {
                        /* external function definition */
                        /* specific case for 函数_调用 attribute */
                        type.ref->f = ad.f;
                    } else if (!(type.t & VT_数组)) {
                        /* not lvalue if array */
                        r |= VT_LVAL;
                    }
                    has_init = (单词编码 == '=');
                    if (has_init && (type.t & VT_变长数组))
                        错误_打印("variable length array cannot be initialized");
                    if (((type.t & VT_外部) && (!has_init || l != VT_VC常量))
		        || (type.t & VT_基本类型) == VT_函数
                        /* as with GCC, uninitialized global arrays with no size
                           are considered extern: */
                        || ((type.t & VT_数组) && !has_init
                            && l == VT_VC常量 && type.ref->c < 0)
                        ) {
                        /* external variable or function */
                        type.t |= VT_外部;
                        sym = 定义外部引用_符号(v, &type, r, &ad);
                        if (ad.alias_target) {
                            ELF符号 *esym;
                            符号 *alias_target;
                            alias_target = 符号_查询(ad.alias_target);
                            esym = elf符号(alias_target);
                            if (!esym)
                                错误_打印("unsupported forward __alias__ attribute");
                            更新_外部_符号2(sym, esym->st_shndx, esym->st_value, esym->st_size, 0);
                        }
                    } else {
                        if (type.t & VT_静态)
                            r |= VT_VC常量;
                        else
                            r |= l;
                        if (has_init)
                            带有宏替换的下个标记();
                        else if (l == VT_VC常量)
                            /* uninitialized global variables may be overridden */
                            type.t |= VT_外部;
                        声明_初始化_分配(&type, &ad, r, has_init, v, l);
                    }
                }
                if (单词编码 != ',') {
                    if (is_for_loop_init)
                        return 1;
                    跳过(';');
                    break;
                }
                带有宏替换的下个标记();
            }
        }
    }
    return 0;
}

static void 声明(int l)
{
    声明0(l, 0, NULL);
}

/* ------------------------------------------------------------------------- */
#undef 生成跳转到_固定地址
#undef 生成跳转到标签
/* ------------------------------------------------------------------------- */
