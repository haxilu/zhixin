#ifndef 宏_ZHI_H
#define 宏_ZHI_H

#define _GNU_SOURCE
#define _DARWIN_C_SOURCE
#include "config.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <fcntl.h>
#include <setjmp.h>
#include <time.h>

#ifndef _WIN32
# define WIN32_LEAN_AND_MEAN 1 /*WIN32精简版*/
# include <unistd.h>
# include <sys/time.h>
# ifndef 配置_ZHI_静态
#  include <dlfcn.h>
# endif
/* XXX: 需要定义它以在非ISOC99上下文中使用它们 */
extern float strtof (const char *__nptr, char **__endptr);
extern long double strtold (const char *__nptr, char **__endptr);
#endif

#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include <io.h> /* open, close etc. */
# include <direct.h> /* getcwd */
# ifdef __GNUC__
#  include <stdint.h>
# endif
# define inline __inline
# define snprintf _snprintf
# define vsnprintf _vsnprintf
# ifndef __GNUC__
#  define strtold (long double)strtod
#  define strtof (float)strtod
#  define strtoll _strtoi64
#  define strtoull _strtoui64
# endif
# ifdef HEXINKU_AS_DLL
#  define HEXINKU接口 __declspec(dllexport)
#  define 公共_函数 HEXINKU接口
# endif
# define 读下个符号处理缓冲区结尾 next_inp /* 读下个符号处理缓冲区结尾 is an intrinsic on msvc/mingw */
# ifdef _MSC_VER
#  pragma warning (disable : 4244)  // 从“ uint64_t”到“ int”的转换，可能会丢失数据
#  pragma warning (disable : 4267)  // 从“ size_t”到“ int”的转换，可能会丢失数据
#  pragma warning (disable : 4996)  // 此项的POSIX名称已弃用。 而是使用符合ISO C和C ++的名称
#  pragma warning (disable : 4018)  // 有符号/无符号不匹配
#  pragma warning (disable : 4146)  // 一元减运算符应用于无符号类型，结果仍为无符号
#  define ssize_t intptr_t
#  ifdef _X86_
#   define __i386__ 1
#  endif
#  ifdef _AMD64_
#   define __x86_64__ 1
#  endif
# endif
# undef 配置_ZHI_静态
#endif

#ifndef O_BINARY
# define O_BINARY 0
#endif

#ifndef offsetof
#define offsetof(type, field) ((size_t) &((type *)0)->field)
#endif

#ifndef countof
#define countof(tab) (sizeof(tab) / sizeof((tab)[0]))
#endif

#ifdef _MSC_VER
# define 无返回 __declspec(noreturn)
# define ALIGNED(x) __declspec(align(x))
# define PRINTF_LIKE(x,y)
#else
# define 无返回 __attribute__((noreturn))
# define ALIGNED(x) __attribute__((aligned(x)))
# define PRINTF_LIKE(x,y) __attribute__ ((format (printf, (x), (y))))
#endif

/* gnu headers use to #define __attribute__ to empty for non-gcc compilers */
#ifdef __TINYC__
# undef __attribute__
#endif

#ifdef _WIN32
# define IS_DIRSEP(c) (c == '/' || c == '\\')
# define IS_ABSPATH(p) (IS_DIRSEP(p[0]) || (p[0] && p[1] == ':' && IS_DIRSEP(p[2])))
# define PATHCMP stricmp
# define PATHSEP ";"
#else
# define IS_DIRSEP(c) (c == '/')
# define IS_ABSPATH(p) IS_DIRSEP(p[0])
# define PATHCMP strcmp
# define PATHSEP ":"
#endif

/* -------------------------------------------- */

#if !defined(ZHI_TARGET_I386) && !defined(ZHI_TARGET_ARM) && \
    !defined(ZHI_TARGET_ARM64) && !defined(ZHI_TARGET_C67) && \
    !defined(ZHI_TARGET_X86_64) && !defined(ZHI_TARGET_RISCV64)
# if defined __x86_64__
#  define ZHI_TARGET_X86_64
# elif defined __arm__
#  define ZHI_TARGET_ARM
#  define ZHI_ARM_EABI
#  define ZHI_ARM_VFP
#  define ZHI_ARM_HARDFLOAT
# elif defined __aarch64__
#  define ZHI_TARGET_ARM64
# elif defined __riscv
#  define ZHI_TARGET_RISCV64
# else
#  define ZHI_TARGET_I386
# endif
# ifdef _WIN32
#  define ZHI_TARGET_PE 1
# endif
#endif

/* 仅本机编译器支持-run */
#if 已定义 _WIN32 == defined ZHI_TARGET_PE
# if defined __i386__ && defined ZHI_TARGET_I386
#  define ZHI_是_本机
# elif defined __x86_64__ && defined ZHI_TARGET_X86_64
#  define ZHI_是_本机
# elif defined __arm__ && defined ZHI_TARGET_ARM
#  define ZHI_是_本机
# elif defined __aarch64__ && defined ZHI_TARGET_ARM64
#  define ZHI_是_本机
# elif defined __riscv && defined __LP64__ && defined ZHI_TARGET_RISCV64
#  define ZHI_是_本机
# endif
#endif

#if defined ZHI_是_本机 && !defined CONFIG_ZHIBOOT
# define ZHI_配置_记录回滚
# if (defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64 || \
      defined ZHI_TARGET_ARM || defined ZHI_TARGET_ARM64) || \
      defined ZHI_TARGET_RISCV64 \
  && !defined ZHI_UCLIBC && !defined ZHI_MUSL
# define 配置_ZHI_边界检查 /* 启用边界检查代码 */
# endif
#endif

/* ------------ 路径配置 ------------ */

#ifndef 配置_系统根目录
# define 配置_系统根目录 ""
#endif
#ifndef 配置_ZHI目录
# define 配置_ZHI目录 "/usr/local/lib/zhi"
#endif
#ifndef 配置_LD目录
# define 配置_LD目录 "lib"
#endif
#ifdef 配置_三目
# define 使用_三目(s) s "/" 配置_三目
# define 也是_三目(s) 使用_三目(s) ":" s
#else
# define 使用_三目(s) s
# define 也是_三目(s) s
#endif

/* 查找crt1.o，crti.o和crtn.o的路径 */
#ifndef 配置_ZHI_CRT前缀
# define 配置_ZHI_CRT前缀 使用_三目(配置_系统根目录 "/usr/" 配置_LD目录)
#endif

#ifndef 配置_USR_包含
# define 配置_USR_包含 "/usr/include"
#endif

/* Below: {B} is substituted by 配置_ZHI目录 (rsp. -B option) */

/* 系统包含路径 */
#ifndef 配置_ZHI_系统包含路径
# if defined ZHI_TARGET_PE || 已定义 _WIN32
#  define 配置_ZHI_系统包含路径 "{B}/include"PATHSEP"{B}/include/winapi"
# else
#  define 配置_ZHI_系统包含路径 \
        "{B}/include" \
    ":" ALSO_TRIPLET(配置_系统根目录 "/usr/local/include") \
    ":" ALSO_TRIPLET(配置_系统根目录 配置_USR_包含)
# endif
#endif

/* 库搜索路径 */
#ifndef 配置_ZHI_库搜索路径
# ifdef ZHI_TARGET_PE
#  define 配置_ZHI_库搜索路径 "{B}/lib"
# else
#  define 配置_ZHI_库搜索路径 \
        ALSO_TRIPLET(配置_系统根目录 "/usr/" 配置_LD目录) \
    ":" ALSO_TRIPLET(配置_系统根目录 "/" 配置_LD目录) \
    ":" ALSO_TRIPLET(配置_系统根目录 "/usr/local/" 配置_LD目录)
# endif
#endif

/* ELF解释器的名称 */
#ifndef 配置_ZHI_ELF解释程序路径
# if defined __FreeBSD__
#  define 配置_ZHI_ELF解释程序路径 "/libexec/ld-elf.so.1"
# elif defined __FreeBSD_kernel__
#  if defined(ZHI_TARGET_X86_64)
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld-kfreebsd-x86-64.so.1"
#  else
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld.so.1"
#  endif
# elif defined __DragonFly__
#  define 配置_ZHI_ELF解释程序路径 "/usr/libexec/ld-elf.so.2"
# elif defined __NetBSD__
#  define 配置_ZHI_ELF解释程序路径 "/usr/libexec/ld.elf_so"
# elif defined __GNU__
#  define 配置_ZHI_ELF解释程序路径 "/lib/ld.so"
# elif defined(ZHI_TARGET_PE)
#  define 配置_ZHI_ELF解释程序路径 "-"
# elif defined(ZHI_UCLIBC)
#  define 配置_ZHI_ELF解释程序路径 "/lib/ld-uClibc.so.0" /* is there a uClibc for x86_64 ? */
# elif defined ZHI_TARGET_ARM64
#  if defined(ZHI_MUSL)
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld-musl-aarch64.so.1"
#  else
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld-linux-aarch64.so.1"
#  endif
# elif defined(ZHI_TARGET_X86_64)
#  if defined(ZHI_MUSL)
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld-musl-x86_64.so.1"
#  else
#   define 配置_ZHI_ELF解释程序路径 "/lib64/ld-linux-x86-64.so.2"
#  endif
# elif defined(ZHI_TARGET_RISCV64)
#  define 配置_ZHI_ELF解释程序路径 "/lib/ld-linux-riscv64-lp64d.so.1"
# elif !defined(ZHI_ARM_EABI)
#  if defined(ZHI_MUSL)
#   if defined(ZHI_TARGET_I386)
#     define 配置_ZHI_ELF解释程序路径 "/lib/ld-musl-i386.so.1"
#    else
#     define 配置_ZHI_ELF解释程序路径 "/lib/ld-musl-arm.so.1"
#    endif
#  else
#   define 配置_ZHI_ELF解释程序路径 "/lib/ld-linux.so.2"
#  endif
# endif
#endif

/* var elf_interp dans *-gen.c */
#ifdef 配置_ZHI_ELF解释程序路径
# define 默认_ELF解释程序路径(s) 配置_ZHI_ELF解释程序路径
#else
# define 默认_ELF解释程序路径(s) 默认_elf插入(s)
#endif

/* （针对特定目标）hexinku1.a */
#ifndef ZHI_HEXINKU1
# define ZHI_HEXINKU1 "hexinku1.a"
#endif

/* 与CONFIG_USE_LIBGCC一起使用的库，而不是hexinku1.a */
#if defined CONFIG_USE_LIBGCC && !defined ZHI_LIBGCC
#define ZHI_LIBGCC 使用_三目(配置_系统根目录 "/" 配置_LD目录) "/libgcc_s.so.1"
#endif

/* -------------------------------------------- */

#include "hexinku.h"
#include "ELF类型结构和宏定义.h"
#include "stab.h"

/* -------------------------------------------- */

#ifndef 公共_函数 /* zhi.c使用的函数，但不在hexinku.h中 */
# define 公共_函数
#endif

#ifndef 是_源文件
# define 是_源文件 1
#endif

/* 支持从线程使用hexinku */
#define 从线程_使用_核心库

#if 是_源文件
#define 静态_内联 static inline
#define 静态_函数 static
#define 静态_外部 static
#else
#define 静态_内联
#define 静态_函数
#define 静态_外部 extern
#endif

#ifdef ZHI_简介 /* 分析所有函数 */
# define static
#endif

/* -------------------------------------------- */
/* 包括目标的特定定义 */

#define TARGET_DEFS_ONLY
#ifdef ZHI_TARGET_I386
# include "i386-gen.c"
# include "i386-link.c"
#elif defined ZHI_TARGET_X86_64
# include "x86_64-gen.c"
# include "x86_64-link.c"
#elif defined ZHI_TARGET_ARM
# include "arm-gen.c"
# include "arm-link.c"
# include "arm-asm.c"
#elif defined ZHI_TARGET_ARM64
# include "arm64-gen.c"
# include "arm64-link.c"
# include "arm-asm.c"
#elif defined ZHI_TARGET_C67
# define ZHI_TARGET_COFF
# include "COFF类型结构宏定义.h"
# include "c67-gen.c"
# include "c67-link.c"
#elif defined(ZHI_TARGET_RISCV64)
# include "riscv64-gen.c"
# include "riscv64-link.c"
# include "riscv64-asm.c"
#else
#error unknown target
#endif
#undef TARGET_DEFS_ONLY

/* -------------------------------------------- */

#if 指针_大小 == 8
# define ELFCLASSW ELF64位文件字节类型
# define ElfW(type) Elf##64##_##type
# define ELFW(type) ELF##64##_##type
# define ElfW_Rel ElfW(Rela)
# define SHT_RELX SHT_RELA
# define REL_SECTION_FMT ".rela%s"
#else
# define ELFCLASSW ELF32位文件字节类型
# define ElfW(type) Elf##32##_##type
# define ELFW(type) ELF##32##_##type
# define ElfW_Rel ElfW(Rel)
# define SHT_RELX SHT_REL
# define REL_SECTION_FMT ".rel%s"
#endif
/* 目标地址类型 */
#define 目标地址_类型 ElfW(地址)
#define ELF符号 ElfW(符号)

#if 指针_大小 == 8 && !defined ZHI_TARGET_PE
# define LONG_SIZE 8
#else
# define LONG_SIZE 4
#endif

/* -------------------------------------------- */

#define 包含_堆栈_大小  32
#define 如果定义_堆栈_大小    64
#define 堆栈值_大小         256
#define 字符串_最大_长度     1024
#define 单词字符串_最大_长度     256
#define 包_堆栈_大小     8

#define 哈希表容量       16384 /* 必须是2的幂 */
#define 符_分配_INCR      512  /* 必须是2的幂 */
#define 符_最大_大小        4 /* 存储在字符串中时，令牌最大大小（以int为单位） */

typedef struct 单词存储结构 {
    struct 单词存储结构 *hash_next;
    struct 符号 *sym_define; /* direct pointer to define */
    struct 符号 *sym_label; /* direct pointer to label */
    struct 符号 *sym_struct; /* direct pointer to structure */
    struct 符号 *sym_identifier; /* direct pointer to identifier */
    int 单词编码; /* 单词编码 */
    int len;
    char str[1];
} 单词存储结构;

#ifdef ZHI_TARGET_PE
typedef unsigned short nwchar_t;
#else
typedef int nwchar_t;
#endif

typedef struct {
    int 字符串长度; /* 字符串长度 */
    void *指向字符串的指针; /* 指向字符串的指针 */
    int 缓冲区长度;/*缓冲区长度*/
} 动态字符串;

/* 类型定义 */
typedef struct C类型 {
    int t;
    struct 符号 *ref;
} C类型;

/* 恒定值 */
typedef union 恒定值 {
    long double ld;
    double d;
    float f;
    uint64_t i;
    struct {
        const void *data;
        int size;
    } str;
    int tab[长双精度_大小/4];
} 恒定值;

typedef struct 堆栈值 {
    C类型 type;      /* type */
    unsigned short r;      /* register + flags */
    unsigned short r2;     /* second register, used for 'long long'
                              type. If not used, set to VT_VC常量 */
    union {
      struct { int jtrue, jfalse; }; /* forward jmps */
      恒定值 c;         /* constant, if VT_VC常量 */
    };
    union {
      struct { unsigned short cmp_op, cmp_r; }; /* VT_CMP operation */
      struct 符号 *sym;  /* symbol, if (VT_符号 | VT_VC常量), or if */
    };                  /* result of 一元() for an identifier. */

} 堆栈值;

struct 符号属性 {
    unsigned short
    aligned     : 5, /* alignment as log2+1 (0 == unspecified) */
    packed      : 1,
    weak        : 1,
    visibility  : 2,
    dllexport   : 1,
    nodecorate  : 1,
    dllimport   : 1,
    addrtaken   : 1,
    xxxx        : 3; /* not used */
};

/* 函数属性或用于解析的临时属性 */
struct 函数属性 {
    unsigned
    函数_调用   : 3, /* calling convention (0..5), see below */
    func_type   : 2, /* 函数_旧/NEW/ELLIPSIS */
    func_noreturn : 1, /* attribute((noreturn)) */
    func_ctor   : 1, /* attribute((constructor)) */
    func_dtor   : 1, /* attribute((destructor)) */
    func_args   : 8, /* PE __stdcall args */
    func_alwinl : 1, /* always_inline */
    xxxx        :15;
};

typedef struct 符号 {
    int v; /* 符号标识符 */
    unsigned short r; /* associated register or VT_VC常量/VT_LOCAL and LVAL type */
    struct 符号属性 a;
    union {
        struct {
            int c; /* associated number or Elf symbol index */
            union {
                int 符号_范围; /* scope level for locals */
                int jnext; /* next jump label */
                struct 函数属性 f; /* function attributes */
                int auxtype; /* bitfield access type */
            };
        };
        long long enum_val; /* enum constant if 是_枚举_变长 */
        int *d; /* define 标识符 stream */
        struct 符号 *ncl; /* next cleanup */
    };
    C类型 type; /* associated type */
    union {
        struct 符号 *next; /* next related symbol (for fields and anoms) */
        struct 符号 *cleanupstate; /* in defined labels */
        int 汇编_label; /* associated asm label */
    };
    struct 符号 *prev; /* prev symbol in stack */
    struct 符号 *prev_tok; /* previous symbol for this 标识符 */
} 符号;
typedef struct 段 {
    unsigned long 数据_偏移; /* current data offset */
    unsigned char *data;       /* section data */
    unsigned long data_allocated; /* used for realloc() handling */
    知心状态机 *状态机1;
    int sh_name;             /* elf section name (only used during output) */
    int sh_num;              /* elf section number */
    int sh_type;             /* elf section type */
    int sh_flags;            /* elf section flags */
    int sh_info;             /* elf section info */
    int sh_addralign;        /* elf section alignment */
    int sh_entsize;          /* elf entry size */
    unsigned long sh_size;   /* section size (only used during output) */
    目标地址_类型 sh_addr;          /* address at which the section is relocated */
    unsigned long sh_偏移; /* file offset */
    int 数量_hashed_syms;      /* 用于调整哈希表的大小 */
    struct 段 *link;    /* link to another section */
    struct 段 *重定位;   /* corresponding section for relocation, if any */
    struct 段 *hash;    /* hash table for symbols */
    struct 段 *prev;    /* previous section on section stack */
    char name[1];           /* section name */
} 段;

typedef struct DLL参考 {
    int level;
    void *handle;
    char name[1];
} DLL参考;

/* -------------------------------------------------- */

#define 符号_结构体     0x40000000 /* struct/union/enum symbol space */
#define 符号_字段      0x20000000 /* struct/union field symbol space */
#define 符号_第一个_匿名 0x10000000 /* first anonymous sym */

/* stored in '符号->f.func_type' field */
#define 函数_新       1 /* ansi function prototype */
#define 函数_旧       2 /* old function prototype */
#define 函数_省略  3 /* ansi function prototype with ... */

/* stored in '符号->f.函数_调用' field */
#define 函数_CDECL     0 /* standard c call */
#define 函数_标准调用   1 /* pascal c call */
#define 函数_快速调用1 2 /* first param in %eax */
#define 函数_快速调用2 3 /* first parameters in %eax, %edx */
#define 函数_快速调用3 4 /* first parameter in %eax, %edx, %ecx */
#define 函数_快速调用W 5 /* first parameter in %ecx, %edx */

/* field '符号.t' for macros */
#define 宏_对象     0 /* object like macro */
#define 宏_函数     1 /* function like macro */

/* field '符号.r' for C labels */
#define 标签_定义  0 /* label is defined */
#define 标签_正向定义  1 /* label is forward defined */
#define 标签_被声明 2 /* 标签声明但从未使用 */
#define 标签_不在范围     3 /* 标签不在范围,但尚未出现从局部_标签_堆栈exprs(支撑) */

/* 类型_声明() types */
#define 类型_抽象  1 /* type without variable */
#define 类型_直接    2 /* type with variable */

#define 输入输出_缓冲_大小 8192

typedef struct 缓冲文件 {/*该缓冲文件结构包含读取文件所需的上下文，包括当前行号。*/
    uint8_t *buf_ptr;
    uint8_t *buf_end;
    int fd;
    struct 缓冲文件 *prev;
    int line_num;    /* 当前行号-此处简化代码 */
    int line_ref;    /* zhi -E：最后打印的行 */
    int ifndef_macro;  /* #ifndef宏/ #endif搜索 */
    int ifndef_macro_saved; /* 保存的ifndef_macro */
    int *如果已宏定义_堆_ptr; /* 文件开头的ifdef_stack值 */
    int include_next_index; /* 下一个搜索路径 */
    char 文件名[1024];    /* 文件名 */
    char *true_filename; /* 文件名未被＃行指令修改 */
    unsigned char unget[4];
    unsigned char buffer[1]; /* CH_缓冲区结尾字符的额外大小 */
} 缓冲文件;

#define CH_缓冲区结尾   '\\'       /* 缓冲区结尾或文件中的'\ 0'char */
#define CH_文件结尾   (-1)   /* 文件结尾 */

/* 用于记录标识符 */
typedef struct 单词字符串 {
    int *str;
    int len;
    int lastlen;
    int allocated_len;
    int 最后_行_号;
    int save_line_num;
    /* 用于使用begin / 结束_宏（）链接单词字符串 */
    struct 单词字符串 *prev;
    const int *prev_ptr;
    char alloc;
} 单词字符串;

/* GNUC属性定义 */
typedef struct 属性定义 {
    struct 符号属性 a;
    struct 函数属性 f;
    struct 段 *section;
    符号 *cleanup_func;
    int alias_target; /* 标识符 */
    int 汇编_label; /* 关联的asm标签 */
    char attr_mode; /* __attribute__((__mode__(...))) */
} 属性定义;

/* 内联函数 */
typedef struct 内联函数 {
    单词字符串 *函数_字符串;
    符号 *sym;
    char 文件名[1];
} 内联函数;

/* 导入文件缓存，用于更快地查找文件，并在导入文件受#ifndef ... #endif保护的情况下消除包含 */
typedef struct 导入文件缓存 {
    int ifndef_macro;
    int once;
    int hash_next; /* 如果没有则为-1 */
    char 文件名[1]; /* #include中指定的路径 */
} 导入文件缓存;

#define 缓存的_导入文件_哈希_大小 32

#ifdef 配置_ZHI_汇编
typedef struct 表达式值 {
    uint64_t v;
    符号 *sym;
    int pcrel;
} 表达式值;

#define 最大_汇编_操作数 30
typedef struct 汇编操作数 {
    int id; /* GCC 3可选标识符（如果仅支持数字，则为0 */
    char *constraint;
    char 汇编_str[16]; /* 为操作数计算的asm字符串 */
    堆栈值 *vt; /* 表达式的C值 */
    int ref_index; /* 如果> = 0，则引用输出约束 */
    int input_index; /* 如果> = 0，则引用输入约束 */
    int priority; /* 优先级，用于分配寄存器 */
    int reg; /* 如果> = 0，则此操作数使用的寄存器号 */
    int is_llong; /* 如果双寄存器值，则为true */
    int is_memory; /* 如果内存操作数为true */
    int is_rw;     /* 用于'+'修饰符 */
} 汇编操作数;
#endif

/* 额外的符号属性（不在单词表中） */
struct 额外_符号_属性 {
    unsigned got_offset;
    unsigned plt_offset;
    int plt_sym;
    int dyn_index;
#ifdef ZHI_TARGET_ARM
    unsigned char plt_thumb_stub:1;
#endif
};
typedef struct {
    char 类型;
    char 名称[1];
}文件名称类型结构;

typedef struct 知心状态机 {
    unsigned char 显示信息; /* 如果为true，则在编译期间显示一些信息 */
    unsigned char 不添加标准头; /* 如果为true，则不添加标准标头*/
    unsigned char 不添加标准库; /* 如果为true，则不添加任何标准库 */
    unsigned char 不使用通用符号; /* 如果为true，请不要对.bss数据使用通用符号 */
    unsigned char 执行_静态链接; /* 如果为true，则执行静态链接 */
    unsigned char 导出所有符号; /* 如果为true，则导出所有符号 */
    unsigned char 先解析当前模块符号; /* 如果为true，请先解析当前模块中的符号 */
    unsigned char 文件类型; /* 编译文件类型（NONE，C，ASM） */
    unsigned char 编译优化; /* 仅用于#define __OPTIMIZE__ */
    unsigned char 选项_线程; /* -pthread选项 */
    unsigned char 启用新的dtags; /* -Wl-启用新的dtags */
    unsigned int  ZHI版本; /* 受支持的C ISO版本199901（默认），201112，... */

    char *zhi_库_路径; /* CONFIG_ZHIDIR或-B选项 */
    char *基本名称; /* 如命令行上所指定（-基本名称） */
    char *动态库路径; /* 如命令行上所指定（-Wl，-动态库路径 =） */

    /* 输出类型，请参见ZHI_OUTPUT_XXX */
    int 输出_类型;
    /* 输出格式，请参见ZHI_输出_格式_xxx */
    int 输出_格式;

    /* C语言选项 */
    unsigned char 字符串_无符号;
    unsigned char 前导_下划线;
    unsigned char 允许_匿名联合和结构;
    unsigned char 允许_在标识符中使用美元符号;
    unsigned char 模拟_对齐位域MS算法; /* 如果为true，则模拟用于对齐位域的MS算法 */

    /* 警告开关 */
    unsigned char 警告_写字符串;
    unsigned char 警告_不支持;
    unsigned char 警告_错误;
    unsigned char 警告_none;
    unsigned char 警告_隐式函数声明;
    unsigned char 警告_gcc兼容;

    /* 使用调试符号进行编译（如果执行期间出错，则使用它们） */
    unsigned char 执行_调试;
    unsigned char 执行_跟踪;
#ifdef 配置_ZHI_边界检查
    /* 使用内置的内存和边界检查器进行编译 */
    unsigned char 执行_边界_检查器;
#endif
#ifdef ZHI_TARGET_ARM
    enum 浮动_abi 浮动_abi; /* 生成代码的浮动ABI*/
#endif
    int 运行_测试; /* 第n个要与-dt -run一起运行的测试 */

    目标地址_类型 代码段_地址;
    unsigned char 已有_代码段_地址;

    unsigned 分段_对齐;

    unsigned char gnu_扩展;
    unsigned char zhi_扩展;

    char *加载_符号; /* 加载时调用的符号（当前未使用） */
    char *卸载_符号; /* 卸载时调用的符号（当前未使用）*/

#ifdef ZHI_TARGET_I386
    int 段_大小; /* 32.使用i386汇编程序（.code16）可以是16 */
#endif
#ifdef ZHI_TARGET_X86_64
    unsigned char 支持mno和sse;
#endif

    /* 所有已加载的dll（包括已加载的dll所引用的那些）的数组 */
    DLL参考 **已加载的_dll数组;
    int 数量_已加载的_dll数组;

    char **包含_路径;
    int 数量_包含_路径;

    char **系统包含_路径;
    int 数量_系统包含_路径;

    char **库_路径;
    int 数量_库_路径;

    /* crt？.o对象路径 */
    char **crt_路径;
    int 数量_crt_路径;

    /* -D / -U 选项 */
    动态字符串 命令行_定义;
    /* -include 选项 */
    动态字符串 命令行_包含;

    /* 错误处理 */
    void *错误_不透明;
    void (*错误_函数)(void *不透明, const char *msg);
    int 错误_设置_jmp_启用;
    jmp_buf 错误_jmp_缓存;
    int 数量_错误;

    /* 预处理输出文件（-E） */
    FILE *预处理输出文件;
    enum {
	LINE_MACRO_OUTPUT_FORMAT_GCC,
	LINE_MACRO_OUTPUT_FORMAT_NONE,
	LINE_MACRO_OUTPUT_FORMAT_STD,
    LINE_MACRO_OUTPUT_FORMAT_P10 = 11
    } P标号; /* -P switch */
    char DX标号; /* -dX value */

    /* -MD / -MF：收集此编译的依赖项 */
    char **目标_依赖;
    int 数量_目标_依赖;

    /* 汇编 */
    缓冲文件 *包含_堆[包含_堆栈_大小];
    缓冲文件 **包含_堆_ptr;

    int 如果已宏定义_堆[如果定义_堆栈_大小];
    int *如果已宏定义_堆_ptr;

    /* #ifndef MACRO随附的导入文件 */
    int 缓存_包含数_哈希[缓存的_导入文件_哈希_大小];
    导入文件缓存 **缓存_包含数;
    int 数量_缓存_包含数;

    /* #pragma pack堆栈 */
    int 包_堆[包_堆栈_大小];
    int *包_堆_ptr;
    char **语法_库数;
    int 数量_语法_库数;

    /* 内联函数存储为令牌列表，并且仅在引用时最后编译 */
    struct 内联函数 **内联_函数数;
    int 数量_内联_函数数;

    /* 段数 */
    段 **段数;
    int 数量_段数; /* 段数，包括第一个虚拟节 */

    段 **私有_节数;
    int 数量_私有_节数; /* 私有部分编码 */

    /* got & plt 处理 */
    段 *got;
    段 *plt;

    /* 预定义的部分 :
     * 生成代码_段(是包含所生成代码的部分。ind包含代码部分中的当前位置。)
     * 初始化数据_部分(包含初始化数据)
     * 未初始化数据_部分(包含未初始化的数据)
     * */
    段 *生成代码_段, *初始化数据_部分, *未初始化数据_部分;
    段 *通用_部分;
    段 *当前_生成代码_段; /* 生成功能代码的当前部分 */
#ifdef 配置_ZHI_边界检查
    /* 边界检查相关部分 */
    段 *全局边界_部分; /* 包含全局数据边界描述,激活边界检查时使用 */
    段 *本地边界_部分; /* 包含本地数据边界描述 */
#endif
    /* 符号部分 */
    段 *单词表_部分;
    段 *符号调试_部分;/* 调试部分 */
    int 新的_未定义的_符号; /* 自上次new_undef_sym（）以来是否存在新的未定义符号 */
    段 *动态单词表_部分;/* 临时动态符号节（用于dll加载） */
    段 *导出的动态单词表;/* 导出的动态符号部分 */
    段 *全局单词表副本;/* 全局symtab_section变量的副本 */
    struct 额外_符号_属性 *符号_额外属性;/* symtab符号的额外属性（例如，GOT / PLT值） */
    int 数量_符号_额外属性;
    /* ptr到下一个重定位项已重用 */
    ElfW_Rel *qrel;
#   define qrel 状态机1->qrel

#ifdef ZHI_TARGET_PE
    /* PE info */
    int pe_子系统;
    unsigned pe_特征;
    unsigned pe_文件_对齐;
    unsigned pe_堆栈_大小;
    目标地址_类型 pe_图像库;
# ifdef ZHI_TARGET_X86_64
    段 *uw_pdata;
    int uw_sym;
    unsigned uw_offs;
# endif
# define ELF_OBJ_ONLY
#endif

#ifdef ZHI_TARGET_MACHO
# define ELF_OBJ_ONLY
#endif

#ifndef ELF_OBJ_ONLY
    int 数量_符号_版本数;
    struct 符号_版本 *符号_版本数;
    int 数量_符号_到_版本;
    int *符号_到_版本;
    int dt_必须的数码;
    段 *versym_section;
    段 *verneed_section;
#endif

#ifdef ZHI_是_本机
    const char *运行时_入口;
    void **运行时_内存;
    int 数量_运行时_内存;
#endif

#ifdef ZHI_配置_记录回滚
    int 运行时_num_callers;
#endif

    int fd, cc; /* zhi_load_ldscript使用 */

    /* 基准信息 */
    int 总_idents;
    int 总_行数;
    int 总_字节数;

    /* 选项-dnum（用于一般开发目的） */
    int g_调试;

    /* 用于目标文件的警告/错误*/
    const char *当前_文件名;

    /* 仅由main和zhi_parse_args使用 */
    文件名称类型结构 **文件数; /* 在命令行上看到的文件 */
    int 数量_文件数; /* 文件数 */
    int 数量_库数; /* 库数 */
    char *输出文件; /* 输出文件名 */
    unsigned char 选项_可重定位目标文件; /* 选项 -r */
    unsigned char 显示_编译统计; /* 选项 -bench */
    int 生成_依赖; /* 选项 -MD  */
    char *依赖_输出文件; /* 选项-MF */
    int 参数数量;
    char **参数数组;
}知心状态机;

/* 值栈.当前值可以是: */
#define VT_值掩码   0x003f  /* 值位置的掩码，寄存器或: */
#define VT_VC常量     0x0030  /* vc中的常量（必须是第一个非寄存器值） */
#define VT_LLOCAL    0x0031  /* 左值，堆栈偏移量 */
#define VT_LOCAL     0x0032  /* 堆栈偏移 */
#define VT_CMP       0x0033  /* 该值存储在处理器标志中（在vc中） */
#define VT_JMP       0x0034  /* 值是jmp的结果true（偶数） */
#define VT_JMPI      0x0035  /* 值是jmp错误（奇数）的结果 */
#define VT_LVAL      0x0100  /* var是一个左值 */
#define VT_符号       0x0200  /* 添加符号值 */
#define VT_强制转换  0x0C00  /* 值必须强制转换为正确值（用于存储在整数寄存器中的char / short） */
#define VT_强制边界检查 0x4000  /* 边界检查必须在取消引用值之前进行 */
#define VT_有界的   0x8000  /* 值是有界的。 边界函数调用点的地址在vc中 */
/* 类型 */
#define VT_基本类型       0x000f /* 基本类型掩码 */
#define VT_无类型             0
#define VT_字节             1  /* 有符号字节类型 */
#define VT_短整数            2
#define VT_整数              3
#define VT_长长整数            4  /* 64位整数 */
#define VT_指针              5  /* 指针*/
#define VT_函数             6  /* 函数类型 */
#define VT_结构体           7  /* 结构/联合定义 */
#define VT_浮点            8  /* IEEE 浮点型 */
#define VT_双精度           9  /* IEEE 双精度 */
#define VT_长双精度         10  /* IEEE 长双精度*/
#define VT_逻辑            11  /* ISOC99布尔类型 */
#define VT_128位整数           13  /* 128位整数。 仅用于x86-64 ABI */
#define VT_128位浮点          14  /* 128位浮点数。 仅用于x86-64 ABI */

#define VT_无符号    0x0010  /* 无符号类型 */
#define VT_显式符号     0x0020  /* 明确签名或未签名 */
#define VT_数组      0x0040  /* 数组类型（也有VT_指针）*/
#define VT_位域    0x0080  /* 位域修饰符 */
#define VT_常量    0x0100  /* const修饰符 */
#define VT_易变    0x0200
#define VT_变长数组         0x0400  /* VLA类型（也具有VT_指针和VT_ARRAY） */
#define VT_长整数        0x0800  /* 长型（也有VT_INTrsp。VT_长长整数） */

/* 存储 :在解析期间，对象的存储也以整数类型存储*/
#define VT_外部  0x00001000  /* 外部定义 */
#define VT_静态  0x00002000  /* 静态变量 */
#define VT_别名 0x00004000  /* typedef定义 */
#define VT_内联  0x00008000  /* 内联定义 */
/* 当前未使用：0x000 [1248] 0000  */

#define VT_结构体_转换 20     /* 位字段移位值的移位（32-2 * 6） */
#define VT_结构体_掩码 (((1U << (6+6)) - 1) << VT_结构体_转换 | VT_位域)
#define BIT_POS(t) (((t) >> VT_结构体_转换) & 0x3f)
#define BIT_大小(t) (((t) >> (VT_结构体_转换 + 6)) & 0x3f)

#define VT_共用体    (1 << VT_结构体_转换 | VT_结构体)
#define VT_枚举     (2 << VT_结构体_转换) /* 整数类型确实是一个枚举*/
#define VT_枚举_变长 (3 << VT_结构体_转换) /* 整数类型确实是一个枚举常量 */

#define 是_枚举(t) ((t & VT_结构体_掩码) == VT_枚举)
#define 是_枚举_变长(t) ((t & VT_结构体_掩码) == VT_枚举_变长)
#define 是_共用体(t) ((t & (VT_结构体_掩码|VT_基本类型)) == VT_共用体)

/* 类型遮罩（存储除外） */
#define VT_存储 (VT_外部 | VT_静态 | VT_别名 | VT_内联)
#define VT_类型 (~(VT_存储|VT_结构体_掩码))

/* 该符号由zhiasm.c首先创建 */
#define VT_汇编 (VT_无类型 | VT_无符号)
#define 是_汇编_符号(sym) (((sym)->type.t & (VT_基本类型 | VT_汇编)) == VT_汇编)

/* 常规：设置/获取位掩码M的伪位域值 */
#define BFVAL(M,N) ((unsigned)((M) & ~((M) << 1)) * (N))
#define BFGET(X,M) (((X) & (M)) / BFVAL(M,1))
#define BFSET(X,M,N) ((X) = ((X) & ~(M)) | BFVAL(M,N))

/* 标识符值 */

/* 有条件的操作 */
#define 双符号_逻辑与  0x90
#define 双符号_逻辑或   0x91
/* 警告：以下标识符编码取决于i386 asm代码 */
#define 符号_ULT 0x92
#define 符号_UGE 0x93
#define 双符号_等于  0x94
#define 双符号_不等于  0x95
#define 符号_ULE 0x96
#define 符_UGT 0x97
#define 符_Nset 0x98
#define 符_Nclear 0x99
#define 符_LT  0x9c
#define 双符号_大于等于  0x9d
#define 双符号_小于等于  0x9e
#define 符_GT  0x9f

#define 符_ISCOND(t) (t >= 双符号_逻辑与 && t <= 符_GT)

#define 双符号_自减1     0x80 /* -- */
#define 符_MID     0x81 /* inc / dec，使常量不变 */
#define 双符号_自加1     0x82 /* ++ */
#define 符_无符除法    0x83 /* 无符号除法 */
#define 符_无符取模运算    0x84 /* 无符号模运算 */
#define 符_指针除法    0x85 /* 使用指针的未定义舍入的快速除法 */
#define 符_无符32位乘法   0x86 /* 无符号 32x32 -> 64 mul */
#define 符_加进位生成   0x87 /* 加进位生成 */
#define 符_加进位使用   0x88 /* 加进位使用 */
#define 符_减借位生成   0x89 /* 减借位生成 */
#define 符_减借位使用   0x8a /* 减借位使用 */
#define 双符号_左位移     '<' /* 左移 */
#define 双符号_右位移     '>' /* 右移 */
#define 符_SHR     0x8b /* 无符号右移 */

#define 双符号_结构体指针运算符   0xa0 /* -> */
#define 符_三个圆点    0xa1 /* 三个点 */
#define 双符号_2个圆点号 0xa2 /* C++ 标识符 ? */
#define 双符号_2个井号 0xa3 /* ## 预处理标识符 */
#define 符_占位符 0xa4 /* C99中定义的占位符标识符 */
#define 符_NOSUBST 0xa5 /* 表示以下标识符已被pp'd */
#define 符_PPJOIN  0xa6 /* 正确位置的“ ##”表示粘贴 */

/* 赋值运算符 */
#define 双符号_先求和后赋值   0xb0
#define 双符号_先求差后赋值   0xb1
#define 双符号_先求积后赋值   0xb2
#define 双符号_先求商后赋值   0xb3
#define 双符号_先取模后赋值   0xb4
#define 双符号_先求位与后赋值   0xb5
#define 双符号_先求位或后赋值    0xb6
#define 双符号_先求异或后赋值   0xb7
#define 符_A_SHL   0xb8
#define 符_A_SAR   0xb9

#define 符_ASSIGN(t) (t >= 双符号_先求和后赋值 && t <= 符_A_SAR)
#define 符_ASSIGN_OP(t) ("+-*/%&|^<>"[t - 双符号_先求和后赋值])

/* 带有值的标识符（在附加单词字符串中/ tokc中） --> */
#define 常量_字符型  0xc0 /* tokc中的char常量 */
#define 常量_长字符型   0xc1
#define 常量_整数    0xc2 /* tokc中的整数 */
#define 常量_无符整数   0xc3 /* 无符号int常数 */
#define 常量_长长整数  0xc4 /* 长长常量 */
#define 常量_无符长长整数 0xc5 /* 无符号长长常量 */
#define 常量_长整数   0xc6 /* 长常量 */
#define 常量_无符长整数  0xc7 /* 无符号长常量 */
#define 常量_字符串     0xc8 /* tokc中的字符串指针 */
#define 常量_长字符串    0xc9
#define 常量_浮点型  0xca /* 浮点常量 */
#define 常量_双精度 0xcb /* 双精度常量 */
#define 常量_长双精度 0xcc /* 长双精度常量 */
#define 常量_预处理编号   0xcd /* 预处理编号 */
#define 常量_预处理字符串   0xce /* 预处理字符串 */
#define 常量_行号 0xcf /* 行号信息 */

#define 符_有_值(t) (t >= 常量_字符型&& t <= 常量_行号)

#define 符_文件结尾       (-1)  /* 文件结尾 */
#define 符_换行  10    /* 换行 */

/* 所有标识符和字符串在其上都具有标记 */
#define 符_识别 256

#define 定义_汇编(x) 定义(符_汇编_ ## x, #x)
#define 符_汇编_int 关键词_INT
#define 定义_汇编目录(x) 定义(符_汇编DIR_ ## x, "." #x)
#define 定义_汇编目录_第一个 符_汇编DIR_byte
#define 定义_汇编目录_最后一个 符_汇编DIR_section

#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
/* 仅用于i386 asm操作码定义 */
#define 定义_BWL(x) \
 定义(符_汇编_ ## x ## b, #x "b") \
 定义(符_汇编_ ## x ## w, #x "w") \
 定义(符_汇编_ ## x ## l, #x "l") \
 定义(符_汇编_ ## x, #x)
#define 定义_WL(x) \
 定义(符_汇编_ ## x ## w, #x "w") \
 定义(符_汇编_ ## x ## l, #x "l") \
 定义(符_汇编_ ## x, #x)
#ifdef ZHI_TARGET_X86_64
# define 定义_BWLQ(x) \
 定义(符_汇编_ ## x ## b, #x "b") \
 定义(符_汇编_ ## x ## w, #x "w") \
 定义(符_汇编_ ## x ## l, #x "l") \
 定义(符_汇编_ ## x ## q, #x "q") \
 定义(符_汇编_ ## x, #x)
# define 定义_WLQ(x) \
 定义(符_汇编_ ## x ## w, #x "w") \
 定义(符_汇编_ ## x ## l, #x "l") \
 定义(符_汇编_ ## x ## q, #x "q") \
 定义(符_汇编_ ## x, #x)
# define 定义_BWLX 定义_BWLQ
# define 定义_WLX 定义_WLQ
/* number of sizes + 1 */
# define NBWLX 5
#else
# define 定义_BWLX 定义_BWL
# define 定义_WLX 定义_WL
/* number of sizes + 1 */
# define NBWLX 4
#endif

#define 定义_FP1(x) \
 定义(符_汇编_ ## f ## x ## s, "f" #x "s") \
 定义(符_汇编_ ## fi ## x ## l, "fi" #x "l") \
 定义(符_汇编_ ## f ## x ## l, "f" #x "l") \
 定义(符_汇编_ ## fi ## x ## s, "fi" #x "s")

#define 定义_FP(x) \
 定义(符_汇编_ ## f ## x, "f" #x ) \
 定义(符_汇编_ ## f ## x ## p, "f" #x "p") \
 定义_FP1(x)

#define 定义_ASMTEST(x,suffix) \
 定义_汇编(x ## o ## suffix) \
 定义_汇编(x ## no ## suffix) \
 定义_汇编(x ## b ## suffix) \
 定义_汇编(x ## c ## suffix) \
 定义_汇编(x ## nae ## suffix) \
 定义_汇编(x ## nb ## suffix) \
 定义_汇编(x ## nc ## suffix) \
 定义_汇编(x ## ae ## suffix) \
 定义_汇编(x ## e ## suffix) \
 定义_汇编(x ## z ## suffix) \
 定义_汇编(x ## ne ## suffix) \
 定义_汇编(x ## nz ## suffix) \
 定义_汇编(x ## be ## suffix) \
 定义_汇编(x ## na ## suffix) \
 定义_汇编(x ## nbe ## suffix) \
 定义_汇编(x ## a ## suffix) \
 定义_汇编(x ## s ## suffix) \
 定义_汇编(x ## ns ## suffix) \
 定义_汇编(x ## p ## suffix) \
 定义_汇编(x ## pe ## suffix) \
 定义_汇编(x ## np ## suffix) \
 定义_汇编(x ## po ## suffix) \
 定义_汇编(x ## l ## suffix) \
 定义_汇编(x ## nge ## suffix) \
 定义_汇编(x ## nl ## suffix) \
 定义_汇编(x ## ge ## suffix) \
 定义_汇编(x ## le ## suffix) \
 定义_汇编(x ## ng ## suffix) \
 定义_汇编(x ## nle ## suffix) \
 定义_汇编(x ## g ## suffix)

#endif /* 定义的ZHI_TARGET_I386 || 定义的ZHI_TARGET_X86_64*/

enum zhi_标识符 {
    符_LAST = 符_识别 - 1
#define 定义(id, str) ,id
#include "标识符.h"
#undef 定义
};

/* 关键词: 单词编码 >= 符_识别 && 单词编码 < 符_没识别 */
#define 符_没识别 关键词_DEFINE

/* ------------ hexinku.c ------------ */

静态_外部 知心状态机 *zhi_状态;

/* zhi 当前使用的主要的公共函数 */
静态_函数 char *p字符串复制(char *buf, size_t buf_size, const char *s);
静态_函数 char *连接_字符串(char *buf, size_t buf_size, const char *s);
静态_函数 char *复制_字符串(char *out, const char *in, size_t num);
公共_函数 char *取_文件基本名(const char *name);
公共_函数 char *取_文件扩展名 (const char *name);

#ifndef 内存_调试
公共_函数 void 内存_释放(void *ptr);
公共_函数 void *内存_申请(unsigned long 内存容量);
公共_函数 void *内存_初始化(unsigned long size);
公共_函数 void *内存_重分配容量(void *ptr, unsigned long size);
公共_函数 char *字符串_宽度加1(const char *str);
#else
#define 内存_释放(ptr)           zhi_释放_调试(ptr)
#define 内存_申请(size)        zhi_分配_调试(size, __FILE__, __LINE__)
#define 内存_初始化(size)       zhi_分配z_调试(size, __FILE__, __LINE__)
#define 内存_重分配容量(ptr,size)   zhi_重新分配_调试(ptr, size, __FILE__, __LINE__)
#define 字符串_宽度加1(str)         zhi_字符串dup_调试(str, __FILE__, __LINE__)
公共_函数 void zhi_释放_调试(void *ptr);
公共_函数 void *zhi_分配_调试(unsigned long size, const char *file, int line);
公共_函数 void *zhi_分配z_调试(unsigned long size, const char *file, int line);
公共_函数 void *zhi_重新分配_调试(void *ptr, unsigned long size, const char *file, int line);
公共_函数 char *zhi_字符串dup_调试(const char *str, const char *file, int line);
#endif

#define free(p) 用_zhi_释放(p)
#define malloc(s) 用_zhi_内存分配(s)
#define realloc(p, s) 用_zhi_重新分配(p, s)
#undef strdup
#define strdup(s) use_zhi_strdup(s)
公共_函数 void 错误提示(const char *fmt, ...) PRINTF_LIKE(1,2);
公共_函数 无返回 void 错误(const char *fmt, ...) PRINTF_LIKE(1,2);
公共_函数 void _zhi_警告(const char *fmt, ...) PRINTF_LIKE(1,2);

/* 其他公用事业 */
静态_函数 void 动态数组_追加元素(void *ptab, int *数量_ptr, void *data);
静态_函数 void 动态数组_重分配容量(void *pp, int *n);
/*****************************动态字符串-开始*****************************/
静态_内联 void 动态字符串_追加单个字符(动态字符串 *cstr, int 当前取到的源码字符);
静态_函数 void 动态字符串_cat(动态字符串 *cstr, const char *str, int len);
静态_函数 void 动态字符串_追加一个宽字符(动态字符串 *cstr, int 当前取到的源码字符);
静态_函数 void 动态字符串_初始化(动态字符串 *cstr);
静态_函数 void 动态字符串_释放(动态字符串 *cstr);
静态_函数 int 动态字符串_打印(动态字符串 *cs, const char *fmt, ...) PRINTF_LIKE(2,3);
静态_函数 void 动态字符串_重置(动态字符串 *cstr);
/*****************************动态字符串-结束*****************************/
静态_内联 void 符号_释放(符号 *sym);
静态_函数 符号 *符号_推送2(符号 **ps, int v, int t, int c);
静态_函数 符号 *符号_查找2(符号 *s, int v);
静态_函数 符号 *符号_压入栈(int v, C类型 *type, int r, int c);
静态_函数 void 符号_弹出(符号 **ptop, 符号 *b, int keep);
静态_内联 符号 *结构体_查询(int v);
静态_内联 符号 *符号_查询(int v);
静态_函数 符号 *推送_一个_全局标识符(int v, int t, int c);

静态_函数 void 打开缓存文件(知心状态机 *状态机1, const char *文件名, int initlen);
静态_函数 int 打开一个新文件(知心状态机 *状态机1, const char *文件名);
静态_函数 void 关闭文件(void);

静态_函数 int 添加内部文件(知心状态机 *状态机1, const char *文件名, int flags);
/* flags: */
#define AFF_打印_错误     0x10 /* 如果找不到文件，则打印错误 */
#define AFF_加载_引用的DLL  0x20 /* 从另一个dll加载引用的dll */
#define 文件格式_类型_BIN        0x40 /* 要添加的文件是二进制的 */
#define AFF_从存档加载_所有对象   0x80 /* 从存档加载所有对象 */
/* s->文件类型: */
#define 文件格式_类型_NONE   0
#define 文件格式_类型_Z      1 /*知语言的后缀扩展名.z*/
#define 文件格式_类型_汇编    2
#define 文件格式_类型_ASMPP  4
#define 文件格式_类型_库    8
#define 文件格式_类型_掩码   (15 | 文件格式_类型_BIN)
/* zhi_目标文件_类型（...）中的值*/
#define AFF_二进制_REL 1
#define AFF_二进制_DYN 2
#define AFF_二进制_AR  3
#define AFF_二进制_C67 4


#ifndef ZHI_TARGET_PE
静态_函数 int zhi_添加_crt(知心状态机 *s, const char *文件名);
#endif
静态_函数 int 添加dll文件(知心状态机 *s, const char *文件名, int flags);
#ifdef 配置_ZHI_边界检查
静态_函数 void zhi_新增_边界检查(知心状态机 *状态机1);
#endif
#ifdef ZHI_配置_记录回滚
静态_函数 void zhi_add_btstub(知心状态机 *状态机1);
#endif
静态_函数 void 处理实用注释库(知心状态机 *状态机1);
公共_函数 int 添加库错误(知心状态机 *s, const char *f);
公共_函数 void 显示编译统计信息(知心状态机 *s, unsigned total_time);
公共_函数 int 解析命令行参数(知心状态机 *s, int *参数数量, char ***参数数组, int optind);
#ifdef _WIN32
静态_函数 char *WIN32规范化斜杠(char *path);
#endif

/* 解析命令行参数 返回码: */
#define 指令_HELP 1
#define 指令_ABOUT 2
#define 指令_V 3
#define 指令_打印_目录 4
#define 指令_AR 5
#define 指令_IMPDEF 6
#define 指令_M32 32
#define 指令_M64 64

/* ------------ 词法分析.c ------------ */

静态_外部 struct 缓冲文件 *file;
静态_外部 int 当前取到的源码字符, 单词编码;
静态_外部 恒定值 单词值;
静态_外部 const int *宏_ptr;
静态_外部 int 解析_标记;
静态_外部 int 标识符_标记;
静态_外部 动态字符串 当前单词字符串; /* 当前单词字符串（如果有） */

/* 显示基准信息 */
静态_外部 int 单词_识别号;
静态_外部 单词存储结构 **单词表;

#define 符_标记_行开始前   0x0001 /* 行前的开始 */
#define 符_标记_文件开始前   0x0002 /* 文件开始之前 */
#define 符_标记_结束如果 0x0004 /* 找到与#ifdef开头匹配的endif */
#define 符_标记_文件结尾   0x0008 /* 文件结尾 */

#define 解析_标记_预处理 0x0001 /* 激活预处理 */
#define 解析_标记_标识符_数字    0x0002 /* 返回数字而不是符_预处理数字 */
#define 解析_标记_换行符   0x0004 /* 换行符作为标识符返回。 换行符也返回eof */
#define 解析_标记_汇编_文件 0x0008 /* 我们正在处理一个asm文件：“＃”可用于行注释等。 */
#define 解析_标记_空间     0x0010 /* next（）返回空间标记（对于-E） */
#define 解析_标记_接受_转义 0x0020 /* next（）返回'\\'标识符 */
#define 解析_标记_单词字符串    0x0040 /* 返回已解析的字符串，而不是符_预处理字符串 */

/* 等值_表 flags: */
#define IS_SPC 1
#define IS_ID  2
#define IS_NUM 4

静态_函数 单词存储结构 *单词表_查找(const char *str, int len);
静态_函数 const char *取_单词字符串(int v, 恒定值 *cv);
静态_函数 void 开始_宏(单词字符串 *str, int alloc);
静态_函数 void 结束_宏(void);
静态_函数 int 设置_等值数(int c, int val);
静态_内联 void 单词字符串_处理(单词字符串 *s);
静态_函数 单词字符串 *单词字符串_分配(void);
静态_函数 void 单词字符串_释放(单词字符串 *s);
静态_函数 void 单词字符串_释放_字符串(int *str);
静态_函数 void 单词字符串_增加大小(单词字符串 *s, int t);
静态_函数 void 单词字符串中添加当前解析的字符(单词字符串 *s);
静态_内联 void 宏定义_处理(int v, int macro_type, int *str, 符号 *first_arg);
静态_函数 void 未宏定义_符号为NULL(符号 *s);
静态_内联 符号 *宏定义_查找(int v);
静态_函数 void 释放_宏定义堆栈(符号 *b);
静态_函数 符号 *标签_查找(int v);
静态_函数 符号 *标签_推送(符号 **ptop, int v, int flags);
静态_函数 void 标签_弹出(符号 **ptop, 符号 *slast, int keep);
静态_函数 void 解析_宏定义(void);
静态_函数 void _预处理(int is_bof);
静态_函数 void 带有宏替换的下个标记(void);/*带有宏替换的下个标记()读取当前文件中的下一个标记*/
静态_内联 void 设为_指定标识符(int last_tok);
静态_函数 void 开始_预处理(知心状态机 *状态机1, int is_asm);
静态_函数 void 结束_预处理(知心状态机 *状态机1);
静态_函数 void 词法分析_开始(知心状态机 *s);
静态_函数 void zhi词法_删除(知心状态机 *s);
静态_函数 int 预处理_源文件(知心状态机 *状态机1);
静态_函数 void 跳过(int c);
静态_函数 无返回 void 应为(const char *msg);

/* 除换行符外的空格 */
static inline int 是_空格(int 当前取到的源码字符) {
    return 当前取到的源码字符 == ' ' || 当前取到的源码字符 == '\t' || 当前取到的源码字符 == '\v' || 当前取到的源码字符 == '\f' || 当前取到的源码字符 == '\r';
}
static inline int 是id(int c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}
static inline int 是数字(int c) {
    return c >= '0' && c <= '9';
}
static inline int isoct(int c) {
    return c >= '0' && c <= '7';
}
static inline int toup(int c) {
    return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
}

/* ------------ 语法分析.c ------------ */

#define 符号_池_NB (8192 / sizeof(符号))

静态_外部 符号 *全局符号_堆栈;
静态_外部 符号 *局部符号_堆栈;
静态_外部 符号 *局部符号_标签_堆栈;
静态_外部 符号 *全局符号_标签_堆栈;
静态_外部 符号 *宏定义符号_堆栈;
静态_外部 C类型 整数_类型, 函数_旧_类型, 字符_指针_类型;
静态_外部 堆栈值 *栈顶值;
静态_外部 int 返回符号, 匿名符号索引, 输出代码索引, 局部变量索引;

静态_外部 int 需要_常量; /* 如果需要常量，则为true */
静态_外部 int 不需要_代码生成; /* 如果不需要为表达式生成代码，则为true */
静态_外部 int 全局_分配复合字符;  /* 如果必须在全局范围内分配复合文字，则为true（在初始化程序解析期间使用） */
静态_外部 C类型 当前函数_返回类型; /* 当前函数的返回类型（由返回指令使用）*/
静态_外部 int 当前函数_可变参数; /* 如果当前函数是可变参数，则为true*/
静态_外部 int 函数_vc;
静态_外部 const char *函数名称;

静态_函数 void zhi_调试_开始(知心状态机 *状态机1);
静态_函数 void zhi_结束_调试(知心状态机 *状态机1);
静态_函数 void zhi_调试_导入文件的开头(知心状态机 *状态机1);
静态_函数 void zhi_调试_导入文件的结尾(知心状态机 *状态机1);
静态_函数 void zhi_调试_备用文件(知心状态机 *状态机1, const char *文件名);
静态_函数 void zhi_调试_函数开始(知心状态机 *状态机1, 符号 *sym);
静态_函数 void zhi_调试_函数结束(知心状态机 *状态机1, int size);
静态_函数 void zhi_调试_行(知心状态机 *状态机1);

静态_函数 void zhi语法_初始化(知心状态机 *状态机1);
静态_函数 int zhi语法_编译(知心状态机 *状态机1);
静态_函数 void zhi语法_完成(知心状态机 *状态机1);
静态_函数 void 检查_堆栈值(void);

静态_内联 int 是_浮点型(int t);
静态_函数 int ieee_有限的(double d);
静态_函数 int 精确_对数2p1(int i);
静态_函数 void 测试_左值(void);
静态_函数 void 压入整数常量(int v);
静态_函数 ELF符号 *elf符号(符号 *);
静态_函数 void 更新_存储(符号 *sym);
静态_函数 符号 *外部_全局_符号(int v, C类型 *type);
静态_函数 void vset(C类型 *type, int r, int v);
静态_函数 void vset_VT_CMP(int op);
静态_函数 void vswap(void);
静态_函数 void 推送对_全局符号V_的引用(C类型 *type, int v);
静态_函数 void vrote(堆栈值 *e, int n);
静态_函数 void vrott(int n);
静态_函数 void vrotb(int n);
#if 指针_大小 == 4
静态_函数 void 整数扩展(void);
#endif
#ifdef ZHI_TARGET_ARM
静态_函数 int 查找rc2寄存器_如果不存在就调用rc(int rc, int rc2);
#endif
静态_函数 void vpushv(堆栈值 *v);
静态_函数 void 将r保存到_内存堆栈(int r);
静态_函数 void 将r保存到_内存堆栈_最多n个条目(int r, int n);
静态_函数 int 查找释放的rc寄存器_如果不存在就保存一个寄存器(int rc);
静态_函数 void 保存_寄存器最多n个堆栈条目(int n);
静态_函数 void 获取栈顶值地址(void);
静态_函数 int 将rc寄存器值存储在栈顶值中(int rc);
静态_函数 void 将rc寄存器值存储在栈顶值中2(int rc1, int rc2);
静态_函数 void 弹出堆栈值(void);
静态_函数 void 通用_操作(int op);
静态_函数 int 类型_大小(C类型 *type, int *a);
静态_函数 void 修改类型_指针类型(C类型 *type);
静态_函数 void 将栈顶值_存储在堆栈左值(void);
静态_函数 void inc(int post, int c);
静态_函数 void 解析_多_字符串 (动态字符串 *astr, const char *msg);
静态_函数 void 解析_汇编_字符串(动态字符串 *astr);
静态_函数 void 间接的(void);
静态_函数 void 一元(void);
静态_函数 void 通用表达式(void);
静态_函数 int 表达式_常量(void);
#if defined 配置_ZHI_边界检查 || defined ZHI_TARGET_C67
静态_函数 符号 *返回_指向节的_静态符号(C类型 *type, 段 *sec, unsigned long offset, unsigned long size);
#endif
#if defined ZHI_TARGET_X86_64 && !defined ZHI_TARGET_PE
静态_函数 int 分类_x86_64_va_arg(C类型 *ty);
#endif
#ifdef 配置_ZHI_边界检查
静态_函数 void 生成左值边界代码_函数参数加载到寄存器(int 数量_args);
#endif

/* ------------ ELF文件处理.c ------------ */

#define ZHI_输出_格式_ELF    0 /* 默认输出格式：ELF */
#define ZHI_输出_格式_二进制 1 /* 二进制图像输出 */
#define ZHI_输出_格式_COFF   2 /* COFF */

#define ARMAG  "!<arch>\012"    /* 对于COFF和a.out档案 */

typedef struct {
    unsigned int n_strx;         /* 索引到名称的字符串表中 */
    unsigned char n_type;         /* 符号类型 */
    unsigned char n_other;        /* 杂项信息（通常为空） */
    unsigned short n_desc;        /* 说明字段（域） */
    unsigned int n_value;        /* 符号值 */
} 单词表_符号;

静态_函数 void zhi_elf_新建(知心状态机 *s);
静态_函数 void zhielf_删除(知心状态机 *s);
静态_函数 void zhielf_stab_新建(知心状态机 *s);
静态_函数 void 保存_段_数据状态(知心状态机 *状态机1);
静态_函数 void 结束_段_数据状态(知心状态机 *状态机1);
#ifdef 配置_ZHI_边界检查
静态_函数 void zhielf_bounds_new(知心状态机 *s);
#endif
静态_函数 段 *创建_节(知心状态机 *状态机1, const char *name, int sh_type, int sh_flags);
静态_函数 void 节_重新分配内存(段 *sec, unsigned long new_size);
静态_函数 size_t 返回_节_对齐偏移量(段 *sec, 目标地址_类型 size, int align);
静态_函数 void *段_ptr_添加(段 *sec, 目标地址_类型 size);
静态_函数 void 段_保留(段 *sec, unsigned long size);
静态_函数 段 *查找_段(知心状态机 *状态机1, const char *name);
静态_函数 段 *新建_字符表(知心状态机 *状态机1, const char *单词表_名称, int sh_type, int sh_flags, const char *strtab_name, const char *hash_name, int hash_sh_flags);

静态_函数 void 更新_外部_符号2(符号 *sym, int sh_num, 目标地址_类型 value, unsigned long size, int can_add_underscore);
静态_函数 void 更新_外部_符号(符号 *sym, 段 *section, 目标地址_类型 value, unsigned long size);
#if 指针_大小 == 4
静态_函数 void 段部分符号重定位(段 *s, 符号 *sym, unsigned long offset, int type);
#endif
静态_函数 void 添加新的重定位项(段 *s, 符号 *sym, unsigned long offset, int type, 目标地址_类型 addend);

静态_函数 int 处理_elf_字符串(段 *s, const char *sym);
静态_函数 int 处理_elf_符号(段 *s, 目标地址_类型 value, unsigned long size, int info, int other, int shndx, const char *name);
静态_函数 int 设置_elf_符号(段 *s, 目标地址_类型 value, unsigned long size, int info, int other, int shndx, const char *name);
静态_函数 int 查找_elf_符号(段 *s, const char *name);
静态_函数 void 使_elf_重定位(段 *全局单词表副本, 段 *s, unsigned long offset, int type, int symbol);
静态_函数 void 使_elf_重定位目标地址(段 *全局单词表副本, 段 *s, unsigned long offset, int type, int symbol, 目标地址_类型 addend);

静态_函数 void 处理_单词表字符串(知心状态机 *状态机1, const char *str, int type, int other, int desc, unsigned long value);
静态_函数 void 处理_单词表_r(知心状态机 *状态机1, const char *str, int type, int other, int desc, unsigned long value, 段 *sec, int sym_index);
静态_函数 void 处理_单词表整数(知心状态机 *状态机1, int type, int other, int desc, int value);

静态_函数 void 解决_常见_符号(知心状态机 *状态机1);
静态_函数 void 重定位_符号(知心状态机 *状态机1, 段 *全局单词表副本, int do_resolve);
静态_函数 void 重定位_段(知心状态机 *状态机1, 段 *s);

静态_函数 ssize_t 全_读取(int fd, void *buf, size_t count);
静态_函数 void *加载_数据(int fd, unsigned long file_offset, unsigned long size);
静态_函数 int zhi_目标文件_类型(int fd, ElfW(ELF文件头) *h);
静态_函数 int zhi_加载_对象_文件(知心状态机 *状态机1, int fd, unsigned long file_offset);
静态_函数 int zhi_加载_档案(知心状态机 *状态机1, int fd, int alacarte);
静态_函数 void 增加_数组(知心状态机 *状态机1, const char *sec, int c);

#if !defined(ELF_OBJ_ONLY) || defined(ZHI_TARGET_MACHO)
静态_函数 void 创建_获取_入口(知心状态机 *状态机1);
#endif
静态_函数 struct 额外_符号_属性 *get_额外_符号_属性(知心状态机 *状态机1, int index, int alloc);
静态_函数 void 压缩_多_重定位(段 *sec, size_t oldrelocoffset);

静态_函数 int 查找_常量_符号(知心状态机 *, const char *name);
静态_函数 目标地址_类型 获取_符号_地址(知心状态机 *s, const char *name, int err, int forc);
静态_函数 void ELF_符号_列表(知心状态机 *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val));
#if defined ZHI_是_本机 || defined ZHI_TARGET_PE
静态_函数 void *zhi_获取_符号_错误(知心状态机 *s, const char *name);
#endif

静态_函数 int 设置_全局_符号(知心状态机 *状态机1, const char *name, 段 *sec, long offs);

/* 使用变量<elem>从元素<startoff>开始浏览<sec>部分中每个<type>类型的元素 */
#define for_each_elem(sec, startoff, elem, type) \
    for (elem = (type *) sec->data + startoff; \
         elem < (type *) (sec->data + sec->数据_偏移); elem++)

#ifndef ZHI_TARGET_PE
静态_函数 int zhi_加载_dll(知心状态机 *状态机1, int fd, const char *文件名, int level);
静态_函数 int zhi_加载_链接脚本(知心状态机 *状态机1, int fd);
静态_函数 void zhi_添加_运行时(知心状态机 *状态机1);
#endif

/* ------------ xxx-link.c ------------ */

/* 是否生成GOT / PLT条目以及何时生成。 首先是NO_GOTPLT_ENTRY，以便未知的重定位不会创建GOT或PLT条目 */
enum 获取plt_入口 {
    NO_GOTPLT_ENTRY,	/* 永远不会生成（例如GLOB_DAT和JMP_SLOT重定位） */
    BUILD_GOT_ONLY,	/* 只建造GOT（一个OFF遗物） */
    AUTO_GOTPLT_ENTRY,	/* 如果sym是UNDEF则生成 */
    ALWAYS_GOTPLT_ENTRY	/* 始终生成（例如PLTOFF重定位） */
};

#if !defined(ELF_OBJ_ONLY) || defined(ZHI_TARGET_MACHO)
静态_函数 int 代码_重定位 (int reloc_type);
静态_函数 int 获取plt_入口_类型 (int reloc_type);
静态_函数 unsigned 创建_plt_入口(知心状态机 *状态机1, unsigned got_offset, struct 额外_符号_属性 *attr);
静态_函数 void 重定位_plt(知心状态机 *状态机1);
#endif
静态_函数 void 重定位(知心状态机 *状态机1, ElfW_Rel *rel, int type, unsigned char *ptr, 目标地址_类型 addr, 目标地址_类型 val);

/* ------------ xxx-gen.c ------------ */

静态_外部 const int 寄存器_类数[可用_寄存器数];

静态_函数 void 生成符号_地址(int t, int a);
静态_函数 void 生成符号(int t);
静态_函数 void 加载(int r, 堆栈值 *sv);
静态_函数 void 存储(int r, 堆栈值 *v);
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *align, int *regsize);
静态_函数 void 具体地址函数_调用(int 数量_args);
静态_函数 void 生成函数_序言(符号 *func_sym);
静态_函数 void 生成函数_结尾(void);
静态_函数 void 生成_fill_nops(int);
静态_函数 int 生成跳转到标签(int t);
静态_函数 void 生成跳转到_固定地址(int a);
静态_函数 int g跳转_条件(int op, int t);
静态_函数 int g跳转_附件(int n, int t);
静态_函数 void 生成_整数二进制运算(int op);
静态_函数 void 生成_浮点运算(int op);
静态_函数 void 生成_浮点转换为整数(int t);
静态_函数 void 生成_整数转换为浮点(int t);
静态_函数 void 生成_浮点转换为另一种浮点(int t);
静态_函数 void g去向(void);
#ifndef ZHI_TARGET_C67
静态_函数 void o(unsigned int c);
#endif
静态_函数 void 生成_vla_sp_保存(int addr);
静态_函数 void 生成_vla_sp_恢复(int addr);
静态_函数 void 生成_vla_分配(C类型 *type, int align);

static inline uint16_t 读16le(unsigned char *p) {
    return p[0] | (uint16_t)p[1] << 8;
}
static inline void 写16le(unsigned char *p, uint16_t x) {
    p[0] = x & 255;  p[1] = x >> 8 & 255;
}
static inline uint32_t 读32le(unsigned char *p) {
  return 读16le(p) | (uint32_t)读16le(p + 2) << 16;
}
static inline void 写32le(unsigned char *p, uint32_t x) {
    写16le(p, x);  写16le(p + 2, x >> 16);
}
static inline void 增加32le(unsigned char *p, int32_t x) {
    写32le(p, 读32le(p) + x);
}
static inline uint64_t 读64le(unsigned char *p) {
  return 读32le(p) | (uint64_t)读32le(p + 4) << 32;
}
static inline void 写64le(unsigned char *p, uint64_t x) {
    写32le(p, x);  写32le(p + 4, x >> 32);
}
static inline void 增加64le(unsigned char *p, int64_t x) {
    写64le(p, 读64le(p) + x);
}

/* ------------ i386-gen.c ------------ */
#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
静态_函数 void 生成(int c);
静态_函数 void 生成_le16(int c);
静态_函数 void 生成_le32(int c);
静态_函数 void 生成_addr32(int r, 符号 *sym, int c);
静态_函数 void 生成_addrpc32(int r, 符号 *sym, int c);
静态_函数 void 生成_cvt_csti(int t);
#endif

#ifdef 配置_ZHI_边界检查
静态_函数 void 生成_边界的_ptr_添加(void);
静态_函数 void 生成_边界的_ptr_取消引用(void);
#endif

/* ------------ x86_64-gen.c ------------ */
#ifdef ZHI_TARGET_X86_64
静态_函数 void 生成_地址64(int r, 符号 *sym, int64_t c);
静态_函数 void 生成_独立于CPU的长时间操作(int op);
#ifdef ZHI_TARGET_PE
静态_函数 void 生成_vla_结果(int addr);
#endif
静态_函数 void 生成_cvt_sxtw(void);
静态_函数 void 生成_cvt_csti(int t);
#endif

/* ------------ arm-gen.c ------------ */
#ifdef ZHI_TARGET_ARM
#if defined(ZHI_ARM_EABI) && !defined(配置_ZHI_ELF解释程序路径)
公共_函数 const char *默认_elf插入(知心状态机 *s);
#endif
静态_函数 void arm_初始化(知心状态机 *s);
#endif

/* ------------ arm64-gen.c ------------ */
#ifdef ZHI_TARGET_ARM64
静态_函数 void 生成_独立于CPU的长时间操作(int op);
静态_函数 void 生成函数_返回值(C类型 *func_type);
静态_函数 void 生成_va_开始(void);
静态_函数 void 生成_va_参数(C类型 *t);
静态_函数 void 生成_清理_缓存(void);
静态_函数 void 生成_cvt_sxtw(void);
静态_函数 void 生成_cvt_csti(int t);
#endif

/* ------------ riscv64-gen.c ------------ */
#ifdef ZHI_TARGET_RISCV64
静态_函数 void 生成_独立于CPU的长时间操作(int op);
//静态_函数 void 生成函数_返回值(C类型 *func_type);
静态_函数 void 生成_va_开始(void);
静态_函数 void arch_transfer_ret_regs(int);
静态_函数 void 生成_cvt_sxtw(void);
#endif

/* ------------ c67-gen.c ------------ */
#ifdef ZHI_TARGET_C67
#endif

/* ------------ COFF文件处理.c ------------ */

#ifdef ZHI_TARGET_COFF
静态_函数 int zhi_输出_coff(知心状态机 *状态机1, FILE *f);
静态_函数 int zhi_加载_coff(知心状态机 * 状态机1, int fd);
#endif

/* ------------ 汇编处理.c ------------ */
静态_函数 void 汇编_指令字符串(void);
静态_函数 void 汇编_全局_instr(void);
#ifdef 配置_ZHI_汇编
静态_函数 int 查找_约束(汇编操作数 *operands, int 数量_operands, const char *name, const char **pp);
静态_函数 符号* 获取_汇编_符号(int name, 符号 *csym);
静态_函数 void 汇编_表达式(知心状态机 *状态机1, 表达式值 *pe);
静态_函数 int 汇编_整数_表达式(知心状态机 *状态机1);
静态_函数 int zhi_汇编(知心状态机 *状态机1, int do_preprocess);
/* ------------ i386-asm.c ------------ */
静态_函数 void 生成_32位表达式(表达式值 *pe);
#ifdef ZHI_TARGET_X86_64
静态_函数 void 生成_64位表达式(表达式值 *pe);
#endif
静态_函数 void 汇编_指令代码(知心状态机 *状态机1, int opcode);
静态_函数 int 汇编_解析_注册变量(int t);
静态_函数 void 汇编_计算_约束(汇编操作数 *operands, int 数量_operands, int 数量_outputs, const uint8_t *clobber_regs, int *pout_reg);
静态_函数 void 替换_汇编_操作数(动态字符串 *add_str, 堆栈值 *sv, int modifier);
静态_函数 void 汇编_生成_代码(汇编操作数 *operands, int 数量_operands, int 数量_outputs, int is_output, uint8_t *clobber_regs, int out_reg);
静态_函数 void 汇编_破坏者(uint8_t *clobber_regs, const char *str);
#endif

/* ------------ PE文件输出.c -------------- */
#ifdef ZHI_TARGET_PE
静态_函数 int pe_加载_文件(知心状态机 *状态机1, const char *文件名, int fd);
静态_函数 int pe_输出_文件(知心状态机 * 状态机1, const char *文件名);
静态_函数 int pe_导入(知心状态机 *状态机1, int dllindex, const char *name, 目标地址_类型 value);
#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
静态_函数 堆栈值 *pe_获取导入(堆栈值 *sv, 堆栈值 *v2);
#endif
#ifdef ZHI_TARGET_X86_64
静态_函数 void pe_添加_uwwind_数据(unsigned start, unsigned end, unsigned stack);
#endif
公共_函数 int zhi_获取_dll导出(const char *文件名, char **pp);
/* 存储在Elf32_Sym-> st_other中的符号属性 */
# define ST_PE_EXPORT 0x10
# define ST_PE_IMPORT 0x20
# define ST_PE_标准调用 0x40
#endif
#define ST_ASM_SET 0x04

/* ------------ MACH系统O文件处理.c ----------------- */
#ifdef ZHI_TARGET_MACHO
静态_函数 int macho_输出_文件(知心状态机 * 状态机1, const char *文件名);
静态_函数 int macho_加载_dll(知心状态机 *状态机1, int fd, const char *文件名, int lev);
#endif
/* ------------ run开关.c ----------------- */
#ifdef ZHI_是_本机
#ifdef 配置_ZHI_静态
#define RTLD_依赖       0x001
#define RTLD_现在       0x002
#define RTLD_全局     0x100
#define RTLD_默认    NULL
/* 用于分析的伪函数 */
静态_函数 void *dl打开(const char *文件名, int flag);
静态_函数 void dl关闭(void *p);
静态_函数 const char *dl错误(void);
静态_函数 void *dl符号(void *handle, const char *symbol);
#endif
静态_函数 void 释放运行时内存(知心状态机 *状态机1);
#endif

/* ------------ 工具集.c ----------------- */
#if 0 /* included in zhi.c */
静态_函数 int 创建静态库文件(知心状态机 *s, int 参数数量, char **参数数组);
#ifdef ZHI_TARGET_PE
静态_函数 int 创建定义文件(知心状态机 *s, int 参数数量, char **参数数组);
#endif
静态_函数 void 编译器工具交叉(知心状态机 *s, char **参数数组, int option);
静态_函数 void 生成_makedeps(知心状态机 *s, const char *target, const char *文件名);
#endif

/********************************************************/
#undef 静态_外部
#if 是_源文件
#define 静态_外部 static
#else
#define 静态_外部
#endif
/********************************************************/

#define 生成代码_段        ZHI_状态_变量(生成代码_段)
#define 初始化数据_部分        ZHI_状态_变量(初始化数据_部分)
#define 未初始化数据_部分         ZHI_状态_变量(未初始化数据_部分)
#define 通用_部分      ZHI_状态_变量(通用_部分)
#define 当前_生成代码_段    ZHI_状态_变量(当前_生成代码_段)
#define 全局边界_部分      ZHI_状态_变量(全局边界_部分)
#define 本地边界_部分     ZHI_状态_变量(本地边界_部分)
#define 单词表_部分      ZHI_状态_变量(单词表_部分)
#define 符号调试_部分        ZHI_状态_变量(符号调试_部分)
#define 单词表字符串_段     符号调试_部分->link
#define gnu_扩展             ZHI_状态_变量(gnu_扩展)
#define zhi_错误_不中止   ZHI_设置_状态(错误提示)
#define 错误_打印           ZHI_设置_状态(错误)
#define zhi_警告         ZHI_设置_状态(_zhi_警告)

#define 总_idents        ZHI_状态_变量(总_idents)
#define 总_行数         ZHI_状态_变量(总_行数)
#define 总_字节数         ZHI_状态_变量(总_字节数)

公共_函数 void 进入状态机(知心状态机 *状态机1);
公共_函数 void 离开状态机(void);

/********************************************************/
#endif /* 宏_ZHI_H */

#undef ZHI_状态_变量
#undef ZHI_设置_状态

#ifdef 全局_使用
# define ZHI_状态_变量(sym) zhi_状态->sym
# define ZHI_设置_状态(fn) fn
# undef 全局_使用
#else
# define ZHI_状态_变量(sym) 状态机1->sym
# define ZHI_设置_状态(fn) (进入状态机(状态机1),fn)
/* 实际上我们可以通过使用避免zhi_enter_state（状态机1）骇客__VA_ARGS__，除了某些编译器不支持它。 */
#endif
