/*
 *  功能：预处理，词法分析
 *  只在hexinku.c低7行一个地方引用
 */
#define 全局_使用
#include "zhi.h"

/********************************************************/
/* 全局变量 */

静态_外部 int 标识符_标记;
静态_外部 int 解析_标记;

静态_外部 struct 缓冲文件 *file;
静态_外部 int 当前取到的源码字符, 单词编码;
静态_外部 恒定值 单词值;
静态_外部 const int *宏_ptr;
静态_外部 动态字符串 当前单词字符串;

/* 显示基准信息 */
静态_外部 int 单词_识别号;
静态_外部 单词存储结构 **单词表;

/* ------------------------------------------------------------------------- */

static 单词存储结构 *单词_哈希表[哈希表容量];
static char 标识符_缓冲[字符串_最大_长度 + 1];
static 动态字符串 动态字符串_缓冲;
static 动态字符串 宏_等于_缓冲区;
static 单词字符串 单词字符串_缓冲;
static unsigned char 等值_表[256 - CH_文件结尾];
static int 词法分析_调试_标识符, 词法分析_调试_字符值;
static int 词法分析_第一次;
static int 词法分析_表达式;
static int 词法分析_计数器;
static void 标识符_打印(const char *msg, const int *str);

static struct 微内存分配 *单词字符_分配内存;
static struct 微内存分配 *单词字符串_分配内存;

static 单词字符串 *宏_堆栈;

static const char 编译_关键词[] = 
#define 定义(id, str) str "\0"
#include "标识符.h"
#undef 定义
;

/* 双字符符号编号 */
static const unsigned char 双字符符号[] =
{
    '<','=', 双符号_小于等于,
    '>','=', 双符号_大于等于,
    '!','=', 双符号_不等于,
    '&','&', 双符号_逻辑与,
    '|','|', 双符号_逻辑或,
    '+','+', 双符号_自加1,
    '-','-', 双符号_自减1,
    '=','=', 双符号_等于,
    '<','<', 双符号_左位移,
    '>','>', 双符号_右位移,
    '+','=', 双符号_先求和后赋值,
    '-','=', 双符号_先求差后赋值,
    '*','=', 双符号_先求积后赋值,
    '/','=', 双符号_先求商后赋值,
    '%','=', 双符号_先取模后赋值,
    '&','=', 双符号_先求位与后赋值,
    '^','=', 双符号_先求异或后赋值,
    '|','=', 双符号_先求位或后赋值,
    '-','>', 双符号_结构体指针运算符,
    '.','.', 双符号_2个圆点号,
    '#','#', 双符号_2个井号,
    0
};

static void 取_下个符号_不宏扩展(void);
/*跳到下一个标识符*/
静态_函数 void 跳过(int c)
{
    if (单词编码 != c)
        错误_打印("'%c'(程序中的 \"%s\")应为:''", c, 取_单词字符串(单词编码, &单词值));
    带有宏替换的下个标记();
}

静态_函数 void 应为(const char *msg)
{
    错误_打印("%s 应为", msg);
}

/* ------------------------------------------------------------------------- */
/* 自定义分配器，用于微小对象 */

#define 小对象_分配器

#ifndef 小对象_分配器
#define 小分配器_释放(al, p) 内存_释放(p)
#define 小分配器_重新分配(al, p, size) 内存_重分配容量(p, size)
#define 小分配器_新建(a,b,c)
#define 小分配器_删除(a)
#else
#if !defined(内存_调试)
#define 小分配器_释放(al, p) 小分配器_释放_impl(al, p)
#define 小分配器_重新分配(al, p, size) 小分配器_重分配_impl(&al, p, size)
#define 小分配器_调试_参数
#else
#define 小分配器_调试 1
#define 小分配器_释放(al, p) 小分配器_释放_impl(al, p, __FILE__, __LINE__)
#define 小分配器_重新分配(al, p, size) 小分配器_重分配_impl(&al, p, size, __FILE__, __LINE__)
#define 小分配器_调试_参数 , const char *file, int line
#define 小分配器_调试_文件_长度 40
#endif

#define 单词字符_小分配器_大小     (768 * 1024) /* 表_识别中微小TokenSym的分配器 */
#define 单词字符串_小分配器_大小     (768 * 1024) /* 微小TokenString实例的分配器 */
#define 动态字符串_小分配器_大小       (256 * 1024) /* 小型CString实例的分配器。没有使用 */
#define 单词字符_小分配器_限制    256 /* 倾向于使用唯一的限制来区分分配器调试消息 */
#define 单词字符串_小分配器_限制    128 /* 32 * sizeof(int) */
#define 动态字符串_小分配器_限制      1024
typedef struct 微内存分配 {
    unsigned  limit;
    unsigned  size;
    uint8_t *buffer;
    uint8_t *p;
    unsigned  数量_allocs;
    struct 微内存分配 *next, *top;
#ifdef 小分配器_信息
    unsigned  数量_peak;
    unsigned  数量_total;
    unsigned  数量_missed;
    uint8_t *peak_p;
#endif
} 微内存分配;

typedef struct 小分配器_header_t {
    unsigned  size;
#ifdef 小分配器_调试
    int     line_num; /* 拒绝line_num用于双重释放检查 */
    char    file_name[小分配器_调试_文件_长度 + 1];
#endif
} 小分配器_header_t;

/* ------------------------------------------------------------------------- */

static 微内存分配 *小分配器_新建(微内存分配 **pal, unsigned limit, unsigned size)
{
    微内存分配 *al = 内存_初始化(sizeof(微内存分配));
    al->p = al->buffer = 内存_申请(size);
    al->limit = limit;
    al->size = size;
    if (pal) *pal = al;
    return al;
}

static void 小分配器_删除(微内存分配 *al)
{
    微内存分配 *next;

tail_call:
    if (!al)
        return;
#ifdef 小分配器_信息
    fprintf(stderr, "limit=%5d, size=%5g MB, 数量_peak=%6d, 数量_total=%8d, 数量_missed=%6d, usage=%5.1f%%\n",
            al->limit, al->size / 1024.0 / 1024.0, al->数量_peak, al->数量_total, al->数量_missed,
            (al->peak_p - al->buffer) * 100.0 / al->size);
#endif
#ifdef 小分配器_调试
    if (al->数量_allocs > 0) {
        uint8_t *p;
        fprintf(stderr, "小分配器_调试: 内存泄漏 %d 个数据块 (限制= %d)\n",
                al->数量_allocs, al->limit);
        p = al->buffer;
        while (p < al->p) {
            小分配器_header_t *header = (小分配器_header_t *)p;
            if (header->line_num > 0) {
                fprintf(stderr, "%s:%d:  %d 个字节的块泄漏\n",
                        header->file_name, header->line_num, header->size);
            }
            p += header->size + sizeof(小分配器_header_t);
        }
#if 内存_调试-0 == 2
        exit(2);
#endif
    }
#endif
    next = al->next;
    内存_释放(al->buffer);
    内存_释放(al);
    al = next;
    goto tail_call;
}

static void 小分配器_释放_impl(微内存分配 *al, void *p 小分配器_调试_参数)
{
    if (!p)
        return;
tail_call:
    if (al->buffer <= (uint8_t *)p && (uint8_t *)p < al->buffer + al->size) {
#ifdef 小分配器_调试
        小分配器_header_t *header = (((小分配器_header_t *)p) - 1);
        if (header->line_num < 0) {
            fprintf(stderr, "%s:%d: 小分配器_调试: 双重释放块\n",
                    file, line);
            fprintf(stderr, "%s:%d: %d 个字节\n",
                    header->file_name, (int)-header->line_num, (int)header->size);
        } else
            header->line_num = -header->line_num;
#endif
        al->数量_allocs--;
        if (!al->数量_allocs)
            al->p = al->buffer;
    } else if (al->next) {
        al = al->next;
        goto tail_call;
    }
    else
        内存_释放(p);
}

static void *小分配器_重分配_impl(微内存分配 **pal, void *p, unsigned size 小分配器_调试_参数)
{
    小分配器_header_t *header;
    void *ret;
    int is_own;
    unsigned adj_size = (size + 3) & -4;
    微内存分配 *al = *pal;

tail_call:
    is_own = (al->buffer <= (uint8_t *)p && (uint8_t *)p < al->buffer + al->size);
    if ((!p || is_own) && size <= al->limit) {
        if (al->p - al->buffer + adj_size + sizeof(小分配器_header_t) < al->size) {
            header = (小分配器_header_t *)al->p;
            header->size = adj_size;
#ifdef 小分配器_调试
            { int ofs = strlen(file) - 小分配器_调试_文件_长度;
            strncpy(header->file_name, file + (ofs > 0 ? ofs : 0), 小分配器_调试_文件_长度);
            header->file_name[小分配器_调试_文件_长度] = 0;
            header->line_num = line; }
#endif
            ret = al->p + sizeof(小分配器_header_t);
            al->p += adj_size + sizeof(小分配器_header_t);
            if (is_own) {
                header = (((小分配器_header_t *)p) - 1);
                if (p) memcpy(ret, p, header->size);
#ifdef 小分配器_调试
                header->line_num = -header->line_num;
#endif
            } else {
                al->数量_allocs++;
            }
#ifdef 小分配器_信息
            if (al->数量_peak < al->数量_allocs)
                al->数量_peak = al->数量_allocs;
            if (al->peak_p < al->p)
                al->peak_p = al->p;
            al->数量_total++;
#endif
            return ret;
        } else if (is_own) {
            al->数量_allocs--;
            ret = 小分配器_重新分配(*pal, 0, size);
            header = (((小分配器_header_t *)p) - 1);
            if (p) memcpy(ret, p, header->size);
#ifdef 小分配器_调试
            header->line_num = -header->line_num;
#endif
            return ret;
        }
        if (al->next) {
            al = al->next;
        } else {
            微内存分配 *bottom = al, *next = al->top ? al->top : al;

            al = 小分配器_新建(pal, next->limit, next->size * 2);
            al->next = next;
            bottom->top = al;
        }
        goto tail_call;
    }
    if (is_own) {
        al->数量_allocs--;
        ret = 内存_申请(size);
        header = (((小分配器_header_t *)p) - 1);
        if (p) memcpy(ret, p, header->size);
#ifdef 小分配器_调试
        header->line_num = -header->line_num;
#endif
    } else if (al->next) {
        al = al->next;
        goto tail_call;
    } else
        ret = 内存_重分配容量(p, size);
#ifdef 小分配器_信息
    al->数量_missed++;
#endif
    return ret;
}

#endif /* 小对象_分配器 */

/* ------------------------------------------------------------------------- */
static void 动态字符串_重分配内存(动态字符串 *cstr, int 字符串新长度)
{
    int size;

    size = cstr->缓冲区长度;
    if (size < 8)
        size = 8;
    while (size < 字符串新长度)
        size = size * 2;
    cstr->指向字符串的指针 = 内存_重分配容量(cstr->指向字符串的指针, size);
    cstr->缓冲区长度 = size;
}

静态_内联 void 动态字符串_追加单个字符(动态字符串 *cstr, int 当前取到的源码字符)
{
    int size;
    size = cstr->字符串长度 + 1;
    if (size > cstr->缓冲区长度)
        动态字符串_重分配内存(cstr, size);
    ((unsigned char *)cstr->指向字符串的指针)[size - 1] = 当前取到的源码字符;
    cstr->字符串长度 = size;
}

静态_函数 void 动态字符串_cat(动态字符串 *cstr, const char *str, int len)
{
    int size;
    if (len <= 0)
        len = strlen(str) + 1 + len;
    size = cstr->字符串长度 + len;
    if (size > cstr->缓冲区长度)
        动态字符串_重分配内存(cstr, size);
    memmove(((unsigned char *)cstr->指向字符串的指针) + cstr->字符串长度, str, len);
    cstr->字符串长度 = size;
}

/* 增加一个宽字符 */
静态_函数 void 动态字符串_追加一个宽字符(动态字符串 *cstr, int 当前取到的源码字符)
{
    int size;
    size = cstr->字符串长度 + sizeof(nwchar_t);
    if (size > cstr->缓冲区长度)
        动态字符串_重分配内存(cstr, size);
    *(nwchar_t *)(((unsigned char *)cstr->指向字符串的指针) + size - sizeof(nwchar_t)) = 当前取到的源码字符;
    cstr->字符串长度 = size;
}
静态_函数 void 动态字符串_初始化(动态字符串 *cstr)
{
	/*memset(void *s, int c, unsigned long n);函数的功能是：将指针变量 s 所指向的前 n 字节的内存单元用一个“整数” c 替换，注意 c 是 int 型。s 是 void* 型的指针变量，所以它可以为任何类型的数据进行初始化。*/
    memset(cstr, 0, sizeof(动态字符串));
}

静态_函数 void 动态字符串_释放(动态字符串 *cstr)
{
    内存_释放(cstr->指向字符串的指针);
    动态字符串_初始化(cstr);
}

/* 将字符串重置为空 */
静态_函数 void 动态字符串_重置(动态字符串 *cstr)
{
    cstr->字符串长度 = 0;
}

静态_函数 int 动态字符串_打印(动态字符串 *cstr, const char *fmt, ...)
{
    va_list v;
    int len, size;
/*在运行VA_START(va_list ap, last_arg)以后，ap指向第一个可变参数在堆栈的地址。
 * last_arg 是最后一个传递给函数的已知的固定参数，即省略号之前的参数。
 */
    va_start(v, fmt);
    len = vsnprintf(NULL, 0, fmt, v);
    va_end(v);
    size = cstr->字符串长度 + len + 1;
    if (size > cstr->缓冲区长度)
        动态字符串_重分配内存(cstr, size);
    va_start(v, fmt);
    vsnprintf((char*)cstr->指向字符串的指针 + cstr->字符串长度, size, fmt, v);
    va_end(v);
    cstr->字符串长度 += len;
    return len;
}

/* XXX: 统一码 ? */
static void 添加_字符(动态字符串 *cstr, int c)
{
    if (c == '\'' || c == '\"' || c == '\\') {
        /* XXX: 如果使用char或string可能更精确 */
        动态字符串_追加单个字符(cstr, '\\');
    }
    if (c >= 32 && c <= 126) {
        动态字符串_追加单个字符(cstr, c);
    } else {
        动态字符串_追加单个字符(cstr, '\\');
        if (c == '\n') {
            动态字符串_追加单个字符(cstr, 'n');
        } else {
            动态字符串_追加单个字符(cstr, '0' + ((c >> 6) & 7));
            动态字符串_追加单个字符(cstr, '0' + ((c >> 3) & 7));
            动态字符串_追加单个字符(cstr, '0' + (c & 7));
        }
    }
}

/* ------------------------------------------------------------------------- */
/* 分配新标识*/
static 单词存储结构 *标识符_分配新内存(单词存储结构 **pts, const char *str, int len)
{
    单词存储结构 *ts, **ptable;
    int i;

    if (单词_识别号 >= 符号_第一个_匿名) 
        错误_打印("内存已满（符号）");

    /* 必要的时候可以扩展标识符表 */
    i = 单词_识别号 - 符_识别;
    if ((i % 符_分配_INCR) == 0) {
        ptable = 内存_重分配容量(单词表, (i + 符_分配_INCR) * sizeof(单词存储结构 *));
        单词表 = ptable;
    }

    ts = 小分配器_重新分配(单词字符_分配内存, 0, sizeof(单词存储结构) + len);
    单词表[i] = ts;
    ts->单词编码 = 单词_识别号++;
    ts->sym_define = NULL;
    ts->sym_label = NULL;
    ts->sym_struct = NULL;
    ts->sym_identifier = NULL;
    ts->len = len;
    ts->hash_next = NULL;
    memcpy(ts->str, str, len);
    ts->str[len] = '\0';
    *pts = ts;
    return ts;
}

#define 符_哈希_初始化 1
#define 符_哈希_函数(h, c) ((h) + ((h) << 5) + ((h) >> 27) + (c))
/*单词表中查找单词_如不存在就添加它*/
静态_函数 单词存储结构 *单词表_查找(const char *str, int len)
{
    单词存储结构 *单词存储结构, **pts;
    int i;
    unsigned int h;
    
    h = 符_哈希_初始化;
    for(i=0;i<len;i++)
        h = 符_哈希_函数(h, ((unsigned char *)str)[i]);
    h &= (哈希表容量 - 1);

    pts = &单词_哈希表[h];
    for(;;) {
    	单词存储结构 = *pts;
        if (!单词存储结构)
            break;
        if (单词存储结构->len == len && !memcmp(单词存储结构->str, str, len))
            return 单词存储结构;
        pts = &(单词存储结构->hash_next);
    }
    return 标识符_分配新内存(pts, str, len);
}
静态_函数 const char *取_单词字符串(int v, 恒定值 *cv)
{
    char *p;
    int i, len;

    动态字符串_重置(&动态字符串_缓冲);
    p = 动态字符串_缓冲.指向字符串的指针;

    switch(v) {
    case 常量_整数:
    case 常量_无符整数:
    case 常量_长整数:
    case 常量_无符长整数:
    case 常量_长长整数:
    case 常量_无符长长整数:
        /* XXX: 不太精确，但仅对测试有用  */
#ifdef _WIN32
        sprintf(p, "%u", (unsigned)cv->i);
#else
        sprintf(p, "%llu", (unsigned long long)cv->i);
#endif
        break;
    case 常量_长字符型:
        动态字符串_追加单个字符(&动态字符串_缓冲, 'L');
    case 常量_字符型:
        动态字符串_追加单个字符(&动态字符串_缓冲, '\'');
        添加_字符(&动态字符串_缓冲, cv->i);
        动态字符串_追加单个字符(&动态字符串_缓冲, '\'');
        动态字符串_追加单个字符(&动态字符串_缓冲, '\0');
        break;
    case 常量_预处理编号:
    case 常量_预处理字符串:
        return (char*)cv->str.data;
    case 常量_长字符串:
        动态字符串_追加单个字符(&动态字符串_缓冲, 'L');
    case 常量_字符串:
        动态字符串_追加单个字符(&动态字符串_缓冲, '\"');
        if (v == 常量_字符串) {
            len = cv->str.size - 1;
            for(i=0;i<len;i++)
                添加_字符(&动态字符串_缓冲, ((unsigned char *)cv->str.data)[i]);
        } else {
            len = (cv->str.size / sizeof(nwchar_t)) - 1;
            for(i=0;i<len;i++)
                添加_字符(&动态字符串_缓冲, ((nwchar_t *)cv->str.data)[i]);
        }
        动态字符串_追加单个字符(&动态字符串_缓冲, '\"');
        动态字符串_追加单个字符(&动态字符串_缓冲, '\0');
        break;

    case 常量_浮点型:
        动态字符串_cat(&动态字符串_缓冲, "<float>", 0);
        break;
    case 常量_双精度:
	动态字符串_cat(&动态字符串_缓冲, "<double>", 0);
	break;
    case 常量_长双精度:
	动态字符串_cat(&动态字符串_缓冲, "<long double>", 0);
	break;
    case 常量_行号:
	动态字符串_cat(&动态字符串_缓冲, "<linenumber>", 0);
	break;

    /* 上面的标识符具有价值，下面的标识符没有价值 */
    case 符_LT:
        v = '<';
        goto addv;
    case 符_GT:
        v = '>';
        goto addv;
    case 符_三个圆点:
        return strcpy(p, "...");
    case 符_A_SHL:
        return strcpy(p, "<<=");
    case 符_A_SAR:
        return strcpy(p, ">>=");
    case 符_文件结尾:
        return strcpy(p, "<eof>");
    default:
        if (v < 符_识别) {
            /* search in two bytes table */
            const unsigned char *q = 双字符符号;
            while (*q) {
                if (q[2] == v) {
                    *p++ = q[0];
                    *p++ = q[1];
                    *p = '\0';
                    return 动态字符串_缓冲.指向字符串的指针;
                }
                q += 3;
            }
        if (v >= 127) {
            sprintf(动态字符串_缓冲.指向字符串的指针, "<%02x>", v);
            return 动态字符串_缓冲.指向字符串的指针;
        }
        addv:
            *p++ = v;
            *p = '\0';
        } else if (v < 单词_识别号) {
            return 单词表[v - 符_识别]->str;
        } else if (v >= 符号_第一个_匿名) {
            /* 匿名符号的特殊名称 */
            sprintf(p, "L.%u", v - 符号_第一个_匿名);
        } else {
            /* 不应该发生 */
            return NULL;
        }
        break;
    }
    return 动态字符串_缓冲.指向字符串的指针;
}

/* 返回当前字符，必要时处理块的结尾 */
static int 处理_块的结尾(void)
{
    缓冲文件 *bf = file;
    int len;

    /* 仅在缓冲区真正结束时尝试读取 */
    if (bf->buf_ptr >= bf->buf_end) {
        if (bf->fd >= 0) {
#if defined(PARSE_DEBUG)
            len = 1;
#else
            len = 输入输出_缓冲_大小;
#endif
            len = read(bf->fd, bf->buffer, len);
            if (len < 0)
                len = 0;
        } else {
            len = 0;
        }
        总_字节数 += len;
        bf->buf_ptr = bf->buffer;
        bf->buf_end = bf->buffer + len;
        *bf->buf_end = CH_缓冲区结尾;
    }
    if (bf->buf_ptr < bf->buf_end) {
        return bf->buf_ptr[0];
    } else {
        bf->buf_ptr = bf->buf_end;
        return CH_文件结尾;
    }
}

static inline void 读下个符号处理缓冲区结尾(void)
{
    当前取到的源码字符 = *(++(file->buf_ptr));
    /* 缓冲区/文件处理结束 */
    if (当前取到的源码字符 == CH_缓冲区结尾)
        当前取到的源码字符 = 处理_块的结尾();
}

/* 处理 '\[\r]\n' */
static int 处理_转义_无错误(void)
{
    while (当前取到的源码字符 == '\\') {
        读下个符号处理缓冲区结尾();
        if (当前取到的源码字符 == '\n') {
            file->line_num++;
            读下个符号处理缓冲区结尾();
        } else if (当前取到的源码字符 == '\r') {
            读下个符号处理缓冲区结尾();
            if (当前取到的源码字符 != '\n')
                goto fail;
            file->line_num++;
            读下个符号处理缓冲区结尾();
        } else {
        fail:
            return 1;
        }
    }
    return 0;
}
/**/
static void 处理_转义(void)
{
    if (处理_转义_无错误())
        错误_打印("程序中转义 '\\' ");
}

/* 跳过转义符，处理\\ n。 如果输出错误
    转义后不正确的字符 */
static int 处理_转义1(uint8_t *p)
{
    int c;

    file->buf_ptr = p;
    if (p >= file->buf_end) {
        c = 处理_块的结尾();
        if (c != '\\')
            return c;
        p = file->buf_ptr;
    }
    当前取到的源码字符 = *p;
    if (处理_转义_无错误()) {
        if (!(解析_标记 & 解析_标记_接受_转义))
            错误_打印("程序中转义 '\\' ");
        *--file->buf_ptr = '\\';
    }
    p = file->buf_ptr;
    c = *p;
    return c;
}

/* 仅处理EOB情况，但不能处理转义 */
#define 仅处理模块结尾_不处理转义(c, p)\
{\
    p++;\
    c = *p;\
    if (c == '\\') {\
        file->buf_ptr = p;\
        c = 处理_块的结尾();\
        p = file->buf_ptr;\
    }\
}

/* 处理复杂的转义情况*/
#define 复杂转义(c, p)\
{\
    p++;\
    c = *p;\
    if (c == '\\') {\
        c = 处理_转义1(p);\
        p = file->buf_ptr;\
    }\
}

/* 输入以'\ [\ r] \ n'处理。 请注意，此函数无法处理'\'之后的其他字符，因此您不能在字符串或注释中调用它 */
static void 仅处理第一个斜杠(void)
{
    读下个符号处理缓冲区结尾();
    if (当前取到的源码字符 == '\\') 
        处理_转义();
}

/* 单行C ++注释 */
static uint8_t *解析_单行_注释(uint8_t *p)
{
    int c;

    p++;
    for(;;) {
        c = *p;
    redo:
        if (c == '\n' || c == CH_文件结尾) {
            break;
        } else if (c == '\\') {
            file->buf_ptr = p;
            c = 处理_块的结尾();
            p = file->buf_ptr;
            if (c == '\\') {
                仅处理模块结尾_不处理转义(c, p);
                if (c == '\n') {
                    file->line_num++;
                    仅处理模块结尾_不处理转义(c, p);
                } else if (c == '\r') {
                    仅处理模块结尾_不处理转义(c, p);
                    if (c == '\n') {
                        file->line_num++;
                        仅处理模块结尾_不处理转义(c, p);
                    }
                }
            } else {
                goto redo;
            }
        } else {
            p++;
        }
    }
    return p;
}

/* C 注释（解释，注释） */
static uint8_t *解析_注释(uint8_t *p)
{
    int c;

    p++;
    for(;;) {
        /* 快速跳过循环 */
        for(;;) {
            c = *p;
            if (c == '\n' || c == '*' || c == '\\')
                break;
            p++;
            c = *p;
            if (c == '\n' || c == '*' || c == '\\')
                break;
            p++;
        }
        /* 现在我们可以处理所有情况 */
        if (c == '\n') {
            file->line_num++;
            p++;
        } else if (c == '*') {
            p++;
            for(;;) {
                c = *p;
                if (c == '*') {
                    p++;
                } else if (c == '/') {
                    goto end_of_comment;
                } else if (c == '\\') {
                    file->buf_ptr = p;
                    c = 处理_块的结尾();
                    p = file->buf_ptr;
                    if (c == CH_文件结尾)
                        错误_打印("注释中的文件意外结束");
                    if (c == '\\') {
                        /* 跳过 '\[\r]\n', otherwise just 跳过 the stray */
                        while (c == '\\') {
                            仅处理模块结尾_不处理转义(c, p);
                            if (c == '\n') {
                                file->line_num++;
                                仅处理模块结尾_不处理转义(c, p);
                            } else if (c == '\r') {
                                仅处理模块结尾_不处理转义(c, p);
                                if (c == '\n') {
                                    file->line_num++;
                                    仅处理模块结尾_不处理转义(c, p);
                                }
                            } else {
                                goto after_star;
                            }
                        }
                    }
                } else {
                    break;
                }
            }
        after_star: ;
        } else {
            /* stray, eob or eof */
            file->buf_ptr = p;
            c = 处理_块的结尾();
            p = file->buf_ptr;
            if (c == CH_文件结尾) {
                错误_打印("注释中的文件意外结束");
            } else if (c == '\\') {
                p++;
            }
        }
    }
 end_of_comment:
    p++;
    return p;
}

静态_函数 int 设置_等值数(int c, int val)
{
    int prev = 等值_表[c - CH_文件结尾];
    等值_表[c - CH_文件结尾] = val;
    return prev;
}

#define cinp 仅处理第一个斜杠

static inline void 跳过_空格(void)
{
    while (等值_表[当前取到的源码字符 - CH_文件结尾] & IS_SPC)
        cinp();
}

static inline int 检查_空格(int t, int *spc)
{
    if (t < 256 && (等值_表[t - CH_文件结尾] & IS_SPC)) {
        if (*spc) 
            return 1;
        *spc = 1;
    } else 
        *spc = 0;
    return 0;
}

static uint8_t *解析字符串_不解析转义(uint8_t *p,int sep, 动态字符串 *str)
{
    int c;
    p++;
    for(;;) {
        c = *p;
        if (c == sep) {
            break;
        } else if (c == '\\') {
            file->buf_ptr = p;
            c = 处理_块的结尾();
            p = file->buf_ptr;
            if (c == CH_文件结尾) {
            unterminated_string:
                /* XXX: 指示字符串开头的行号 */
                错误_打印("缺少终止符 %c ", sep);
            } else if (c == '\\') {
                /* escape : just 跳过 \[\r]\n */
                仅处理模块结尾_不处理转义(c, p);
                if (c == '\n') {
                    file->line_num++;
                    p++;
                } else if (c == '\r') {
                    仅处理模块结尾_不处理转义(c, p);
                    if (c != '\n')
                        应为("'\n' after '\r'");
                    file->line_num++;
                    p++;
                } else if (c == CH_文件结尾) {
                    goto unterminated_string;
                } else {
                    if (str) {
                        动态字符串_追加单个字符(str, '\\');
                        动态字符串_追加单个字符(str, c);
                    }
                    p++;
                }
            }
        } else if (c == '\n') {
            file->line_num++;
            goto 添加_字符;
        } else if (c == '\r') {
            仅处理模块结尾_不处理转义(c, p);
            if (c != '\n') {
                if (str)
                    动态字符串_追加单个字符(str, '\r');
            } else {
                file->line_num++;
                goto 添加_字符;
            }
        } else {
        添加_字符:
            if (str)
                动态字符串_追加单个字符(str, c);
            p++;
        }
    }
    p++;
    return p;
}

/* 跳过预处理。_预处理（预处理）。跳过＃else，＃elif或#endif。 也跳过
    ＃if /＃endif */
static void 跳过_预处理(void)
{
    int a, start_of_line, c, in_warn_or_error;
    uint8_t *p;

    p = file->buf_ptr;
    a = 0;
redo_start:
    start_of_line = 1;
    in_warn_or_error = 0;
    for(;;) {
    redo_no_start:
        c = *p;
        switch(c) {
        case ' ':
        case '\t':
        case '\f':
        case '\v':
        case '\r':
            p++;
            goto redo_no_start;
        case '\n':
            file->line_num++;
            p++;
            goto redo_start;
        case '\\':
            file->buf_ptr = p;
            c = 处理_块的结尾();
            if (c == CH_文件结尾) {
                应为("#endif");
            } else if (c == '\\') {
                当前取到的源码字符 = file->buf_ptr[0];
                处理_转义_无错误();
            }
            p = file->buf_ptr;
            goto redo_no_start;
        /* 跳过字符串 */
        case '\"':
        case '\'':
            if (in_warn_or_error)
                goto _default;
            p = 解析字符串_不解析转义(p, c, NULL);
            break;
        /* 跳过注释 */
        case '/':
            if (in_warn_or_error)
                goto _default;
            file->buf_ptr = p;
            当前取到的源码字符 = *p;
            仅处理第一个斜杠();
            p = file->buf_ptr;
            if (当前取到的源码字符 == '*') {
                p = 解析_注释(p);
            } else if (当前取到的源码字符 == '/') {
                p = 解析_单行_注释(p);
            }
            break;
        case '#':
            p++;
            if (start_of_line) {
                file->buf_ptr = p;
                取_下个符号_不宏扩展();
                p = file->buf_ptr;
                if (a == 0 && (单词编码 == 关键词_ELSE || 单词编码 == 关键词_否则 || 单词编码 == 关键词_ELIF || 单词编码 == 关键词_否则如果 || 单词编码 == 关键词_ENDIF || 单词编码 == 关键词_结束如果))
                    goto the_end;
                if (单词编码 == 关键词_IF || 单词编码 == 关键词_如果 || 单词编码 == 关键词_IFDEF || 单词编码 == 关键词_如果已定义 || 单词编码 == 关键词_IFNDEF || 单词编码 == 关键词_如果未定义)
                    a++;
                else if (单词编码 == 关键词_ENDIF || 单词编码 == 关键词_结束如果)
                    a--;
                else if( 单词编码 == 关键词_ERROR || 单词编码 == 关键词_错误  || 单词编码 == 关键词_WARNING || 单词编码 == 关键词_警告)
                    in_warn_or_error = 1;
                else if (单词编码 == 符_换行)
                    goto redo_start;
                else if (解析_标记 & 解析_标记_汇编_文件)
                    p = 解析_单行_注释(p - 1);
            } else if (解析_标记 & 解析_标记_汇编_文件)
                p = 解析_单行_注释(p - 1);
            break;
_default:
        default:
            p++;
            break;
        }
        start_of_line = 0;
    }
 the_end: ;
    file->buf_ptr = p;
}

#if 0
/* 返回存储标识符所需的其他“ int”数 。次函数没有被使用*/
static inline int 标识符_大小(const int *p)
{
    switch(*p) {
        /* 4 字节 */
    case 常量_整数:
    case 常量_无符整数:
    case 常量_字符型:
    case 常量_长字符型:
    case 常量_浮点型:
    case 常量_行号:
        return 1 + 1;
    case 常量_字符串:
    case 常量_长字符串:
    case 常量_预处理编号:
    case 常量_预处理字符串:
        return 1 + ((sizeof(动态字符串) + ((动态字符串 *)(p+1))->size + 3) >> 2);
    case 常量_长整数:
    case 常量_无符长整数:
	return 1 + LONG_SIZE / 4;
    case 常量_双精度:
    case 常量_长长整数:
    case 常量_无符长长整数:
        return 1 + 2;
    case 常量_长双精度:
        return 1 + 长双精度_大小 / 4;
    default:
        return 1 + 0;
    }
}
#endif
静态_内联 void 单词字符串_处理(单词字符串 *s)
{
    s->str = NULL;
    s->len = s->lastlen = 0;
    s->allocated_len = 0;
    s->最后_行_号 = -1;
}

静态_函数 单词字符串 *单词字符串_分配(void)
{
    单词字符串 *str = 小分配器_重新分配(单词字符串_分配内存, 0, sizeof *str);
    单词字符串_处理(str);
    return str;
}

静态_函数 int *单词字符串_复制到新内存地址(单词字符串 *s)
{
    int *str;

    str = 小分配器_重新分配(单词字符串_分配内存, 0, s->len * sizeof(int));
    memcpy(str, s->str, s->len * sizeof(int));
    return str;
}

静态_函数 void 单词字符串_释放_字符串(int *str)
{
    小分配器_释放(单词字符串_分配内存, str);
}

静态_函数 void 单词字符串_释放(单词字符串 *str)
{
    单词字符串_释放_字符串(str->str);
    小分配器_释放(单词字符串_分配内存, str);
}

静态_函数 int *单词字符串_重分配(单词字符串 *s, int new_size)
{
    int *str, size;

    size = s->allocated_len;
    if (size < 16)
        size = 16;
    while (size < new_size)
        size = size * 2;
    if (size > s->allocated_len) {
        str = 小分配器_重新分配(单词字符串_分配内存, s->str, size * sizeof(int));
        s->allocated_len = size;
        s->str = str;
    }
    return s->str;
}

静态_函数 void 单词字符串_增加大小(单词字符串 *s, int t)
{
    int len, *str;

    len = s->len;
    str = s->str;
    if (len >= s->allocated_len)
        str = 单词字符串_重分配(s, len + 1);
    str[len++] = t;
    s->len = len;
}

静态_函数 void 开始_宏(单词字符串 *str, int alloc)
{
    str->alloc = alloc;
    str->prev = 宏_堆栈;
    str->prev_ptr = 宏_ptr;
    str->save_line_num = file->line_num;
    宏_ptr = str->str;
    宏_堆栈 = str;
}

静态_函数 void 结束_宏(void)
{
    单词字符串 *str = 宏_堆栈;
    宏_堆栈 = str->prev;
    宏_ptr = str->prev_ptr;
    file->line_num = str->save_line_num;
    if (str->alloc != 0) {
        if (str->alloc == 2)
            str->str = NULL; /* 不释放 */
        单词字符串_释放(str);
    }
}

static void 单词字符串_增加大小2(单词字符串 *s, int t, 恒定值 *cv)
{
    int len, *str;

    len = s->lastlen = s->len;
    str = s->str;

    /* allocate space for worst case */
    if (len + 符_最大_大小 >= s->allocated_len)
        str = 单词字符串_重分配(s, len + 符_最大_大小 + 1);
    str[len++] = t;
    switch(t) {
    case 常量_整数:
    case 常量_无符整数:
    case 常量_字符型:
    case 常量_长字符型:
    case 常量_浮点型:
    case 常量_行号:
#if LONG_SIZE == 4
    case 常量_长整数:
    case 常量_无符长整数:
#endif
        str[len++] = cv->tab[0];
        break;
    case 常量_预处理编号:
    case 常量_预处理字符串:
    case 常量_字符串:
    case 常量_长字符串:
        {
            /* 将字符串插入int数组. */
            size_t 数量_words =
                1 + (cv->str.size + sizeof(int) - 1) / sizeof(int);
            if (len + 数量_words >= s->allocated_len)
                str = 单词字符串_重分配(s, len + 数量_words + 1);
            str[len] = cv->str.size;
            memcpy(&str[len + 1], cv->str.data, cv->str.size);
            len += 数量_words;
        }
        break;
    case 常量_双精度:
    case 常量_长长整数:
    case 常量_无符长长整数:
#if LONG_SIZE == 8
    case 常量_长整数:
    case 常量_无符长整数:
#endif
#if 长双精度_大小 == 8
    case 常量_长双精度:
#endif
        str[len++] = cv->tab[0];
        str[len++] = cv->tab[1];
        break;
#if 长双精度_大小 == 12
    case 常量_长双精度:
        str[len++] = cv->tab[0];
        str[len++] = cv->tab[1];
        str[len++] = cv->tab[2];
#elif 长双精度_大小 == 16
    case 常量_长双精度:
        str[len++] = cv->tab[0];
        str[len++] = cv->tab[1];
        str[len++] = cv->tab[2];
        str[len++] = cv->tab[3];
#elif 长双精度_大小 != 8
#error add long double size support
#endif
        break;
    default:
        break;
    }
    s->len = len;
}

/* 在单词字符串“ *s”中添加当前的解析字符 */
静态_函数 void 单词字符串中添加当前解析的字符(单词字符串 *s)
{
    恒定值 cval;

    /* 保存行号信息 */
    if (file->line_num != s->最后_行_号) {
        s->最后_行_号 = file->line_num;
        cval.i = s->最后_行_号;
        单词字符串_增加大小2(s, 常量_行号, &cval);
    }
    单词字符串_增加大小2(s, 单词编码, &单词值);
}

/* 从整数数组和增量指针获取标识符. */
static inline void 获取_标识符(int *t, const int **pp, 恒定值 *cv)
{
    const int *p = *pp;
    int n, *tab;

    tab = cv->tab;
    switch(*t = *p++) {
#if LONG_SIZE == 4
    case 常量_长整数:
#endif
    case 常量_整数:
    case 常量_字符型:
    case 常量_长字符型:
    case 常量_行号:
        cv->i = *p++;
        break;
#if LONG_SIZE == 4
    case 常量_无符长整数:
#endif
    case 常量_无符整数:
        cv->i = (unsigned)*p++;
        break;
    case 常量_浮点型:
	tab[0] = *p++;
	break;
    case 常量_字符串:
    case 常量_长字符串:
    case 常量_预处理编号:
    case 常量_预处理字符串:
        cv->str.size = *p++;
        cv->str.data = p;
        p += (cv->str.size + sizeof(int) - 1) / sizeof(int);
        break;
    case 常量_双精度:
    case 常量_长长整数:
    case 常量_无符长长整数:
#if LONG_SIZE == 8
    case 常量_长整数:
    case 常量_无符长整数:
#endif
        n = 2;
        goto copy;
    case 常量_长双精度:
#if 长双精度_大小 == 16
        n = 4;
#elif 长双精度_大小 == 12
        n = 3;
#elif 长双精度_大小 == 8
        n = 2;
#else
# error add long double size support
#endif
    copy:
        do
            *tab++ = *p++;
        while (--n);
        break;
    default:
        break;
    }
    *pp = p;
}

#if 0
# define 符_GET(t,p,c) 获取_标识符(t,p,c)
#else
# define 符_GET(t,p,c) do { \
    int _t = **(p); \
    if (符_有_值(_t)) \
        获取_标识符(t, p, c); \
    else \
        *(t) = _t, ++*(p); \
    } while (0)
#endif

static int 宏_等于(const int *a, const int *b)
{
    恒定值 cv;
    int t;

    if (!a || !b)
        return 1;

    while (*a && *b) {
        /* 第一次预分配macro_equal_buf，下次仅重置位置以开始 */
        动态字符串_重置(&宏_等于_缓冲区);
        符_GET(&t, &a, &cv);
        动态字符串_cat(&宏_等于_缓冲区, 取_单词字符串(t, &cv), 0);
        符_GET(&t, &b, &cv);
        if (strcmp(宏_等于_缓冲区.指向字符串的指针, 取_单词字符串(t, &cv)))
            return 0;
    }
    return !(*a || *b);
}

/* 定义处理 */
静态_内联 void 宏定义_处理(int v, int macro_type, int *str, 符号 *first_arg)
{
    符号 *s, *o;

    o = 宏定义_查找(v);
    s = 符号_推送2(&宏定义符号_堆栈, v, macro_type, 0);
    s->d = str;
    s->next = first_arg;
    单词表[v - 符_识别]->sym_define = s;

    if (o && !宏_等于(o->d, s->d))
	zhi_警告("%s 重复定义", 取_单词字符串(v, NULL));
}

/* 没有被定义的定义符号，它的名称仅能设置为零 */
静态_函数 void 未宏定义_符号为NULL(符号 *s)
{
    int v = s->v;
    if (v >= 符_识别 && v < 单词_识别号)
        单词表[v - 符_识别]->sym_define = NULL;
}

静态_内联 符号 *宏定义_查找(int v)
{
    v -= 符_识别;
    if ((unsigned)v >= (unsigned)(单词_识别号 - 符_识别))
        return NULL;
    return 单词表[v]->sym_define;
}

/* 释放定义堆栈，直到顶部达到“ b” */
静态_函数 void 释放_宏定义堆栈(符号 *b)
{
    while (宏定义符号_堆栈 != b) {
        符号 *top = 宏定义符号_堆栈;
        宏定义符号_堆栈 = top->prev;
        单词字符串_释放_字符串(top->d);
        未宏定义_符号为NULL(top);
        符号_释放(top);
    }
}

/* 标签查找 */
静态_函数 符号 *标签_查找(int v)
{
    v -= 符_识别;
    if ((unsigned)v >= (unsigned)(单词_识别号 - 符_识别))
        return NULL;
    return 单词表[v]->sym_label;
}

静态_函数 符号 *标签_推送(符号 **ptop, int v, int flags)
{
    符号 *s, **ps;
    s = 符号_推送2(ptop, v, 0, 0);
    s->r = flags;
    ps = &单词表[v - 符_识别]->sym_label;
    if (ptop == &全局符号_标签_堆栈) {
        /* 修改最上面的本地标识符，以便在弹出时sym_identifier指向“ s” */
        while (*ps != NULL)
            ps = &(*ps)->prev_tok;
    }
    s->prev_tok = *ps;
    *ps = s;
    return s;
}

/* 弹出标签，直到到达元素的最后一个。 查看是否有未定义的标签。 如果使用'&& label'，请定义符号. */
静态_函数 void 标签_弹出(符号 **ptop, 符号 *slast, int keep)
{
    符号 *s, *符1;
    for(s = *ptop; s != slast; s = 符1) {
    	符1 = s->prev;
        if (s->r == 标签_被声明) {
            zhi_警告("标签 '%s' 已声明但未使用", 取_单词字符串(s->v, NULL));
        } else if (s->r == 标签_正向定义) {
                错误_打印("标签 '%s' 被使用但未定义",
                      取_单词字符串(s->v, NULL));
        } else {
            if (s->c) {
                /* 定义相应的符号。 尺寸为1. */
                更新_外部_符号(s, 当前_生成代码_段, s->jnext, 1);
            }
        }
        /* remove label */
        if (s->r != 标签_不在范围)
            单词表[s->v - 符_识别]->sym_label = s->prev_tok;
        if (!keep)
            符号_释放(s);
        else
            s->r = 标签_不在范围;
    }
    if (!keep)
        *ptop = slast;
}

/* 为zhi -dt -run伪造第n个“ #ifdefined test _...” */
static void 可能_运行_测试(知心状态机 *s)
{
    const char *p;
    if (s->包含_堆_ptr != s->包含_堆)
        return;
    p = 取_单词字符串(单词编码, NULL);
    if (0 != memcmp(p, "test_", 5))
        return;
    if (0 != --s->运行_测试)
        return;
    fprintf(s->预处理输出文件, "\n[%s]\n" + !(s->DX标号 & 32), p), fflush(s->预处理输出文件);
    宏定义_处理(单词编码, 宏_对象, NULL, NULL);
}

/* 评估＃if /＃elif的表达式 */
static int 预处理_表达式(void)
{
    int c, t;
    单词字符串 *str;
    
    str = 单词字符串_分配();
    词法分析_表达式 = 1;
    while (单词编码 != 符_换行 && 单词编码 != 符_文件结尾) {
        带有宏替换的下个标记(); /* 做宏替换 */
      redo:
        if (单词编码 == 关键词_DEFINED || 单词编码 == 关键词_已定义) {
            取_下个符号_不宏扩展();
            t = 单词编码;
            if (t == '(') 
                取_下个符号_不宏扩展();
            if (单词编码 < 符_识别)
                应为("identifier");
            if (zhi_状态->运行_测试)
                可能_运行_测试(zhi_状态);
            c = 宏定义_查找(单词编码) != 0;
            if (t == '(') {
                取_下个符号_不宏扩展();
                if (单词编码 != ')')
                    应为("')'");
            }
            单词编码 = 常量_整数;
            单词值.i = c;
        } else if (1 && 单词编码 == 符___HAS_INCLUDE) {
            带有宏替换的下个标记();  /* XXX 检查是否正确使用扩展 */
            跳过('(');
            while (单词编码 != ')' && 单词编码 != 符_文件结尾)
              带有宏替换的下个标记();
            if (单词编码 != ')')
              应为("')'");
            单词编码 = 常量_整数;
            单词值.i = 0;
        } else if (单词编码 >= 符_识别) {
            /* 如果未定义宏，则替换为零，检查类似func的宏 */
            t = 单词编码;
            单词编码 = 常量_整数;
            单词值.i = 0;
            单词字符串中添加当前解析的字符(str);
            带有宏替换的下个标记();
            if (单词编码 == '(')
                错误_打印("未定义类似函数的宏 '%s' ",
                          取_单词字符串(t, NULL));
            goto redo;
        }
        单词字符串中添加当前解析的字符(str);
    }
    词法分析_表达式 = 0;
    单词字符串_增加大小(str, -1); /* 模拟文件结尾 */
    单词字符串_增加大小(str, 0);
    /* 现在评估C常数表达式 */
    开始_宏(str, 1);
    带有宏替换的下个标记();
    c = 表达式_常量();
    结束_宏();
    return c != 0;
}


/* #define之后解析 */
静态_函数 void 解析_宏定义(void)
{
    符号 *s, *first, **ps;
    int v, t, varg, is_vaargs, spc;
    int saved_解析_标记 = 解析_标记;

    v = 单词编码;
    if (v < 符_识别 || v == 关键词_DEFINED || v == 关键词_已定义)
        错误_打印("无效的宏名称 '%s'", 取_单词字符串(单词编码, &单词值));
    /* XXX: 应该检查是否有相同的宏（ANSI） */
    first = NULL;
    t = 宏_对象;
    /* 我们必须解析整个定义，好像不是在asm模式下一样，尤其是必须忽略带有'＃'的行注释。 同样对于函数宏，参数列表也必须不带“。”进行分析。 作为ID字符.  */
    解析_标记 = ((解析_标记 & ~解析_标记_汇编_文件) | 解析_标记_空间);
    /* '(' 必须紧随MACRO_FUNC的宏定义之后 */
    取_下个符号_不宏扩展();
    解析_标记 &= ~解析_标记_空间;
    if (单词编码 == '(') {
        int dotid = 设置_等值数('.', 0);
        取_下个符号_不宏扩展();
        ps = &first;
        if (单词编码 != ')') for (;;) {
            varg = 单词编码;
            取_下个符号_不宏扩展();
            is_vaargs = 0;
            if (varg == 符_三个圆点) {
                varg = 符___VA_ARGS__;
                is_vaargs = 1;
            } else if (单词编码 == 符_三个圆点 && gnu_扩展) {
                is_vaargs = 1;
                取_下个符号_不宏扩展();
            }
            if (varg < 符_识别)
        bad_list:
                错误_打印("错误的宏参数列表");
            s = 符号_推送2(&宏定义符号_堆栈, varg | 符号_字段, is_vaargs, 0);
            *ps = s;
            ps = &s->next;
            if (单词编码 == ')')
                break;
            if (单词编码 != ',' || is_vaargs)
                goto bad_list;
            取_下个符号_不宏扩展();
        }
        解析_标记 |= 解析_标记_空间;
        取_下个符号_不宏扩展();
        t = 宏_函数;
        设置_等值数('.', dotid);
    }

    单词字符串_缓冲.len = 0;
    spc = 2;
    解析_标记 |= 解析_标记_接受_转义 | 解析_标记_空间 | 解析_标记_换行符;
    /* 应该对宏定义的主体进行解析，以便像文件模式确定的那样解析标识符（即在asm模式下以'。'为ID字符）。 但是应保留“＃”而不是将其视为行注释的引号，因此仍不要在解析_标记中设置ASM_FILE。 */
    while (单词编码 != 符_换行 && 单词编码 != 符_文件结尾) {
        /* 删除##周围和'＃'之后的空格 */
        if (双符号_2个井号 == 单词编码) {
            if (2 == spc)
                goto bad_twosharp;
            if (1 == spc)
                --单词字符串_缓冲.len;
            spc = 3;
	    单词编码 = 符_PPJOIN;
        } else if ('#' == 单词编码) {
            spc = 4;
        } else if (检查_空格(单词编码, &spc)) {
            goto 跳过;
        }
        单词字符串_增加大小2(&单词字符串_缓冲, 单词编码, &单词值);
    跳过:
        取_下个符号_不宏扩展();
    }

    解析_标记 = saved_解析_标记;
    if (spc == 1)
        --单词字符串_缓冲.len; /* 删除尾随空间 */
    单词字符串_增加大小(&单词字符串_缓冲, 0);
    if (3 == spc)
bad_twosharp:
        错误_打印("'##' 不能出现在宏的任何一端");
    宏定义_处理(v, t, 单词字符串_复制到新内存地址(&单词字符串_缓冲), first);
}

static 导入文件缓存 *查找_缓存的_导入文件(知心状态机 *状态机1, const char *文件名, int add)
{
    const unsigned char *s;
    unsigned int h;
    导入文件缓存 *e;
    int i;

    h = 符_哈希_初始化;
    s = (unsigned char *) 文件名;
    while (*s) {
#ifdef _WIN32
        h = 符_哈希_函数(h, toup(*s));
#else
        h = 符_哈希_函数(h, *s);
#endif
        s++;
    }
    h &= (缓存的_导入文件_哈希_大小 - 1);

    i = 状态机1->缓存_包含数_哈希[h];
    for(;;) {
        if (i == 0)
            break;
        e = 状态机1->缓存_包含数[i - 1];
        if (0 == PATHCMP(e->文件名, 文件名))
            return e;
        i = e->hash_next;
    }
    if (!add)
        return NULL;

    e = 内存_申请(sizeof(导入文件缓存) + strlen(文件名));
    strcpy(e->文件名, 文件名);
    e->ifndef_macro = e->once = 0;
    动态数组_追加元素(&状态机1->缓存_包含数, &状态机1->数量_缓存_包含数, e);
    /* 在哈希表中添加 */
    e->hash_next = 状态机1->缓存_包含数_哈希[h];
    状态机1->缓存_包含数_哈希[h] = 状态机1->数量_缓存_包含数;
#ifdef 导入文件_调试
    printf("添加缓存的 '%s'\n", 文件名);
#endif
    return e;
}

static void 语用_解析(知心状态机 *态1)
{
    取_下个符号_不宏扩展();
    if (单词编码 == 符_push_macro || 单词编码 == 符_pop_macro) {
        int t = 单词编码, v;
        符号 *s;

        if (带有宏替换的下个标记(), 单词编码 != '(')
            goto pragma_err;
        if (带有宏替换的下个标记(), 单词编码 != 常量_字符串)
            goto pragma_err;
        v = 单词表_查找(单词值.str.data, 单词值.str.size - 1)->单词编码;
        if (带有宏替换的下个标记(), 单词编码 != ')')
            goto pragma_err;
        if (t == 符_push_macro) {
            while (NULL == (s = 宏定义_查找(v)))
                宏定义_处理(v, 0, NULL, NULL);
            s->type.ref = s; /* 设置推送边界 */
        } else {
            for (s = 宏定义符号_堆栈; s; s = s->prev)
                if (s->v == v && s->type.ref == s) {
                    s->type.ref = NULL;
                    break;
                }
        }
        if (s)
            单词表[v - 符_识别]->sym_define = s->d ? s : NULL;
        else
            zhi_警告("不平衡的 #pragma pop_macro");
        词法分析_调试_标识符 = t, 词法分析_调试_字符值 = v;

    } else if (单词编码 == 符_once) {
        查找_缓存的_导入文件(态1, file->文件名, 1)->once = 词法分析_第一次;

    } else if (态1->输出_类型 == ZHI_输出_预处理) {
        /* zhi -E：保持语用表保持不变 */
        设为_指定标识符(' ');
        设为_指定标识符(关键词_PRAGMA);
        设为_指定标识符('#');
        设为_指定标识符(符_换行);

    } else if (单词编码 == 符_pack) {
        带有宏替换的下个标记();
        跳过('(');
        if (单词编码 == 符_汇编_pop) {
            带有宏替换的下个标记();
            if (态1->包_堆_ptr <= 态1->包_堆) {
            stk_error:
                错误_打印("超出了堆包");
            }
            态1->包_堆_ptr--;
        } else {
            int val = 0;
            if (单词编码 != ')') {
                if (单词编码 == 符_汇编_push) {
                    带有宏替换的下个标记();
                    if (态1->包_堆_ptr >= 态1->包_堆 + 包_堆栈_大小 - 1)
                        goto stk_error;
                    态1->包_堆_ptr++;
                    跳过(',');
                }
                if (单词编码 != 常量_整数)
                    goto pragma_err;
                val = 单词值.i;
                if (val < 1 || val > 16 || (val & (val - 1)) != 0)
                    goto pragma_err;
                带有宏替换的下个标记();
            }
            *态1->包_堆_ptr = val;
        }
        if (单词编码 != ')')
            goto pragma_err;

    } else if (单词编码 == 符_comment) {
        char *p; int t;
        带有宏替换的下个标记();
        跳过('(');
        t = 单词编码;
        带有宏替换的下个标记();
        跳过(',');
        if (单词编码 != 常量_字符串)
            goto pragma_err;
        p = 字符串_宽度加1((char *)单词值.str.data);
        带有宏替换的下个标记();
        if (单词编码 != ')')
            goto pragma_err;
        if (t == 符_lib) {
            动态数组_追加元素(&态1->语法_库数, &态1->数量_语法_库数, p);
        } else {
            if (t == 符_option)
                设置编译选项(态1, p);
            内存_释放(p);
        }

    } else if (态1->警告_不支持) {
        zhi_警告("#pragma %s 被忽略", 取_单词字符串(单词编码, &单词值));
    }
    return;

pragma_err:
    错误_打印(" #pragma 指令格式错误");
    return;
}

/* 如果文件开头的第一个非空间标识符，则is_bof为true */
静态_函数 void _预处理(int is_bof)
{
    知心状态机 *态1 = zhi_状态;
    int i, c, n, saved_解析_标记;
    char buf[1024], *q;
    符号 *s;

    saved_解析_标记 = 解析_标记;
    解析_标记 = 解析_标记_预处理| 解析_标记_标识符_数字| 解析_标记_单词字符串| 解析_标记_换行符| (解析_标记 & 解析_标记_汇编_文件);

    取_下个符号_不宏扩展();
 redo:
    switch(单词编码) {
    case 关键词_DEFINE:
    case 关键词_定义:
        词法分析_调试_标识符 = 单词编码;
        取_下个符号_不宏扩展();
        词法分析_调试_字符值 = 单词编码;
        解析_宏定义();
        break;
    case 关键词_UNDEF:
    case 关键词_取消定义:
        词法分析_调试_标识符 = 单词编码;
        取_下个符号_不宏扩展();
        词法分析_调试_字符值 = 单词编码;
        s = 宏定义_查找(单词编码);
        /* 通过输入无效名称来定义符号 */
        if (s)
            未宏定义_符号为NULL(s);
        break;
    case 关键词_INCLUDE:
    case 关键词_导入:
    case 关键词_INCLUDE_NEXT:
    case 关键词_导入_下个:
        当前取到的源码字符 = file->buf_ptr[0];
        /* XXX: 如果注释不正确：以特殊模式使用next_nomacro */
        跳过_空格();
        if (当前取到的源码字符 == '<') {
            c = '>';
            goto read_name;
        } else if (当前取到的源码字符 == '\"') {
            c = 当前取到的源码字符;
        read_name:
            读下个符号处理缓冲区结尾();
            q = buf;
            while (当前取到的源码字符 != c && 当前取到的源码字符 != '\n' && 当前取到的源码字符 != CH_文件结尾) {
                if ((q - buf) < sizeof(buf) - 1)
                    *q++ = 当前取到的源码字符;
                if (当前取到的源码字符 == '\\') {
                    if (处理_转义_无错误() == 0)
                        --q;
                } else
                    读下个符号处理缓冲区结尾();
            }
            *q = '\0';
            仅处理第一个斜杠();
#if 0
            /* 吃掉所有空格并在包含之后发表评论 */
            /* XXX: 稍微不正确 */
            while (ch1 != '\n' && ch1 != CH_文件结尾)
                读下个符号处理缓冲区结尾();
#endif
        } else {
	    int len;
            /* 计算出的#include：将所有内容连接到换行符，结果必须是两种可接受的形式之一。请勿在此处将pp标识符转换为标识符。  */
	    解析_标记 = (解析_标记_预处理
			   | 解析_标记_换行符
			   | (解析_标记 & 解析_标记_汇编_文件));
            带有宏替换的下个标记();
            buf[0] = '\0';
	    while (单词编码 != 符_换行) {
		连接_字符串(buf, sizeof(buf), 取_单词字符串(单词编码, &单词值));
		带有宏替换的下个标记();
	    }
	    len = strlen(buf);
	    /* 检查语法并删除'<> |“”'*/
	    if ((len < 2 || ((buf[0] != '"' || buf[len-1] != '"') &&
			     (buf[0] != '<' || buf[len-1] != '>'))))
	        错误_打印("'#include' 之后应该有 \"FILENAME\" 或 <FILENAME>");
	    c = buf[len-1];
	    memmove(buf, buf + 1, len - 2);
	    buf[len - 2] = '\0';
        }

        if (态1->包含_堆_ptr >= 态1->包含_堆 + 包含_堆栈_大小)
            错误_打印("#include递归太深");
        /* 将当前文件推送到堆栈 */
        *态1->包含_堆_ptr++ = file;
        i = (单词编码 == 关键词_INCLUDE_NEXT || 单词编码 == 关键词_导入_下个) ? file->include_next_index + 1 : 0;
        n = 2 + 态1->数量_包含_路径 + 态1->数量_系统包含_路径;
        for (; i < n; ++i) {
            char buf1[sizeof file->文件名];
            导入文件缓存 *e;
            const char *path;

            if (i == 0) {
                /* 检查绝对包含路径 */
                if (!IS_ABSPATH(buf))
                    continue;
                buf1[0] = 0;

            } else if (i == 1) {
                /* 在文件的目录中搜索“ header.h” */
                if (c != '\"')
                    continue;
                /* https://savannah.nongnu.org/bugs/index.php?50847
                 * #line指令损坏了#include搜索路径
                 *  */
                path = file->true_filename;
                复制_字符串(buf1, path, 取_文件基本名(path) - path);

            } else {
                /* 搜索所有包含路径 */
                int j = i - 2, k = j - 态1->数量_包含_路径;
                path = k < 0 ? 态1->包含_路径[j] : 态1->系统包含_路径[k];
                p字符串复制(buf1, sizeof(buf1), path);
                连接_字符串(buf1, sizeof(buf1), "/");
            }

            连接_字符串(buf1, sizeof(buf1), buf);
            e = 查找_缓存的_导入文件(态1, buf1, 0);
            if (e && (宏定义_查找(e->ifndef_macro) || e->once == 词法分析_第一次)) {
                /* 无需解析包含，因为已定义了“ ifndef宏”（或具有一次#pragma） */
#ifdef 导入文件_调试
                printf("%s: 跳过缓存的 %s\n", file->文件名, buf1);
#endif
                goto include_done;
            }

            if (打开一个新文件(态1, buf1) < 0)
                continue;

            file->include_next_index = i;
#ifdef 导入文件_调试
            printf("%s: 包含 %s\n", file->prev->文件名, file->文件名);
#endif
            /* 更新目标部门 */
            if (态1->生成_依赖) {
                缓冲文件 *bf = file;
                while (i == 1 && (bf = bf->prev))
                    i = bf->include_next_index;
                /* 跳过系统导入文件 */
                if (n - i > 态1->数量_系统包含_路径)
                    动态数组_追加元素(&态1->目标_依赖, &态1->数量_目标_依赖,
                        字符串_宽度加1(buf1));
            }
            /* 添加导入文件调试信息 */
            zhi_调试_导入文件的开头(zhi_状态);
            标识符_标记 |= 符_标记_文件开始前 | 符_标记_行开始前;
            当前取到的源码字符 = file->buf_ptr[0];
            goto the_end;
        }
        错误_打印("找不到导入文件 '%s' ", buf);
include_done:
        --态1->包含_堆_ptr;
        break;
    case 关键词_IFNDEF:
    case 关键词_如果未定义:
        c = 1;
        goto do_ifdef;
    case 关键词_IF:
    case 关键词_如果:
        c = 预处理_表达式();
        goto do_if;
    case 关键词_IFDEF:
    case 关键词_如果已定义:
        c = 0;
    do_ifdef:
        取_下个符号_不宏扩展();
        if (单词编码 < 符_识别)
            错误_打印(" '#if%sdef' 的无效参数", c ? "n" : "");
        if (is_bof) {
            if (c) {
#ifdef 导入文件_调试
                printf("#ifndef %s\n", 取_单词字符串(单词编码, NULL));
#endif
                file->ifndef_macro = 单词编码;
            }
        }
        c = (宏定义_查找(单词编码) != 0) ^ c;
    do_if:
        if (态1->如果已宏定义_堆_ptr >= 态1->如果已宏定义_堆 + 如果定义_堆栈_大小)
            错误_打印("内存已满（ifdef）");
        *态1->如果已宏定义_堆_ptr++ = c;
        goto test_skip;
    case 关键词_ELSE:
    case 关键词_否则:
        if (态1->如果已宏定义_堆_ptr == 态1->如果已宏定义_堆)
            错误_打印("#else不匹配#if");
        if (态1->如果已宏定义_堆_ptr[-1] & 2)
            错误_打印("#else之后#else");
        c = (态1->如果已宏定义_堆_ptr[-1] ^= 3);
        goto test_else;
    case 关键词_ELIF:
    case 关键词_否则如果:
        if (态1->如果已宏定义_堆_ptr == 态1->如果已宏定义_堆)
            错误_打印("#elif没有匹配的#if");
        c = 态1->如果已宏定义_堆_ptr[-1];
        if (c > 1)
            错误_打印("#else之后#elif");
        /* 最后的＃if /＃elif表达式为true：我们跳过 */
        if (c == 1) {
            c = 0;
        } else {
            c = 预处理_表达式();
            态1->如果已宏定义_堆_ptr[-1] = c;
        }
    test_else:
        if (态1->如果已宏定义_堆_ptr == file->如果已宏定义_堆_ptr + 1)
            file->ifndef_macro = 0;
    test_skip:
        if (!(c & 1)) {
            跳过_预处理();
            is_bof = 0;
            goto redo;
        }
        break;
    case 关键词_ENDIF:
    case 关键词_结束如果:
        if (态1->如果已宏定义_堆_ptr <= file->如果已宏定义_堆_ptr)
            错误_打印("#endif没有匹配的#if");
        态1->如果已宏定义_堆_ptr--;
        /* “ #ifndef宏”位于文件的开头。 现在，我们检查“ #endif”是否恰好在文件末尾 */
        if (file->ifndef_macro &&
        		态1->如果已宏定义_堆_ptr == file->如果已宏定义_堆_ptr) {
            file->ifndef_macro_saved = file->ifndef_macro;
            /* 如果文件中间还有另一个#ifndef，则需要将其设置为零以避免错误的匹配 */
            file->ifndef_macro = 0;
            while (单词编码 != 符_换行)
                取_下个符号_不宏扩展();
            标识符_标记 |= 符_标记_结束如果;
            goto the_end;
        }
        break;
    case 常量_预处理编号:
        n = strtoul((char*)单词值.str.data, &q, 10);
        goto _line_num;
    case 关键词_LINE:
    case 管家词_行号:
        带有宏替换的下个标记();
        if (单词编码 != 常量_整数)
    _line_err:
            错误_打印("错误的#line格式");
        n = 单词值.i;
    _line_num:
        带有宏替换的下个标记();
        if (单词编码 != 符_换行) {
            if (单词编码 == 常量_字符串) {
                if (file->true_filename == file->文件名)
                    file->true_filename = 字符串_宽度加1(file->文件名);
                /* 从真实文件添加目录 */
                p字符串复制(buf, sizeof buf, file->true_filename);
                *取_文件基本名(buf) = 0;
                连接_字符串(buf, sizeof buf, (char *)单词值.str.data);
                zhi_调试_备用文件(态1, buf);
            } else if (解析_标记 & 解析_标记_汇编_文件)
                break;
            else
                goto _line_err;
            --n;
        }
        if (file->fd > 0)
            总_行数 += file->line_num - n;
        file->line_num = n;
        break;
    case 关键词_ERROR:
    case 关键词_错误:
    case 关键词_WARNING:
    case 关键词_警告:
        c = 单词编码;
        当前取到的源码字符 = file->buf_ptr[0];
        跳过_空格();
        q = buf;
        while (当前取到的源码字符 != '\n' && 当前取到的源码字符 != CH_文件结尾) {
            if ((q - buf) < sizeof(buf) - 1)
                *q++ = 当前取到的源码字符;
            if (当前取到的源码字符 == '\\') {
                if (处理_转义_无错误() == 0)
                    --q;
            } else
                读下个符号处理缓冲区结尾();
        }
        *q = '\0';
        if (c == 关键词_ERROR || c == 关键词_错误)
            错误_打印("#error %s", buf);
        else
            zhi_警告("#warning %s", buf);
        break;
    case 关键词_PRAGMA:
    case 关键词_杂注:
        语用_解析(态1);
        break;
    case 符_换行:
        goto the_end;
    default:
        /* 忽略“ S”文件中的天然气行注释。 */
        if (saved_解析_标记 & 解析_标记_汇编_文件)
            goto ignore;
        if (单词编码 == '!' && is_bof)
            /*'！' 开始时会被忽略以允许使用C脚本。 */
            goto ignore;
        zhi_警告("忽略未知的预处理指令 #%s", 取_单词字符串(单词编码, &单词值));
    ignore:
        file->buf_ptr = 解析_单行_注释(file->buf_ptr - 1);
        goto the_end;
    }
    /* 忽略其他预处理命令或＃！ 对于C脚本 */
    while (单词编码 != 符_换行)
        取_下个符号_不宏扩展();
 the_end:
    解析_标记 = saved_解析_标记;
}

static void 解析_转义_字符串(动态字符串 *outstr, const uint8_t *buf, int is_long)
{
    int c, n;
    const uint8_t *p;

    p = buf;
    for(;;) {
        c = *p;
        if (c == '\0')
            break;
        if (c == '\\') {
            p++;
            /* 逃逸 */
            c = *p;
            switch(c) {
            case '0': case '1': case '2': case '3':
            case '4': case '5': case '6': case '7':
                /* 最多三位八进制数字 */
                n = c - '0';
                p++;
                c = *p;
                if (isoct(c)) {
                    n = n * 8 + c - '0';
                    p++;
                    c = *p;
                    if (isoct(c)) {
                        n = n * 8 + c - '0';
                        p++;
                    }
                }
                c = n;
                goto add_char_nonext;
            case 'x':
            case 'u':
            case 'U':
                p++;
                n = 0;
                for(;;) {
                    c = *p;
                    if (c >= 'a' && c <= 'f')
                        c = c - 'a' + 10;
                    else if (c >= 'A' && c <= 'F')
                        c = c - 'A' + 10;
                    else if (是数字(c))
                        c = c - '0';
                    else
                        break;
                    n = n * 16 + c;
                    p++;
                }
                c = n;
                goto add_char_nonext;
            case 'a':
                c = '\a';
                break;
            case 'b':
                c = '\b';
                break;
            case 'f':
                c = '\f';
                break;
            case 'n':
                c = '\n';
                break;
            case 'r':
                c = '\r';
                break;
            case 't':
                c = '\t';
                break;
            case 'v':
                c = '\v';
                break;
            case 'e':
                if (!gnu_扩展)
                    goto invalid_escape;
                c = 27;
                break;
            case '\'':
            case '\"':
            case '\\': 
            case '?':
                break;
            default:
            invalid_escape:
                if (c >= '!' && c <= '~')
                    zhi_警告("未知的转义序列: \'\\%c\'", c);
                else
                    zhi_警告("未知的转义序列: \'\\x%x\'", c);
                break;
            }
        } else if (is_long && c >= 0x80) {
            /* 假设我们正在处理UTF-8序列 */
            /* 参考：Unicode标准，版本10.0，ch3.9 */

            int cont; /* 连续字节数 */
            int 跳过; /* 发生错误时应跳过多少个字节 */
            int i;

            /* 解码前导字节 */
            if (c < 0xC2) {
	            跳过 = 1; goto invalid_utf8_sequence;
            } else if (c <= 0xDF) {
	            cont = 1; n = c & 0x1f;
            } else if (c <= 0xEF) {
	            cont = 2; n = c & 0xf;
            } else if (c <= 0xF4) {
	            cont = 3; n = c & 0x7;
            } else {
	            跳过 = 1; goto invalid_utf8_sequence;
            }

            /* 解码连续字节 */
            for (i = 1; i <= cont; i++) {
                int l = 0x80, h = 0xBF;

                /* 调整第二个字节的限制 */
                if (i == 1) {
                    switch (c) {
                    case 0xE0: l = 0xA0; break;
                    case 0xED: h = 0x9F; break;
                    case 0xF0: l = 0x90; break;
                    case 0xF4: h = 0x8F; break;
                    }
                }

                if (p[i] < l || p[i] > h) {
                    跳过 = i; goto invalid_utf8_sequence;
                }

                n = (n << 6) | (p[i] & 0x3f);
            }

            /* 前进指针 */
            p += 1 + cont;
            c = n;
            goto add_char_nonext;

            /* 错误处理 */
        invalid_utf8_sequence:
            zhi_警告("格式错误的UTF-8子序列，开头为: \'\\x%x\'", c);
            c = 0xFFFD;
            p += 跳过;
            goto add_char_nonext;

        }
        p++;
    add_char_nonext:
        if (!is_long)
            动态字符串_追加单个字符(outstr, c);
        else {
#ifdef ZHI_TARGET_PE
            /* 存储为UTF-16 */
            if (c < 0x10000) {
                动态字符串_追加一个宽字符(outstr, c);
            } else {
                c -= 0x10000;
                动态字符串_追加一个宽字符(outstr, (c >> 10) + 0xD800);
                动态字符串_追加一个宽字符(outstr, (c & 0x3FF) + 0xDC00);
            }
#else
            动态字符串_追加一个宽字符(outstr, c);
#endif
        }
    }
    /* 添加结尾 '\0' */
    if (!is_long)
        动态字符串_追加单个字符(outstr, '\0');
    else
        动态字符串_追加一个宽字符(outstr, '\0');
}

static void 解析_字符串(const char *s, int len)
{
    uint8_t buf[1000], *p = buf;
    int is_long, sep;

    if ((is_long = *s == 'L'))
        ++s, --len;
    sep = *s++;
    len -= 2;
    if (len >= sizeof buf)
        p = 内存_申请(len + 1);
    memcpy(p, s, len);
    p[len] = 0;

    动态字符串_重置(&当前单词字符串);
    解析_转义_字符串(&当前单词字符串, p, is_long);
    if (p != buf)
        内存_释放(p);

    if (sep == '\'') {
        int char_size, i, n, c;
        /* XXX: 使其便携 */
        if (!is_long)
            单词编码 = 常量_字符型, char_size = 1;
        else
            单词编码 = 常量_长字符型, char_size = sizeof(nwchar_t);
        n = 当前单词字符串.字符串长度 / char_size - 1;
        if (n < 1)
            错误_打印("空字符常量");
        if (n > 1)
            zhi_警告("多字符常量");
        for (c = i = 0; i < n; ++i) {
            if (is_long)
                c = ((nwchar_t *)当前单词字符串.指向字符串的指针)[i];
            else
                c = (c << 8) | ((char *)当前单词字符串.指向字符串的指针)[i];
        }
        单词值.i = c;
    } else {
        单词值.str.size = 当前单词字符串.字符串长度;
        单词值.str.data = 当前单词字符串.指向字符串的指针;
        if (!is_long)
            单词编码 = 常量_字符串;
        else
            单词编码 = 常量_长字符串;
    }
}

/* 我们使用64位数字 */
#define BN_SIZE 2
static void bn_lshift(unsigned int *bn, int shift, int or_val)
{
    int i;
    unsigned int v;
    for(i=0;i<BN_SIZE;i++)
    {
        v = bn[i];
        bn[i] = (v << shift) | or_val;
        or_val = v >> (32 - shift);
    }
}

static void bn_零(unsigned int *bn)
{
    int i;
    for(i=0;i<BN_SIZE;i++) {
        bn[i] = 0;
    }
}

/* 解析空终止字符串'p'中的数字，并以当前标识符返回 */
static void 解析_数字(const char *p)
{
    int b, t, shift, frac_bits, s, exp_val, 当前取到的源码字符;
    char *q;
    unsigned int bn[BN_SIZE];
    double d;

    /* number */
    q = 标识符_缓冲;
    当前取到的源码字符 = *p++;
    t = 当前取到的源码字符;
    当前取到的源码字符 = *p++;
    *q++ = t;
    b = 10;
    if (t == '.') {
        goto float_frac_parse;
    } else if (t == '0') {
        if (当前取到的源码字符 == 'x' || 当前取到的源码字符 == 'X') {
            q--;
            当前取到的源码字符 = *p++;
            b = 16;
        } else if (zhi_状态->zhi_扩展 && (当前取到的源码字符 == 'b' || 当前取到的源码字符 == 'B')) {
            q--;
            当前取到的源码字符 = *p++;
            b = 2;
        }
    }
    /* 解析所有数字。 由于浮点常量，目前无法检查八进制数 */
    while (1) {
        if (当前取到的源码字符 >= 'a' && 当前取到的源码字符 <= 'f')
            t = 当前取到的源码字符 - 'a' + 10;
        else if (当前取到的源码字符 >= 'A' && 当前取到的源码字符 <= 'F')
            t = 当前取到的源码字符 - 'A' + 10;
        else if (是数字(当前取到的源码字符))
            t = 当前取到的源码字符 - '0';
        else
            break;
        if (t >= b)
            break;
        if (q >= 标识符_缓冲 + 字符串_最大_长度) {
        num_too_long:
            错误_打印("数字太长");
        }
        *q++ = 当前取到的源码字符;
        当前取到的源码字符 = *p++;
    }
    if (当前取到的源码字符 == '.' ||
        ((当前取到的源码字符 == 'e' || 当前取到的源码字符 == 'E') && b == 10) ||
        ((当前取到的源码字符 == 'p' || 当前取到的源码字符 == 'P') && (b == 16 || b == 2))) {
        if (b != 10) {
            /* 注意：strtox应该支持十六进制数字，但是非ISOC99 libcs不支持它，因此我们更愿意手工完成 */
            /* 十六进制或二进制浮点数 */
            /* XXX: 处理溢出 */
            *q = '\0';
            if (b == 16)
                shift = 4;
            else 
                shift = 1;
            bn_零(bn);
            q = 标识符_缓冲;
            while (1) {
                t = *q++;
                if (t == '\0') {
                    break;
                } else if (t >= 'a') {
                    t = t - 'a' + 10;
                } else if (t >= 'A') {
                    t = t - 'A' + 10;
                } else {
                    t = t - '0';
                }
                bn_lshift(bn, shift, t);
            }
            frac_bits = 0;
            if (当前取到的源码字符 == '.') {
                当前取到的源码字符 = *p++;
                while (1) {
                    t = 当前取到的源码字符;
                    if (t >= 'a' && t <= 'f') {
                        t = t - 'a' + 10;
                    } else if (t >= 'A' && t <= 'F') {
                        t = t - 'A' + 10;
                    } else if (t >= '0' && t <= '9') {
                        t = t - '0';
                    } else {
                        break;
                    }
                    if (t >= b)
                        错误_打印("无效数字");
                    bn_lshift(bn, shift, t);
                    frac_bits += shift;
                    当前取到的源码字符 = *p++;
                }
            }
            if (当前取到的源码字符 != 'p' && 当前取到的源码字符 != 'P')
                应为("指数");
            当前取到的源码字符 = *p++;
            s = 1;
            exp_val = 0;
            if (当前取到的源码字符 == '+') {
                当前取到的源码字符 = *p++;
            } else if (当前取到的源码字符 == '-') {
                s = -1;
                当前取到的源码字符 = *p++;
            }
            if (当前取到的源码字符 < '0' || 当前取到的源码字符 > '9')
                应为("指数位数");
            while (当前取到的源码字符 >= '0' && 当前取到的源码字符 <= '9') {
                exp_val = exp_val * 10 + 当前取到的源码字符 - '0';
                当前取到的源码字符 = *p++;
            }
            exp_val = exp_val * s;
            
            /* 现在我们可以生成数字 */
            /* XXX: 应直接修补浮点数 */
            d = (double)bn[1] * 4294967296.0 + (double)bn[0];
            d = ldexp(d, exp_val - frac_bits);
            t = toup(当前取到的源码字符);
            if (t == 'F') {
                当前取到的源码字符 = *p++;
                单词编码 = 常量_浮点型;
                /* float：应该处理溢出 */
                单词值.f = (float)d;
            } else if (t == 'L') {
                当前取到的源码字符 = *p++;
#ifdef ZHI_TARGET_PE
                单词编码 = 常量_双精度;
                单词值.d = d;
#else
                单词编码 = 常量_长双精度;
                /* XXX: 不够大 */
                单词值.ld = (long double)d;
#endif
            } else {
                单词编码 = 常量_双精度;
                单词值.d = d;
            }
        } else {
            /* 十进制浮点数 */
            if (当前取到的源码字符 == '.') {
                if (q >= 标识符_缓冲 + 字符串_最大_长度)
                    goto num_too_long;
                *q++ = 当前取到的源码字符;
                当前取到的源码字符 = *p++;
            float_frac_parse:
                while (当前取到的源码字符 >= '0' && 当前取到的源码字符 <= '9') {
                    if (q >= 标识符_缓冲 + 字符串_最大_长度)
                        goto num_too_long;
                    *q++ = 当前取到的源码字符;
                    当前取到的源码字符 = *p++;
                }
            }
            if (当前取到的源码字符 == 'e' || 当前取到的源码字符 == 'E') {
                if (q >= 标识符_缓冲 + 字符串_最大_长度)
                    goto num_too_long;
                *q++ = 当前取到的源码字符;
                当前取到的源码字符 = *p++;
                if (当前取到的源码字符 == '-' || 当前取到的源码字符 == '+') {
                    if (q >= 标识符_缓冲 + 字符串_最大_长度)
                        goto num_too_long;
                    *q++ = 当前取到的源码字符;
                    当前取到的源码字符 = *p++;
                }
                if (当前取到的源码字符 < '0' || 当前取到的源码字符 > '9')
                    应为("指数位数");
                while (当前取到的源码字符 >= '0' && 当前取到的源码字符 <= '9') {
                    if (q >= 标识符_缓冲 + 字符串_最大_长度)
                        goto num_too_long;
                    *q++ = 当前取到的源码字符;
                    当前取到的源码字符 = *p++;
                }
            }
            *q = '\0';
            t = toup(当前取到的源码字符);
            errno = 0;
            if (t == 'F') {
                当前取到的源码字符 = *p++;
                单词编码 = 常量_浮点型;
                单词值.f = strtof(标识符_缓冲, NULL);
            } else if (t == 'L') {
                当前取到的源码字符 = *p++;
#ifdef ZHI_TARGET_PE
                单词编码 = 常量_双精度;
                单词值.d = strtod(标识符_缓冲, NULL);
#else
                单词编码 = 常量_长双精度;
                单词值.ld = strtold(标识符_缓冲, NULL);
#endif
            } else {
                单词编码 = 常量_双精度;
                单词值.d = strtod(标识符_缓冲, NULL);
            }
        }
    } else {
        unsigned long long n, n1;
        int lcount, ucount, ov = 0;
        const char *p1;

        /* 整数 */
        *q = '\0';
        q = 标识符_缓冲;
        if (b == 10 && *q == '0') {
            b = 8;
            q++;
        }
        n = 0;
        while(1) {
            t = *q++;
            /* 除基数为10/8的错误外，无需检查 */
            if (t == '\0')
                break;
            else if (t >= 'a')
                t = t - 'a' + 10;
            else if (t >= 'A')
                t = t - 'A' + 10;
            else
                t = t - '0';
            if (t >= b)
                错误_打印("无效数字");
            n1 = n;
            n = n * b + t;
            /* 检测溢出 */
            if (n1 >= 0x1000000000000000ULL && n / b != n1)
                ov = 1;
        }

        /* 根据常量后缀确定常量类型必须具有的特征（无符号和/或64位） */
        lcount = ucount = 0;
        p1 = p;
        for(;;) {
            t = toup(当前取到的源码字符);
            if (t == 'L') {
                if (lcount >= 2)
                    错误_打印("整数常量中的三个 'l'");
                if (lcount && *(p - 1) != 当前取到的源码字符)
                    错误_打印("不正确的整数后缀: %s", p1);
                lcount++;
                当前取到的源码字符 = *p++;
            } else if (t == 'U') {
                if (ucount >= 1)
                    错误_打印("整数常量中的两个 'u'");
                ucount++;
                当前取到的源码字符 = *p++;
            } else {
                break;
            }
        }

        /* 确定是否需要64位和/或无符号才能适合 */
        if (ucount == 0 && b == 10) {
            if (lcount <= (LONG_SIZE == 4)) {
                if (n >= 0x80000000U)
                    lcount = (LONG_SIZE == 4) + 1;
            }
            if (n >= 0x8000000000000000ULL)
                ov = 1, ucount = 1;
        } else {
            if (lcount <= (LONG_SIZE == 4)) {
                if (n >= 0x100000000ULL)
                    lcount = (LONG_SIZE == 4) + 1;
                else if (n >= 0x80000000U)
                    ucount = 1;
            }
            if (n >= 0x8000000000000000ULL)
                ucount = 1;
        }

        if (ov)
            zhi_警告("整数常量溢出");

        单词编码 = 常量_整数;
	if (lcount) {
            单词编码 = 常量_长整数;
            if (lcount == 2)
                单词编码 = 常量_长长整数;
	}
	if (ucount)
	    ++单词编码; /* 符_CU... */
        单词值.i = n;
    }
    if (当前取到的源码字符)
        错误_打印("无效号码\n");
}


#define PARSE2(c1, tok1, c2, tok2)              \
    case c1:                                    \
        复杂转义(c, p);                            \
        if (c == c2) {                          \
            p++;                                \
            单词编码 = tok2;                         \
        } else {                                \
            单词编码 = tok1;                         \
        }                                       \
        break;

/* 返回下一个标识符而不进行宏替换 */
static inline void 取_下个符号_不宏扩展1(void)
{
    int t, c, is_long, len;
    单词存储结构 *ts;
    uint8_t *p, *p1;
    unsigned int h;

    p = file->buf_ptr;
 redo_no_start:
    c = *p;
    switch(c) {
    case ' ':
    case '\t':
        单词编码 = c;
        p++;
 maybe_space:
        if (解析_标记 & 解析_标记_空间)
            goto keep_标识符_标记;
        while (等值_表[*p - CH_文件结尾] & IS_SPC)
            ++p;
        goto redo_no_start;
    case '\f':
    case '\v':
    case '\r':
        p++;
        goto redo_no_start;
    case '\\':
        /* 首先查看它是否实际上是缓冲区的结尾 */
        c = 处理_转义1(p);
        p = file->buf_ptr;
        if (c == '\\')
            goto parse_simple;
        if (c != CH_文件结尾)
            goto redo_no_start;
        {
            知心状态机 *状态机1 = zhi_状态;
            if ((解析_标记 & 解析_标记_换行符)&& !(标识符_标记 & 符_标记_文件结尾))
            {
                标识符_标记 |= 符_标记_文件结尾;
                单词编码 = 符_换行;
                goto keep_标识符_标记;
            } else if (!(解析_标记 & 解析_标记_预处理))
            {
                单词编码 = 符_文件结尾;
            } else if (状态机1->如果已宏定义_堆_ptr != file->如果已宏定义_堆_ptr)
            {
                错误_打印("缺少 #endif");
            } else if (状态机1->包含_堆_ptr == 状态机1->包含_堆)
            {
                /* 没有包括左：文件末尾. */
                单词编码 = 符_文件结尾;
            } else {
                标识符_标记 &= ~符_标记_文件结尾;
                /* 弹出导入文件 */
                
                /* 在文件开始时测试先前的'#endif'是否在#ifdef之后 */
                if (标识符_标记 & 符_标记_结束如果)
                {
#ifdef 导入文件_调试
                    printf("#endif %s\n", 取_单词字符串(file->ifndef_macro_saved, NULL));
#endif
                    查找_缓存的_导入文件(状态机1, file->文件名, 1)
                        ->ifndef_macro = file->ifndef_macro_saved;
                    标识符_标记 &= ~符_标记_结束如果;
                }

                /* 添加导入文件调试信息的末尾 */
                zhi_调试_导入文件的结尾(zhi_状态);
                /* 弹出包含堆栈 */
                关闭文件();
                状态机1->包含_堆_ptr--;
                p = file->buf_ptr;
                if (p == file->buffer)
                    标识符_标记 = 符_标记_文件开始前|符_标记_行开始前;
                goto redo_no_start;
            }
        }
        break;

    case '\n':
        file->line_num++;
        标识符_标记 |= 符_标记_行开始前;
        p++;
maybe_newline:
        if (0 == (解析_标记 & 解析_标记_换行符))
            goto redo_no_start;
        单词编码 = 符_换行;
        goto keep_标识符_标记;

    case '#':
        /* XXX: 简化 */
        复杂转义(c, p);
        if ((标识符_标记 & 符_标记_行开始前) && 
            (解析_标记 & 解析_标记_预处理)) {
            file->buf_ptr = p;
            _预处理(标识符_标记 & 符_标记_文件开始前);
            p = file->buf_ptr;
            goto maybe_newline;
        } else {
            if (c == '#') {
                p++;
                单词编码 = 双符号_2个井号;
            } else {
                if (解析_标记 & 解析_标记_汇编_文件) {
                    p = 解析_单行_注释(p - 1);
                    goto redo_no_start;
                } else {
                    单词编码 = '#';
                }
            }
        }
        break;
    
    /* 不解析asm时，美元可以开始标识符 */
    case '$':
        if (!(等值_表[c - CH_文件结尾] & IS_ID)|| (解析_标记 & 解析_标记_汇编_文件))
            goto parse_simple;
    case 'a': case 'b': case 'c': case 'd':
    case 'e': case 'f': case 'g': case 'h':
    case 'i': case 'j': case 'k': case 'l':
    case 'm': case 'n': case 'o': case 'p':
    case 'q': case 'r': case 's': case 't':
    case 'u': case 'v': case 'w': case 'x':
    case 'y': case 'z': 
    case 'A': case 'B': case 'C': case 'D':
    case 'E': case 'F': case 'G': case 'H':
    case 'I': case 'J': case 'K': 
    case 'M': case 'N': case 'O': case 'P':
    case 'Q': case 'R': case 'S': case 'T':
    case 'U': case 'V': case 'W': case 'X':
    case 'Y': case 'Z': 
    case '_':
    parse_ident_fast:
        p1 = p;
        h = 符_哈希_初始化;
        h = 符_哈希_函数(h, c);
        while (c = *++p, 等值_表[c - CH_文件结尾] & (IS_ID|IS_NUM))
            h = 符_哈希_函数(h, c);
        len = p - p1;
        if (c != '\\') {
            单词存储结构 **pts;

            /* 快速案例：找不到散落的，所以我们有完整的标识符而且我们已经对其进行了哈希处理 */
            h &= (哈希表容量 - 1);
            pts = &单词_哈希表[h];
            for(;;) {
                ts = *pts;
                if (!ts)
                    break;
                if (ts->len == len && !memcmp(ts->str, p1, len))
                    goto 标识符_found;
                pts = &(ts->hash_next);
            }
            ts = 标识符_分配新内存(pts, (char *) p1, len);
        标识符_found: ;
        } else {
            /* 较慢的情况 */
            动态字符串_重置(&当前单词字符串);
            动态字符串_cat(&当前单词字符串, (char *) p1, len);
            p--;
            复杂转义(c, p);
        parse_ident_slow:
            while (等值_表[c - CH_文件结尾] & (IS_ID|IS_NUM))
            {
                动态字符串_追加单个字符(&当前单词字符串, c);
                复杂转义(c, p);
            }
            ts = 单词表_查找(当前单词字符串.指向字符串的指针, 当前单词字符串.字符串长度);
        }
        单词编码 = ts->单词编码;
        break;
    case 'L':
        t = p[1];
        if (t != '\\' && t != '\'' && t != '\"') {
            /* 快的方案 */
            goto parse_ident_fast;
        } else {
            复杂转义(c, p);
            if (c == '\'' || c == '\"') {
                is_long = 1;
                goto str_const;
            } else {
                动态字符串_重置(&当前单词字符串);
                动态字符串_追加单个字符(&当前单词字符串, 'L');
                goto parse_ident_slow;
            }
        }
        break;

    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7':
    case '8': case '9':
        t = c;
        复杂转义(c, p);
        /* 在第一个数字之后，接受数字，字母“。”。 或以'eEpP'开头的符号 */
    parse_num:
        动态字符串_重置(&当前单词字符串);
        for(;;) {
            动态字符串_追加单个字符(&当前单词字符串, t);
            if (!((等值_表[c - CH_文件结尾] & (IS_ID|IS_NUM))
                  || c == '.'
                  || ((c == '+' || c == '-')
                      && (((t == 'e' || t == 'E')
                            && !(解析_标记 & 解析_标记_汇编_文件
                                /* 0xe + 1是asm中的3个标识符 */
                                && ((char*)当前单词字符串.指向字符串的指针)[0] == '0'
                                && toup(((char*)当前单词字符串.指向字符串的指针)[1]) == 'X'))
                          || t == 'p' || t == 'P'))))
                break;
            t = c;
            复杂转义(c, p);
        }
        /* 我们添加尾随“ \ 0”以简化解析 */
        动态字符串_追加单个字符(&当前单词字符串, '\0');
        单词值.str.size = 当前单词字符串.字符串长度;
        单词值.str.data = 当前单词字符串.指向字符串的指针;
        单词编码 = 常量_预处理编号;
        break;

    case '.':
        /* 特殊的点处理，因为它也可以以数字开头 */
        复杂转义(c, p);
        if (是数字(c)) {
            t = '.';
            goto parse_num;
        } else if ((等值_表['.' - CH_文件结尾] & IS_ID)
                   && (等值_表[c - CH_文件结尾] & (IS_ID|IS_NUM))) {
            *--p = c = '.';
            goto parse_ident_fast;
        } else if (c == '.') {
            复杂转义(c, p);
            if (c == '.') {
                p++;
                单词编码 = 符_三个圆点;
            } else {
                *--p = '.'; /* 可能会下溢到file-> unget []中 */
                单词编码 = '.';
            }
        } else {
            单词编码 = '.';
        }
        break;
    case '\'':
    case '\"':
        is_long = 0;
    str_const:
        动态字符串_重置(&当前单词字符串);
        if (is_long)
            动态字符串_追加单个字符(&当前单词字符串, 'L');
        动态字符串_追加单个字符(&当前单词字符串, c);
        p = 解析字符串_不解析转义(p, c, &当前单词字符串);
        动态字符串_追加单个字符(&当前单词字符串, c);
        动态字符串_追加单个字符(&当前单词字符串, '\0');
        单词值.str.size = 当前单词字符串.字符串长度;
        单词值.str.data = 当前单词字符串.指向字符串的指针;
        单词编码 = 常量_预处理字符串;
        break;

    case '<':
        复杂转义(c, p);
        if (c == '=') {
            p++;
            单词编码 = 双符号_小于等于;
        } else if (c == '<') {
            复杂转义(c, p);
            if (c == '=') {
                p++;
                单词编码 = 符_A_SHL;
            } else {
                单词编码 = 双符号_左位移;
            }
        } else {
            单词编码 = 符_LT;
        }
        break;
    case '>':
        复杂转义(c, p);
        if (c == '=') {
            p++;
            单词编码 = 双符号_大于等于;
        } else if (c == '>') {
            复杂转义(c, p);
            if (c == '=') {
                p++;
                单词编码 = 符_A_SAR;
            } else {
                单词编码 = 双符号_右位移;
            }
        } else {
            单词编码 = 符_GT;
        }
        break;
        
    case '&':
        复杂转义(c, p);
        if (c == '&') {
            p++;
            单词编码 = 双符号_逻辑与;
        } else if (c == '=') {
            p++;
            单词编码 = 双符号_先求位与后赋值;
        } else {
            单词编码 = '&';
        }
        break;
        
    case '|':
        复杂转义(c, p);
        if (c == '|') {
            p++;
            单词编码 = 双符号_逻辑或;
        } else if (c == '=') {
            p++;
            单词编码 = 双符号_先求位或后赋值;
        } else {
            单词编码 = '|';
        }
        break;

    case '+':
        复杂转义(c, p);
        if (c == '+') {
            p++;
            单词编码 = 双符号_自加1;
        } else if (c == '=') {
            p++;
            单词编码 = 双符号_先求和后赋值;
        } else {
            单词编码 = '+';
        }
        break;
        
    case '-':
        复杂转义(c, p);
        if (c == '-') {
            p++;
            单词编码 = 双符号_自减1;
        } else if (c == '=') {
            p++;
            单词编码 = 双符号_先求差后赋值;
        } else if (c == '>') {
            p++;
            单词编码 = 双符号_结构体指针运算符;
        } else {
            单词编码 = '-';
        }
        break;

    PARSE2('!', '!', '=', 双符号_不等于)
    PARSE2('=', '=', '=', 双符号_等于)
    PARSE2('*', '*', '=', 双符号_先求积后赋值)
    PARSE2('%', '%', '=', 双符号_先取模后赋值)
    PARSE2('^', '^', '=', 双符号_先求异或后赋值)
        
        /* 注释或运算符 */
    case '/':
        复杂转义(c, p);
        if (c == '*') {
            p = 解析_注释(p);
            /* 注释用空格代替 */
            单词编码 = ' ';
            goto maybe_space;
        } else if (c == '/') {
            p = 解析_单行_注释(p);
            单词编码 = ' ';
            goto maybe_space;
        } else if (c == '=') {
            p++;
            单词编码 = 双符号_先求商后赋值;
        } else {
            单词编码 = '/';
        }
        break;
        
        /* 单一标识符 */
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
    case ',':
    case ';':
    case ':':
    case '?':
    case '~':
    case '@': /* 仅用于汇编程序 */
    parse_simple:
        单词编码 = c;
        p++;
        break;
    default:
        if (c >= 0x80 && c <= 0xFF) /* utf8 identifiers */
	    goto parse_ident_fast;
        if (解析_标记 & 解析_标记_汇编_文件)
            goto parse_simple;
        错误_打印("无法识别的角色 \\x%02x", c);
        break;
    }
    标识符_标记 = 0;
keep_标识符_标记:
    file->buf_ptr = p;
#if defined(PARSE_DEBUG)
    printf("标识符 = %d %s\n", 单词编码, 取_单词字符串(单词编码, &单词值));
#endif
}

static void 宏_替代(
    单词字符串 *tok_str,
    符号 **nested_list,
    const int *macro_str
    );

/* 用rgs（字段d）中的值替换macro_str中替换列表中的参数，并返回分配的字符串 */
static int *宏_参数_替换(符号 **nested_list, const int *macro_str, 符号 *args)
{
    int t, t0, t1, spc;
    const int *st;
    符号 *s;
    恒定值 cval;
    单词字符串 str;
    动态字符串 cstr;

    单词字符串_处理(&str);
    t0 = t1 = 0;
    while(1) {
        符_GET(&t, &macro_str, &cval);
        if (!t)
            break;
        if (t == '#') {
            /* 串化 */
            符_GET(&t, &macro_str, &cval);
            if (!t)
                goto bad_stringy;
            s = 符号_查找2(args, t);
            if (s) {
                动态字符串_初始化(&cstr);
                动态字符串_追加单个字符(&cstr, '\"');
                st = s->d;
                spc = 0;
                while (*st >= 0) {
                    符_GET(&t, &st, &cval);
                    if (t != 符_占位符
                     && t != 符_NOSUBST
                     && 0 == 检查_空格(t, &spc)) {
                        const char *s = 取_单词字符串(t, &cval);
                        while (*s) {
                            if (t == 常量_预处理字符串 && *s != '\'')
                                添加_字符(&cstr, *s);
                            else
                                动态字符串_追加单个字符(&cstr, *s);
                            ++s;
                        }
                    }
                }
                cstr.字符串长度 -= spc;
                动态字符串_追加单个字符(&cstr, '\"');
                动态字符串_追加单个字符(&cstr, '\0');
#ifdef PP_DEBUG
                printf("\nstringize: <%s>\n", (char *)cstr.指向字符串的指针;
#endif
                /* 添加字符串 */
                cval.str.size = cstr.字符串长度;
                cval.str.data = cstr.指向字符串的指针;
                单词字符串_增加大小2(&str, 常量_预处理字符串, &cval);
                动态字符串_释放(&cstr);
            } else {
        bad_stringy:
                应为(" '#'后的宏参数");
            }
        } else if (t >= 符_识别) {
            s = 符号_查找2(args, t);
            if (s) {
                int l0 = str.len;
                st = s->d;
                /* 如果'##'之前或之后存在，则无arg替换 */
                if (*macro_str == 符_PPJOIN || t1 == 符_PPJOIN) {
                    /* var arg宏的特殊情况：##如果空的VA_ARGS变量吃掉'，' */
                    if (t1 == 符_PPJOIN && t0 == ',' && gnu_扩展 && s->type.t) {
                        if (*st <= 0) {
                            /* 禁止'，''##' */
                            str.len -= 2;
                        } else {
                            /* 取消显示“ ##”并添加变量*/
                            str.len--;
                            goto add_var;
                        }
                    }
                } else {
            add_var:
		    if (!s->next) {
			/* 展开参数令牌并存储它们。 在大多数情况下，如果多次使用，我们还可以重新扩展每个参数，但是如果参数包含__COUNTER__宏，则不能。  */
			单词字符串 str2;
			符号_推送2(&s->next, s->v, s->type.t, 0);
			单词字符串_处理(&str2);
			宏_替代(&str2, nested_list, st);
			单词字符串_增加大小(&str2, 0);
			s->next->d = str2.str;
		    }
		    st = s->next->d;
                }
                for(;;) {
                    int t2;
                    符_GET(&t2, &st, &cval);
                    if (t2 <= 0)
                        break;
                    单词字符串_增加大小2(&str, t2, &cval);
                }
                if (str.len == l0) /* 扩展为空字符串 */
                    单词字符串_增加大小(&str, 符_占位符);
            } else {
                单词字符串_增加大小(&str, t);
            }
        } else {
            单词字符串_增加大小2(&str, t, &cval);
        }
        t0 = t1, t1 = t;
    }
    单词字符串_增加大小(&str, 0);
    return str.str;
}

static char const ab_月份_名称[12][4] =
{
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static int 粘贴_标识符(int t1, 恒定值 *v1, int t2, 恒定值 *v2)
{
    动态字符串 cstr;
    int n, ret = 1;

    动态字符串_初始化(&cstr);
    if (t1 != 符_占位符)
        动态字符串_cat(&cstr, 取_单词字符串(t1, v1), -1);
    n = cstr.字符串长度;
    if (t2 != 符_占位符)
        动态字符串_cat(&cstr, 取_单词字符串(t2, v2), -1);
    动态字符串_追加单个字符(&cstr, '\0');

    打开缓存文件(zhi_状态, ":paste:", cstr.字符串长度);
    memcpy(file->buffer, cstr.指向字符串的指针, cstr.字符串长度);
    标识符_标记 = 0;
    for (;;) {
        取_下个符号_不宏扩展1();
        if (0 == *file->buf_ptr)
            break;
        if (是_空格(单词编码))
            continue;
        zhi_警告("粘贴 \"%.*s\" 和 \"%s\" 没有给出有效的预处理标识符", n, (char *)cstr.指向字符串的指针, (char*)cstr.指向字符串的指针 + n);
        ret = 0;
        break;
    }
    关闭文件();
    动态字符串_释放(&cstr);
    return ret;
}

/* 处理“ ##”运算符。 如果未看到“ ##”，则返回NULL。 否则返回结果字符串（必须释放）. */
static inline int *宏_俩井(const int *ptr0)
{
    int t;
    恒定值 cval;
    单词字符串 macro_str1;
    int start_of_nosubsts = -1;
    const int *ptr;

    /* 我们搜索第一个“ ##” */
    for (ptr = ptr0;;) {
        符_GET(&t, &ptr, &cval);
        if (t == 符_PPJOIN)
            break;
        if (t == 0)
            return NULL;
    }

    单词字符串_处理(&macro_str1);

    //标识符_打印(" $$$", ptr0);
    for (ptr = ptr0;;) {
        符_GET(&t, &ptr, &cval);
        if (t == 0)
            break;
        if (t == 符_PPJOIN)
            continue;
        while (*ptr == 符_PPJOIN) {
            int t1; 恒定值 cv1;
            /* 给定'a ## b'，删除'a'之前的nosubstst */
            if (start_of_nosubsts >= 0)
                macro_str1.len = start_of_nosubsts;
            /* 给定'a ## b'，删除'b'之前的nosubsts */
            while ((t1 = *++ptr) == 符_NOSUBST)
                ;
            if (t1 && t1 != 符_PPJOIN) {
                符_GET(&t1, &ptr, &cv1);
                if (t != 符_占位符 || t1 != 符_占位符) {
                    if (粘贴_标识符(t, &cval, t1, &cv1)) {
                        t = 单词编码, cval = 单词值;
                    } else {
                        单词字符串_增加大小2(&macro_str1, t, &cval);
                        t = t1, cval = cv1;
                    }
                }
            }
        }
        if (t == 符_NOSUBST) {
            if (start_of_nosubsts < 0)
                start_of_nosubsts = macro_str1.len;
        } else {
            start_of_nosubsts = -1;
        }
        单词字符串_增加大小2(&macro_str1, t, &cval);
    }
    单词字符串_增加大小(&macro_str1, 0);
    //标识符_打印(" ###", macro_str1.str);
    return macro_str1.str;
}

/* 从函数宏调用中窥视或读取[ws_str == NULL]下一个标记，如有必要，将宏级别提升到文件*/
static int 从函数宏调用中_读取下一个标记(符号 **nested_list, 单词字符串 *ws_str)
{
    int t;
    const int *p;
    符号 *sa;

    for (;;) {
        if (宏_ptr) {
            p = 宏_ptr, t = *p;
            if (ws_str) {
                while (是_空格(t) || 符_换行 == t || 符_占位符 == t)
                    单词字符串_增加大小(ws_str, t), t = *++p;
            }
            if (t == 0) {
                结束_宏();
                /* 同样，嵌套定义符号的作用域结尾 */
                sa = *nested_list;
                while (sa && sa->v == 0)
                    sa = sa->prev;
                if (sa)
                    sa->v = 0;
                continue;
            }
        } else {
            当前取到的源码字符 = 处理_块的结尾();
            if (ws_str) {
                while (是_空格(当前取到的源码字符) || 当前取到的源码字符 == '\n' || 当前取到的源码字符 == '/') {
                    if (当前取到的源码字符 == '/') {
                        int c;
                        uint8_t *p = file->buf_ptr;
                        复杂转义(c, p);
                        if (c == '*') {
                            p = 解析_注释(p);
                            file->buf_ptr = p - 1;
                        } else if (c == '/') {
                            p = 解析_单行_注释(p);
                            file->buf_ptr = p - 1;
                        } else
                            break;
                        当前取到的源码字符 = ' ';
                    }
                    if (当前取到的源码字符 == '\n')
                        file->line_num++;
                    if (!(当前取到的源码字符 == '\f' || 当前取到的源码字符 == '\v' || 当前取到的源码字符 == '\r'))
                        单词字符串_增加大小(ws_str, 当前取到的源码字符);
                    cinp();
                }
            }
            t = 当前取到的源码字符;
        }

        if (ws_str)
            return t;
        取_下个符号_不宏扩展();
        return 单词编码;
    }
}

/* 用宏“ s”对当前令牌进行宏替换，并将结果添加到（tok_str，tok_len）。 “ nested_list”是我们为了避免递归而进入的所有宏的列表。 如果不需要替换，则返回非零 */
static int 宏替换_当前标识符(
    单词字符串 *tok_str,
    符号 **nested_list,
    符号 *s)
{
    符号 *args, *sa, *sa1;
    int parlevel, t, t1, spc;
    单词字符串 str;
    char *cstrval;
    恒定值 cval;
    动态字符串 cstr;
    char buf[32];

    /* 如果符号是宏，则准备替换 */
    /* 特殊宏 */
    if (单词编码 == 符___LINE__  || 单词编码 == 符___LINE___CN || 单词编码 == 符___COUNTER__ || 单词编码 == 符___COUNTER___CN) {
        t =(单词编码 == 符___LINE__ || 单词编码 == 符___LINE___CN) ? file->line_num : 词法分析_计数器++;
        snprintf(buf, sizeof(buf), "%d", t);
        cstrval = buf;
        t1 = 常量_预处理编号;
        goto add_cstr1;
    } else if (单词编码 == 符___FILE__ || 单词编码 == 符___FILE___CN) {
        cstrval = file->文件名;
        goto add_cstr;
    } else if (单词编码 == 符___DATE__ || 单词编码 == 符___DATE___CN || 单词编码 == 符___TIME__ || 单词编码 == 符___TIME___CN) {
        time_t ti;
        struct tm *tm;

        time(&ti);
        tm = localtime(&ti);
        if (单词编码 == 符___DATE__ || 单词编码 == 符___DATE___CN) {
            snprintf(buf, sizeof(buf), "%s %2d %d", 
                     ab_月份_名称[tm->tm_mon], tm->tm_mday, tm->tm_year + 1900);
        } else {
            snprintf(buf, sizeof(buf), "%02d:%02d:%02d", 
                     tm->tm_hour, tm->tm_min, tm->tm_sec);
        }
        cstrval = buf;
    add_cstr:
        t1 = 常量_字符串;
    add_cstr1:
        动态字符串_初始化(&cstr);
        动态字符串_cat(&cstr, cstrval, 0);
        cval.str.size = cstr.字符串长度;
        cval.str.data = cstr.指向字符串的指针;
        单词字符串_增加大小2(tok_str, t1, &cval);
        动态字符串_释放(&cstr);
    } else if (s->d) {
        int saved_解析_标记 = 解析_标记;
	int *joined_str = NULL;
        int *mstr = s->d;

        if (s->type.t == 宏_函数) {
            /* 宏名称和参数列表之间的空格 */
            单词字符串 ws_str;
            单词字符串_处理(&ws_str);

            spc = 0;
            解析_标记 |= 解析_标记_空间 | 解析_标记_换行符
                | 解析_标记_接受_转义;

            /* 从参数流中获取下一个标记 */
            t = 从函数宏调用中_读取下一个标记(nested_list, &ws_str);
            if (t != '(') {
                /* 毕竟不是宏替换，请恢复宏令牌以及我们已阅读的所有空白。故意不合并空白以保留换行符。 */
                解析_标记 = saved_解析_标记;
                单词字符串_增加大小(tok_str, 单词编码);
                if (解析_标记 & 解析_标记_空间) {
                    int i;
                    for (i = 0; i < ws_str.len; i++)
                        单词字符串_增加大小(tok_str, ws_str.str[i]);
                }
                单词字符串_释放_字符串(ws_str.str);
                return 0;
            } else {
                单词字符串_释放_字符串(ws_str.str);
            }
	    do {
		取_下个符号_不宏扩展(); /* eat '(' */
	    } while (单词编码 == 符_占位符 || 是_空格(单词编码));

            /* 参数宏 */
            args = NULL;
            sa = s->next;
            /* 注意：允许使用空参数，除非没有参数 */
            for(;;) {
                do {
                    从函数宏调用中_读取下一个标记(nested_list, NULL);
                } while (是_空格(单词编码) || 符_换行 == 单词编码);
    empty_arg:
                /* 处理'（）'实例 */
                if (!args && !sa && 单词编码 == ')')
                    break;
                if (!sa)
                    错误_打印("宏 '%s' 与过多的参数一起使用",
                          取_单词字符串(s->v, 0));
                单词字符串_处理(&str);
                parlevel = spc = 0;
                /* 注意：非零sa-> t表示VA_ARGS */
                while ((parlevel > 0 || 
                        (单词编码 != ')' && 
                         (单词编码 != ',' || sa->type.t)))) {
                    if (单词编码 == 符_文件结尾 || 单词编码 == 0)
                        break;
                    if (单词编码 == '(')
                        parlevel++;
                    else if (单词编码 == ')')
                        parlevel--;
                    if (单词编码 == 符_换行)
                        单词编码 = ' ';
                    if (!检查_空格(单词编码, &spc))
                        单词字符串_增加大小2(&str, 单词编码, &单词值);
                    从函数宏调用中_读取下一个标记(nested_list, NULL);
                }
                if (parlevel)
                    应为(")");
                str.len -= spc;
                单词字符串_增加大小(&str, -1);
                单词字符串_增加大小(&str, 0);
                sa1 = 符号_推送2(&args, sa->v & ~符号_字段, sa->type.t, 0);
                sa1->d = str.str;
                sa = sa->next;
                if (单词编码 == ')') {
                    /* gcc var args的特殊情况：如果省略，则添加一个空的var arg参数*/
                    if (sa && sa->type.t && gnu_扩展)
                        goto empty_arg;
                    break;
                }
                if (单词编码 != ',')
                    应为(",");
            }
            if (sa) {
                错误_打印("宏 '%s' 使用的参数太少",
                      取_单词字符串(s->v, 0));
            }

            /* 现在替换每个arg */
            mstr = 宏_参数_替换(nested_list, mstr, args);
            /* 内存_释放 */
            sa = args;
            while (sa) {
                sa1 = sa->prev;
                单词字符串_释放_字符串(sa->d);
                if (sa->next) {
                    单词字符串_释放_字符串(sa->next->d);
                    符号_释放(sa->next);
                }
                符号_释放(sa);
                sa = sa1;
            }
            解析_标记 = saved_解析_标记;
        }

        符号_推送2(nested_list, s->v, 0, 0);
        解析_标记 = saved_解析_标记;
        joined_str = 宏_俩井(mstr);
        宏_替代(tok_str, nested_list, joined_str ? joined_str : mstr);

        /* 弹出嵌套定义的符号 */
        sa1 = *nested_list;
        *nested_list = sa1->prev;
        符号_释放(sa1);
	if (joined_str)
	    单词字符串_释放_字符串(joined_str);
        if (mstr != s->d)
            单词字符串_释放_字符串(mstr);
    }
    return 0;
}

/* 对macro_str进行宏替换，并将结果添加到（tok_str，tok_len）。 “ nested_list”是我们为了避免递归而进入的所有宏的列表。 */
static void 宏_替代(
    单词字符串 *tok_str,
    符号 **nested_list,
    const int *macro_str
    )
{
    符号 *s;
    int t, spc, nosubst;
    恒定值 cval;
    
    spc = nosubst = 0;

    while (1) {
        符_GET(&t, &macro_str, &cval);
        if (t <= 0)
            break;

        if (t >= 符_识别 && 0 == nosubst) {
            s = 宏定义_查找(t);
            if (s == NULL)
                goto no_subst;

            /* 如果嵌套替换，则不执行任何操作 */
            if (符号_查找2(*nested_list, t)) {
                /* 并将其标记为符_NOSUBST，因此不会再次被替换 */
                单词字符串_增加大小2(tok_str, 符_NOSUBST, NULL);
                goto no_subst;
            }

            {
                单词字符串 *str = 单词字符串_分配();
                str->str = (int*)macro_str;
                开始_宏(str, 2);

                单词编码 = t;
                宏替换_当前标识符(tok_str, nested_list, s);

                if (宏_堆栈 != str) {
                    /* 通过读取函数宏参数已完成 */
                    break;
                }

                macro_str = 宏_ptr;
                结束_宏 ();
            }
            if (tok_str->len)
                spc = 是_空格(t = tok_str->str[tok_str->lastlen]);
        } else {
no_subst:
            if (!检查_空格(t, &spc))
                单词字符串_增加大小2(tok_str, t, &cval);

            if (nosubst) {
                if (nosubst > 1 && (spc || (++nosubst == 3 && t == '(')))
                    continue;
                nosubst = 0;
            }
            if (t == 符_NOSUBST)
                nosubst = 1;
        }
        /* 由于宏替换，GCC支持“定义” */
        if ((t == 关键词_DEFINED || t == 关键词_已定义)  && 词法分析_表达式)
            nosubst = 2;
    }
}

/* 返回下一个标记而不进行宏替换。 可以从宏_ptr缓冲区读取输入 */
static void 取_下个符号_不宏扩展(void)
{
    int t;
    if (宏_ptr) {
 redo:
        t = *宏_ptr;
        if (符_有_值(t)) {
            获取_标识符(&单词编码, &宏_ptr, &单词值);
            if (t == 常量_行号) {
                file->line_num = 单词值.i;
                goto redo;
            }
        } else {
            宏_ptr++;
            if (t < 符_识别) {
                if (!(解析_标记 & 解析_标记_空间)
                    && (等值_表[t - CH_文件结尾] & IS_SPC))
                    goto redo;
            }
            单词编码 = t;
        }
    } else {
        取_下个符号_不宏扩展1();
    }
}

/* 返回带有宏替换的下一个标记 */
静态_函数 void 带有宏替换的下个标记(void)
{
    int t;
 redo:
    取_下个符号_不宏扩展();
    t = 单词编码;
    if (宏_ptr) {
        if (!符_有_值(t)) {
            if (t == 符_NOSUBST || t == 符_占位符) {
                /* 丢弃预处理器标记 */
                goto redo;
            } else if (t == 0) {
                /* 宏结尾或获取令牌字符串 */
                结束_宏();
                goto redo;
            } else if (t == '\\') {
                if (!(解析_标记 & 解析_标记_接受_转义))
                    错误_打印("程序中流浪 '\\' ");
            }
            return;
        }
    } else if (t >= 符_识别 && (解析_标记 & 解析_标记_预处理)) {
        /* 如果从文件读取，请尝试替换宏 */
        符号 *s = 宏定义_查找(t);
        if (s) {
            符号 *nested_list = NULL;
            单词字符串_缓冲.len = 0;
            宏替换_当前标识符(&单词字符串_缓冲, &nested_list, s);
            单词字符串_增加大小(&单词字符串_缓冲, 0);
            开始_宏(&单词字符串_缓冲, 0);
            goto redo;
        }
        return;
    }
    /* 将预处理器标识符转换为C标识符 */
    if (t == 常量_预处理编号) {
        if  (解析_标记 & 解析_标记_标识符_数字)
            解析_数字((char *)单词值.str.data);
    } else if (t == 常量_预处理字符串) {
        if (解析_标记 & 解析_标记_单词字符串)
            解析_字符串((char *)单词值.str.data, 单词值.str.size - 1);
    }
}

/* 推回当前令牌并将当前令牌设置为“ last_tok”。 仅处理标签的标识符大小写. */
静态_内联 void 设为_指定标识符(int last_tok)
{

    单词字符串 *str = 单词字符串_分配();
    单词字符串_增加大小2(str, 单词编码, &单词值);
    单词字符串_增加大小(str, 0);
    开始_宏(str, 1);
    单词编码 = last_tok;
}

static void zhi_预定义(动态字符串 *cstr)
{
    动态字符串_cat(cstr,
#if defined ZHI_TARGET_X86_64
#ifndef ZHI_TARGET_PE
    /* va_list的GCC兼容定义。 这应该与我们的lib / hexinku1.c中的声明同步 */
    "typedef struct{\n"
    "unsigned gp_offset,fp_offset;\n"
    "union{\n"
    "unsigned overflow_offset;\n"
    "char*overflow_arg_area;\n"
    "};\n"
    "char*reg_save_area;\n"
    "}__builtin_va_list[1];\n"
    "void*__va_arg(__builtin_va_list ap,int arg_type,int size,int align);\n"
    "#define __builtin_va_start(ap,last) (*(ap)=*(__builtin_va_list)((char*)__builtin_frame_address(0)-24))\n"
    "#define __builtin_va_arg(ap,t) (*(t*)(__va_arg(ap,__builtin_va_arg_types(t),sizeof(t),__alignof__(t))))\n"
    "#define __builtin_va_copy(dest,src) (*(dest)=*(src))\n"
#else /* ZHI_TARGET_PE */
    "typedef char*__builtin_va_list;\n"
    "#define __builtin_va_arg(ap,t) ((sizeof(t)>8||(sizeof(t)&(sizeof(t)-1)))?**(t**)((ap+=8)-8):*(t*)((ap+=8)-8))\n"
    "#define __builtin_va_copy(dest,src) (dest)=(src)\n"
#endif
#elif defined ZHI_TARGET_ARM
    "typedef char*__builtin_va_list;\n"
    "#define _zhi_alignof(type) ((int)&((struct{char c;type x;}*)0)->x)\n"
    "#define _zhi_align(addr,type) (((unsigned)addr+_zhi_alignof(type)-1)&~(_zhi_alignof(type)-1))\n"
    "#define __builtin_va_start(ap,last) (ap=((char*)&(last))+((sizeof(last)+3)&~3))\n"
    "#define __builtin_va_arg(ap,type) (ap=(void*)((_zhi_align(ap,type)+sizeof(type)+3)&~3),*(type*)(ap-((sizeof(type)+3)&~3)))\n"
    "#define __builtin_va_copy(dest,src) (dest)=(src)\n"
#elif defined ZHI_TARGET_ARM64
    "typedef struct{\n"
    "void*__stack,*__gr_top,*__vr_top;\n"
    "int __gr_offs,__vr_offs;\n"
    "}__builtin_va_list;\n"
    "#define __builtin_va_copy(dest,src) (dest)=(src)\n"
#elif defined ZHI_TARGET_RISCV64
    "typedef char*__builtin_va_list;\n"
    "#define __va_reg_size (__riscv_xlen>>3)\n"
    "#define _zhi_align(addr,type) (((unsigned long)addr+__alignof__(type)-1)&-(__alignof__(type)))\n"
    "#define __builtin_va_arg(ap,type) (*(sizeof(type)>(2*__va_reg_size)?*(type**)((ap+=__va_reg_size)-__va_reg_size):(ap=(va_list)(_zhi_align(ap,type)+(sizeof(type)+__va_reg_size-1)&-__va_reg_size),(type*)(ap-((sizeof(type)+__va_reg_size-1)&-__va_reg_size)))))\n"
    "#define __builtin_va_copy(dest,src) (dest)=(src)\n"
#else /* ZHI_TARGET_I386 */
    "typedef char*__builtin_va_list;\n"
    "#define __builtin_va_start(ap,last) (ap=((char*)&(last))+((sizeof(last)+3)&~3))\n"
    "#define __builtin_va_arg(ap,t) (*(t*)((ap+=(sizeof(t)+3)&~3)-((sizeof(t)+3)&~3)))\n"
    "#define __builtin_va_copy(dest,src) (dest)=(src)\n"
#endif
    "#define __builtin_va_end(ap) (void)(ap)\n"
    , -1);
}

静态_函数 void 开始_预处理(知心状态机 *状态机1, int is_asm)
{
    动态字符串 cstr;

    词法分析_开始(状态机1);

    状态机1->包含_堆_ptr = 状态机1->包含_堆;
    状态机1->如果已宏定义_堆_ptr = 状态机1->如果已宏定义_堆;
    file->如果已宏定义_堆_ptr = 状态机1->如果已宏定义_堆_ptr;
    词法分析_表达式 = 0;
    词法分析_计数器 = 0;
    词法分析_调试_标识符 = 词法分析_调试_字符值 = 0;
    词法分析_第一次++;
    状态机1->包_堆[0] = 0;
    状态机1->包_堆_ptr = 状态机1->包_堆;

    设置_等值数('$', !is_asm && 状态机1->允许_在标识符中使用美元符号 ? IS_ID : 0);
    设置_等值数('.', is_asm ? IS_ID : 0);

    动态字符串_初始化(&cstr);
    if (状态机1->命令行_定义.字符串长度)
        动态字符串_cat(&cstr, 状态机1->命令行_定义.指向字符串的指针, 状态机1->命令行_定义.字符串长度);
    动态字符串_打印(&cstr, "#define __BASE_FILE__ \"%s\"\n", file->文件名);
    if (is_asm)
        动态字符串_打印(&cstr, "#define __ASSEMBLER__ 1\n");
    if (状态机1->输出_类型 == ZHI_输出_内存中运行)
        动态字符串_打印(&cstr, "#define __ZHI_RUN__ 1\n");
    if (!is_asm && 状态机1->输出_类型 != ZHI_输出_预处理)
        zhi_预定义(&cstr);
    if (状态机1->命令行_包含.字符串长度)
        动态字符串_cat(&cstr, 状态机1->命令行_包含.指向字符串的指针, 状态机1->命令行_包含.字符串长度);
    //printf("%s\n", (char*)cstr.data);
    *状态机1->包含_堆_ptr++ = file;
    打开缓存文件(状态机1, "<command line>", cstr.字符串长度);
    memcpy(file->buffer, cstr.指向字符串的指针, cstr.字符串长度);
    动态字符串_释放(&cstr);

    解析_标记 = is_asm ? 解析_标记_汇编_文件 : 0;
    标识符_标记 = 符_标记_行开始前 | 符_标记_文件开始前;
}

/* 从错误/ setjmp中清除 */
静态_函数 void 结束_预处理(知心状态机 *状态机1)
{
    while (宏_堆栈)
        结束_宏();
    宏_ptr = NULL;
    while (file)
        关闭文件();
    zhi词法_删除(状态机1);
}

静态_函数 void 词法分析_开始(知心状态机 *s)
{
    int i, c;
    const char *p, *r;

    /* 初始化是id表 */
    for(i = CH_文件结尾; i<128; i++)
        设置_等值数(i,
            是_空格(i) ? IS_SPC
            : 是id(i) ? IS_ID
            : 是数字(i) ? IS_NUM
            : 0);

    for(i = 128; i<256; i++)
        设置_等值数(i, IS_ID);

    /* 初始化分配器 */
    小分配器_新建(&单词字符_分配内存, 单词字符_小分配器_限制, 单词字符_小分配器_大小);
    小分配器_新建(&单词字符串_分配内存, 单词字符串_小分配器_限制, 单词字符串_小分配器_大小);

    memset(单词_哈希表, 0, 哈希表容量 * sizeof(单词存储结构 *));
    memset(s->缓存_包含数_哈希, 0, sizeof s->缓存_包含数_哈希);

    动态字符串_初始化(&动态字符串_缓冲);
    动态字符串_重分配内存(&动态字符串_缓冲, 字符串_最大_长度);
    单词字符串_处理(&单词字符串_缓冲);
    单词字符串_重分配(&单词字符串_缓冲, 单词字符串_最大_长度);

    单词_识别号 = 符_识别;
    p = 编译_关键词;
    while (*p) {
        r = p;
        for(;;) {
            c = *r++;
            if (c == '\0')
                break;
        }
        单词表_查找(p, r - p - 1);
        p = r;
    }

    /* 我们为一些特殊的宏添加了虚拟定义，以加快测试速度，并具有有效的define（） */
    宏定义_处理(符___LINE__, 宏_对象, NULL, NULL);
    宏定义_处理(符___FILE__, 宏_对象, NULL, NULL);
    宏定义_处理(符___DATE__, 宏_对象, NULL, NULL);
    宏定义_处理(符___TIME__, 宏_对象, NULL, NULL);
    宏定义_处理(符___COUNTER__, 宏_对象, NULL, NULL);
}

静态_函数 void zhi词法_删除(知心状态机 *s)
{
    int i, n;

    动态数组_重分配容量(&s->缓存_包含数, &s->数量_缓存_包含数);

    /* 释放标识符 */
    n = 单词_识别号 - 符_识别;
    if (n > 总_idents)
        总_idents = n;
    for(i = 0; i < n; i++)
        小分配器_释放(单词字符_分配内存, 单词表[i]);
    内存_释放(单词表);
    单词表 = NULL;

    /* 释放静态缓冲区 */
    动态字符串_释放(&当前单词字符串);
    动态字符串_释放(&动态字符串_缓冲);
    动态字符串_释放(&宏_等于_缓冲区);
    单词字符串_释放_字符串(单词字符串_缓冲.str);

    /* 释放分配器 */
    小分配器_删除(单词字符_分配内存);
    单词字符_分配内存 = NULL;
    小分配器_删除(单词字符串_分配内存);
    单词字符串_分配内存 = NULL;
}

/* ------------------------------------------------------------------------- */

static void 标识符_打印(const char *msg, const int *str)
{
    FILE *fp;
    int t, s = 0;
    恒定值 cval;

    fp = zhi_状态->预处理输出文件;
    fprintf(fp, "%s", msg);
    while (str) {
	符_GET(&t, &str, &cval);
	if (!t)
	    break;
	fprintf(fp, " %s" + s, 取_单词字符串(t, &cval)), s = 1;
    }
    fprintf(fp, "\n");
}

static void 词法_行(知心状态机 *状态机1, 缓冲文件 *f, int level)
{
    int d = f->line_num - f->line_ref;

    if (状态机1->DX标号 & 4)
	return;

    if (状态机1->P标号 == LINE_MACRO_OUTPUT_FORMAT_NONE) {
        ;
    } else if (level == 0 && f->line_ref && d < 8) {
	while (d > 0)
	    fputs("\n", 状态机1->预处理输出文件), --d;
    } else if (状态机1->P标号 == LINE_MACRO_OUTPUT_FORMAT_STD) {
	fprintf(状态机1->预处理输出文件, "#line %d \"%s\"\n", f->line_num, f->文件名);
    } else {
	fprintf(状态机1->预处理输出文件, "# %d \"%s\"%s\n", f->line_num, f->文件名,
	    level > 0 ? " 1" : level < 0 ? " 2" : "");
    }
    f->line_ref = f->line_num;
}

static void 宏定义_打印(知心状态机 *状态机1, int v)
{
    FILE *fp;
    符号 *s;

    s = 宏定义_查找(v);
    if (NULL == s || NULL == s->d)
        return;

    fp = 状态机1->预处理输出文件;
    fprintf(fp, "#define %s", 取_单词字符串(v, NULL));
    if (s->type.t == 宏_函数) {
        符号 *a = s->next;
        fprintf(fp,"(");
        if (a)
            for (;;) {
                fprintf(fp,"%s", 取_单词字符串(a->v & ~符号_字段, NULL));
                if (!(a = a->next))
                    break;
                fprintf(fp,",");
            }
        fprintf(fp,")");
    }
    标识符_打印("", s->d);
}

static void 词法_调试_宏定义(知心状态机 *状态机1)
{
    int v, t;
    const char *vs;
    FILE *fp;

    t = 词法分析_调试_标识符;
    if (t == 0)
        return;

    file->line_num--;
    词法_行(状态机1, file, 0);
    file->line_ref = ++file->line_num;

    fp = 状态机1->预处理输出文件;
    v = 词法分析_调试_字符值;
    vs = 取_单词字符串(v, NULL);
    if (t == 关键词_DEFINE || t == 关键词_定义) {
        宏定义_打印(状态机1, v);
    } else if (t == 关键词_UNDEF || t == 关键词_取消定义) {
        fprintf(fp, "#undef %s\n", vs);
    } else if (t == 符_push_macro) {
        fprintf(fp, "#pragma push_macro(\"%s\")\n", vs);
    } else if (t == 符_pop_macro) {
        fprintf(fp, "#pragma pop_macro(\"%s\")\n", vs);
    }
    词法分析_调试_标识符 = 0;
}

static void 词法_调试_内置(知心状态机 *状态机1)
{
    int v;
    for (v = 符_识别; v < 单词_识别号; ++v)
        宏定义_打印(状态机1, v);
}

/* 在标记a和b之间添加一个空格，以避免不必要的文本粘贴 */
static int 词法_添加_空格(int a, int b)
{
    return 'E' == a ? '+' == b || '-' == b
        : '+' == a ? 双符号_自加1 == b || '+' == b
        : '-' == a ? 双符号_自减1 == b || '-' == b
        : a >= 符_识别 ? b >= 符_识别
	: a == 常量_预处理编号 ? b >= 符_识别
        : 0;
}

/* 也许像0x1e这样的十六进制 */
static int 词法_检查_he0xE(int t, const char *p)
{
    if (t == 常量_预处理编号 && toup(strchr(p, 0)[-1]) == 'E')
        return 'E';
    return t;
}

/* 预处理当前文件 */
静态_函数 int 预处理_源文件(知心状态机 *状态机1)
{
    缓冲文件 **iptr;
    int 标识符_seen, spcs, level;
    const char *p;
    char white[400];

    解析_标记 = 解析_标记_预处理 | (解析_标记 & 解析_标记_汇编_文件) | 解析_标记_换行符 | 解析_标记_空间 | 解析_标记_接受_转义;
    if (状态机1->P标号 == LINE_MACRO_OUTPUT_FORMAT_P10)
        解析_标记 |= 解析_标记_标识符_数字, 状态机1->P标号 = 1;

#ifdef PP_BENCH
    /* 用于PP基准 */
    do 带有宏替换的下个标记(); while (单词编码 != 符_文件结尾);
    return 0;
#endif

    if (状态机1->DX标号 & 1) {
        词法_调试_内置(状态机1);
        状态机1->DX标号 &= ~1;
    }

    标识符_seen = 符_换行, spcs = 0, level = 0;
    if (file->prev)
        词法_行(状态机1, file->prev, level++);
    词法_行(状态机1, file, level);
    for (;;) {
        iptr = 状态机1->包含_堆_ptr;
        带有宏替换的下个标记();
        if (单词编码 == 符_文件结尾)
            break;

        level = 状态机1->包含_堆_ptr - iptr;
        if (level) {
            if (level > 0)
                词法_行(状态机1, *iptr, 0);
            词法_行(状态机1, file, level);
        }
        if (状态机1->DX标号 & 7) {
            词法_调试_宏定义(状态机1);
            if (状态机1->DX标号 & 4)
                continue;
        }

        if (是_空格(单词编码)) {
            if (spcs < sizeof white - 1)
                white[spcs++] = 单词编码;
            continue;
        } else if (单词编码 == 符_换行) {
            spcs = 0;
            if (标识符_seen == 符_换行)
                continue;
            ++file->line_ref;
        } else if (标识符_seen == 符_换行) {
            词法_行(状态机1, file, 0);
        } else if (spcs == 0 && 词法_添加_空格(标识符_seen, 单词编码)) {
            white[spcs++] = ' ';
        }

        white[spcs] = 0, fputs(white, 状态机1->预处理输出文件), spcs = 0;
        fputs(p = 取_单词字符串(单词编码, &单词值), 状态机1->预处理输出文件);
        标识符_seen = 词法_检查_he0xE(单词编码, p);
    }
    return 0;
}

/* ------------------------------------------------------------------------- */
