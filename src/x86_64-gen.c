/*
 *  x86-64 code generator for ZHI
 */

#ifdef TARGET_DEFS_ONLY

/* number of available registers */
#define 可用_寄存器数         25
#define NB_ASM_REGS     16
#define 配置_ZHI_汇编

/* a register can belong to several classes. The classes must be
   sorted from more general to more precise (see 将rc寄存器值存储在栈顶值中2() code which does
   assumptions on it). */
#define 寄存器类_整数     0x0001 /* generic integer register */
#define 寄存器类_浮点   0x0002 /* generic float register */
#define RC_RAX     0x0004
#define RC_RCX     0x0008
#define RC_RDX     0x0010
#define 寄存器类_堆栈0     0x0080 /* only for long double */
#define RC_R8      0x0100
#define RC_R9      0x0200
#define RC_R10     0x0400
#define RC_R11     0x0800
#define RC_XMM0    0x1000
#define RC_XMM1    0x2000
#define RC_XMM2    0x4000
#define RC_XMM3    0x8000
#define RC_XMM4    0x10000
#define RC_XMM5    0x20000
#define RC_XMM6    0x40000
#define RC_XMM7    0x80000
#define 寄存器类_返回整数寄存器    RC_RAX /* function return: integer register */
#define RC_IRE2    RC_RDX /* function return: second integer register */
#define 寄存器类_返回浮点寄存器    RC_XMM0 /* function return: float register */
#define RC_FRE2    RC_XMM1 /* function return: second float register */

/* pretty names for the registers */
enum {
    TREG_RAX = 0,
    TREG_RCX = 1,
    TREG_RDX = 2,
    TREG_RSP = 4,
    TREG_RSI = 6,
    TREG_RDI = 7,

    TREG_R8  = 8,
    TREG_R9  = 9,
    TREG_R10 = 10,
    TREG_R11 = 11,

    TREG_XMM0 = 16,
    TREG_XMM1 = 17,
    TREG_XMM2 = 18,
    TREG_XMM3 = 19,
    TREG_XMM4 = 20,
    TREG_XMM5 = 21,
    TREG_XMM6 = 22,
    TREG_XMM7 = 23,

    TREG_ST0 = 24,

    TREG_MEM = 0x20
};

#define REX_BASE(reg) (((reg) >> 3) & 1)
#define REG_VALUE(reg) ((reg) & 7)

/* return registers for function */
#define 寄存器_返回16位整数寄存器 TREG_RAX /* single word int return register */
#define 寄存器_返回32位整数寄存器 TREG_RDX /* second word return register (for long long) */
#define 寄存器_返回浮点寄存器 TREG_XMM0 /* float return register */
#define 寄存器_返回第二个浮点寄存器 TREG_XMM1 /* second float return register */

/* 定义是否必须以相反顺序求值函数参数 */
#define 相反顺序_函数_参数

/* pointer size, in bytes */
#define 指针_大小 8

/* long double size and alignment, in bytes */
#define 长双精度_大小  16
#define 长双精度_对齐 16
/* maximum alignment (for aligned attribute support) */
#define MAX_ALIGN     16

/* define if return values need to be extended explicitely
   at caller side (for interfacing with non-ZHI compilers) */
#define PROMOTE_RET
/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局_使用
#include "zhi.h"
#include <assert.h>

静态_外部 const int 寄存器_类数[可用_寄存器数] = {
    /* eax */ 寄存器类_整数 | RC_RAX,
    /* ecx */ 寄存器类_整数 | RC_RCX,
    /* edx */ 寄存器类_整数 | RC_RDX,
    0,
    0,
    0,
    0,
    0,
    RC_R8,
    RC_R9,
    RC_R10,
    RC_R11,
    0,
    0,
    0,
    0,
    /* xmm0 */ 寄存器类_浮点 | RC_XMM0,
    /* xmm1 */ 寄存器类_浮点 | RC_XMM1,
    /* xmm2 */ 寄存器类_浮点 | RC_XMM2,
    /* xmm3 */ 寄存器类_浮点 | RC_XMM3,
    /* xmm4 */ 寄存器类_浮点 | RC_XMM4,
    /* xmm5 */ 寄存器类_浮点 | RC_XMM5,
    /* xmm6 an xmm7 are included so 将rc寄存器值存储在栈顶值中() can be used on them,
       but they are not tagged with 寄存器类_浮点 because they are
       callee saved on Windows */
    RC_XMM6,
    RC_XMM7,
    /* st0 */ 寄存器类_堆栈0
};

static unsigned long func_sub_sp_offset;
static int func_ret_sub;

#if defined(配置_ZHI_边界检查)
static 目标地址_类型 func_bound_offset;
static unsigned long func_bound_ind;
static int func_bound_add_epilog;
#endif

#ifdef ZHI_TARGET_PE
static int func_scratch, func_alloca;
#endif

/* XXX: make it faster ? */
静态_函数 void 生成(int c)
{
    int ind1;
    if (不需要_代码生成)
        return;
    ind1 = 输出代码索引 + 1;
    if (ind1 > 当前_生成代码_段->data_allocated)
        节_重新分配内存(当前_生成代码_段, ind1);
    当前_生成代码_段->data[输出代码索引] = c;
    输出代码索引 = ind1;
}

静态_函数 void o(unsigned int c)
{
    while (c) {
        生成(c);
        c = c >> 8;
    }
}

静态_函数 void 生成_le16(int v)
{
    生成(v);
    生成(v >> 8);
}

静态_函数 void 生成_le32(int c)
{
    生成(c);
    生成(c >> 8);
    生成(c >> 16);
    生成(c >> 24);
}

静态_函数 void 生成_le64(int64_t c)
{
    生成(c);
    生成(c >> 8);
    生成(c >> 16);
    生成(c >> 24);
    生成(c >> 32);
    生成(c >> 40);
    生成(c >> 48);
    生成(c >> 56);
}

static void orex(int ll, int r, int r2, int b)
{
    if ((r & VT_值掩码) >= VT_VC常量)
        r = 0;
    if ((r2 & VT_值掩码) >= VT_VC常量)
        r2 = 0;
    if (ll || REX_BASE(r) || REX_BASE(r2))
        o(0x40 | REX_BASE(r) | (REX_BASE(r2) << 2) | (ll << 3));
    o(b);
}

/* output a symbol and patch all calls to it */
静态_函数 void 生成符号_地址(int t, int a)
{
    while (t) {
        unsigned char *ptr = 当前_生成代码_段->data + t;
        uint32_t n = 读32le(ptr); /* next value */
        写32le(ptr, a < 0 ? -a : a - t - 4);
        t = n;
    }
}

static int is64_type(int t)
{
    return ((t & VT_基本类型) == VT_指针 ||
            (t & VT_基本类型) == VT_函数 ||
            (t & VT_基本类型) == VT_长长整数);
}

/* instruction + 4 bytes data. Return the address of the data */
static int oad(int c, int s)
{
    int t;
    if (不需要_代码生成)
        return s;
    o(c);
    t = 输出代码索引;
    生成_le32(s);
    return t;
}

/* generate jmp to a label */
#define gjmp2(instr,lbl) oad(instr,lbl)

静态_函数 void 生成_addr32(int r, 符号 *sym, int c)
{
    if (r & VT_符号)
        添加新的重定位项(当前_生成代码_段, sym, 输出代码索引, R_X86_64_32S, c), c=0;
    生成_le32(c);
}

/* output constant with relocation if 'r & VT_符号' is true */
静态_函数 void 生成_地址64(int r, 符号 *sym, int64_t c)
{
    if (r & VT_符号)
        添加新的重定位项(当前_生成代码_段, sym, 输出代码索引, R_X86_64_64, c), c=0;
    生成_le64(c);
}

/* output constant with relocation if 'r & VT_符号' is true */
静态_函数 void 生成_addrpc32(int r, 符号 *sym, int c)
{
    if (r & VT_符号)
        添加新的重定位项(当前_生成代码_段, sym, 输出代码索引, R_X86_64_PC32, c-4), c=4;
    生成_le32(c-4);
}

/* output got address with relocation */
static void 生成_gotpcrel(int r, 符号 *sym, int c)
{
#ifdef ZHI_TARGET_PE
    错误_打印("内部错误: no GOT on PE: %s %x %x | %02x %02x %02x\n",
        取_单词字符串(sym->v, NULL), c, r,
        当前_生成代码_段->data[输出代码索引-3],
        当前_生成代码_段->data[输出代码索引-2],
        当前_生成代码_段->data[输出代码索引-1]
        );
#endif
    添加新的重定位项(当前_生成代码_段, sym, 输出代码索引, R_X86_64_GOTPCREL, -4);
    生成_le32(0);
    if (c) {
        /* we use add c, %xxx for displacement */
        orex(1, r, 0, 0x81);
        o(0xc0 + REG_VALUE(r));
        生成_le32(c);
    }
}

static void 生成_modrm_impl(int op_reg, int r, 符号 *sym, int c, int is_got)
{
    op_reg = REG_VALUE(op_reg) << 3;
    if ((r & VT_值掩码) == VT_VC常量) {
        /* constant memory reference */
	if (!(r & VT_符号)) {
	    /* Absolute memory reference */
	    o(0x04 | op_reg); /* [sib] | destreg */
	    oad(0x25, c);     /* disp32 */
	} else {
	    o(0x05 | op_reg); /* (%rip)+disp32 | destreg */
	    if (is_got) {
		生成_gotpcrel(r, sym, c);
	    } else {
		生成_addrpc32(r, sym, c);
	    }
	}
    } else if ((r & VT_值掩码) == VT_LOCAL) {
        /* currently, we use only ebp as base */
        if (c == (char)c) {
            /* short reference */
            o(0x45 | op_reg);
            生成(c);
        } else {
            oad(0x85 | op_reg, c);
        }
    } else if ((r & VT_值掩码) >= TREG_MEM) {
        if (c) {
            生成(0x80 | op_reg | REG_VALUE(r));
            生成_le32(c);
        } else {
            生成(0x00 | op_reg | REG_VALUE(r));
        }
    } else {
        生成(0x00 | op_reg | REG_VALUE(r));
    }
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void 生成_modrm(int op_reg, int r, 符号 *sym, int c)
{
    生成_modrm_impl(op_reg, r, sym, c, 0);
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void 生成_modrm64(int opcode, int op_reg, int r, 符号 *sym, int c)
{
    int is_got;
    is_got = (op_reg & TREG_MEM) && !(sym->type.t & VT_静态);
    orex(1, r, op_reg, opcode);
    生成_modrm_impl(op_reg, r, sym, c, is_got);
}


/* 加载 'r' from value 'sv' */
void 加载(int r, 堆栈值 *sv)
{
    int v, t, ft, fc, fr;
    堆栈值 v1;

#ifdef ZHI_TARGET_PE
    堆栈值 v2;
    sv = pe_获取导入(sv, &v2);
#endif

    fr = sv->r;
    ft = sv->type.t & ~VT_显式符号;
    fc = sv->c.i;
    if (fc != sv->c.i && (fr & VT_符号))
      错误_打印("64 bit addend in 加载");

    ft &= ~(VT_易变 | VT_常量);

#ifndef ZHI_TARGET_PE
    /* we use 间接的ect access via got */
    if ((fr & VT_值掩码) == VT_VC常量 && (fr & VT_符号) &&
        (fr & VT_LVAL) && !(sv->sym->type.t & VT_静态)) {
        /* use the result register as a temporal register */
        int tr = r | TREG_MEM;
        if (是_浮点型(ft)) {
            /* we cannot use float registers as a temporal register */
            tr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数) | TREG_MEM;
        }
        生成_modrm64(0x8b, tr, fr, sv->sym, 0);

        /* 加载 from the temporal register */
        fr = tr | VT_LVAL;
    }
#endif

    v = fr & VT_值掩码;
    if (fr & VT_LVAL) {
        int b, ll;
        if (v == VT_LLOCAL) {
            v1.type.t = VT_指针;
            v1.r = VT_LOCAL | VT_LVAL;
            v1.c.i = fc;
            fr = r;
            if (!(寄存器_类数[fr] & (寄存器类_整数|RC_R11)))
                fr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            加载(fr, &v1);
        }
	if (fc != sv->c.i) {
	    /* If the addends doesn't fit into a 32bit signed
	       we must use a 64bit move.  We've checked above
	       that this doesn't have a sym associated.  */
	    v1.type.t = VT_长长整数;
	    v1.r = VT_VC常量;
	    v1.c.i = sv->c.i;
	    fr = r;
	    if (!(寄存器_类数[fr] & (寄存器类_整数|RC_R11)))
	        fr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
	    加载(fr, &v1);
	    fc = 0;
	}
        ll = 0;
	/* Like GCC we can 加载 from small enough properly sized
	   structs and unions as well.
	   XXX maybe move to generic operand handling, but should
	   occur only with asm, so 汇编处理.c might also be a better place */
	if ((ft & VT_基本类型) == VT_结构体) {
	    int align;
	    switch (类型_大小(&sv->type, &align)) {
		case 1: ft = VT_字节; break;
		case 2: ft = VT_短整数; break;
		case 4: ft = VT_整数; break;
		case 8: ft = VT_长长整数; break;
		default:
		    错误_打印("invalid aggregate type for register 加载");
		    break;
	    }
	}
        if ((ft & VT_基本类型) == VT_浮点) {
            b = 0x6e0f66;
            r = REG_VALUE(r); /* movd */
        } else if ((ft & VT_基本类型) == VT_双精度) {
            b = 0x7e0ff3; /* movq */
            r = REG_VALUE(r);
        } else if ((ft & VT_基本类型) == VT_长双精度) {
            b = 0xdb, r = 5; /* fldt */
        } else if ((ft & VT_类型) == VT_字节 || (ft & VT_类型) == VT_逻辑) {
            b = 0xbe0f;   /* movsbl */
        } else if ((ft & VT_类型) == (VT_字节 | VT_无符号)) {
            b = 0xb60f;   /* movzbl */
        } else if ((ft & VT_类型) == VT_短整数) {
            b = 0xbf0f;   /* movswl */
        } else if ((ft & VT_类型) == (VT_短整数 | VT_无符号)) {
            b = 0xb70f;   /* movzwl */
        } else {
            assert(((ft & VT_基本类型) == VT_整数)
                   || ((ft & VT_基本类型) == VT_长长整数)
                   || ((ft & VT_基本类型) == VT_指针)
                   || ((ft & VT_基本类型) == VT_函数)
                );
            ll = is64_type(ft);
            b = 0x8b;
        }
        if (ll) {
            生成_modrm64(b, r, fr, sv->sym, fc);
        } else {
            orex(ll, fr, r, b);
            生成_modrm(r, fr, sv->sym, fc);
        }
    } else {
        if (v == VT_VC常量) {
            if (fr & VT_符号) {
#ifdef ZHI_TARGET_PE
                orex(1,0,r,0x8d);
                o(0x05 + REG_VALUE(r) * 8); /* lea xx(%rip), r */
                生成_addrpc32(fr, sv->sym, fc);
#else
                if (sv->sym->type.t & VT_静态) {
                    orex(1,0,r,0x8d);
                    o(0x05 + REG_VALUE(r) * 8); /* lea xx(%rip), r */
                    生成_addrpc32(fr, sv->sym, fc);
                } else {
                    orex(1,0,r,0x8b);
                    o(0x05 + REG_VALUE(r) * 8); /* mov xx(%rip), r */
                    生成_gotpcrel(r, sv->sym, fc);
                }
#endif
            } else if (is64_type(ft)) {
                orex(1,r,0, 0xb8 + REG_VALUE(r)); /* mov $xx, r */
                生成_le64(sv->c.i);
            } else {
                orex(0,r,0, 0xb8 + REG_VALUE(r)); /* mov $xx, r */
                生成_le32(fc);
            }
        } else if (v == VT_LOCAL) {
            orex(1,0,r,0x8d); /* lea xxx(%ebp), r */
            生成_modrm(r, VT_LOCAL, sv->sym, fc);
        } else if (v == VT_CMP) {
	    if (fc & 0x100)
	      {
                v = 栈顶值->cmp_r;
                fc &= ~0x100;
	        /* This was a float compare.  If the parity bit is
		   set the result was unordered, meaning false for everything
		   except 双符号_不等于, and true for 双符号_不等于.  */
                orex(0, r, 0, 0xb0 + REG_VALUE(r)); /* mov $0/1,%al */
                生成(v ^ fc ^ (v == 双符号_不等于));
                o(0x037a + (REX_BASE(r) << 8));
              }
            orex(0,r,0, 0x0f); /* setxx %br */
            o(fc);
            o(0xc0 + REG_VALUE(r));
            orex(0,r,0, 0x0f);
            o(0xc0b6 + REG_VALUE(r) * 0x900); /* movzbl %al, %eax */
        } else if (v == VT_JMP || v == VT_JMPI) {
            t = v & 1;
            orex(0,r,0,0);
            oad(0xb8 + REG_VALUE(r), t); /* mov $1, r */
            o(0x05eb + (REX_BASE(r) << 8)); /* jmp after */
            生成符号(fc);
            orex(0,r,0,0);
            oad(0xb8 + REG_VALUE(r), t ^ 1); /* mov $0, r */
        } else if (v != r) {
            if ((r >= TREG_XMM0) && (r <= TREG_XMM7)) {
                if (v == TREG_ST0) {
                    /* 生成_浮点转换为另一种浮点(VT_双精度); */
                    o(0xf0245cdd); /* fstpl -0x10(%rsp) */
                    /* movsd -0x10(%rsp),%xmmN */
                    o(0x100ff2);
                    o(0x44 + REG_VALUE(r)*8); /* %xmmN */
                    o(0xf024);
                } else {
                    assert((v >= TREG_XMM0) && (v <= TREG_XMM7));
                    if ((ft & VT_基本类型) == VT_浮点) {
                        o(0x100ff3);
                    } else {
                        assert((ft & VT_基本类型) == VT_双精度);
                        o(0x100ff2);
                    }
                    o(0xc0 + REG_VALUE(v) + REG_VALUE(r)*8);
                }
            } else if (r == TREG_ST0) {
                assert((v >= TREG_XMM0) && (v <= TREG_XMM7));
                /* 生成_浮点转换为另一种浮点(VT_长双精度); */
                /* movsd %xmmN,-0x10(%rsp) */
                o(0x110ff2);
                o(0x44 + REG_VALUE(r)*8); /* %xmmN */
                o(0xf024);
                o(0xf02444dd); /* fldl -0x10(%rsp) */
            } else {
                orex(is64_type(ft), r, v, 0x89);
                o(0xc0 + REG_VALUE(r) + REG_VALUE(v) * 8); /* mov v, r */
            }
        }
    }
}

/* 存储 register 'r' in lvalue 'v' */
void 存储(int r, 堆栈值 *v)
{
    int fr, bt, ft, fc;
    int op64 = 0;
    /* 存储 the REX prefix in this variable when PIC is enabled */
    int pic = 0;

#ifdef ZHI_TARGET_PE
    堆栈值 v2;
    v = pe_获取导入(v, &v2);
#endif

    fr = v->r & VT_值掩码;
    ft = v->type.t;
    fc = v->c.i;
    if (fc != v->c.i && (fr & VT_符号))
      错误_打印("64 bit addend in 存储");
    ft &= ~(VT_易变 | VT_常量);
    bt = ft & VT_基本类型;

#ifndef ZHI_TARGET_PE
    /* we need to access the variable via got */
    if (fr == VT_VC常量 && (v->r & VT_符号)) {
        /* mov xx(%rip), %r11 */
        o(0x1d8b4c);
        生成_gotpcrel(TREG_R11, v->sym, v->c.i);
        pic = is64_type(bt) ? 0x49 : 0x41;
    }
#endif

    /* XXX: incorrect if float reg to reg */
    if (bt == VT_浮点) {
        o(0x66);
        o(pic);
        o(0x7e0f); /* movd */
        r = REG_VALUE(r);
    } else if (bt == VT_双精度) {
        o(0x66);
        o(pic);
        o(0xd60f); /* movq */
        r = REG_VALUE(r);
    } else if (bt == VT_长双精度) {
        o(0xc0d9); /* fld %st(0) */
        o(pic);
        o(0xdb); /* fstpt */
        r = 7;
    } else {
        if (bt == VT_短整数)
            o(0x66);
        o(pic);
        if (bt == VT_字节 || bt == VT_逻辑)
            orex(0, 0, r, 0x88);
        else if (is64_type(bt))
            op64 = 0x89;
        else
            orex(0, 0, r, 0x89);
    }
    if (pic) {
        /* xxx r, (%r11) where xxx is mov, movq, fld, or etc */
        if (op64)
            o(op64);
        o(3 + (r << 3));
    } else if (op64) {
        if (fr == VT_VC常量 || fr == VT_LOCAL || (v->r & VT_LVAL)) {
            生成_modrm64(op64, r, v->r, v->sym, fc);
        } else if (fr != r) {
            orex(1, fr, r, op64);
            o(0xc0 + fr + r * 8); /* mov r, fr */
        }
    } else {
        if (fr == VT_VC常量 || fr == VT_LOCAL || (v->r & VT_LVAL)) {
            生成_modrm(r, v->r, v->sym, fc);
        } else if (fr != r) {
            o(0xc0 + fr + r * 8); /* mov r, fr */
        }
    }
}

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
    int r;
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量 &&
	((栈顶值->r & VT_符号) && (栈顶值->c.i-4) == (int)(栈顶值->c.i-4))) {
        /* constant 先解析当前模块符号 case -> simple relocation */
#ifdef ZHI_TARGET_PE
        添加新的重定位项(当前_生成代码_段, 栈顶值->sym, 输出代码索引 + 1, R_X86_64_PC32, (int)(栈顶值->c.i-4));
#else
        添加新的重定位项(当前_生成代码_段, 栈顶值->sym, 输出代码索引 + 1, R_X86_64_PLT32, (int)(栈顶值->c.i-4));
#endif
        oad(0xe8 + is_jmp, 0); /* call/jmp im */
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器 &&
            (栈顶值->sym->v == 符_alloca ||
             栈顶值->sym->v == 符_setjmp ||
             栈顶值->sym->v == 符__setjmp
#ifndef ZHI_TARGET_PE
             || 栈顶值->sym->v == 符_sigsetjmp
             || 栈顶值->sym->v == 符___sigsetjmp
#endif
            ))
            func_bound_add_epilog = 1;
#endif
    } else {
        /* otherwise, 间接的ect call */
        r = TREG_R11;
        加载(r, 栈顶值);
        o(0x41); /* REX */
        o(0xff); /* call/jmp *r */
        o(0xd0 + REG_VALUE(r) + (is_jmp << 4));
    }
}

#if defined(配置_ZHI_边界检查)

static void 生成_bounds_call(int v)
{
    符号 *sym = 外部_全局_符号(v, &函数_旧_类型);
    oad(0xe8, 0);
#ifdef ZHI_TARGET_PE
    添加新的重定位项(当前_生成代码_段, sym, 输出代码索引-4, R_X86_64_PC32, -4);
#else
    添加新的重定位项(当前_生成代码_段, sym, 输出代码索引-4, R_X86_64_PLT32, -4);
#endif
}

/* generate a bounded pointer addition */
静态_函数 void 生成_边界的_ptr_添加(void)
{
    推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_ptr_add);
    vrott(3);
    具体地址函数_调用(2);
    压入整数常量(0);
    /* returned pointer is in rax */
    栈顶值->r = TREG_RAX | VT_有界的;
    if (不需要_代码生成)
        return;
    /* relocation offset of the bounding function call point */
    栈顶值->c.i = (当前_生成代码_段->重定位->数据_偏移 - sizeof(ElfW(Rela)));
}

/* patch pointer addition in 栈顶值 so that pointer dereferencing is
   also tested */
静态_函数 void 生成_边界的_ptr_取消引用(void)
{
    目标地址_类型 func;
    int size, align;
    ElfW(Rela) *rel;
    符号 *sym;

    if (不需要_代码生成)
        return;

    size = 类型_大小(&栈顶值->type, &align);
    switch(size) {
    case  1: func = 符___bound_ptr_间接的1; break;
    case  2: func = 符___bound_ptr_间接的2; break;
    case  4: func = 符___bound_ptr_间接的4; break;
    case  8: func = 符___bound_ptr_间接的8; break;
    case 12: func = 符___bound_ptr_间接的12; break;
    case 16: func = 符___bound_ptr_间接的16; break;
    default:
        /* may happen with struct member access */
        return;
        //错误_打印("unhandled size when dereferencing bounded pointer");
        //func = 0;
        //break;
    }
    sym = 外部_全局_符号(func, &函数_旧_类型);
    if (!sym->c)
        更新_外部_符号(sym, NULL, 0, 0);
    /* patch relocation */
    /* XXX: find a better solution ? */
    rel = (ElfW(Rela) *)(当前_生成代码_段->重定位->data + 栈顶值->c.i);
    rel->r_info = ELF64_R_INFO(sym->c, ELF64_R_TYPE(rel->r_info));
}

#ifdef ZHI_TARGET_PE
# define TREG_快速调用_1 TREG_RCX
#else
# define TREG_快速调用_1 TREG_RDI
#endif

static void 生成_bounds_prolog(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 本地边界_部分->数据_偏移;
    func_bound_ind = 输出代码索引;
    func_bound_add_epilog = 0;
    o(0xb848 + TREG_快速调用_1 * 0x100); /*lbound section pointer */
    生成_le64 (0);
    oad(0xb8, 0); /* call to function */
}

static void 生成_bounds_epilog(void)
{
    目标地址_类型 saved_ind;
    目标地址_类型 *bounds_ptr;
    符号 *sym_data;
    int offset_modified = func_bound_offset != 本地边界_部分->数据_偏移;

    if (!offset_modified && !func_bound_add_epilog)
        return;

    /* add end of table info */
    bounds_ptr = 段_ptr_添加(本地边界_部分, sizeof(目标地址_类型));
    *bounds_ptr = 0;

    sym_data = 返回_指向节的_静态符号(&字符_指针_类型, 本地边界_部分, 
                           func_bound_offset, 本地边界_部分->数据_偏移);

    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 输出代码索引;
        输出代码索引 = func_bound_ind;
        添加新的重定位项(当前_生成代码_段, sym_data, 输出代码索引 + 2, R_X86_64_64, 0);
        输出代码索引 = 输出代码索引 + 10;
        生成_bounds_call(符___bound_local_new);
        输出代码索引 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0x5250); /* save returned value, if any */
    添加新的重定位项(当前_生成代码_段, sym_data, 输出代码索引 + 2, R_X86_64_64, 0);
    o(0xb848 + TREG_快速调用_1 * 0x100); /* mov xxx, %rcx/di */
    生成_le64 (0);
    生成_bounds_call(符___bound_local_delete);
    o(0x585a); /* restore returned value, if any */
}
#endif

#ifdef ZHI_TARGET_PE

#define REGN 4
static const uint8_t arg_regs[REGN] = {
    TREG_RCX, TREG_RDX, TREG_R8, TREG_R9
};

/* Prepare arguments in R10 and R11 rather than RCX and RDX
   because 将rc寄存器值存储在栈顶值中() will not ever use these */
static int arg_prepare_reg(int idx) {
  if (idx == 0 || idx == 1)
      /* idx=0: r10, idx=1: r11 */
      return idx + 10;
  else
      return arg_regs[idx];
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */

static void 生成_offs_sp(int b, int r, int d)
{
    orex(1,0,r & 0x100 ? 0 : r, b);
    if (d == (char)d) {
        o(0x2444 | (REG_VALUE(r) << 3));
        生成(d);
    } else {
        o(0x2484 | (REG_VALUE(r) << 3));
        生成_le32(d);
    }
}

static int using_regs(int size)
{
    return !(size > 8 || (size & (size - 1)));
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *ret_align, int *regsize)
{
    int size, align;
    *ret_align = 1; // Never have to re-align return values for x86-64
    *regsize = 8;
    size = 类型_大小(vt, &align);
    if (!using_regs(size))
        return 0;
    if (size == 8)
        ret->t = VT_长长整数;
    else if (size == 4)
        ret->t = VT_整数;
    else if (size == 2)
        ret->t = VT_短整数;
    else
        ret->t = VT_字节;
    ret->ref = NULL;
    return 1;
}

static int is_sse_float(int t) {
    int bt;
    bt = t & VT_基本类型;
    return bt == VT_双精度 || bt == VT_浮点;
}

static int gfunc_arg_size(C类型 *type) {
    int align;
    if (type->t & (VT_数组|VT_位域))
        return 8;
    return 类型_大小(type, &align);
}

void 具体地址函数_调用(int 数量_args)
{
    int size, r, args_size, i, d, bt, struct_size;
    int arg;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成左值边界代码_函数参数加载到寄存器(数量_args);
#endif

    args_size = (数量_args < REGN ? REGN : 数量_args) * 指针_大小;
    arg = 数量_args;

    /* for struct arguments, we need to call memcpy and the function
       call breaks register passing arguments we are preparing.
       So, we process arguments which will be passed by stack first. */
    struct_size = args_size;
    for(i = 0; i < 数量_args; i++) {
        堆栈值 *sv;
        
        --arg;
        sv = &栈顶值[-i];
        bt = (sv->type.t & VT_基本类型);
        size = gfunc_arg_size(&sv->type);

        if (using_regs(size))
            continue; /* arguments smaller than 8 bytes passed in registers or on stack */

        if (bt == VT_结构体) {
            /* align to stack align size */
            size = (size + 15) & ~15;
            /* generate structure 存储 */
            r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            生成_offs_sp(0x8d, r, struct_size);
            struct_size += size;

            /* generate memcpy call */
            vset(&sv->type, r | VT_LVAL, 0);
            vpushv(sv);
            将栈顶值_存储在堆栈左值();
            --栈顶值;
        } else if (bt == VT_长双精度) {
            将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
            生成_offs_sp(0xdb, 0x107, struct_size);
            struct_size += 16;
        }
    }

    if (func_scratch < struct_size)
        func_scratch = struct_size;

    arg = 数量_args;
    struct_size = args_size;

    for(i = 0; i < 数量_args; i++) {
        --arg;
        bt = (栈顶值->type.t & VT_基本类型);

        size = gfunc_arg_size(&栈顶值->type);
        if (!using_regs(size)) {
            /* align to stack align size */
            size = (size + 15) & ~15;
            if (arg >= REGN) {
                d = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
                生成_offs_sp(0x8d, d, struct_size);
                生成_offs_sp(0x89, d, arg*8);
            } else {
                d = arg_prepare_reg(arg);
                生成_offs_sp(0x8d, d, struct_size);
            }
            struct_size += size;
        } else {
            if (is_sse_float(栈顶值->type.t)) {
		if (zhi_状态->支持mno和sse)
		  错误_打印("SSE disabled");
                if (arg >= REGN) {
                    将rc寄存器值存储在栈顶值中(RC_XMM0);
                    /* movq %xmm0, j*8(%rsp) */
                    生成_offs_sp(0xd60f66, 0x100, arg*8);
                } else {
                    /* Load directly to xmmN register */
                    将rc寄存器值存储在栈顶值中(RC_XMM0 << arg);
                    d = arg_prepare_reg(arg);
                    /* mov %xmmN, %rxx */
                    o(0x66);
                    orex(1,d,0, 0x7e0f);
                    o(0xc0 + arg*8 + REG_VALUE(d));
                }
            } else {
                if (bt == VT_结构体) {
                    栈顶值->type.ref = NULL;
                    栈顶值->type.t = size > 4 ? VT_长长整数 : size > 2 ? VT_整数
                        : size > 1 ? VT_短整数 : VT_字节;
                }
                
                r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
                if (arg >= REGN) {
                    生成_offs_sp(0x89, r, arg*8);
                } else {
                    d = arg_prepare_reg(arg);
                    orex(1,d,r,0x89); /* mov */
                    o(0xc0 + REG_VALUE(r) * 8 + REG_VALUE(d));
                }
            }
        }
        栈顶值--;
    }
    保存_寄存器最多n个堆栈条目(0);
    /* Copy R10 and R11 into RCX and RDX, respectively */
    if (数量_args > 0) {
        o(0xd1894c); /* mov %r10, %rcx */
        if (数量_args > 1) {
            o(0xda894c); /* mov %r11, %rdx */
        }
    }
    
    gcall_or_jmp(0);

    if ((栈顶值->r & VT_符号) && 栈顶值->sym->v == 符_alloca) {
        /* need to add the "func_scratch" area after alloca */
        o(0x48); func_alloca = oad(0x05, func_alloca); /* add $NN, %rax */
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器)
            生成_bounds_call(符___bound_alloca_nr); /* new region */
#endif
    }
    栈顶值--;
}


#define 函数_PROLOG_SIZE 11

/* generate function prolog of type 't' */
void 生成函数_序言(符号 *func_sym)
{
    C类型 *func_type = &func_sym->type;
    int addr, reg_param_index, bt, size;
    符号 *sym;
    C类型 *type;

    func_ret_sub = 0;
    func_scratch = 32;
    func_alloca = 0;
    局部变量索引 = 0;

    addr = 指针_大小 * 2;
    输出代码索引 += 函数_PROLOG_SIZE;
    func_sub_sp_offset = 输出代码索引;
    reg_param_index = 0;

    sym = func_type->ref;

    /* if the function returns a structure, then add an
       implicit pointer parameter */
    size = gfunc_arg_size(&当前函数_返回类型);
    if (!using_regs(size)) {
        生成_modrm64(0x89, arg_regs[reg_param_index], VT_LOCAL, NULL, addr);
        函数_vc = addr;
        reg_param_index++;
        addr += 8;
    }

    /* define parameters */
    while ((sym = sym->next) != NULL) {
        type = &sym->type;
        bt = type->t & VT_基本类型;
        size = gfunc_arg_size(type);
        if (!using_regs(size)) {
            if (reg_param_index < REGN) {
                生成_modrm64(0x89, arg_regs[reg_param_index], VT_LOCAL, NULL, addr);
            }
            符号_压入栈(sym->v & ~符号_字段, type,
                     VT_LLOCAL | VT_LVAL, addr);
        } else {
            if (reg_param_index < REGN) {
                /* save arguments passed by register */
                if ((bt == VT_浮点) || (bt == VT_双精度)) {
		    if (zhi_状态->支持mno和sse)
		      错误_打印("SSE disabled");
                    o(0xd60f66); /* movq */
                    生成_modrm(reg_param_index, VT_LOCAL, NULL, addr);
                } else {
                    生成_modrm64(0x89, arg_regs[reg_param_index], VT_LOCAL, NULL, addr);
                }
            }
            符号_压入栈(sym->v & ~符号_字段, type,
		     VT_LOCAL | VT_LVAL, addr);
        }
        addr += 8;
        reg_param_index++;
    }

    while (reg_param_index < REGN) {
        if (当前函数_可变参数) {
            生成_modrm64(0x89, arg_regs[reg_param_index], VT_LOCAL, NULL, addr);
            addr += 8;
        }
        reg_param_index++;
    }
#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_prolog();
#endif
}

/* generate function epilog */
void 生成函数_结尾(void)
{
    int v, saved_ind;

    /* align local size to word & save local variables */
    func_scratch = (func_scratch + 15) & -16;
    局部变量索引 = (局部变量索引 & -16) - func_scratch;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_epilog();
#endif

    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        生成(func_ret_sub);
        生成(func_ret_sub >> 8);
    }

    saved_ind = 输出代码索引;
    输出代码索引 = func_sub_sp_offset - 函数_PROLOG_SIZE;
    v = -局部变量索引;

    if (v >= 4096) {
        符号 *sym = 外部_全局_符号(符___chkstk, &函数_旧_类型);
        oad(0xb8, v); /* mov stacksize, %eax */
        oad(0xe8, 0); /* call __chkstk, (does the stackframe too) */
        添加新的重定位项(当前_生成代码_段, sym, 输出代码索引-4, R_X86_64_PC32, -4);
        o(0x90); /* fill for 函数_PROLOG_SIZE = 11 bytes */
    } else {
        o(0xe5894855);  /* push %rbp, mov %rsp, %rbp */
        o(0xec8148);  /* sub rsp, stacksize */
        生成_le32(v);
    }

    /* add the "func_scratch" area after each alloca seen */
    生成符号_地址(func_alloca, -func_scratch);

    当前_生成代码_段->数据_偏移 = saved_ind;
    pe_添加_uwwind_数据(输出代码索引, saved_ind, v);
    输出代码索引 = 当前_生成代码_段->数据_偏移;
}

#else

static void gadd_sp(int val)
{
    if (val == (char)val) {
        o(0xc48348);
        生成(val);
    } else {
        oad(0xc48148, val); /* add $xxx, %rsp */
    }
}

typedef enum X86_64_Mode {
  x86_64_mode_none,
  x86_64_mode_memory,
  x86_64_mode_integer,
  x86_64_mode_sse,
  x86_64_mode_x87
} X86_64_Mode;

static X86_64_Mode classify_x86_64_merge(X86_64_Mode a, X86_64_Mode b)
{
    if (a == b)
        return a;
    else if (a == x86_64_mode_none)
        return b;
    else if (b == x86_64_mode_none)
        return a;
    else if ((a == x86_64_mode_memory) || (b == x86_64_mode_memory))
        return x86_64_mode_memory;
    else if ((a == x86_64_mode_integer) || (b == x86_64_mode_integer))
        return x86_64_mode_integer;
    else if ((a == x86_64_mode_x87) || (b == x86_64_mode_x87))
        return x86_64_mode_memory;
    else
        return x86_64_mode_sse;
}

static X86_64_Mode classify_x86_64_inner(C类型 *ty)
{
    X86_64_Mode mode;
    符号 *f;
    
    switch (ty->t & VT_基本类型) {
    case VT_无类型: return x86_64_mode_none;
    
    case VT_整数:
    case VT_字节:
    case VT_短整数:
    case VT_长长整数:
    case VT_逻辑:
    case VT_指针:
    case VT_函数:
        return x86_64_mode_integer;
    
    case VT_浮点:
    case VT_双精度: return x86_64_mode_sse;
    
    case VT_长双精度: return x86_64_mode_x87;
      
    case VT_结构体:
        f = ty->ref;

        mode = x86_64_mode_none;
        for (f = f->next; f; f = f->next)
            mode = classify_x86_64_merge(mode, classify_x86_64_inner(&f->type));
        
        return mode;
    }
    assert(0);
    return 0;
}

static X86_64_Mode classify_x86_64_arg(C类型 *ty, C类型 *ret, int *psize, int *palign, int *reg_count)
{
    X86_64_Mode mode;
    int size, align, ret_t = 0;
    
    if (ty->t & (VT_位域|VT_数组)) {
        *psize = 8;
        *palign = 8;
        *reg_count = 1;
        ret_t = ty->t;
        mode = x86_64_mode_integer;
    } else {
        size = 类型_大小(ty, &align);
        *psize = (size + 7) & ~7;
        *palign = (align + 7) & ~7;
    
        if (size > 16) {
            mode = x86_64_mode_memory;
        } else {
            mode = classify_x86_64_inner(ty);
            switch (mode) {
            case x86_64_mode_integer:
                if (size > 8) {
                    *reg_count = 2;
                    ret_t = VT_128位整数;
                } else {
                    *reg_count = 1;
                    if (size > 4)
                        ret_t = VT_长长整数;
                    else if (size > 2)
                        ret_t = VT_整数;
                    else if (size > 1)
                        ret_t = VT_短整数;
                    else
                        ret_t = VT_字节;
                    if ((ty->t & VT_基本类型) == VT_结构体 || (ty->t & VT_无符号))
                        ret_t |= VT_无符号;
                }
                break;
                
            case x86_64_mode_x87:
                *reg_count = 1;
                ret_t = VT_长双精度;
                break;

            case x86_64_mode_sse:
                if (size > 8) {
                    *reg_count = 2;
                    ret_t = VT_128位浮点;
                } else {
                    *reg_count = 1;
                    ret_t = (size > 4) ? VT_双精度 : VT_浮点;
                }
                break;
            default: break; /* nothing to be done for x86_64_mode_memory and x86_64_mode_none*/
            }
        }
    }
    
    if (ret) {
        ret->ref = NULL;
        ret->t = ret_t;
    }
    
    return mode;
}

静态_函数 int 分类_x86_64_va_arg(C类型 *ty)
{
    /* This definition must be synced with stdarg.h */
    enum __va_arg_type {
        __va_生成_reg, __va_float_reg, __va_stack
    };
    int size, align, reg_count;
    X86_64_Mode mode = classify_x86_64_arg(ty, NULL, &size, &align, &reg_count);
    switch (mode) {
    default: return __va_stack;
    case x86_64_mode_integer: return __va_生成_reg;
    case x86_64_mode_sse: return __va_float_reg;
    }
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *ret_align, int *regsize)
{
    int size, align, reg_count;
    *ret_align = 1; // Never have to re-align return values for x86-64
    *regsize = 8;
    return (classify_x86_64_arg(vt, ret, &size, &align, &reg_count) != x86_64_mode_memory);
}

#define REGN 6
static const uint8_t arg_regs[REGN] = {
    TREG_RDI, TREG_RSI, TREG_RDX, TREG_RCX, TREG_R8, TREG_R9
};

static int arg_prepare_reg(int idx) {
  if (idx == 2 || idx == 3)
      /* idx=2: r10, idx=3: r11 */
      return idx + 8;
  else
      return arg_regs[idx];
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */
void 具体地址函数_调用(int 数量_args)
{
    X86_64_Mode mode;
    C类型 type;
    int size, align, r, args_size, stack_adjust, i, reg_count;
    int 数量_reg_args = 0;
    int 数量_sse_args = 0;
    int sse_reg, 生成_reg;
    char _onstack[数量_args ? 数量_args : 1], *onstack = _onstack;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成左值边界代码_函数参数加载到寄存器(数量_args);
#endif

    /* calculate the number of integer/float register arguments, remember
       arguments to be passed via stack (in onstack[]), and also remember
       if we have to align the stack pointer to 16 (onstack[i] == 2).  Needs
       to be done in a left-to-right pass over arguments.  */
    stack_adjust = 0;
    for(i = 数量_args - 1; i >= 0; i--) {
        mode = classify_x86_64_arg(&栈顶值[-i].type, NULL, &size, &align, &reg_count);
        if (mode == x86_64_mode_sse && 数量_sse_args + reg_count <= 8) {
            数量_sse_args += reg_count;
	    onstack[i] = 0;
	} else if (mode == x86_64_mode_integer && 数量_reg_args + reg_count <= REGN) {
            数量_reg_args += reg_count;
	    onstack[i] = 0;
	} else if (mode == x86_64_mode_none) {
	    onstack[i] = 0;
	} else {
	    if (align == 16 && (stack_adjust &= 15)) {
		onstack[i] = 2;
		stack_adjust = 0;
	    } else
	      onstack[i] = 1;
	    stack_adjust += size;
	}
    }

    if (数量_sse_args && zhi_状态->支持mno和sse)
      错误_打印("SSE disabled but floating point arguments passed");

    /* fetch cpu flag before generating any code */
    if ((栈顶值->r & VT_值掩码) == VT_CMP)
      将rc寄存器值存储在栈顶值中(寄存器类_整数);

    /* for struct arguments, we need to call memcpy and the function
       call breaks register passing arguments we are preparing.
       So, we process arguments which will be passed by stack first. */
    生成_reg = 数量_reg_args;
    sse_reg = 数量_sse_args;
    args_size = 0;
    stack_adjust &= 15;
    for (i = 0; i < 数量_args;) {
	mode = classify_x86_64_arg(&栈顶值[-i].type, NULL, &size, &align, &reg_count);
	if (!onstack[i]) {
	    ++i;
	    continue;
	}
        /* Possibly adjust stack to align SSE boundary.  We're processing
	   args from right to left while allocating happens left to right
	   (stack grows down), so the adjustment needs to happen _after_
	   an argument that requires it.  */
        if (stack_adjust) {
	    o(0x50); /* push %rax; aka sub $8,%rsp */
            args_size += 8;
	    stack_adjust = 0;
        }
	if (onstack[i] == 2)
	  stack_adjust = 1;

	vrotb(i+1);

	switch (栈顶值->type.t & VT_基本类型) {
	    case VT_结构体:
		/* allocate the necessary size on stack */
		o(0x48);
		oad(0xec81, size); /* sub $xxx, %rsp */
		/* generate structure 存储 */
		r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
		orex(1, r, 0, 0x89); /* mov %rsp, r */
		o(0xe0 + REG_VALUE(r));
		vset(&栈顶值->type, r | VT_LVAL, 0);
		vswap();
		将栈顶值_存储在堆栈左值();
		break;

	    case VT_长双精度:
                将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
                oad(0xec8148, size); /* sub $xxx, %rsp */
                o(0x7cdb); /* fstpt 0(%rsp) */
                生成(0x24);
                生成(0x00);
		break;

	    case VT_浮点:
	    case VT_双精度:
		assert(mode == x86_64_mode_sse);
		r = 将rc寄存器值存储在栈顶值中(寄存器类_浮点);
		o(0x50); /* push $rax */
		/* movq %xmmN, (%rsp) */
		o(0xd60f66);
		o(0x04 + REG_VALUE(r)*8);
		o(0x24);
		break;

	    default:
		assert(mode == x86_64_mode_integer);
		/* simple type */
		/* XXX: implicit cast ? */
		r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
		orex(0,r,0,0x50 + REG_VALUE(r)); /* push r */
		break;
	}
	args_size += size;

	弹出堆栈值();
	--数量_args;
	onstack++;
    }

    /* XXX This should be superfluous.  */
    保存_寄存器最多n个堆栈条目(0); /* save used temporary registers */

    /* then, we prepare register passing arguments.
       Note that we cannot set RDX and RCX in this loop because 将rc寄存器值存储在栈顶值中()
       may break these temporary registers. Let's use R10 and R11
       instead of them */
    assert(生成_reg <= REGN);
    assert(sse_reg <= 8);
    for(i = 0; i < 数量_args; i++) {
        mode = classify_x86_64_arg(&栈顶值->type, &type, &size, &align, &reg_count);
        /* Alter stack entry type so that 将rc寄存器值存储在栈顶值中() knows how to treat it */
        栈顶值->type = type;
        if (mode == x86_64_mode_sse) {
            if (reg_count == 2) {
                sse_reg -= 2;
                将rc寄存器值存储在栈顶值中(寄存器类_返回浮点寄存器); /* Use pair 加载 into xmm0 & xmm1 */
                if (sse_reg) { /* avoid redundant movaps %xmm0, %xmm0 */
                    /* movaps %xmm1, %xmmN */
                    o(0x280f);
                    o(0xc1 + ((sse_reg+1) << 3));
                    /* movaps %xmm0, %xmmN */
                    o(0x280f);
                    o(0xc0 + (sse_reg << 3));
                }
            } else {
                assert(reg_count == 1);
                --sse_reg;
                /* Load directly to register */
                将rc寄存器值存储在栈顶值中(RC_XMM0 << sse_reg);
            }
        } else if (mode == x86_64_mode_integer) {
            /* simple type */
            /* XXX: implicit cast ? */
            int d;
            生成_reg -= reg_count;
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            d = arg_prepare_reg(生成_reg);
            orex(1,d,r,0x89); /* mov */
            o(0xc0 + REG_VALUE(r) * 8 + REG_VALUE(d));
            if (reg_count == 2) {
                d = arg_prepare_reg(生成_reg+1);
                orex(1,d,栈顶值->r2,0x89); /* mov */
                o(0xc0 + REG_VALUE(栈顶值->r2) * 8 + REG_VALUE(d));
            }
        }
        栈顶值--;
    }
    assert(生成_reg == 0);
    assert(sse_reg == 0);

    /* We shouldn't have many operands on the stack anymore, but the
       call address itself is still there, and it might be in %eax
       (or edx/ecx) currently, which the below writes would clobber.
       So evict all remaining operands here.  */
    保存_寄存器最多n个堆栈条目(0);

    /* Copy R10 and R11 into RDX and RCX, respectively */
    if (数量_reg_args > 2) {
        o(0xd2894c); /* mov %r10, %rdx */
        if (数量_reg_args > 3) {
            o(0xd9894c); /* mov %r11, %rcx */
        }
    }

    if (栈顶值->type.ref->f.func_type != 函数_新) /* implies 函数_旧 or 函数_省略 */
        oad(0xb8, 数量_sse_args < 8 ? 数量_sse_args : 8); /* mov 数量_sse_args, %eax */
    gcall_or_jmp(0);
    if (args_size)
        gadd_sp(args_size);
    栈顶值--;
}

#define 函数_PROLOG_SIZE 11

static void push_arg_reg(int i) {
    局部变量索引 -= 8;
    生成_modrm64(0x89, arg_regs[i], VT_LOCAL, NULL, 局部变量索引);
}

/* generate function prolog of type 't' */
void 生成函数_序言(符号 *func_sym)
{
    C类型 *func_type = &func_sym->type;
    X86_64_Mode mode;
    int i, addr, align, size, reg_count;
    int param_addr = 0, reg_param_index, sse_param_index;
    符号 *sym;
    C类型 *type;

    sym = func_type->ref;
    addr = 指针_大小 * 2;
    局部变量索引 = 0;
    输出代码索引 += 函数_PROLOG_SIZE;
    func_sub_sp_offset = 输出代码索引;
    func_ret_sub = 0;

    if (当前函数_可变参数) {
        int seen_reg_num, seen_sse_num, seen_stack_size;
        seen_reg_num = seen_sse_num = 0;
        /* frame pointer and return address */
        seen_stack_size = 指针_大小 * 2;
        /* count the number of seen parameters */
        sym = func_type->ref;
        while ((sym = sym->next) != NULL) {
            type = &sym->type;
            mode = classify_x86_64_arg(type, NULL, &size, &align, &reg_count);
            switch (mode) {
            default:
            stack_arg:
                seen_stack_size = ((seen_stack_size + align - 1) & -align) + size;
                break;
                
            case x86_64_mode_integer:
                if (seen_reg_num + reg_count > REGN)
		    goto stack_arg;
		seen_reg_num += reg_count;
                break;
                
            case x86_64_mode_sse:
                if (seen_sse_num + reg_count > 8)
		    goto stack_arg;
		seen_sse_num += reg_count;
                break;
            }
        }

        局部变量索引 -= 24;
        /* movl $0x????????, -0x18(%rbp) */
        o(0xe845c7);
        生成_le32(seen_reg_num * 8);
        /* movl $0x????????, -0x14(%rbp) */
        o(0xec45c7);
        生成_le32(seen_sse_num * 16 + 48);
	/* leaq $0x????????, %r11 */
	o(0x9d8d4c);
	生成_le32(seen_stack_size);
	/* movq %r11, -0x10(%rbp) */
	o(0xf05d894c);
	/* leaq $-192(%rbp), %r11 */
	o(0x9d8d4c);
	生成_le32(-176 - 24);
	/* movq %r11, -0x8(%rbp) */
	o(0xf85d894c);

        /* save all register passing arguments */
        for (i = 0; i < 8; i++) {
            局部变量索引 -= 16;
	    if (!zhi_状态->支持mno和sse) {
		o(0xd60f66); /* movq */
		生成_modrm(7 - i, VT_LOCAL, NULL, 局部变量索引);
	    }
            /* movq $0, 局部变量索引+8(%rbp) */
            o(0x85c748);
            生成_le32(局部变量索引 + 8);
            生成_le32(0);
        }
        for (i = 0; i < REGN; i++) {
            push_arg_reg(REGN-1-i);
        }
    }

    sym = func_type->ref;
    reg_param_index = 0;
    sse_param_index = 0;

    /* if the function returns a structure, then add an
       implicit pointer parameter */
    mode = classify_x86_64_arg(&当前函数_返回类型, NULL, &size, &align, &reg_count);
    if (mode == x86_64_mode_memory) {
        push_arg_reg(reg_param_index);
        函数_vc = 局部变量索引;
        reg_param_index++;
    }
    /* define parameters */
    while ((sym = sym->next) != NULL) {
        type = &sym->type;
        mode = classify_x86_64_arg(type, NULL, &size, &align, &reg_count);
        switch (mode) {
        case x86_64_mode_sse:
	    if (zhi_状态->支持mno和sse)
	        错误_打印("SSE disabled but floating point arguments used");
            if (sse_param_index + reg_count <= 8) {
                /* save arguments passed by register */
                局部变量索引 -= reg_count * 8;
                param_addr = 局部变量索引;
                for (i = 0; i < reg_count; ++i) {
                    o(0xd60f66); /* movq */
                    生成_modrm(sse_param_index, VT_LOCAL, NULL, param_addr + i*8);
                    ++sse_param_index;
                }
            } else {
                addr = (addr + align - 1) & -align;
                param_addr = addr;
                addr += size;
            }
            break;
            
        case x86_64_mode_memory:
        case x86_64_mode_x87:
            addr = (addr + align - 1) & -align;
            param_addr = addr;
            addr += size;
            break;
            
        case x86_64_mode_integer: {
            if (reg_param_index + reg_count <= REGN) {
                /* save arguments passed by register */
                局部变量索引 -= reg_count * 8;
                param_addr = 局部变量索引;
                for (i = 0; i < reg_count; ++i) {
                    生成_modrm64(0x89, arg_regs[reg_param_index], VT_LOCAL, NULL, param_addr + i*8);
                    ++reg_param_index;
                }
            } else {
                addr = (addr + align - 1) & -align;
                param_addr = addr;
                addr += size;
            }
            break;
        }
	default: break; /* nothing to be done for x86_64_mode_none */
        }
        符号_压入栈(sym->v & ~符号_字段, type,
                 VT_LOCAL | VT_LVAL, param_addr);
    }

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_prolog();
#endif
}

/* generate function epilog */
void 生成函数_结尾(void)
{
    int v, saved_ind;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_epilog();
#endif
    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        生成(func_ret_sub);
        生成(func_ret_sub >> 8);
    }
    /* align local size to word & save local variables */
    v = (-局部变量索引 + 15) & -16;
    saved_ind = 输出代码索引;
    输出代码索引 = func_sub_sp_offset - 函数_PROLOG_SIZE;
    o(0xe5894855);  /* push %rbp, mov %rsp, %rbp */
    o(0xec8148);  /* sub rsp, stacksize */
    生成_le32(v);
    输出代码索引 = saved_ind;
}

#endif /* not PE */

静态_函数 void 生成_fill_nops(int bytes)
{
    while (bytes--)
      生成(0x90);
}

/* generate a jump to a label */
int 生成跳转到标签(int t)
{
    return gjmp2(0xe9, t);
}

/* generate a jump to a fixed address */
void 生成跳转到_固定地址(int a)
{
    int r;
    r = a - 输出代码索引 - 2;
    if (r == (char)r) {
        生成(0xeb);
        生成(r);
    } else {
        oad(0xe9, a - 输出代码索引 - 5);
    }
}

静态_函数 int g跳转_附件(int n, int t)
{
    void *p;
    /* insert 栈顶值->c jump list in t */
    if (n) {
        uint32_t n1 = n, n2;
        while ((n2 = 读32le(p = 当前_生成代码_段->data + n1)))
            n1 = n2;
        写32le(p, t);
        t = n;
    }
    return t;
}

静态_函数 int g跳转_条件(int op, int t)
{
        if (op & 0x100)
	  {
	    /* This was a float compare.  If the parity flag is set
	       the result was unordered.  For anything except != this
	       means false and we don't jump (anding both conditions).
	       For != this means true (oring both).
	       Take care about inverting the test.  We need to jump
	       to our target if the result was unordered and test wasn't NE,
	       otherwise if unordered we don't want to jump.  */
            int v = 栈顶值->cmp_r;
            op &= ~0x100;
            if (op ^ v ^ (v != 双符号_不等于))
              o(0x067a);  /* jp +6 */
	    else
	      {
	        生成(0x0f);
		t = gjmp2(0x8a, t); /* jp t */
	      }
	  }
        生成(0x0f);
        t = gjmp2(op - 16, t);
        return t;
}

/* generate an integer binary operation */
void 生成_整数二进制运算(int op)
{
    int r, fr, opc, c;
    int ll, uu, cc;

    ll = is64_type(栈顶值[-1].type.t);
    uu = (栈顶值[-1].type.t & VT_无符号) != 0;
    cc = (栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量;

    switch(op) {
    case '+':
    case 符_加进位生成: /* add with carry generation */
        opc = 0;
    生成_op8:
        if (cc && (!ll || (int)栈顶值->c.i == 栈顶值->c.i)) {
            /* constant case */
            vswap();
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            vswap();
            c = 栈顶值->c.i;
            if (c == (char)c) {
                /* XXX: generate inc and dec for smaller code ? */
                orex(ll, r, 0, 0x83);
                o(0xc0 | (opc << 3) | REG_VALUE(r));
                生成(c);
            } else {
                orex(ll, r, 0, 0x81);
                oad(0xc0 | (opc << 3) | REG_VALUE(r), c);
            }
        } else {
            将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
            r = 栈顶值[-1].r;
            fr = 栈顶值[0].r;
            orex(ll, r, fr, (opc << 3) | 0x01);
            o(0xc0 + REG_VALUE(r) + REG_VALUE(fr) * 8);
        }
        栈顶值--;
        if (op >= 符号_ULT && op <= 符_GT)
            vset_VT_CMP(op);
        break;
    case '-':
    case 符_减借位生成: /* sub with carry generation */
        opc = 5;
        goto 生成_op8;
    case 符_加进位使用: /* add with carry use */
        opc = 2;
        goto 生成_op8;
    case 符_减借位使用: /* sub with carry use */
        opc = 3;
        goto 生成_op8;
    case '&':
        opc = 4;
        goto 生成_op8;
    case '^':
        opc = 6;
        goto 生成_op8;
    case '|':
        opc = 1;
        goto 生成_op8;
    case '*':
        将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
        r = 栈顶值[-1].r;
        fr = 栈顶值[0].r;
        orex(ll, fr, r, 0xaf0f); /* imul fr, r */
        o(0xc0 + REG_VALUE(fr) + REG_VALUE(r) * 8);
        栈顶值--;
        break;
    case 双符号_左位移:
        opc = 4;
        goto 生成_shift;
    case 符_SHR:
        opc = 5;
        goto 生成_shift;
    case 双符号_右位移:
        opc = 7;
    生成_shift:
        opc = 0xc0 | (opc << 3);
        if (cc) {
            /* constant case */
            vswap();
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            vswap();
            orex(ll, r, 0, 0xc1); /* shl/shr/sar $xxx, r */
            o(opc | REG_VALUE(r));
            生成(栈顶值->c.i & (ll ? 63 : 31));
        } else {
            /* we generate the shift in ecx */
            将rc寄存器值存储在栈顶值中2(寄存器类_整数, RC_RCX);
            r = 栈顶值[-1].r;
            orex(ll, r, 0, 0xd3); /* shl/shr/sar %cl, r */
            o(opc | REG_VALUE(r));
        }
        栈顶值--;
        break;
    case 符_无符除法:
    case 符_无符取模运算:
        uu = 1;
        goto divmod;
    case '/':
    case '%':
    case 符_指针除法:
        uu = 0;
    divmod:
        /* first operand must be in eax */
        /* XXX: need better constraint for second operand */
        将rc寄存器值存储在栈顶值中2(RC_RAX, RC_RCX);
        r = 栈顶值[-1].r;
        fr = 栈顶值[0].r;
        栈顶值--;
        将r保存到_内存堆栈(TREG_RDX);
        orex(ll, 0, 0, uu ? 0xd231 : 0x99); /* xor %edx,%edx : cqto */
        orex(ll, fr, 0, 0xf7); /* div fr, %eax */
        o((uu ? 0xf0 : 0xf8) + REG_VALUE(fr));
        if (op == '%' || op == 符_无符取模运算)
            r = TREG_RDX;
        else
            r = TREG_RAX;
        栈顶值->r = r;
        break;
    default:
        opc = 7;
        goto 生成_op8;
    }
}

void 生成_独立于CPU的长时间操作(int op)
{
    生成_整数二进制运算(op);
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point type */
/* XXX: need to use ST1 too */
void 生成_浮点运算(int op)
{
    int a, ft, fc, swapped, r;
    int float_type =
        (栈顶值->type.t & VT_基本类型) == VT_长双精度 ? 寄存器类_堆栈0 : 寄存器类_浮点;

    /* convert constants to memory references */
    if ((栈顶值[-1].r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
        vswap();
        将rc寄存器值存储在栈顶值中(float_type);
        vswap();
    }
    if ((栈顶值[0].r & (VT_值掩码 | VT_LVAL)) == VT_VC常量)
        将rc寄存器值存储在栈顶值中(float_type);

    /* must put at least one value in the floating point register */
    if ((栈顶值[-1].r & VT_LVAL) &&
        (栈顶值[0].r & VT_LVAL)) {
        vswap();
        将rc寄存器值存储在栈顶值中(float_type);
        vswap();
    }
    swapped = 0;
    /* swap the stack if needed so that t1 is the register and t2 is
       the memory reference */
    if (栈顶值[-1].r & VT_LVAL) {
        vswap();
        swapped = 1;
    }
    if ((栈顶值->type.t & VT_基本类型) == VT_长双精度) {
        if (op >= 符号_ULT && op <= 符_GT) {
            /* 加载 on stack second operand */
            加载(TREG_ST0, 栈顶值);
            将r保存到_内存堆栈(TREG_RAX); /* eax is used by FP comparison code */
            if (op == 双符号_大于等于 || op == 符_GT)
                swapped = !swapped;
            else if (op == 双符号_等于 || op == 双符号_不等于)
                swapped = 0;
            if (swapped)
                o(0xc9d9); /* fxch %st(1) */
            if (op == 双符号_等于 || op == 双符号_不等于)
                o(0xe9da); /* fucompp */
            else
                o(0xd9de); /* fcompp */
            o(0xe0df); /* fnstsw %ax */
            if (op == 双符号_等于) {
                o(0x45e480); /* and $0x45, %ah */
                o(0x40fC80); /* cmp $0x40, %ah */
            } else if (op == 双符号_不等于) {
                o(0x45e480); /* and $0x45, %ah */
                o(0x40f480); /* xor $0x40, %ah */
                op = 双符号_不等于;
            } else if (op == 双符号_大于等于 || op == 双符号_小于等于) {
                o(0x05c4f6); /* test $0x05, %ah */
                op = 双符号_等于;
            } else {
                o(0x45c4f6); /* test $0x45, %ah */
                op = 双符号_等于;
            }
            栈顶值--;
            vset_VT_CMP(op);
        } else {
            /* no memory reference possible for long double operations */
            加载(TREG_ST0, 栈顶值);
            swapped = !swapped;

            switch(op) {
            default:
            case '+':
                a = 0;
                break;
            case '-':
                a = 4;
                if (swapped)
                    a++;
                break;
            case '*':
                a = 1;
                break;
            case '/':
                a = 6;
                if (swapped)
                    a++;
                break;
            }
            ft = 栈顶值->type.t;
            fc = 栈顶值->c.i;
            o(0xde); /* fxxxp %st, %st(1) */
            o(0xc1 + (a << 3));
            栈顶值--;
        }
    } else {
        if (op >= 符号_ULT && op <= 符_GT) {
            /* if saved lvalue, then we must reload it */
            r = 栈顶值->r;
            fc = 栈顶值->c.i;
            if ((r & VT_值掩码) == VT_LLOCAL) {
                堆栈值 v1;
                r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
                v1.type.t = VT_指针;
                v1.r = VT_LOCAL | VT_LVAL;
                v1.c.i = fc;
                加载(r, &v1);
                fc = 0;
                栈顶值->r = r = r | VT_LVAL;
            }

            if (op == 双符号_等于 || op == 双符号_不等于) {
                swapped = 0;
            } else {
                if (op == 双符号_小于等于 || op == 符_LT)
                    swapped = !swapped;
                if (op == 双符号_小于等于 || op == 双符号_大于等于) {
                    op = 0x93; /* setae */
                } else {
                    op = 0x97; /* seta */
                }
            }

            if (swapped) {
                将rc寄存器值存储在栈顶值中(寄存器类_浮点);
                vswap();
            }
            assert(!(栈顶值[-1].r & VT_LVAL));
            
            if ((栈顶值->type.t & VT_基本类型) == VT_双精度)
                o(0x66);
            if (op == 双符号_等于 || op == 双符号_不等于)
                o(0x2e0f); /* ucomisd */
            else
                o(0x2f0f); /* comisd */

            if (栈顶值->r & VT_LVAL) {
                生成_modrm(栈顶值[-1].r, r, 栈顶值->sym, fc);
            } else {
                o(0xc0 + REG_VALUE(栈顶值[0].r) + REG_VALUE(栈顶值[-1].r)*8);
            }

            栈顶值--;
            vset_VT_CMP(op | 0x100);
            栈顶值->cmp_r = op;
        } else {
            assert((栈顶值->type.t & VT_基本类型) != VT_长双精度);
            switch(op) {
            default:
            case '+':
                a = 0;
                break;
            case '-':
                a = 4;
                break;
            case '*':
                a = 1;
                break;
            case '/':
                a = 6;
                break;
            }
            ft = 栈顶值->type.t;
            fc = 栈顶值->c.i;
            assert((ft & VT_基本类型) != VT_长双精度);
            
            r = 栈顶值->r;
            /* if saved lvalue, then we must reload it */
            if ((栈顶值->r & VT_值掩码) == VT_LLOCAL) {
                堆栈值 v1;
                r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
                v1.type.t = VT_指针;
                v1.r = VT_LOCAL | VT_LVAL;
                v1.c.i = fc;
                加载(r, &v1);
                fc = 0;
                栈顶值->r = r = r | VT_LVAL;
            }
            
            assert(!(栈顶值[-1].r & VT_LVAL));
            if (swapped) {
                assert(栈顶值->r & VT_LVAL);
                将rc寄存器值存储在栈顶值中(寄存器类_浮点);
                vswap();
            }
            
            if ((ft & VT_基本类型) == VT_双精度) {
                o(0xf2);
            } else {
                o(0xf3);
            }
            o(0x0f);
            o(0x58 + a);
            
            if (栈顶值->r & VT_LVAL) {
                生成_modrm(栈顶值[-1].r, r, 栈顶值->sym, fc);
            } else {
                o(0xc0 + REG_VALUE(栈顶值[0].r) + REG_VALUE(栈顶值[-1].r)*8);
            }

            栈顶值--;
        }
    }
}

/* convert integers to fp 't' type. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
void 生成_整数转换为浮点(int t)
{
    if ((t & VT_基本类型) == VT_长双精度) {
        将r保存到_内存堆栈(TREG_ST0);
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
        if ((栈顶值->type.t & VT_基本类型) == VT_长长整数) {
            /* signed long long to float/double/long double (unsigned case
               is handled generically) */
            o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
            o(0x242cdf); /* fildll (%rsp) */
            o(0x08c48348); /* add $8, %rsp */
        } else if ((栈顶值->type.t & (VT_基本类型 | VT_无符号)) ==
                   (VT_整数 | VT_无符号)) {
            /* unsigned int to float/double/long double */
            o(0x6a); /* push $0 */
            生成(0x00);
            o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
            o(0x242cdf); /* fildll (%rsp) */
            o(0x10c48348); /* add $16, %rsp */
        } else {
            /* int to float/double/long double */
            o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
            o(0x2404db); /* fildl (%rsp) */
            o(0x08c48348); /* add $8, %rsp */
        }
        栈顶值->r = TREG_ST0;
    } else {
        int r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点);
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
        o(0xf2 + ((t & VT_基本类型) == VT_浮点?1:0));
        if ((栈顶值->type.t & (VT_基本类型 | VT_无符号)) ==
            (VT_整数 | VT_无符号) ||
            (栈顶值->type.t & VT_基本类型) == VT_长长整数) {
            o(0x48); /* REX */
        }
        o(0x2a0f);
        o(0xc0 + (栈顶值->r & VT_值掩码) + REG_VALUE(r)*8); /* cvtsi2sd */
        栈顶值->r = r;
    }
}

/* convert from one floating point type to another */
void 生成_浮点转换为另一种浮点(int t)
{
    int ft, bt, tbt;

    ft = 栈顶值->type.t;
    bt = ft & VT_基本类型;
    tbt = t & VT_基本类型;
    
    if (bt == VT_浮点) {
        将rc寄存器值存储在栈顶值中(寄存器类_浮点);
        if (tbt == VT_双精度) {
            o(0x140f); /* unpcklps */
            o(0xc0 + REG_VALUE(栈顶值->r)*9);
            o(0x5a0f); /* cvtps2pd */
            o(0xc0 + REG_VALUE(栈顶值->r)*9);
        } else if (tbt == VT_长双精度) {
            将r保存到_内存堆栈(寄存器类_堆栈0);
            /* movss %xmm0,-0x10(%rsp) */
            o(0x110ff3);
            o(0x44 + REG_VALUE(栈顶值->r)*8);
            o(0xf024);
            o(0xf02444d9); /* flds -0x10(%rsp) */
            栈顶值->r = TREG_ST0;
        }
    } else if (bt == VT_双精度) {
        将rc寄存器值存储在栈顶值中(寄存器类_浮点);
        if (tbt == VT_浮点) {
            o(0x140f66); /* unpcklpd */
            o(0xc0 + REG_VALUE(栈顶值->r)*9);
            o(0x5a0f66); /* cvtpd2ps */
            o(0xc0 + REG_VALUE(栈顶值->r)*9);
        } else if (tbt == VT_长双精度) {
            将r保存到_内存堆栈(寄存器类_堆栈0);
            /* movsd %xmm0,-0x10(%rsp) */
            o(0x110ff2);
            o(0x44 + REG_VALUE(栈顶值->r)*8);
            o(0xf024);
            o(0xf02444dd); /* fldl -0x10(%rsp) */
            栈顶值->r = TREG_ST0;
        }
    } else {
        int r;
        将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
        r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点);
        if (tbt == VT_双精度) {
            o(0xf0245cdd); /* fstpl -0x10(%rsp) */
            /* movsd -0x10(%rsp),%xmm0 */
            o(0x100ff2);
            o(0x44 + REG_VALUE(r)*8);
            o(0xf024);
            栈顶值->r = r;
        } else if (tbt == VT_浮点) {
            o(0xf0245cd9); /* fstps -0x10(%rsp) */
            /* movss -0x10(%rsp),%xmm0 */
            o(0x100ff3);
            o(0x44 + REG_VALUE(r)*8);
            o(0xf024);
            栈顶值->r = r;
        }
    }
}

/* convert fp to int 't' type */
void 生成_浮点转换为整数(int t)
{
    int ft, bt, size, r;
    ft = 栈顶值->type.t;
    bt = ft & VT_基本类型;
    if (bt == VT_长双精度) {
        生成_浮点转换为另一种浮点(VT_双精度);
        bt = VT_双精度;
    }

    将rc寄存器值存储在栈顶值中(寄存器类_浮点);
    if (t != VT_整数)
        size = 8;
    else
        size = 4;

    r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
    if (bt == VT_浮点) {
        o(0xf3);
    } else if (bt == VT_双精度) {
        o(0xf2);
    } else {
        assert(0);
    }
    orex(size == 8, r, 0, 0x2c0f); /* cvttss2si or cvttsd2si */
    o(0xc0 + REG_VALUE(栈顶值->r) + REG_VALUE(r)*8);
    栈顶值->r = r;
}

// Generate sign extension from 32 to 64 bits:
静态_函数 void 生成_cvt_sxtw(void)
{
    int r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
    /* x86_64 specific: movslq */
    o(0x6348);
    o(0xc0 + (REG_VALUE(r) << 3) + REG_VALUE(r));
}

/* char/short to int conversion */
静态_函数 void 生成_cvt_csti(int t)
{
    int r, sz, xl, ll;
    r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
    sz = !(t & VT_无符号);
    xl = (t & VT_基本类型) == VT_短整数;
    ll = (栈顶值->type.t & VT_基本类型) == VT_长长整数;
    orex(ll, r, 0, 0xc0b60f /* mov[sz] %a[xl], %eax */
        | (sz << 3 | xl) << 8
        | (REG_VALUE(r) << 3 | REG_VALUE(r)) << 16
        );
}

/* computed goto support */
void g去向(void)
{
    gcall_or_jmp(1);
    栈顶值--;
}

/* Save the stack pointer onto the stack and return the 位置 of its address */
静态_函数 void 生成_vla_sp_保存(int addr) {
    /* mov %rsp,addr(%rbp)*/
    生成_modrm64(0x89, TREG_RSP, VT_LOCAL, NULL, addr);
}

/* Restore the SP from a 位置 on the stack */
静态_函数 void 生成_vla_sp_恢复(int addr) {
    生成_modrm64(0x8b, TREG_RSP, VT_LOCAL, NULL, addr);
}

#ifdef ZHI_TARGET_PE
/* Save result of 生成_vla_分配 onto the stack */
静态_函数 void 生成_vla_结果(int addr) {
    /* mov %rax,addr(%rbp)*/
    生成_modrm64(0x89, TREG_RAX, VT_LOCAL, NULL, addr);
}
#endif

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态_函数 void 生成_vla_分配(C类型 *type, int align) {
    int use_call = 0;

#if defined(配置_ZHI_边界检查)
    use_call = zhi_状态->执行_边界_检查器;
#endif
#ifdef ZHI_TARGET_PE	/* alloca does more than just adjust %rsp on Windows */
    use_call = 1;
#endif
    if (use_call)
    {
        推送对_全局符号V_的引用(&函数_旧_类型, 符_alloca);
        vswap(); /* Move alloca ref past allocation size */
        具体地址函数_调用(1);
    }
    else {
        int r;
        r = 将rc寄存器值存储在栈顶值中(寄存器类_整数); /* allocation size */
        /* sub r,%rsp */
        o(0x2b48);
        o(0xe0 | REG_VALUE(r));
        /* We align to 16 bytes rather than align */
        /* and ~15, %rsp */
        o(0xf0e48348);
        弹出堆栈值();
    }
}


/* end of x86-64 code generator */
/*************************************************************/
#endif /* ! TARGET_DEFS_ONLY */
/******************************************************/
