/*
 *  X86 code generator for ZHI
 * 
 */

#ifdef TARGET_DEFS_ONLY

/* number of available registers */
#define 可用_寄存器数         5
#define NB_ASM_REGS     8
#define 配置_ZHI_汇编

/* a register can belong to several classes. The classes must be
   sorted from more general to more precise (see 将rc寄存器值存储在栈顶值中2() code which does
   assumptions on it). */
#define 寄存器类_整数     0x0001 /* generic integer register */
#define 寄存器类_浮点   0x0002 /* generic float register */
#define RC_EAX     0x0004
#define 寄存器类_堆栈0     0x0008 
#define RC_ECX     0x0010
#define RC_EDX     0x0020
#define RC_EBX     0x0040

#define 寄存器类_返回整数寄存器    RC_EAX /* function return: integer register */
#define RC_IRE2    RC_EDX /* function return: second integer register */
#define 寄存器类_返回浮点寄存器    寄存器类_堆栈0 /* function return: float register */

/* pretty names for the registers */
enum {
    TREG_EAX = 0,
    TREG_ECX,
    TREG_EDX,
    TREG_EBX,
    TREG_ST0,
    TREG_ESP = 4
};

/* return registers for function */
#define 寄存器_返回16位整数寄存器 TREG_EAX /* single word int return register */
#define 寄存器_返回32位整数寄存器 TREG_EDX /* second word return register (for long long) */
#define 寄存器_返回浮点寄存器 TREG_ST0 /* float return register */

/* defined if function parameters must be evaluated in reverse order */
#define 相反顺序_函数_参数

/* defined if structures are passed as pointers. Otherwise structures
   are directly pushed on stack. */
/* #define 函数_STRUCT_PARAM_AS_PTR */

/* pointer size, in bytes */
#define 指针_大小 4

/* long double size and alignment, in bytes */
#define 长双精度_大小  12
#define 长双精度_对齐 4
/* maximum alignment (for aligned attribute support) */
#define MAX_ALIGN     8

/* define if return values need to be extended explicitely
   at caller side (for interfacing with non-ZHI compilers) */
#define PROMOTE_RET

/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局_使用
#include "zhi.h"

/* define to 1/0 to [not] have EBX as 4th register */
#define USE_EBX 0

静态_外部 const int 寄存器_类数[可用_寄存器数] = {
    /* eax */ 寄存器类_整数 | RC_EAX,
    /* ecx */ 寄存器类_整数 | RC_ECX,
    /* edx */ 寄存器类_整数 | RC_EDX,
    /* ebx */ (寄存器类_整数 | RC_EBX) * USE_EBX,
    /* st0 */ 寄存器类_浮点 | 寄存器类_堆栈0,
};

static unsigned long func_sub_sp_offset;
static int func_ret_sub;
#ifdef 配置_ZHI_边界检查
static 目标地址_类型 func_bound_offset;
static unsigned long func_bound_ind;
static int func_bound_add_epilog;
static void 生成_bounds_prolog(void);
static void 生成_bounds_epilog(void);
#endif

/* XXX: make it faster ? */
静态_函数 void 生成(int c)
{
    int ind1;
    if (不需要_代码生成)
        return;
    ind1 = 输出代码索引 + 1;
    if (ind1 > 当前_生成代码_段->data_allocated)
        节_重新分配内存(当前_生成代码_段, ind1);
    当前_生成代码_段->data[输出代码索引] = c;
    输出代码索引 = ind1;
}

静态_函数 void o(unsigned int c)
{
    while (c) {
        生成(c);
        c = c >> 8;
    }
}

静态_函数 void 生成_le16(int v)
{
    生成(v);
    生成(v >> 8);
}

静态_函数 void 生成_le32(int c)
{
    生成(c);
    生成(c >> 8);
    生成(c >> 16);
    生成(c >> 24);
}

/* output a symbol and patch all calls to it */
静态_函数 void 生成符号_地址(int t, int a)
{
    while (t) {
        unsigned char *ptr = 当前_生成代码_段->data + t;
        uint32_t n = 读32le(ptr); /* next value */
        写32le(ptr, a - t - 4);
        t = n;
    }
}

/* instruction + 4 bytes data. Return the address of the data */
static int oad(int c, int s)
{
    int t;
    if (不需要_代码生成)
        return s;
    o(c);
    t = 输出代码索引;
    生成_le32(s);
    return t;
}

静态_函数 void 生成_fill_nops(int bytes)
{
    while (bytes--)
      生成(0x90);
}

/* generate jmp to a label */
#define gjmp2(instr,lbl) oad(instr,lbl)

/* output constant with relocation if 'r & VT_符号' is true */
静态_函数 void 生成_addr32(int r, 符号 *sym, int c)
{
    if (r & VT_符号)
        段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_386_32);
    生成_le32(c);
}

静态_函数 void 生成_addrpc32(int r, 符号 *sym, int c)
{
    if (r & VT_符号)
        段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_386_PC32);
    生成_le32(c - 4);
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void 生成_modrm(int op_reg, int r, 符号 *sym, int c)
{
    op_reg = op_reg << 3;
    if ((r & VT_值掩码) == VT_VC常量) {
        /* constant memory reference */
        o(0x05 | op_reg);
        生成_addr32(r, sym, c);
    } else if ((r & VT_值掩码) == VT_LOCAL) {
        /* currently, we use only ebp as base */
        if (c == (char)c) {
            /* short reference */
            o(0x45 | op_reg);
            生成(c);
        } else {
            oad(0x85 | op_reg, c);
        }
    } else {
        生成(0x00 | op_reg | (r & VT_值掩码));
    }
}

/* 从值“ sv”加载“ r”.必须生成将堆栈值加载到寄存器所需的代码。 */
静态_函数 void 加载(int r, 堆栈值 *sv)
{
    int v, t, ft, fc, fr;
    堆栈值 v1;

#ifdef ZHI_TARGET_PE
    堆栈值 v2;
    sv = pe_获取导入(sv, &v2);
#endif

    fr = sv->r;
    ft = sv->type.t & ~VT_显式符号;
    fc = sv->c.i;

    ft &= ~(VT_易变 | VT_常量);

    v = fr & VT_值掩码;
    if (fr & VT_LVAL) {
        if (v == VT_LLOCAL) {
            v1.type.t = VT_整数;
            v1.r = VT_LOCAL | VT_LVAL;
            v1.c.i = fc;
            v1.sym = NULL;
            fr = r;
            if (!(寄存器_类数[fr] & 寄存器类_整数))
                fr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            加载(fr, &v1);
        }
        if ((ft & VT_基本类型) == VT_浮点) {
            o(0xd9); /* flds */
            r = 0;
        } else if ((ft & VT_基本类型) == VT_双精度) {
            o(0xdd); /* fldl */
            r = 0;
        } else if ((ft & VT_基本类型) == VT_长双精度) {
            o(0xdb); /* fldt */
            r = 5;
        } else if ((ft & VT_类型) == VT_字节 || (ft & VT_类型) == VT_逻辑) {
            o(0xbe0f);   /* movsbl */
        } else if ((ft & VT_类型) == (VT_字节 | VT_无符号)) {
            o(0xb60f);   /* movzbl */
        } else if ((ft & VT_类型) == VT_短整数) {
            o(0xbf0f);   /* movswl */
        } else if ((ft & VT_类型) == (VT_短整数 | VT_无符号)) {
            o(0xb70f);   /* movzwl */
        } else {
            o(0x8b);     /* movl */
        }
        生成_modrm(r, fr, sv->sym, fc);
    } else {
        if (v == VT_VC常量) {
            o(0xb8 + r); /* mov $xx, r */
            生成_addr32(fr, sv->sym, fc);
        } else if (v == VT_LOCAL) {
            if (fc) {
                o(0x8d); /* lea xxx(%ebp), r */
                生成_modrm(r, VT_LOCAL, sv->sym, fc);
            } else {
                o(0x89);
                o(0xe8 + r); /* mov %ebp, r */
            }
        } else if (v == VT_CMP) {
            o(0x0f); /* setxx %br */
            o(fc);
            o(0xc0 + r);
            o(0xc0b60f + r * 0x90000); /* movzbl %al, %eax */
        } else if (v == VT_JMP || v == VT_JMPI) {
            t = v & 1;
            oad(0xb8 + r, t); /* mov $1, r */
            o(0x05eb); /* jmp after */
            生成符号(fc);
            oad(0xb8 + r, t ^ 1); /* mov $0, r */
        } else if (v != r) {
            o(0x89);
            o(0xc0 + r + v * 8); /* mov v, r */
        }
    }
}

/* 将寄存器“ r”存储在左值“ v”中.必须生成将寄存器存储到堆栈值左值所需的代码。 */
静态_函数 void 存储(int r, 堆栈值 *v)
{
    int fr, bt, ft, fc;

#ifdef ZHI_TARGET_PE
    堆栈值 v2;
    v = pe_获取导入(v, &v2);
#endif

    ft = v->type.t;
    fc = v->c.i;
    fr = v->r & VT_值掩码;
    ft &= ~(VT_易变 | VT_常量);
    bt = ft & VT_基本类型;
    /* XXX: incorrect if float reg to reg */
    if (bt == VT_浮点) {
        o(0xd9); /* fsts */
        r = 2;
    } else if (bt == VT_双精度) {
        o(0xdd); /* fstpl */
        r = 2;
    } else if (bt == VT_长双精度) {
        o(0xc0d9); /* fld %st(0) */
        o(0xdb); /* fstpt */
        r = 7;
    } else {
        if (bt == VT_短整数)
            o(0x66);
        if (bt == VT_字节 || bt == VT_逻辑)
            o(0x88);
        else
            o(0x89);
    }
    if (fr == VT_VC常量 ||
        fr == VT_LOCAL ||
        (v->r & VT_LVAL)) {
        生成_modrm(r, v->r, v->sym, fc);
    } else if (fr != r) {
        o(0xc0 + fr + r * 8); /* mov r, fr */
    }
}

static void gadd_sp(int val)
{
    if (val == (char)val) {
        o(0xc483);
        生成(val);
    } else {
        oad(0xc481, val); /* add $xxx, %esp */
    }
}

#if defined 配置_ZHI_边界检查 || defined ZHI_TARGET_PE
static void 生成_static_call(int v)
{
    符号 *sym;

    sym = 外部_全局_符号(v, &函数_旧_类型);
    oad(0xe8, -4);
    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引-4, R_386_PC32);
}
#endif

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
    int r;
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量 && (栈顶值->r & VT_符号)) {
        /* constant and relocation case */
        段部分符号重定位(当前_生成代码_段, 栈顶值->sym, 输出代码索引 + 1, R_386_PC32);
        oad(0xe8 + is_jmp, 栈顶值->c.i - 4); /* call/jmp im */
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器 &&
            (栈顶值->sym->v == 符_alloca ||
             栈顶值->sym->v == 符_setjmp ||
             栈顶值->sym->v == 符__setjmp
#ifndef ZHI_TARGET_PE
             || 栈顶值->sym->v == 符_sigsetjmp
             || 栈顶值->sym->v == 符___sigsetjmp
#endif
            ))
            func_bound_add_epilog = 1;
#endif
    } else {
        /* otherwise, 间接的ect call */
        r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
        o(0xff); /* call/jmp *r */
        o(0xd0 + r + (is_jmp << 4));
    }
}

static uint8_t fastcall_regs[3] = { TREG_EAX, TREG_EDX, TREG_ECX };
static uint8_t fastcallw_regs[2] = { TREG_ECX, TREG_EDX };

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *ret_align, int *regsize)
{
#ifdef ZHI_TARGET_PE
    int size, align;
    *ret_align = 1; // Never have to re-align return values for x86
    *regsize = 4;
    size = 类型_大小(vt, &align);
    if (size > 8 || (size & (size - 1)))
        return 0;
    if (size == 8)
        ret->t = VT_长长整数;
    else if (size == 4)
        ret->t = VT_整数;
    else if (size == 2)
        ret->t = VT_短整数;
    else
        ret->t = VT_字节;
    ret->ref = NULL;
    return 1;
#else
    *ret_align = 1; // Never have to re-align return values for x86
    return 0;
#endif
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */
静态_函数 void 具体地址函数_调用(int 数量_args)
{
    int size, align, r, args_size, i, 函数_调用;
    符号 *func_sym;
    
#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成左值边界代码_函数参数加载到寄存器(数量_args);
#endif

    args_size = 0;
    for(i = 0;i < 数量_args; i++) {
        if ((栈顶值->type.t & VT_基本类型) == VT_结构体) {
            size = 类型_大小(&栈顶值->type, &align);
            /* align to stack align size */
            size = (size + 3) & ~3;
            /* allocate the necessary size on stack */
            oad(0xec81, size); /* sub $xxx, %esp */
            /* generate structure 存储 */
            r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            o(0x89); /* mov %esp, r */
            o(0xe0 + r);
            vset(&栈顶值->type, r | VT_LVAL, 0);
            vswap();
            将栈顶值_存储在堆栈左值();
            args_size += size;
        } else if (是_浮点型(栈顶值->type.t)) {
            将rc寄存器值存储在栈顶值中(寄存器类_浮点); /* only one float register */
            if ((栈顶值->type.t & VT_基本类型) == VT_浮点)
                size = 4;
            else if ((栈顶值->type.t & VT_基本类型) == VT_双精度)
                size = 8;
            else
                size = 12;
            oad(0xec81, size); /* sub $xxx, %esp */
            if (size == 12)
                o(0x7cdb);
            else
                o(0x5cd9 + size - 4); /* fstp[s|l] 0(%esp) */
            生成(0x24);
            生成(0x00);
            args_size += size;
        } else {
            /* simple type (currently always same size) */
            /* XXX: implicit cast ? */
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            if ((栈顶值->type.t & VT_基本类型) == VT_长长整数) {
                size = 8;
                o(0x50 + 栈顶值->r2); /* push r */
            } else {
                size = 4;
            }
            o(0x50 + r); /* push r */
            args_size += size;
        }
        栈顶值--;
    }
    保存_寄存器最多n个堆栈条目(0); /* save used temporary registers */
    func_sym = 栈顶值->type.ref;
    函数_调用 = func_sym->f.函数_调用;
    /* fast call case */
    if ((函数_调用 >= 函数_快速调用1 && 函数_调用 <= 函数_快速调用3) ||
        函数_调用 == 函数_快速调用W) {
        int fastcall_nb_regs;
        uint8_t *fastcall_regs_ptr;
        if (函数_调用 == 函数_快速调用W) {
            fastcall_regs_ptr = fastcallw_regs;
            fastcall_nb_regs = 2;
        } else {
            fastcall_regs_ptr = fastcall_regs;
            fastcall_nb_regs = 函数_调用 - 函数_快速调用1 + 1;
        }
        for(i = 0;i < fastcall_nb_regs; i++) {
            if (args_size <= 0)
                break;
            o(0x58 + fastcall_regs_ptr[i]); /* pop r */
            /* XXX: incorrect for struct/floats */
            args_size -= 4;
        }
    }
#ifndef ZHI_TARGET_PE
    else if ((栈顶值->type.ref->type.t & VT_基本类型) == VT_结构体)
        args_size -= 4;
#endif

    gcall_or_jmp(0);

    if (args_size && 函数_调用 != 函数_标准调用 && 函数_调用 != 函数_快速调用W)
        gadd_sp(args_size);
    栈顶值--;
}

#ifdef ZHI_TARGET_PE
#define 函数_PROLOG_SIZE (10 + USE_EBX)
#else
#define 函数_PROLOG_SIZE (9 + USE_EBX)
#endif

/* generate function prolog of type 't' */
静态_函数 void 生成函数_序言(符号 *func_sym)
{
    C类型 *func_type = &func_sym->type;
    int addr, align, size, 函数_调用, fastcall_nb_regs;
    int param_index, param_addr;
    uint8_t *fastcall_regs_ptr;
    符号 *sym;
    C类型 *type;

    sym = func_type->ref;
    函数_调用 = sym->f.函数_调用;
    addr = 8;
    局部变量索引 = 0;
    函数_vc = 0;

    if (函数_调用 >= 函数_快速调用1 && 函数_调用 <= 函数_快速调用3) {
        fastcall_nb_regs = 函数_调用 - 函数_快速调用1 + 1;
        fastcall_regs_ptr = fastcall_regs;
    } else if (函数_调用 == 函数_快速调用W) {
        fastcall_nb_regs = 2;
        fastcall_regs_ptr = fastcallw_regs;
    } else {
        fastcall_nb_regs = 0;
        fastcall_regs_ptr = NULL;
    }
    param_index = 0;

    输出代码索引 += 函数_PROLOG_SIZE;
    func_sub_sp_offset = 输出代码索引;
    /* if the function returns a structure, then add an
       implicit pointer parameter */
#ifdef ZHI_TARGET_PE
    size = 类型_大小(&当前函数_返回类型,&align);
    if (((当前函数_返回类型.t & VT_基本类型) == VT_结构体)
        && (size > 8 || (size & (size - 1)))) {
#else
    if ((当前函数_返回类型.t & VT_基本类型) == VT_结构体) {
#endif
        /* XXX: fastcall case ? */
        函数_vc = addr;
        addr += 4;
        param_index++;
    }
    /* define parameters */
    while ((sym = sym->next) != NULL) {
        type = &sym->type;
        size = 类型_大小(type, &align);
        size = (size + 3) & ~3;
#ifdef 函数_STRUCT_PARAM_AS_PTR
        /* structs are passed as pointer */
        if ((type->t & VT_基本类型) == VT_结构体) {
            size = 4;
        }
#endif
        if (param_index < fastcall_nb_regs) {
            /* save FASTCALL register */
            局部变量索引 -= 4;
            o(0x89);     /* movl */
            生成_modrm(fastcall_regs_ptr[param_index], VT_LOCAL, NULL, 局部变量索引);
            param_addr = 局部变量索引;
        } else {
            param_addr = addr;
            addr += size;
        }
        符号_压入栈(sym->v & ~符号_字段, type,
                 VT_LOCAL | VT_LVAL, param_addr);
        param_index++;
    }
    func_ret_sub = 0;
    /* pascal type call or fastcall ? */
    if (函数_调用 == 函数_标准调用 || 函数_调用 == 函数_快速调用W)
        func_ret_sub = addr - 8;
#ifndef ZHI_TARGET_PE
    else if (函数_vc)
        func_ret_sub = 4;
#endif

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_prolog();
#endif
}

/* generate function epilog */
静态_函数 void 生成函数_结尾(void)
{
    目标地址_类型 v, saved_ind;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_epilog();
#endif

    /* align local size to word & save local variables */
    v = (-局部变量索引 + 3) & -4;

#if USE_EBX
    o(0x8b);
    生成_modrm(TREG_EBX, VT_LOCAL, NULL, -(v+4));
#endif

    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        生成(func_ret_sub);
        生成(func_ret_sub >> 8);
    }
    saved_ind = 输出代码索引;
    输出代码索引 = func_sub_sp_offset - 函数_PROLOG_SIZE;
#ifdef ZHI_TARGET_PE
    if (v >= 4096) {
        oad(0xb8, v); /* mov stacksize, %eax */
        生成_static_call(符___chkstk); /* call __chkstk, (does the stackframe too) */
    } else
#endif
    {
        o(0xe58955);  /* push %ebp, mov %esp, %ebp */
        o(0xec81);  /* sub esp, stacksize */
        生成_le32(v);
#ifdef ZHI_TARGET_PE
        o(0x90);  /* adjust to 函数_PROLOG_SIZE */
#endif
    }
    o(0x53 * USE_EBX); /* push ebx */
    输出代码索引 = saved_ind;
}

/* generate a jump to a label */
静态_函数 int 生成跳转到标签(int t)
{
    return gjmp2(0xe9, t);
}

/* generate a jump to a fixed address */
静态_函数 void 生成跳转到_固定地址(int a)
{
    int r;
    r = a - 输出代码索引 - 2;
    if (r == (char)r) {
        生成(0xeb);
        生成(r);
    } else {
        oad(0xe9, a - 输出代码索引 - 5);
    }
}

#if 0
/* generate a jump to a fixed address */
静态_函数 void gjmp_cond_addr(int a, int op)
{
    int r = a - 输出代码索引 - 2;
    if (r == (char)r)
        生成(op - 32), 生成(r);
    else
        生成(0x0f), gjmp2(op - 16, r - 4);
}
#endif

静态_函数 int g跳转_附件(int n, int t)
{
    void *p;
    /* insert 栈顶值->c jump list in t */
    if (n) {
        uint32_t n1 = n, n2;
        while ((n2 = 读32le(p = 当前_生成代码_段->data + n1)))
            n1 = n2;
        写32le(p, t);
        t = n;
    }
    return t;
}

静态_函数 int g跳转_条件(int op, int t)
{
    生成(0x0f);
    t = gjmp2(op - 16, t);
    return t;
}

静态_函数 void 生成_整数二进制运算(int op)
{
    int r, fr, opc, c;

    switch(op) {
    case '+':
    case 符_加进位生成: /* add with carry generation */
        opc = 0;
    生成_op8:
        if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
            /* constant case */
            vswap();
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            vswap();
            c = 栈顶值->c.i;
            if (c == (char)c) {
                /* generate inc and dec for smaller code */
                if ((c == 1 || c == -1) && (op == '+' || op == '-')) {
                    opc = (c == 1) ^ (op == '+');
                    o (0x40 | (opc << 3) | r); // inc,dec
                } else {
                    o(0x83);
                    o(0xc0 | (opc << 3) | r);
                    生成(c);
                }
            } else {
                o(0x81);
                oad(0xc0 | (opc << 3) | r, c);
            }
        } else {
            将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
            r = 栈顶值[-1].r;
            fr = 栈顶值[0].r;
            o((opc << 3) | 0x01);
            o(0xc0 + r + fr * 8); 
        }
        栈顶值--;
        if (op >= 符号_ULT && op <= 符_GT)
            vset_VT_CMP(op);
        break;
    case '-':
    case 符_减借位生成: /* sub with carry generation */
        opc = 5;
        goto 生成_op8;
    case 符_加进位使用: /* add with carry use */
        opc = 2;
        goto 生成_op8;
    case 符_减借位使用: /* sub with carry use */
        opc = 3;
        goto 生成_op8;
    case '&':
        opc = 4;
        goto 生成_op8;
    case '^':
        opc = 6;
        goto 生成_op8;
    case '|':
        opc = 1;
        goto 生成_op8;
    case '*':
        将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
        r = 栈顶值[-1].r;
        fr = 栈顶值[0].r;
        栈顶值--;
        o(0xaf0f); /* imul fr, r */
        o(0xc0 + fr + r * 8);
        break;
    case 双符号_左位移:
        opc = 4;
        goto 生成_shift;
    case 符_SHR:
        opc = 5;
        goto 生成_shift;
    case 双符号_右位移:
        opc = 7;
    生成_shift:
        opc = 0xc0 | (opc << 3);
        if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
            /* constant case */
            vswap();
            r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
            vswap();
            c = 栈顶值->c.i & 0x1f;
            o(0xc1); /* shl/shr/sar $xxx, r */
            o(opc | r);
            生成(c);
        } else {
            /* we generate the shift in ecx */
            将rc寄存器值存储在栈顶值中2(寄存器类_整数, RC_ECX);
            r = 栈顶值[-1].r;
            o(0xd3); /* shl/shr/sar %cl, r */
            o(opc | r);
        }
        栈顶值--;
        break;
    case '/':
    case 符_无符除法:
    case 符_指针除法:
    case '%':
    case 符_无符取模运算:
    case 符_无符32位乘法:
        /* first operand must be in eax */
        /* XXX: need better constraint for second operand */
        将rc寄存器值存储在栈顶值中2(RC_EAX, RC_ECX);
        r = 栈顶值[-1].r;
        fr = 栈顶值[0].r;
        栈顶值--;
        将r保存到_内存堆栈(TREG_EDX);
        /* save EAX too if used otherwise */
        将r保存到_内存堆栈_最多n个条目(TREG_EAX, 1);
        if (op == 符_无符32位乘法) {
            o(0xf7); /* mul fr */
            o(0xe0 + fr);
            栈顶值->r2 = TREG_EDX;
            r = TREG_EAX;
        } else {
            if (op == 符_无符除法 || op == 符_无符取模运算) {
                o(0xf7d231); /* xor %edx, %edx, div fr, %eax */
                o(0xf0 + fr);
            } else {
                o(0xf799); /* cltd, idiv fr, %eax */
                o(0xf8 + fr);
            }
            if (op == '%' || op == 符_无符取模运算)
                r = TREG_EDX;
            else
                r = TREG_EAX;
        }
        栈顶值->r = r;
        break;
    default:
        opc = 7;
        goto 生成_op8;
    }
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point type */
/* XXX: need to use ST1 too */
静态_函数 void 生成_浮点运算(int op)
{
    int a, ft, fc, swapped, r;

    /* convert constants to memory references */
    if ((栈顶值[-1].r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
        vswap();
        将rc寄存器值存储在栈顶值中(寄存器类_浮点);
        vswap();
    }
    if ((栈顶值[0].r & (VT_值掩码 | VT_LVAL)) == VT_VC常量)
        将rc寄存器值存储在栈顶值中(寄存器类_浮点);

    /* must put at least one value in the floating point register */
    if ((栈顶值[-1].r & VT_LVAL) &&
        (栈顶值[0].r & VT_LVAL)) {
        vswap();
        将rc寄存器值存储在栈顶值中(寄存器类_浮点);
        vswap();
    }
    swapped = 0;
    /* swap the stack if needed so that t1 is the register and t2 is
       the memory reference */
    if (栈顶值[-1].r & VT_LVAL) {
        vswap();
        swapped = 1;
    }
    if (op >= 符号_ULT && op <= 符_GT) {
        /* 加载 on stack second operand */
        加载(TREG_ST0, 栈顶值);
        将r保存到_内存堆栈(TREG_EAX); /* eax is used by FP comparison code */
        if (op == 双符号_大于等于 || op == 符_GT)
            swapped = !swapped;
        else if (op == 双符号_等于 || op == 双符号_不等于)
            swapped = 0;
        if (swapped)
            o(0xc9d9); /* fxch %st(1) */
        if (op == 双符号_等于 || op == 双符号_不等于)
            o(0xe9da); /* fucompp */
        else
            o(0xd9de); /* fcompp */
        o(0xe0df); /* fnstsw %ax */
        if (op == 双符号_等于) {
            o(0x45e480); /* and $0x45, %ah */
            o(0x40fC80); /* cmp $0x40, %ah */
        } else if (op == 双符号_不等于) {
            o(0x45e480); /* and $0x45, %ah */
            o(0x40f480); /* xor $0x40, %ah */
            op = 双符号_不等于;
        } else if (op == 双符号_大于等于 || op == 双符号_小于等于) {
            o(0x05c4f6); /* test $0x05, %ah */
            op = 双符号_等于;
        } else {
            o(0x45c4f6); /* test $0x45, %ah */
            op = 双符号_等于;
        }
        栈顶值--;
        vset_VT_CMP(op);
    } else {
        /* no memory reference possible for long double operations */
        if ((栈顶值->type.t & VT_基本类型) == VT_长双精度) {
            加载(TREG_ST0, 栈顶值);
            swapped = !swapped;
        }
        
        switch(op) {
        default:
        case '+':
            a = 0;
            break;
        case '-':
            a = 4;
            if (swapped)
                a++;
            break;
        case '*':
            a = 1;
            break;
        case '/':
            a = 6;
            if (swapped)
                a++;
            break;
        }
        ft = 栈顶值->type.t;
        fc = 栈顶值->c.i;
        if ((ft & VT_基本类型) == VT_长双精度) {
            o(0xde); /* fxxxp %st, %st(1) */
            o(0xc1 + (a << 3));
        } else {
            /* if saved lvalue, then we must reload it */
            r = 栈顶值->r;
            if ((r & VT_值掩码) == VT_LLOCAL) {
                堆栈值 v1;
                r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
                v1.type.t = VT_整数;
                v1.r = VT_LOCAL | VT_LVAL;
                v1.c.i = fc;
                v1.sym = NULL;
                加载(r, &v1);
                fc = 0;
            }

            if ((ft & VT_基本类型) == VT_双精度)
                o(0xdc);
            else
                o(0xd8);
            生成_modrm(a, r, 栈顶值->sym, fc);
        }
        栈顶值--;
    }
}

/* convert integers to fp 't' type. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
静态_函数 void 生成_整数转换为浮点(int t)
{
    将r保存到_内存堆栈(TREG_ST0);
    将rc寄存器值存储在栈顶值中(寄存器类_整数);
    if ((栈顶值->type.t & VT_基本类型) == VT_长长整数) {
        /* signed long long to float/double/long double (unsigned case
           is handled generically) */
        o(0x50 + 栈顶值->r2); /* push r2 */
        o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
        o(0x242cdf); /* fildll (%esp) */
        o(0x08c483); /* add $8, %esp */
        栈顶值->r2 = VT_VC常量;
    } else if ((栈顶值->type.t & (VT_基本类型 | VT_无符号)) == 
               (VT_整数 | VT_无符号)) {
        /* unsigned int to float/double/long double */
        o(0x6a); /* push $0 */
        生成(0x00);
        o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
        o(0x242cdf); /* fildll (%esp) */
        o(0x08c483); /* add $8, %esp */
    } else {
        /* int to float/double/long double */
        o(0x50 + (栈顶值->r & VT_值掩码)); /* push r */
        o(0x2404db); /* fildl (%esp) */
        o(0x04c483); /* add $4, %esp */
    }
    栈顶值->r2 = VT_VC常量;
    栈顶值->r = TREG_ST0;
}

/* convert fp to int 't' type */
静态_函数 void 生成_浮点转换为整数(int t)
{
    int bt = 栈顶值->type.t & VT_基本类型;
    if (bt == VT_浮点)
        推送对_全局符号V_的引用(&函数_旧_类型, 符___fixsfdi);
    else if (bt == VT_长双精度)
        推送对_全局符号V_的引用(&函数_旧_类型, 符___fixxfdi);
    else
        推送对_全局符号V_的引用(&函数_旧_类型, 符___fixdfdi);
    vswap();
    具体地址函数_调用(1);
    压入整数常量(0);
    栈顶值->r = 寄存器_返回16位整数寄存器;
    if ((t & VT_基本类型) == VT_长长整数)
        栈顶值->r2 = 寄存器_返回32位整数寄存器;
}

/* convert from one floating point type to another */
静态_函数 void 生成_浮点转换为另一种浮点(int t)
{
    /* all we have to do on i386 is to put the float in a register */
    将rc寄存器值存储在栈顶值中(寄存器类_浮点);
}

/* char/short to int conversion */
静态_函数 void 生成_cvt_csti(int t)
{
    int r, sz, xl;
    r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
    sz = !(t & VT_无符号);
    xl = (t & VT_基本类型) == VT_短整数;
    o(0xc0b60f /* mov[sz] %a[xl], %eax */
        | (sz << 3 | xl) << 8
        | (r << 3 | r) << 16
        );
}

/* computed goto support */
静态_函数 void g去向(void)
{
    gcall_or_jmp(1);
    栈顶值--;
}

/* bound check support functions */
#ifdef 配置_ZHI_边界检查
/* generate a bounded pointer addition */
静态_函数 void 生成_边界的_ptr_添加(void)
{
    推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_ptr_add);
    vrott(3);
    具体地址函数_调用(2);
    压入整数常量(0);
    /* returned pointer is in eax */
    栈顶值->r = TREG_EAX | VT_有界的;
    if (不需要_代码生成)
        return;
    /* relocation offset of the bounding function call point */
    栈顶值->c.i = (当前_生成代码_段->重定位->数据_偏移 - sizeof(Elf32_Rel));
}

/* patch pointer addition in 栈顶值 so that pointer dereferencing is
   also tested */
静态_函数 void 生成_边界的_ptr_取消引用(void)
{
    目标地址_类型 func;
    int  size, align;
    Elf32_Rel *rel;
    符号 *sym;

    if (不需要_代码生成)
        return;

    size = 类型_大小(&栈顶值->type, &align);
    switch(size) {
    case  1: func = 符___bound_ptr_间接的1; break;
    case  2: func = 符___bound_ptr_间接的2; break;
    case  4: func = 符___bound_ptr_间接的4; break;
    case  8: func = 符___bound_ptr_间接的8; break;
    case 12: func = 符___bound_ptr_间接的12; break;
    case 16: func = 符___bound_ptr_间接的16; break;
    default:
        /* 结构成员访问可能发生 */
        return;
    }
    sym = 外部_全局_符号(func, &函数_旧_类型);
    if (!sym->c)
        更新_外部_符号(sym, NULL, 0, 0);
    /* patch relocation */
    /* XXX: find a better solution ? */
    rel = (Elf32_Rel *)(当前_生成代码_段->重定位->data + 栈顶值->c.i);
    rel->r_info = ELF32_R_INFO(sym->c, ELF32_R_TYPE(rel->r_info));
}

static void 生成_bounds_prolog(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 本地边界_部分->数据_偏移;
    func_bound_ind = 输出代码索引;
    func_bound_add_epilog = 0;
    oad(0xb8, 0); /* lbound section pointer */
    oad(0xb8, 0); /* call to function */
}

static void 生成_bounds_epilog(void)
{
    目标地址_类型 saved_ind;
    目标地址_类型 *bounds_ptr;
    符号 *sym_data;
    int offset_modified = func_bound_offset != 本地边界_部分->数据_偏移;

    if (!offset_modified && !func_bound_add_epilog)
        return;

    /* add end of table info */
    bounds_ptr = 段_ptr_添加(本地边界_部分, sizeof(目标地址_类型));
    *bounds_ptr = 0;

    sym_data = 返回_指向节的_静态符号(&字符_指针_类型, 本地边界_部分,
                           func_bound_offset, 本地边界_部分->数据_偏移);

    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 输出代码索引;
        输出代码索引 = func_bound_ind;
        段部分符号重定位(当前_生成代码_段, sym_data, 输出代码索引 + 1, R_386_32);
        输出代码索引 = 输出代码索引 + 5;
        生成_static_call(符___bound_local_new);
        输出代码索引 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0x5250); /* save returned value, if any */
    段部分符号重定位(当前_生成代码_段, sym_data, 输出代码索引 + 1, R_386_32);
    oad(0xb8, 0); /* mov %eax, xxx */
    生成_static_call(符___bound_local_delete);
    o(0x585a); /* restore returned value, if any */
}
#endif

/* Save the stack pointer onto the stack */
静态_函数 void 生成_vla_sp_保存(int addr) {
    /* mov %esp,addr(%ebp)*/
    o(0x89);
    生成_modrm(TREG_ESP, VT_LOCAL, NULL, addr);
}

/* Restore the SP from a 位置 on the stack */
静态_函数 void 生成_vla_sp_恢复(int addr) {
    o(0x8b);
    生成_modrm(TREG_ESP, VT_LOCAL, NULL, addr);
}

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态_函数 void 生成_vla_分配(C类型 *type, int align) {
    int use_call = 0;

#if defined(配置_ZHI_边界检查)
    use_call = zhi_状态->执行_边界_检查器;
#endif
#ifdef ZHI_TARGET_PE    /* alloca does more than just adjust %rsp on Windows */
    use_call = 1;
#endif
    if (use_call)
    {
        推送对_全局符号V_的引用(&函数_旧_类型, 符_alloca);
        vswap(); /* Move alloca ref past allocation size */
        具体地址函数_调用(1);
    }
    else {
        int r;
        r = 将rc寄存器值存储在栈顶值中(寄存器类_整数); /* allocation size */
        /* sub r,%rsp */
        o(0x2b);
        o(0xe0 | r);
        /* We align to 16 bytes rather than align */
        /* and ~15, %esp */
        o(0xf0e483);
        弹出堆栈值();
    }
}

/* end of X86 code generator */
/*************************************************************/
#endif
/*************************************************************/
