/*
 *  CIL操作码定义
 * CIL一般指通用中间语言。通用中间语言（Common Intermediate Language，简称CIL）
 */
操作码(NOP, "nop", 0x00)
操作码(BREAK, "break", 0x01)      //向公共语言结构 (CLI) 发出信号以通知调试器已撞上了一个断点。
操作码(LDARG_0, "ldarg.0", 0x02)  //将索引为 0 的参数加载到计算堆栈上。
操作码(LDARG_1, "ldarg.1", 0x03)  //将索引为 1 的参数加载到计算堆栈上。
操作码(LDARG_2, "ldarg.2", 0x04)  //将索引为 2 的参数加载到计算堆栈上。
操作码(LDARG_3, "ldarg.3", 0x05)  //将索引为 3 的参数加载到计算堆栈上。
操作码(LDLOC_0, "ldloc.0", 0x06)  //将索引 0 处的局部变量加载到计算堆栈上。
操作码(LDLOC_1, "ldloc.1", 0x07)  //将索引 1 处的局部变量加载到计算堆栈上。
操作码(LDLOC_2, "ldloc.2", 0x08)  //将索引 2 处的局部变量加载到计算堆栈上。
操作码(LDLOC_3, "ldloc.3", 0x09)  //将索引 3 处的局部变量加载到计算堆栈上。
操作码(STLOC_0, "stloc.0", 0x0a)  //从计算堆栈的顶部弹出当前值并将其存储到索引 0 处的局部变量列表中。
操作码(STLOC_1, "stloc.1", 0x0b)  //从计算堆栈的顶部弹出当前值并将其存储到索引 1 处的局部变量列表中。
操作码(STLOC_2, "stloc.2", 0x0c)  //从计算堆栈的顶部弹出当前值并将其存储到索引 2 处的局部变量列表中。
操作码(STLOC_3, "stloc.3", 0x0d)  //从计算堆栈的顶部弹出当前值并将其存储到索引 3 处的局部变量列表中。
操作码(LDARG_S, "ldarg.s", 0x0e)  //将参数（由指定的短格式索引引用）加载到计算堆栈上。
操作码(LDARGA_S, "ldarga.s", 0x0f)//以短格式将参数地址加载到计算堆栈上。
操作码(STARG_S, "starg.s", 0x10)  //将位于计算堆栈顶部的值存储在参数槽中的指定索引处（短格式）。
操作码(LDLOC_S, "ldloc.s", 0x11)  //将特定索引处的局部变量加载到计算堆栈上（短格式）。
操作码(LDLOCA_S, "ldloca.s", 0x12)//将位于特定索引处的局部变量的地址加载到计算堆栈上（短格式）。
操作码(STLOC_S, "stloc.s", 0x13)  //从计算堆栈的顶部弹出当前值并将其存储在局部变量列表中的 index 处（短格式）。
操作码(LDNULL, "ldnull", 0x14)    //将空引用（O 类型）推送到计算堆栈上。
操作码(LDC_I4_M1, "ldc.i4.m1", 0x15)//将整数值 -1 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_0, "ldc.i4.0", 0x16)//将整数值 0 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_1, "ldc.i4.1", 0x17)//将整数值 1 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_2, "ldc.i4.2", 0x18)//将整数值 2 作为 int32 推送到计算堆栈上
操作码(LDC_I4_3, "ldc.i4.3", 0x19)//将整数值 3 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_4, "ldc.i4.4", 0x1a)//将整数值 4 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_5, "ldc.i4.5", 0x1b)//将整数值 5 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_6, "ldc.i4.6", 0x1c)//将整数值 6 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_7, "ldc.i4.7", 0x1d)//将整数值 7 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_8, "ldc.i4.8", 0x1e)//将整数值 8 作为 int32 推送到计算堆栈上。
操作码(LDC_I4_S, "ldc.i4.s", 0x1f)//将提供的 int8 值作为 int32 推送到计算堆栈上（短格式）。
操作码(LDC_I4, "ldc.i4", 0x20)    //将所提供的 int32 类型的值作为 int32 推送到计算堆栈上。
操作码(LDC_I8, "ldc.i8", 0x21)    //将所提供的 int64 类型的值作为 int64 推送到计算堆栈上。
操作码(LDC_R4, "ldc.r4", 0x22)    //将所提供的 float32 类型的值作为 F (float) 类型推送到计算堆栈上。
操作码(LDC_R8, "ldc.r8", 0x23)    //将所提供的 float64 类型的值作为 F (float) 类型推送到计算堆栈上。
操作码(LDPTR, "ldptr", 0x24)
操作码(DUP, "dup", 0x25)          //复制计算堆栈上当前最顶端的值，然后将副本推送到计算堆栈上。
操作码(POP, "pop", 0x26)          //移除当前位于计算堆栈顶部的值。
操作码(JMP, "jmp", 0x27)          //退出当前方法并跳至指定方法。
操作码(CALL, "call", 0x28)        //调用由传递的方法说明符指示的方法。
操作码(CALLI, "calli", 0x29)      //通过调用约定描述的参数调用在计算堆栈上指示的方法（作为指向入口点的指针）。
操作码(RET, "ret", 0x2a)          //从当前方法返回，并将返回值（如果存在）从调用方的计算堆栈推送到被调用方的计算堆栈上。
操作码(BR_S, "br.s", 0x2b)        //无条件地将控制转移到目标指令（短格式）。
操作码(BRFALSE_S, "brfalse.s", 0x2c)//如果 value 为 false、空引用或零，则将控制转移到目标指令。
操作码(BRTRUE_S, "brtrue.s", 0x2d)//如果 value 为 true、非空或非零，则将控制转移到目标指令（短格式）。
操作码(BEQ_S, "beq.s", 0x2e)      //如果两个值相等，则将控制转移到目标指令（短格式）。
操作码(BGE_S, "bge.s", 0x2f)      //如果第一个值大于或等于第二个值，则将控制转移到目标指令（短格式）。
操作码(BGT_S, "bgt.s", 0x30)      //如果第一个值大于第二个值，则将控制转移到目标指令（短格式）。
操作码(BLE_S, "ble.s", 0x31)      //如果第一个值小于或等于第二个值，则将控制转移到目标指令（短格式）。
操作码(BLT_S, "blt.s", 0x32)      //如果第一个值小于第二个值，则将控制转移到目标指令（短格式）。
操作码(BNE_UN_S, "bne.un.s", 0x33)//当两个无符号整数值或不可排序的浮点型值不相等时，将控制转移到目标指令（短格式）。
操作码(BGE_UN_S, "bge.un.s", 0x34)//当比较无符号整数值或不可排序的浮点型值时，如果第一个值大于第二个值，则将控制转移到目标指令（短格式）。
操作码(BGT_UN_S, "bgt.un.s", 0x35)//当比较无符号整数值或不可排序的浮点型值时，如果第一个值大于第二个值，则将控制转移到目标指令（短格式）。
操作码(BLE_UN_S, "ble.un.s", 0x36)//当比较无符号整数值或不可排序的浮点值时，如果第一个值小于或等于第二个值，则将控制权转移到目标指令（短格式）。
操作码(BLT_UN_S, "blt.un.s", 0x37)//当比较无符号整数值或不可排序的浮点型值时，如果第一个值小于第二个值，则将控制转移到目标指令（短格式）。
操作码(BR, "br", 0x38)            //无条件地将控制转移到目标指令。
操作码(BRFALSE, "brfalse", 0x39)  //如果 value 为 false、空引用（Visual Basic 中的 Nothing）或零，则将控制转移到目标指令。
操作码(BRTRUE, "brtrue", 0x3a)    //如果 value 为 true、非空或非零，则将控制转移到目标指令。
操作码(BEQ, "beq", 0x3b)          //如果两个值相等，则将控制转移到目标指令。
操作码(BGE, "bge", 0x3c)          //如果第一个值大于或等于第二个值，则将控制转移到目标指令。
操作码(BGT, "bgt", 0x3d)          //如果第一个值大于第二个值，则将控制转移到目标指令。
操作码(BLE, "ble", 0x3e)          //如果第一个值小于或等于第二个值，则将控制转移到目标指令。
操作码(BLT, "blt", 0x3f)          //如果第一个值小于第二个值，则将控制转移到目标指令。
操作码(BNE_UN, "bne.un", 0x40)    //当两个无符号整数值或不可排序的浮点型值不相等时，将控制转移到目标指令。
操作码(BGE_UN, "bge.un", 0x41)    //当比较无符号整数值或不可排序的浮点型值时，如果第一个值大于第二个值，则将控制转移到目标指令。
操作码(BGT_UN, "bgt.un", 0x42)    //当比较无符号整数值或不可排序的浮点型值时，如果第一个值大于第二个值，则将控制转移到目标指令。
操作码(BLE_UN, "ble.un", 0x43)    //当比较无符号整数值或不可排序的浮点型值时，如果第一个值小于或等于第二个值，则将控制转移到目标指令。
操作码(BLT_UN, "blt.un", 0x44)    //当比较无符号整数值或不可排序的浮点型值时，如果第一个值小于第二个值，则将控制转移到目标指令。
操作码(SWITCH, "switch", 0x45)    //实现跳转表。
操作码(LDIND_I1, "ldind.i1", 0x46)//将 int8 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_U1, "ldind.u1", 0x47)//将 unsigned int8 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_I2, "ldind.i2", 0x48)//将 int16 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_U2, "ldind.u2", 0x49)//将 unsigned int16 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_I4, "ldind.i4", 0x4a)//将 int32 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_U4, "ldind.u4", 0x4b)//将 unsigned int32 类型的值作为 int32 间接加载到计算堆栈上。
操作码(LDIND_I8, "ldind.i8", 0x4c)//将 int64 类型的值作为 int64 间接加载到计算堆栈上。
操作码(LDIND_I, "ldind.i", 0x4d)  //将 native int 类型的值作为 native int 间接加载到计算堆栈上。
操作码(LDIND_R4, "ldind.r4", 0x4e)//将 float32 类型的值作为 F (float) 类型间接加载到计算堆栈上。
操作码(LDIND_R8, "ldind.r8", 0x4f)//将 float64 类型的值作为 F (float) 类型间接加载到计算堆栈上。
操作码(LDIND_REF, "ldind.ref", 0x50)//将对象引用作为 O（对象引用）类型间接加载到计算堆栈上。
操作码(STIND_REF, "stind.ref", 0x51)//存储所提供地址处的对象引用值。
操作码(STIND_I1, "stind.i1", 0x52)//在所提供的地址存储 int8 类型的值。
操作码(STIND_I2, "stind.i2", 0x53)//在所提供的地址存储 int16 类型的值。
操作码(STIND_I4, "stind.i4", 0x54)//在所提供的地址存储 int32 类型的值。
操作码(STIND_I8, "stind.i8", 0x55)//在所提供的地址存储 int64 类型的值。
操作码(STIND_R4, "stind.r4", 0x56)//在所提供的地址存储 float32 类型的值。
操作码(STIND_R8, "stind.r8", 0x57)//在所提供的地址存储 float64 类型的值。
操作码(ADD, "add", 0x58)          //将两个值相加并将结果推送到计算堆栈上。
操作码(SUB, "sub", 0x59)          //从其他值中减去一个值并将结果推送到计算堆栈上。
操作码(MUL, "mul", 0x5a)          //将两个值相乘并将结果推送到计算堆栈上。
操作码(DIV, "div", 0x5b)          //将两个值相除并将结果作为浮点（F 类型）或商（int32 类型）推送到计算堆栈上。
操作码(DIV_UN, "div.un", 0x5c)    //两个无符号整数值相除并将结果 ( int32 ) 推送到计算堆栈上。
操作码(REM, "rem", 0x5d)          //将两个值相除并将余数推送到计算堆栈上。
操作码(REM_UN, "rem.un", 0x5e)    //将两个无符号值相除并将余数推送到计算堆栈上。
操作码(AND, "and", 0x5f)          //计算两个值的按位“与”并将结果推送到计算堆栈上。
操作码(OR, "or", 0x60)            //计算位于堆栈顶部的两个整数值的按位求补并将结果推送到计算堆栈上。
操作码(XOR, "xor", 0x61)          //计算位于计算堆栈顶部的两个值的按位异或，并且将结果推送到计算堆栈上。
操作码(SHL, "shl", 0x62)          //将整数值左移（用零填充）指定的位数，并将结果推送到计算堆栈上。
操作码(SHR, "shr", 0x63)          //将整数值右移（保留符号）指定的位数，并将结果推送到计算堆栈上。
操作码(SHR_UN, "shr.un", 0x64)    //将无符号整数值右移（用零填充）指定的位数，并将结果推送到计算堆栈上。
操作码(NEG, "neg", 0x65)          //对一个值执行求反并将结果推送到计算堆栈上。
操作码(NOT, "not", 0x66)          //计算堆栈顶部整数值的按位求补并将结果作为相同的类型推送到计算堆栈上。
操作码(CONV_I1, "conv.i1", 0x67)  //将位于计算堆栈顶部的值转换为 int8，然后将其扩展（填充）为 int32。
操作码(CONV_I2, "conv.i2", 0x68)  //将位于计算堆栈顶部的值转换为 int16，然后将其扩展（填充）为 int32。
操作码(CONV_I4, "conv.i4", 0x69)  //将位于计算堆栈顶部的值转换为 int32。
操作码(CONV_I8, "conv.i8", 0x6a)  //将位于计算堆栈顶部的值转换为 int64。
操作码(CONV_R4, "conv.r4", 0x6b)  //将位于计算堆栈顶部的值转换为 float32。
操作码(CONV_R8, "conv.r8", 0x6c)  //将位于计算堆栈顶部的值转换为 float64。
操作码(CONV_U4, "conv.u4", 0x6d)  //将位于计算堆栈顶部的值转换为 unsigned int32，然后将其扩展为 int32。
操作码(CONV_U8, "conv.u8", 0x6e)  //将位于计算堆栈顶部的值转换为 unsigned int64，然后将其扩展为 int64。
操作码(CALLVIRT, "callvirt", 0x6f)//对对象调用后期绑定方法，并且将返回值推送到计算堆栈上。
操作码(CPOBJ, "cpobj", 0x70)      //将位于对象（&、* 或 native int 类型）地址的值类型复制到目标对象（&、* 或 native int 类型）的地址。
操作码(LDOBJ, "ldobj", 0x71)      //将地址指向的值类型对象复制到计算堆栈的顶部。
操作码(LDSTR, "ldstr", 0x72)      //推送对元数据中存储的字符串的新对象引用。
操作码(NEWOBJ, "newobj", 0x73)    //创建一个值类型的新对象或新实例，并将对象引用（O 类型）推送到计算堆栈上。
操作码(CASTCLASS, "castclass", 0x74)//尝试将引用传递的对象转换为指定的类。
操作码(ISINST, "isinst", 0x75)    //测试对象引用（O 类型）是否为特定类的实例。
操作码(CONV_R_UN, "conv.r.un", 0x76)//将位于计算堆栈顶部的无符号整数值转换为 float32。
操作码(ANN_DATA_S, "ann.data.s", 0x77)
操作码(UNBOX, "unbox", 0x79)      //将值类型的已装箱的表示形式转换为其未装箱的形式。
操作码(THROW, "throw", 0x7a)      //引发当前位于计算堆栈上的异常对象。
操作码(LDFLD, "ldfld", 0x7b)      //查找对象中其引用当前位于计算堆栈的字段的值。
操作码(LDFLDA, "ldflda", 0x7c)    //查找对象中其引用当前位于计算堆栈的字段的地址。
操作码(STFLD, "stfld", 0x7d)      //用新值替换在对象引用或指针的字段中存储的值。
操作码(LDSFLD, "ldsfld", 0x7e)    //将静态字段的值推送到计算堆栈上。
操作码(LDSFLDA, "ldsflda", 0x7f)  //将静态字段的地址推送到计算堆栈上。
操作码(STSFLD, "stsfld", 0x80)    //用来自计算堆栈的值替换静态字段的值。
操作码(STOBJ, "stobj", 0x81)      //将指定类型的值从计算堆栈复制到所提供的内存地址中。
操作码(CONV_OVF_I1_UN, "conv.ovf.i1.un", 0x82)//将位于计算堆栈顶部的无符号值转换为有符号 int8 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I2_UN, "conv.ovf.i2.un", 0x83)//将位于计算堆栈顶部的无符号值转换为有符号 int16 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I4_UN, "conv.ovf.i4.un", 0x84)//将位于计算堆栈顶部的无符号值转换为有符号 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I8_UN, "conv.ovf.i8.un", 0x85)//将位于计算堆栈顶部的无符号值转换为有符号 int64，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U1_UN, "conv.ovf.u1.un", 0x86)//将位于计算堆栈顶部的无符号值转换为 unsigned int8 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U2_UN, "conv.ovf.u2.un", 0x87)//将位于计算堆栈顶部的无符号值转换为 unsigned int16 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U4_UN, "conv.ovf.u4.un", 0x88)//将位于计算堆栈顶部的无符号值转换为 unsigned int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U8_UN, "conv.ovf.u8.un", 0x89)//将位于计算堆栈顶部的无符号值转换为 unsigned int64，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I_UN, "conv.ovf.i.un", 0x8a)//将位于计算堆栈顶部的无符号值转换为有符号 native int，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U_UN, "conv.ovf.u.un", 0x8b)//将位于计算堆栈顶部的无符号值转换为 unsigned native int，并在溢出时引发 OverflowException。
操作码(BOX, "box", 0x8c)          //将值类转换为对象引用（O 类型）。
操作码(NEWARR, "newarr", 0x8d)    //将对新的从零开始的一维数组（其元素属于特定类型）的对象引用推送到计算堆栈上。
操作码(LDLEN, "ldlen", 0x8e)      //将从零开始的、一维数组的元素的数目推送到计算堆栈上。
操作码(LDELEMA, "ldelema", 0x8f)  //将位于指定数组索引的数组元素的地址作为 & 类型（托管指针）加载到计算堆栈的顶部。
操作码(LDELEM_I1, "ldelem.i1", 0x90)//将位于指定数组索引的数组元素的地址作为 & 类型（托管指针）加载到计算堆栈的顶部。
操作码(LDELEM_U1, "ldelem.u1", 0x91)//将位于指定数组索引处的 unsigned int8 类型的元素作为 int32 加载到计算堆栈的顶部。
操作码(LDELEM_I2, "ldelem.i2", 0x92)//将位于指定数组索引处的 int16 类型的元素作为 int32 加载到计算堆栈的顶部。
操作码(LDELEM_U2, "ldelem.u2", 0x93)//将位于指定数组索引处的 unsigned int16 类型的元素作为 int32 加载到计算堆栈的顶部。
操作码(LDELEM_I4, "ldelem.i4", 0x94)//将位于指定数组索引处的 int32 类型的元素作为 int32 加载到计算堆栈的顶部。
操作码(LDELEM_U4, "ldelem.u4", 0x95)//将位于指定数组索引处的 unsigned int32 类型的元素作为 int32 加载到计算堆栈的顶部。
操作码(LDELEM_I8, "ldelem.i8", 0x96)//将位于指定数组索引处的 int64 类型的元素作为 int64 加载到计算堆栈的顶部。
操作码(LDELEM_I, "ldelem.i", 0x97)//将位于指定数组索引处的 native int 类型的元素作为 native int 加载到计算堆栈的顶部。
操作码(LDELEM_R4, "ldelem.r4", 0x98)//将位于指定数组索引处的 float32 类型的元素作为 F 类型（浮点型）加载到计算堆栈的顶部。
操作码(LDELEM_R8, "ldelem.r8", 0x99)//将位于指定数组索引处的 float64 类型的元素作为 F 类型（浮点型）加载到计算堆栈的顶部。
操作码(LDELEM_REF, "ldelem.ref", 0x9a)//将位于指定数组索引处的包含对象引用的元素作为 O 类型（对象引用）加载到计算堆栈的顶部。
操作码(STELEM_I, "stelem.i", 0x9b)//用计算堆栈上的 native int 值替换给定索引处的数组元素。
操作码(STELEM_I1, "stelem.i1", 0x9c)//用计算堆栈上的 int8 值替换给定索引处的数组元素。
操作码(STELEM_I2, "stelem.i2", 0x9d)//用计算堆栈上的 int16 值替换给定索引处的数组元素。
操作码(STELEM_I4, "stelem.i4", 0x9e)//用计算堆栈上的 int32 值替换给定索引处的数组元素。
操作码(STELEM_I8, "stelem.i8", 0x9f)//用计算堆栈上的 int64 值替换给定索引处的数组元素。
操作码(STELEM_R4, "stelem.r4", 0xa0)//用计算堆栈上的 float32 值替换给定索引处的数组元素。
操作码(STELEM_R8, "stelem.r8", 0xa1)//用计算堆栈上的 float64 值替换给定索引处的数组元素。
操作码(STELEM_REF, "stelem.ref", 0xa2)//用计算堆栈上的对象 ref 值（O 类型）替换给定索引处的数组元素。
操作码(CONV_OVF_I1, "conv.ovf.i1", 0xb3)//将位于计算堆栈顶部的有符号值转换为有符号 int8 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U1, "conv.ovf.u1", 0xb4)//将位于计算堆栈顶部的有符号值转换为 unsigned int8 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I2, "conv.ovf.i2", 0xb5)//将位于计算堆栈顶部的有符号值转换为有符号 int16 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U2, "conv.ovf.u2", 0xb6)//将位于计算堆栈顶部的有符号值转换为 unsigned int16 并将其扩展为 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I4, "conv.ovf.i4", 0xb7)//将位于计算堆栈顶部的有符号值转换为有符号 int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U4, "conv.ovf.u4", 0xb8)//将位于计算堆栈顶部的有符号值转换为 unsigned int32，并在溢出时引发 OverflowException。
操作码(CONV_OVF_I8, "conv.ovf.i8", 0xb9)//将位于计算堆栈顶部的有符号值转换为有符号 int64，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U8, "conv.ovf.u8", 0xba)//将位于计算堆栈顶部的有符号值转换为 unsigned int64，并在溢出时引发 OverflowException。
操作码(REFANYVAL, "refanyval", 0xc2)//检索嵌入在类型化引用内的地址（& 类型）。
操作码(CKFINITE, "ckfinite", 0xc3)  //如果值不是有限数，则引发 ArithmeticException。
操作码(MKREFANY, "mkrefany", 0xc6)  //将对特定类型实例的类型化引用推送到计算堆栈上。
操作码(ANN_CALL, "ann.call", 0xc7)  //
操作码(ANN_CATCH, "ann.catch", 0xc8)//
操作码(ANN_DEAD, "ann.dead", 0xc9)  //
操作码(ANN_HOISTED, "ann.hoisted", 0xca)//
操作码(ANN_HOISTED_CALL, "ann.hoisted.call", 0xcb)//
操作码(ANN_LAB, "ann.lab", 0xcc)    //
操作码(ANN_DEF, "ann.def", 0xcd)    //
操作码(ANN_REF_S, "ann.ref.s", 0xce)//
操作码(ANN_PHI, "ann.phi", 0xcf)    //
操作码(LDTOKEN, "ld标识符", 0xd0)    //将元数据标记转换为其运行时表示形式，并将其推送到计算堆栈上。
操作码(CONV_U2, "conv.u2", 0xd1)    //将位于计算堆栈顶部的值转换为 unsigned int16，然后将其扩展为 int32。
操作码(CONV_U1, "conv.u1", 0xd2)    //将位于计算堆栈顶部的值转换为 unsigned int8，然后将其扩展为 int32。
操作码(CONV_I, "conv.i", 0xd3)      //将位于计算堆栈顶部的值转换为 native int。
操作码(CONV_OVF_I, "conv.ovf.i", 0xd4)//将位于计算堆栈顶部的有符号值转换为有符号 native int，并在溢出时引发 OverflowException。
操作码(CONV_OVF_U, "conv.ovf.u", 0xd5)//将位于计算堆栈顶部的有符号值转换为 unsigned native int，并在溢出时引发 OverflowException。
操作码(ADD_OVF, "add.ovf", 0xd6)    //
操作码(ADD_OVF_UN, "add.ovf.un", 0xd7)//
操作码(MUL_OVF, "mul.ovf", 0xd8)    //将两个整数值相乘，执行溢出检查，并将结果推送到计算堆栈上。
操作码(MUL_OVF_UN, "mul.ovf.un", 0xd9)//将两个无符号整数值相乘，执行溢出检查，并将结果推送到计算堆栈上。
操作码(SUB_OVF, "sub.ovf", 0xda)    //从另一值中减去一个整数值，执行溢出检查，并且将结果推送到计算堆栈上。
操作码(SUB_OVF_UN, "sub.ovf.un", 0xdb)//从另一值中减去一个无符号整数值，执行溢出检查，并且将结果推送到计算堆栈上。
操作码(ENDFINALLY, "endfinally", 0xdc)//将控制从异常块的 fault 或 finally 子句转移回公共语言结构 (CLI) 异常处理程序。
操作码(LEAVE, "leave", 0xdd)        //退出受保护的代码区域，无条件将控制转移到特定目标指令。
操作码(LEAVE_S, "leave.s", 0xde)    //退出受保护的代码区域，无条件将控制转移到目标指令（缩写形式）。
操作码(STIND_I, "stind.i", 0xdf)    //在所提供的地址存储 native int 类型的值。
操作码(CONV_U, "conv.u", 0xe0)      //将位于计算堆栈顶部的值转换为 unsigned native int，然后将其扩展为 native int。

/* 前缀说明。 我们使用> = 256的操作码来简化编码 */

操作码(ARGLIST, "arglist", 0x100)   //返回指向当前方法的参数列表的非托管指针。
操作码(CEQ, "ceq", 0x101)           //比较两个值。如果这两个值相等，则将整数值 1 (int32) 推送到计算堆栈上；否则，将 0 (int32) 推送到计算堆栈上。
操作码(CGT, "cgt", 0x102)           //比较两个值。如果第一个值大于第二个值，则将整数值 1 (int32) 推送到计算堆栈上；反之，将 0 (int32) 推送到计算堆栈上。
操作码(CGT_UN, "cgt.un", 0x103)     //比较两个无符号的或不可排序的值。如果第一个值大于第二个值，则将整数值 1 (int32) 推送到计算堆栈上；反之，将 0 (int32) 推送到计算堆栈上。
操作码(CLT, "clt", 0x104)           //比较两个值。如果第一个值小于第二个值，则将整数值 1 (int32) 推送到计算堆栈上；反之，将 0 (int32) 推送到计算堆栈上。
操作码(CLT_UN, "clt.un", 0x105)     //比较无符号的或不可排序的值 value1 和 value2。如果 value1 小于 value2，则将整数值 1 (int32 ) 推送到计算堆栈上；反之，将 0 ( int32 ) 推送到计算堆栈上。
操作码(LDFTN, "ldftn", 0x106)       //将指向实现特定方法的本机代码的非托管指针（native int 类型）推送到计算堆栈上。
操作码(LDVIRTFTN, "ldvirtftn", 0x107)//将指向实现与指定对象关联的特定虚方法的本机代码的非托管指针（native int 类型）推送到计算堆栈上。
操作码(JMPI, "jmpi", 0x108)         //
操作码(LDARG, "ldarg", 0x109)       //将参数（由指定索引值引用）加载到堆栈上。
操作码(LDARGA, "ldarga", 0x10a)     //将参数地址加载到计算堆栈上。
操作码(STARG, "starg", 0x10b)       //将位于计算堆栈顶部的值存储到位于指定索引的参数槽中。
操作码(LDLOC, "ldloc", 0x10c)       //将指定索引处的局部变量加载到计算堆栈上。
操作码(LDLOCA, "ldloca", 0x10d)     //将位于特定索引处的局部变量的地址加载到计算堆栈上。
操作码(STLOC, "stloc", 0x10e)       //从计算堆栈的顶部弹出当前值并将其存储到指定索引处的局部变量列表中。
操作码(LOCALLOC, "localloc", 0x10f) //从本地动态内存池分配特定数目的字节并将第一个分配的字节的地址（瞬态指针，* 类型）推送到计算堆栈上。
操作码(ENDFILTER, "endfilter", 0x111)//将控制从异常的 filter 子句转移回公共语言结构 (CLI) 异常处理程序。
操作码(UNALIGNED, "unaligned", 0x112)//指示当前位于计算堆栈上的地址可能没有与紧接的 ldind、stind、ldfld、stfld、ldobj、stobj、initblk 或 cpblk 指令的自然大小对齐。
操作码(VOLATILE, "volatile", 0x113) //指定当前位于计算堆栈顶部的地址可以是易失的，并且读取该位置的结果不能被缓存，或者对该地址的多个存储区不能被取消。
操作码(TAIL, "tail", 0x114)         //
操作码(INITOBJ, "initobj", 0x115)   //将位于指定地址的值类型的每个字段初始化为空引用或适当的基元类型的 0。
操作码(ANN_LIVE, "ann.live", 0x116) //
操作码(CPBLK, "cpblk", 0x117)       //将指定数目的字节从源地址复制到目标地址。
操作码(INITBLK, "initblk", 0x118)   //将位于特定地址的内存的指定块初始化为给定大小和初始值。
操作码(ANN_REF, "ann.ref", 0x119)   //
操作码(RETHROW, "rethrow", 0x11a)   //再次引发当前异常。
操作码(SIZEOF, "sizeof", 0x11c)     //将提供的值类型的大小（以字节为单位）推送到计算堆栈上。
操作码(REFANYTYPE, "refanytype", 0x11d)//检索嵌入在类型化引用内的类型标记。
操作码(ANN_DATA, "ann.data", 0x122) //
操作码(ANN_ARG, "ann.arg", 0x123)   //
