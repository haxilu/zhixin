/*************************************************************/
/*
 *  ARM dummy assembler for ZHI
 *
 */

#ifdef TARGET_DEFS_ONLY

#define 配置_ZHI_汇编
#define NB_ASM_REGS 16

静态_函数 void 生成(int c);
静态_函数 void 生成_le16(int c);
静态_函数 void 生成_le32(int c);

/*************************************************************/
#else
/*************************************************************/
#define 全局_使用
#include "zhi.h"

static void 汇编_error(void)
{
    错误_打印("未实施 ARM 汇编.");
}

/* XXX: make it faster ? */
静态_函数 void 生成(int c)
{
    int ind1;
    if (不需要_代码生成)
        return;
    ind1 = 输出代码索引 + 1;
    if (ind1 > 当前_生成代码_段->data_allocated)
        节_重新分配内存(当前_生成代码_段, ind1);
    当前_生成代码_段->data[输出代码索引] = c;
    输出代码索引 = ind1;
}

静态_函数 void 生成_le16 (int i)
{
    生成(i);
    生成(i>>8);
}

静态_函数 void 生成_le32 (int i)
{
    生成_le16(i);
    生成_le16(i>>16);
}

静态_函数 void 生成_32位表达式(表达式值 *pe)
{
    生成_le32(pe->v);
}

静态_函数 void 汇编_指令代码(知心状态机 *状态机1, int opcode)
{
    汇编_error();
}

静态_函数 void 替换_汇编_操作数(动态字符串 *add_str, 堆栈值 *sv, int modifier)
{
    汇编_error();
}

/* generate prolog and epilog code for asm statement */
静态_函数 void 汇编_生成_代码(汇编操作数 *operands, int 数量_operands,
                         int 数量_outputs, int is_output,
                         uint8_t *clobber_regs,
                         int out_reg)
{
}

静态_函数 void 汇编_计算_约束(汇编操作数 *operands,
                                    int 数量_operands, int 数量_outputs,
                                    const uint8_t *clobber_regs,
                                    int *pout_reg)
{
}

静态_函数 void 汇编_破坏者(uint8_t *clobber_regs, const char *str)
{
    汇编_error();
}

静态_函数 int 汇编_解析_注册变量 (int t)
{
    汇编_error();
    return -1;
}

/*************************************************************/
#endif /* ndef TARGET_DEFS_ONLY */
