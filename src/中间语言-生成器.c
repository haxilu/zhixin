/*
 *  ZHI的CIL(Common Intermediate Language:通用中间语言)代码生成器
 */

#error 自2003年以来，此代码已被破坏

/* 可用寄存器数 */
#define 可用_寄存器数             3

/* 一个寄存器可以属于几个类。 这些类必须从更一般的类到更精确的类进行排序（请参见gv2（）代码进行假设）。 */
#define 寄存器类_堆栈     0x0001  /* 任何堆栈条目 */
#define 寄存器类_堆栈0     0x0002  /* 堆栈顶部 */
#define 寄存器类_堆栈1     0x0004  /* top - 1 */

#define 寄存器类_整数     寄存器类_堆栈
#define 寄存器类_浮点   寄存器类_堆栈
#define 寄存器类_返回整数寄存器    寄存器类_堆栈0 /* 函数返回：整数寄存器 */
#define 寄存器类_返回长整数寄存器    寄存器类_堆栈0 /* 函数返回：第二个整数寄存器*/
#define 寄存器类_返回浮点寄存器    寄存器类_堆栈0 /* 函数返回：浮点寄存器 */

/* 寄存器的漂亮名字 */
enum {
    寄存器_堆栈0 = 0,
    寄存器_堆栈1,
    寄存器_堆栈2,
};

const int 寄存器_类数[可用_寄存器数] = {
    /* ST0 */ 寄存器类_堆栈| 寄存器类_堆栈0,
    /* ST1 */ 寄存器类_堆栈| 寄存器类_堆栈1,
    /* ST2 */ 寄存器类_堆栈,
};

/* 返回功能寄存器 */
#define 寄存器_返回16位整数寄存器 寄存器_堆栈0 /* 单字int返回寄存器 */
#define 寄存器_返回长整数寄存器 寄存器_堆栈0 /* 第二个字返回寄存器（很长很长） */
#define 寄存器_返回浮点寄存器 寄存器_堆栈0 /* 浮动返回寄存器 */

/* 定义是否必须以相反顺序求值函数参数 */
/* #define 相反顺序_函数_参数 */

/* 定义是否将结构作为指针传递。 否则，结构将直接压入堆栈。 */
/* #define 函数_STRUCT_PARAM_AS_PTR */

/* 指针大小，以字节为单位 */
#define 指针_大小 4

/* 长整型和对齐（以字节为单位） */
#define 长双精度_大小  8
#define 长双精度_对齐 8

/* 函数调用上下文 */
typedef struct 调用函数上下文 {
    int 函数_调用; /* 函数调用类型（函数_标准调用或函数_CDECL） */
} 调用函数上下文;

/******************************************************/
/* 操作码定义 */

#define 中间语言_操作码_前缀 0xFE

enum 中间语言操作代码 {
#define 操作码(名称, 字符串, 编号) 中间语言_操作码_ ## name = n,
#include "中间语言-操作码.h"
#undef 操作码
};

char *中间语言_操作码_字符串[] = {
#define 操作码(名称, 字符串, 编号) [n] = str,
#include "中间语言-操作码.h"
#undef 操作码
};

/******************************************************/

/* 参数变量编号从此处开始 */
#define 参数变量_编号 0x70000000

static FILE *中间语言_输出文件;

static void 输出_字节(int c)
{
    *(char *)输出代码索引++ = c;
}

static void 输出_le32(int c)
{
    输出_字节(c);
    输出_字节(c >> 8);
    输出_字节(c >> 16);
    输出_字节(c >> 24);
}

static void 初始化_输出文件(void)
{
    if (!中间语言_输出文件) {
        中间语言_输出文件 = stdout;
        fprintf(中间语言_输出文件,
                ".汇编 外部 mscorlib\n"
                "{\n"
                ".ver 1:0:2411:0\n"
                "}\n\n");
    }
}

static void 输出_操作码1(int op)
{
    if (op & 0x100)
        输出_字节(中间语言_操作码_前缀);
    输出_字节(op & 0xff);
}

/* 输出带有前缀的操作码 */
static void 输出_操作码(int op)
{
    输出_操作码1(op);
    fprintf(中间语言_输出文件, " %s\n", 中间语言_操作码_字符串[op]);
}

static void 输出_操作码字节(int op, int c)
{
    输出_操作码1(op);
    输出_字节(c);
    fprintf(中间语言_输出文件, " %s %d\n", 中间语言_操作码_字符串[op], c);
}

static void 输出_操作码整数(int op, int c)
{
    输出_操作码1(op);
    输出_le32(c);
    fprintf(中间语言_输出文件, " %s 0x%x\n", 中间语言_操作码_字符串[op], c);
}

/* XXX: 不完整 */
static void 中间语言_类型_的_字符串(char *buf, int buf_size,
                           int t, const char *varstr)
{
    int bt;
    符号 *s, *sa;
    char buf1[256];
    const char *tstr;

    t = t & VT_类型;
    bt = t & VT_基本类型;
    buf[0] = '\0';
    if (t & VT_无符号)
        连接_字符串(buf, buf_size, "unsigned ");
    switch(bt) {
    case VT_无类型:
        tstr = "void";
        goto add_tstr;
    case VT_逻辑:
        tstr = "bool";
        goto add_tstr;
    case VT_字节:
        tstr = "int8";
        goto add_tstr;
    case VT_短整数:
        tstr = "int16";
        goto add_tstr;
    case VT_枚举:
    case VT_整数:
    case VT_长整数:
        tstr = "int32";
        goto add_tstr;
    case VT_长长整数:
        tstr = "int64";
        goto add_tstr;
    case VT_浮点:
        tstr = "float32";
        goto add_tstr;
    case VT_双精度:
    case VT_长双精度:
        tstr = "float64";
    add_tstr:
        连接_字符串(buf, buf_size, tstr);
        break;
    case VT_结构体:
        错误_打印("尚未处理的结构");
        break;
    case VT_函数:
        s = 符号_查询((unsigned)t >> VT_结构体_转换);
        中间语言_类型_的_字符串(buf, buf_size, s->t, varstr);
        连接_字符串(buf, buf_size, "(");
        sa = s->next;
        while (sa != NULL) {
            中间语言_类型_的_字符串(buf1, sizeof(buf1), sa->t, NULL);
            连接_字符串(buf, buf_size, buf1);
            sa = sa->next;
            if (sa)
                连接_字符串(buf, buf_size, ", ");
        }
        连接_字符串(buf, buf_size, ")");
        goto no_var;
    case VT_指针:
        s = 符号_查询((unsigned)t >> VT_结构体_转换);
        p字符串复制(buf1, sizeof(buf1), "*");
        if (varstr)
            连接_字符串(buf1, sizeof(buf1), varstr);
        中间语言_类型_的_字符串(buf, buf_size, s->t, buf1);
        goto no_var;
    }
    if (varstr) {
        连接_字符串(buf, buf_size, " ");
        连接_字符串(buf, buf_size, varstr);
    }
 no_var: ;
}


/* 值为“ val”的修补程序重定位条目 */
void greloc_patch1(Reloc *p, int val)
{
}

/* 输出一个符号并修补对它的所有调用 */
void 生成符号_地址(t, a)
{
}

/* 输出跳转和返回符号 */
static int 输出_跳转和操作码符号(int op, int c)
{
    输出_操作码1(op);
    输出_le32(0);
    if (c == 0) {
        c = 输出代码索引 - (int)当前_生成代码_段->data;
    }
    fprintf(中间语言_输出文件, " %s L%d\n", 中间语言_操作码_字符串[op], c);
    return c;
}

void 生成符号(int t)
{
    fprintf(中间语言_输出文件, "L%d:\n", t);
}

/* 从值“ sv”加载“ r” */
void 加载(int r, 堆栈值 *sv)
{
    int v, fc, ft;

    v = sv->r & VT_值掩码;
    fc = sv->c.i;
    ft = sv->t;

    if (sv->r & VT_LVAL) {
        if (v == VT_LOCAL) {
            if (fc >= 参数变量_编号) {
                fc -= 参数变量_编号;
                if (fc >= 0 && fc <= 4) {
                    输出_操作码(中间语言_操作码_LDARG_0 + fc);
                } else if (fc <= 0xff) {
                    输出_操作码字节(中间语言_操作码_LDARG_S, fc);
                } else {
                    输出_操作码整数(中间语言_操作码_LDARG, fc);
                }
            } else {
                if (fc >= 0 && fc <= 4) {
                    输出_操作码(中间语言_操作码_LDLOC_0 + fc);
                } else if (fc <= 0xff) {
                    输出_操作码字节(中间语言_操作码_LDLOC_S, fc);
                } else {
                    输出_操作码整数(中间语言_操作码_LDLOC, fc);
                }
            }
        } else if (v == VT_VC常量) {
                /* XXX: handle globals */
                输出_操作码整数(中间语言_操作码_LDSFLD, 0);
        } else {
            if ((ft & VT_基本类型) == VT_浮点) {
                输出_操作码(中间语言_操作码_LDIND_R4);
            } else if ((ft & VT_基本类型) == VT_双精度) {
                输出_操作码(中间语言_操作码_LDIND_R8);
            } else if ((ft & VT_基本类型) == VT_长双精度) {
                输出_操作码(中间语言_操作码_LDIND_R8);
            } else if ((ft & VT_类型) == VT_字节)
                输出_操作码(中间语言_操作码_LDIND_I1);
            else if ((ft & VT_类型) == (VT_字节 | VT_无符号))
                输出_操作码(中间语言_操作码_LDIND_U1);
            else if ((ft & VT_类型) == VT_短整数)
                输出_操作码(中间语言_操作码_LDIND_I2);
            else if ((ft & VT_类型) == (VT_短整数 | VT_无符号))
                输出_操作码(中间语言_操作码_LDIND_U2);
            else
                输出_操作码(中间语言_操作码_LDIND_I4);
        } 
    } else {
        if (v == VT_VC常量) {
            /* XXX: handle globals */
            if (fc >= -1 && fc <= 8) {
                输出_操作码(中间语言_操作码_LDC_I4_M1 + fc + 1); 
            } else {
                输出_操作码整数(中间语言_操作码_LDC_I4, fc);
            }
        } else if (v == VT_LOCAL) {
            if (fc >= 参数变量_编号) {
                fc -= 参数变量_编号;
                if (fc <= 0xff) {
                    输出_操作码字节(中间语言_操作码_LDARGA_S, fc);
                } else {
                    输出_操作码整数(中间语言_操作码_LDARGA, fc);
                }
            } else {
                if (fc <= 0xff) {
                    输出_操作码字节(中间语言_操作码_LDLOCA_S, fc);
                } else {
                    输出_操作码整数(中间语言_操作码_LDLOCA, fc);
                }
            }
        } else {
            /* XXX: do it */
        }
    }
}

/* 将寄存器“ r”存储在左值“ v”中 */
void 存储(int r, 堆栈值 *sv)
{
    int v, fc, ft;

    v = sv->r & VT_值掩码;
    fc = sv->c.i;
    ft = sv->t;
    if (v == VT_LOCAL) {
        if (fc >= 参数变量_编号) {
            fc -= 参数变量_编号;
            /* XXX: check IL arg 存储 semantics */
            if (fc <= 0xff) {
                输出_操作码字节(中间语言_操作码_STARG_S, fc);
            } else {
                输出_操作码整数(中间语言_操作码_STARG, fc);
            }
        } else {
            if (fc >= 0 && fc <= 4) {
                输出_操作码(中间语言_操作码_STLOC_0 + fc);
            } else if (fc <= 0xff) {
                输出_操作码字节(中间语言_操作码_STLOC_S, fc);
            } else {
                输出_操作码整数(中间语言_操作码_STLOC, fc);
            }
        }
    } else if (v == VT_VC常量) {
        /* XXX: handle globals */
        输出_操作码整数(中间语言_操作码_STSFLD, 0);
    } else {
        if ((ft & VT_基本类型) == VT_浮点)
            输出_操作码(中间语言_操作码_STIND_R4);
        else if ((ft & VT_基本类型) == VT_双精度)
            输出_操作码(中间语言_操作码_STIND_R8);
        else if ((ft & VT_基本类型) == VT_长双精度)
            输出_操作码(中间语言_操作码_STIND_R8);
        else if ((ft & VT_基本类型) == VT_字节)
            输出_操作码(中间语言_操作码_STIND_I1);
        else if ((ft & VT_基本类型) == VT_短整数)
            输出_操作码(中间语言_操作码_STIND_I2);
        else
            输出_操作码(中间语言_操作码_STIND_I4);
    }
}

/* 开始函数调用和返回函数调用上下文 */
void 开始调用函数_返回函数调用上下文(调用函数上下文 *c, int 函数_调用)
{
    c->函数_调用 = 函数_调用;
}

/* 在（栈顶值-> t，栈顶值-> c）中的push函数参数。 然后弹出堆栈条目. */
void 开始调用函数_参数(调用函数上下文 *c)
{
    if ((栈顶值->t & VT_基本类型) == VT_结构体) {
        错误_打印("作为值传递的结构尚未处理");
    } else {
        /* simply push on stack */
        将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
    }
    栈顶值--;
}

/* 在（栈顶值-> t，栈顶值-> c）和自由函数上下文中生成具有地址的函数调用。 弹出堆栈条目 */
void 具体地址函数_调用(调用函数上下文 *c)
{
    char buf[1024];

    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
        /* XXX: zhi需要更多信息 */
        中间语言_类型_的_字符串(buf, sizeof(buf), 栈顶值->t, "xxx");
        fprintf(中间语言_输出文件, " call %s\n", buf);
    } else {
        /* 间接的ect call */
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
        中间语言_类型_的_字符串(buf, sizeof(buf), 栈顶值->t, NULL);
        fprintf(中间语言_输出文件, " calli %s\n", buf);
    }
    栈顶值--;
}

/* 生成类型为“ t”的函数序言 */
void 生成函数_序言(int t)
{
    int addr, u, 函数_调用;
    符号 *sym;
    char buf[1024];

    初始化_输出文件();

    /* XXX: pass function name to 生成函数_序言 */
    中间语言_类型_的_字符串(buf, sizeof(buf), t, 函数名称);
    fprintf(中间语言_输出文件, ".method static %s il managed\n", buf);
    fprintf(中间语言_输出文件, "{\n");
    /* XXX: cannot do better now */
    fprintf(中间语言_输出文件, " .maxstack %d\n", 可用_寄存器数);
    fprintf(中间语言_输出文件, " .locals (int32, int32, int32, int32, int32, int32, int32, int32)\n");
    
    if (!strcmp(函数名称, "main")||!strcmp(函数名称, "主函数"))
        fprintf(中间语言_输出文件, " .entrypoint\n");
        
    sym = 符号_查询((unsigned)t >> VT_结构体_转换);
    函数_调用 = sym->r;

    addr = 参数变量_编号;
    /* 如果函数返回结构，则添加一个隐式指针参数 */
    当前函数_返回类型 = sym->t;
    当前函数_可变参数 = (sym->c == 函数_省略);
    if ((当前函数_返回类型 & VT_基本类型) == VT_结构体) {
        函数_vc = addr;
        addr++;
    }
    /* 定义参数 */
    while ((sym = sym->next) != NULL) {
        u = sym->t;
        符号_压入栈(sym->v & ~符号_字段, u,
                 VT_LOCAL | lvalue_type(sym->type.t), addr);
        addr++;
    }
}

/* 生成函数结尾 */
void 生成函数_结尾(void)
{
    输出_操作码(中间语言_操作码_RET);
    fprintf(中间语言_输出文件, "}\n\n");
}

/* 生成跳转到标签 */
int 生成跳转到标签(int t)
{
    return 输出_跳转和操作码符号(中间语言_操作码_BR, t);
}

/* 生成跳转到固定地址 */
void 生成跳转到_固定地址(int a)
{
    /* XXX: 处理符号 */
    输出_操作码整数(中间语言_操作码_BR, a);
}

/* 生成测试。 将“ inv”设置为反转测试。 弹出堆栈条目 */
int 生成测试(int inv, int t)
{
    int v, *p, c;

    v = 栈顶值->r & VT_值掩码;
    if (v == VT_CMP) {
        c = 栈顶值->c.i ^ inv;
        switch(c) {
        case 双符号_等于:
            c = 中间语言_操作码_BEQ;
            break;
        case 双符号_不等于:
            c = 中间语言_操作码_BNE_UN;
            break;
        case 符_LT:
            c = 中间语言_操作码_BLT;
            break;
        case 双符号_小于等于:
            c = 中间语言_操作码_BLE;
            break;
        case 符_GT:
            c = 中间语言_操作码_BGT;
            break;
        case 双符号_大于等于:
            c = 中间语言_操作码_BGE;
            break;
        case 符号_ULT:
            c = 中间语言_操作码_BLT_UN;
            break;
        case 符号_ULE:
            c = 中间语言_操作码_BLE_UN;
            break;
        case 符_UGT:
            c = 中间语言_操作码_BGT_UN;
            break;
        case 符号_UGE:
            c = 中间语言_操作码_BGE_UN;
            break;
        }
        t = 输出_跳转和操作码符号(c, t);
    } else if (v == VT_JMP || v == VT_JMPI) {
        /* && or || optimization */
        if ((v & 1) == inv) {
            /* insert 栈顶值->c jump list in t */
            p = &栈顶值->c.i;
            while (*p != 0)
                p = (int *)*p;
            *p = t;
            t = 栈顶值->c.i;
        } else {
            t = 生成跳转到标签(t);
            生成符号(栈顶值->c.i);
        }
    }
    栈顶值--;
    return t;
}

/* 生成整数二进制运算 */
void 生成_整数二进制运算(int op)
{
    将rc寄存器值存储在栈顶值中2(寄存器类_堆栈1, 寄存器类_堆栈0);
    switch(op) {
    case '+':
        输出_操作码(中间语言_操作码_ADD);
        goto std_op;
    case '-':
        输出_操作码(中间语言_操作码_SUB);
        goto std_op;
    case '&':
        输出_操作码(中间语言_操作码_AND);
        goto std_op;
    case '^':
        输出_操作码(中间语言_操作码_XOR);
        goto std_op;
    case '|':
        输出_操作码(中间语言_操作码_OR);
        goto std_op;
    case '*':
        输出_操作码(中间语言_操作码_MUL);
        goto std_op;
    case 双符号_左位移:
        输出_操作码(中间语言_操作码_SHL);
        goto std_op;
    case 符_SHR:
        输出_操作码(中间语言_操作码_SHR_UN);
        goto std_op;
    case 双符号_右位移:
        输出_操作码(中间语言_操作码_SHR);
        goto std_op;
    case '/':
    case 符_指针除法:
        输出_操作码(中间语言_操作码_DIV);
        goto std_op;
    case 符_无符除法:
        输出_操作码(中间语言_操作码_DIV_UN);
        goto std_op;
    case '%':
        输出_操作码(中间语言_操作码_REM);
        goto std_op;
    case 符_无符取模运算:
        输出_操作码(中间语言_操作码_REM_UN);
    std_op:
        栈顶值--;
        栈顶值[0].r = 寄存器_堆栈0;
        break;
    case 双符号_等于:
    case 双符号_不等于:
    case 符_LT:
    case 双符号_小于等于:
    case 符_GT:
    case 双符号_大于等于:
    case 符号_ULT:
    case 符号_ULE:
    case 符_UGT:
    case 符号_UGE:
        栈顶值--;
        栈顶值[0].r = VT_CMP;
        栈顶值[0].c.i = op;
        break;
    }
}

/* 生成浮点运算“ v = t1 op t2”指令。 保证两个操作数具有相同的浮点类型 */
void 生成_浮点运算(int op)
{
    /* same as integer */
    生成_整数二进制运算(op);
}

/* 将整数转换为fp't'类型。 必须处理'int'，'unsigned int'和'long long'情况。 */
void 生成_整数转换为浮点(int t)
{
    将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
    if (t == VT_浮点)
        输出_操作码(中间语言_操作码_CONV_R4);
    else
        输出_操作码(中间语言_操作码_CONV_R8);
}

/* 将fp转换为int't'类型 */
/* XXX: handle long long case */
void 生成_浮点转换为整数(int t)
{
    将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
    switch(t) {
    case VT_整数 | VT_无符号:
        输出_操作码(中间语言_操作码_CONV_U4);
        break;
    case VT_长长整数:
        输出_操作码(中间语言_操作码_CONV_I8);
        break;
    case VT_长长整数 | VT_无符号:
        输出_操作码(中间语言_操作码_CONV_U8);
        break;
    default:
        输出_操作码(中间语言_操作码_CONV_I4);
        break;
    }
}

/* 从一种浮点类型转换为另一种 */
void 生成_浮点转换为另一种浮点(int t)
{
    将rc寄存器值存储在栈顶值中(寄存器类_堆栈0);
    if (t == VT_浮点) {
        输出_操作码(中间语言_操作码_CONV_R4);
    } else {
        输出_操作码(中间语言_操作码_CONV_R8);
    }
}

/* CIL代码生成器的结尾 */
/*************************************************************/

