#ifdef TARGET_DEFS_ONLY

#define EM_ZHI_TARGET EM_X86_64

/* 32位数据重定位的重定位类型 */
#define R_DATA_32   R_X86_64_32S
#define R_DATA_PTR  R_X86_64_64
#define R_JMP_SLOT  R_X86_64_JUMP_SLOT
#define R_GLOB_DAT  R_X86_64_GLOB_DAT
#define R_COPY      R_X86_64_COPY
#define R_RELATIVE  R_X86_64_RELATIVE

#define R_NUM       R_X86_64_NUM

#define ELF_START_ADDR 0x400000
#define ELF_PAGE_SIZE  0x200000

#define PCRELATIVE_DLLPLT 1
#define RELOCATE_DLLPLT 1

#else /* !TARGET_DEFS_ONLY */

#include "zhi.h"

#if !defined(ELF_OBJ_ONLY) || defined(ZHI_TARGET_MACHO)
/* 返回1表示代码重定位，0表示数据重定位。对于未知的重新定位，返回-1。 */
int 代码_重定位 (int reloc_type)
{
    switch (reloc_type) {
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
        case R_X86_64_GOTPC32:
        case R_X86_64_GOTPC64:
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_REX_GOTPCRELX:
        case R_X86_64_GOTTPOFF:
        case R_X86_64_GOT32:
        case R_X86_64_GOT64:
        case R_X86_64_GLOB_DAT:
        case R_X86_64_COPY:
        case R_X86_64_RELATIVE:
        case R_X86_64_GOTOFF64:
            return 0;

        case R_X86_64_PC32:
        case R_X86_64_PC64:
        case R_X86_64_PLT32:
        case R_X86_64_PLTOFF64:
        case R_X86_64_JUMP_SLOT:
            return 1;
    }
    return -1;
}

/* 返回一个枚举数，以描述重新定位是否以及何时需要创建GET与/或PLT条目。有关不同值的描述，请参见zhi.h。 */
int 获取plt_入口_类型 (int reloc_type)
{
    switch (reloc_type) {
        case R_X86_64_GLOB_DAT:
        case R_X86_64_JUMP_SLOT:
        case R_X86_64_COPY:
        case R_X86_64_RELATIVE:
            return NO_GOTPLT_ENTRY;

	/* 下面的relocs通常不需要GOT或PLT插槽，但是为了简化链接编辑器部分，我们需要它们。请向我们的来电者咨询。  */
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
        case R_X86_64_PC32:
        case R_X86_64_PC64:
            return AUTO_GOTPLT_ENTRY;

        case R_X86_64_GOTTPOFF:
            return BUILD_GOT_ONLY;

        case R_X86_64_GOT32:
        case R_X86_64_GOT64:
        case R_X86_64_GOTPC32:
        case R_X86_64_GOTPC64:
        case R_X86_64_GOTOFF64:
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_REX_GOTPCRELX:
        case R_X86_64_PLT32:
        case R_X86_64_PLTOFF64:
            return ALWAYS_GOTPLT_ENTRY;
    }

    return -1;
}

静态_函数 unsigned 创建_plt_入口(知心状态机 *状态机1, unsigned got_offset, struct 额外_符号_属性 *attr)
{
    段 *plt = 状态机1->plt;
    uint8_t *p;
    int modrm;
    unsigned plt_offset, relofs;

    modrm = 0x25;

    /* empty PLT: create PLT0 entry that pushes the library identifier
       (GOT + 指针_大小) and jumps to ld.so resolution routine
       (GOT + 2 * 指针_大小) */
    if (plt->数据_偏移 == 0) {
        p = 段_ptr_添加(plt, 16);
        p[0] = 0xff; /* pushl got + 指针_大小 */
        p[1] = modrm + 0x10;
        写32le(p + 2, 指针_大小);
        p[6] = 0xff; /* jmp *(got + 指针_大小 * 2) */
        p[7] = modrm;
        写32le(p + 8, 指针_大小 * 2);
    }
    plt_offset = plt->数据_偏移;

    /* The PLT slot refers to the relocation entry it needs via offset.
       The 重定位 entry is created below, so its offset is the current
       数据_偏移 */
    relofs = 状态机1->got->重定位 ? 状态机1->got->重定位->数据_偏移 : 0;

    /* Jump to GOT entry where ld.so initially put the address of ip + 4 */
    p = 段_ptr_添加(plt, 16);
    p[0] = 0xff; /* jmp *(got + x) */
    p[1] = modrm;
    写32le(p + 2, got_offset);
    p[6] = 0x68; /* push $xxx */
    /* On x86-64, the relocation is referred to by _index_ */
    写32le(p + 7, relofs / sizeof (ElfW_Rel));
    p[11] = 0xe9; /* jmp plt_start */
    写32le(p + 12, -(plt->数据_偏移));
    return plt_offset;
}

/* 重定位 the PLT: compute addresses and offsets in the PLT now that final
   address for PLT and GOT are known (see fill_program_header) */
静态_函数 void 重定位_plt(知心状态机 *状态机1)
{
    uint8_t *p, *p_end;

    if (!状态机1->plt)
      return;

    p = 状态机1->plt->data;
    p_end = p + 状态机1->plt->数据_偏移;

    if (p < p_end) {
        int x = 状态机1->got->sh_addr - 状态机1->plt->sh_addr - 6;
        增加32le(p + 2, x);
        增加32le(p + 8, x - 6);
        p += 16;
        while (p < p_end) {
            增加32le(p + 2, x + (状态机1->plt->data - p));
            p += 16;
        }
    }
}
#endif

void 重定位(知心状态机 *状态机1, ElfW_Rel *rel, int type, unsigned char *ptr, 目标地址_类型 addr, 目标地址_类型 val)
{
    int sym_index, esym_index;

    sym_index = ELFW(R_SYM)(rel->r_info);

    switch (type) {
        case R_X86_64_64:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                esym_index = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
                qrel->r_offset = rel->r_offset;
                if (esym_index) {
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_64);
                    qrel->r_addend = rel->r_addend;
                    qrel++;
                    break;
                } else {
                    qrel->r_info = ELFW(R_INFO)(0, R_X86_64_RELATIVE);
                    qrel->r_addend = 读64le(ptr) + val;
                    qrel++;
                }
            }
            增加64le(ptr, val);
            break;
        case R_X86_64_32:
        case R_X86_64_32S:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                /* XXX: this logic may depend on ZHI's codegen
                   now ZHI uses R_X86_64_32 even for a 64bit pointer */
                qrel->r_offset = rel->r_offset;
                qrel->r_info = ELFW(R_INFO)(0, R_X86_64_RELATIVE);
                /* Use sign extension! */
                qrel->r_addend = (int)读32le(ptr) + val;
                qrel++;
            }
            增加32le(ptr, val);
            break;

        case R_X86_64_PC32:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                /* DLL relocation */
                esym_index = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_PC32);
                    /* Use sign extension! */
                    qrel->r_addend = (int)读32le(ptr) + rel->r_addend;
                    qrel++;
                    break;
                }
            }
            goto plt32pc32;

        case R_X86_64_PLT32:
            /* fallthrough: val already holds the PLT slot address */

        plt32pc32:
        {
            long long diff;
            diff = (long long)val - addr;
            if (diff < -2147483648LL || diff > 2147483647LL) {
                错误_打印("内部错误: 重新定位失败");
            }
            增加32le(ptr, diff);
        }
            break;

        case R_X86_64_PLTOFF64:
            增加64le(ptr, val - 状态机1->got->sh_addr + rel->r_addend);
            break;

        case R_X86_64_PC64:
            if (状态机1->输出_类型 == ZHI_输出_DLL) {
                /* DLL relocation */
                esym_index = get_额外_符号_属性(状态机1, sym_index, 0)->dyn_index;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_PC64);
                    qrel->r_addend = 读64le(ptr) + rel->r_addend;
                    qrel++;
                    break;
                }
            }
            增加64le(ptr, val - addr);
            break;

        case R_X86_64_GLOB_DAT:
        case R_X86_64_JUMP_SLOT:
            /* They don't need addend */
            写64le(ptr, val - rel->r_addend);
            break;
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_REX_GOTPCRELX:
            增加32le(ptr, 状态机1->got->sh_addr - addr +
                         get_额外_符号_属性(状态机1, sym_index, 0)->got_offset - 4);
            break;
        case R_X86_64_GOTPC32:
            增加32le(ptr, 状态机1->got->sh_addr - addr + rel->r_addend);
            break;
        case R_X86_64_GOTPC64:
            增加64le(ptr, 状态机1->got->sh_addr - addr + rel->r_addend);
            break;
        case R_X86_64_GOTTPOFF:
            增加32le(ptr, val - 状态机1->got->sh_addr);
            break;
        case R_X86_64_GOT32:
            /* we 加载 the got offset */
            增加32le(ptr, get_额外_符号_属性(状态机1, sym_index, 0)->got_offset);
            break;
        case R_X86_64_GOT64:
            /* we 加载 the got offset */
            增加64le(ptr, get_额外_符号_属性(状态机1, sym_index, 0)->got_offset);
            break;
        case R_X86_64_GOTOFF64:
            增加64le(ptr, val - 状态机1->got->sh_addr);
            break;
        case R_X86_64_RELATIVE:
#ifdef ZHI_TARGET_PE
            增加32le(ptr, val - 状态机1->pe_图像库);
#endif
            /* 什么都不做 */
            break;
    }
}

#endif /* !TARGET_DEFS_ONLY */
