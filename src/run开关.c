/*
 *  支持-run开关
 */

#include "zhi.h"

/* 仅本机编译器支持-run */
#ifdef ZHI_是_本机

#ifdef ZHI_配置_记录回滚
typedef struct 运行时_上下文
{
    /* --> ELF文件处理.c:zhi_add_btstub wants those below in that order: */
    单词表_符号 *stab_sym, *单词表_符号_结束;
    char *单词表_字符串;
    ElfW(符号) *elf符号_开始, *elf符号_结束;
    char *elf_字符串;
    目标地址_类型 prog_base;
    void *bounds_start;
    struct 运行时_上下文 *next;
    /* <-- */
    int num_callers;
    目标地址_类型 ip, fp, sp;
    void *top_func;
    jmp_buf jmp_buf;
    char do_jmp;
} 运行时_上下文;

static 运行时_上下文 g_rtctxt;
static void 设置_异常_处理程序(void);
static int _rt_错误(void *fp, void *ip, const char *fmt, va_list ap);
static void 运行时_退出(int code);
#endif /* ZHI_配置_记录回滚 */

/* 从lib / bt-exe.c包含时定义 */
#ifndef 仅仅_配置_ZHI_回滚

#ifndef _WIN32
# include <sys/mman.h>
#endif

static void 设置_可执行_页面(知心状态机 *状态机1, void *ptr, unsigned long length);
static int zhi_重新定位_代码(知心状态机 *状态机1, void *ptr, 目标地址_类型 ptr_diff);

#ifdef _WIN64
static void *win64_添加_函数_表(知心状态机 *状态机1);
static void win64_删除_函数_表(void *);
#endif

/* ------------------------------------------------------------- */
/* 进行所有重定位（在使用zhi_get_symbol（）之前需要）在发生错误时返回-1 */

HEXINKU接口 int 执行所有重定位(知心状态机 *状态机1, void *ptr)
{
    int size;
    目标地址_类型 ptr_diff = 0;

    if (ZHI_自动_重新排列 != ptr)
        return zhi_重新定位_代码(状态机1, ptr, 0);

    size = zhi_重新定位_代码(状态机1, NULL, 0);
    if (size < 0)
        return -1;

#ifdef HAVE_SELINUX
{
    /* 使用mmap代替malloc */
    void *prx;
    char tmpfname[] = "/tmp/.zhirunXXXXXX";
    int fd = mkstemp(tmpfname);
    unlink(tmpfname);
    ftruncate(fd, size);

    ptr = mmap (NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    prx = mmap (NULL, size, PROT_READ|PROT_EXEC, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED || prx == MAP_FAILED)
	错误_打印("zhirun: 无法映射内存");
    动态数组_追加元素(&状态机1->运行时_内存, &状态机1->数量_运行时_内存, (void*)(目标地址_类型)size);
    动态数组_追加元素(&状态机1->运行时_内存, &状态机1->数量_运行时_内存, prx);
    ptr_diff = (char*)prx - (char*)ptr;
    close(fd);
}
#else
    ptr = 内存_申请(size);
#endif
    zhi_重新定位_代码(状态机1, ptr, ptr_diff); /* 没有更多的错误预期 */
    动态数组_追加元素(&状态机1->运行时_内存, &状态机1->数量_运行时_内存, ptr);
    return 0;
}

静态_函数 void 释放运行时内存(知心状态机 *状态机1)
{
    int i;

    for (i = 0; i < 状态机1->数量_运行时_内存; ++i) {
#ifdef HAVE_SELINUX
        unsigned size = (unsigned)(目标地址_类型)状态机1->运行时_内存[i++];
        munmap(状态机1->运行时_内存[i++], size);
        munmap(状态机1->运行时_内存[i], size);
#else
#ifdef _WIN64
        win64_删除_函数_表(*(void**)状态机1->运行时_内存[i]);
#endif
        内存_释放(状态机1->运行时_内存[i]);
#endif
    }
    内存_释放(状态机1->运行时_内存);
}

static void 运行_光盘(知心状态机 *状态机1, const char *start, const char *end)
{
    void **a = (void **)获取_符号_地址(状态机1, start, 0, 0);
    void **b = (void **)获取_符号_地址(状态机1, end, 0, 0);
    while (a != b)
        ((void(*)(void))*a++)();
}

/* 使用给定的参数启动编译的程序 */
HEXINKU接口 int ZHI_运行(知心状态机 *状态机1, int 参数数量, char **参数数组)
{
    int (*prog_main)(int, char **), ret;
#ifdef ZHI_配置_记录回滚
    运行时_上下文 *rc = &g_rtctxt;
#endif
    状态机1->运行时_入口 = 状态机1->不添加标准库 ? "_start" : "主函数";/* "main"主函数 */
    if ((状态机1->DX标号 & 16) && !查找_常量_符号(状态机1, 状态机1->运行时_入口))
        return 0;
#ifdef ZHI_配置_记录回滚
    if (状态机1->执行_调试)
        在已编译的程序中添加符号(状态机1, "_exit" + !状态机1->前导_下划线, 运行时_退出);
#endif
    if (执行所有重定位(状态机1, ZHI_自动_重新排列) < 0)
        return -1;
    prog_main = zhi_获取_符号_错误(状态机1, 状态机1->运行时_入口);

#ifdef ZHI_配置_记录回滚
    memset(rc, 0, sizeof *rc);
    if (状态机1->执行_调试) {
        void *p;
        rc->stab_sym = (单词表_符号 *)符号调试_部分->data;
        rc->单词表_符号_结束 = (单词表_符号 *)(符号调试_部分->data + 符号调试_部分->数据_偏移);
        rc->单词表_字符串 = (char *)符号调试_部分->link->data;
        rc->elf符号_开始 = (ElfW(符号) *)(单词表_部分->data);
        rc->elf符号_结束 = (ElfW(符号) *)(单词表_部分->data + 单词表_部分->数据_偏移);
        rc->elf_字符串 = (char *)单词表_部分->link->data;
#if 指针_大小 == 8
        rc->prog_base = 生成代码_段->sh_addr & 0xffffffff00000000ULL;
#endif
        rc->top_func = 获取elf符号值(状态机1, "主函数");/* "main"主函数 */
        rc->num_callers = 状态机1->运行时_num_callers;
        rc->do_jmp = 1;
        if ((p = 获取elf符号值(状态机1, "__rt_错误")))
            *(void**)p = _rt_错误;
#ifdef 配置_ZHI_边界检查
        if (状态机1->执行_边界_检查器) {
            if ((p = 获取elf符号值(状态机1, "__bound_init")))
                ((void(*)(void*, int))p)(全局边界_部分->data, 1);
        }
#endif
        设置_异常_处理程序();
    }
#endif

    errno = 0; /* 清除errno值 */
    fflush(stdout);
    fflush(stderr);
    /* 这些不是C符号，因此不需要前导下划线处理.  */
    运行_光盘(状态机1, "__init_array_start", "__init_array_end");
#ifdef ZHI_配置_记录回滚
    if (!rc->do_jmp || !(ret = setjmp(rc->jmp_buf)))
#endif
    {
        ret = prog_main(参数数量, 参数数组);
    }
    运行_光盘(状态机1, "__fini_array_start", "__fini_array_end");
    if ((状态机1->DX标号 & 16) && ret)
        fprintf(状态机1->预处理输出文件, "[returns %d]\n", ret), fflush(状态机1->预处理输出文件);
    return ret;
}

#if defined ZHI_TARGET_I386 || defined ZHI_TARGET_X86_64
/* 为避免x86处理器每次在附近写入数据时每次都会重载缓存的指令，我们需要确保代码和数据不会共享相同的64字节单元 */
 #define RUN_SECTION_ALIGNMENT 63
#else
 #define RUN_SECTION_ALIGNMENT 0
#endif

/* 重新定位代码。 如果错误，则返回-1，如果ptr为NULL，则返回所需的大小，否则将代码复制到调用方传递的缓冲区中 */
static int zhi_重新定位_代码(知心状态机 *状态机1, void *ptr, 目标地址_类型 ptr_diff)
{
    段 *s;
    unsigned offset, length, align, max_align, i, k, f;
    目标地址_类型 mem, addr;

    if (NULL == ptr) {
        状态机1->数量_错误 = 0;
#ifdef ZHI_TARGET_PE
        pe_输出_文件(状态机1, NULL);
#else
        zhi_添加_运行时(状态机1);
	解决_常见_符号(状态机1);
        创建_获取_入口(状态机1);
#endif
        if (状态机1->数量_错误)
            return -1;
    }

    offset = max_align = 0, mem = (目标地址_类型)ptr;
#ifdef _WIN64
    offset += sizeof (void*); /* function_table指针的空间 */
#endif
    for (k = 0; k < 2; ++k) {
        f = 0, addr = k ? mem : mem + ptr_diff;
        for(i = 1; i < 状态机1->数量_段数; i++) {
            s = 状态机1->段数[i];
            if (0 == (s->sh_flags & SHF_ALLOC))
                continue;
            if (k != !(s->sh_flags & SHF_EXECINSTR))
                continue;
            align = s->sh_addralign - 1;
            if (++f == 1 && align < RUN_SECTION_ALIGNMENT)
                align = RUN_SECTION_ALIGNMENT;
            if (max_align < align)
                max_align = align;
            offset += -(addr + offset) & align;
            s->sh_addr = mem ? addr + offset : 0;
            offset += s->数据_偏移;
#if 0
            if (mem)
                printf("%-16s %p  len %04x  align %2d\n",
                    s->name, (void*)s->sh_addr, (unsigned)s->数据_偏移, align + 1);
#endif
        }
    }

    /* 重新定位符号 */
    重定位_符号(状态机1, 状态机1->全局单词表副本, !(状态机1->不添加标准库));
    if (状态机1->数量_错误)
        return -1;

    if (0 == mem)
        return offset + max_align;

#ifdef ZHI_TARGET_PE
    状态机1->pe_图像库 = mem;
#endif

    /* 重定位 each section */
    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (s->重定位)
            重定位_段(状态机1, s);
    }
#if !defined(ZHI_TARGET_PE) || defined(ZHI_TARGET_MACHO)
    重定位_plt(状态机1);
#endif

    for(i = 1; i < 状态机1->数量_段数; i++) {
        s = 状态机1->段数[i];
        if (0 == (s->sh_flags & SHF_ALLOC))
            continue;
        length = s->数据_偏移;
        ptr = (void*)s->sh_addr;
        if (s->sh_flags & SHF_EXECINSTR)
            ptr = (char*)((目标地址_类型)ptr - ptr_diff);
        if (NULL == s->data || s->sh_type == SHT_NOBITS)
            memset(ptr, 0, length);
        else
            memcpy(ptr, s->data, length);
        /* mark executable 段数 as executable in memory */
        if (s->sh_flags & SHF_EXECINSTR)
            设置_可执行_页面(状态机1, (char*)((目标地址_类型)ptr + ptr_diff), length);
    }

#ifdef _WIN64
    *(void**)mem = win64_添加_函数_表(状态机1);
#endif

    return 0;
}

/* ------------------------------------------------------------- */
/* 允许在内存中运行代码 */

static void 设置_可执行_页面(知心状态机 *状态机1, void *ptr, unsigned long length)
{
#ifdef _WIN32
    unsigned long old_protect;
    VirtualProtect(ptr, length, PAGE_EXECUTE_READWRITE, &old_protect);
#else
    void __clear_cache(void *beginning, void *end);
# ifndef HAVE_SELINUX
    目标地址_类型 start, end;
#  ifndef PAGESIZE
#   define PAGESIZE 4096
#  endif
    start = (目标地址_类型)ptr & ~(PAGESIZE - 1);
    end = (目标地址_类型)ptr + length;
    end = (end + PAGESIZE - 1) & ~(PAGESIZE - 1);
    if (mprotect((void *)start, end - start, PROT_READ | PROT_WRITE | PROT_EXEC))
        错误_打印("mprotect failed: did you mean to configure --with-selinux?");
# endif
# if defined ZHI_TARGET_ARM || defined ZHI_TARGET_ARM64
    __clear_cache(ptr, (char *)ptr + length);
# endif
#endif
}

#ifdef _WIN64
static void *win64_添加_函数_表(知心状态机 *状态机1)
{
    void *p = NULL;
    if (状态机1->uw_pdata) {
        p = (void*)状态机1->uw_pdata->sh_addr;
        RtlAddFunctionTable(
            (RUNTIME_FUNCTION*)p,
            状态机1->uw_pdata->数据_偏移 / sizeof (RUNTIME_FUNCTION),
            状态机1->pe_图像库
            );
        状态机1->uw_pdata = NULL;
    }
    return p;
}

static void win64_删除_函数_表(void *p)
{
    if (p) {
        RtlDeleteFunctionTable((RUNTIME_FUNCTION*)p);
    }
}
#endif
#endif //ndef 仅仅_配置_ZHI_回滚
/* ------------------------------------------------------------- */
#ifdef ZHI_配置_记录回滚

static int 运行时_vprintf(const char *fmt, va_list ap)
{
    int ret = vfprintf(stderr, fmt, ap);
    fflush(stderr);
    return ret;
}

static int 运行时_printf(const char *fmt, ...)
{
    va_list ap;
    int r;
    va_start(ap, fmt);
    r = 运行时_vprintf(fmt, ap);
    va_end(ap);
    return r;
}

#define 包含_堆栈_大小 32

/* 通过读取单词表调试信息在PC值'pc'的源文件中打印位置 */
static 目标地址_类型 运行时_打印行 (运行时_上下文 *rc, 目标地址_类型 wanted_pc,const char *msg, const char *跳过)
{
    char func_name[128];
    目标地址_类型 func_addr, last_pc, pc;
    const char *incl_files[包含_堆栈_大小];
    int incl_index, last_incl_index, len, 最后_行_号, i;
    const char *str, *p;
    ElfW(符号) *esym;
    单词表_符号 *sym;

next:
    func_name[0] = '\0';
    func_addr = 0;
    incl_index = 0;
    last_pc = (目标地址_类型)-1;
    最后_行_号 = 1;
    last_incl_index = 0;

    for (sym = rc->stab_sym + 1; sym < rc->单词表_符号_结束; ++sym) {
        str = rc->单词表_字符串 + sym->n_strx;
        pc = sym->n_value;

        switch(sym->n_type) {
        case N_SLINE:
            if (func_addr)
                goto rel_pc;
        case N_SO:
        case N_SOL:
            goto abs_pc;
        case N_FUN:
            if (sym->n_strx == 0) /* end of function */
                goto rel_pc;
        abs_pc:
#if 指针_大小 == 8
            /* 单词表_符号.n_value is only 32bits */
            pc += rc->prog_base;
#endif
            goto check_pc;
        rel_pc:
            pc += func_addr;
        check_pc:
            if (pc >= wanted_pc && wanted_pc >= last_pc)
                goto found;
            break;
        }

        switch(sym->n_type) {
            /* function start or end */
        case N_FUN:
            if (sym->n_strx == 0)
                goto reset_func;
            p = strchr(str, ':');
            if (0 == p || (len = p - str + 1, len > sizeof func_name))
                len = sizeof func_name;
            p字符串复制(func_name, len, str);
            func_addr = pc;
            break;
            /* line number info */
        case N_SLINE:
            last_pc = pc;
            最后_行_号 = sym->n_desc;
            last_incl_index = incl_index;
            break;
            /* include 文件数 */
        case N_BINCL:
            if (incl_index < 包含_堆栈_大小)
                incl_files[incl_index++] = str;
            break;
        case N_EINCL:
            if (incl_index > 1)
                incl_index--;
            break;
            /* start/end of translation unit */
        case N_SO:
            incl_index = 0;
            if (sym->n_strx) {
                /* do not add path */
                len = strlen(str);
                if (len > 0 && str[len - 1] != '/')
                    incl_files[incl_index++] = str;
            }
        reset_func:
            func_name[0] = '\0';
            func_addr = 0;
            last_pc = (目标地址_类型)-1;
            break;
            /* alternative file name (from #line or #include directives) */
        case N_SOL:
            if (incl_index)
                incl_files[incl_index-1] = str;
            break;
        }
    }

    func_name[0] = '\0';
    func_addr = 0;
    last_incl_index = 0;

    /* we try 全局单词表副本 symbols (no line number info) */
    for (esym = rc->elf符号_开始 + 1; esym < rc->elf符号_结束; ++esym) {
        int type = ELFW(ST_TYPE)(esym->st_info);
        if (type == STT_FUNC || type == STT_GNU_IFUNC) {
            if (wanted_pc >= esym->st_value &&
                wanted_pc < esym->st_value + esym->st_size) {
                p字符串复制(func_name, sizeof(func_name),
                    rc->elf_字符串 + esym->st_name);
                func_addr = esym->st_value;
                goto found;
            }
        }
    }

    if ((rc = rc->next))
        goto next;

found:
    i = last_incl_index;
    if (i > 0) {
        str = incl_files[--i];
        if (跳过[0] && strstr(str, 跳过))
            return (目标地址_类型)-1;
        运行时_printf("%s:%d: ", str, 最后_行_号);
    } else
        运行时_printf("%08llx : ", (long long)wanted_pc);
    运行时_printf("%s %s", msg, func_name[0] ? func_name : "???");
#if 0
    if (--i >= 0) {
        运行时_printf(" (included from ");
        for (;;) {
            运行时_printf("%s", incl_files[i]);
            if (--i < 0)
                break;
            运行时_printf(", ");
        }
        运行时_printf(")");
    }
#endif
    return func_addr;
}

static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level);

static int _rt_错误(void *fp, void *ip, const char *fmt, va_list ap)
{
    运行时_上下文 *rc = &g_rtctxt;
    目标地址_类型 pc = 0;
    char 跳过[100];
    int i, level, ret, n;
    const char *a, *b, *msg;

    if (fp) {
        /* we're called from zhi_backtrace. */
        rc->fp = (目标地址_类型)fp;
        rc->ip = (目标地址_类型)ip;
        msg = "";
    } else {
        /* we're called from signal/exception handler */
        msg = "RUNTIME ERROR: ";
    }

    跳过[0] = 0;
    /* If fmt is like "^file.c^..." then 跳过 calls from 'file.c' */
    if (fmt[0] == '^' && (b = strchr(a = fmt + 1, fmt[0]))) {
        memcpy(跳过, a, b - a), 跳过[b - a] = 0;
        fmt = b + 1;
    }

    n = rc->num_callers ? rc->num_callers : 6;
    for (i = level = 0; level < n; i++) {
        ret = 运行时_获取_调用_pc(&pc, rc, i);
        a = "%s";
        if (ret != -1) {
            pc = 运行时_打印行(rc, pc, level ? "by" : "at", 跳过);
            if (pc == (目标地址_类型)-1)
                continue;
            a = ": %s";
        }
        if (level == 0) {
            运行时_printf(a, msg);
            运行时_vprintf(fmt, ap);
        } else if (ret == -1)
            break;
        运行时_printf("\n");
        if (ret == -1 || (pc == (目标地址_类型)rc->top_func && pc))
            break;
        ++level;
    }

    rc->ip = rc->fp = 0;
    return 0;
}

/* 在位置“ pc”处发出运行时错误 */
static int 运行时_错误(const char *fmt, ...)
{
    va_list ap;
    int ret;
    va_start(ap, fmt);
    ret = _rt_错误(0, 0, fmt, ap);
    va_end(ap);
    return ret;
}

static void 运行时_退出(int code)
{
    运行时_上下文 *rc = &g_rtctxt;
    if (rc->do_jmp)
        longjmp(rc->jmp_buf, code ? code : 256);
    exit(code);
}

/* ------------------------------------------------------------- */

#ifndef _WIN32
# include <signal.h>
# ifndef __OpenBSD__
#  include <sys/ucontext.h>
# endif
#else
# define ucontext_t CONTEXT
#endif

/* 从ucontext_t *转换为内部运行时_上下文* */
static void 运行时_获取上下文(ucontext_t *uc, 运行时_上下文 *rc)
{
#if defined _WIN64
    rc->ip = uc->Rip;
    rc->fp = uc->Rbp;
    rc->sp = uc->Rsp;
#elif 已定义 _WIN32
    rc->ip = uc->Eip;
    rc->fp = uc->Ebp;
    rc->sp = uc->Esp;
#elif defined __i386__
# if defined(__APPLE__)
    rc->ip = uc->uc_mcontext->__ss.__eip;
    rc->fp = uc->uc_mcontext->__ss.__ebp;
# elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__DragonFly__)
    rc->ip = uc->uc_mcontext.mc_eip;
    rc->fp = uc->uc_mcontext.mc_ebp;
# elif defined(__dietlibc__)
    rc->ip = uc->uc_mcontext.eip;
    rc->fp = uc->uc_mcontext.ebp;
# elif defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_EIP];
    rc->fp = uc->uc_mcontext.__gregs[_REG_EBP];
# elif defined(__OpenBSD__)
    rc->ip = uc->sc_eip;
    rc->fp = uc->sc_ebp;
# elif !defined REG_EIP && defined EIP /* fix for glibc 2.1 */
    rc->ip = uc->uc_mcontext.gregs[EIP];
    rc->fp = uc->uc_mcontext.gregs[EBP];
# else
    rc->ip = uc->uc_mcontext.gregs[REG_EIP];
    rc->fp = uc->uc_mcontext.gregs[REG_EBP];
# endif
#elif defined(__x86_64__)
# if defined(__APPLE__)
    rc->ip = uc->uc_mcontext->__ss.__rip;
    rc->fp = uc->uc_mcontext->__ss.__rbp;
# elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__DragonFly__)
    rc->ip = uc->uc_mcontext.mc_rip;
    rc->fp = uc->uc_mcontext.mc_rbp;
# elif defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_RIP];
    rc->fp = uc->uc_mcontext.__gregs[_REG_RBP];
# else
    rc->ip = uc->uc_mcontext.gregs[REG_RIP];
    rc->fp = uc->uc_mcontext.gregs[REG_RBP];
# endif
#elif defined(__arm__)
    rc->ip = uc->uc_mcontext.arm_pc;
    rc->fp = uc->uc_mcontext.arm_fp;
#elif defined(__aarch64__)
    rc->ip = uc->uc_mcontext.pc;
    rc->fp = uc->uc_mcontext.regs[29];
#elif defined(__riscv)
    rc->ip = uc->uc_mcontext.__gregs[REG_PC];
    rc->fp = uc->uc_mcontext.__gregs[REG_S0];
#endif
}

/* ------------------------------------------------------------- */
#ifndef _WIN32
static void 致命错误_信号处理(int signum, siginfo_t *siginf, void *puc)
{
    运行时_上下文 *rc = &g_rtctxt;
    运行时_获取上下文(puc, rc);

    switch(signum) {
    case SIGFPE:
        switch(siginf->si_code) {
        case FPE_INTDIV:
        case FPE_FLTDIV:
            运行时_错误("被零除");
            break;
        default:
            运行时_错误("浮点异常");
            break;
        }
        break;
    case SIGBUS:
    case SIGSEGV:
        运行时_错误("无效的内存访问");
        break;
    case SIGILL:
        运行时_错误("非法指示");
        break;
    case SIGABRT:
        运行时_错误("abort（）被调用");
        break;
    default:
        运行时_错误("捕获信号 %d", signum);
        break;
    }
    运行时_退出(255);
}

#ifndef SA_SIGINFO
# define SA_SIGINFO 0x00000004u
#endif

/* 发生CPU异常时生成堆栈回溯。 */
static void 设置_异常_处理程序(void)
{
    struct sigaction sigact;
    /* install ZHI signal handlers to print debug info on fatal
       runtime errors */
    sigact.sa_flags = SA_SIGINFO | SA_RESETHAND;
#if 0//def SIGSTKSZ // this causes signals not to work at all on some (older) linuxes
    sigact.sa_flags |= SA_ONSTACK;
#endif
    sigact.sa_sigaction = 致命错误_信号处理;
    sigemptyset(&sigact.sa_mask);
    sigaction(SIGFPE, &sigact, NULL);
    sigaction(SIGILL, &sigact, NULL);
    sigaction(SIGSEGV, &sigact, NULL);
    sigaction(SIGBUS, &sigact, NULL);
    sigaction(SIGABRT, &sigact, NULL);
#if 0//def SIGSTKSZ
    /* This allows stack overflow to be reported instead of a SEGV */
    {
        stack_t ss;
        static unsigned char stack[SIGSTKSZ] __attribute__((aligned(16)));

        ss.ss_sp = stack;
        ss.ss_size = SIGSTKSZ;
        ss.ss_flags = 0;
        sigaltstack(&ss, NULL);
    }
#endif
}

#else /* WIN32 */

/* 致命错误的信号处理程序 */
static long __stdcall cpu_异常_处理程序(EXCEPTION_POINTERS *ex_info)
{
    运行时_上下文 *rc = &g_rtctxt;
    unsigned code;
    运行时_获取上下文(ex_info->ContextRecord, rc);

    switch (code = ex_info->ExceptionRecord->ExceptionCode) {
    case EXCEPTION_ACCESS_VIOLATION:
	运行时_错误("无效的内存访问");
        break;
    case EXCEPTION_STACK_OVERFLOW:
        运行时_错误("堆栈溢出");
        break;
    case EXCEPTION_INT_DIVIDE_BY_ZERO:
        运行时_错误("被零除");
        break;
    case EXCEPTION_BREAKPOINT:
    case EXCEPTION_SINGLE_STEP:
        rc->ip = *(目标地址_类型*)rc->sp;
        运行时_错误("断点/单步异常:");
        return EXCEPTION_CONTINUE_SEARCH;
    default:
        运行时_错误("捕获到异常 %08x", code);
        break;
    }
    if (rc->do_jmp)
        运行时_退出(255);
    return EXCEPTION_EXECUTE_HANDLER;
}

/* 发生CPU异常时生成堆栈回溯。 */
static void 设置_异常_处理程序(void)
{
    SetUnhandledExceptionFilter(cpu_异常_处理程序);//设置未处理的异常过滤器
}

#endif

/* ------------------------------------------------------------- */
/* return the PC at frame level 'level'. Return negative if not found */
#if defined(__i386__) || defined(__x86_64__)
static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level)
{
    目标地址_类型 ip, fp;
    if (level == 0) {
        ip = rc->ip;
    } else {
        ip = 0;
        fp = rc->fp;
        while (--level) {
            /* XXX: check address validity with program info */
            if (fp <= 0x1000)
                break;
            fp = ((目标地址_类型 *)fp)[0];
        }
        if (fp > 0x1000)
            ip = ((目标地址_类型 *)fp)[1];
    }
    if (ip <= 0x1000)
        return -1;
    *paddr = ip;
    return 0;
}

#elif defined(__arm__)
static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level)
{
    /* XXX: only supports linux */
#if !defined(__linux__)
    return -1;
#else
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        目标地址_类型 fp = rc->fp;
        while (--level)
            fp = ((目标地址_类型 *)fp)[0];
        *paddr = ((目标地址_类型 *)fp)[2];
    }
    return 0;
#endif
}

#elif defined(__aarch64__)
static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level)
{
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        目标地址_类型 *fp = (目标地址_类型*)rc->fp;
        while (--level)
            fp = (目标地址_类型 *)fp[0];
        *paddr = fp[1];
    }
    return 0;
}

#elif defined(__riscv)
static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level)
{
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        目标地址_类型 *fp = (目标地址_类型*)rc->fp;
        while (--level)
            fp = (目标地址_类型 *)fp[-2];
        *paddr = fp[-1];
    }
    return 0;
}

#else
#warning add arch specific 运行时_获取_调用_pc()
static int 运行时_获取_调用_pc(目标地址_类型 *paddr, 运行时_上下文 *rc, int level)
{
    return -1;
}

#endif
#endif /* ZHI_配置_记录回滚 */
/* ------------------------------------------------------------- */
#ifdef 配置_ZHI_静态

/* 用于分析的伪函数 */
静态_函数 void *dl打开(const char *文件名, int flag)
{
    return NULL;
}

静态_函数 void dl关闭(void *p)
{
}

静态_函数 const char *dl错误(void)
{
    return "error";
}

typedef struct ZHI符号 {
    char *str;
    void *ptr;
} ZHI符号;


/* 如果未完成动态链接，请在此处添加所需的符号 */
static ZHI符号 zhi_符号[] = {
#if !defined(CONFIG_ZHIBOOT)
#define ZHISYM(a) { #a, &a, },
    ZHISYM(printf)
    ZHISYM(fprintf)
    ZHISYM(fopen)
    ZHISYM(fclose)
#undef ZHISYM
#endif
    { NULL, NULL },
};

静态_函数 void *dl符号(void *handle, const char *symbol)
{
    ZHI符号 *p;
    p = zhi_符号;
    while (p->str != NULL) {
        if (!strcmp(p->str, symbol))
            return p->ptr;
        p++;
    }
    return NULL;
}

#endif /* 配置_ZHI_静态 */
#endif /* ZHI_是_本机 */
/* ------------------------------------------------------------- */
