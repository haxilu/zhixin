/*
 *功能：静态库。制作没有ar的hexinku1.a。在Linux系统中，静态库的文件名多以.a结尾，它是一个或多个目标文件的集合。
 *用法：tiny_libmaker [lib]文件...
 */

#include "zhi.h"

#define ARFMAG "`\n"

typedef struct {
    char ar_name[16];
    char ar_date[12];
    char ar_uid[6];
    char ar_gid[6];
    char ar_mode[8];
    char ar_size[10];
    char ar_fmag[2];
} Ar头;/*静态库头*/
/*可能是哈希算法*/
static unsigned long le2belong(unsigned long ul)
{
    return ((ul & 0xFF0000)>>8)+((ul & 0xFF000000)>>24) +((ul & 0xFF)<<24)+((ul & 0xFF00)<<8);
}

/* 如果s包含list列表的任何字符，则返回1，否则返回0 */
static int 包含任何字符(const char *s, const char *list) {
  const char *l;
  for (; *s; s++) {
      for (l = list; *l; l++) {
          if (*s == *l)
              return 1;
      }
  }
  return 0;
}

static int 静态库_用法(int ret) {
    fprintf(stderr, "用法: zhi -ar [rcsv] lib file...\n");
    fprintf(stderr, "创建库 ([abdioptxN] not supported).\n");
    return ret;
}

静态_函数 int 创建静态库文件(知心状态机 *状态机1, int 参数数量, char **参数数组)
{
	/*初始化静态库头*/
    static Ar头 arhdr = {
        "/               ",
        "            ",
        "0     ",
        "0     ",
        "0       ",
        "          ",
        ARFMAG
        };

    static Ar头 arhdro = {
        "                ",
        "            ",
        "0     ",
        "0     ",
        "0       ",
        "          ",
        ARFMAG
        };

    FILE *fi, *fh = NULL, *fo = NULL;
    ElfW(ELF文件头) *elf头;
    ElfW(节头表) *节头表;
    ElfW(符号) *sym;
    int i, fsize, i_lib, i_obj;
    char *buf, *shstr, *全局单词表副本 = NULL, *字符表 = NULL;
    int symtabsize = 0;//, strtabsize = 0;
    char *anames = NULL;
    int *afpos = NULL;
    int istrlen, strpos = 0, fpos = 0, funccnt = 0, funcmax, hofs;
    char tfile[260], stmp[20];
    char *file, *name;
    int ret = 2;
    const char *ops_conflict = "habdioptxN";  // 不支持，但如果忽略则具有破坏性。
    int 显示信息 = 0;

    i_lib = 0; i_obj = 0;  // 将保存lib和第一个obj的索引
    for (i = 1; i < 参数数量; i++)
    {
        const char *命令行参数 = 参数数组[i];
        if (*命令行参数 == '-' && strstr(命令行参数, "."))/*strstr(命令行参数, "."):在“命令行参数”中查找“.”第一次出现的位置并返回此位置，找不到则返回null*/
            ret = 1; // -x.y始终无效（与gnu ar相同）
        if ((*命令行参数 == '-') || (i == 1 && !strstr(命令行参数, ".")))
        {
            if (包含任何字符(命令行参数, ops_conflict))
                ret = 1;
            if (strstr(命令行参数, "v"))
                显示信息 = 1;
        } else
        {  // lib或obj文件：请勿中止-继续验证所有args。
            if (!i_lib)  // 第一个文件是库
                i_lib = i;
            else if (!i_obj)  // 第二个文件是第一个obj
                i_obj = i;
        }
    }

    if (!i_obj)
        ret = 1;

    if (ret == 1)
        return 静态库_用法(ret);

    if ((fh = fopen(参数数组[i_lib], "wb")) == NULL)
    {
        fprintf(stderr, "zhi: ar: 无法打开文件 %s \n", 参数数组[i_lib]);
        goto the_end;
    }

    sprintf(tfile, "%s.tmp", 参数数组[i_lib]);
    if ((fo = fopen(tfile, "wb+")) == NULL)
    {
        fprintf(stderr, "zhi: ar: 无法创建临时文件 %s\n", tfile);
        goto the_end;
    }

    funcmax = 250;
    afpos = 内存_重分配容量(NULL, funcmax * sizeof *afpos); // 250 func
    memcpy(&arhdro.ar_mode, "100666", 6);

    // i_obj =第一个输入目标文件
    while (i_obj < 参数数量)
    {
        if (*参数数组[i_obj] == '-') {  // 到目前为止，所有选项均以“-”开头
            i_obj++;
            continue;
        }
        if ((fi = fopen(参数数组[i_obj], "rb")) == NULL) {
            fprintf(stderr, "zhi: ar: 无法打开文件 %s \n", 参数数组[i_obj]);
            goto the_end;
        }
        if (显示信息)
            printf("a - %s\n", 参数数组[i_obj]);

        fseek(fi, 0, SEEK_END);
        fsize = ftell(fi);
        fseek(fi, 0, SEEK_SET);
        buf = 内存_申请(fsize + 1);
        fread(buf, fsize, 1, fi);
        fclose(fi);

        // elf ELF文件头
        elf头 = (ElfW(ELF文件头) *)buf;
        if (elf头->e_ident[4] != ELFCLASSW)
        {
            fprintf(stderr, "zhi: ar: 不支持的目标类: %s\n", 参数数组[i_obj]);
            goto the_end;
        }

        节头表 = (ElfW(节头表) *) (buf + elf头->e_shoff + elf头->e_shstrndx * elf头->e_shentsize);
        shstr = (char *)(buf + 节头表->sh_偏移);
        for (i = 0; i < elf头->e_shnum; i++)
        {
            节头表 = (ElfW(节头表) *) (buf + elf头->e_shoff + i * elf头->e_shentsize);
            if (!节头表->sh_偏移)
                continue;
            if (节头表->sh_type == SHT_SYMTAB)
            {
                全局单词表副本 = (char *)(buf + 节头表->sh_偏移);
                symtabsize = 节头表->sh_size;
            }
            if (节头表->sh_type == SHT_STRTAB)
            {
                if (!strcmp(shstr + 节头表->sh_name, ".字符表"))
                {
                    字符表 = (char *)(buf + 节头表->sh_偏移);
                    //strtabsize = 节头表->sh_size;
                }
            }
        }

        if (全局单词表副本 && symtabsize)
        {
            int nsym = symtabsize / sizeof(ElfW(符号));
            //printf("全局单词表副本: info size shndx name\n");
            for (i = 1; i < nsym; i++)
            {
                sym = (ElfW(符号) *) (全局单词表副本 + i * sizeof(ElfW(符号)));
                if (sym->st_shndx &&
                    (sym->st_info == 0x10
                    || sym->st_info == 0x11
                    || sym->st_info == 0x12
                    )) {
                    //printf("全局单词表副本: %2Xh %4Xh %2Xh %s\n", sym->st_info, sym->st_size, sym->st_shndx, 字符表 + sym->st_name);
                    istrlen = strlen(字符表 + sym->st_name)+1;
                    anames = 内存_重分配容量(anames, strpos+istrlen);
                    strcpy(anames + strpos, 字符表 + sym->st_name);
                    strpos += istrlen;
                    if (++funccnt >= funcmax) {
                        funcmax += 250;
                        afpos = 内存_重分配容量(afpos, funcmax * sizeof *afpos); // 250 func more
                    }
                    afpos[funccnt] = fpos;
                }
            }
        }

        file = 参数数组[i_obj];
        for (name = strchr(file, 0);
             name > file && name[-1] != '/' && name[-1] != '\\';
             --name);
        istrlen = strlen(name);
        if (istrlen >= sizeof(arhdro.ar_name))
            istrlen = sizeof(arhdro.ar_name) - 1;
        memset(arhdro.ar_name, ' ', sizeof(arhdro.ar_name));
        memcpy(arhdro.ar_name, name, istrlen);
        arhdro.ar_name[istrlen] = '/';
        sprintf(stmp, "%-10d", fsize);
        memcpy(&arhdro.ar_size, stmp, 10);
        fwrite(&arhdro, sizeof(arhdro), 1, fo);
        fwrite(buf, fsize, 1, fo);
        内存_释放(buf);
        i_obj++;
        fpos += (fsize + sizeof(arhdro));
    }
    hofs = 8 + sizeof(arhdr) + strpos + (funccnt+1) * sizeof(int);
    fpos = 0;
    if ((hofs & 1)) // 对齐
        hofs++, fpos = 1;
    // 写头文件
    fwrite("!<arch>\n", 8, 1, fh);
    sprintf(stmp, "%-10d", (int)(strpos + (funccnt+1) * sizeof(int)));
    memcpy(&arhdr.ar_size, stmp, 10);
    fwrite(&arhdr, sizeof(arhdr), 1, fh);
    afpos[0] = le2belong(funccnt);
    for (i=1; i<=funccnt; i++)
        afpos[i] = le2belong(afpos[i] + hofs);
    fwrite(afpos, (funccnt+1) * sizeof(int), 1, fh);
    fwrite(anames, strpos, 1, fh);
    if (fpos)
        fwrite("", 1, 1, fh);
    fseek(fo, 0, SEEK_END);
    fsize = ftell(fo);
    fseek(fo, 0, SEEK_SET);
    buf = 内存_申请(fsize + 1);
    fread(buf, fsize, 1, fo);
    fwrite(buf, fsize, 1, fh);
    内存_释放(buf);
    ret = 0;
the_end:
    if (anames)
        内存_释放(anames);
    if (afpos)
        内存_释放(afpos);
    if (fh)
        fclose(fh);
    if (fo)
        fclose(fo), remove(tfile);
    return ret;
}

/* -------------------------------------------------------------- */
/*tiny_impdef从MS-Windows上的dll创建一个导出定义文件（.def）。 用法：tiny_impdef library.dll [-o输出文件]“
 */

#ifdef ZHI_TARGET_PE

静态_函数 int 创建定义文件(知心状态机 *状态机1, int 参数数量, char **参数数组)
{
    int ret, v, i;
    char infile[260];
    char 输出文件[260];

    const char *file;
    char *p, *q;
    FILE *fp, *op;

#ifdef _WIN32
    char path[260];
#endif

    infile[0] = 输出文件[0] = 0;
    fp = op = NULL;
    ret = 1;
    p = NULL;
    v = 0;

    for (i = 1; i < 参数数量; ++i)
    {
        const char *a = 参数数组[i];
        if ('-' == a[0])
        {
            if (0 == strcmp(a, "-v"))
            {
                v = 1;
            } else if (0 == strcmp(a, "-o"))
            {
                if (++i == 参数数量)
                    goto usage;
                strcpy(输出文件, 参数数组[i]);
            } else
                goto usage;
        } else if (0 == infile[0])
            strcpy(infile, a);
        else
            goto usage;
    }

    if (0 == infile[0]) {
usage:
        fprintf(stderr,
            "用法: zhi -impdef library.dll [-v] [-o outputfile]\n"
            "从dll创建导出定义文件（.def）\n"
            );
        goto the_end;
    }

    if (0 == 输出文件[0]) {
        strcpy(输出文件, 取_文件基本名(infile));
        q = strrchr(输出文件, '.');
        if (NULL == q)
            q = strchr(输出文件, 0);
        strcpy(q, ".def");
    }

    file = infile;
#ifdef _WIN32
    if (SearchPath(NULL, file, ".dll", sizeof path, path, NULL))
        file = path;
#endif
    ret = zhi_获取_dll导出(file, &p);
    if (ret || !p) {
        fprintf(stderr, "zhi: impdef: %s '%s'\n",
            ret == -1 ? "找不到文件" :
            ret ==  1 ? "无法读取符号" :
            ret ==  0 ? "找不到任何符号" :
            "未知的文件类型", file);
        ret = 1;
        goto the_end;
    }

    if (v)
        printf("-> %s\n", file);

    op = fopen(输出文件, "wb");
    if (NULL == op) {
        fprintf(stderr, "zhi: impdef: 无法创建输出文件: %s\n", 输出文件);
        goto the_end;
    }

    fprintf(op, "LIBRARY %s\n\nEXPORTS\n", 取_文件基本名(file));
    for (q = p, i = 0; *q; ++i) {
        fprintf(op, "%s\n", q);
        q += strlen(q) + 1;
    }

    if (v)
        printf("<- %s (%d symbol%s)\n", 输出文件, i, &"s"[i<2]);

    ret = 0;

the_end:
    /* 无法释放从zhi_get_dllexports接收到的内存（如果它来自dll） */
    /* if (p)
        内存_释放(p); */
    if (fp)
        fclose(fp);
    if (op)
        fclose(op);
    return ret;
}

#endif /* ZHI_TARGET_PE */

/* re-execute the i386/x86_64 cross-compilers with zhi -m32/-m64: */

#if !defined ZHI_TARGET_I386 && !defined ZHI_TARGET_X86_64

静态_函数 void 编译器工具交叉(知心状态机 *状态机1, char **参数数组, int option)
{
    错误_打印("-m％d未实现.", option);
}

#else
#ifdef _WIN32
#include <process.h>

static char *字符串_替换(const char *str, const char *p, const char *r)
{
    const char *s, *s0;
    char *d, *d0;
    int sl, pl, rl;

    sl = strlen(str);
    pl = strlen(p);
    rl = strlen(r);
    for (d0 = NULL;; d0 = 内存_申请(sl + 1)) {
        for (d = d0, s = str; s0 = s, s = strstr(s, p), s; s += pl) {
            if (d) {
                memcpy(d, s0, sl = s - s0), d += sl;
                memcpy(d, r, rl), d += rl;
            } else
                sl += rl - pl;
        }
        if (d) {
            strcpy(d, s0);
            return d0;
        }
    }
}

static int execvp_win32(const char *prog, char **参数数组)
{
    int ret; char **p;
    /* replace all " by \" */
    for (p = 参数数组; *p; ++p)
        if (strchr(*p, '"'))
            *p = 字符串_替换(*p, "\"", "\\\"");
    ret = _spawnvp(P_NOWAIT, prog, (const char *const*)参数数组);
    if (-1 == ret)
        return ret;
    _cwait(&ret, ret, WAIT_CHILD);
    exit(ret);
}
#define execvp execvp_win32
#endif /* _WIN32 */
/*编译器工具交叉*/
静态_函数 void 编译器工具交叉(知心状态机 *状态机1, char **参数数组, int target)/*状态机, 参数数组, 编译指令*/
{
    char program[4096];
    char *a0 = 参数数组[0];
    int prefix = 取_文件基本名(a0) - a0;

    snprintf(program, sizeof program,
        "%.*s%s"
#ifdef ZHI_TARGET_PE
        "-win32"
#endif
        "-zhi"
#ifdef _WIN32
        ".exe"
#endif
        , prefix, a0, target == 64 ? "x86_64" : "i386");

    if (strcmp(a0, program))
        execvp(参数数组[0] = program, 参数数组);
    错误_打印("无法运行 '%s'", program);
}

#endif /* ZHI_TARGET_I386 && ZHI_TARGET_X86_64 */
/* -------------------------------------------------------------- */
/* 启用命令行通配符扩展（zhi -o x.exe * .c） */

#ifdef _WIN32
int _CRT_glob = 1;
#ifndef _CRT_glob
int _dowildcard = 1;
#endif
#endif

/* -------------------------------------------------------------- */
/* generate xxx.d file */

静态_函数 void 生成_makedeps(知心状态机 *状态机1, const char *target, const char *文件名)
{
    FILE *depout;
    char buf[1024];
    int i, k;

    if (!文件名) {
        /* compute 文件名 automatically: dir/file.o -> dir/file.d */
        snprintf(buf, sizeof buf, "%.*s.d",
            (int)(取_文件扩展名(target) - target), target);
        文件名 = buf;
    }

    if (状态机1->显示信息)
        printf("<- %s\n", 文件名);

    /* XXX 返回错误代码，而不是error（）? */
    depout = fopen(文件名, "w");
    if (!depout)
        错误_打印("无法打开 '%s'", 文件名);
    fprintf(depout, "%s:", target);
    for (i = 0; i<状态机1->数量_目标_依赖; ++i) {
        for (k = 0; k < i; ++k)
            if (0 == strcmp(状态机1->目标_依赖[i], 状态机1->目标_依赖[k]))
                goto next;
        fprintf(depout, " \\\n  %s", 状态机1->目标_依赖[i]);
    next:;
    }
    fprintf(depout, "\n");
    fclose(depout);
}

/* -------------------------------------------------------------- */
