#ifdef TARGET_DEFS_ONLY

// Number of registers available to allocator:
#define 可用_寄存器数 19 // x10-x17 aka a0-a7, f10-f17 aka fa0-fa7, xxx, ra, sp
#define NB_ASM_REGS 32
#define 配置_ZHI_汇编

#define TREG_R(x) (x) // x = 0..7
#define TREG_F(x) (x + 8) // x = 0..7

// Register classes sorted from more general to more precise:
#define 寄存器类_整数 (1 << 0)
#define 寄存器类_浮点 (1 << 1)
#define RC_R(x) (1 << (2 + (x))) // x = 0..7
#define RC_F(x) (1 << (10 + (x))) // x = 0..7

#define 寄存器类_返回整数寄存器 (RC_R(0)) // int return register class
#define RC_IRE2 (RC_R(1)) // int 2nd return register class
#define 寄存器类_返回浮点寄存器 (RC_F(0)) // float return register class

#define 寄存器_返回16位整数寄存器 (TREG_R(0)) // int return register number
#define 寄存器_返回32位整数寄存器 (TREG_R(1)) // int 2nd return register number
#define 寄存器_返回浮点寄存器 (TREG_F(0)) // float return register number

#define 指针_大小 8

#define 长双精度_大小 16
#define 长双精度_对齐 16

#define MAX_ALIGN 16

#define 字符串_是_无符号

#else
#define 全局_使用
#include "zhi.h"
#include <assert.h>

#define XLEN 8

#define TREG_RA 17
#define TREG_SP 18

静态_外部 const int 寄存器_类数[可用_寄存器数] = {
  寄存器类_整数 | RC_R(0),
  寄存器类_整数 | RC_R(1),
  寄存器类_整数 | RC_R(2),
  寄存器类_整数 | RC_R(3),
  寄存器类_整数 | RC_R(4),
  寄存器类_整数 | RC_R(5),
  寄存器类_整数 | RC_R(6),
  寄存器类_整数 | RC_R(7),
  寄存器类_浮点 | RC_F(0),
  寄存器类_浮点 | RC_F(1),
  寄存器类_浮点 | RC_F(2),
  寄存器类_浮点 | RC_F(3),
  寄存器类_浮点 | RC_F(4),
  寄存器类_浮点 | RC_F(5),
  寄存器类_浮点 | RC_F(6),
  寄存器类_浮点 | RC_F(7),
  0,
  1 << TREG_RA,
  1 << TREG_SP
};

#if defined(配置_ZHI_边界检查)
static 目标地址_类型 func_bound_offset;
static unsigned long func_bound_ind;
static int func_bound_add_epilog;
#endif

static int ireg(int r)
{
    if (r == TREG_RA)
      return 1; // ra
    if (r == TREG_SP)
      return 2; // sp
    assert(r >= 0 && r < 8);
    return r + 10;  // zhirX --> aX == x(10+X)
}

static int is_ireg(int r)
{
    return (unsigned)r < 8 || r == TREG_RA || r == TREG_SP;
}

static int freg(int r)
{
    assert(r >= 8 && r < 16);
    return r - 8 + 10;  // zhifX --> faX == f(10+X)
}

static int is_freg(int r)
{
    return r >= 8 && r < 16;
}

静态_函数 void o(unsigned int c)
{
    int ind1 = 输出代码索引 + 4;
    if (不需要_代码生成)
        return;
    if (ind1 > 当前_生成代码_段->data_allocated)
        节_重新分配内存(当前_生成代码_段, ind1);
    写32le(当前_生成代码_段->data + 输出代码索引, c);
    输出代码索引 = ind1;
}

static void EIu(uint32_t opcode, uint32_t func3,
               uint32_t rd, uint32_t rs1, uint32_t imm)
{
    o(opcode | (func3 << 12) | (rd << 7) | (rs1 << 15) | (imm << 20));
}

static void ER(uint32_t opcode, uint32_t func3,
               uint32_t rd, uint32_t rs1, uint32_t rs2, uint32_t func7)
{
    o(opcode | func3 << 12 | rd << 7 | rs1 << 15 | rs2 << 20 | func7 << 25);
}

static void EI(uint32_t opcode, uint32_t func3,
               uint32_t rd, uint32_t rs1, uint32_t imm)
{
    assert(! ((imm + (1 << 11)) >> 12));
    EIu(opcode, func3, rd, rs1, imm);
}

static void ES(uint32_t opcode, uint32_t func3,
               uint32_t rs1, uint32_t rs2, uint32_t imm)
{
    assert(! ((imm + (1 << 11)) >> 12));
    o(opcode | (func3 << 12) | ((imm & 0x1f) << 7) | (rs1 << 15)
      | (rs2 << 20) | ((imm >> 5) << 25));
}

// Patch all branches in list pointed to by t to branch to a:
静态_函数 void 生成符号_地址(int t_, int a_)
{
    uint32_t t = t_;
    uint32_t a = a_;
    while (t) {
        unsigned char *ptr = 当前_生成代码_段->data + t;
        uint32_t next = 读32le(ptr);
        uint32_t r = a - t, imm;
        if ((r + (1 << 21)) & ~((1U << 22) - 2))
          错误_打印("out-of-range branch chain");
        imm =   (((r >> 12) &  0xff) << 12)
            | (((r >> 11) &     1) << 20)
            | (((r >>  1) & 0x3ff) << 21)
            | (((r >> 20) &     1) << 31);
        写32le(ptr, r == 4 ? 0x33 : 0x6f | imm); // nop || j imm
        t = next;
    }
}

static int load_symofs(int r, 堆栈值 *sv, int forstore)
{
    static 符号 label;
    int rr, doload = 0;
    int fc = sv->c.i, v = sv->r & VT_值掩码;
    if (sv->r & VT_符号) {
        assert(v == VT_VC常量);
        if (sv->sym->type.t & VT_静态) { // XXX do this per linker relax
            添加新的重定位项(当前_生成代码_段, sv->sym, 输出代码索引,
                    R_RISCV_PCREL_HI20, sv->c.i);
            sv->c.i = 0;
        } else {
            if (((unsigned)fc + (1 << 11)) >> 12)
              错误_打印("unimp: large addend for global address (0x%llx)", (long long)sv->c.i);
            添加新的重定位项(当前_生成代码_段, sv->sym, 输出代码索引,
                    R_RISCV_GOT_HI20, 0);
            doload = 1;
        }
        if (!label.v) {
            label.v = 单词表_查找(".L0 ", 4)->单词编码;
            label.type.t = VT_无类型 | VT_静态;
        }
        label.c = 0; /* force new local ELF symbol */
        更新_外部_符号(&label, 当前_生成代码_段, 输出代码索引, 0);
        rr = is_ireg(r) ? ireg(r) : 5;
        o(0x17 | (rr << 7));   // auipc RR, 0 %pcrel_hi(sym)+addend
        添加新的重定位项(当前_生成代码_段, &label, 输出代码索引,
                doload || !forstore
                  ? R_RISCV_PCREL_LO12_I : R_RISCV_PCREL_LO12_S, 0);
        if (doload) {
            EI(0x03, 3, rr, rr, 0); // ld RR, 0(RR)
        }
    } else if (v == VT_LOCAL || v == VT_LLOCAL) {
        rr = 8; // s0
        if (fc != sv->c.i)
          错误_打印("unimp: 存储(giant local off) (0x%llx)", (long long)sv->c.i);
        if (((unsigned)fc + (1 << 11)) >> 12) {
            rr = is_ireg(r) ? ireg(r) : 5; // t0
            o(0x37 | (rr << 7) | ((0x800 + fc) & 0xfffff000)); //lui RR, upper(fc)
            ER(0x33, 0, rr, rr, 8, 0); // add RR, RR, s0
            sv->c.i = fc << 20 >> 20;
        }
    } else
      错误_打印("uhh");
    return rr;
}

静态_函数 void 加载(int r, 堆栈值 *sv)
{
    int fr = sv->r;
    int v = fr & VT_值掩码;
    int rr = is_ireg(r) ? ireg(r) : freg(r);
    int fc = sv->c.i;
    int bt = sv->type.t & VT_基本类型;
    int align, size = 类型_大小(&sv->type, &align);
    if (fr & VT_LVAL) {
        int func3, opcode = is_freg(r) ? 0x07 : 0x03, br;
        assert (!is_freg(r) || bt == VT_浮点 || bt == VT_双精度);
        if (bt == VT_函数) /* XXX should be done in generic code */
          size = 指针_大小;
        func3 = size == 1 ? 0 : size == 2 ? 1 : size == 4 ? 2 : 3;
        if (size < 4 && !是_浮点型(sv->type.t) && (sv->type.t & VT_无符号))
          func3 |= 4;
        if (v == VT_LOCAL || (fr & VT_符号)) {
            br = load_symofs(r, sv, 0);
            fc = sv->c.i;
        } else if (v < VT_VC常量) {
            br = ireg(v);
            /*if (((unsigned)fc + (1 << 11)) >> 12)
              错误_打印("unimp: 加载(large addend) (0x%x)", fc);*/
            fc = 0; // XXX 存储 ofs in LVAL(reg)
        } else if (v == VT_LLOCAL) {
            br = load_symofs(r, sv, 0);
            fc = sv->c.i;
            EI(0x03, 3, rr, br, fc); // ld RR, fc(BR)
            br = rr;
            fc = 0;
        } else {
            错误_打印("unimp: 加载(non-local lval)");
        }
        EI(opcode, func3, rr, br, fc); // l[bhwd][u] / fl[wd] RR, fc(BR)
    } else if (v == VT_VC常量) {
        int rb = 0, do32bit = 8, zext = 0;
        assert((!是_浮点型(sv->type.t) && is_ireg(r)) || bt == VT_长双精度);
        if (fr & VT_符号) {
            rb = load_symofs(r, sv, 0);
            fc = sv->c.i;
            do32bit = 0;
        }
        if (是_浮点型(sv->type.t) && bt != VT_长双精度)
          错误_打印("unimp: 加载(float)");
        if (fc != sv->c.i) {
            int64_t si = sv->c.i;
            uint32_t pi;
            si >>= 32;
            if (si != 0) {
                pi = si;
                if (fc < 0)
                  pi++;
                o(0x37 | (rr << 7) | (((pi + 0x800) & 0xfffff000))); // lui RR, up(up(fc))
                EI(0x13, 0, rr, rr, (int)pi << 20 >> 20);   // addi RR, RR, lo(up(fc))
                EI(0x13, 1, rr, rr, 12); // slli RR, RR, 12
                EI(0x13, 0, rr, rr, (fc + (1 << 19)) >> 20);  // addi RR, RR, up(lo(fc))
                EI(0x13, 1, rr, rr, 12); // slli RR, RR, 12
                fc = fc << 12 >> 12;
                EI(0x13, 0, rr, rr, fc >> 8);  // addi RR, RR, lo1(lo(fc))
                EI(0x13, 1, rr, rr, 8); // slli RR, RR, 8
                fc &= 0xff;
                rb = rr;
                do32bit = 0;
            } else if (bt == VT_长长整数) {
                /* A 32bit unsigned constant for a 64bit type.
                   lui always sign extends, so we need to do an explicit zext.*/
                zext = 1;
            }
        }
        if (((unsigned)fc + (1 << 11)) >> 12)
            o(0x37 | (rr << 7) | ((0x800 + fc) & 0xfffff000)), rb = rr; //lui RR, upper(fc)
        if (fc || (rr != rb) || do32bit || (fr & VT_符号))
          EI(0x13 | do32bit, 0, rr, rb, fc << 20 >> 20); // addi[w] R, x0|R, FC
        if (zext) {
            EI(0x13, 1, rr, rr, 32); // slli RR, RR, 32
            EI(0x13, 5, rr, rr, 32); // srli RR, RR, 32
        }
    } else if (v == VT_LOCAL) {
        int br = load_symofs(r, sv, 0);
        assert(is_ireg(r));
        fc = sv->c.i;
        EI(0x13, 0, rr, br, fc); // addi R, s0, FC
    } else if (v < VT_VC常量) { /* reg-reg */
        //assert(!fc); XXX support offseted regs
        if (is_freg(r) && is_freg(v))
          ER(0x53, 0, rr, freg(v), freg(v), bt == VT_双精度 ? 0x11 : 0x10); //fsgnj.[sd] RR, V, V == fmv.[sd] RR, V
        else if (is_ireg(r) && is_ireg(v))
          EI(0x13, 0, rr, ireg(v), 0); // addi RR, V, 0 == mv RR, V
        else {
            int func7 = is_ireg(r) ? 0x70 : 0x78;
            if (size == 8)
              func7 |= 1;
            assert(size == 4 || size == 8);
            o(0x53 | (rr << 7) | ((is_freg(v) ? freg(v) : ireg(v)) << 15)
              | (func7 << 25)); // fmv.{w.x, x.w, d.x, x.d} RR, VR
        }
    } else if (v == VT_CMP) {
        int op = 栈顶值->cmp_op;
        int a = 栈顶值->cmp_r & 0xff;
        int b = (栈顶值->cmp_r >> 8) & 0xff;
        int inv = 0;
        switch (op) {
            case 符号_ULT:
            case 符号_UGE:
            case 符号_ULE:
            case 符_UGT:
            case 符_LT:
            case 双符号_大于等于:
            case 双符号_小于等于:
            case 符_GT:
                if (op & 1) { // remove [U]GE,GT
                    inv = 1;
                    op--;
                }
                if ((op & 7) == 6) { // [U]LE
                    int t = a; a = b; b = t;
                    inv ^= 1;
                }
                ER(0x33, (op > 符_UGT) ? 2 : 3, rr, a, b, 0); // slt[u] d, a, b
                if (inv)
                  EI(0x13, 4, rr, rr, 1); // xori d, d, 1
                break;
            case 双符号_不等于:
            case 双符号_等于:
                if (rr != a || b)
                  ER(0x33, 0, rr, a, b, 0x20); // sub d, a, b
                if (op == 双符号_不等于)
                  ER(0x33, 3, rr, 0, rr, 0); // sltu d, x0, d == snez d,d
                else
                  EI(0x13, 3, rr, rr, 1); // sltiu d, d, 1 == seqz d,d
                break;
        }
    } else if ((v & ~1) == VT_JMP) {
        int t = v & 1;
        assert(is_ireg(r));
        EI(0x13, 0, rr, 0, t);      // addi RR, x0, t
        生成跳转到_固定地址(输出代码索引 + 8);
        生成符号(fc);
        EI(0x13, 0, rr, 0, t ^ 1);  // addi RR, x0, !t
    } else
      错误_打印("unimp: 加载(non-const)");
}

静态_函数 void 存储(int r, 堆栈值 *sv)
{
    int fr = sv->r & VT_值掩码;
    int rr = is_ireg(r) ? ireg(r) : freg(r), ptrreg;
    int fc = sv->c.i;
    int bt = sv->type.t & VT_基本类型;
    int align, size = 类型_大小(&sv->type, &align);
    assert(!是_浮点型(bt) || is_freg(r) || bt == VT_长双精度);
    /* long doubles are in two integer registers, but the 加载/存储
       primitives only deal with one, so do as if it's one reg.  */
    if (bt == VT_长双精度)
      size = align = 8;
    if (bt == VT_结构体)
      错误_打印("unimp: 存储(struct)");
    if (size > 8)
      错误_打印("unimp: large sized 存储");
    assert(sv->r & VT_LVAL);
    if (fr == VT_LOCAL || (sv->r & VT_符号)) {
        ptrreg = load_symofs(-1, sv, 1);
        fc = sv->c.i;
    } else if (fr < VT_VC常量) {
        ptrreg = ireg(fr);
        /*if (((unsigned)fc + (1 << 11)) >> 12)
          错误_打印("unimp: 存储(large addend) (0x%x)", fc);*/
        fc = 0; // XXX support offsets regs
    } else
      错误_打印("implement me: %s(!local)", __FUNCTION__);
    ES(is_freg(r) ? 0x27 : 0x23,                          // fs... | s...
       size == 1 ? 0 : size == 2 ? 1 : size == 4 ? 2 : 3, // ... [wd] | [bhwd]
       ptrreg, rr, fc);                                   // RR, fc(base)
}

static void gcall_or_jmp(int docall)
{
    int tr = docall ? 1 : 5; // ra or t0
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量 &&
        ((栈顶值->r & VT_符号) && 栈顶值->c.i == (int)栈顶值->c.i)) {
        /* constant 先解析当前模块符号 case -> simple relocation */
        添加新的重定位项(当前_生成代码_段, 栈顶值->sym, 输出代码索引,
                R_RISCV_CALL_PLT, (int)栈顶值->c.i);
        o(0x17 | (tr << 7));   // auipc TR, 0 %call(func)
        EI(0x67, 0, tr, tr, 0);// jalr  TR, r(TR)
#ifdef 配置_ZHI_边界检查
        if (zhi_状态->执行_边界_检查器 &&
            (栈顶值->sym->v == 符_setjmp ||
             栈顶值->sym->v == 符__setjmp ||
             栈顶值->sym->v == 符_sigsetjmp ||
             栈顶值->sym->v == 符___sigsetjmp))
            func_bound_add_epilog = 1;
#endif
    } else if (栈顶值->r < VT_VC常量) {
        int r = ireg(栈顶值->r);
        EI(0x67, 0, tr, r, 0);      // jalr TR, 0(R)
    } else {
        int r = TREG_RA;
        加载(r, 栈顶值);
        r = ireg(r);
        EI(0x67, 0, tr, r, 0);      // jalr TR, 0(R)
    }
}

#if defined(配置_ZHI_边界检查)

static void 生成_bounds_call(int v)
{
    符号 *sym = 外部_全局_符号(v, &函数_旧_类型);

    添加新的重定位项(当前_生成代码_段, sym, 输出代码索引, R_RISCV_CALL_PLT, 0);
    o(0x17 | (1 << 7));   // auipc TR, 0 %call(func)
    EI(0x67, 0, 1, 1, 0); // jalr  TR, r(TR)
}

/* generate a bounded pointer addition */
静态_函数 void 生成_边界的_ptr_添加(void)
{
    推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_ptr_add);
    vrott(3);
    具体地址函数_调用(2);
    压入整数常量(0);
    /* returned pointer is in 寄存器_返回16位整数寄存器 */
    栈顶值->r = 寄存器_返回16位整数寄存器 | VT_有界的;
    if (不需要_代码生成)
        return;
    /* relocation offset of the bounding function call point */
    栈顶值->c.i = (当前_生成代码_段->重定位->数据_偏移 - sizeof(ElfW(Rela)));
}

/* patch pointer addition in 栈顶值 so that pointer dereferencing is
   also tested */
静态_函数 void 生成_边界的_ptr_取消引用(void)
{
    目标地址_类型 func;
    int size, align;
    ElfW(Rela) *rel;
    符号 *sym;

    if (不需要_代码生成)
        return;

    size = 类型_大小(&栈顶值->type, &align);
    switch(size) {
    case  1: func = 符___bound_ptr_间接的1; break;
    case  2: func = 符___bound_ptr_间接的2; break;
    case  4: func = 符___bound_ptr_间接的4; break;
    case  8: func = 符___bound_ptr_间接的8; break;
    case 12: func = 符___bound_ptr_间接的12; break;
    case 16: func = 符___bound_ptr_间接的16; break;
    default:
        /* may happen with struct member access */
        return;
        //错误_打印("unhandled size when dereferencing bounded pointer");
        //func = 0;
        //break;
    }
    sym = 外部_全局_符号(func, &函数_旧_类型);
    if (!sym->c)
        更新_外部_符号(sym, NULL, 0, 0);
    /* patch relocation */
    /* XXX: find a better solution ? */
    rel = (ElfW(Rela) *)(当前_生成代码_段->重定位->data + 栈顶值->c.i);
    rel->r_info = ELF64_R_INFO(sym->c, ELF64_R_TYPE(rel->r_info));
}

static void 生成_bounds_prolog(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 本地边界_部分->数据_偏移;
    func_bound_ind = 输出代码索引;
    func_bound_add_epilog = 0;
    o(0x00000013);  /* ld a0,#lbound section pointer */
    o(0x00000013);
    o(0x00000013);  /* nop -> call __bound_local_new */
    o(0x00000013);
}

static void 生成_bounds_epilog(void)
{
    static 符号 label;
    目标地址_类型 saved_ind;
    目标地址_类型 *bounds_ptr;
    符号 *sym_data;
    int offset_modified = func_bound_offset != 本地边界_部分->数据_偏移;

    if (!offset_modified && !func_bound_add_epilog)
        return;

    /* add end of table info */
    bounds_ptr = 段_ptr_添加(本地边界_部分, sizeof(目标地址_类型));
    *bounds_ptr = 0;

    sym_data = 返回_指向节的_静态符号(&字符_指针_类型, 本地边界_部分,
                           func_bound_offset, 本地边界_部分->数据_偏移);

    if (!label.v) {
        label.v = 单词表_查找(".LB0 ", 4)->单词编码;
        label.type.t = VT_无类型 | VT_静态;
    }
    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 输出代码索引;
        输出代码索引 = func_bound_ind;
        label.c = 0; /* force new local ELF symbol */
        更新_外部_符号(&label, 当前_生成代码_段, 输出代码索引, 0);
        添加新的重定位项(当前_生成代码_段, sym_data, 输出代码索引, R_RISCV_GOT_HI20, 0);
        o(0x17 | (10 << 7));    // auipc a0, 0 %pcrel_hi(sym)+addend
        添加新的重定位项(当前_生成代码_段, &label, 输出代码索引, R_RISCV_PCREL_LO12_I, 0);
        EI(0x03, 3, 10, 10, 0); // ld a0, 0(a0)
        生成_bounds_call(符___bound_local_new);
        输出代码索引 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0xe02a1101); /* addi sp,sp,-32  sd   a0,0(sp)   */
    o(0xa82ae42e); /* sd   a1,8(sp)   fsd  fa0,16(sp) */
    label.c = 0; /* force new local ELF symbol */
    更新_外部_符号(&label, 当前_生成代码_段, 输出代码索引, 0);
    添加新的重定位项(当前_生成代码_段, sym_data, 输出代码索引, R_RISCV_GOT_HI20, 0);
    o(0x17 | (10 << 7));    // auipc a0, 0 %pcrel_hi(sym)+addend
    添加新的重定位项(当前_生成代码_段, &label, 输出代码索引, R_RISCV_PCREL_LO12_I, 0);
    EI(0x03, 3, 10, 10, 0); // ld a0, 0(a0)
    生成_bounds_call(符___bound_local_delete);
    o(0x65a26502); /* ld   a0,0(sp)   ld   a1,8(sp)   */
    o(0x61052542); /* fld  fa0,16(sp) addi sp,sp,32   */
}
#endif
static void reg_pass_rec(C类型 *type, int *rc, int *fieldofs, int ofs)
{
    if ((type->t & VT_基本类型) == VT_结构体) {
        符号 *f;
        if (type->ref->type.t == VT_共用体)
          rc[0] = -1;
        else for (f = type->ref->next; f; f = f->next)
          reg_pass_rec(&f->type, rc, fieldofs, ofs + f->c);
    } else if (type->t & VT_数组) {
        if (type->ref->c < 0 || type->ref->c > 2)
          rc[0] = -1;
        else {
            int a, sz = 类型_大小(&type->ref->type, &a);
            reg_pass_rec(&type->ref->type, rc, fieldofs, ofs);
            if (rc[0] > 2 || (rc[0] == 2 && type->ref->c > 1))
              rc[0] = -1;
            else if (type->ref->c == 2 && rc[0] && rc[1] == 寄存器类_浮点) {
              rc[++rc[0]] = 寄存器类_浮点;
              fieldofs[rc[0]] = ((ofs + sz) << 4)
                                | (type->ref->type.t & VT_基本类型);
            } else if (type->ref->c == 2)
              rc[0] = -1;
        }
    } else if (rc[0] == 2 || rc[0] < 0 || (type->t & VT_基本类型) == VT_长双精度)
      rc[0] = -1;
    else if (!rc[0] || rc[1] == 寄存器类_浮点 || 是_浮点型(type->t)) {
      rc[++rc[0]] = 是_浮点型(type->t) ? 寄存器类_浮点 : 寄存器类_整数;
      fieldofs[rc[0]] = (ofs << 4) | (type->t & VT_基本类型);
    } else
      rc[0] = -1;
}

static void reg_pass(C类型 *type, int *prc, int *fieldofs, int named)
{
    prc[0] = 0;
    reg_pass_rec(type, prc, fieldofs, 0);
    if (prc[0] <= 0 || !named) {
        int align, size = 类型_大小(type, &align);
        prc[0] = (size + 7) >> 3;
        prc[1] = prc[2] = 寄存器类_整数;
        fieldofs[1] = (0 << 4) | (size <= 1 ? VT_字节 : size <= 2 ? VT_短整数 : size <= 4 ? VT_整数 : VT_长长整数);
        fieldofs[2] = (8 << 4) | (size <= 9 ? VT_字节 : size <= 10 ? VT_短整数 : size <= 12 ? VT_整数 : VT_长长整数);
    }
}

静态_函数 void 具体地址函数_调用(int 数量_args)
{
    int i, align, size, areg[2];
    int info[数量_args ? 数量_args : 1];
    int stack_adj = 0, tempspace = 0, ofs, splitofs = 0;
    堆栈值 *sv;
    符号 *sa;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成左值边界代码_函数参数加载到寄存器(数量_args);
#endif

    areg[0] = 0; /* int arg regs */
    areg[1] = 8; /* float arg regs */
    sa = 栈顶值[-数量_args].type.ref->next;
    for (i = 0; i < 数量_args; i++) {
        int nregs, byref = 0, tempofs;
        int prc[3], fieldofs[3];
        sv = &栈顶值[1 + i - 数量_args];
        sv->type.t &= ~VT_数组; // XXX this should be done in 语法分析.c
        size = 类型_大小(&sv->type, &align);
        if (size > 16) {
            if (align < XLEN)
              align = XLEN;
            tempspace = (tempspace + align - 1) & -align;
            tempofs = tempspace;
            tempspace += size;
            size = align = 8;
            byref = 64 | (tempofs << 7);
        }
        reg_pass(&sv->type, prc, fieldofs, sa != 0);
        if (!sa && align == 2*XLEN && size <= 2*XLEN)
          areg[0] = (areg[0] + 1) & ~1;
        nregs = prc[0];
        if ((prc[1] == 寄存器类_整数 && areg[0] >= 8)
            || (prc[1] == 寄存器类_浮点 && areg[1] >= 16)
            || (nregs == 2 && prc[1] == 寄存器类_浮点 && prc[2] == 寄存器类_浮点
                && areg[1] >= 15)
            || (nregs == 2 && prc[1] != prc[2]
                && (areg[1] >= 16 || areg[0] >= 8))) {
            info[i] = 32;
            if (align < XLEN)
              align = XLEN;
            stack_adj += (size + align - 1) & -align;
            if (!sa) /* one vararg on stack forces the rest on stack */
              areg[0] = 8, areg[1] = 16;
        } else {
            info[i] = areg[prc[1] - 1]++;
            if (!byref)
              info[i] |= (fieldofs[1] & VT_基本类型) << 12;
            assert(!(fieldofs[1] >> 4));
            if (nregs == 2) {
                if (prc[2] == 寄存器类_浮点 || areg[0] < 8)
                  info[i] |= (1 + areg[prc[2] - 1]++) << 7;
                else {
                    info[i] |= 16;
                    stack_adj += 8;
                }
                if (!byref) {
                    assert((fieldofs[2] >> 4) < 2048);
                    info[i] |= fieldofs[2] << (12 + 4); // includes offset
                }
            }
        }
        info[i] |= byref;
        if (sa)
          sa = sa->next;
    }
    stack_adj = (stack_adj + 15) & -16;
    tempspace = (tempspace + 15) & -16;
    if (stack_adj + tempspace) {
        EI(0x13, 0, 2, 2, -(stack_adj + tempspace));   // addi sp, sp, -adj
        for (i = ofs = 0; i < 数量_args; i++) {
            if (info[i] & (64 | 32)) {
                vrotb(数量_args - i);
                size = 类型_大小(&栈顶值->type, &align);
                if (info[i] & 64) {
                    vset(&字符_指针_类型, TREG_SP, 0);
                    压入整数常量(stack_adj + (info[i] >> 7));
                    通用_操作('+');
                    vpushv(栈顶值); // this replaces the old argument
                    vrott(3);
                    间接的();
                    栈顶值->type = 栈顶值[-1].type;
                    vswap();
                    将栈顶值_存储在堆栈左值();
                    弹出堆栈值();
                    size = align = 8;
                }
                if (info[i] & 32) {
                    if (align < XLEN)
                      align = XLEN;
                    /* Once we support offseted regs we can do this:
                       vset(&栈顶值->type, TREG_SP | VT_LVAL, ofs);
                       to construct the lvalue for the outgoing stack slot,
                       until then we have to jump through hoops.  */
                    vset(&字符_指针_类型, TREG_SP, 0);
                    ofs = (ofs + align - 1) & -align;
                    压入整数常量(ofs);
                    通用_操作('+');
                    间接的();
                    栈顶值->type = 栈顶值[-1].type;
                    vswap();
                    将栈顶值_存储在堆栈左值();
                    栈顶值->r = 栈顶值->r2 = VT_VC常量; // this arg is done
                    ofs += size;
                }
                vrott(数量_args - i);
            } else if (info[i] & 16) {
                assert(!splitofs);
                splitofs = ofs;
                ofs += 8;
            }
        }
    }
    for (i = 0; i < 数量_args; i++) {
        int ii = info[数量_args - 1 - i], r = ii, r2 = r;
        if (!(r & 32)) {
            C类型 origtype;
            int loadt;
            r &= 15;
            r2 = r2 & 64 ? 0 : (r2 >> 7) & 31;
            assert(r2 <= 16);
            vrotb(i+1);
            origtype = 栈顶值->type;
            size = 类型_大小(&栈顶值->type, &align);
            loadt = 栈顶值->type.t & VT_基本类型;
            if (loadt == VT_结构体) {
                loadt = (ii >> 12) & VT_基本类型;
            }
            if (info[数量_args - 1 - i] & 16) {
                assert(!r2);
                r2 = 1 + TREG_RA;
            }
            if (loadt == VT_长双精度) {
                assert(r2);
                r2--;
            } else if (r2) {
                测试_左值();
                vpushv(栈顶值);
            }
            栈顶值->type.t = loadt | (栈顶值->type.t & VT_无符号);
            将rc寄存器值存储在栈顶值中(r < 8 ? RC_R(r) : RC_F(r - 8));
            栈顶值->type = origtype;

            if (r2 && loadt != VT_长双精度) {
                r2--;
                assert(r2 < 16 || r2 == TREG_RA);
                vswap();
                获取栈顶值地址();
                栈顶值->type = 字符_指针_类型;
                压入整数常量(ii >> 20);
                通用_操作('+');
                间接的();
                栈顶值->type = origtype;
                loadt = 栈顶值->type.t & VT_基本类型;
                if (loadt == VT_结构体) {
                    loadt = (ii >> 16) & VT_基本类型;
                }
                将r保存到_内存堆栈_最多n个条目(r2, 1);
                栈顶值->type.t = loadt | (栈顶值->type.t & VT_无符号);
                加载(r2, 栈顶值);
                assert(r2 < VT_VC常量);
                栈顶值--;
                栈顶值->r2 = r2;
            }
            if (info[数量_args - 1 - i] & 16) {
                ES(0x23, 3, 2, ireg(栈顶值->r2), splitofs); // sd t0, ofs(sp)
                栈顶值->r2 = VT_VC常量;
            } else if (loadt == VT_长双精度 && 栈顶值->r2 != r2) {
                assert(栈顶值->r2 <= 7 && r2 <= 7);
                /* XXX we'd like to have '将rc寄存器值存储在栈顶值中' move directly into
                   the right class instead of us fixing it up.  */
                EI(0x13, 0, ireg(r2), ireg(栈顶值->r2), 0); // mv Ra+1, RR2
                栈顶值->r2 = r2;
            }
            vrott(i+1);
        }
    }
    vrotb(数量_args + 1);
    保存_寄存器最多n个堆栈条目(数量_args + 1);
    gcall_or_jmp(1);
    栈顶值 -= 数量_args + 1;
    if (stack_adj + tempspace)
      EI(0x13, 0, 2, 2, stack_adj + tempspace);      // addi sp, sp, adj
}

static int func_sub_sp_offset, num_va_regs, func_va_list_ofs;

静态_函数 void 生成函数_序言(符号 *func_sym)
{
    C类型 *func_type = &func_sym->type;
    int i, addr, align, size;
    int param_addr = 0;
    int areg[2];
    符号 *sym;
    C类型 *type;

    sym = func_type->ref;
    局部变量索引 = -16; // for ra and s0
    func_sub_sp_offset = 输出代码索引;
    输出代码索引 += 5 * 4;

    areg[0] = 0, areg[1] = 0;
    addr = 0;
    /* if the function returns by reference, then add an
       implicit pointer parameter */
    size = 类型_大小(&当前函数_返回类型, &align);
    if (size > 2 * XLEN) {
        局部变量索引 -= 8;
        函数_vc = 局部变量索引;
        ES(0x23, 3, 8, 10 + areg[0]++, 局部变量索引); // sd a0, 局部变量索引(s0)
    }
    /* define parameters */
    while ((sym = sym->next) != NULL) {
        int byref = 0;
        int regcount;
        int prc[3], fieldofs[3];
        type = &sym->type;
        size = 类型_大小(type, &align);
        if (size > 2 * XLEN) {
            type = &字符_指针_类型;
            size = align = byref = 8;
        }
        reg_pass(type, prc, fieldofs, 1);
        regcount = prc[0];
        if (areg[prc[1] - 1] >= 8
            || (regcount == 2
                && ((prc[1] == 寄存器类_浮点 && prc[2] == 寄存器类_浮点 && areg[1] >= 7)
                    || (prc[1] != prc[2] && (areg[1] >= 8 || areg[0] >= 8))))) {
            if (align < XLEN)
              align = XLEN;
            addr = (addr + align - 1) & -align;
            param_addr = addr;
            addr += size;
        } else {
            局部变量索引 -= regcount * 8; // XXX could reserve only 'size' bytes
            param_addr = 局部变量索引;
            for (i = 0; i < regcount; i++) {
                if (areg[prc[1+i] - 1] >= 8) {
                    assert(i == 1 && regcount == 2 && !(addr & 7));
                    EI(0x03, 3, 5, 8, addr); // ld t0, addr(s0)
                    addr += 8;
                    ES(0x23, 3, 8, 5, 局部变量索引 + i*8); // sd t0, 局部变量索引(s0)
                } else if (prc[1+i] == 寄存器类_浮点) {
                    ES(0x27, (size / regcount) == 4 ? 2 : 3, 8, 10 + areg[1]++, 局部变量索引 + (fieldofs[i+1] >> 4)); // fs[wd] FAi, 局部变量索引(s0)
                } else {
                    ES(0x23, 3, 8, 10 + areg[0]++, 局部变量索引 + i*8); // sd aX, 局部变量索引(s0) // XXX
                }
            }
        }
        符号_压入栈(sym->v & ~符号_字段, &sym->type,
                 (byref ? VT_LLOCAL : VT_LOCAL) | VT_LVAL,
                 param_addr);
    }
    func_va_list_ofs = addr;
    num_va_regs = 0;
    if (当前函数_可变参数) {
        for (; areg[0] < 8; areg[0]++) {
            num_va_regs++;
            ES(0x23, 3, 8, 10 + areg[0], -8 + num_va_regs * 8); // sd aX, 局部变量索引(s0)
        }
    }
#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_prolog();
#endif
}

静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret,
                       int *ret_align, int *regsize)
{
    int align, size = 类型_大小(vt, &align), nregs;
    int prc[3], fieldofs[3];
    *ret_align = 1;
    *regsize = 8;
    if (size > 16)
      return 0;
    reg_pass(vt, prc, fieldofs, 1);
    nregs = prc[0];
    if (nregs == 2 && prc[1] != prc[2])
      return -1;  /* generic code can't deal with this case */
    if (prc[1] == 寄存器类_浮点) {
        *regsize = size / nregs;
    }
    ret->t = fieldofs[1] & VT_基本类型;
    return nregs;
}

静态_函数 void arch_transfer_ret_regs(int aftercall)
{
    int prc[3], fieldofs[3];
    reg_pass(&栈顶值->type, prc, fieldofs, 1);
    assert(prc[0] == 2 && prc[1] != prc[2] && !(fieldofs[1] >> 4));
    assert(栈顶值->r == (VT_LOCAL | VT_LVAL));
    vpushv(栈顶值);
    栈顶值->type.t = fieldofs[1] & VT_基本类型;
    (aftercall ? 存储 : 加载)(prc[1] == 寄存器类_整数 ? 寄存器_返回16位整数寄存器 : 寄存器_返回浮点寄存器, 栈顶值);
    栈顶值->c.i += fieldofs[2] >> 4;
    栈顶值->type.t = fieldofs[2] & VT_基本类型;
    (aftercall ? 存储 : 加载)(prc[2] == 寄存器类_整数 ? 寄存器_返回16位整数寄存器 : 寄存器_返回浮点寄存器, 栈顶值);
    栈顶值--;
}

静态_函数 void 生成函数_结尾(void)
{
    int v, saved_ind, d, large_ofs_ind;

#ifdef 配置_ZHI_边界检查
    if (zhi_状态->执行_边界_检查器)
        生成_bounds_epilog();
#endif

    局部变量索引 = (局部变量索引 - num_va_regs * 8);
    d = v = (-局部变量索引 + 15) & -16;

    if (v >= (1 << 11)) {
        d = 16;
        o(0x37 | (5 << 7) | ((0x800 + (v-16)) & 0xfffff000)); //lui t0, upper(v)
        EI(0x13, 0, 5, 5, (v-16) << 20 >> 20); // addi t0, t0, lo(v)
        ER(0x33, 0, 2, 2, 5, 0); // add sp, sp, t0
    }
    EI(0x03, 3, 1, 2, d - 8 - num_va_regs * 8);  // ld ra, v-8(sp)
    EI(0x03, 3, 8, 2, d - 16 - num_va_regs * 8); // ld s0, v-16(sp)
    EI(0x13, 0, 2, 2, d);      // addi sp, sp, v
    EI(0x67, 0, 0, 1, 0);      // jalr x0, 0(x1), aka ret
    large_ofs_ind = 输出代码索引;
    if (v >= (1 << 11)) {
        EI(0x13, 0, 8, 2, d - num_va_regs * 8);      // addi s0, sp, d
        o(0x37 | (5 << 7) | ((0x800 + (v-16)) & 0xfffff000)); //lui t0, upper(v)
        EI(0x13, 0, 5, 5, (v-16) << 20 >> 20); // addi t0, t0, lo(v)
        ER(0x33, 0, 2, 2, 5, 0x20); // sub sp, sp, t0
        生成跳转到_固定地址(func_sub_sp_offset + 5*4);
    }
    saved_ind = 输出代码索引;

    输出代码索引 = func_sub_sp_offset;
    EI(0x13, 0, 2, 2, -d);     // addi sp, sp, -d
    ES(0x23, 3, 2, 1, d - 8 - num_va_regs * 8);  // sd ra, d-8(sp)
    ES(0x23, 3, 2, 8, d - 16 - num_va_regs * 8); // sd s0, d-16(sp)
    if (v < (1 << 11))
      EI(0x13, 0, 8, 2, d - num_va_regs * 8);      // addi s0, sp, d
    else
      生成跳转到_固定地址(large_ofs_ind);
    if ((输出代码索引 - func_sub_sp_offset) != 5*4)
      EI(0x13, 0, 0, 0, 0);      // addi x0, x0, 0 == nop
    输出代码索引 = saved_ind;
}

静态_函数 void 生成_va_开始(void)
{
    栈顶值--;
    vset(&字符_指针_类型, VT_LOCAL, func_va_list_ofs);
}

静态_函数 void 生成_fill_nops(int bytes)
{
    if ((bytes & 3))
      错误_打印("alignment of code section not multiple of 4");
    while (bytes > 0) {
        EI(0x13, 0, 0, 0, 0);      // addi x0, x0, 0 == nop
        bytes -= 4;
    }
}

// Generate forward branch to label:
静态_函数 int 生成跳转到标签(int t)
{
    if (不需要_代码生成)
      return t;
    o(t);
    return 输出代码索引 - 4;
}

// Generate branch to known address:
静态_函数 void 生成跳转到_固定地址(int a)
{
    uint32_t r = a - 输出代码索引, imm;
    if ((r + (1 << 21)) & ~((1U << 22) - 2)) {
        o(0x17 | (5 << 7) | (((r + 0x800) & 0xfffff000))); // lui RR, up(r)
        r = (int)r << 20 >> 20;
        EI(0x67, 0, 0, 5, r);      // jalr x0, r(t0)
    } else {
        imm = (((r >> 12) &  0xff) << 12)
            | (((r >> 11) &     1) << 20)
            | (((r >>  1) & 0x3ff) << 21)
            | (((r >> 20) &     1) << 31);
        o(0x6f | imm); // jal x0, imm ==  j imm
    }
}

静态_函数 int g跳转_条件(int op, int t)
{
    int tmp;
    int a = 栈顶值->cmp_r & 0xff;
    int b = (栈顶值->cmp_r >> 8) & 0xff;
    switch (op) {
        case 符号_ULT: op = 6; break;
        case 符号_UGE: op = 7; break;
        case 符号_ULE: op = 7; tmp = a; a = b; b = tmp; break;
        case 符_UGT: op = 6; tmp = a; a = b; b = tmp; break;
        case 符_LT:  op = 4; break;
        case 双符号_大于等于:  op = 5; break;
        case 双符号_小于等于:  op = 5; tmp = a; a = b; b = tmp; break;
        case 符_GT:  op = 4; tmp = a; a = b; b = tmp; break;
        case 双符号_不等于:  op = 1; break;
        case 双符号_等于:  op = 0; break;
    }
    o(0x63 | (op ^ 1) << 12 | a << 15 | b << 20 | 8 << 7); // bOP a,b,+4
    return 生成跳转到标签(t);
}

静态_函数 int g跳转_附件(int n, int t)
{
    void *p;
    /* insert jump list n into t */
    if (n) {
        uint32_t n1 = n, n2;
        while ((n2 = 读32le(p = 当前_生成代码_段->data + n1)))
            n1 = n2;
        写32le(p, t);
        t = n;
    }
    return t;
}

static void 生成_opil(int op, int ll)
{
    int a, b, d;
    int func3 = 0;
    ll = ll ? 0 : 8;
    if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
        int fc = 栈顶值->c.i;
        if (fc == 栈顶值->c.i && !(((unsigned)fc + (1 << 11)) >> 12)) {
            int cll = 0;
            vswap();
            将rc寄存器值存储在栈顶值中(寄存器类_整数);
            a = ireg(栈顶值[0].r);
            --栈顶值;
            d = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            ++栈顶值;
            vswap();
            switch (op) {
                case '-':
                    if (fc <= -(1 << 11))
                      break;
                    fc = -fc;
                case '+':
                    func3 = 0; // addi d, a, fc
                do_cop:
                    EI(0x13 | cll, func3, ireg(d), a, fc);
                    --栈顶值;
                    if (op >= 符号_ULT && op <= 符_GT) {
                      vset_VT_CMP(双符号_不等于);
                      栈顶值->cmp_r = ireg(d) | 0 << 8;
                    } else
                      栈顶值[0].r = d;
                    return;
                case 双符号_小于等于:
                    if (fc >= (1 << 11) - 1)
                      break;
                    ++fc;
                case 符_LT:  func3 = 2; goto do_cop; // slti d, a, fc
                case 符号_ULE:
                    if (fc >= (1 << 11) - 1)
                      break;
                    ++fc;
                case 符号_ULT: func3 = 3; goto do_cop; // sltiu d, a, fc
                case '^':     func3 = 4; goto do_cop; // xori d, a, fc
                case '|':     func3 = 6; goto do_cop; // ori  d, a, fc
                case '&':     func3 = 7; goto do_cop; // andi d, a, fc
                case 双符号_左位移: func3 = 1; fc &= 63; goto do_cop; // slli d, a, fc
                case 符_SHR: func3 = 5; cll = ll; fc &= 63; goto do_cop; // srli d, a, fc
                case 双符号_右位移: func3 = 5; cll = ll; fc = 1024 | (fc & 63); goto do_cop;

                case 符号_UGE:
                case 符_UGT:
                case 双符号_大于等于:
                case 符_GT:
                    生成_opil(op - 1, ll);
                    栈顶值->cmp_op ^= 1;
                    return;

                case 双符号_不等于:
                case 双符号_等于:
                    if (fc)
                      生成_opil('-', ll), a = ireg(栈顶值++->r);
                    --栈顶值;
                    vset_VT_CMP(op);
                    栈顶值->cmp_r = a | 0 << 8;
                    return;
            }
        }
    }
    将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
    a = ireg(栈顶值[-1].r);
    b = ireg(栈顶值[0].r);
    栈顶值 -= 2;
    d = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
    栈顶值++;
    栈顶值[0].r = d;
    d = ireg(d);
    switch (op) {
    default:
        if (op >= 符号_ULT && op <= 符_GT) {
            vset_VT_CMP(op);
            栈顶值->cmp_r = a | b << 8;
            break;
        }
        错误_打印("implement me: %s(%s)", __FUNCTION__, 取_单词字符串(op, NULL));
        break;

    case '+':
        ER(0x33, 0, d, a, b, 0); // add d, a, b
        break;
    case '-':
        ER(0x33, 0, d, a, b, 0x20); // sub d, a, b
        break;
    case 双符号_右位移:
        ER(0x33 | ll, 5, d, a, b, 0x20); // sra d, a, b
        break;
    case 符_SHR:
        ER(0x33 | ll, 5, d, a, b, 0); // srl d, a, b
        break;
    case 双符号_左位移:
        ER(0x33, 1, d, a, b, 0); // sll d, a, b
        break;
    case '*':
        ER(0x33, 0, d, a, b, 1); // mul d, a, b
        break;
    case '/':
        ER(0x33, 4, d, a, b, 1); // div d, a, b
        break;
    case '&':
        ER(0x33, 7, d, a, b, 0); // and d, a, b
        break;
    case '^':
        ER(0x33, 4, d, a, b, 0); // xor d, a, b
        break;
    case '|':
        ER(0x33, 6, d, a, b, 0); // or d, a, b
        break;
    case '%':
        ER(0x33, 6, d, a, b, 1); // rem d, a, b
        break;
    case 符_无符取模运算:
        ER(0x33, 7, d, a, b, 1); // remu d, a, b
        break;
    case 符_指针除法:
    case 符_无符除法:
        ER(0x33, 5, d, a, b, 1); // divu d, a, b
        break;
    }
}

静态_函数 void 生成_整数二进制运算(int op)
{
    生成_opil(op, 0);
}

静态_函数 void 生成_独立于CPU的长时间操作(int op)
{
    生成_opil(op, 1);
}

静态_函数 void 生成_浮点运算(int op)
{
    int rs1, rs2, rd, dbl, invert;
    if (栈顶值[0].type.t == VT_长双精度) {
        C类型 type = 栈顶值[0].type;
        int func = 0;
        int cond = -1;
        switch (op) {
        case '*': func = 符___multf3; break;
        case '+': func = 符___addtf3; break;
        case '-': func = 符___subtf3; break;
        case '/': func = 符___divtf3; break;
        case 双符号_等于: func = 符___eqtf2; cond = 1; break;
        case 双符号_不等于: func = 符___netf2; cond = 0; break;
        case 符_LT: func = 符___lttf2; cond = 10; break;
        case 双符号_大于等于: func = 符___getf2; cond = 11; break;
        case 双符号_小于等于: func = 符___letf2; cond = 12; break;
        case 符_GT: func = 符___gttf2; cond = 13; break;
        default: assert(0); break;
        }
        推送对_全局符号V_的引用(&函数_旧_类型, func);
        vrott(3);
        具体地址函数_调用(2);
        压入整数常量(0);
        栈顶值->r = 寄存器_返回16位整数寄存器;
        栈顶值->r2 = cond < 0 ? TREG_R(1) : VT_VC常量;
        if (cond < 0)
            栈顶值->type = type;
        else {
            压入整数常量(0);
            生成_opil(op, 1);
        }
        return;
    }

    将rc寄存器值存储在栈顶值中2(寄存器类_浮点, 寄存器类_浮点);
    assert(栈顶值->type.t == VT_双精度 || 栈顶值->type.t == VT_浮点);
    dbl = 栈顶值->type.t == VT_双精度;
    rs1 = freg(栈顶值[-1].r);
    rs2 = freg(栈顶值->r);
    栈顶值--;
    invert = 0;
    switch(op) {
    default:
        assert(0);
    case '+':
        op = 0; // fadd
    arithop:
        rd = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点);
        栈顶值->r = rd;
        rd = freg(rd);
        ER(0x53, 7, rd, rs1, rs2, dbl | (op << 2)); // fop.[sd] RD, RS1, RS2 (dyn rm)
        break;
    case '-':
        op = 1; // fsub
        goto arithop;
    case '*':
        op = 2; // fmul
        goto arithop;
    case '/':
        op = 3; // fdiv
        goto arithop;
    case 双符号_等于:
        op = 2; // EQ
    cmpop:
        rd = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
        栈顶值->r = rd;
        rd = ireg(rd);
        ER(0x53, op, rd, rs1, rs2, dbl | 0x50); // fcmp.[sd] RD, RS1, RS2 (op == eq/lt/le)
        if (invert)
          EI(0x13, 4, rd, rd, 1); // xori RD, 1
        break;
    case 双符号_不等于:
        invert = 1;
        op = 2; // EQ
        goto cmpop;
    case 符_LT:
        op = 1; // LT
        goto cmpop;
    case 双符号_小于等于:
        op = 0; // LE
        goto cmpop;
    case 符_GT:
        op = 1; // LT
        rd = rs1, rs1 = rs2, rs2 = rd;
        goto cmpop;
    case 双符号_大于等于:
        op = 0; // LE
        rd = rs1, rs1 = rs2, rs2 = rd;
        goto cmpop;
    }
}

静态_函数 void 生成_cvt_sxtw(void)
{
    /* XXX on risc-v the registers are usually sign-extended already.
       Let's try to not do anything here.  */
}

静态_函数 void 生成_整数转换为浮点(int t)
{
    int rr = ireg(将rc寄存器值存储在栈顶值中(寄存器类_整数)), dr;
    int u = 栈顶值->type.t & VT_无符号;
    int l = (栈顶值->type.t & VT_基本类型) == VT_长长整数;
    if (t == VT_长双精度) {
        int func = l ?
          (u ? 符___floatunditf : 符___floatditf) :
          (u ? 符___floatunsitf : 符___floatsitf);
        推送对_全局符号V_的引用(&函数_旧_类型, func);
        vrott(2);
        具体地址函数_调用(1);
        压入整数常量(0);
        栈顶值->type.t = t;
        栈顶值->r = 寄存器_返回16位整数寄存器;
        栈顶值->r2 = TREG_R(1);
    } else {
        栈顶值--;
        dr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点);
        栈顶值++;
        栈顶值->r = dr;
        dr = freg(dr);
        EIu(0x53, 7, dr, rr, ((0x68 | (t == VT_双精度 ? 1 : 0)) << 5) | (u ? 1 : 0) | (l ? 2 : 0)); // fcvt.[sd].[wl][u]
    }
}

静态_函数 void 生成_浮点转换为整数(int t)
{
    int ft = 栈顶值->type.t & VT_基本类型;
    int l = (t & VT_基本类型) == VT_长长整数;
    int u = t & VT_无符号;
    if (ft == VT_长双精度) {
        int func = l ?
          (u ? 符___fixunstfdi : 符___fixtfdi) :
          (u ? 符___fixunstfsi : 符___fixtfsi);
        推送对_全局符号V_的引用(&函数_旧_类型, func);
        vrott(2);
        具体地址函数_调用(1);
        压入整数常量(0);
        栈顶值->type.t = t;
        栈顶值->r = 寄存器_返回16位整数寄存器;
    } else {
        int rr = freg(将rc寄存器值存储在栈顶值中(寄存器类_浮点)), dr;
        栈顶值--;
        dr = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
        栈顶值++;
        栈顶值->r = dr;
        dr = ireg(dr);
        EIu(0x53, 1, dr, rr, ((0x60 | (ft == VT_双精度 ? 1 : 0)) << 5) | (u ? 1 : 0) | (l ? 2 : 0)); // fcvt.[wl][u].[sd] rtz
    }
}

静态_函数 void 生成_浮点转换为另一种浮点(int dt)
{
    int st = 栈顶值->type.t & VT_基本类型, rs, rd;
    dt &= VT_基本类型;
    if (st == dt)
      return;
    if (dt == VT_长双精度 || st == VT_长双精度) {
        int func = (dt == VT_长双精度) ?
            (st == VT_浮点 ? 符___extendsftf2 : 符___extenddftf2) :
            (dt == VT_浮点 ? 符___trunctfsf2 : 符___trunctfdf2);
        /* We can't use 具体地址函数_调用, as 函数_旧_类型 works like vararg
           functions, and on riscv unnamed float args are passed like
           integers.  But we really need them in the float argument registers
           for extendsftf2/extenddftf2.  So, do it explicitely.  */
        保存_寄存器最多n个堆栈条目(1);
        if (dt == VT_长双精度)
          将rc寄存器值存储在栈顶值中(RC_F(0));
        else {
            将rc寄存器值存储在栈顶值中(RC_R(0));
            assert(栈顶值->r2 < 7);
            if (栈顶值->r2 != 1 + 栈顶值->r) {
                EI(0x13, 0, ireg(栈顶值->r) + 1, ireg(栈顶值->r2), 0); // mv Ra+1, RR2
                栈顶值->r2 = 1 + 栈顶值->r;
            }
        }
        推送对_全局符号V_的引用(&函数_旧_类型, func);
        gcall_or_jmp(1);
        栈顶值 -= 2;
        压入整数常量(0);
        栈顶值->type.t = dt;
        if (dt == VT_长双精度)
          栈顶值->r = 寄存器_返回16位整数寄存器, 栈顶值->r2 = 寄存器_返回16位整数寄存器+1;
        else
          栈顶值->r = 寄存器_返回浮点寄存器;
    } else {
        assert (dt == VT_浮点 || dt == VT_双精度);
        assert (st == VT_浮点 || st == VT_双精度);
        rs = 将rc寄存器值存储在栈顶值中(寄存器类_浮点);
        rd = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点);
        if (dt == VT_双精度)
          EI(0x53, 0, freg(rd), freg(rs), 0x21 << 5); // fcvt.d.s RD, RS (no rm)
        else
          EI(0x53, 7, freg(rd), freg(rs), (0x20 << 5) | 1); // fcvt.s.d RD, RS (dyn rm)
        栈顶值->r = rd;
    }
}

静态_函数 void g去向(void)
{
    gcall_or_jmp(0);
    栈顶值--;
}

静态_函数 void 生成_vla_sp_保存(int addr)
{
    ES(0x23, 3, 8, 2, addr); // sd sp, fc(s0)
}

静态_函数 void 生成_vla_sp_恢复(int addr)
{
    EI(0x03, 3, 2, 8, addr); // ld sp, fc(s0)
}

静态_函数 void 生成_vla_分配(C类型 *type, int align)
{
    int rr;
#if defined(配置_ZHI_边界检查)
    if (zhi_状态->执行_边界_检查器)
        vpushv(栈顶值);
#endif
    rr = ireg(将rc寄存器值存储在栈顶值中(寄存器类_整数));
    EI(0x13, 0, rr, rr, 15);   // addi RR, RR, 15
    EI(0x13, 7, rr, rr, -16);  // andi, RR, RR, -16
    ER(0x33, 0, 2, 2, rr, 0x20); // sub sp, sp, rr
    弹出堆栈值();
#if defined(配置_ZHI_边界检查)
    if (zhi_状态->执行_边界_检查器) {
        压入整数常量(0);
        栈顶值->r = TREG_R(0);
        o(0x00010513); /* mv a0,sp */
        vswap();
        推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_new_region);
        vrott(3);
        具体地址函数_调用(2);
        func_bound_add_epilog = 1;
    }
#endif
}
#endif
