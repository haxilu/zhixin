/*
 *  TMS320C67xx code generator for ZHI
 * 
 */

#ifdef TARGET_DEFS_ONLY

/* #define ASSEMBLY_LISTING_C67 */

/* number of available registers */
#define 可用_寄存器数            24

/* a register can belong to several classes. The classes must be
   sorted from more general to more precise (see 将rc寄存器值存储在栈顶值中2() code which does
   assumptions on it). */
#define 寄存器类_整数     0x0001	/* generic integer register */
#define 寄存器类_浮点   0x0002	/* generic float register */
#define RC_EAX     0x0004
#define 寄存器类_堆栈0     0x0008
#define RC_ECX     0x0010
#define RC_EDX     0x0020
#define RC_INT_BSIDE  0x00000040	/* generic integer register  on b side */
#define RC_C67_A4     0x00000100
#define RC_C67_A5     0x00000200
#define RC_C67_B4     0x00000400
#define RC_C67_B5     0x00000800
#define RC_C67_A6     0x00001000
#define RC_C67_A7     0x00002000
#define RC_C67_B6     0x00004000
#define RC_C67_B7     0x00008000
#define RC_C67_A8     0x00010000
#define RC_C67_A9     0x00020000
#define RC_C67_B8     0x00040000
#define RC_C67_B9     0x00080000
#define RC_C67_A10    0x00100000
#define RC_C67_A11    0x00200000
#define RC_C67_B10    0x00400000
#define RC_C67_B11    0x00800000
#define RC_C67_A12    0x01000000
#define RC_C67_A13    0x02000000
#define RC_C67_B12    0x04000000
#define RC_C67_B13    0x08000000
#define 寄存器类_返回整数寄存器    RC_C67_A4	/* function return: integer register */
#define RC_IRE2    RC_C67_A5	/* function return: second integer register */
#define 寄存器类_返回浮点寄存器    RC_C67_A4	/* function return: float register */

/* pretty names for the registers */
enum {
    TREG_EAX = 0,		// really A2
    TREG_ECX,			// really A3
    TREG_EDX,			// really B0
    TREG_ST0,			// really B1
    TREG_C67_A4,
    TREG_C67_A5,
    TREG_C67_B4,
    TREG_C67_B5,
    TREG_C67_A6,
    TREG_C67_A7,
    TREG_C67_B6,
    TREG_C67_B7,
    TREG_C67_A8,
    TREG_C67_A9,
    TREG_C67_B8,
    TREG_C67_B9,
    TREG_C67_A10,
    TREG_C67_A11,
    TREG_C67_B10,
    TREG_C67_B11,
    TREG_C67_A12,
    TREG_C67_A13,
    TREG_C67_B12,
    TREG_C67_B13,
};

/* return registers for function */
#define 寄存器_返回16位整数寄存器 TREG_C67_A4	/* single word int return register */
#define 寄存器_返回32位整数寄存器 TREG_C67_A5    /* second word return register (for long long) */
#define 寄存器_返回浮点寄存器 TREG_C67_A4	/* float return register */

/* defined if function parameters must be evaluated in reverse order */
/* #define 相反顺序_函数_参数 */

/* defined if structures are passed as pointers. Otherwise structures
   are directly pushed on stack. */
/* #define 函数_STRUCT_PARAM_AS_PTR */

/* pointer size, in bytes */
#define 指针_大小 4

/* long double size and alignment, in bytes */
#define 长双精度_大小  12
#define 长双精度_对齐 4
/* maximum alignment (for aligned attribute support) */
#define MAX_ALIGN     8

/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局_使用
#include "zhi.h"

静态_外部 const int 寄存器_类数[可用_寄存器数] = {
    /* eax */ 寄存器类_整数 | 寄存器类_浮点 | RC_EAX, 
    // only allow even regs for floats (allow for doubles)
    /* ecx */ 寄存器类_整数 | RC_ECX,
    /* edx */ 寄存器类_整数 | RC_INT_BSIDE | 寄存器类_浮点 | RC_EDX,
    // only allow even regs for floats (allow for doubles)
    /* st0 */ 寄存器类_整数 | RC_INT_BSIDE | 寄存器类_堆栈0,
    /* A4  */ RC_C67_A4,
    /* A5  */ RC_C67_A5,
    /* B4  */ RC_C67_B4,
    /* B5  */ RC_C67_B5,
    /* A6  */ RC_C67_A6,
    /* A7  */ RC_C67_A7,
    /* B6  */ RC_C67_B6,
    /* B7  */ RC_C67_B7,
    /* A8  */ RC_C67_A8,
    /* A9  */ RC_C67_A9,
    /* B8  */ RC_C67_B8,
    /* B9  */ RC_C67_B9,
    /* A10  */ RC_C67_A10,
    /* A11  */ RC_C67_A11,
    /* B10  */ RC_C67_B10,
    /* B11  */ RC_C67_B11,
    /* A12  */ RC_C67_A10,
    /* A13  */ RC_C67_A11,
    /* B12  */ RC_C67_B10,
    /* B13  */ RC_C67_B11
};

// although zhi thinks it is passing parameters on the stack,
// the C67 really passes up to the first 10 params in special
// regs or regs pairs (for 64 bit params).  So keep track of
// the stack offsets so we can translate to the appropriate 
// reg (pair)

#define NoCallArgsPassedOnStack 10
int NoOfCurFuncArgs;
int TranslateStackToReg[NoCallArgsPassedOnStack];
int ParamLocOnStack[NoCallArgsPassedOnStack];
int TotalBytesPushedOnStack;

#ifndef FALSE
# define FALSE 0
# define TRUE 1
#endif

#undef BOOL
#define BOOL int

#define ALWAYS_ASSERT(x) \
do {\
   if (!(x))\
       错误_打印("internal compiler error file at %s:%d", __FILE__, __LINE__);\
} while (0)

/******************************************************/
static unsigned long func_sub_sp_offset;
static int func_ret_sub;

static BOOL C67_invert_test;
static int C67_compare_reg;

#ifdef ASSEMBLY_LISTING_C67
FILE *f = NULL;
#endif

void C67_g(int c)
{
    int ind1;
    if (不需要_代码生成)
        return;
#ifdef ASSEMBLY_LISTING_C67
    fprintf(f, " %08X", c);
#endif
    ind1 = 输出代码索引 + 4;
    if (ind1 > (int) 当前_生成代码_段->data_allocated)
	节_重新分配内存(当前_生成代码_段, ind1);
    当前_生成代码_段->data[输出代码索引] = c & 0xff;
    当前_生成代码_段->data[输出代码索引 + 1] = (c >> 8) & 0xff;
    当前_生成代码_段->data[输出代码索引 + 2] = (c >> 16) & 0xff;
    当前_生成代码_段->data[输出代码索引 + 3] = (c >> 24) & 0xff;
    输出代码索引 = ind1;
}


/* output a symbol and patch all calls to it */
void 生成符号_地址(int t, int a)
{
    int n, *ptr;
    while (t) {
	ptr = (int *) (当前_生成代码_段->data + t);
	{
	    符号 *sym;

	    // extract 32 bit address from MVKH/MVKL
	    n = ((*ptr >> 7) & 0xffff);
	    n |= ((*(ptr + 1) >> 7) & 0xffff) << 16;

	    // define a label that will be relocated

	    sym = 返回_指向节的_静态符号(&字符_指针_类型, 当前_生成代码_段, a, 0);
	    段部分符号重定位(当前_生成代码_段, sym, t, R_C60LO16);
	    段部分符号重定位(当前_生成代码_段, sym, t + 4, R_C60HI16);

	    // clear out where the pointer was

	    *ptr &= ~(0xffff << 7);
	    *(ptr + 1) &= ~(0xffff << 7);
	}
	t = n;
    }
}

// these are regs that zhi doesn't really know about, 
// but assign them unique values so the mapping routines
// can distinguish them

#define C67_A0 105
#define C67_SP 106
#define C67_B3 107
#define C67_FP 108
#define C67_B2 109
#define C67_CREG_ZERO -1	/* Special code for no condition reg test */


int ConvertRegToRegClass(int r)
{
    // only works for A4-B13

    return RC_C67_A4 << (r - TREG_C67_A4);
}


// map ZHI reg to C67 reg number

int C67_map_regn(int r)
{
    if (r == 0)			// normal zhi regs
	return 0x2;		// A2
    else if (r == 1)		// normal zhi regs
	return 3;		// A3
    else if (r == 2)		// normal zhi regs
	return 0;		// B0
    else if (r == 3)		// normal zhi regs
	return 1;		// B1
    else if (r >= TREG_C67_A4 && r <= TREG_C67_B13)	// these form a pattern of alt pairs
	return (((r & 0xfffffffc) >> 1) | (r & 1)) + 2;
    else if (r == C67_A0)
	return 0;		// set to A0 (offset reg)
    else if (r == C67_B2)
	return 2;		// set to B2 (offset reg)
    else if (r == C67_B3)
	return 3;		// set to B3 (return address reg)
    else if (r == C67_SP)
	return 15;		// set to SP (B15) (offset reg)
    else if (r == C67_FP)
	return 15;		// set to FP (A15) (offset reg)
    else if (r == C67_CREG_ZERO)
	return 0;		// Special code for no condition reg test
    else
	ALWAYS_ASSERT(FALSE);

    return 0;
}

// mapping from zhi reg number to 
// C67 register to condition code field
//
// valid condition code regs are:
//
// zhi reg 2 ->B0 -> 1
// zhi reg 3 ->B1 -> 2
// zhi reg 0 -> A2 -> 5
// zhi reg 1 -> A3 -> X
// zhi reg      B2 -> 3

int C67_map_regc(int r)
{
    if (r == 0)			// normal zhi regs
	return 0x5;
    else if (r == 2)		// normal zhi regs
	return 0x1;
    else if (r == 3)		// normal zhi regs
	return 0x2;
    else if (r == C67_B2)	// normal zhi regs
	return 0x3;
    else if (r == C67_CREG_ZERO)
	return 0;		// Special code for no condition reg test
    else
	ALWAYS_ASSERT(FALSE);

    return 0;
}


// map ZHI reg to C67 reg side A or B

int C67_map_regs(int r)
{
    if (r == 0)			// normal zhi regs
	return 0x0;
    else if (r == 1)		// normal zhi regs
	return 0x0;
    else if (r == 2)		// normal zhi regs
	return 0x1;
    else if (r == 3)		// normal zhi regs
	return 0x1;
    else if (r >= TREG_C67_A4 && r <= TREG_C67_B13)	// these form a pattern of alt pairs
	return (r & 2) >> 1;
    else if (r == C67_A0)
	return 0;		// set to A side 
    else if (r == C67_B2)
	return 1;		// set to B side 
    else if (r == C67_B3)
	return 1;		// set to B side
    else if (r == C67_SP)
	return 0x1;		// set to SP (B15) B side 
    else if (r == C67_FP)
	return 0x0;		// set to FP (A15) A side 
    else
	ALWAYS_ASSERT(FALSE);

    return 0;
}

int C67_map_S12(char *s)
{
    if (strstr(s, ".S1") != NULL)
	return 0;
    else if (strcmp(s, ".S2"))
	return 1;
    else
	ALWAYS_ASSERT(FALSE);

    return 0;
}

int C67_map_D12(char *s)
{
    if (strstr(s, ".D1") != NULL)
	return 0;
    else if (strcmp(s, ".D2"))
	return 1;
    else
	ALWAYS_ASSERT(FALSE);

    return 0;
}



void C67_asm(char *s, int a, int b, int c)
{
    BOOL xpath;

#ifdef ASSEMBLY_LISTING_C67
    if (!f) {
	f = fopen("ZHI67_out.txt", "wt");
    }
    fprintf(f, "%04X ", 输出代码索引);
#endif

    if (strstr(s, "MVKL") == s) {
	C67_g((C67_map_regn(b) << 23) |
	      ((a & 0xffff) << 7) | (0x0a << 2) | (C67_map_regs(b) << 1));
    } else if (strstr(s, "MVKH") == s) {
	C67_g((C67_map_regn(b) << 23) |
	      (((a >> 16) & 0xffff) << 7) |
	      (0x1a << 2) | (C67_map_regs(b) << 1));
    } else if (strstr(s, "STW.D SP POST DEC") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//SP B15
	      (2 << 13) |	//ucst5 (must keep 8 byte boundary !!)
	      (0xa << 9) |	//mode a = post dec ucst
	      (0 << 8) |	//r (LDDW bit 0)
	      (1 << 7) |	//y D1/D2 use B side
	      (7 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STB.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2 A side
	      (3 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STH.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2 A side
	      (5 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STB.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2 A side
	      (3 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STH.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2 A side
	      (5 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STW.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2 A side
	      (7 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STW.D *") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (C67_map_regn(b) << 18) |	//base reg A0
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(b) << 7) |	//y D1/D2 base reg side
	      (7 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STH.D *") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (C67_map_regn(b) << 18) |	//base reg A0
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(b) << 7) |	//y D1/D2 base reg side
	      (5 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STB.D *") == s) {
	C67_g((C67_map_regn(a) << 23) |	//src
	      (C67_map_regn(b) << 18) |	//base reg A0
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(b) << 7) |	//y D1/D2 base reg side
	      (3 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "STW.D +*") == s) {
	ALWAYS_ASSERT(c < 32);
	C67_g((C67_map_regn(a) << 23) |	//src
	      (C67_map_regn(b) << 18) |	//base reg A0
	      (c << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(b) << 7) |	//y D1/D2 base reg side
	      (7 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of src
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDW.D SP PRE INC") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg B15
	      (2 << 13) |	//ucst5 (must keep 8 byte boundary)
	      (9 << 9) |	//mode 9 = pre inc ucst5
	      (0 << 8) |	//r (LDDW bit 0)
	      (1 << 7) |	//y D1/D2  B side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDDW.D SP PRE INC") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg B15
	      (1 << 13) |	//ucst5 (must keep 8 byte boundary)
	      (9 << 9) |	//mode 9 = pre inc ucst5
	      (1 << 8) |	//r (LDDW bit 1)
	      (1 << 7) |	//y D1/D2  B side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDW.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2  A side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDDW.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (1 << 8) |	//r (LDDW bit 1)
	      (0 << 7) |	//y D1/D2  A side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDH.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2  A side
	      (4 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDB.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2  A side
	      (2 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDHU.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2  A side
	      (0 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDBU.D *+SP[A0]") == s) {
	C67_g((C67_map_regn(a) << 23) |	//dst
	      (15 << 18) |	//base reg A15
	      (0 << 13) |	//offset reg A0
	      (5 << 9) |	//mode 5 = pos offset, base reg + off reg
	      (0 << 8) |	//r (LDDW bit 0)
	      (0 << 7) |	//y D1/D2  A side
	      (1 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(a) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDW.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDDW.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (1 << 8) |	//r (LDDW bit 1)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDH.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (4 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDB.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (2 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDHU.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (0 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDBU.D *") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (0 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (1 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "LDW.D +*") == s) {
	C67_g((C67_map_regn(b) << 23) |	//dst
	      (C67_map_regn(a) << 18) |	//base reg A15
	      (1 << 13) |	//cst5
	      (1 << 9) |	//mode 1 = pos cst offset
	      (0 << 8) |	//r (LDDW bit 0)
	      (C67_map_regs(a) << 7) |	//y D1/D2  src side
	      (6 << 4) |	//ldst 3=STB, 5=STH 5, 7=STW, 6=LDW 4=LDH 2=LDB 0=LDHU 1=LDBU 
	      (1 << 2) |	//opcode
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPLTSP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x3a << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPGTSP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x39 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPEQSP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x38 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    }

    else if (strstr(s, "CMPLTDP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x2a << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPGTDP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x29 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPEQDP") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x28 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPLT") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x57 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPGT") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x47 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPEQ") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x53 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPLTU") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x5f << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "CMPGTU") == s) {
	xpath = C67_map_regs(a) ^ C67_map_regs(b);
	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1
	      (xpath << 12) |	//x use cross path for src2
	      (0x4f << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side for reg c
	      (0 << 0));	//parallel
    } else if (strstr(s, "B DISP") == s) {
	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//z
	      (a << 7) |	//cnst
	      (0x4 << 2) |	//opcode fixed
	      (0 << 1) |	//S0/S1
	      (0 << 0));	//parallel
    } else if (strstr(s, "B.") == s) {
	xpath = C67_map_regs(c) ^ 1;

	C67_g((C67_map_regc(b) << 29) |	//creg
	      (a << 28) |	//inv
	      (0 << 23) |	//dst
	      (C67_map_regn(c) << 18) |	//src2
	      (0 << 13) |	//
	      (xpath << 12) |	//x cross path if !B side
	      (0xd << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (1 << 1) |	//must be S2
	      (0 << 0));	//parallel
    } else if (strstr(s, "MV.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (0 << 13) |	//src1 (cst5)
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x2 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SPTRUNC.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0xb << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "DPTRUNC.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      ((C67_map_regn(b) + 1) << 18) |	//src2   WEIRD CPU must specify odd reg for some reason
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x1 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "INTSP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2   
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x4a << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "INTSPU.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2  
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x49 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "INTDP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2  
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x39 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "INTDPU.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      ((C67_map_regn(b) + 1) << 18) |	//src2   WEIRD CPU must specify odd reg for some reason
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x3b << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SPDP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (0 << 13) |	//src1 NA
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x2 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "DPSP.L") == s) {
	ALWAYS_ASSERT(C67_map_regs(b) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      ((C67_map_regn(b) + 1) << 18) |	//src2 WEIRD CPU must specify odd reg for some reason
	      (0 << 13) |	//src1 NA
	      (0 << 12) |	//x cross path if opposite sides
	      (0x9 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "ADD.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x3 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SUB.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x7 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "OR.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x7f << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "AND.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x7b << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "XOR.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x6f << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "ADDSP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x10 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "ADDDP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x18 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SUBSP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x11 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SUBDP.L") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x19 << 5) |	//opcode
	      (0x6 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "MPYSP.M") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x1c << 7) |	//opcode
	      (0x0 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "MPYDP.M") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2 (possible x path)
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x0e << 7) |	//opcode
	      (0x0 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "MPYI.M") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(a) == C67_map_regs(c));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1 (cst5)
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x4 << 7) |	//opcode
	      (0x0 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SHR.S") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x37 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SHRU.S") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x27 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "SHL.S") == s) {
	xpath = C67_map_regs(b) ^ C67_map_regs(c);

	ALWAYS_ASSERT(C67_map_regs(c) == C67_map_regs(a));

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(c) << 23) |	//dst
	      (C67_map_regn(b) << 18) |	//src2
	      (C67_map_regn(a) << 13) |	//src1 
	      (xpath << 12) |	//x cross path if opposite sides
	      (0x33 << 6) |	//opcode
	      (0x8 << 2) |	//opcode fixed
	      (C67_map_regs(c) << 1) |	//side of dest
	      (0 << 0));	//parallel
    } else if (strstr(s, "||ADDK") == s) {
	xpath = 0;		// no xpath required just use the side of the src/dst

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(b) << 23) |	//dst
	      (a << 07) |	//scst16
	      (0x14 << 2) |	//opcode fixed
	      (C67_map_regs(b) << 1) |	//side of dst
	      (1 << 0));	//parallel
    } else if (strstr(s, "ADDK") == s) {
	xpath = 0;		// no xpath required just use the side of the src/dst

	C67_g((0 << 29) |	//creg
	      (0 << 28) |	//inv
	      (C67_map_regn(b) << 23) |	//dst
	      (a << 07) |	//scst16
	      (0x14 << 2) |	//opcode fixed
	      (C67_map_regs(b) << 1) |	//side of dst
	      (0 << 0));	//parallel
    } else if (strstr(s, "NOP") == s) {
	C67_g(((a - 1) << 13) |	//no of cycles
	      (0 << 0));	//parallel
    } else
	ALWAYS_ASSERT(FALSE);

#ifdef ASSEMBLY_LISTING_C67
    fprintf(f, " %s %d %d %d\n", s, a, b, c);
#endif

}

//r=reg to 加载, fr=from reg, symbol for relocation, constant

void C67_MVKL(int r, int fc)
{
    C67_asm("MVKL.", fc, r, 0);
}

void C67_MVKH(int r, int fc)
{
    C67_asm("MVKH.", fc, r, 0);
}

void C67_STB_SP_A0(int r)
{
    C67_asm("STB.D *+SP[A0]", r, 0, 0);	// STB  r,*+SP[A0]
}

void C67_STH_SP_A0(int r)
{
    C67_asm("STH.D *+SP[A0]", r, 0, 0);	// STH  r,*+SP[A0]
}

void C67_STW_SP_A0(int r)
{
    C67_asm("STW.D *+SP[A0]", r, 0, 0);	// STW  r,*+SP[A0]
}

void C67_STB_PTR(int r, int r2)
{
    C67_asm("STB.D *", r, r2, 0);	// STB  r, *r2
}

void C67_STH_PTR(int r, int r2)
{
    C67_asm("STH.D *", r, r2, 0);	// STH  r, *r2
}

void C67_STW_PTR(int r, int r2)
{
    C67_asm("STW.D *", r, r2, 0);	// STW  r, *r2
}

void C67_STW_PTR_PRE_INC(int r, int r2, int n)
{
    C67_asm("STW.D +*", r, r2, n);	// STW  r, *+r2
}

void C67_PUSH(int r)
{
    C67_asm("STW.D SP POST DEC", r, 0, 0);	// STW  r,*SP--
}

void C67_LDW_SP_A0(int r)
{
    C67_asm("LDW.D *+SP[A0]", r, 0, 0);	// LDW  *+SP[A0],r
}

void C67_LDDW_SP_A0(int r)
{
    C67_asm("LDDW.D *+SP[A0]", r, 0, 0);	// LDDW  *+SP[A0],r
}

void C67_LDH_SP_A0(int r)
{
    C67_asm("LDH.D *+SP[A0]", r, 0, 0);	// LDH  *+SP[A0],r
}

void C67_LDB_SP_A0(int r)
{
    C67_asm("LDB.D *+SP[A0]", r, 0, 0);	// LDB  *+SP[A0],r
}

void C67_LDHU_SP_A0(int r)
{
    C67_asm("LDHU.D *+SP[A0]", r, 0, 0);	// LDHU  *+SP[A0],r
}

void C67_LDBU_SP_A0(int r)
{
    C67_asm("LDBU.D *+SP[A0]", r, 0, 0);	// LDBU  *+SP[A0],r
}

void C67_LDW_PTR(int r, int r2)
{
    C67_asm("LDW.D *", r, r2, 0);	// LDW  *r,r2
}

void C67_LDDW_PTR(int r, int r2)
{
    C67_asm("LDDW.D *", r, r2, 0);	// LDDW  *r,r2
}

void C67_LDH_PTR(int r, int r2)
{
    C67_asm("LDH.D *", r, r2, 0);	// LDH  *r,r2
}

void C67_LDB_PTR(int r, int r2)
{
    C67_asm("LDB.D *", r, r2, 0);	// LDB  *r,r2
}

void C67_LDHU_PTR(int r, int r2)
{
    C67_asm("LDHU.D *", r, r2, 0);	// LDHU  *r,r2
}

void C67_LDBU_PTR(int r, int r2)
{
    C67_asm("LDBU.D *", r, r2, 0);	// LDBU  *r,r2
}

void C67_LDW_PTR_PRE_INC(int r, int r2)
{
    C67_asm("LDW.D +*", r, r2, 0);	// LDW  *+r,r2
}

void C67_POP(int r)
{
    C67_asm("LDW.D SP PRE INC", r, 0, 0);	// LDW  *++SP,r
}

void C67_POP_DW(int r)
{
    C67_asm("LDDW.D SP PRE INC", r, 0, 0);	// LDDW  *++SP,r
}

void C67_CMPLT(int 状态机1, int s2, int dst)
{
    C67_asm("CMPLT.L1", 状态机1, s2, dst);
}

void C67_CMPGT(int 状态机1, int s2, int dst)
{
    C67_asm("CMPGT.L1", 状态机1, s2, dst);
}

void C67_CMPEQ(int 状态机1, int s2, int dst)
{
    C67_asm("CMPEQ.L1", 状态机1, s2, dst);
}

void C67_CMPLTU(int 状态机1, int s2, int dst)
{
    C67_asm("CMPLTU.L1", 状态机1, s2, dst);
}

void C67_CMPGTU(int 状态机1, int s2, int dst)
{
    C67_asm("CMPGTU.L1", 状态机1, s2, dst);
}


void C67_CMPLTSP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPLTSP.S1", 状态机1, s2, dst);
}

void C67_CMPGTSP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPGTSP.S1", 状态机1, s2, dst);
}

void C67_CMPEQSP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPEQSP.S1", 状态机1, s2, dst);
}

void C67_CMPLTDP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPLTDP.S1", 状态机1, s2, dst);
}

void C67_CMPGTDP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPGTDP.S1", 状态机1, s2, dst);
}

void C67_CMPEQDP(int 状态机1, int s2, int dst)
{
    C67_asm("CMPEQDP.S1", 状态机1, s2, dst);
}


void C67_IREG_B_REG(int inv, int r1, int r2)	// [!R] B  r2
{
    C67_asm("B.S2", inv, r1, r2);
}


// call with how many 32 bit words to 跳过
// (0 would branch to the branch instruction)

void C67_B_DISP(int disp)	//  B  +2  Branch with constant displacement
{
    // Branch point is relative to the 8 word fetch packet
    //
    // we will assume the text section always starts on an 8 word (32 byte boundary)
    //
    // so add in how many words into the fetch packet the branch is


    C67_asm("B DISP", disp + ((输出代码索引 & 31) >> 2), 0, 0);
}

void C67_NOP(int n)
{
    C67_asm("NOP", n, 0, 0);
}

void C67_ADDK(int n, int r)
{
    ALWAYS_ASSERT(abs(n) < 32767);

    C67_asm("ADDK", n, r, 0);
}

void C67_ADDK_PARALLEL(int n, int r)
{
    ALWAYS_ASSERT(abs(n) < 32767);

    C67_asm("||ADDK", n, r, 0);
}

void C67_Adjust_ADDK(int *inst, int n)
{
    ALWAYS_ASSERT(abs(n) < 32767);

    *inst = (*inst & (~(0xffff << 7))) | ((n & 0xffff) << 7);
}

void C67_MV(int r, int v)
{
    C67_asm("MV.L", 0, r, v);
}


void C67_DPTRUNC(int r, int v)
{
    C67_asm("DPTRUNC.L", 0, r, v);
}

void C67_SPTRUNC(int r, int v)
{
    C67_asm("SPTRUNC.L", 0, r, v);
}

void C67_INTSP(int r, int v)
{
    C67_asm("INTSP.L", 0, r, v);
}

void C67_INTDP(int r, int v)
{
    C67_asm("INTDP.L", 0, r, v);
}

void C67_INTSPU(int r, int v)
{
    C67_asm("INTSPU.L", 0, r, v);
}

void C67_INTDPU(int r, int v)
{
    C67_asm("INTDPU.L", 0, r, v);
}

void C67_SPDP(int r, int v)
{
    C67_asm("SPDP.L", 0, r, v);
}

void C67_DPSP(int r, int v)	// note regs must be on the same side
{
    C67_asm("DPSP.L", 0, r, v);
}

void C67_ADD(int r, int v)
{
    C67_asm("ADD.L", v, r, v);
}

void C67_SUB(int r, int v)
{
    C67_asm("SUB.L", v, r, v);
}

void C67_AND(int r, int v)
{
    C67_asm("AND.L", v, r, v);
}

void C67_OR(int r, int v)
{
    C67_asm("OR.L", v, r, v);
}

void C67_XOR(int r, int v)
{
    C67_asm("XOR.L", v, r, v);
}

void C67_ADDSP(int r, int v)
{
    C67_asm("ADDSP.L", v, r, v);
}

void C67_SUBSP(int r, int v)
{
    C67_asm("SUBSP.L", v, r, v);
}

void C67_MPYSP(int r, int v)
{
    C67_asm("MPYSP.M", v, r, v);
}

void C67_ADDDP(int r, int v)
{
    C67_asm("ADDDP.L", v, r, v);
}

void C67_SUBDP(int r, int v)
{
    C67_asm("SUBDP.L", v, r, v);
}

void C67_MPYDP(int r, int v)
{
    C67_asm("MPYDP.M", v, r, v);
}

void C67_MPYI(int r, int v)
{
    C67_asm("MPYI.M", v, r, v);
}

void C67_SHL(int r, int v)
{
    C67_asm("SHL.S", r, v, v);
}

void C67_SHRU(int r, int v)
{
    C67_asm("SHRU.S", r, v, v);
}

void C67_SHR(int r, int v)
{
    C67_asm("SHR.S", r, v, v);
}



/* 加载 'r' from value 'sv' */
void 加载(int r, 堆栈值 * sv)
{
    int v, t, ft, fc, fr, size = 0, element;
    BOOL Unsigned = FALSE;
    堆栈值 v1;

    fr = sv->r;
    ft = sv->type.t;
    fc = sv->c.i;

    v = fr & VT_值掩码;
    if (fr & VT_LVAL) {
	if (v == VT_LLOCAL) {
	    v1.type.t = VT_整数;
	    v1.r = VT_LOCAL | VT_LVAL;
	    v1.c.i = fc;
	    加载(r, &v1);
	    fr = r;
	} else if ((ft & VT_基本类型) == VT_长双精度) {
	    错误_打印("long double not supported");
	} else if ((ft & VT_类型) == VT_字节) {
	    size = 1;
	} else if ((ft & VT_类型) == (VT_字节 | VT_无符号)) {
	    size = 1;
	    Unsigned = TRUE;
	} else if ((ft & VT_类型) == VT_短整数) {
	    size = 2;
	} else if ((ft & VT_类型) == (VT_短整数 | VT_无符号)) {
	    size = 2;
	    Unsigned = TRUE;
	} else if ((ft & VT_基本类型) == VT_双精度) {
	    size = 8;
	} else {
	    size = 4;
	}

	// check if fc is a positive reference on the stack, 
	// if it is zhi is referencing what it thinks is a parameter
	// on the stack, so check if it is really in a register.


	if (v == VT_LOCAL && fc > 0) {
	    int stack_pos = 8;

	    for (t = 0; t < NoCallArgsPassedOnStack; t++) {
		if (fc == stack_pos)
		    break;

		stack_pos += TranslateStackToReg[t];
	    }

	    // param has been pushed on stack, get it like a local var

	    fc = ParamLocOnStack[t] - 8;
	}

	if ((fr & VT_值掩码) < VT_VC常量)	// check for pure 间接的ect
	{
	    if (size == 1) {
		if (Unsigned)
		    C67_LDBU_PTR(v, r);	// LDBU  *v,r
		else
		    C67_LDB_PTR(v, r);	// LDB  *v,r
	    } else if (size == 2) {
		if (Unsigned)
		    C67_LDHU_PTR(v, r);	// LDHU  *v,r
		else
		    C67_LDH_PTR(v, r);	// LDH  *v,r
	    } else if (size == 4) {
		C67_LDW_PTR(v, r);	// LDW  *v,r
	    } else if (size == 8) {
		C67_LDDW_PTR(v, r);	// LDDW  *v,r
	    }

	    C67_NOP(4);		// NOP 4
	    return;
	} else if (fr & VT_符号) {
	    段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
	    段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引 + 4, R_C60HI16);


	    C67_MVKL(C67_A0, fc);	//r=reg to 加载,  constant
	    C67_MVKH(C67_A0, fc);	//r=reg to 加载,  constant


	    if (size == 1) {
		if (Unsigned)
		    C67_LDBU_PTR(C67_A0, r);	// LDBU  *A0,r
		else
		    C67_LDB_PTR(C67_A0, r);	// LDB  *A0,r
	    } else if (size == 2) {
		if (Unsigned)
		    C67_LDHU_PTR(C67_A0, r);	// LDHU  *A0,r
		else
		    C67_LDH_PTR(C67_A0, r);	// LDH  *A0,r
	    } else if (size == 4) {
		C67_LDW_PTR(C67_A0, r);	// LDW  *A0,r
	    } else if (size == 8) {
		C67_LDDW_PTR(C67_A0, r);	// LDDW  *A0,r
	    }

	    C67_NOP(4);		// NOP 4
	    return;
	} else {
	    element = size;

	    // divide offset in bytes to create element index
	    C67_MVKL(C67_A0, (fc / element) + 8 / element);	//r=reg to 加载,  constant
	    C67_MVKH(C67_A0, (fc / element) + 8 / element);	//r=reg to 加载,  constant

	    if (size == 1) {
		if (Unsigned)
		    C67_LDBU_SP_A0(r);	// LDBU  r, SP[A0]
		else
		    C67_LDB_SP_A0(r);	// LDB  r, SP[A0]
	    } else if (size == 2) {
		if (Unsigned)
		    C67_LDHU_SP_A0(r);	// LDHU  r, SP[A0]
		else
		    C67_LDH_SP_A0(r);	// LDH  r, SP[A0]
	    } else if (size == 4) {
		C67_LDW_SP_A0(r);	// LDW  r, SP[A0]
	    } else if (size == 8) {
		C67_LDDW_SP_A0(r);	// LDDW  r, SP[A0]
	    }


	    C67_NOP(4);		// NOP 4
	    return;
	}
    } else {
	if (v == VT_VC常量) {
	    if (fr & VT_符号) {
		段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
		段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引 + 4, R_C60HI16);
	    }
	    C67_MVKL(r, fc);	//r=reg to 加载, constant
	    C67_MVKH(r, fc);	//r=reg to 加载, constant
	} else if (v == VT_LOCAL) {
	    C67_MVKL(r, fc + 8);	//r=reg to 加载, constant C67 stack points to next free
	    C67_MVKH(r, fc + 8);	//r=reg to 加载, constant
	    C67_ADD(C67_FP, r);	// MV v,r   v -> r
	} else if (v == VT_CMP) {
	    C67_MV(C67_compare_reg, r);	// MV v,r   v -> r
	} else if (v == VT_JMP || v == VT_JMPI) {
	    t = v & 1;
	    C67_B_DISP(4);	//  Branch with constant displacement, 跳过 over this branch, 加载, nop, 加载
	    C67_MVKL(r, t);	//  r=reg to 加载, 0 or 1 (do this while branching)
	    C67_NOP(4);		//  NOP 4
	    生成符号(fc);		//  modifies other branches to branch here
	    C67_MVKL(r, t ^ 1);	//  r=reg to 加载, 0 or 1
	} else if (v != r) {
	    C67_MV(v, r);	// MV v,r   v -> r

	    if ((ft & VT_基本类型) == VT_双精度)
		C67_MV(v + 1, r + 1);	// MV v,r   v -> r
	}
    }
}


/* 存储 register 'r' in lvalue 'v' */
void 存储(int r, 堆栈值 * v)
{
    int fr, bt, ft, fc, size, t, element;

    ft = v->type.t;
    fc = v->c.i;
    fr = v->r & VT_值掩码;
    bt = ft & VT_基本类型;
    /* XXX: incorrect if float reg to reg */

    if (bt == VT_长双精度) {
	错误_打印("long double not supported");
    } else {
	if (bt == VT_短整数)
	    size = 2;
	else if (bt == VT_字节)
	    size = 1;
	else if (bt == VT_双精度)
	    size = 8;
	else
	    size = 4;

	if ((v->r & VT_值掩码) == VT_VC常量) {
	    /* constant memory reference */

	    if (v->r & VT_符号) {
		段部分符号重定位(当前_生成代码_段, v->sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
		段部分符号重定位(当前_生成代码_段, v->sym, 输出代码索引 + 4, R_C60HI16);
	    }
	    C67_MVKL(C67_A0, fc);	//r=reg to 加载,  constant
	    C67_MVKH(C67_A0, fc);	//r=reg to 加载,  constant

	    if (size == 1)
		C67_STB_PTR(r, C67_A0);	// STB  r, *A0
	    else if (size == 2)
		C67_STH_PTR(r, C67_A0);	// STH  r, *A0
	    else if (size == 4 || size == 8)
		C67_STW_PTR(r, C67_A0);	// STW  r, *A0

	    if (size == 8)
		C67_STW_PTR_PRE_INC(r + 1, C67_A0, 1);	// STW  r, *+A0[1]
	} else if ((v->r & VT_值掩码) == VT_LOCAL) {
	    // check case of storing to passed argument that
	    // zhi thinks is on the stack but for C67 is
	    // passed as a reg.  However it may have been
	    // saved to the stack, if that reg was required
	    // for a call to a child function

	    if (fc > 0)		// argument ??
	    {
		// walk through sizes and figure which param

		int stack_pos = 8;

		for (t = 0; t < NoCallArgsPassedOnStack; t++) {
		    if (fc == stack_pos)
			break;

		    stack_pos += TranslateStackToReg[t];
		}

		// param has been pushed on stack, get it like a local var
		fc = ParamLocOnStack[t] - 8;
	    }

	    if (size == 8)
		element = 4;
	    else
		element = size;

	    // divide offset in bytes to create word index
	    C67_MVKL(C67_A0, (fc / element) + 8 / element);	//r=reg to 加载,  constant
	    C67_MVKH(C67_A0, (fc / element) + 8 / element);	//r=reg to 加载,  constant



	    if (size == 1)
		C67_STB_SP_A0(r);	// STB  r, SP[A0]
	    else if (size == 2)
		C67_STH_SP_A0(r);	// STH  r, SP[A0]
	    else if (size == 4 || size == 8)
		C67_STW_SP_A0(r);	// STW  r, SP[A0]

	    if (size == 8) {
		C67_ADDK(1, C67_A0);	//  ADDK 1,A0
		C67_STW_SP_A0(r + 1);	//  STW  r, SP[A0]
	    }
	} else {
	    if (size == 1)
		C67_STB_PTR(r, fr);	// STB  r, *fr
	    else if (size == 2)
		C67_STH_PTR(r, fr);	// STH  r, *fr
	    else if (size == 4 || size == 8)
		C67_STW_PTR(r, fr);	// STW  r, *fr

	    if (size == 8) {
		C67_STW_PTR_PRE_INC(r + 1, fr, 1);	// STW  r, *+fr[1]
	    }
	}
    }
}

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
    int r;
    符号 *sym;

    if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
	/* constant case */
	if (栈顶值->r & VT_符号) {
	    /* relocation case */

	    // get add into A0, then start the jump B3

	    段部分符号重定位(当前_生成代码_段, 栈顶值->sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
	    段部分符号重定位(当前_生成代码_段, 栈顶值->sym, 输出代码索引 + 4, R_C60HI16);

	    C67_MVKL(C67_A0, 0);	//r=reg to 加载, constant
	    C67_MVKH(C67_A0, 0);	//r=reg to 加载, constant
	    C67_IREG_B_REG(0, C67_CREG_ZERO, C67_A0);	//  B.S2x  A0

	    if (is_jmp) {
		C67_NOP(5);	// simple jump, just put NOP
	    } else {
		// Call, must 加载 return address into B3 during delay slots

		sym = 返回_指向节的_静态符号(&字符_指针_类型, 当前_生成代码_段, 输出代码索引 + 12, 0);	// symbol for return address
		段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
		段部分符号重定位(当前_生成代码_段, sym, 输出代码索引 + 4, R_C60HI16);
		C67_MVKL(C67_B3, 0);	//r=reg to 加载, constant
		C67_MVKH(C67_B3, 0);	//r=reg to 加载, constant
		C67_NOP(3);	// put remaining NOPs
	    }
	} else {
	    /* put an empty PC32 relocation */
	    ALWAYS_ASSERT(FALSE);
	}
    } else {
	/* otherwise, 间接的ect call */
	r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
	C67_IREG_B_REG(0, C67_CREG_ZERO, r);	//  B.S2x  r

	if (is_jmp) {
	    C67_NOP(5);		// simple jump, just put NOP
	} else {
	    // Call, must 加载 return address into B3 during delay slots

	    sym = 返回_指向节的_静态符号(&字符_指针_类型, 当前_生成代码_段, 输出代码索引 + 12, 0);	// symbol for return address
	    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_C60LO16);	// rem the inst need to be patched
	    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引 + 4, R_C60HI16);
	    C67_MVKL(C67_B3, 0);	//r=reg to 加载, constant
	    C67_MVKH(C67_B3, 0);	//r=reg to 加载, constant
	    C67_NOP(3);		// put remaining NOPs
	}
    }
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *ret_align, int *regsize) {
    *ret_align = 1; // Never have to re-align return values for x86-64
    return 0;
}

/* generate function call with address in (栈顶值->t, 栈顶值->c) and free function
   context. Stack entry is popped */
void 具体地址函数_调用(int 数量_args)
{
    int i, r, size = 0;
    int args_sizes[NoCallArgsPassedOnStack];

    if (数量_args > NoCallArgsPassedOnStack) {
	错误_打印("more than 10 function params not currently supported");
	// handle more than 10, put some on the stack
    }

    for (i = 0; i < 数量_args; i++) {
	if ((栈顶值->type.t & VT_基本类型) == VT_结构体) {
	    ALWAYS_ASSERT(FALSE);
	} else {
	    /* simple type (currently always same size) */
	    /* XXX: implicit cast ? */


	    if ((栈顶值->type.t & VT_基本类型) == VT_长长整数) {
		错误_打印("long long not supported");
	    } else if ((栈顶值->type.t & VT_基本类型) == VT_长双精度) {
		错误_打印("long double not supported");
	    } else if ((栈顶值->type.t & VT_基本类型) == VT_双精度) {
		size = 8;
	    } else {
		size = 4;
	    }

	    // put the parameter into the corresponding reg (pair)

	    r = 将rc寄存器值存储在栈顶值中(RC_C67_A4 << (2 * i));

	    // must put on stack because with 1 pass compiler , no way to tell
	    // if an up coming nested call might overwrite these regs

	    C67_PUSH(r);

	    if (size == 8) {
		C67_STW_PTR_PRE_INC(r + 1, C67_SP, 3);	// STW  r, *+SP[3] (go back and put the other)
	    }
	    args_sizes[i] = size;
	}
	栈顶值--;
    }
    // POP all the params on the stack into registers for the
    // immediate call (in reverse order)

    for (i = 数量_args - 1; i >= 0; i--) {

	if (args_sizes[i] == 8)
	    C67_POP_DW(TREG_C67_A4 + i * 2);
	else
	    C67_POP(TREG_C67_A4 + i * 2);
    }
    gcall_or_jmp(0);
    栈顶值--;
}


// to be compatible with Code Composer for the C67
// the first 10 parameters must be passed in registers
// (pairs for 64 bits) starting wit; A4:A5, then B4:B5 and
// ending with B12:B13.
//
// When a call is made, if the caller has its parameters
// in regs A4-B13 these must be saved before/as the call 
// parameters are loaded and restored upon return (or if/when needed).

/* generate function prolog of type 't' */
void 生成函数_序言(符号 *func_sym)
{
    C类型 *func_type = &func_sym->type;
    int addr, align, size, 函数_调用, i;
    符号 *sym;
    C类型 *type;

    sym = func_type->ref;
    函数_调用 = sym->f.函数_调用;
    addr = 8;
    /* if the function returns a structure, then add an
       implicit pointer parameter */
    if ((当前函数_返回类型.t & VT_基本类型) == VT_结构体) {
	函数_vc = addr;
	addr += 4;
    }

    NoOfCurFuncArgs = 0;

    /* define parameters */
    while ((sym = sym->next) != NULL) {
	type = &sym->type;
	符号_压入栈(sym->v & ~符号_字段, type, VT_LOCAL | VT_LVAL, addr);
	size = 类型_大小(type, &align);
	size = (size + 3) & ~3;

	// keep track of size of arguments so
	// we can translate where zhi thinks they
	// are on the stack into the appropriate reg

	TranslateStackToReg[NoOfCurFuncArgs] = size;
	NoOfCurFuncArgs++;

#ifdef 函数_STRUCT_PARAM_AS_PTR
	/* structs are passed as pointer */
	if ((type->t & VT_基本类型) == VT_结构体) {
	    size = 4;
	}
#endif
	addr += size;
    }
    func_ret_sub = 0;
    /* pascal type call ? */
    if (函数_调用 == 函数_标准调用)
	func_ret_sub = addr - 8;

    C67_MV(C67_FP, C67_A0);	//  move FP -> A0
    C67_MV(C67_SP, C67_FP);	//  move SP -> FP

    // place all the args passed in regs onto the stack

    局部变量索引 = 0;
    for (i = 0; i < NoOfCurFuncArgs; i++) {

	ParamLocOnStack[i] = 局部变量索引;	// remember where the param is
	局部变量索引 += -8;

	C67_PUSH(TREG_C67_A4 + i * 2);

	if (TranslateStackToReg[i] == 8) {
	    C67_STW_PTR_PRE_INC(TREG_C67_A4 + i * 2 + 1, C67_SP, 3);	// STW  r, *+SP[1] (go back and put the other)
	}
    }

    TotalBytesPushedOnStack = -局部变量索引;

    func_sub_sp_offset = 输出代码索引;	// remember where we put the stack instruction 
    C67_ADDK(0, C67_SP);	//  ADDK.L2 局部变量索引,SP  (just put zero temporarily)

    C67_PUSH(C67_A0);
    C67_PUSH(C67_B3);
}

/* generate function epilog */
void 生成函数_结尾(void)
{
    {
	int local = (-局部变量索引 + 7) & -8;	// stack must stay aligned to 8 bytes for LDDW instr
	C67_POP(C67_B3);
	C67_NOP(4);		// NOP wait for 加载
	C67_IREG_B_REG(0, C67_CREG_ZERO, C67_B3);	//  B.S2  B3
	C67_POP(C67_FP);
	C67_ADDK(local, C67_SP);	//  ADDK.L2 局部变量索引,SP  
	C67_Adjust_ADDK((int *) (当前_生成代码_段->data +
				 func_sub_sp_offset),
			-local + TotalBytesPushedOnStack);
	C67_NOP(3);		// NOP 
    }
}

静态_函数 void 生成_fill_nops(int bytes)
{
    if ((bytes & 3))
      错误_打印("alignment of code section not multiple of 4");
    while (bytes > 0) {
	C67_NOP(4);
	bytes -= 4;
    }
}

/* generate a jump to a label */
int 生成跳转到标签(int t)
{
    int ind1 = 输出代码索引;
    if (不需要_代码生成)
        return t;

    C67_MVKL(C67_A0, t);	//r=reg to 加载,  constant
    C67_MVKH(C67_A0, t);	//r=reg to 加载,  constant
    C67_IREG_B_REG(0, C67_CREG_ZERO, C67_A0);	// [!R] B.S2x  A0
    C67_NOP(5);
    return ind1;
}

/* generate a jump to a fixed address */
void 生成跳转到_固定地址(int a)
{
    符号 *sym;
    // I guess this routine is used for relative short
    // local jumps, for now just handle it as the general
    // case

    // define a label that will be relocated

    sym = 返回_指向节的_静态符号(&字符_指针_类型, 当前_生成代码_段, a, 0);
    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_C60LO16);
    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引 + 4, R_C60HI16);

    生成跳转到标签(0);			// place a zero there later the symbol will be added to it
}

/* generate a test. set 'inv' to invert test. Stack entry is popped */
静态_函数 int g跳转_条件(int op, int t)
{
        int ind1;
        int inv = op & 1;
        if (不需要_代码生成)
            return t;

	/* fast case : can jump directly since flags are set */
	// C67 uses B2 sort of as flags register
	ind1 = 输出代码索引;
	C67_MVKL(C67_A0, t);	//r=reg to 加载, constant
	C67_MVKH(C67_A0, t);	//r=reg to 加载, constant

	if (C67_compare_reg != TREG_EAX &&	// check if not already in a conditional test reg
	    C67_compare_reg != TREG_EDX &&
	    C67_compare_reg != TREG_ST0 && C67_compare_reg != C67_B2) {
	    C67_MV(C67_compare_reg, C67_B2);
	    C67_compare_reg = C67_B2;
	}

	C67_IREG_B_REG(C67_invert_test ^ inv, C67_compare_reg, C67_A0);	// [!R] B.S2x  A0
	C67_NOP(5);
	t = ind1;		//return where we need to patch

        return t;
}

静态_函数 int g跳转_附件(int n0, int t)
{
    if (n0) {
            int n = n0, *p;
	    /* insert 栈顶值->c jump list in t */

	    // I guess the idea is to traverse to the
	    // null at the end of the list and 存储 t
	    // there
	    while (n != 0) {
		p = (int *) (当前_生成代码_段->data + n);

		// extract 32 bit address from MVKH/MVKL
		n = ((*p >> 7) & 0xffff);
		n |= ((*(p + 1) >> 7) & 0xffff) << 16;
	    }
	    *p |= (t & 0xffff) << 7;
	    *(p + 1) |= ((t >> 16) & 0xffff) << 7;
	    t = n0;
    }
    return t;
}

/* generate an integer binary operation */
void 生成_整数二进制运算(int op)
{
    int r, fr, opc, t;

    switch (op) {
    case '+':
    case 符_加进位生成:		/* add with carry generation */
	opc = 0;
      生成_op8:


// C67 can't do const compares, must 加载 into a reg
// so just go to 将rc寄存器值存储在栈顶值中2 directly - tktk



	if (op >= 符号_ULT && op <= 符_GT)
	    将rc寄存器值存储在栈顶值中2(RC_INT_BSIDE, 寄存器类_整数);	// make sure r (src1) is on the B Side of CPU
	else
	    将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);

	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;

	C67_compare_reg = C67_B2;


	if (op == 符_LT) {
	    C67_CMPLT(r, fr, C67_B2);
	    C67_invert_test = FALSE;
	} else if (op == 双符号_大于等于) {
	    C67_CMPLT(r, fr, C67_B2);
	    C67_invert_test = TRUE;
	} else if (op == 符_GT) {
	    C67_CMPGT(r, fr, C67_B2);
	    C67_invert_test = FALSE;
	} else if (op == 双符号_小于等于) {
	    C67_CMPGT(r, fr, C67_B2);
	    C67_invert_test = TRUE;
	} else if (op == 双符号_等于) {
	    C67_CMPEQ(r, fr, C67_B2);
	    C67_invert_test = FALSE;
	} else if (op == 双符号_不等于) {
	    C67_CMPEQ(r, fr, C67_B2);
	    C67_invert_test = TRUE;
	} else if (op == 符号_ULT) {
	    C67_CMPLTU(r, fr, C67_B2);
	    C67_invert_test = FALSE;
	} else if (op == 符号_UGE) {
	    C67_CMPLTU(r, fr, C67_B2);
	    C67_invert_test = TRUE;
	} else if (op == 符_UGT) {
	    C67_CMPGTU(r, fr, C67_B2);
	    C67_invert_test = FALSE;
	} else if (op == 符号_ULE) {
	    C67_CMPGTU(r, fr, C67_B2);
	    C67_invert_test = TRUE;
	} else if (op == '+')
	    C67_ADD(fr, r);	// ADD  r,fr,r
	else if (op == '-')
	    C67_SUB(fr, r);	// SUB  r,fr,r
	else if (op == '&')
	    C67_AND(fr, r);	// AND  r,fr,r
	else if (op == '|')
	    C67_OR(fr, r);	// OR  r,fr,r
	else if (op == '^')
	    C67_XOR(fr, r);	// XOR  r,fr,r
	else
	    ALWAYS_ASSERT(FALSE);

	栈顶值--;
	if (op >= 符号_ULT && op <= 符_GT)
            vset_VT_CMP(0x80);
	break;
    case '-':
    case 符_减借位生成:		/* sub with carry generation */
	opc = 5;
	goto 生成_op8;
    case 符_加进位使用:		/* add with carry use */
	opc = 2;
	goto 生成_op8;
    case 符_减借位使用:		/* sub with carry use */
	opc = 3;
	goto 生成_op8;
    case '&':
	opc = 4;
	goto 生成_op8;
    case '^':
	opc = 6;
	goto 生成_op8;
    case '|':
	opc = 1;
	goto 生成_op8;
    case '*':
    case 符_无符32位乘法:
	将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;
	栈顶值--;
	C67_MPYI(fr, r);	// 32 bit multiply  fr,r,fr
	C67_NOP(8);		// NOP 8 for worst case
	break;
    case 双符号_左位移:
	将rc寄存器值存储在栈顶值中2(RC_INT_BSIDE, RC_INT_BSIDE);	// shift amount must be on same side as dst
	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;
	栈顶值--;
	C67_SHL(fr, r);		// arithmetic/logical shift
	break;

    case 符_SHR:
	将rc寄存器值存储在栈顶值中2(RC_INT_BSIDE, RC_INT_BSIDE);	// shift amount must be on same side as dst
	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;
	栈顶值--;
	C67_SHRU(fr, r);	// logical shift
	break;

    case 双符号_右位移:
	将rc寄存器值存储在栈顶值中2(RC_INT_BSIDE, RC_INT_BSIDE);	// shift amount must be on same side as dst
	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;
	栈顶值--;
	C67_SHR(fr, r);		// arithmetic shift
	break;

    case '/':
	t = 符__divi;
      call_func:
	vswap();
	/* call generic idiv function */
	推送对_全局符号V_的引用(&函数_旧_类型, t);
	vrott(3);
	具体地址函数_调用(2);
	压入整数常量(0);
	栈顶值->r = 寄存器_返回16位整数寄存器;
	栈顶值->r2 = VT_VC常量;
	break;
    case 符_无符除法:
    case 符_指针除法:
	t = 符__divu;
	goto call_func;
    case '%':
	t = 符__remi;
	goto call_func;
    case 符_无符取模运算:
	t = 符__remu;
	goto call_func;

    default:
	opc = 7;
	goto 生成_op8;
    }
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point type */
/* XXX: need to use ST1 too */
void 生成_浮点运算(int op)
{
    int ft, fc, fr, r;

    if (op >= 符号_ULT && op <= 符_GT)
	将rc寄存器值存储在栈顶值中2(RC_EDX, RC_EAX);	// make sure src2 is on b side
    else
	将rc寄存器值存储在栈顶值中2(寄存器类_浮点, 寄存器类_浮点);	// make sure src2 is on b side

    ft = 栈顶值->type.t;
    fc = 栈顶值->c.i;
    r = 栈顶值->r;
    fr = 栈顶值[-1].r;


    if ((ft & VT_基本类型) == VT_长双精度)
	错误_打印("long doubles not supported");

    if (op >= 符号_ULT && op <= 符_GT) {

	r = 栈顶值[-1].r;
	fr = 栈顶值[0].r;

	C67_compare_reg = C67_B2;

	if (op == 符_LT) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPLTDP(r, fr, C67_B2);
	    else
		C67_CMPLTSP(r, fr, C67_B2);

	    C67_invert_test = FALSE;
	} else if (op == 双符号_大于等于) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPLTDP(r, fr, C67_B2);
	    else
		C67_CMPLTSP(r, fr, C67_B2);

	    C67_invert_test = TRUE;
	} else if (op == 符_GT) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPGTDP(r, fr, C67_B2);
	    else
		C67_CMPGTSP(r, fr, C67_B2);

	    C67_invert_test = FALSE;
	} else if (op == 双符号_小于等于) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPGTDP(r, fr, C67_B2);
	    else
		C67_CMPGTSP(r, fr, C67_B2);

	    C67_invert_test = TRUE;
	} else if (op == 双符号_等于) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPEQDP(r, fr, C67_B2);
	    else
		C67_CMPEQSP(r, fr, C67_B2);

	    C67_invert_test = FALSE;
	} else if (op == 双符号_不等于) {
	    if ((ft & VT_基本类型) == VT_双精度)
		C67_CMPEQDP(r, fr, C67_B2);
	    else
		C67_CMPEQSP(r, fr, C67_B2);

	    C67_invert_test = TRUE;
	} else {
	    ALWAYS_ASSERT(FALSE);
	}
        vset_VT_CMP(0x80);
    } else {
	if (op == '+') {
	    if ((ft & VT_基本类型) == VT_双精度) {
		C67_ADDDP(r, fr);	// ADD  fr,r,fr
		C67_NOP(6);
	    } else {
		C67_ADDSP(r, fr);	// ADD  fr,r,fr
		C67_NOP(3);
	    }
	    栈顶值--;
	} else if (op == '-') {
	    if ((ft & VT_基本类型) == VT_双精度) {
		C67_SUBDP(r, fr);	// SUB  fr,r,fr
		C67_NOP(6);
	    } else {
		C67_SUBSP(r, fr);	// SUB  fr,r,fr
		C67_NOP(3);
	    }
	    栈顶值--;
	} else if (op == '*') {
	    if ((ft & VT_基本类型) == VT_双精度) {
		C67_MPYDP(r, fr);	// MPY  fr,r,fr
		C67_NOP(9);
	    } else {
		C67_MPYSP(r, fr);	// MPY  fr,r,fr
		C67_NOP(3);
	    }
	    栈顶值--;
	} else if (op == '/') {
	    if ((ft & VT_基本类型) == VT_双精度) {
		// must call intrinsic DP floating point divide
		vswap();
		/* call generic idiv function */
		推送对_全局符号V_的引用(&函数_旧_类型, 符__divd);
		vrott(3);
		具体地址函数_调用(2);
		压入整数常量(0);
		栈顶值->r = 寄存器_返回浮点寄存器;
		栈顶值->r2 = 寄存器_返回32位整数寄存器;

	    } else {
		// must call intrinsic SP floating point divide
		vswap();
		/* call generic idiv function */
		推送对_全局符号V_的引用(&函数_旧_类型, 符__divf);
		vrott(3);
		具体地址函数_调用(2);
		压入整数常量(0);
		栈顶值->r = 寄存器_返回浮点寄存器;
		栈顶值->r2 = VT_VC常量;
	    }
	} else
	    ALWAYS_ASSERT(FALSE);


    }
}


/* convert integers to fp 't' type. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
void 生成_整数转换为浮点(int t)
{
    int r;

    将rc寄存器值存储在栈顶值中(寄存器类_整数);
    r = 栈顶值->r;

    if ((t & VT_基本类型) == VT_双精度) {
	if (t & VT_无符号)
	    C67_INTDPU(r, r);
	else
	    C67_INTDP(r, r);

	C67_NOP(4);
	栈顶值->type.t = VT_双精度;
    } else {
	if (t & VT_无符号)
	    C67_INTSPU(r, r);
	else
	    C67_INTSP(r, r);
	C67_NOP(3);
	栈顶值->type.t = VT_浮点;
    }

}

/* convert fp to int 't' type */
/* XXX: handle long long case */
void 生成_浮点转换为整数(int t)
{
    int r;

    将rc寄存器值存储在栈顶值中(寄存器类_浮点);
    r = 栈顶值->r;

    if (t != VT_整数)
	错误_打印("long long not supported");
    else {
	if ((栈顶值->type.t & VT_基本类型) == VT_双精度) {
	    C67_DPTRUNC(r, r);
	    C67_NOP(3);
	} else {
	    C67_SPTRUNC(r, r);
	    C67_NOP(3);
	}

	栈顶值->type.t = VT_整数;

    }
}

/* convert from one floating point type to another */
void 生成_浮点转换为另一种浮点(int t)
{
    int r, r2;

    if ((栈顶值->type.t & VT_基本类型) == VT_双精度 &&
	(t & VT_基本类型) == VT_浮点) {
	// convert double to float

	将rc寄存器值存储在栈顶值中(寄存器类_浮点);		// get it in a register pair

	r = 栈顶值->r;

	C67_DPSP(r, r);		// convert it to SP same register
	C67_NOP(3);

	栈顶值->type.t = VT_浮点;
	栈顶值->r2 = VT_VC常量;	// set this as unused
    } else if ((栈顶值->type.t & VT_基本类型) == VT_浮点 &&
	       (t & VT_基本类型) == VT_双精度) {
	// convert float to double

	将rc寄存器值存储在栈顶值中(寄存器类_浮点);		// get it in a register

	r = 栈顶值->r;

	if (r == TREG_EAX) {	// make sure the paired reg is avail
	    r2 = 查找释放的rc寄存器_如果不存在就保存一个寄存器(RC_ECX);
	} else if (r == TREG_EDX) {
	    r2 = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_堆栈0);
	} else {
	    ALWAYS_ASSERT(FALSE);
            r2 = 0; /* avoid warning */
        }

	C67_SPDP(r, r);		// convert it to DP same register
	C67_NOP(1);

	栈顶值->type.t = VT_双精度;
	栈顶值->r2 = r2;		// set this as unused
    } else {
	ALWAYS_ASSERT(FALSE);
    }
}

/* computed goto support */
void g去向(void)
{
    gcall_or_jmp(1);
    栈顶值--;
}

/* Save the stack pointer onto the stack and return the 位置 of its address */
静态_函数 void 生成_vla_sp_保存(int addr) {
    错误_打印("variable length arrays unsupported for this target");
}

/* Restore the SP from a 位置 on the stack */
静态_函数 void 生成_vla_sp_恢复(int addr) {
    错误_打印("variable length arrays unsupported for this target");
}

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态_函数 void 生成_vla_分配(C类型 *type, int align) {
    错误_打印("variable length arrays unsupported for this target");
}

/* end of C67 code generator */
/*************************************************************/
#endif
/*************************************************************/
