/*
 *  ARMv4 code generator for ZHI
 *
 */

#ifdef TARGET_DEFS_ONLY

#if defined(ZHI_ARM_EABI) && !defined(ZHI_ARM_VFP)
#error "Currently TinyCC only supports float computation with VFP instructions"
#endif

/* number of available registers */
#ifdef ZHI_ARM_VFP
#define 可用_寄存器数            13
#else
#define 可用_寄存器数             9
#endif

#ifndef ZHI_CPU_VERSION
# define ZHI_CPU_VERSION 5
#endif

/* a register can belong to several classes. The classes must be
   sorted from more general to more precise (see 将rc寄存器值存储在栈顶值中2() code which does
   assumptions on it). */
#define 寄存器类_整数     0x0001 /* generic integer register */
#define 寄存器类_浮点   0x0002 /* generic float register */
#define RC_R0      0x0004
#define RC_R1      0x0008
#define RC_R2      0x0010
#define RC_R3      0x0020
#define RC_R12     0x0040
#define RC_F0      0x0080
#define RC_F1      0x0100
#define RC_F2      0x0200
#define RC_F3      0x0400
#ifdef ZHI_ARM_VFP
#define RC_F4      0x0800
#define RC_F5      0x1000
#define RC_F6      0x2000
#define RC_F7      0x4000
#endif
#define 寄存器类_返回整数寄存器    RC_R0  /* function return: integer register */
#define RC_IRE2    RC_R1  /* function return: second integer register */
#define 寄存器类_返回浮点寄存器    RC_F0  /* function return: float register */

/* pretty names for the registers */
enum {
    TREG_R0 = 0,
    TREG_R1,
    TREG_R2,
    TREG_R3,
    TREG_R12,
    TREG_F0,
    TREG_F1,
    TREG_F2,
    TREG_F3,
#ifdef ZHI_ARM_VFP
    TREG_F4,
    TREG_F5,
    TREG_F6,
    TREG_F7,
#endif
    TREG_SP = 13,
    TREG_LR,
};

#ifdef ZHI_ARM_VFP
#define T2CPR(t) (((t) & VT_基本类型) != VT_浮点 ? 0x100 : 0)
#endif

/* return registers for function */
#define 寄存器_返回16位整数寄存器 TREG_R0 /* single word int return register */
#define 寄存器_返回32位整数寄存器 TREG_R1 /* second word return register (for long long) */
#define 寄存器_返回浮点寄存器 TREG_F0 /* float return register */

#ifdef ZHI_ARM_EABI
#define 符___divdi3 符___aeabi_ldivmod
#define 符___moddi3 符___aeabi_ldivmod
#define 符___udivdi3 符___aeabi_uldivmod
#define 符___umoddi3 符___aeabi_uldivmod
#endif

/* defined if function parameters must be evaluated in reverse order */
#define 相反顺序_函数_参数

/* defined if structures are passed as pointers. Otherwise structures
   are directly pushed on stack. */
/* #define 函数_STRUCT_PARAM_AS_PTR */

/* pointer size, in bytes */
#define 指针_大小 4

/* long double size and alignment, in bytes */
#ifdef ZHI_ARM_VFP
#define 长双精度_大小  8
#endif

#ifndef 长双精度_大小
#define 长双精度_大小  8
#endif

#ifdef ZHI_ARM_EABI
#define 长双精度_对齐 8
#else
#define 长双精度_对齐 4
#endif

/* maximum alignment (for aligned attribute support) */
#define MAX_ALIGN     8

#define 字符串_是_无符号

/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局_使用
#include "zhi.h"

enum 浮动_abi 浮动_abi;

静态_外部 const int 寄存器_类数[可用_寄存器数] = {
    /* r0 */ 寄存器类_整数 | RC_R0,
    /* r1 */ 寄存器类_整数 | RC_R1,
    /* r2 */ 寄存器类_整数 | RC_R2,
    /* r3 */ 寄存器类_整数 | RC_R3,
    /* r12 */ 寄存器类_整数 | RC_R12,
    /* f0 */ 寄存器类_浮点 | RC_F0,
    /* f1 */ 寄存器类_浮点 | RC_F1,
    /* f2 */ 寄存器类_浮点 | RC_F2,
    /* f3 */ 寄存器类_浮点 | RC_F3,
#ifdef ZHI_ARM_VFP
 /* d4/s8 */ 寄存器类_浮点 | RC_F4,
/* d5/s10 */ 寄存器类_浮点 | RC_F5,
/* d6/s12 */ 寄存器类_浮点 | RC_F6,
/* d7/s14 */ 寄存器类_浮点 | RC_F7,
#endif
};

static int func_sub_sp_offset, last_itod_magic;
static int leaffunc;

#if defined(配置_ZHI_边界检查)
static 目标地址_类型 func_bound_offset;
static unsigned long func_bound_ind;
static int func_bound_add_epilog;
#endif

#if defined(ZHI_ARM_EABI) && defined(ZHI_ARM_VFP)
static C类型 float_type, double_type, func_float_type, func_double_type;
静态_函数 void arm_初始化(知心状态机 *s)
{
    float_type.t = VT_浮点;
    double_type.t = VT_双精度;
    func_float_type.t = VT_函数;
    func_float_type.ref = 符号_压入栈(符号_字段, &float_type, 函数_CDECL, 函数_旧);
    func_double_type.t = VT_函数;
    func_double_type.ref = 符号_压入栈(符号_字段, &double_type, 函数_CDECL, 函数_旧);

    浮动_abi = s->浮动_abi;
#ifndef ZHI_ARM_HARDFLOAT
# warning "soft float ABI currently not supported: default to softfp"
#endif
}
#else
#define func_float_type 函数_旧_类型
#define func_double_type 函数_旧_类型
#define func_ldouble_type 函数_旧_类型
静态_函数 void arm_初始化(知心状态机 *s)
{
#if 0
#if !defined (ZHI_ARM_VFP)
    zhi_警告("Support for FPA is deprecated and will be removed in next"
                " release");
#endif
#if !defined (ZHI_ARM_EABI)
    zhi_警告("Support for OABI is deprecated and will be removed in next"
                " release");
#endif
#endif
}
#endif

#define CHECK_R(r) ((r) >= TREG_R0 && (r) <= TREG_LR)

static int two2mask(int a,int b) {
  if (!CHECK_R(a) || !CHECK_R(b))
    错误_打印("compiler error! registers %i,%i is not valid",a,b);
  return (寄存器_类数[a]|寄存器_类数[b])&~(寄存器类_整数|寄存器类_浮点);
}

static int regmask(int r) {
  if (!CHECK_R(r))
    错误_打印("compiler error! register %i is not valid",r);
  return 寄存器_类数[r]&~(寄存器类_整数|寄存器类_浮点);
}

/******************************************************/

#if defined(ZHI_ARM_EABI) && !defined(配置_ZHI_ELF解释程序路径)
const char *默认_elf插入(知心状态机 *s)
{
    if (s->浮动_abi == ARM_HARD_FLOAT)
        return "/lib/ld-linux-armhf.so.3";
    else
        return "/lib/ld-linux.so.3";
}
#endif

void o(uint32_t i)
{
  /* this is a good place to start adding big-endian support*/
  int ind1;
  if (不需要_代码生成)
    return;
  ind1 = 输出代码索引 + 4;
  if (!当前_生成代码_段)
    错误_打印("compiler error! This happens f.ex. if the compiler\n"
         "can't evaluate constant expressions outside of a function.");
  if (ind1 > 当前_生成代码_段->data_allocated)
    节_重新分配内存(当前_生成代码_段, ind1);
  当前_生成代码_段->data[输出代码索引++] = i&255;
  i>>=8;
  当前_生成代码_段->data[输出代码索引++] = i&255;
  i>>=8;
  当前_生成代码_段->data[输出代码索引++] = i&255;
  i>>=8;
  当前_生成代码_段->data[输出代码索引++] = i;
}

static uint32_t stuff_const(uint32_t op, uint32_t c)
{
  int try_neg=0;
  uint32_t nc = 0, negop = 0;

  switch(op&0x1F00000)
  {
    case 0x800000: //add
    case 0x400000: //sub
      try_neg=1;
      negop=op^0xC00000;
      nc=-c;
      break;
    case 0x1A00000: //mov
    case 0x1E00000: //mvn
      try_neg=1;
      negop=op^0x400000;
      nc=~c;
      break;
    case 0x200000: //xor
      if(c==~0)
	return (op&0xF010F000)|((op>>16)&0xF)|0x1E00000;
      break;
    case 0x0: //and
      if(c==~0)
	return (op&0xF010F000)|((op>>16)&0xF)|0x1A00000;
    case 0x1C00000: //bic
      try_neg=1;
      negop=op^0x1C00000;
      nc=~c;
      break;
    case 0x1800000: //orr
      if(c==~0)
	return (op&0xFFF0FFFF)|0x1E00000;
      break;
  }
  do {
    uint32_t m;
    int i;
    if(c<256) /* catch undefined <<32 */
      return op|c;
    for(i=2;i<32;i+=2) {
      m=(0xff>>i)|(0xff<<(32-i));
      if(!(c&~m))
	return op|(i<<7)|(c<<i)|(c>>(32-i));
    }
    op=negop;
    c=nc;
  } while(try_neg--);
  return 0;
}


//only add,sub
void stuff_const_harder(uint32_t op, uint32_t v) {
  uint32_t x;
  x=stuff_const(op,v);
  if(x)
    o(x);
  else {
    uint32_t a[16], nv, no, o2, n2;
    int i,j,k;
    a[0]=0xff;
    o2=(op&0xfff0ffff)|((op&0xf000)<<4);;
    for(i=1;i<16;i++)
      a[i]=(a[i-1]>>2)|(a[i-1]<<30);
    for(i=0;i<12;i++)
      for(j=i<4?i+12:15;j>=i+4;j--)
	if((v&(a[i]|a[j]))==v) {
	  o(stuff_const(op,v&a[i]));
	  o(stuff_const(o2,v&a[j]));
	  return;
	}
    no=op^0xC00000;
    n2=o2^0xC00000;
    nv=-v;
    for(i=0;i<12;i++)
      for(j=i<4?i+12:15;j>=i+4;j--)
	if((nv&(a[i]|a[j]))==nv) {
	  o(stuff_const(no,nv&a[i]));
	  o(stuff_const(n2,nv&a[j]));
	  return;
	}
    for(i=0;i<8;i++)
      for(j=i+4;j<12;j++)
	for(k=i<4?i+12:15;k>=j+4;k--)
	  if((v&(a[i]|a[j]|a[k]))==v) {
	    o(stuff_const(op,v&a[i]));
	    o(stuff_const(o2,v&a[j]));
	    o(stuff_const(o2,v&a[k]));
	    return;
	  }
    no=op^0xC00000;
    nv=-v;
    for(i=0;i<8;i++)
      for(j=i+4;j<12;j++)
	for(k=i<4?i+12:15;k>=j+4;k--)
	  if((nv&(a[i]|a[j]|a[k]))==nv) {
	    o(stuff_const(no,nv&a[i]));
	    o(stuff_const(n2,nv&a[j]));
	    o(stuff_const(n2,nv&a[k]));
	    return;
	  }
    o(stuff_const(op,v&a[0]));
    o(stuff_const(o2,v&a[4]));
    o(stuff_const(o2,v&a[8]));
    o(stuff_const(o2,v&a[12]));
  }
}

uint32_t encbranch(int pos, int addr, int fail)
{
  addr-=pos+8;
  addr/=4;
  if(addr>=0x1000000 || addr<-0x1000000) {
    if(fail)
      错误_打印("FIXME: function bigger than 32MB");
    return 0;
  }
  return 0x0A000000|(addr&0xffffff);
}

int decbranch(int pos)
{
  int x;
  x=*(uint32_t *)(当前_生成代码_段->data + pos);
  x&=0x00ffffff;
  if(x&0x800000)
    x-=0x1000000;
  return x*4+pos+8;
}

/* output a symbol and patch all calls to it */
void 生成符号_地址(int t, int a)
{
  uint32_t *x;
  int lt;
  while(t) {
    x=(uint32_t *)(当前_生成代码_段->data + t);
    t=decbranch(lt=t);
    if(a==lt+4)
      *x=0xE1A00000; // nop
    else {
      *x &= 0xff000000;
      *x |= encbranch(lt,a,1);
    }
  }
}

#ifdef ZHI_ARM_VFP
static uint32_t vfpr(int r)
{
  if(r<TREG_F0 || r>TREG_F7)
    错误_打印("compiler error! register %i is no vfp register",r);
  return r - TREG_F0;
}
#else
static uint32_t fpr(int r)
{
  if(r<TREG_F0 || r>TREG_F3)
    错误_打印("compiler error! register %i is no fpa register",r);
  return r - TREG_F0;
}
#endif

static uint32_t intr(int r)
{
  if(r == TREG_R12)
    return 12;
  if(r >= TREG_R0 && r <= TREG_R3)
    return r - TREG_R0;
  if (!(r >= TREG_SP && r <= TREG_LR))
    错误_打印("compiler error! register %i is no int register",r);
  return r + (13 - TREG_SP);
}

static void calcaddr(uint32_t *base, int *off, int *sgn, int maxoff, unsigned shift)
{
  if(*off>maxoff || *off&((1<<shift)-1)) {
    uint32_t x, y;
    x=0xE280E000;
    if(*sgn)
      x=0xE240E000;
    x|=(*base)<<16;
    *base=14; // lr
    y=stuff_const(x,*off&~maxoff);
    if(y) {
      o(y);
      *off&=maxoff;
      return;
    }
    y=stuff_const(x,(*off+maxoff)&~maxoff);
    if(y) {
      o(y);
      *sgn=!*sgn;
      *off=((*off+maxoff)&~maxoff)-*off;
      return;
    }
    stuff_const_harder(x,*off&~maxoff);
    *off&=maxoff;
  }
}

static uint32_t mapcc(int cc)
{
  switch(cc)
  {
    case 符号_ULT:
      return 0x30000000; /* CC/LO */
    case 符号_UGE:
      return 0x20000000; /* CS/HS */
    case 双符号_等于:
      return 0x00000000; /* EQ */
    case 双符号_不等于:
      return 0x10000000; /* NE */
    case 符号_ULE:
      return 0x90000000; /* LS */
    case 符_UGT:
      return 0x80000000; /* HI */
    case 符_Nset:
      return 0x40000000; /* MI */
    case 符_Nclear:
      return 0x50000000; /* PL */
    case 符_LT:
      return 0xB0000000; /* LT */
    case 双符号_大于等于:
      return 0xA0000000; /* GE */
    case 双符号_小于等于:
      return 0xD0000000; /* LE */
    case 符_GT:
      return 0xC0000000; /* GT */
  }
  错误_打印("unexpected condition code");
  return 0xE0000000; /* AL */
}

static int negcc(int cc)
{
  switch(cc)
  {
    case 符号_ULT:
      return 符号_UGE;
    case 符号_UGE:
      return 符号_ULT;
    case 双符号_等于:
      return 双符号_不等于;
    case 双符号_不等于:
      return 双符号_等于;
    case 符号_ULE:
      return 符_UGT;
    case 符_UGT:
      return 符号_ULE;
    case 符_Nset:
      return 符_Nclear;
    case 符_Nclear:
      return 符_Nset;
    case 符_LT:
      return 双符号_大于等于;
    case 双符号_大于等于:
      return 符_LT;
    case 双符号_小于等于:
      return 符_GT;
    case 符_GT:
      return 双符号_小于等于;
  }
  错误_打印("unexpected condition code");
  return 双符号_不等于;
}

/* 加载 'r' from value 'sv' */
void 加载(int r, 堆栈值 *sv)
{
  int v, ft, fc, fr, sign;
  uint32_t op;
  堆栈值 v1;

  fr = sv->r;
  ft = sv->type.t;
  fc = sv->c.i;

  if(fc>=0)
    sign=0;
  else {
    sign=1;
    fc=-fc;
  }

  v = fr & VT_值掩码;
  if (fr & VT_LVAL) {
    uint32_t base = 0xB; // fp
    if(v == VT_LLOCAL) {
      v1.type.t = VT_指针;
      v1.r = VT_LOCAL | VT_LVAL;
      v1.c.i = sv->c.i;
      加载(TREG_LR, &v1);
      base = 14; /* lr */
      fc=sign=0;
      v=VT_LOCAL;
    } else if(v == VT_VC常量) {
      v1.type.t = VT_指针;
      v1.r = fr&~VT_LVAL;
      v1.c.i = sv->c.i;
      v1.sym=sv->sym;
      加载(TREG_LR, &v1);
      base = 14; /* lr */
      fc=sign=0;
      v=VT_LOCAL;
    } else if(v < VT_VC常量) {
      base=intr(v);
      fc=sign=0;
      v=VT_LOCAL;
    }
    if(v == VT_LOCAL) {
      if(是_浮点型(ft)) {
	calcaddr(&base,&fc,&sign,1020,2);
#ifdef ZHI_ARM_VFP
        op=0xED100A00; /* flds */
        if(!sign)
          op|=0x800000;
        if ((ft & VT_基本类型) != VT_浮点)
          op|=0x100;   /* flds -> fldd */
        o(op|(vfpr(r)<<12)|(fc>>2)|(base<<16));
#else
	op=0xED100100;
	if(!sign)
	  op|=0x800000;
#if 长双精度_大小 == 8
	if ((ft & VT_基本类型) != VT_浮点)
	  op|=0x8000;
#else
	if ((ft & VT_基本类型) == VT_双精度)
	  op|=0x8000;
	else if ((ft & VT_基本类型) == VT_长双精度)
	  op|=0x400000;
#endif
	o(op|(fpr(r)<<12)|(fc>>2)|(base<<16));
#endif
      } else if((ft & (VT_基本类型|VT_无符号)) == VT_字节
                || (ft & VT_基本类型) == VT_短整数) {
	calcaddr(&base,&fc,&sign,255,0);
	op=0xE1500090;
	if ((ft & VT_基本类型) == VT_短整数)
	  op|=0x20;
	if ((ft & VT_无符号) == 0)
	  op|=0x40;
	if(!sign)
	  op|=0x800000;
	o(op|(intr(r)<<12)|(base<<16)|((fc&0xf0)<<4)|(fc&0xf));
      } else {
	calcaddr(&base,&fc,&sign,4095,0);
	op=0xE5100000;
	if(!sign)
	  op|=0x800000;
        if ((ft & VT_基本类型) == VT_字节 || (ft & VT_基本类型) == VT_逻辑)
          op|=0x400000;
        o(op|(intr(r)<<12)|fc|(base<<16));
      }
      return;
    }
  } else {
    if (v == VT_VC常量) {
      op=stuff_const(0xE3A00000|(intr(r)<<12),sv->c.i);
      if (fr & VT_符号 || !op) {
        o(0xE59F0000|(intr(r)<<12));
        o(0xEA000000);
        if(fr & VT_符号)
	  段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引, R_ARM_ABS32);
        o(sv->c.i);
      } else
        o(op);
      return;
    } else if (v == VT_LOCAL) {
      op=stuff_const(0xE28B0000|(intr(r)<<12),sv->c.i);
      if (fr & VT_符号 || !op) {
	o(0xE59F0000|(intr(r)<<12));
	o(0xEA000000);
	if(fr & VT_符号) // needed ?
	  段部分符号重定位(当前_生成代码_段, sv->sym, 输出代码索引, R_ARM_ABS32);
	o(sv->c.i);
	o(0xE08B0000|(intr(r)<<12)|intr(r));
      } else
	o(op);
      return;
    } else if(v == VT_CMP) {
      o(mapcc(sv->c.i)|0x3A00001|(intr(r)<<12));
      o(mapcc(negcc(sv->c.i))|0x3A00000|(intr(r)<<12));
      return;
    } else if (v == VT_JMP || v == VT_JMPI) {
      int t;
      t = v & 1;
      o(0xE3A00000|(intr(r)<<12)|t);
      o(0xEA000000);
      生成符号(sv->c.i);
      o(0xE3A00000|(intr(r)<<12)|(t^1));
      return;
    } else if (v < VT_VC常量) {
      if(是_浮点型(ft))
#ifdef ZHI_ARM_VFP
        o(0xEEB00A40|(vfpr(r)<<12)|vfpr(v)|T2CPR(ft)); /* fcpyX */
#else
	o(0xEE008180|(fpr(r)<<12)|fpr(v));
#endif
      else
	o(0xE1A00000|(intr(r)<<12)|intr(v));
      return;
    }
  }
  错误_打印("加载 unimplemented!");
}

/* 存储 register 'r' in lvalue 'v' */
void 存储(int r, 堆栈值 *sv)
{
  堆栈值 v1;
  int v, ft, fc, fr, sign;
  uint32_t op;

  fr = sv->r;
  ft = sv->type.t;
  fc = sv->c.i;

  if(fc>=0)
    sign=0;
  else {
    sign=1;
    fc=-fc;
  }

  v = fr & VT_值掩码;
  if (fr & VT_LVAL || fr == VT_LOCAL) {
    uint32_t base = 0xb; /* fp */
    if(v < VT_VC常量) {
      base=intr(v);
      v=VT_LOCAL;
      fc=sign=0;
    } else if(v == VT_VC常量) {
      v1.type.t = ft;
      v1.r = fr&~VT_LVAL;
      v1.c.i = sv->c.i;
      v1.sym=sv->sym;
      加载(TREG_LR, &v1);
      base = 14; /* lr */
      fc=sign=0;
      v=VT_LOCAL;
    }
    if(v == VT_LOCAL) {
       if(是_浮点型(ft)) {
	calcaddr(&base,&fc,&sign,1020,2);
#ifdef ZHI_ARM_VFP
        op=0xED000A00; /* fsts */
        if(!sign)
          op|=0x800000;
        if ((ft & VT_基本类型) != VT_浮点)
          op|=0x100;   /* fsts -> fstd */
        o(op|(vfpr(r)<<12)|(fc>>2)|(base<<16));
#else
	op=0xED000100;
	if(!sign)
	  op|=0x800000;
#if 长双精度_大小 == 8
	if ((ft & VT_基本类型) != VT_浮点)
	  op|=0x8000;
#else
	if ((ft & VT_基本类型) == VT_双精度)
	  op|=0x8000;
	if ((ft & VT_基本类型) == VT_长双精度)
	  op|=0x400000;
#endif
	o(op|(fpr(r)<<12)|(fc>>2)|(base<<16));
#endif
	return;
      } else if((ft & VT_基本类型) == VT_短整数) {
	calcaddr(&base,&fc,&sign,255,0);
	op=0xE14000B0;
	if(!sign)
	  op|=0x800000;
	o(op|(intr(r)<<12)|(base<<16)|((fc&0xf0)<<4)|(fc&0xf));
      } else {
	calcaddr(&base,&fc,&sign,4095,0);
	op=0xE5000000;
	if(!sign)
	  op|=0x800000;
        if ((ft & VT_基本类型) == VT_字节 || (ft & VT_基本类型) == VT_逻辑)
          op|=0x400000;
        o(op|(intr(r)<<12)|fc|(base<<16));
      }
      return;
    }
  }
  错误_打印("存储 unimplemented");
}

static void gadd_sp(int val)
{
  stuff_const_harder(0xE28DD000,val);
}

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
  int r;
  uint32_t x;
  if ((栈顶值->r & (VT_值掩码 | VT_LVAL)) == VT_VC常量) {
    /* constant case */
	if(栈顶值->r & VT_符号){
		x=encbranch(输出代码索引,输出代码索引+栈顶值->c.i,0);
		if(x) {
		/* relocation case */
		  段部分符号重定位(当前_生成代码_段, 栈顶值->sym, 输出代码索引, R_ARM_PC24);
		  o(x|(is_jmp?0xE0000000:0xE1000000));
		} else {
			if(!is_jmp)
				o(0xE28FE004); // add lr,pc,#4
			o(0xE51FF004);   // ldr pc,[pc,#-4]
			段部分符号重定位(当前_生成代码_段, 栈顶值->sym, 输出代码索引, R_ARM_ABS32);
			o(栈顶值->c.i);
		}
#ifdef 配置_ZHI_边界检查
                if (zhi_状态->执行_边界_检查器 &&
                    (栈顶值->sym->v == 符_setjmp ||
                     栈顶值->sym->v == 符__setjmp ||
                     栈顶值->sym->v == 符_sigsetjmp ||
                     栈顶值->sym->v == 符___sigsetjmp))
                    func_bound_add_epilog = 1;
#endif
	}else{
		if(!is_jmp)
			o(0xE28FE004); // add lr,pc,#4
		o(0xE51FF004);   // ldr pc,[pc,#-4]
		o(栈顶值->c.i);
	}
  } else {
    /* otherwise, 间接的ect call */
#ifdef 配置_ZHI_边界检查
    栈顶值->r &= ~VT_强制边界检查;
#endif
    r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
    if(!is_jmp)
      o(0xE1A0E00F);       // mov lr,pc
    o(0xE1A0F000|intr(r)); // mov pc,r
  }
}

#if defined(配置_ZHI_边界检查)

static void 生成_bounds_call(int v)
{
    符号 *sym = 外部_全局_符号(v, &函数_旧_类型);

    段部分符号重定位(当前_生成代码_段, sym, 输出代码索引, R_ARM_PC24);
    o(0xebfffffe);
}

/* generate a bounded pointer addition */
静态_函数 void 生成_边界的_ptr_添加(void)
{
    推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_ptr_add);
    vrott(3);
    具体地址函数_调用(2);
    压入整数常量(0);
    /* returned pointer is in 寄存器_返回16位整数寄存器 */
    栈顶值->r = 寄存器_返回16位整数寄存器 | VT_有界的;
    if (不需要_代码生成)
        return;
    /* relocation offset of the bounding function call point */
    栈顶值->c.i = (当前_生成代码_段->重定位->数据_偏移 - sizeof(Elf32_Rel));
}

/* patch pointer addition in 栈顶值 so that pointer dereferencing is
   also tested */
静态_函数 void 生成_边界的_ptr_取消引用(void)
{
    目标地址_类型 func;
    int size, align;
    Elf32_Rel *rel;
    符号 *sym;

    if (不需要_代码生成)
        return;

    size = 类型_大小(&栈顶值->type, &align);
    switch(size) {
    case  1: func = 符___bound_ptr_间接的1; break;
    case  2: func = 符___bound_ptr_间接的2; break;
    case  4: func = 符___bound_ptr_间接的4; break;
    case  8: func = 符___bound_ptr_间接的8; break;
    case 12: func = 符___bound_ptr_间接的12; break;
    case 16: func = 符___bound_ptr_间接的16; break;
    default:
        /* may happen with struct member access */
        return;
        //错误_打印("unhandled size when dereferencing bounded pointer");
        //func = 0;
        //break;
    }
    sym = 外部_全局_符号(func, &函数_旧_类型);
    if (!sym->c)
        更新_外部_符号(sym, NULL, 0, 0);
    /* patch relocation */
    /* XXX: find a better solution ? */
    rel = (Elf32_Rel *)(当前_生成代码_段->重定位->data + 栈顶值->c.i);
    rel->r_info = ELF32_R_INFO(sym->c, ELF32_R_TYPE(rel->r_info));
}

static void 生成_bounds_prolog(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 本地边界_部分->数据_偏移;
    func_bound_ind = 输出代码索引;
    func_bound_add_epilog = 0;
    o(0xe1a00000);  /* ld r0,本地边界_部分->数据_偏移 */
    o(0xe1a00000);
    o(0xe1a00000);
    o(0xe1a00000);  /* call __bound_local_new */
}

static void 生成_bounds_epilog(void)
{
    目标地址_类型 saved_ind;
    目标地址_类型 *bounds_ptr;
    符号 *sym_data;
    int offset_modified = func_bound_offset != 本地边界_部分->数据_偏移;

    if (!offset_modified && !func_bound_add_epilog)
        return;

    /* add end of table info */
    bounds_ptr = 段_ptr_添加(本地边界_部分, sizeof(目标地址_类型));
    *bounds_ptr = 0;

    sym_data = 返回_指向节的_静态符号(&字符_指针_类型, 本地边界_部分,
                           func_bound_offset, 本地边界_部分->数据_偏移);

    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 输出代码索引;
        输出代码索引 = func_bound_ind;
        o(0xe59f0000);  /* ldr r0, [pc] */
        o(0xea000000);  /* b $+4 */
        段部分符号重定位(当前_生成代码_段, sym_data, 输出代码索引, R_ARM_ABS32);
        o(0x00000000);  /* 本地边界_部分->数据_偏移 */
        生成_bounds_call(符___bound_local_new);
        输出代码索引 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0xe92d0003);  /* push {r0,r1} */
    o(0xed2d0b02);  /* 压入指定类型常量 {d0} */
    o(0xe59f0000);  /* ldr r0, [pc] */
    o(0xea000000);  /* b $+4 */
    段部分符号重定位(当前_生成代码_段, sym_data, 输出代码索引, R_ARM_ABS32);
    o(0x00000000);  /* 本地边界_部分->数据_偏移 */
    生成_bounds_call(符___bound_local_delete);
    o(0xecbd0b02); /* 弹出堆栈值 {d0} */
    o(0xe8bd0003); /* pop {r0,r1} */
}
#endif

static int unalias_ldbl(int btype)
{
#if 长双精度_大小 == 8
    if (btype == VT_长双精度)
      btype = VT_双精度;
#endif
    return btype;
}

/* Return whether a structure is an homogeneous float aggregate or not.
   The answer is true if all the elements of the structure are of the same
   primitive float type and there is less than 4 elements.

   type: the type corresponding to the structure to be tested */
static int is_h生成_float_aggr(C类型 *type)
{
  if ((type->t & VT_基本类型) == VT_结构体) {
    struct 符号 *ref;
    int btype, 数量_fields = 0;

    ref = type->ref->next;
    btype = unalias_ldbl(ref->type.t & VT_基本类型);
    if (btype == VT_浮点 || btype == VT_双精度) {
      for(; ref && btype == unalias_ldbl(ref->type.t & VT_基本类型); ref = ref->next, 数量_fields++);
      return !ref && 数量_fields <= 4;
    }
  }
  return 0;
}

struct avail_regs {
  signed char avail[3]; /* 3 holes max with only float and double alignments */
  int first_hole; /* first available hole */
  int last_hole; /* last available hole (none if equal to first_hole) */
  int first_free_reg; /* next free register in the sequence, hole excluded */
};

#define AVAIL_REGS_INITIALIZER (struct avail_regs) { { 0, 0, 0}, 0, 0, 0 }

/* Find suitable registers for a VFP Co-Processor Register Candidate (VFP CPRC
   param) according to the rules described in the procedure call standard for
   the ARM architecture (AAPCS). If found, the registers are assigned to this
   VFP CPRC parameter. Registers are allocated in sequence unless a hole exists
   and the parameter is a single float.

   avregs: 不透明 structure to keep track of available VFP co-processor regs
   align: alignment constraints for the param, as returned by 类型_大小()
   size: size of the parameter, as returned by 类型_大小() */
int assign_vfpreg(struct avail_regs *avregs, int align, int size)
{
  int first_reg = 0;

  if (avregs->first_free_reg == -1)
    return -1;
  if (align >> 3) { /* double alignment */
    first_reg = avregs->first_free_reg;
    /* alignment constraint not respected so use next reg and record hole */
    if (first_reg & 1)
      avregs->avail[avregs->last_hole++] = first_reg++;
  } else { /* no special alignment (float or array of float) */
    /* if single float and a hole is available, assign the param to it */
    if (size == 4 && avregs->first_hole != avregs->last_hole)
      return avregs->avail[avregs->first_hole++];
    else
      first_reg = avregs->first_free_reg;
  }
  if (first_reg + size / 4 <= 16) {
    avregs->first_free_reg = first_reg + size / 4;
    return first_reg;
  }
  avregs->first_free_reg = -1;
  return -1;
}

/* Returns whether all params need to be passed in core registers or not.
   This is the case for function part of the runtime ABI. */
int floats_in_core_regs(堆栈值 *sval)
{
  if (!sval->sym)
    return 0;

  switch (sval->sym->v) {
    case 符___floatundisf:
    case 符___floatundidf:
    case 符___fixunssfdi:
    case 符___fixunsdfdi:
#ifndef ZHI_ARM_VFP
    case 符___fixunsxfdi:
#endif
    case 符___floatdisf:
    case 符___floatdidf:
    case 符___fixsfdi:
    case 符___fixdfdi:
      return 1;

    default:
      return 0;
  }
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态_函数 int gfunc_sret(C类型 *vt, int variadic, C类型 *ret, int *ret_align, int *regsize) {
#ifdef ZHI_ARM_EABI
    int size, align;
    size = 类型_大小(vt, &align);
    if (浮动_abi == ARM_HARD_FLOAT && !variadic &&
        (是_浮点型(vt->t) || is_h生成_float_aggr(vt))) {
        *ret_align = 8;
	*regsize = 8;
        ret->ref = NULL;
        ret->t = VT_双精度;
        return (size + 7) >> 3;
    } else if (size <= 4) {
        *ret_align = 4;
	*regsize = 4;
        ret->ref = NULL;
        ret->t = VT_整数;
        return 1;
    } else
        return 0;
#else
    return 0;
#endif
}

/* Parameters are classified according to how they are copied to their final
   destination for the function call. Because the copying is performed class
   after class according to the order in the union below, it is important that
   some constraints about the order of the members of this union are respected:
   - CORE_STRUCT_CLASS must come after STACK_CLASS;
   - CORE_CLASS must come after STACK_CLASS, CORE_STRUCT_CLASS and
     VFP_STRUCT_CLASS;
   - VFP_STRUCT_CLASS must come after VFP_CLASS.
   See the comment for the main loop in copy_params() for the reason. */
enum reg_class {
	STACK_CLASS = 0,
	CORE_STRUCT_CLASS,
	VFP_CLASS,
	VFP_STRUCT_CLASS,
	CORE_CLASS,
	NB_CLASSES
};

struct param_plan {
    int start; /* first reg or addr used depending on the class */
    int end; /* last reg used or next free addr depending on the class */
    堆栈值 *sval; /* pointer to 堆栈值 on the value stack */
    struct param_plan *prev; /*  previous element in this class */
};

struct plan {
    struct param_plan *pplans; /* array of all the param plans */
    struct param_plan *clsplans[NB_CLASSES]; /* per class lists of param plans */
};

#define add_param_plan(plan,pplan,class)                        \
    do {                                                        \
        pplan.prev = plan->clsplans[class];                     \
        plan->pplans[plan ## _nb] = pplan;                      \
        plan->clsplans[class] = &plan->pplans[plan ## _nb++];   \
    } while(0)

/* Assign parameters to registers and stack with alignment according to the
   rules in the procedure call standard for the ARM architecture (AAPCS).
   The overall assignment is recorded in an array of per parameter structures
   called parameter plans. The parameter plans are also further organized in a
   number of linked lists, one per class of parameter (see the comment for the
   definition of union reg_class).

   数量_args: number of parameters of the function for which a call is generated
   浮动_abi: float ABI in use for this function call
   plan: the structure where the overall assignment is recorded
   todo: a bitmap that record which core registers hold a parameter

   Returns the amount of stack space needed for parameter passing

   Note: this function allocated an array in plan->pplans with 内存_申请. It
   is the responsibility of the caller to free this array once used (ie not
   before copy_params). */
static int assign_regs(int 数量_args, int 浮动_abi, struct plan *plan, int *todo)
{
  int i, size, align;
  int ncrn /* next core register number */, nsaa /* next stacked argument address*/;
  int plan_nb = 0;
  struct param_plan pplan;
  struct avail_regs avregs = AVAIL_REGS_INITIALIZER;

  ncrn = nsaa = 0;
  *todo = 0;
  plan->pplans = 数量_args ? 内存_申请(数量_args * sizeof(*plan->pplans)) : NULL;
  memset(plan->clsplans, 0, sizeof(plan->clsplans));
  for(i = 数量_args; i-- ;) {
    int j, start_vfpreg = 0;
    C类型 type = 栈顶值[-i].type;
    type.t &= ~VT_数组;
    size = 类型_大小(&type, &align);
    size = (size + 3) & ~3;
    align = (align + 3) & ~3;
    switch(栈顶值[-i].type.t & VT_基本类型) {
      case VT_结构体:
      case VT_浮点:
      case VT_双精度:
      case VT_长双精度:
      if (浮动_abi == ARM_HARD_FLOAT) {
        int is_hfa = 0; /* Homogeneous float aggregate */

        if (是_浮点型(栈顶值[-i].type.t)
            || (is_hfa = is_h生成_float_aggr(&栈顶值[-i].type))) {
          int end_vfpreg;

          start_vfpreg = assign_vfpreg(&avregs, align, size);
          end_vfpreg = start_vfpreg + ((size - 1) >> 2);
          if (start_vfpreg >= 0) {
            pplan = (struct param_plan) {start_vfpreg, end_vfpreg, &栈顶值[-i]};
            if (is_hfa)
              add_param_plan(plan, pplan, VFP_STRUCT_CLASS);
            else
              add_param_plan(plan, pplan, VFP_CLASS);
            continue;
          } else
            break;
        }
      }
      ncrn = (ncrn + (align-1)/4) & ~((align/4) - 1);
      if (ncrn + size/4 <= 4 || (ncrn < 4 && start_vfpreg != -1)) {
        /* The parameter is allocated both in core register and on stack. As
	 * such, it can be of either class: it would either be the last of
	 * CORE_STRUCT_CLASS or the first of STACK_CLASS. */
        for (j = ncrn; j < 4 && j < ncrn + size / 4; j++)
          *todo|=(1<<j);
        pplan = (struct param_plan) {ncrn, j, &栈顶值[-i]};
        add_param_plan(plan, pplan, CORE_STRUCT_CLASS);
        ncrn += size/4;
        if (ncrn > 4)
          nsaa = (ncrn - 4) * 4;
      } else {
        ncrn = 4;
        break;
      }
      continue;
      default:
      if (ncrn < 4) {
        int is_long = (栈顶值[-i].type.t & VT_基本类型) == VT_长长整数;

        if (is_long) {
          ncrn = (ncrn + 1) & -2;
          if (ncrn == 4)
            break;
        }
        pplan = (struct param_plan) {ncrn, ncrn, &栈顶值[-i]};
        ncrn++;
        if (is_long)
          pplan.end = ncrn++;
        add_param_plan(plan, pplan, CORE_CLASS);
        continue;
      }
    }
    nsaa = (nsaa + (align - 1)) & ~(align - 1);
    pplan = (struct param_plan) {nsaa, nsaa + size, &栈顶值[-i]};
    add_param_plan(plan, pplan, STACK_CLASS);
    nsaa += size; /* size already rounded up before */
  }
  return nsaa;
}

#undef add_param_plan

/* Copy parameters to their final destination (core reg, VFP reg or stack) for
   function call.

   数量_args: number of parameters the function take
   plan: the overall assignment plan for parameters
   todo: a bitmap indicating what core reg will hold a parameter

   Returns the number of 堆栈值 added by this function on the value stack */
static int copy_params(int 数量_args, struct plan *plan, int todo)
{
  int size, align, r, i, 数量_extra_sval = 0;
  struct param_plan *pplan;
  int pass = 0;

   /* Several constraints require parameters to be copied in a specific order:
      - structures are copied to the stack before being loaded in a reg;
      - floats loaded to an odd numbered VFP reg are first copied to the
        preceding even numbered VFP reg and then moved to the next VFP reg.

      It is thus important that:
      - structures assigned to core regs must be copied after parameters
        assigned to the stack but before structures assigned to VFP regs because
        a structure can lie partly in core registers and partly on the stack;
      - parameters assigned to the stack and all structures be copied before
        parameters assigned to a core reg since copying a parameter to the stack
        require using a core reg;
      - parameters assigned to VFP regs be copied before structures assigned to
        VFP regs as the copy might use an even numbered VFP reg that already
        holds part of a structure. */
again:
  for(i = 0; i < NB_CLASSES; i++) {
    for(pplan = plan->clsplans[i]; pplan; pplan = pplan->prev) {

      if (pass
          && (i != CORE_CLASS || pplan->sval->r < VT_VC常量))
        continue;

      vpushv(pplan->sval);
      pplan->sval->r = pplan->sval->r2 = VT_VC常量; /* disable entry */
      switch(i) {
        case STACK_CLASS:
        case CORE_STRUCT_CLASS:
        case VFP_STRUCT_CLASS:
          if ((pplan->sval->type.t & VT_基本类型) == VT_结构体) {
            int padding = 0;
            size = 类型_大小(&pplan->sval->type, &align);
            /* align to stack align size */
            size = (size + 3) & ~3;
            if (i == STACK_CLASS && pplan->prev)
              padding = pplan->start - pplan->prev->end;
            size += padding; /* Add padding if any */
            /* allocate the necessary size on stack */
            gadd_sp(-size);
            /* generate structure 存储 */
            r = 查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数);
            o(0xE28D0000|(intr(r)<<12)|padding); /* add r, sp, padding */
            vset(&栈顶值->type, r | VT_LVAL, 0);
            vswap();
            将栈顶值_存储在堆栈左值(); /* memcpy to current sp + potential padding */

            /* Homogeneous float aggregate are loaded to VFP registers
               immediately since there is no way of loading data in multiple
               non consecutive VFP registers as what is done for other
               structures (see the use of todo). */
            if (i == VFP_STRUCT_CLASS) {
              int first = pplan->start, nb = pplan->end - first + 1;
              /* 弹出堆栈值.32 {pplan->start, ..., pplan->end} */
              o(0xECBD0A00|(first&1)<<22|(first>>1)<<12|nb);
              /* No need to write the register used to a 堆栈值 since VFP regs
                 cannot be used for gcall_or_jmp */
            }
          } else {
            if (是_浮点型(pplan->sval->type.t)) {
#ifdef ZHI_ARM_VFP
              r = vfpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点)) << 12;
              if ((pplan->sval->type.t & VT_基本类型) == VT_浮点)
                size = 4;
              else {
                size = 8;
                r |= 0x101; /* 压入指定类型常量.32 -> 压入指定类型常量.64 */
              }
              o(0xED2D0A01 + r); /* 压入指定类型常量 */
#else
              r = fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点)) << 12;
              if ((pplan->sval->type.t & VT_基本类型) == VT_浮点)
                size = 4;
              else if ((pplan->sval->type.t & VT_基本类型) == VT_双精度)
                size = 8;
              else
                size = 长双精度_大小;

              if (size == 12)
                r |= 0x400000;
              else if(size == 8)
                r|=0x8000;

              o(0xED2D0100|r|(size>>2)); /* some kind of 压入指定类型常量 for FPA */
#endif
            } else {
              /* simple type (currently always same size) */
              /* XXX: implicit cast ? */
              size=4;
              if ((pplan->sval->type.t & VT_基本类型) == VT_长长整数) {
                整数扩展();
                size = 8;
                r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
                o(0xE52D0004|(intr(r)<<12)); /* push r */
                栈顶值--;
              }
              r = 将rc寄存器值存储在栈顶值中(寄存器类_整数);
              o(0xE52D0004|(intr(r)<<12)); /* push r */
            }
            if (i == STACK_CLASS && pplan->prev)
              gadd_sp(pplan->prev->end - pplan->start); /* Add padding if any */
          }
          break;

        case VFP_CLASS:
          将rc寄存器值存储在栈顶值中(regmask(TREG_F0 + (pplan->start >> 1)));
          if (pplan->start & 1) { /* Must be in upper part of double register */
            o(0xEEF00A40|((pplan->start>>1)<<12)|(pplan->start>>1)); /* vmov.f32 s(n+1), sn */
            栈顶值->r = VT_VC常量; /* avoid being saved on stack by 将rc寄存器值存储在栈顶值中 for next float */
          }
          break;

        case CORE_CLASS:
          if ((pplan->sval->type.t & VT_基本类型) == VT_长长整数) {
            整数扩展();
            将rc寄存器值存储在栈顶值中(regmask(pplan->end));
            pplan->sval->r2 = 栈顶值->r;
            栈顶值--;
          }
          将rc寄存器值存储在栈顶值中(regmask(pplan->start));
          /* Mark register as used so that gcall_or_jmp use another one
             (regs >=4 are free as never used to pass parameters) */
          pplan->sval->r = 栈顶值->r;
          break;
      }
      栈顶值--;
    }
  }

  /* second pass to restore registers that were saved on stack by accident.
     Maybe redundant after the "lvalue_save" patch in 语法分析.c:将rc寄存器值存储在栈顶值中() */
  if (++pass < 2)
    goto again;

  /* Manually free remaining registers since next parameters are loaded
   * manually, without the help of 将rc寄存器值存储在栈顶值中(int). */
  保存_寄存器最多n个堆栈条目(数量_args);

  if(todo) {
    o(0xE8BD0000|todo); /* pop {todo} */
    for(pplan = plan->clsplans[CORE_STRUCT_CLASS]; pplan; pplan = pplan->prev) {
      int r;
      pplan->sval->r = pplan->start;
      /* An 堆栈值 can only pin 2 registers at best (r and r2) but a structure
         can occupy more than 2 registers. Thus, we need to push on the value
         stack some fake parameter to have on 堆栈值 for each registers used
         by a structure (r2 is not used). */
      for (r = pplan->start + 1; r <= pplan->end; r++) {
        if (todo & (1 << r)) {
          数量_extra_sval++;
          压入整数常量(0);
          栈顶值->r = r;
        }
      }
    }
  }
  return 数量_extra_sval;
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */
void 具体地址函数_调用(int 数量_args)
{
  int r, args_size;
  int def_float_abi = 浮动_abi;
  int todo;
  struct plan plan;
#ifdef ZHI_ARM_EABI
  int variadic;
#endif

#ifdef 配置_ZHI_边界检查
  if (zhi_状态->执行_边界_检查器)
    生成左值边界代码_函数参数加载到寄存器(数量_args);
#endif

#ifdef ZHI_ARM_EABI
  if (浮动_abi == ARM_HARD_FLOAT) {
    variadic = (栈顶值[-数量_args].type.ref->f.func_type == 函数_省略);
    if (variadic || floats_in_core_regs(&栈顶值[-数量_args]))
      浮动_abi = ARM_SOFTFP_FLOAT;
  }
#endif
  /* cannot let cpu flags if other instruction are generated. Also avoid leaving
     VT_JMP anywhere except on the top of the stack because it would complicate
     the code generator. */
  r = 栈顶值->r & VT_值掩码;
  if (r == VT_CMP || (r & ~1) == VT_JMP)
    将rc寄存器值存储在栈顶值中(寄存器类_整数);

  args_size = assign_regs(数量_args, 浮动_abi, &plan, &todo);

#ifdef ZHI_ARM_EABI
  if (args_size & 7) { /* Stack must be 8 byte aligned at fct call for EABI */
    args_size = (args_size + 7) & ~7;
    o(0xE24DD004); /* sub sp, sp, #4 */
  }
#endif

  数量_args += copy_params(数量_args, &plan, todo);
  内存_释放(plan.pplans);

  /* Move fct 堆栈值 on top as required by gcall_or_jmp */
  vrotb(数量_args + 1);
  gcall_or_jmp(0);
  if (args_size)
      gadd_sp(args_size); /* pop all parameters passed on the stack */
#if defined(ZHI_ARM_EABI) && defined(ZHI_ARM_VFP)
  if(浮动_abi == ARM_SOFTFP_FLOAT && 是_浮点型(栈顶值->type.ref->type.t)) {
    if((栈顶值->type.ref->type.t & VT_基本类型) == VT_浮点) {
      o(0xEE000A10); /*vmov s0, r0 */
    } else {
      o(0xEE000B10); /* vmov.32 d0[0], r0 */
      o(0xEE201B10); /* vmov.32 d0[1], r1 */
    }
  }
#endif
  栈顶值 -= 数量_args + 1; /* Pop all params and fct address from value stack */
  leaffunc = 0; /* we are calling a function, so we aren't in a leaf function */
  浮动_abi = def_float_abi;
}

/* generate function prolog of type 't' */
void 生成函数_序言(符号 *func_sym)
{
  C类型 *func_type = &func_sym->type;
  符号 *sym,*sym2;
  int n, nf, size, align, rs, struct_ret = 0;
  int addr, pn, sn; /* pn=core, sn=stack */
  C类型 ret_type;

#ifdef ZHI_ARM_EABI
  struct avail_regs avregs = AVAIL_REGS_INITIALIZER;
#endif

  sym = func_type->ref;

  n = nf = 0;
  if ((当前函数_返回类型.t & VT_基本类型) == VT_结构体 &&
      !gfunc_sret(&当前函数_返回类型, 当前函数_可变参数, &ret_type, &align, &rs))
  {
    n++;
    struct_ret = 1;
    函数_vc = 12; /* Offset from fp of the place to 存储 the result */
  }
  for(sym2 = sym->next; sym2 && (n < 4 || nf < 16); sym2 = sym2->next) {
    size = 类型_大小(&sym2->type, &align);
#ifdef ZHI_ARM_EABI
    if (浮动_abi == ARM_HARD_FLOAT && !当前函数_可变参数 &&
        (是_浮点型(sym2->type.t) || is_h生成_float_aggr(&sym2->type))) {
      int tmpnf = assign_vfpreg(&avregs, align, size);
      tmpnf += (size + 3) / 4;
      nf = (tmpnf > nf) ? tmpnf : nf;
    } else
#endif
    if (n < 4)
      n += (size + 3) / 4;
  }
  o(0xE1A0C00D); /* mov ip,sp */
  if (当前函数_可变参数)
    n=4;
  if (n) {
    if(n>4)
      n=4;
#ifdef ZHI_ARM_EABI
    n=(n+1)&-2;
#endif
    o(0xE92D0000|((1<<n)-1)); /* save r0-r4 on stack if needed */
  }
  if (nf) {
    if (nf>16)
      nf=16;
    nf=(nf+1)&-2; /* nf => HARDFLOAT => EABI */
    o(0xED2D0A00|nf); /* save s0-s15 on stack if needed */
  }
  o(0xE92D5800); /* save fp, ip, lr */
  o(0xE1A0B00D); /* mov fp, sp */
  func_sub_sp_offset = 输出代码索引;
  o(0xE1A00000); /* nop, leave space for stack adjustment in epilog */

#ifdef ZHI_ARM_EABI
  if (浮动_abi == ARM_HARD_FLOAT) {
    函数_vc += nf * 4;
    avregs = AVAIL_REGS_INITIALIZER;
  }
#endif
  pn = struct_ret, sn = 0;
  while ((sym = sym->next)) {
    C类型 *type;
    type = &sym->type;
    size = 类型_大小(type, &align);
    size = (size + 3) >> 2;
    align = (align + 3) & ~3;
#ifdef ZHI_ARM_EABI
    if (浮动_abi == ARM_HARD_FLOAT && !当前函数_可变参数 && (是_浮点型(sym->type.t)
        || is_h生成_float_aggr(&sym->type))) {
      int fpn = assign_vfpreg(&avregs, align, size << 2);
      if (fpn >= 0)
        addr = fpn * 4;
      else
        goto from_stack;
    } else
#endif
    if (pn < 4) {
#ifdef ZHI_ARM_EABI
        pn = (pn + (align-1)/4) & -(align/4);
#endif
      addr = (nf + pn) * 4;
      pn += size;
      if (!sn && pn > 4)
        sn = (pn - 4);
    } else {
#ifdef ZHI_ARM_EABI
from_stack:
        sn = (sn + (align-1)/4) & -(align/4);
#endif
      addr = (n + nf + sn) * 4;
      sn += size;
    }
    符号_压入栈(sym->v & ~符号_字段, type, VT_LOCAL | VT_LVAL,
             addr + 12);
  }
  last_itod_magic=0;
  leaffunc = 1;
  局部变量索引 = 0;
#ifdef 配置_ZHI_边界检查
  if (zhi_状态->执行_边界_检查器)
    生成_bounds_prolog();
#endif
}

/* generate function epilog */
void 生成函数_结尾(void)
{
  uint32_t x;
  int diff;

#ifdef 配置_ZHI_边界检查
  if (zhi_状态->执行_边界_检查器)
    生成_bounds_epilog();
#endif
  /* Copy float return value to core register if base standard is used and
     float computation is made with VFP */
#if defined(ZHI_ARM_EABI) && defined(ZHI_ARM_VFP)
  if ((浮动_abi == ARM_SOFTFP_FLOAT || 当前函数_可变参数) && 是_浮点型(当前函数_返回类型.t)) {
    if((当前函数_返回类型.t & VT_基本类型) == VT_浮点)
      o(0xEE100A10); /* fmrs r0, s0 */
    else {
      o(0xEE100B10); /* fmrdl r0, d0 */
      o(0xEE301B10); /* fmrdh r1, d0 */
    }
  }
#endif
  o(0xE89BA800); /* restore fp, sp, pc */
  diff = (-局部变量索引 + 3) & -4;
#ifdef ZHI_ARM_EABI
  if(!leaffunc)
    diff = ((diff + 11) & -8) - 4;
#endif
  if(diff > 0) {
    x=stuff_const(0xE24BD000, diff); /* sub sp,fp,# */
    if(x)
      *(uint32_t *)(当前_生成代码_段->data + func_sub_sp_offset) = x;
    else {
      int addr;
      addr=输出代码索引;
      o(0xE59FC004); /* ldr ip,[pc+4] */
      o(0xE04BD00C); /* sub sp,fp,ip  */
      o(0xE1A0F00E); /* mov pc,lr */
      o(diff);
      *(uint32_t *)(当前_生成代码_段->data + func_sub_sp_offset) = 0xE1000000|encbranch(func_sub_sp_offset,addr,1);
    }
  }
}

静态_函数 void 生成_fill_nops(int bytes)
{
    if ((bytes & 3))
      错误_打印("alignment of code section not multiple of 4");
    while (bytes > 0) {
	o(0xE1A00000);
	bytes -= 4;
    }
}

/* generate a jump to a label */
静态_函数 int 生成跳转到标签(int t)
{
  int r;
  if (不需要_代码生成)
    return t;
  r=输出代码索引;
  o(0xE0000000|encbranch(r,t,1));
  return r;
}

/* generate a jump to a fixed address */
静态_函数 void 生成跳转到_固定地址(int a)
{
  生成跳转到标签(a);
}

静态_函数 int g跳转_条件(int op, int t)
{
  int r;
  if (不需要_代码生成)
    return t;
  r=输出代码索引;
  op=mapcc(op);
  op|=encbranch(r,t,1);
  o(op);
  return r;
}

静态_函数 int g跳转_附件(int n, int t)
{
  uint32_t *x;
  int p,lp;
  if(n) {
    p = n;
    do {
      p = decbranch(lp=p);
    } while(p);
    x = (uint32_t *)(当前_生成代码_段->data + lp);
    *x &= 0xff000000;
    *x |= encbranch(lp,t,1);
    t = n;
  }
  return t;
}

/* generate an integer binary operation */
void 生成_整数二进制运算(int op)
{
  int c, func = 0;
  uint32_t opc = 0, r, fr;
  unsigned short retreg = 寄存器_返回16位整数寄存器;

  c=0;
  switch(op) {
    case '+':
      opc = 0x8;
      c=1;
      break;
    case 符_加进位生成: /* add with carry generation */
      opc = 0x9;
      c=1;
      break;
    case '-':
      opc = 0x4;
      c=1;
      break;
    case 符_减借位生成: /* sub with carry generation */
      opc = 0x5;
      c=1;
      break;
    case 符_加进位使用: /* add with carry use */
      opc = 0xA;
      c=1;
      break;
    case 符_减借位使用: /* sub with carry use */
      opc = 0xC;
      c=1;
      break;
    case '&':
      opc = 0x0;
      c=1;
      break;
    case '^':
      opc = 0x2;
      c=1;
      break;
    case '|':
      opc = 0x18;
      c=1;
      break;
    case '*':
      将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
      r = 栈顶值[-1].r;
      fr = 栈顶值[0].r;
      栈顶值--;
      o(0xE0000090|(intr(r)<<16)|(intr(r)<<8)|intr(fr));
      return;
    case 双符号_左位移:
      opc = 0;
      c=2;
      break;
    case 符_SHR:
      opc = 1;
      c=2;
      break;
    case 双符号_右位移:
      opc = 2;
      c=2;
      break;
    case '/':
    case 符_指针除法:
      func=符___divsi3;
      c=3;
      break;
    case 符_无符除法:
      func=符___udivsi3;
      c=3;
      break;
    case '%':
#ifdef ZHI_ARM_EABI
      func=符___aeabi_idivmod;
      retreg=寄存器_返回32位整数寄存器;
#else
      func=符___modsi3;
#endif
      c=3;
      break;
    case 符_无符取模运算:
#ifdef ZHI_ARM_EABI
      func=符___aeabi_uidivmod;
      retreg=寄存器_返回32位整数寄存器;
#else
      func=符___umodsi3;
#endif
      c=3;
      break;
    case 符_无符32位乘法:
      将rc寄存器值存储在栈顶值中2(寄存器类_整数, 寄存器类_整数);
      r=intr(栈顶值[-1].r2=查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数));
      c=栈顶值[-1].r;
      栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_整数,regmask(c));
      栈顶值--;
      o(0xE0800090|(r<<16)|(intr(栈顶值->r)<<12)|(intr(c)<<8)|intr(栈顶值[1].r));
      return;
    default:
      opc = 0x15;
      c=1;
      break;
  }
  switch(c) {
    case 1:
      if((栈顶值[-1].r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
	if(opc == 4 || opc == 5 || opc == 0xc) {
	  vswap();
	  opc|=2; // sub -> rsb
	}
      }
      if ((栈顶值->r & VT_值掩码) == VT_CMP ||
          (栈顶值->r & (VT_值掩码 & ~1)) == VT_JMP)
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
      vswap();
      c=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
      vswap();
      opc=0xE0000000|(opc<<20);
      if((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
	uint32_t x;
	x=stuff_const(opc|0x2000000|(c<<16),栈顶值->c.i);
	if(x) {
	  r=intr(栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_整数,regmask(栈顶值[-1].r)));
	  o(x|(r<<12));
	  goto done;
	}
      }
      fr=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
      if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
        vswap();
        c=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
        vswap();
      }
      r=intr(栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_整数,two2mask(栈顶值->r,栈顶值[-1].r)));
      o(opc|(c<<16)|(r<<12)|fr);
done:
      栈顶值--;
      if (op >= 符号_ULT && op <= 符_GT)
        vset_VT_CMP(op);
      break;
    case 2:
      opc=0xE1A00000|(opc<<5);
      if ((栈顶值->r & VT_值掩码) == VT_CMP ||
          (栈顶值->r & (VT_值掩码 & ~1)) == VT_JMP)
        将rc寄存器值存储在栈顶值中(寄存器类_整数);
      vswap();
      r=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
      vswap();
      if ((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) == VT_VC常量) {
	fr=intr(栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_整数,regmask(栈顶值[-1].r)));
	c = 栈顶值->c.i & 0x1f;
	o(opc|r|(c<<7)|(fr<<12));
      } else {
        fr=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
          vswap();
          r=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
          vswap();
        }
	c=intr(栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_整数,two2mask(栈顶值->r,栈顶值[-1].r)));
	o(opc|r|(c<<12)|(fr<<8)|0x10);
      }
      栈顶值--;
      break;
    case 3:
      推送对_全局符号V_的引用(&函数_旧_类型, func);
      vrott(3);
      具体地址函数_调用(2);
      压入整数常量(0);
      栈顶值->r = retreg;
      break;
    default:
      错误_打印("生成_整数二进制运算 %i unimplemented!",op);
  }
}

#ifdef ZHI_ARM_VFP
static int is_zero(int i)
{
  if((栈顶值[i].r & (VT_值掩码 | VT_LVAL | VT_符号)) != VT_VC常量)
    return 0;
  if (栈顶值[i].type.t == VT_浮点)
    return (栈顶值[i].c.f == 0.f);
  else if (栈顶值[i].type.t == VT_双精度)
    return (栈顶值[i].c.d == 0.0);
  return (栈顶值[i].c.ld == 0.l);
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
 *    two operands are guaranteed to have the same floating point type */
void 生成_浮点运算(int op)
{
  uint32_t x;
  int fneg=0,r;
  x=0xEE000A00|T2CPR(栈顶值->type.t);
  switch(op) {
    case '+':
      if(is_zero(-1))
        vswap();
      if(is_zero(0)) {
        栈顶值--;
        return;
      }
      x|=0x300000;
      break;
    case '-':
      x|=0x300040;
      if(is_zero(0)) {
        栈顶值--;
        return;
      }
      if(is_zero(-1)) {
        x|=0x810000; /* fsubX -> fnegX */
        vswap();
        栈顶值--;
        fneg=1;
      }
      break;
    case '*':
      x|=0x200000;
      break;
    case '/':
      x|=0x800000;
      break;
    default:
      if(op < 符号_ULT || op > 符_GT) {
        错误_打印("unknown fp op %x!",op);
        return;
      }
      if(is_zero(-1)) {
        vswap();
        switch(op) {
          case 符_LT: op=符_GT; break;
          case 双符号_大于等于: op=符号_ULE; break;
          case 双符号_小于等于: op=双符号_大于等于; break;
          case 符_GT: op=符号_ULT; break;
        }
      }
      x|=0xB40040; /* fcmpX */
      if(op!=双符号_等于 && op!=双符号_不等于)
        x|=0x80; /* fcmpX -> fcmpeX */
      if(is_zero(0)) {
        栈顶值--;
        o(x|0x10000|(vfpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点))<<12)); /* fcmp(e)X -> fcmp(e)zX */
      } else {
        将rc寄存器值存储在栈顶值中2(寄存器类_浮点,寄存器类_浮点);
        x|=vfpr(栈顶值[0].r);
        o(x|(vfpr(栈顶值[-1].r) << 12));
        栈顶值--;
      }
      o(0xEEF1FA10); /* fmstat */

      switch(op) {
        case 双符号_小于等于: op=符号_ULE; break;
        case 符_LT: op=符号_ULT; break;
        case 符号_UGE: op=双符号_大于等于; break;
        case 符_UGT: op=符_GT; break;
      }
      vset_VT_CMP(op);
      return;
  }
  r=将rc寄存器值存储在栈顶值中(寄存器类_浮点);
  x|=vfpr(r);
  r=regmask(r);
  if(!fneg) {
    int r2;
    vswap();
    r2=将rc寄存器值存储在栈顶值中(寄存器类_浮点);
    x|=vfpr(r2)<<16;
    r|=regmask(r2);
    if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
      vswap();
      r=将rc寄存器值存储在栈顶值中(寄存器类_浮点);
      vswap();
      x=(x&~0xf)|vfpr(r);
    }
  }
  栈顶值->r=查找rc2寄存器_如果不存在就调用rc(寄存器类_浮点,r);
  if(!fneg)
    栈顶值--;
  o(x|(vfpr(栈顶值->r)<<12));
}

#else
static uint32_t is_fconst()
{
  long double f;
  uint32_t r;
  if((栈顶值->r & (VT_值掩码 | VT_LVAL | VT_符号)) != VT_VC常量)
    return 0;
  if (栈顶值->type.t == VT_浮点)
    f = 栈顶值->c.f;
  else if (栈顶值->type.t == VT_双精度)
    f = 栈顶值->c.d;
  else
    f = 栈顶值->c.ld;
  if(!ieee_有限的(f))
    return 0;
  r=0x8;
  if(f<0.0) {
    r=0x18;
    f=-f;
  }
  if(f==0.0)
    return r;
  if(f==1.0)
    return r|1;
  if(f==2.0)
    return r|2;
  if(f==3.0)
    return r|3;
  if(f==4.0)
    return r|4;
  if(f==5.0)
    return r|5;
  if(f==0.5)
    return r|6;
  if(f==10.0)
    return r|7;
  return 0;
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point type */
void 生成_浮点运算(int op)
{
  uint32_t x, r, r2, c1, c2;
  //fputs("生成_浮点运算\n",stderr);
  vswap();
  c1 = is_fconst();
  vswap();
  c2 = is_fconst();
  x=0xEE000100;
#if 长双精度_大小 == 8
  if ((栈顶值->type.t & VT_基本类型) != VT_浮点)
    x|=0x80;
#else
  if ((栈顶值->type.t & VT_基本类型) == VT_双精度)
    x|=0x80;
  else if ((栈顶值->type.t & VT_基本类型) == VT_长双精度)
    x|=0x80000;
#endif
  switch(op)
  {
    case '+':
      if(!c2) {
	vswap();
	c2=c1;
      }
      vswap();
      r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
      vswap();
      if(c2) {
	if(c2>0xf)
	  x|=0x200000; // suf
	r2=c2&0xf;
      } else {
	r2=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
          vswap();
          r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
          vswap();
        }
      }
      break;
    case '-':
      if(c2) {
	if(c2<=0xf)
	  x|=0x200000; // suf
	r2=c2&0xf;
	vswap();
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
      } else if(c1 && c1<=0xf) {
	x|=0x300000; // rsf
	r2=c1;
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
      } else {
	x|=0x200000; // suf
	vswap();
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
	r2=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
          vswap();
          r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
          vswap();
        }
      }
      break;
    case '*':
      if(!c2 || c2>0xf) {
	vswap();
	c2=c1;
      }
      vswap();
      r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
      vswap();
      if(c2 && c2<=0xf)
	r2=c2;
      else {
	r2=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
          vswap();
          r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
          vswap();
        }
      }
      x|=0x100000; // muf
      break;
    case '/':
      if(c2 && c2<=0xf) {
	x|=0x400000; // dvf
	r2=c2;
	vswap();
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
      } else if(c1 && c1<=0xf) {
	x|=0x500000; // rdf
	r2=c1;
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
      } else {
	x|=0x400000; // dvf
	vswap();
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
	r2=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
        if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
          vswap();
          r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
          vswap();
        }
      }
      break;
    default:
      if(op >= 符号_ULT && op <= 符_GT) {
	x|=0xd0f110; // cmfe
/* bug (intention?) in Linux FPU emulator
   doesn't set carry if equal */
	switch(op) {
	  case 符号_ULT:
	  case 符号_UGE:
	  case 符号_ULE:
	  case 符_UGT:
            错误_打印("unsigned comparison on floats?");
	    break;
	  case 符_LT:
            op=符_Nset;
	    break;
	  case 双符号_小于等于:
            op=符号_ULE; /* correct in unordered case only if AC bit in FPSR set */
	    break;
	  case 双符号_等于:
	  case 双符号_不等于:
	    x&=~0x400000; // cmfe -> cmf
	    break;
	}
	if(c1 && !c2) {
	  c2=c1;
	  vswap();
	  switch(op) {
            case 符_Nset:
              op=符_GT;
	      break;
            case 双符号_大于等于:
	      op=符号_ULE;
	      break;
	    case 符号_ULE:
              op=双符号_大于等于;
	      break;
            case 符_GT:
              op=符_Nset;
	      break;
	  }
	}
	vswap();
	r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
	vswap();
	if(c2) {
	  if(c2>0xf)
	    x|=0x200000;
	  r2=c2&0xf;
	} else {
	  r2=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
          if ((栈顶值[-1].r & VT_值掩码) >= VT_VC常量) {
            vswap();
            r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
            vswap();
          }
	}
        --栈顶值;
        vset_VT_CMP(op);
        ++栈顶值;
      } else {
        错误_打印("unknown fp op %x!",op);
	return;
      }
  }
  if(栈顶值[-1].r == VT_CMP)
    c1=15;
  else {
    c1=栈顶值->r;
    if(r2&0x8)
      c1=栈顶值[-1].r;
    栈顶值[-1].r=查找rc2寄存器_如果不存在就调用rc(寄存器类_浮点,two2mask(栈顶值[-1].r,c1));
    c1=fpr(栈顶值[-1].r);
  }
  栈顶值--;
  o(x|(r<<16)|(c1<<12)|r2);
}
#endif

/* convert integers to fp 't' type. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
静态_函数 void 生成_整数转换为浮点(int t)
{
  uint32_t r, r2;
  int bt;
  bt=栈顶值->type.t & VT_基本类型;
  if(bt == VT_整数 || bt == VT_短整数 || bt == VT_字节) {
#ifndef ZHI_ARM_VFP
    uint32_t dsize = 0;
#endif
    r=intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
#ifdef ZHI_ARM_VFP
    r2=vfpr(栈顶值->r=查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点));
    o(0xEE000A10|(r<<12)|(r2<<16)); /* fmsr */
    r2|=r2<<12;
    if(!(栈顶值->type.t & VT_无符号))
      r2|=0x80;                /* fuitoX -> fsituX */
    o(0xEEB80A40|r2|T2CPR(t)); /* fYitoX*/
#else
    r2=fpr(栈顶值->r=查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点));
    if((t & VT_基本类型) != VT_浮点)
      dsize=0x80;    /* flts -> fltd */
    o(0xEE000110|dsize|(r2<<16)|(r<<12)); /* flts */
    if((栈顶值->type.t & (VT_无符号|VT_基本类型)) == (VT_无符号|VT_整数)) {
      uint32_t off = 0;
      o(0xE3500000|(r<<12));        /* cmp */
      r=fpr(查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_浮点));
      if(last_itod_magic) {
	off=输出代码索引+8-last_itod_magic;
	off/=4;
	if(off>255)
	  off=0;
      }
      o(0xBD1F0100|(r<<12)|off);    /* ldflts */
      if(!off) {
        o(0xEA000000);              /* b */
        last_itod_magic=输出代码索引;
        o(0x4F800000);              /* 4294967296.0f */
      }
      o(0xBE000100|dsize|(r2<<16)|(r2<<12)|r); /* adflt */
    }
#endif
    return;
  } else if(bt == VT_长长整数) {
    int func;
    C类型 *func_type = 0;
    if((t & VT_基本类型) == VT_浮点) {
      func_type = &func_float_type;
      if(栈顶值->type.t & VT_无符号)
        func=符___floatundisf;
      else
        func=符___floatdisf;
#if 长双精度_大小 != 8
    } else if((t & VT_基本类型) == VT_长双精度) {
      func_type = &func_ldouble_type;
      if(栈顶值->type.t & VT_无符号)
        func=符___floatundixf;
      else
        func=符___floatdixf;
    } else if((t & VT_基本类型) == VT_双精度) {
#else
    } else if((t & VT_基本类型) == VT_双精度 || (t & VT_基本类型) == VT_长双精度) {
#endif
      func_type = &func_double_type;
      if(栈顶值->type.t & VT_无符号)
        func=符___floatundidf;
      else
        func=符___floatdidf;
    }
    if(func_type) {
      推送对_全局符号V_的引用(func_type, func);
      vswap();
      具体地址函数_调用(1);
      压入整数常量(0);
      栈顶值->r=TREG_F0;
      return;
    }
  }
  错误_打印("unimplemented 生成_整数转换为浮点 %x!",栈顶值->type.t);
}

/* convert fp to int 't' type */
void 生成_浮点转换为整数(int t)
{
  uint32_t r, r2;
  int u, func = 0;
  u=t&VT_无符号;
  t&=VT_基本类型;
  r2=栈顶值->type.t & VT_基本类型;
  if(t==VT_整数) {
#ifdef ZHI_ARM_VFP
    r=vfpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
    u=u?0:0x10000;
    o(0xEEBC0AC0|(r<<12)|r|T2CPR(r2)|u); /* ftoXizY */
    r2=intr(栈顶值->r=查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数));
    o(0xEE100A10|(r<<16)|(r2<<12));
    return;
#else
    if(u) {
      if(r2 == VT_浮点)
        func=符___fixunssfsi;
#if 长双精度_大小 != 8
      else if(r2 == VT_长双精度)
	func=符___fixunsxfsi;
      else if(r2 == VT_双精度)
#else
      else if(r2 == VT_长双精度 || r2 == VT_双精度)
#endif
	func=符___fixunsdfsi;
    } else {
      r=fpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
      r2=intr(栈顶值->r=查找释放的rc寄存器_如果不存在就保存一个寄存器(寄存器类_整数));
      o(0xEE100170|(r2<<12)|r);
      return;
    }
#endif
  } else if(t == VT_长长整数) { // unsigned handled in 生成_cvt_ftoi1
    if(r2 == VT_浮点)
      func=符___fixsfdi;
#if 长双精度_大小 != 8
    else if(r2 == VT_长双精度)
      func=符___fixxfdi;
    else if(r2 == VT_双精度)
#else
    else if(r2 == VT_长双精度 || r2 == VT_双精度)
#endif
      func=符___fixdfdi;
  }
  if(func) {
    推送对_全局符号V_的引用(&函数_旧_类型, func);
    vswap();
    具体地址函数_调用(1);
    压入整数常量(0);
    if(t == VT_长长整数)
      栈顶值->r2 = 寄存器_返回32位整数寄存器;
    栈顶值->r = 寄存器_返回16位整数寄存器;
    return;
  }
  错误_打印("unimplemented 生成_浮点转换为整数!");
}

/* convert from one floating point type to another */
void 生成_浮点转换为另一种浮点(int t)
{
#ifdef ZHI_ARM_VFP
  if(((栈顶值->type.t & VT_基本类型) == VT_浮点) != ((t & VT_基本类型) == VT_浮点)) {
    uint32_t r = vfpr(将rc寄存器值存储在栈顶值中(寄存器类_浮点));
    o(0xEEB70AC0|(r<<12)|r|T2CPR(栈顶值->type.t));
  }
#else
  /* all we have to do on i386 and FPA ARM is to put the float in a register */
  将rc寄存器值存储在栈顶值中(寄存器类_浮点);
#endif
}

/* computed goto support */
void g去向(void)
{
  gcall_or_jmp(1);
  栈顶值--;
}

/* Save the stack pointer onto the stack and return the 位置 of its address */
静态_函数 void 生成_vla_sp_保存(int addr) {
    堆栈值 v;
    v.type.t = VT_指针;
    v.r = VT_LOCAL | VT_LVAL;
    v.c.i = addr;
    存储(TREG_SP, &v);
}

/* Restore the SP from a 位置 on the stack */
静态_函数 void 生成_vla_sp_恢复(int addr) {
    堆栈值 v;
    v.type.t = VT_指针;
    v.r = VT_LOCAL | VT_LVAL;
    v.c.i = addr;
    加载(TREG_SP, &v);
}

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态_函数 void 生成_vla_分配(C类型 *type, int align) {
    int r;
#if defined(配置_ZHI_边界检查)
    if (zhi_状态->执行_边界_检查器)
        vpushv(栈顶值);
#endif
    r = intr(将rc寄存器值存储在栈顶值中(寄存器类_整数));
    o(0xE04D0000|(r<<12)|r); /* sub r, sp, r */
#ifdef ZHI_ARM_EABI
    if (align < 8)
        align = 8;
#else
    if (align < 4)
        align = 4;
#endif
    if (align & (align - 1))
        错误_打印("alignment is not a power of 2: %i", align);
    o(stuff_const(0xE3C0D000|(r<<16), align - 1)); /* bic sp, r, #align-1 */
    弹出堆栈值();
#if defined(配置_ZHI_边界检查)
    if (zhi_状态->执行_边界_检查器) {
        压入整数常量(0);
        栈顶值->r = TREG_R0;
        o(0xe1a0000d | (栈顶值->r << 12)); // mov r0,sp
        vswap();
        推送对_全局符号V_的引用(&函数_旧_类型, 符___bound_new_region);
        vrott(3);
        具体地址函数_调用(2);
        func_bound_add_epilog = 1;
    }
#endif
}

/* end of ARM code generator */
/*************************************************************/
#endif
/*************************************************************/
